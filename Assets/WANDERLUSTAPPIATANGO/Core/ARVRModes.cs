﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

// tango mod
public class ARVRModes : MonoBehaviour {
	
	public GameObject goMaskThese;
	public GameObject goARModeOnlyStuff;
	
	public GameObject camAR;
	public GameObject camVR; public static GameObject goMe;  
	
	public GameObject goVRSet;

	public Material matTouchMeLipstick;

	public GameObject goUI_PanelNav;

	public GameObject[] goDisableForEntire = new GameObject[0];
	public GameObject[] goEnableForEntire = new GameObject[0];
	public GameObject[] goDisableForDoor = new GameObject[0];
	public GameObject[] goEnableForDoor = new GameObject[0];

	public enum TheCurrentMode{
		AR,
		VR
	}
	public static TheCurrentMode tcm = TheCurrentMode.VR;

	void Start(){ 
		goMe = goVRSet;
		SwitchVRToAR ();
	}

	void FlipGameObjectArray(GameObject[] g,bool flip){
		for (int i=0; i<g.Length; i++) {
			g[i].SetActive(flip);
		}
	}

	public void SeeEntire(){
		FlipGameObjectArray (goDisableForEntire, false);
		FlipGameObjectArray (goEnableForEntire, true);
	}

	public void SeeDoor(){
		FlipGameObjectArray (goDisableForDoor, false);
		FlipGameObjectArray (goEnableForDoor, true);
	}

	void SetChildrenActive(Transform t,bool flip){
		foreach(Transform tran in t){
			tran.gameObject.SetActive(flip);
		}
	}

	public void SwitchARToVR(){
		goMaskThese.SetActiveRecursively(true);
		goARModeOnlyStuff.SetActive(false);
		goVRSet.SetActive(true);
		camVR.SetActive(true);
		SetChildrenActive(camAR.transform,false);
		matTouchMeLipstick.color = new Color (0,0,0,1);
		Transform camvrparent = camVR.transform.parent;
		//camvrparent.transform.rotation = camAR.transform.rotation;
		Vector3 eulerar = camAR.transform.rotation.eulerAngles;
		Gyro2.StationParent (camAR.transform.position, camAR.transform.rotation);
		Gyro2.ResetStatic ();
		Gyro2.gyroEnabled = true;
		goUI_PanelNav.SetActive (true);
		print ("AR to VR");

		// fix for enter from obstuse angle
		camVR.transform.parent.DORotate (new Vector3(0,eulerar.y,0),3.14f).OnComplete(Gyro2.ResetStaticSmoothly);
	}
	
	public void SwitchVRToAR(){
		//goMaskThese.SetActive(false);
		goARModeOnlyStuff.SetActive(true);
		goVRSet.SetActive(false);
		camVR.SetActive(false);
		SetChildrenActive(camAR.transform,true);
		matTouchMeLipstick.color = new Color (1,1,1,1);
		Gyro2.gyroEnabled = false;
		goUI_PanelNav.SetActive (false);
		print ("VR to AR");
	}

	public void SwitchModes(){
		if (tcm == TheCurrentMode.AR) {
			tcm = TheCurrentMode.VR;
			SwitchARToVR();
		} else if (tcm == TheCurrentMode.VR) {
			tcm = TheCurrentMode.AR;
			SwitchVRToAR();
		}
	}

	void Update(){
		if (Input2.HasPointStarted (1)) {//print ("!");
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);  
			RaycastHit hit ;
			if(Physics.Raycast (ray, out hit, 3000f) ) {
				Debug.DrawRay (Camera.main.transform.position,hit.point);
				string name = hit.transform.name;
//				print (name);
				if(name == "TouchMeLipstick"){
					SwitchModes ();
				}
			}
		}
//		print (Gyro2.gyroEnabled);
//		print (Input2.Swipe ());
	}
	
}
