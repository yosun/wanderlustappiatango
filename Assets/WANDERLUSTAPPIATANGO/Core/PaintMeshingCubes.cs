﻿using UnityEngine;
using System.Collections;

public class PaintMeshingCubes : MonoBehaviour {
 
	void Update () {
		RaycastHit hit = Mathf2.WhatDidWeHit (Input.mousePosition);

		if (hit.point != Mathf2.FarFarAway) {
			string tag = hit.transform.tag;
			GameObject g = hit.transform.gameObject;

			if(tag == "MeshingCube"){
				// TODO - temp shows cube turns red

				Renderer ren = g.GetComponent<Renderer>();
				ren.material.color = new Color(1,0,0,1);
			}
		}
	}
}
