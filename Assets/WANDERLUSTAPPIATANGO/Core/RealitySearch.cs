﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// v0.02 Sep 15 2015 can be used for both finding in-scene SceneSearch and SearchDBToCreate
public class RealitySearch : MonoBehaviour {

	public enum WhichMode
	{
		SceneSearch,
		SearchDBToCreate
	}
	public static WhichMode wm = WhichMode.SearchDBToCreate;

	public GameObject goUI_CanvasSearch; public Text goUI_SearchResult;

	public EachSearchableObject[] ess = new EachSearchableObject[0];

	public Dictionary<string,GameObject[]> dic = new Dictionary<string, GameObject[]>();

	public NavMeshAgent rsagent;
	public GameObject goRSArrow; public Transform tranArrowParent; public Transform tranPool; int poolNum = -1; public GameObject[] goPool;

	public GameObject appia;

	public InputField ifsearch;

	public GameObject goCircle;

	public static GameObject goCurrentFound; public GameObject goUI_FingerPoint;

	float minDist = 0.75f; Vector3 lastPos = new Vector3(-9999f,-9999f,-9999f);

	void BuildFromLocalDB(){
		for (int i=0; i<ess.Length; i++) {
			EachSearchableObject e = ess[i];
			string[] tags = e.myTags;
			GameObject[] gs = e.myGameObjects;
			for(int j=0; j<tags.Length; j++){
				string tag = tags[j];
				if(dic.ContainsKey(tag)){
					GameObject[] existing = dic[tag];
					GameObject[] gtemp = new GameObject[gs.Length+existing.Length];  
					for(int dici=0;dici<existing.Length;dici++){ 
						gtemp[dici] = existing[dici];
					}
					for(int dici=existing.Length;dici<gtemp.Length;dici++){
						gtemp[dici] = gs[dici - existing.Length];
					}
					dic[tag] = gtemp;
				}else{
					dic[tag] = gs;
				}
			}
		}
	}

	public void SearchFromInputField(){
		string q = ifsearch.text;
		Search (q);
	}

	void Search(string tag){
		tag = tag.ToLower ();
		print ("Searching " + tag);
		SearchUIResult ("Searching for "+tag);
		if (dic.ContainsKey (tag)) {
			GameObject[] gs = dic [tag];
			for (int i=0; i<gs.Length; i++) {
				//TODO
				print ("Found: " + gs [i].name);
				AssignCurrentFound (gs [i]);
				if (wm == WhichMode.SceneSearch) {
					SearchUIResult("Found: "+tag);
					CreatePathFromCurrentTo (gs [i]);
					goCircle.transform.position = gs [i].transform.position + new Vector3 (0, 0.5f, 0);
				} else if (wm == WhichMode.SearchDBToCreate) {
					SearchUIResult ("Touch to place a "+tag);
					ShowFingerGlowPoint(Mathf2.GetScreenCenter());
				}
			}	
			goUI_CanvasSearch.SetActive (false);
		} else {
			SearchUIResult ("Cannot find "+tag);
			ShowFingerGlowPoint(new Vector2(-9999,-9999));
		}
	}

	static void AssignCurrentFound(GameObject g){
		goCurrentFound = g;
	}
	void SearchUIResult(string txt){
		goUI_SearchResult.text = txt;
	}

	void ShowFingerGlowPoint(Vector2 v){
		goUI_FingerPoint.transform.position = v;
	}

	void CreatePathFromCurrentTo(GameObject g){ 
		for (int i=0; i<tranArrowParent.childCount; i++) {

			tranArrowParent.GetChild (i).transform.position = Mathf2.FarFarAway;
			tranArrowParent.GetChild (i).transform.parent = tranPool;
			poolNum--;

		}

		Vector3 posGlobal = g.transform.position ;// transform.InverseTransformPoint (g.transform.position);// g.transform.position ;
	//	posGlobal = new Vector3 (posGlobal.x*10f,posGlobal.y * 10f, posGlobal.z * 12f); // transform.TransformPoint (g.transform.position);
		print (g.name + " " + posGlobal);
		//goRSArrow.transform.position = posGlobal;
		rsagent.transform.position = ARVRModes.goMe.transform.position;
		rsagent.enabled = true;
		rsagent.SetDestination (posGlobal);

		/*rsagent.CalculatePath (posGlobal, rsagent.path);
		Vector3[] corners = rsagent.path.corners; print (corners.Length);

		for(int i=1;i<corners.Length;i++){ print (g.name);
			Vector3 diff = corners[i] - corners[i-1];
			GameObject go = GameObject.Instantiate(goRSArrow,corners[i],Quaternion.Euler (diff)) as GameObject;
			go.transform.parent = tranArrowParent;
		}*/
	}

	public GameObject InstantiateFromPool(GameObject g,Vector3 pos,Quaternion rot){
		poolNum++;
		if (poolNum < (tranPool.childCount - 1)) {
			goPool[poolNum].transform.position = pos;
			//goPool[poolNum].transform.rotation = rot;
			return goPool[poolNum];
		} else {
			return GameObject.Instantiate(g,pos,rot) as GameObject;
		}
	}

	void CreatePool(int num){
		goPool = new GameObject[num];
		for (int i=0; i<num; i++) {
			GameObject g = GameObject.Instantiate(goRSArrow,Mathf2.FarFarAway,Quaternion.identity) as GameObject;
			g.transform.parent = tranPool;
			goPool[i] = g;
		}
	}

	void Awake () {
		BuildFromLocalDB ();
		//Search ("tree");

		CreatePool (200);
	}

	void Update () {
		if (Input.touchCount>=4) {
			if(Input2.HasPointStarted(4))
				goUI_CanvasSearch.SetActive(true);
		}

		if (rsagent.enabled) {
			Transform rsagentransform = rsagent.transform;
			float dist = Vector3.Distance ( lastPos , rsagentransform.position);
			lastPos = rsagentransform.position;
			if(dist>minDist){
				print (dist + " "+rsagentransform.position);
				Vector3 diff = rsagent.destination - rsagentransform.position;
				GameObject go = InstantiateFromPool(goRSArrow,rsagentransform.position,Quaternion.Euler (diff)) as GameObject;
				go.transform.right = -diff;
				go.transform.parent = tranArrowParent;
			}

			if (Vector3.Magnitude (rsagent.destination - rsagent.transform.position) < 2f) {
				rsagent.enabled = false;
			}
		}
	}
}

[System.Serializable]
public class EachSearchableObject{
	public GameObject[] myGameObjects;
	public string[] myTags;
	public string[] myColorTags;
	public Vector3[] mySearchCirclePos;
}

