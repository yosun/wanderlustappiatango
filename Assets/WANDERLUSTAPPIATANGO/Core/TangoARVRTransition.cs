﻿using UnityEngine;
using System.Collections;

// v0.11 (Appia)
// recognizes Vuforia AR as well as allows placement of image anywhere to "walk into it" 
public class TangoARVRTransition : MonoBehaviour {

	//public GameObject camARVuforia;

	public GameObject camARTango; Transform tARTango;
	public GameObject camVRTango; Transform tVRTango;

	public GameObject goPaintingEntire; Transform tPaintingEntire;
	public GameObject goPainting2DOnly; public GameObject goPainting3DOnly; 

	public GameObject goVRModeHide; public GameObject goARModeHide;

	public float minDistanecToWalkIn = 1f;

	void Awake(){
		tARTango = camARTango.transform; tVRTango = camVRTango.transform;
		tPaintingEntire = goPaintingEntire.transform;
	}

	void Start(){
		goPaintingEntire.transform.position = Mathf2.FarFarAway;
		SwitchVRToAR ();

		print (gameObject.name);
	}

	void Update(){
		if (camARTango.activeSelf) {
			if (Input2.HasPointStarted (1)) {
				Place2DPainting();
			}
			float distanceToPainting = Vector3.Distance ( tARTango.position , goPainting2DOnly.transform.position);

			if(distanceToPainting < minDistanecToWalkIn){
				WalkIntoPainting();
			}
		}
	}

	public void SwitchARToVR(){
		camARTango.SetActive (false);
		camVRTango.SetActive (true);
		goVRModeHide.SetActive (false);
		goARModeHide.SetActive (true);
		goPainting2DOnly.SetActive (false);
		goPainting3DOnly.SetActive (true);
	}

	public void SwitchVRToAR(){
		camVRTango.SetActive (false);
		camARTango.SetActive (true);
		goVRModeHide.SetActive (true);
		goARModeHide.SetActive (false);
		goPainting3DOnly.SetActive (false);
	}

	// 
	public void Place2DPainting(){
		print ("TANGO Placing 2D Painting");
		print ("TANGO v "+tARTango.name);
		print ("TANGO v  " + tARTango.forward);
		print ("TANGO v " + tARTango.position);
		tPaintingEntire.forward = -tARTango.forward;
		tPaintingEntire.position = tARTango.position + 2*tARTango.forward;
		goPainting2DOnly.SetActive (true);
		print ("TANGO v " + tPaintingEntire.position);
		print ("TANGO v " + tPaintingEntire.forward);
	}

	// triggered when we get close to the painting
	public void WalkIntoPainting(){
		SwitchARToVR ();
	}



}
