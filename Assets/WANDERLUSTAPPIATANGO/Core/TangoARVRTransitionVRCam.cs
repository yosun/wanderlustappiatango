﻿using UnityEngine;
using System.Collections; 
using UnityEngine.UI;
using UnityEngine.EventSystems;

// v0.11 (Appia) only has VR
// recognizes Vuforia AR as well as allows placement of image anywhere to "walk into it" 
public class TangoARVRTransitionVRCam : MonoBehaviour {
	
	//public GameObject camARVuforia;

	public GameObject goVideoTextures; Transform tVideoTextures;
	public GameObject camVRTango; Transform tVRTango;
	
	public GameObject goPaintingEntire; public static Transform tPaintingEntire;
	public GameObject goPainting2DOnly; public GameObject goPainting3DOnly; 

	public GameObject goVRModeHide; public GameObject goARModeHide;
	
	public float minDistanecToWalkIn = 0.5f;

	public Camera LeftCamera; public Camera RightCamera;
	public GameObject goUIStereo; public GameObject goUINonStereo;
	public GameObject goUIVRModeOnly;

	public GameObject goUIViewMode;

	public GameObject goPointsUpLocal;

	bool vrMode = false;

	Rect rectLeftCamStart;
	Rect rectRightCamStart; 

	void Awake(){ 
		tVRTango = camVRTango.transform;
		tPaintingEntire = goPaintingEntire.transform;
		tVideoTextures = goVideoTextures.transform;
	}

	void ChooseViewModes(){
		goUIViewMode.SetActive (true);
	}

	void Start(){
		//goPaintingEntire.transform.position = Mathf2.FarFarAway;
		SwitchVRToAR ();
		ChooseViewModes ();
		print (gameObject.name);
 
	} 
	public void EnableStereo(){ 
		StereoCameraManager.enableStereo = true;
		//LeftCamera.rect = rectLeftCamStart; // new Rect (8.0734908f, 0, 0.4265092f, 1);
		//RightCamera.rect = rectRightCamStart; // new Rect (0.5f,0,0.4265092f,1);
		goUINonStereo.SetActive (false);
		goUIStereo.SetActive (true);
		goUIViewMode.SetActive (false);
	}
	public void DisableStereo(){
		StereoCameraManager.enableStereo = false;
		//LeftCamera.rect = new Rect (0, 0, 0, 0);
		//RightCamera.rect = new Rect (0, 0, 1, 1);
		goUINonStereo.SetActive (true);
		goUIStereo.SetActive (false);
		goUIViewMode.SetActive(false);
	}

	void Update(){
		if (EventSystem.current.IsPointerOverGameObject ()) {
			return;
		}

		if (goVideoTextures.activeSelf||!vrMode) {
			if (Input2.HasPointStarted (1)) {
				Place2DPainting();
			}
			float distanceToPainting = Vector3.Distance ( tVRTango.position , goPainting2DOnly.transform.position );
			
			if(distanceToPainting < minDistanecToWalkIn){
				WalkIntoPainting();
			}
		}


	}

	void LateUpdate(){
		if (vrMode) {
			// set artificial gravity
//			print ("LateU");
			//camVRTango.transform.up = goPaintingEntire.transform.up;
			//camVRTango.transform.position -= goPaintingEntire.transform.up;// * 0.01f;
			//tVRTango.forward = -tPaintingEntire.forward;
			goPointsUpLocal.transform.up = tPaintingEntire.transform.up;
			//goPointsUpLocal.transform.forward = camVRTango.transform.forward;
			//goPointsUpLocal.transform.position = camVRTango.transform.position;
		}  
	}
	
	public void SwitchARToVR(){
	//	camVRTango.SetActive (true);
		goVRModeHide.SetActive (false);
		goARModeHide.SetActive (true);
		goPainting2DOnly.SetActive (false);
		goPainting3DOnly.SetActive (true);
		goVideoTextures.SetActive (false);
		goUIVRModeOnly.SetActive (true);
		//camVRTango.GetComponent<Rigidbody> ().isKinematic = true;
		vrMode = true;
	}
	
	public void SwitchVRToAR(){
		//camVRTango.SetActive (false);
		goVRModeHide.SetActive (true);
		goARModeHide.SetActive (false);
		goPainting3DOnly.SetActive (false);
		goVideoTextures.SetActive (true);
		goUIVRModeOnly.SetActive (false);
		//camVRTango.GetComponent<Rigidbody> ().isKinematic = true;
		vrMode = false;
	}
	
	// 
	public void Place2DPainting(){
		print ("TANGO Placing 2D Painting");
		//print ("TANGO v "+tARTango.name);
		//print ("TANGO v  " + tARTango.forward);
		//print ("TANGO v " + tARTango.position);
		tPaintingEntire.forward = -tVRTango.forward;
		tPaintingEntire.position = tVRTango.position + 1f * tVRTango.forward;
		goPainting2DOnly.SetActive (true);
		print ("TANGO v " + tPaintingEntire.position);
		print ("TANGO v " + tPaintingEntire.forward);
	}
	
	// triggered when we get close to the painting
	public void WalkIntoPainting(){
		SwitchARToVR ();
	}
	
	
	
}
