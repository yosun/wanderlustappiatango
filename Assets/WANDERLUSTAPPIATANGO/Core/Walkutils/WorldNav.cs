﻿using UnityEngine;
using System.Collections;

public class WorldNav : MonoBehaviour {

	static float coeff = 0.2f;// 0.1f;

	public GameObject mainCharacter; public static GameObject goMainCharacter;
	static Camera camCharacter;

	static bool walking = false; int dirwalking = 0;
	
	public GameObject goUI_Directions_TextWalkButton;

	void Awake(){
		//print (transform.name);
		goMainCharacter = mainCharacter;
		camCharacter = goMainCharacter.transform.Find ("CamParent").Find ("CameraVR").GetComponent<Camera>();

	}

	void Update(){
		if (walking)
			Walk (dirwalking);

		if (ARVRModes.tcm == ARVRModes.TheCurrentMode.AR)
			goUI_Directions_TextWalkButton.SetActive (true);
	}

	public static void Walk(int dir){
		Vector3 vc = camCharacter.transform.forward;
		Vector3 vf = coeff * new Vector3(vc.x,0,vc.z);
		if(dir > 0){
			goMainCharacter.transform.position += vf;
		}else{
			goMainCharacter.transform.position -= vf;
		}
		//Error.cout (dir);
	}

	public void WalkDirTrue(int dir){
		walking = true;
		dirwalking = dir;
		goUI_Directions_TextWalkButton.SetActive (false);
	}

	public void WalkDirFalse(int dir){
		walking = false;
		dirwalking = dir;
		goUI_Directions_TextWalkButton.SetActive (false);
	}

}
