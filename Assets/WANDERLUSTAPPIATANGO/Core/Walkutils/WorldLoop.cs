﻿using UnityEngine;
using System.Collections;

public class WorldLoop : MonoBehaviour {

	public Camera cam;
	Raycaster raycaster;

	void Awake(){
		raycaster = GetComponent<Raycaster> ();
	}

	// Use this for initialization
	void Start () {
	
	}

	void Update () {
// ui


// nav & interactivity

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
		if(Input.touchCount>0){
			if(Input.GetTouch(0).position.y>Screen.height * 0.5f)
				WorldNav.Walk (1);
			else if(Input.GetTouch(0).position.y<Screen.height * 0.5f)
				WorldNav.Walk (-1);

			if(Input.GetTouch(0).phase == TouchPhase.Began)
				raycaster.RaycastIt(cam,Input.GetTouch(0).position,100f);
		}
#elif UNITY_EDITOR
		if(Input.GetKey (KeyCode.UpArrow))
			WorldNav.Walk (1);
		else if(Input.GetKey (KeyCode.DownArrow))
			WorldNav.Walk (-1);

		if(Input.GetMouseButtonDown(0))
			raycaster.RaycastIt(cam,Input.mousePosition,100f);
#endif
	}

}
