﻿using UnityEngine;
using System.Collections;

public class FadeLoop : MonoBehaviour {

	float minAlpha = 0.2f;
	float lastTime = 0f;
	float incrementor = -0.01f;
	 
	void Update () {
		lastTime += Time.deltaTime;
		if(lastTime>0.5f){
			Color c = GetComponent<Renderer>().material.color;
			if(c.a < minAlpha || c.a >= 0.8f){
				incrementor*=-1f;
			}
			GetComponent<Renderer>().material.color = new Color(c.r,c.g,c.b,c.a+incrementor);
			lastTime = 0f;
		}
	}
}
