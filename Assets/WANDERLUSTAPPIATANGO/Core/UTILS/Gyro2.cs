﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

// v0.2 with swipe! and clamp for AR to VR cam
// v0.3 uses UsabilityPanel for globals
public class Gyro2 : MonoBehaviour {
	
	public static Transform transform;
	
	public static bool gyroEnabled = true;
	
	static Quaternion camBase;
	static Quaternion calibration;
	static Quaternion refRot;
	static Quaternion baseOrientation = Quaternion.identity;//Quaternion.Euler (90,0,0);
	
	public GameObject goUI_Directions_FiveFingers;
	public GameObject goUI_Directions_Swipe;
	
	public GameObject goUI_SettingsButtonToggle;

	static float countdown = 0f;
	
	void Awake(){
		transform = gameObject.transform;
	}
	
	void Start () {
		Init (gameObject.transform);
	}
	
	static Vector3 ClampPosInRoom(Vector3 p){
		// 7.4 29.8
		// -29.4 29.8
		// -29.4 3
		// 7.4 3
		p = new Vector3 (Mathf.Clamp (p.x,-29.4f,7.4f),Mathf.Clamp (p.y,0,10f),Mathf.Clamp (p.z,3,29.8f));
		return p;
	}
	
	public static void StationParent(Vector3 pos,Quaternion rot){
		//print (transform.parent.name);
		//print (transform.parent.parent.name);
		transform.position = ClampPosInRoom(pos) - new Vector3(0,3f,0);
		transform.rotation = rot;

	}
	
	public void StationParentShiftAngle(float angle){
		transform.parent.rotation *= Quaternion.Euler (0, angle, 0);
	}
	
	public static void Init(Transform t){
		transform = t;
		Input.gyro.enabled = true;
		
		ResetStatic ();
	}
	
	// realigns forward to current view defined by parent transform  
	public static void ResetStatic(){
		AssignCalibration ();
		AssignCamBaseRotation ();
		baseOrientation = transform.rotation * Quaternion.FromToRotation ( transform.forward, Vector3.forward);
		refRot = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
	}

	public static void ResetStaticSmoothly(){
		countdown = 3.14f;
		ResetStatic ();
	}
	
	public void Reset(){
		ResetStatic();
	}
	
	void DoSwipeRotation(float degree){
		//if (UsabilityPanel.enableDiscreteRotations) {
			StationParentShiftAngle (degree);
		/*//}/* else {
			if(degree<0)
				StationParentShiftAngle(-1f);
			else 
				StationParentShiftAngle(1f);
		//}*/
	}
	
	void DisableSettingsButotn(){
		goUI_SettingsButtonToggle.SetActive (false);
	} 
	
	void Update () {
		if (!Input2.IsPointerOverUI ()) {
			if (Input.touchCount >= 4) {//print ("Gt4");
				if (Input2.HasPointStarted (4)) {
					print ("Resetting...");
					ResetStatic ();
					goUI_Directions_FiveFingers.SetActive (false);
//					goUI_SettingsButtonToggle.SetActive (true);
//					Material mat = goUI_SettingsButtonToggle.GetComponent<Image> ().material;
//					mat.color = new Color (1, 1, 1, 1);
//					mat.DOFade (0, 3.14f).OnComplete (DisableSettingsButotn).SetEase (Ease.InExpo);
				}
			} else {
				Vector2 swipe = Vector2.zero; 
//				if (UsabilityPanel.enableDiscreteRotations) {
			//		swipe = Input2.Swipe ();
				//} else {
					swipe = Input2.ContinuousSwipe ();
			//	}
				if (Input2.SwipeLeft (swipe)) {
					gyroEnabled = false;
					DoSwipeRotation (-1f);
					ResetStatic ();
					gyroEnabled = true;
					goUI_Directions_Swipe.SetActive (false);
				} else if (Input2.SwipeRight (swipe)) {
					gyroEnabled = false;
					DoSwipeRotation (1f);
					ResetStatic ();
					gyroEnabled = true;
					goUI_Directions_Swipe.SetActive (false);
				}
			}
		}
		
		if (ARVRModes.TheCurrentMode.AR == ARVRModes.tcm) {
			goUI_Directions_FiveFingers.SetActive(true);
			goUI_Directions_Swipe.SetActive(true);
		}

		if(gyroEnabled){
			if(countdown>0f){
				countdown-=Time.deltaTime;
				transform.rotation = Quaternion.Slerp (transform.rotation, camBase * ConvertRotation (refRot * G ()), 0.1f);
			}else 
				transform.rotation = Quaternion.Slerp (transform.rotation, camBase * ConvertRotation (refRot * G ()), 0.9f);
		}
	}
	
	static void AssignCalibration(){
		calibration = G ();
	}
	
	static void AssignCamBaseRotation(){
		camBase = Quaternion.FromToRotation (Vector3.forward, transform.forward);
	}
	
	static Quaternion G(){
		return Input.gyro.attitude;
	}
	private static Quaternion ConvertRotation(Quaternion q)
	{
		return new Quaternion(q.x, q.y, -q.z, -q.w);	
	}
	
}