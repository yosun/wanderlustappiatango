﻿//v0.03 + continuous swipe and over UI check
using UnityEngine;
using System.Collections;

public class Input2 : MonoBehaviour {
	
	public static Vector2 offScreen = new Vector2(-999,-999);
	static float oldAngle = 0f;
	
	static Vector2 lastPos = Vector2.zero;
	
	public static bool useMouse = true; 
	
	static float swipeThreshhold= 50f;
	static float swipeThreshX = 50f; // minimum for detecting swipe
	static float swipeThreshY = 50f; // minimum for detecting swipe
	
	
	public static bool IsPointerOverUI(){
		#if UNITY_EDITOR
		return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ();
		#else
		int touches = Input.touchCount;
		int touchbool = 0;
		for(int i=0;i<touches;i++){
			if(UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (i))
				return true;
		}
		return false;
		#endif
	}
	
	public static bool TouchBegin(){
		if (Input.touchCount > 0)
			if (Input.GetTouch (0).phase == TouchPhase.Began)
				return true;
		return false;
	}
	
	public static Vector2 GetFirstPoint(){
		if(useMouse)
			return Input.mousePosition;
		else
			return Input.GetTouch(0).position;
		
		return offScreen;
	}
	
	public static Vector2 GetSecondPoint(){
		if(useMouse)
			return Input.mousePosition;
		else
			return Input.GetTouch(1).position;
		
		return offScreen;
	}
	
	public static int GetPointerCount(){
		if (useMouse) {
			if (Input.GetMouseButton (1))
				return 2;
			else if (Input.GetMouseButton (0))
				return 1;
		}else
			return Input.touchCount;
		
		return 0;
	}
	
	public static bool HasPointStarted(int num){
		int points = GetPointerCount ();
		if (points == 0)
			return false;
		if (num == points) {
			if(useMouse){
				if(points == 1)return (Input.GetMouseButtonDown(0));
				else if(points == 2)return (Input.GetMouseButtonDown(1));
			}else{
				if(points == 1)return (Input.GetTouch(0).phase == TouchPhase.Began);
				else if(points == 2)return (Input.GetTouch(1).phase == TouchPhase.Began);
				else if(points == 3)return (Input.GetTouch(2).phase == TouchPhase.Began);
				else if(points == 4)return (Input.GetTouch(3).phase == TouchPhase.Began);
			}
		}
		return false;
	}
	
	public static bool HasPointEnded(int num){
		int points = GetPointerCount ();
		if (points == 0)
			return false;
		if (num == points) {
			if(useMouse){
				if(points == 1)return (Input.GetMouseButtonUp(0));
				else if(points == 2)return (Input.GetMouseButtonUp(1));
			}else{
				if(points == 1)return (Input.GetTouch(0).phase == TouchPhase.Ended);
				else if(points == 2)return (Input.GetTouch(1).phase == TouchPhase.Ended);
			}
		}
		return false;
	}
	
	public static bool HasPointMoved(int num){
		int points = GetPointerCount ();
		if (points == 0)
			return false;
		if (num == points) {
			if(useMouse){
				if(points == 1)return true;
				else if(points == 2)return true;
			}else{
				if(points == 1)return (Input.GetTouch(0).phase == TouchPhase.Moved);
				else if(points == 2)return (Input.GetTouch(1).phase == TouchPhase.Moved);
			}
		}
		return false;
	}
	
	public static float Pinch(){
		// If there are two touches on the device...
		if (GetPointerCount () == 2)
		{
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			
			
			return deltaMagnitudeDiff;
		}
		return 0f;
	}
	
	public static float Twist(){
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);
		
		Vector2 v2 = touchZero.position - touchOne.position;
		float newAngle = Mathf.Atan2(v2.y, v2.x);
		float deltaAngle = Mathf.DeltaAngle(newAngle, oldAngle);
		oldAngle = newAngle;
		//print (newAngle);
		return newAngle;
	}
	
	public static int TwistInt(){
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);
		
		Vector2 v2 = touchZero.position - touchOne.position;
		float newAngle = Mathf.Atan2(v2.y, v2.x);
		float deltaAngle = Mathf.DeltaAngle(newAngle, oldAngle);
		
		
		int diff = (int)Mathf.Sign((newAngle - oldAngle));
		//oldAngle = newAngle;
		//print (diff + " "+oldAngle + " "+newAngle);
		oldAngle = newAngle;
		return -diff;
	}
	
	//TODO drag 
	//public static 
	
	public static bool SwipeLeft(Vector2 swipedir){
		if (swipedir.x < 0 && Mathf.Abs (swipedir.x)>swipeThreshX)
			return true;
		return false;
	}
	public static bool SwipeRight(Vector2 swipedir){
		if (swipedir.x > 0 && Mathf.Abs (swipedir.x)>swipeThreshX)
			return true;
		return false;
	}
	public static bool SwipeDown(Vector2 swipedir){
		if (swipedir.y < 0 && Mathf.Abs (swipedir.y)>swipeThreshY)
			return true;
		return false;
	}
	public static bool SwipeUp(Vector2 swipedir){
		if (swipedir.y > 0 && Mathf.Abs (swipedir.y)>swipeThreshY)
			return true;
		return false;
	}
	
	public static Vector2 ContinuousSwipe(){ 
		if(GetPointerCount () == 1){
			Vector2 v = GetFirstPoint();  
			if(!HasPointStarted(1)){
				Vector2 d = v - lastPos;
				//				print (v + " " + lastPos);
				if(d.magnitude > 0){
					if(Mathf.Abs (d.x) > Mathf.Abs(d.y)){
						// swipe horiz
						return new Vector2(d.x,0) ;
					}else{
						// swipe vert
						return new Vector2(0,d.y) ;
					}
				}
			}else
				lastPos = v;
		}
		return Vector2.zero;
	}
	
	public static Vector2 Swipe(){ 
		if(GetPointerCount () == 1){
			Vector2 v = GetFirstPoint();  
			if(HasPointEnded(1)){
				Vector2 d = v - lastPos;
				//				print (v + " " + lastPos);
				if(d.magnitude > swipeThreshhold){
					if(Mathf.Abs (d.x) > Mathf.Abs(d.y)){
						// swipe horiz
						return new Vector2(d.x,0) ;
					}else{
						// swipe vert
						return new Vector2(0,d.y) ;
					}
				}
			}
			lastPos = v;
		}
		return Vector2.zero;
	}
	
}
