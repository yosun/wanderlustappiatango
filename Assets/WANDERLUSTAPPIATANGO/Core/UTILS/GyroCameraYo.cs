﻿using UnityEngine;
using System.Collections;

// rotates only single axis ie euler-y
public class GyroCameraYo : MonoBehaviour {
	
	[SerializeField] private Transform parentTransform;
	new private Transform transform;
	
	Quaternion rotFix = new Quaternion (0, 0, 1, 0);
	
	Vector3 rotateAround; 
	
	void Start(){
		transform = GetComponent<Transform>();
		 
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		Input.gyro.enabled = true;
		Input.gyro.updateInterval = 0.01f;
		
		parentTransform.transform.eulerAngles = new Vector3 (90, 200, 0);
		//rotFix = Quaternion.Euler(90,90,90);
		 
	}
	
	void Update () {
		//#if !UNITY_EDITOR
		GyroCam();
	}
	
	private static Quaternion ConvertRotation(Quaternion q)
	{
		return new Quaternion(q.x, q.y, -q.z, -q.w) ;
	}
	
	void GyroCam(){
		Quaternion targetRotation = ConvertRotation(Quaternion.Euler(Input.gyro.attitude.eulerAngles)) ;//* rotFix; //Sensor.rotationQuaternion;
		Vector3 euler = targetRotation.eulerAngles;
		
		
		transform.Rotate(0, -Input.gyro.rotationRate.y , 0);
		Quaternion currentRotation = transform.localRotation;
		Quaternion quat = Quaternion.Slerp(currentRotation, targetRotation, Mathf.Clamp01(5f*Time.deltaTime));;
		Quaternion value = quat; // Quaternion.Euler (0,quat.eulerAngles.y,0);
		transform.localRotation = value;// Quaternion.Euler ( value.eulerAngles + new Vector3(0,90,0));//* Quaternion.Euler(0,90f,0);
	}
	
	
}