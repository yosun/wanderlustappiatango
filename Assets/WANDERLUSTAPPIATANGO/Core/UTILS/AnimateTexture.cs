﻿using UnityEngine;
using System.Collections;

public class AnimateTexture : MonoBehaviour {

	public Vector2 uvOffset = Vector2.zero;
	public Vector2 uvAnimationRate = new Vector2(0.1f,0.1f);

	Renderer renderer;

	void Awake(){ 
		renderer = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		uvOffset += ( uvAnimationRate * Time.deltaTime );
		if( renderer.enabled )
		{
			renderer.material.SetTextureOffset( "_MainTex", uvOffset );
		}
	}
}
