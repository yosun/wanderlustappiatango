﻿/*
	SetRenderQueue.cs
 
	Sets the RenderQueue of an object's materials on Awake. This will instance
	the materials, so the script won't interfere with other renderers that
	reference the same materials.
*/

using UnityEngine;

public class SetRenderQueueChildren : MonoBehaviour {
	
	[SerializeField]
	protected int[] m_queues = new int[]{3000};

	void Start(){
		print(transform.name);
	}

	protected void Awake() {
		/*Material[] materials = GetComponentsInChildren<Material>(true);
		print (materials);
		for (int i = 0; i < materials.Length ; ++i) {
			materials[i].renderQueue = 3020;
		}*/
		Renderer[] renderers = GetComponentsInChildren<Renderer>(true);	
		for(int i=0;i<renderers.Length;i++){
			Material[] materials = renderers[i].materials;
			for(int j=0;j<materials.Length;j++){
				materials[j].renderQueue = 3020;
			}
		}
	}
}