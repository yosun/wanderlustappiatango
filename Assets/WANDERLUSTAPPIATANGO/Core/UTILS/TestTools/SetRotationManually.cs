﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetRotationManually : MonoBehaviour {

	public Slider sliderX;
	public Slider sliderY;
	public Slider sliderZ;

	public Text rotationvalue;

	public void UpdateRotationFromSliderX(){
		Vector3 prev = transform.rotation.eulerAngles;
		transform.rotation = Quaternion.Euler (sliderX.value, prev.y, prev.z);
	}

	public void UpdateRotationFromSliderY(){
		Vector3 prev = transform.rotation.eulerAngles;
		transform.rotation = Quaternion.Euler (prev.x,sliderY.value,   prev.z);		
	}


	public void UpdateRotationFromSliderZ(){
		Vector3 prev = transform.rotation.eulerAngles;
		transform.rotation = Quaternion.Euler (prev.x,prev.y,sliderZ.value);		
	}

	void Update(){
		rotationvalue.text = transform.rotation.eulerAngles.ToString ();
	}
	
}
