﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2353;
// System.MarshalByRefObject
struct MarshalByRefObject_t1885;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t1885 * ClientActivatedIdentity_GetServerObject_m12352 (ClientActivatedIdentity_t2353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
