﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Marker>
struct List_1_t813;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t739;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t766;
// System.Collections.Generic.IEnumerator`1<Vuforia.Marker>
struct IEnumerator_1_t4169;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.Marker>
struct ICollection_1_t4164;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Marker>
struct ReadOnlyCollection_1_t3436;
// Vuforia.Marker[]
struct MarkerU5BU5D_t3430;
// System.Predicate`1<Vuforia.Marker>
struct Predicate_1_t3437;
// System.Comparison`1<Vuforia.Marker>
struct Comparison_1_t3438;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m19675(__this, method) (( void (*) (List_1_t813 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4395(__this, ___collection, method) (( void (*) (List_1_t813 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor(System.Int32)
#define List_1__ctor_m19676(__this, ___capacity, method) (( void (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.cctor()
#define List_1__cctor_m19677(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19678(__this, method) (( Object_t* (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19679(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t813 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19680(__this, method) (( Object_t * (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19681(__this, ___item, method) (( int32_t (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19682(__this, ___item, method) (( bool (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19683(__this, ___item, method) (( int32_t (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19684(__this, ___index, ___item, method) (( void (*) (List_1_t813 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19685(__this, ___item, method) (( void (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19686(__this, method) (( bool (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19687(__this, method) (( bool (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19688(__this, method) (( Object_t * (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19689(__this, method) (( bool (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19690(__this, method) (( bool (*) (List_1_t813 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19691(__this, ___index, method) (( Object_t * (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19692(__this, ___index, ___value, method) (( void (*) (List_1_t813 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Add(T)
#define List_1_Add_m19693(__this, ___item, method) (( void (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19694(__this, ___newCount, method) (( void (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19695(__this, ___collection, method) (( void (*) (List_1_t813 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19696(__this, ___enumerable, method) (( void (*) (List_1_t813 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19697(__this, ___collection, method) (( void (*) (List_1_t813 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Marker>::AsReadOnly()
#define List_1_AsReadOnly_m19698(__this, method) (( ReadOnlyCollection_1_t3436 * (*) (List_1_t813 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Clear()
#define List_1_Clear_m19699(__this, method) (( void (*) (List_1_t813 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::Contains(T)
#define List_1_Contains_m19700(__this, ___item, method) (( bool (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19701(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t813 *, MarkerU5BU5D_t3430*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Marker>::Find(System.Predicate`1<T>)
#define List_1_Find_m19702(__this, ___match, method) (( Object_t * (*) (List_1_t813 *, Predicate_1_t3437 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19703(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3437 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19704(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t813 *, int32_t, int32_t, Predicate_1_t3437 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Marker>::GetEnumerator()
#define List_1_GetEnumerator_m4396(__this, method) (( Enumerator_t814  (*) (List_1_t813 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::IndexOf(T)
#define List_1_IndexOf_m19705(__this, ___item, method) (( int32_t (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19706(__this, ___start, ___delta, method) (( void (*) (List_1_t813 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19707(__this, ___index, method) (( void (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Insert(System.Int32,T)
#define List_1_Insert_m19708(__this, ___index, ___item, method) (( void (*) (List_1_t813 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19709(__this, ___collection, method) (( void (*) (List_1_t813 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::Remove(T)
#define List_1_Remove_m19710(__this, ___item, method) (( bool (*) (List_1_t813 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19711(__this, ___match, method) (( int32_t (*) (List_1_t813 *, Predicate_1_t3437 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19712(__this, ___index, method) (( void (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Reverse()
#define List_1_Reverse_m19713(__this, method) (( void (*) (List_1_t813 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Sort()
#define List_1_Sort_m19714(__this, method) (( void (*) (List_1_t813 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19715(__this, ___comparison, method) (( void (*) (List_1_t813 *, Comparison_1_t3438 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Marker>::ToArray()
#define List_1_ToArray_m19716(__this, method) (( MarkerU5BU5D_t3430* (*) (List_1_t813 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::TrimExcess()
#define List_1_TrimExcess_m19717(__this, method) (( void (*) (List_1_t813 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::get_Capacity()
#define List_1_get_Capacity_m19718(__this, method) (( int32_t (*) (List_1_t813 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19719(__this, ___value, method) (( void (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::get_Count()
#define List_1_get_Count_m19720(__this, method) (( int32_t (*) (List_1_t813 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Marker>::get_Item(System.Int32)
#define List_1_get_Item_m19721(__this, ___index, method) (( Object_t * (*) (List_1_t813 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::set_Item(System.Int32,T)
#define List_1_set_Item_m19722(__this, ___index, ___value, method) (( void (*) (List_1_t813 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
