﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t268;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.FontUpdateTracker
struct  FontUpdateTracker_t269  : public Object_t
{
};
struct FontUpdateTracker_t269_StaticFields{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>> UnityEngine.UI.FontUpdateTracker::m_Tracked
	Dictionary_2_t268 * ___m_Tracked_0;
};
