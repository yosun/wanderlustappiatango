﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1269);
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 16;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1133_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m5647(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1133_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m5648(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m5652(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m5653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m5654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_LayerToName_m5657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m5658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_t95_LayerMask_GetMask_m5659_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void RuntimePlatform_t1140_CustomAttributesCacheGenerator_NaCl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("NaCl export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void RuntimePlatform_t1140_CustomAttributesCacheGenerator_FlashPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("FlashPlayer export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerX86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX86 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerX64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX64 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerARM(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerARM instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void SystemInfo_t1142_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m5660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Coroutine_t316_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m5663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m5665(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_t955_ScriptableObject_Internal_CreateScriptableObject_m5665_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m5666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m5668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m5673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m5674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m5675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m5676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m5677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m5678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m5679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m5680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m5681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m5682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m5683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m5684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m5685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m5686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m5687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m5688(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m5737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m5738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m5739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m5740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_get_mesh_m4283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_set_mesh_m4596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_get_sharedMesh_m486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_set_sharedMesh_m4337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_Internal_Create_m5741(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_t153_Mesh_Internal_Create_m5741_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_Clear_m5742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_t153_Mesh_Clear_m5742_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_Clear_m4284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_vertices_m487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_vertices_m4285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_normals_m4289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_uv_m4288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_uv_m4287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_INTERNAL_get_bounds_m5743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_RecalculateNormals_m4290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_triangles_m488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_triangles_m4286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_enabled_m268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_enabled_m505(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_material_m269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_material_m4329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_sharedMaterial_m451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_materials_m340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_sharedMaterials_m452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m2046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m2047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_width_m316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_height_m317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_dpi_m2381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToPortrait_m4411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToPortrait_m4415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToPortraitUpsideDown_m4412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToPortraitUpsideDown_m4416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeLeft_m4409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeLeft_m4413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeRight_m4410(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeRight_m4414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_get_orientation_m421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_orientation_m4636(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Screen_t1157_CustomAttributesCacheGenerator_Screen_set_sleepTimeout_m291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Vertex_m5764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_Begin_m493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_End_m495(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_MultMatrix_m5765(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_PushMatrix_m489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_PopMatrix_m496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_SetRevertBackfacing_m4663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use invertCulling property"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_Clear_m4351(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_t1158_GL_Clear_m5766_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("1.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m5768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GL_t1158_CustomAttributesCacheGenerator_GL_IssuePluginEvent_m4430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture_t321_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m5770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture_t321_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m5771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture_t321_CustomAttributesCacheGenerator_Texture_set_filterMode_m4666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture_t321_CustomAttributesCacheGenerator_Texture_set_wrapMode_m4667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture_t321_CustomAttributesCacheGenerator_Texture_GetNativeTextureID_m4668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m5774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Internal_Create_m5774_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_get_format_m4365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m2093(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m2177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels_m4370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels_m5775_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels_m5776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels_m5776_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetAllPixels32_m5777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels32_m4608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels32_m5778_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels_m4367(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels_m5779_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels_m5780(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels_m5780_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m5781(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels32_m5781_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m4606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Apply_m5782(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Apply_m5782_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Apply_m5782_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Apply_m4609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Resize_m4366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_ReadPixels_m4605_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_ReadPixels_m5783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m5784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureFormat.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureReadWrite.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m4597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m4607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m5785(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m5786(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_set_depth_m4600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_set_active_m4604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUILayer_t1161_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m5790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gradient_t1164_CustomAttributesCacheGenerator_Gradient_Init_m5794(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gradient_t1164_CustomAttributesCacheGenerator_Gradient_Cleanup_m5795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m5802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_GUI_set_changed_m5805(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m5808(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m5810(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoWindow_m5813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1173_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m5825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1173_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m5858(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m5859(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m5861(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m5863(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m5867(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISettings_t1181_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISettings_t1181_CustomAttributesCacheGenerator_m_TripleClickSelectsLine(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISettings_t1181_CustomAttributesCacheGenerator_m_CursorColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISettings_t1181_CustomAttributesCacheGenerator_m_CursorFlashSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISettings_t1181_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t500_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t500 * tmp;
		tmp = (ExecuteInEditMode_t500 *)il2cpp_codegen_object_new (ExecuteInEditMode_t500_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2493(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_box(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_button(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_toggle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_label(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_textField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_textArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_window(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_ScrollView(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_CustomStyles(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUISkin_t1166_CustomAttributesCacheGenerator_m_Settings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUIContent_t456_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUIContent_t456_CustomAttributesCacheGenerator_m_Image(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GUIContent_t456_CustomAttributesCacheGenerator_m_Tooltip(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_Init_m5935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m5936(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m5937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m5938(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_Init_m5942(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_Cleanup_m5943(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_left_m2398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_left_m5526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_right_m5523(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_right_m5527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_top_m2399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_top_m5528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_bottom_m5524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_bottom_m5529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m2391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_vertical_m2393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m5945(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Init_m5950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m5951(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_name_m5952(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_name_m5953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m5955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m5958(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m5959(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m5960(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m5961(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m5962(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m5963(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m5964(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m5965(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m5967(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5971(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m5973(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m5975(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m5978(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5980(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("TouchScreenKeyboardType.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m2205(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m2206(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m2280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m2204(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m2279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m2221(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m2218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_Init_m5982(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_Cleanup_m5984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_rawType_m2235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_type_m5985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m5987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_modifiers_m2231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_character_m2233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_commandName_m5988(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_get_keyCode_m2232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m5990(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Event_t317_CustomAttributesCacheGenerator_Event_PopEvent_m2236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void EventModifiers_t1190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m5997(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireCube_m5998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawCube_m5999(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m6000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_matrix_m6001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Vector2_t10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Vector3_t15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Color_t90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern TypeInfo* IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo_var;
void Color32_t421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2313);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		IL2CPPStructAlignmentAttribute_t1295 * tmp;
		tmp = (IL2CPPStructAlignmentAttribute_t1295 *)il2cpp_codegen_object_new (IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo_var);
		IL2CPPStructAlignmentAttribute__ctor_m6725(tmp, NULL);
		tmp->___Align_0 = 4;
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m6019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_t13_Quaternion_LookRotation_m5543_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m6020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m6029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6043(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6086(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Vector4_t413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_Max_m4571_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Mathf_t104_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_SmoothDamp_m2306_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_SmoothDamp_m2306_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void DrivenTransformProperties_t1193_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6115(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6116(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6117(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6118(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6123(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6124(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6125(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void Resources_t1197_CustomAttributesCacheGenerator_Resources_Load_m6131(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Resources_t1197_CustomAttributesCacheGenerator_Resources_UnloadUnusedAssets_m4546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void SerializePrivateVariables_t1198_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Shader_t152_CustomAttributesCacheGenerator_Shader_Find_m4594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Shader_t152_CustomAttributesCacheGenerator_Shader_PropertyToID_m6133(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material__ctor_m481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Creating materials from shader source string will be removed in the future. Use Shader assets instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_get_shader_m483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6136(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetTexture_m6138(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_GetTexture_m6140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetTextureOffset_m6141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetFloat_m6143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_HasProperty_m6144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetPass_m492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_set_renderQueue_m341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithString_m6145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithString_m6145_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithShader_m6146_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithMaterial_m6147_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6150(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6153(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6156(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_rect_m2152(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m2148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_texture_m2145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_textureRect_m2174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_border_m2146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m2163(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m2162(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetPadding_m2151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6166(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_InitWWW_m6171(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_get_bytes_m6177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_get_error_m6178(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WWW_t1201_CustomAttributesCacheGenerator_WWW_get_isDone_m6179(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void WWWForm_t1203_CustomAttributesCacheGenerator_WWWForm_AddField_m6183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void WWWForm_t1203_CustomAttributesCacheGenerator_WWWForm_t1203_WWWForm_AddField_m6184_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void WWWTranscoder_t1204_CustomAttributesCacheGenerator_WWWTranscoder_t1204_WWWTranscoder_QPEncode_m6191_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void WWWTranscoder_t1204_CustomAttributesCacheGenerator_WWWTranscoder_t1204_WWWTranscoder_SevenBitClean_m6194_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void CacheIndex_t1205_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("this API is not for public use."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void UnityString_t1206_CustomAttributesCacheGenerator_UnityString_t1206_UnityString_Format_m6196_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AsyncOperation_t1134_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_Quit_m402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_get_isPlaying_m2284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_get_isEditor_m2287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_get_platform_m459(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_set_runInBackground_m4676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_get_dataPath_m4610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Application_t1208_CustomAttributesCacheGenerator_Application_get_unityVersion_m4431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_get_enabled_m497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_set_enabled_m240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m2038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_nearClipPlane_m4589(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m2037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_farClipPlane_m4590(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4587(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographic_m4586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_depth_m1949(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_aspect_m4588(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_cullingMask_m4591(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_eventMask_m6210(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_rect_m6211(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6213(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_targetTexture_m4598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m6214(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_projectionMatrix_m6215(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6216(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6217(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6219(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_main_m252(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_current_m485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6221(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_Render_m4601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var;
void CameraCallback_t1209_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2314);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1442 * tmp;
		tmp = (EditorBrowsableAttribute_t1442 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7049(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6230(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_DrawRay_m257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_Internal_Log_m6232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_Internal_Log_m6232_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_Internal_LogException_m6233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_Internal_LogException_m6233_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m6259(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m6260(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m6261(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m6262(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m6263(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_SetParamsImpl_m6264(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m6265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Display_t1213_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m6266(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m6267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m6270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_rotationRate_Internal_m6272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_attitude_Internal_m6273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_setEnabled_Internal_m6274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_setUpdateInterval_Internal_m6275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_mainGyroIndex_Internal_m6277(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetKeyUpInt_m6278(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetAxis_m376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetAxisRaw_m2028(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetButtonDown_m2027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButton_m306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_get_mousePosition_m253(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m2001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_get_mousePresent_m2026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_GetTouch_m302(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_get_touchCount_m286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m2283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_get_compositionString_m2208(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Input_t102_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m6279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void HideFlags_t1216_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m6281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_Destroy_m6282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_Destroy_m6282_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_Destroy_m471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_DestroyImmediate_m6283_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m4425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_get_name_m259(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_set_name_m2352(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m4299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_set_hideFlags_m482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_DestroyObject_m6284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_DestroyObject_m6284_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_DestroyObject_m387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_ToString_m539(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_Instantiate_m4491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void Object_t123_CustomAttributesCacheGenerator_Object_FindObjectOfType_m396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_get_transform_m243(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_get_gameObject_m272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_GetComponent_m2406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m6290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_GetComponent_m7051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2315);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1443 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1443 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7052(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m6291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Component_t103_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m6292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject__ctor_m4592_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_CreatePrimitive_m4327(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m6293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m6294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m7059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2315);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1443 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1443 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7052(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m6295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m6296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_transform_m273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_layer_m2254(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_set_layer_m2255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActive_m238(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m1986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActiveRecursively_m239(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("gameObject.SetActiveRecursively() is obsolete. Use GameObject.SetActive(), which is now inherited by children."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SendMessage_m6297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6297_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6297_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m6298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_AddComponent_m6299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2310);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1352 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1352 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6898(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m6300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_Internal_CreateGameObject_m6300_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m6304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m6305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m6306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m6307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m6308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m6309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m6310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m6311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m6312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m6313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_parentInternal_m6314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_set_parentInternal_m6315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_SetParent_m6316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m6317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localToWorldMatrix_m6318(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6319_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_Rotate_m296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6320_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_LookAt_m379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6321_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6322_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m6323(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m6324(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m6325(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_root_m4615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_childCount_m2407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m2253(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_Find_m348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_lossyScale_m6326(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_GetChild_m2405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Time_t1218_CustomAttributesCacheGenerator_Time_get_deltaTime_m265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Time_t1218_CustomAttributesCacheGenerator_Time_get_unscaledTime_m2030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Time_t1218_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m2060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Time_t1218_CustomAttributesCacheGenerator_Time_get_timeScale_m5511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Time_t1218_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m5510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Random_t1219_CustomAttributesCacheGenerator_Random_RandomRangeInt_m6328(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_t1220_PlayerPrefs_GetString_m6330_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m6349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m6350_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m6350_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_Raycast_m255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m321_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m321_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m2050_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m2050_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m6351_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m6351_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Physics_t1222_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m6352(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Collider_t157_CustomAttributesCacheGenerator_Collider_set_enabled_m506(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Collider_t157_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m6353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void MeshCollider_t590_CustomAttributesCacheGenerator_MeshCollider_set_sharedMesh_m4338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m6357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_Raycast_m2127(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1349_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2307);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1349 * tmp;
		tmp = (DefaultValueAttribute_t1349 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1349_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6893(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m2041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2312);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1350 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1350 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6897(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m6359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Collider2D_t433_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m6361(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_Internal_CreateWebCamTexture_m6378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1301_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_t682_WebCamTexture_Internal_CreateWebCamTexture_m6378_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2311);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1301 * tmp;
		tmp = (WritableAttribute_t1301 *)il2cpp_codegen_object_new (WritableAttribute_t1301_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6733(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Play_m6379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Stop_m6380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_isPlaying_m4446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_deviceName_m4448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedFPS_m4449(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedWidth_m4450(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedHeight_m4451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_devices_m4602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_didUpdateThisFrame_m4445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void AnimationEvent_t1233_CustomAttributesCacheGenerator_AnimationEvent_t1233____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void AnimationCurve_t1237_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_t1237_AnimationCurve__ctor_m6404_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m6406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_Init_m6408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void AnimatorStateInfo_t1234_CustomAttributesCacheGenerator_AnimatorStateInfo_t1234____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Animator_t415_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m2343(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Animator_t415_CustomAttributesCacheGenerator_Animator_StringToHash_m6427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Animator_t415_CustomAttributesCacheGenerator_Animator_SetTriggerString_m6428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Animator_t415_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m6429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void CharacterInfo_t1243_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void CharacterInfo_t1243_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void CharacterInfo_t1243_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void CharacterInfo_t1243_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Font_t266_CustomAttributesCacheGenerator_Font_get_material_m2356(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Font_t266_CustomAttributesCacheGenerator_Font_HasCharacter_m2234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Font_t266_CustomAttributesCacheGenerator_Font_get_dynamic_m2359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Font_t266_CustomAttributesCacheGenerator_Font_get_fontSize_m2361(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var;
void FontTextureRebuildCallback_t1244_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2314);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1442 * tmp;
		tmp = (EditorBrowsableAttribute_t1442 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7049(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_Init_m6457(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m6458(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m2250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m6462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m6463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m6464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m6465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m6466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m6467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m2227(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m6468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m6469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m2270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_renderMode_m2123(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m2378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m2131(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m2360(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m2382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m2149(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m2383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m2108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m2125(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m2124(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m2130(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m2090(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void Canvas_t274_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m2355(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m2334(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m6481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m2107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m6484(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m2111(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m2417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m2101(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m6485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m6486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m2094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m2091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1294_il2cpp_TypeInfo_var;
void RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m2110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2309);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1294 * tmp;
		tmp = (WrapperlessIcall_t1294 *)il2cpp_codegen_object_new (WrapperlessIcall_t1294_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6724(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_Request_get_sourceId_m6495(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_Request_get_appId_m6496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Request_t1248_CustomAttributesCacheGenerator_Request_get_domain_m6497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_Response_get_success_m6506(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_Response_set_success_m6507(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_Response_get_extendedInfo_m6508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Response_t1250_CustomAttributesCacheGenerator_Response_set_extendedInfo_m6509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m6514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m6515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m6516(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m6517(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m6518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m6519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m6520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m6521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m6522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m6525(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m6526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m6527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m6528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m6529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m6530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m6531(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m6532(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m6533(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m6534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m6535(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m6536(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m6540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m6541(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m6542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m6543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m6546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m6547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m6548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m6549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m6550(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m6551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m6552(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m6553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m6554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m6555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m6556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m6557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m6561(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m6562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m6565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m6566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m6567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m6568(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m6571(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m6572(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m6573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m6574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m6575(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m6576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m6577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m6578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m6581(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m6582(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m6583(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m6584(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m6585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m6586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m6590(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m6591(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_name_m6592(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_name_m6593(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m6594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m6595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m6596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m6597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m6598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m6599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m6600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m6601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m6602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m6603(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m6604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchResponse_t1264_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchResponse_t1264_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m6608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ListMatchResponse_t1264_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m6609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
extern TypeInfo* DefaultValueAttribute_t1445_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t1265_il2cpp_TypeInfo_var;
void AppID_t1265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1445_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2316);
		AppID_t1265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2203);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1445 * tmp;
		tmp = (DefaultValueAttribute_t1445 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1445_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7070(tmp, Box(AppID_t1265_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
extern TypeInfo* DefaultValueAttribute_t1445_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t1266_il2cpp_TypeInfo_var;
void SourceID_t1266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1445_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2316);
		SourceID_t1266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2202);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1445 * tmp;
		tmp = (DefaultValueAttribute_t1445 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1445_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7070(tmp, Box(SourceID_t1266_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
extern TypeInfo* DefaultValueAttribute_t1445_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t1267_il2cpp_TypeInfo_var;
void NetworkID_t1267_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1445_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2316);
		NetworkID_t1267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2206);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1445 * tmp;
		tmp = (DefaultValueAttribute_t1445 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1445_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7070(tmp, Box(NetworkID_t1267_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
extern TypeInfo* DefaultValueAttribute_t1445_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t1268_il2cpp_TypeInfo_var;
void NodeID_t1268_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1445_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2316);
		NodeID_t1268_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2207);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint16_t _tmp_0 = 0;
		DefaultValueAttribute_t1445 * tmp;
		tmp = (DefaultValueAttribute_t1445 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1445_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7070(tmp, Box(NodeID_t1268_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void NetworkMatch_t1274_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m7071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m7080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var;
void JsonArray_t1275_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2314);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t1442 * tmp;
		tmp = (EditorBrowsableAttribute_t1442 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7049(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var;
void JsonObject_t1277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2314);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t1442 * tmp;
		tmp = (EditorBrowsableAttribute_t1442 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7049(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
void SimpleJson_t1280_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
extern TypeInfo* SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var;
void SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m6653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2318);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1449 * tmp;
		tmp = (SuppressMessageAttribute_t1449 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7082(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7083(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var;
void SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_NextToken_m6665(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2318);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1449 * tmp;
		tmp = (SuppressMessageAttribute_t1449 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7082(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Maintainability"), il2cpp_codegen_string_new_wrapper("CA1502:AvoidExcessiveComplexity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var;
void SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_t1280____PocoJsonSerializerStrategy_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2314);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1442 * tmp;
		tmp = (EditorBrowsableAttribute_t1442 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1442_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7049(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t1278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t1278_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m7084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2318);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1449 * tmp;
		tmp = (SuppressMessageAttribute_t1449 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7082(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7083(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2318);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1449 * tmp;
		tmp = (SuppressMessageAttribute_t1449 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7082(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7083(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2318);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1449 * tmp;
		tmp = (SuppressMessageAttribute_t1449 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1449_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7082(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7083(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var;
void ReflectionUtils_t1293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2317);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1448 * tmp;
		tmp = (GeneratedCodeAttribute_t1448 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1448_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7081(tmp, il2cpp_codegen_string_new_wrapper("reflection-utils"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetConstructorInfo_m6708_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetContructor_m6713_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6715_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void ThreadSafeDictionary_2_t1450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void ConstructorDelegate_t1286_CustomAttributesCacheGenerator_ConstructorDelegate_t1286_ConstructorDelegate_Invoke_m6693_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void ConstructorDelegate_t1286_CustomAttributesCacheGenerator_ConstructorDelegate_t1286_ConstructorDelegate_BeginInvoke_m6694_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void IL2CPPStructAlignmentAttribute_t1295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 8, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DisallowMultipleComponent_t501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void RequireComponent_t163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void WritableAttribute_t1301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2048, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void AssemblyIsEditorAssembly_t1302_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_Achievement_get_id_m6766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_Achievement_set_id_m6767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Achievement_t1317_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void AchievementDescription_t1318_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void AchievementDescription_t1318_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void AchievementDescription_t1318_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6786(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_Score_get_value_m6788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Score_t1319_CustomAttributesCacheGenerator_Score_set_value_m6789(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_id_m6797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_id_m6798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_range_m6801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_range_m6802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void PropertyAttribute_t1329_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void TooltipAttribute_t505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void SpaceAttribute_t503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void RangeAttribute_t499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void TextAreaAttribute_t506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void SelectionBaseAttribute_t504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var;
void StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2315);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1443 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1443 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7052(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var;
void StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2315);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1443 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1443 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7052(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var;
void StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2315);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1443 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1443 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1443_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7052(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void SharedBetweenAnimatorsAttribute_t1332_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ArgumentCache_t1338_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void PersistentCall_t1342_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void PersistentCall_t1342_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void PersistentCall_t1342_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void PersistentCall_t1342_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void PersistentCall_t1342_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void PersistentCallGroup_t1344_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var;
void UnityEventBase_t1347_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(489);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t492 * tmp;
		tmp = (FormerlySerializedAsAttribute_t492 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void UnityEventBase_t1347_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t491_il2cpp_TypeInfo_var;
void UserAuthorizationDialog_t1348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(488);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t491 * tmp;
		tmp = (AddComponentMenu_t491 *)il2cpp_codegen_object_new (AddComponentMenu_t491_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t1349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 18432, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ExcludeFromDocsAttribute_t1350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void FormerlySerializedAsAttribute_t492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void TypeInferenceRuleAttribute_t1352_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_Assembly_AttributeGenerators[826] = 
{
	NULL,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	AssetBundleCreateRequest_t1133_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m5647,
	AssetBundleCreateRequest_t1133_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m5648,
	AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m5652,
	AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m5653,
	AssetBundle_t1135_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m5654,
	LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_LayerToName_m5657,
	LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m5658,
	LayerMask_t95_CustomAttributesCacheGenerator_LayerMask_t95_LayerMask_GetMask_m5659_Arg0_ParameterInfo,
	RuntimePlatform_t1140_CustomAttributesCacheGenerator_NaCl,
	RuntimePlatform_t1140_CustomAttributesCacheGenerator_FlashPlayer,
	RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerX86,
	RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerX64,
	RuntimePlatform_t1140_CustomAttributesCacheGenerator_MetroPlayerARM,
	SystemInfo_t1142_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m5660,
	Coroutine_t316_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m5663,
	ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m5665,
	ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_t955_ScriptableObject_Internal_CreateScriptableObject_m5665_Arg0_ParameterInfo,
	ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m5666,
	ScriptableObject_t955_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m5668,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m5673,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m5674,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m5675,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m5676,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m5677,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m5678,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m5679,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m5680,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m5681,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m5682,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m5683,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m5684,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m5685,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m5686,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m5687,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m5688,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5689,
	GameCenterPlatform_t1153_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5693,
	GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m5737,
	GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m5738,
	GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m5739,
	GcLeaderboard_t1155_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m5740,
	MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_get_mesh_m4283,
	MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_set_mesh_m4596,
	MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_get_sharedMesh_m486,
	MeshFilter_t149_CustomAttributesCacheGenerator_MeshFilter_set_sharedMesh_m4337,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_Internal_Create_m5741,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_t153_Mesh_Internal_Create_m5741_Arg0_ParameterInfo,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_Clear_m5742,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_t153_Mesh_Clear_m5742_Arg0_ParameterInfo,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_Clear_m4284,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_vertices_m487,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_vertices_m4285,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_normals_m4289,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_uv_m4288,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_uv_m4287,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_INTERNAL_get_bounds_m5743,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_RecalculateNormals_m4290,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_get_triangles_m488,
	Mesh_t153_CustomAttributesCacheGenerator_Mesh_set_triangles_m4286,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_enabled_m268,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_enabled_m505,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_material_m269,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_material_m4329,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_sharedMaterial_m451,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_materials_m340,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_set_sharedMaterials_m452,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m2046,
	Renderer_t8_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m2047,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_width_m316,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_height_m317,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_dpi_m2381,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToPortrait_m4411,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToPortrait_m4415,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToPortraitUpsideDown_m4412,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToPortraitUpsideDown_m4416,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeLeft_m4409,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeLeft_m4413,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeRight_m4410,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeRight_m4414,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_get_orientation_m421,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_orientation_m4636,
	Screen_t1157_CustomAttributesCacheGenerator_Screen_set_sleepTimeout_m291,
	GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Vertex_m5764,
	GL_t1158_CustomAttributesCacheGenerator_GL_Begin_m493,
	GL_t1158_CustomAttributesCacheGenerator_GL_End_m495,
	GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_MultMatrix_m5765,
	GL_t1158_CustomAttributesCacheGenerator_GL_PushMatrix_m489,
	GL_t1158_CustomAttributesCacheGenerator_GL_PopMatrix_m496,
	GL_t1158_CustomAttributesCacheGenerator_GL_SetRevertBackfacing_m4663,
	GL_t1158_CustomAttributesCacheGenerator_GL_Clear_m4351,
	GL_t1158_CustomAttributesCacheGenerator_GL_t1158_GL_Clear_m5766_Arg3_ParameterInfo,
	GL_t1158_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m5768,
	GL_t1158_CustomAttributesCacheGenerator_GL_IssuePluginEvent_m4430,
	Texture_t321_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m5770,
	Texture_t321_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m5771,
	Texture_t321_CustomAttributesCacheGenerator_Texture_set_filterMode_m4666,
	Texture_t321_CustomAttributesCacheGenerator_Texture_set_wrapMode_m4667,
	Texture_t321_CustomAttributesCacheGenerator_Texture_GetNativeTextureID_m4668,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m5774,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Internal_Create_m5774_Arg0_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_get_format_m4365,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m2093,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m2177,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels_m4370,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels_m5775_Arg1_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels_m5776,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels_m5776_Arg5_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetAllPixels32_m5777,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_SetPixels32_m4608,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_SetPixels32_m5778_Arg1_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels_m4367,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels_m5779_Arg0_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels_m5780,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels_m5780_Arg4_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m5781,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_GetPixels32_m5781_Arg0_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m4606,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Apply_m5782,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Apply_m5782_Arg0_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_Apply_m5782_Arg1_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Apply_m4609,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_Resize_m4366,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_t270_Texture2D_ReadPixels_m4605_Arg3_ParameterInfo,
	Texture2D_t270_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_ReadPixels_m5783,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m5784,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg2_ParameterInfo,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg3_ParameterInfo,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg4_ParameterInfo,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_t786_RenderTexture_GetTemporary_m5784_Arg5_ParameterInfo,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m4597,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m4607,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m5785,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m5786,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_set_depth_m4600,
	RenderTexture_t786_CustomAttributesCacheGenerator_RenderTexture_set_active_m4604,
	GUILayer_t1161_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m5790,
	Gradient_t1164_CustomAttributesCacheGenerator_Gradient_Init_m5794,
	Gradient_t1164_CustomAttributesCacheGenerator_Gradient_Cleanup_m5795,
	GUI_t126_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField,
	GUI_t126_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField,
	GUI_t126_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m5802,
	GUI_t126_CustomAttributesCacheGenerator_GUI_set_changed_m5805,
	GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m5808,
	GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m5810,
	GUI_t126_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoWindow_m5813,
	GUILayoutUtility_t1173_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m5825,
	GUILayoutUtility_t1173_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5827,
	GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m5858,
	GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m5859,
	GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m5861,
	GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m5863,
	GUIUtility_t1180_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m5867,
	GUISettings_t1181_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord,
	GUISettings_t1181_CustomAttributesCacheGenerator_m_TripleClickSelectsLine,
	GUISettings_t1181_CustomAttributesCacheGenerator_m_CursorColor,
	GUISettings_t1181_CustomAttributesCacheGenerator_m_CursorFlashSpeed,
	GUISettings_t1181_CustomAttributesCacheGenerator_m_SelectionColor,
	GUISkin_t1166_CustomAttributesCacheGenerator,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_Font,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_box,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_button,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_toggle,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_label,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_textField,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_textArea,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_window,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalSlider,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalSliderThumb,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalSlider,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalSliderThumb,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbar,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbar,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarThumb,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_ScrollView,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_CustomStyles,
	GUISkin_t1166_CustomAttributesCacheGenerator_m_Settings,
	GUIContent_t456_CustomAttributesCacheGenerator_m_Text,
	GUIContent_t456_CustomAttributesCacheGenerator_m_Image,
	GUIContent_t456_CustomAttributesCacheGenerator_m_Tooltip,
	GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_Init_m5935,
	GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m5936,
	GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m5937,
	GUIStyleState_t1185_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m5938,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_Init_m5942,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_Cleanup_m5943,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_left_m2398,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_left_m5526,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_right_m5523,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_right_m5527,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_top_m2399,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_top_m5528,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_bottom_m5524,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_set_bottom_m5529,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m2391,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_get_vertical_m2393,
	RectOffset_t371_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m5945,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Init_m5950,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m5951,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_name_m5952,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_name_m5953,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m5955,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m5958,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m5959,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m5960,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m5961,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m5962,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m5963,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m5964,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m5965,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m5967,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5971,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m5973,
	GUIStyle_t1172_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m5975,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m5978,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5980,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2281,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2282,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg1_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg2_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg3_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg4_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg5_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_t311_TouchScreenKeyboard_Open_m5981_Arg6_ParameterInfo,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m2205,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m2206,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m2280,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m2204,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m2279,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m2221,
	TouchScreenKeyboard_t311_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m2218,
	Event_t317_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	Event_t317_CustomAttributesCacheGenerator_Event_Init_m5982,
	Event_t317_CustomAttributesCacheGenerator_Event_Cleanup_m5984,
	Event_t317_CustomAttributesCacheGenerator_Event_get_rawType_m2235,
	Event_t317_CustomAttributesCacheGenerator_Event_get_type_m5985,
	Event_t317_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m5987,
	Event_t317_CustomAttributesCacheGenerator_Event_get_modifiers_m2231,
	Event_t317_CustomAttributesCacheGenerator_Event_get_character_m2233,
	Event_t317_CustomAttributesCacheGenerator_Event_get_commandName_m5988,
	Event_t317_CustomAttributesCacheGenerator_Event_get_keyCode_m2232,
	Event_t317_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m5990,
	Event_t317_CustomAttributesCacheGenerator_Event_PopEvent_m2236,
	EventModifiers_t1190_CustomAttributesCacheGenerator,
	Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m5997,
	Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireCube_m5998,
	Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawCube_m5999,
	Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m6000,
	Gizmos_t1191_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_matrix_m6001,
	Vector2_t10_CustomAttributesCacheGenerator,
	Vector3_t15_CustomAttributesCacheGenerator,
	Color_t90_CustomAttributesCacheGenerator,
	Color32_t421_CustomAttributesCacheGenerator,
	Quaternion_t13_CustomAttributesCacheGenerator,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6018,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m6019,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_t13_Quaternion_LookRotation_m5543_Arg1_ParameterInfo,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m6020,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6021,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6022,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6025,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6027,
	Quaternion_t13_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m6029,
	Matrix4x4_t156_CustomAttributesCacheGenerator,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6039,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6041,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6043,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6046,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6055,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6058,
	Matrix4x4_t156_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6059,
	Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6076,
	Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6079,
	Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6082,
	Bounds_t334_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6086,
	Vector4_t413_CustomAttributesCacheGenerator,
	Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_Max_m4571_Arg0_ParameterInfo,
	Mathf_t104_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m377,
	Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_SmoothDamp_m2306_Arg4_ParameterInfo,
	Mathf_t104_CustomAttributesCacheGenerator_Mathf_t104_Mathf_SmoothDamp_m2306_Arg5_ParameterInfo,
	DrivenTransformProperties_t1193_CustomAttributesCacheGenerator,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6115,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6116,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6117,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6118,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6119,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6120,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6121,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6122,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6123,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6124,
	RectTransform_t272_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6125,
	Resources_t1197_CustomAttributesCacheGenerator_Resources_Load_m6131,
	Resources_t1197_CustomAttributesCacheGenerator_Resources_UnloadUnusedAssets_m4546,
	SerializePrivateVariables_t1198_CustomAttributesCacheGenerator,
	Shader_t152_CustomAttributesCacheGenerator_Shader_Find_m4594,
	Shader_t152_CustomAttributesCacheGenerator_Shader_PropertyToID_m6133,
	Material_t4_CustomAttributesCacheGenerator_Material__ctor_m481,
	Material_t4_CustomAttributesCacheGenerator_Material_get_shader_m483,
	Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6136,
	Material_t4_CustomAttributesCacheGenerator_Material_SetTexture_m6138,
	Material_t4_CustomAttributesCacheGenerator_Material_GetTexture_m6140,
	Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetTextureOffset_m6141,
	Material_t4_CustomAttributesCacheGenerator_Material_SetFloat_m6143,
	Material_t4_CustomAttributesCacheGenerator_Material_HasProperty_m6144,
	Material_t4_CustomAttributesCacheGenerator_Material_SetPass_m492,
	Material_t4_CustomAttributesCacheGenerator_Material_set_renderQueue_m341,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithString_m6145,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithString_m6145_Arg0_ParameterInfo,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6146,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithShader_m6146_Arg0_ParameterInfo,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6147,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithMaterial_m6147_Arg0_ParameterInfo,
	SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator,
	SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6150,
	SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6153,
	SphericalHarmonicsL2_t1199_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6156,
	Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_rect_m2152,
	Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m2148,
	Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_texture_m2145,
	Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_textureRect_m2174,
	Sprite_t292_CustomAttributesCacheGenerator_Sprite_get_border_m2146,
	DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m2163,
	DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m2162,
	DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_GetPadding_m2151,
	DataUtility_t1200_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6166,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6170,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_InitWWW_m6171,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6173,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_get_bytes_m6177,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_get_error_m6178,
	WWW_t1201_CustomAttributesCacheGenerator_WWW_get_isDone_m6179,
	WWWForm_t1203_CustomAttributesCacheGenerator_WWWForm_AddField_m6183,
	WWWForm_t1203_CustomAttributesCacheGenerator_WWWForm_t1203_WWWForm_AddField_m6184_Arg2_ParameterInfo,
	WWWTranscoder_t1204_CustomAttributesCacheGenerator_WWWTranscoder_t1204_WWWTranscoder_QPEncode_m6191_Arg1_ParameterInfo,
	WWWTranscoder_t1204_CustomAttributesCacheGenerator_WWWTranscoder_t1204_WWWTranscoder_SevenBitClean_m6194_Arg1_ParameterInfo,
	CacheIndex_t1205_CustomAttributesCacheGenerator,
	UnityString_t1206_CustomAttributesCacheGenerator_UnityString_t1206_UnityString_Format_m6196_Arg1_ParameterInfo,
	AsyncOperation_t1134_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6198,
	Application_t1208_CustomAttributesCacheGenerator_Application_Quit_m402,
	Application_t1208_CustomAttributesCacheGenerator_Application_get_isPlaying_m2284,
	Application_t1208_CustomAttributesCacheGenerator_Application_get_isEditor_m2287,
	Application_t1208_CustomAttributesCacheGenerator_Application_get_platform_m459,
	Application_t1208_CustomAttributesCacheGenerator_Application_set_runInBackground_m4676,
	Application_t1208_CustomAttributesCacheGenerator_Application_get_dataPath_m4610,
	Application_t1208_CustomAttributesCacheGenerator_Application_get_unityVersion_m4431,
	Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_get_enabled_m497,
	Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_set_enabled_m240,
	Behaviour_t477_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1985,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m2038,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_nearClipPlane_m4589,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m2037,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_farClipPlane_m4590,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4587,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographic_m4586,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_depth_m1949,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_aspect_m4588,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2049,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_cullingMask_m4591,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_eventMask_m6210,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_rect_m6211,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6212,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6213,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4599,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_targetTexture_m4598,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m6214,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_projectionMatrix_m6215,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6216,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6217,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6218,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6219,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_main_m252,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_current_m485,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6220,
	Camera_t3_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6221,
	Camera_t3_CustomAttributesCacheGenerator_Camera_Render_m4601,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6226,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6228,
	CameraCallback_t1209_CustomAttributesCacheGenerator,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg2_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg3_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawLine_m6229_Arg4_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6230,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_DrawRay_m257,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg2_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg3_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_DrawRay_m6231_Arg4_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_Internal_Log_m6232,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_Internal_Log_m6232_Arg2_ParameterInfo,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_Internal_LogException_m6233,
	Debug_t1210_CustomAttributesCacheGenerator_Debug_t1210_Debug_Internal_LogException_m6233_Arg1_ParameterInfo,
	Display_t1213_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m6259,
	Display_t1213_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m6260,
	Display_t1213_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m6261,
	Display_t1213_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m6262,
	Display_t1213_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m6263,
	Display_t1213_CustomAttributesCacheGenerator_Display_SetParamsImpl_m6264,
	Display_t1213_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m6265,
	Display_t1213_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m6266,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m6267,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6269,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m6270,
	Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_rotationRate_Internal_m6272,
	Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_attitude_Internal_m6273,
	Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_setEnabled_Internal_m6274,
	Gyroscope_t105_CustomAttributesCacheGenerator_Gyroscope_setUpdateInterval_Internal_m6275,
	Input_t102_CustomAttributesCacheGenerator_Input_mainGyroIndex_Internal_m6277,
	Input_t102_CustomAttributesCacheGenerator_Input_GetKeyUpInt_m6278,
	Input_t102_CustomAttributesCacheGenerator_Input_GetAxis_m376,
	Input_t102_CustomAttributesCacheGenerator_Input_GetAxisRaw_m2028,
	Input_t102_CustomAttributesCacheGenerator_Input_GetButtonDown_m2027,
	Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButton_m306,
	Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m307,
	Input_t102_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m308,
	Input_t102_CustomAttributesCacheGenerator_Input_get_mousePosition_m253,
	Input_t102_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m2001,
	Input_t102_CustomAttributesCacheGenerator_Input_get_mousePresent_m2026,
	Input_t102_CustomAttributesCacheGenerator_Input_GetTouch_m302,
	Input_t102_CustomAttributesCacheGenerator_Input_get_touchCount_m286,
	Input_t102_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m2283,
	Input_t102_CustomAttributesCacheGenerator_Input_get_compositionString_m2208,
	Input_t102_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m6279,
	HideFlags_t1216_CustomAttributesCacheGenerator,
	Object_t123_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m6281,
	Object_t123_CustomAttributesCacheGenerator_Object_Destroy_m6282,
	Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_Destroy_m6282_Arg1_ParameterInfo,
	Object_t123_CustomAttributesCacheGenerator_Object_Destroy_m471,
	Object_t123_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6283,
	Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_DestroyImmediate_m6283_Arg1_ParameterInfo,
	Object_t123_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2286,
	Object_t123_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m4425,
	Object_t123_CustomAttributesCacheGenerator_Object_get_name_m259,
	Object_t123_CustomAttributesCacheGenerator_Object_set_name_m2352,
	Object_t123_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m4299,
	Object_t123_CustomAttributesCacheGenerator_Object_set_hideFlags_m482,
	Object_t123_CustomAttributesCacheGenerator_Object_DestroyObject_m6284,
	Object_t123_CustomAttributesCacheGenerator_Object_t123_Object_DestroyObject_m6284_Arg1_ParameterInfo,
	Object_t123_CustomAttributesCacheGenerator_Object_DestroyObject_m387,
	Object_t123_CustomAttributesCacheGenerator_Object_ToString_m539,
	Object_t123_CustomAttributesCacheGenerator_Object_Instantiate_m4491,
	Object_t123_CustomAttributesCacheGenerator_Object_FindObjectOfType_m396,
	Component_t103_CustomAttributesCacheGenerator_Component_get_transform_m243,
	Component_t103_CustomAttributesCacheGenerator_Component_get_gameObject_m272,
	Component_t103_CustomAttributesCacheGenerator_Component_GetComponent_m2406,
	Component_t103_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m6290,
	Component_t103_CustomAttributesCacheGenerator_Component_GetComponent_m7051,
	Component_t103_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m6291,
	Component_t103_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m6292,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject__ctor_m4592_Arg1_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_CreatePrimitive_m4327,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m6293,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m6294,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m7059,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m6295,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m6296,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_transform_m273,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_layer_m2254,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_set_layer_m2255,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActive_m238,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m1986,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActiveRecursively_m239,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SendMessage_m6297,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6297_Arg1_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6297_Arg2_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m6298,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_AddComponent_m6299,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m6300,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_Internal_CreateGameObject_m6300_Arg0_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m6304,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m6305,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m6306,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m6307,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m6308,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m6309,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m6310,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m6311,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m6312,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m6313,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_parentInternal_m6314,
	Transform_t11_CustomAttributesCacheGenerator_Transform_set_parentInternal_m6315,
	Transform_t11_CustomAttributesCacheGenerator_Transform_SetParent_m6316,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m6317,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localToWorldMatrix_m6318,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6319_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_Rotate_m296,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6320_Arg3_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_LookAt_m379,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6321_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6322_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m6323,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m6324,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m6325,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_root_m4615,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_childCount_m2407,
	Transform_t11_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m2253,
	Transform_t11_CustomAttributesCacheGenerator_Transform_Find_m348,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_lossyScale_m6326,
	Transform_t11_CustomAttributesCacheGenerator_Transform_GetChild_m2405,
	Time_t1218_CustomAttributesCacheGenerator_Time_get_deltaTime_m265,
	Time_t1218_CustomAttributesCacheGenerator_Time_get_unscaledTime_m2030,
	Time_t1218_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m2060,
	Time_t1218_CustomAttributesCacheGenerator_Time_get_timeScale_m5511,
	Time_t1218_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m5510,
	Random_t1219_CustomAttributesCacheGenerator_Random_RandomRangeInt_m6328,
	PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6330,
	PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_t1220_PlayerPrefs_GetString_m6330_Arg1_ParameterInfo,
	PlayerPrefs_t1220_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6331,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m6349,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m6350_Arg3_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m6350_Arg4_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_Raycast_m255,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m321_Arg2_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_Raycast_m321_Arg3_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m2050_Arg1_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m2050_Arg2_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m6351_Arg2_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_t1222_Physics_RaycastAll_m6351_Arg3_ParameterInfo,
	Physics_t1222_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m6352,
	Collider_t157_CustomAttributesCacheGenerator_Collider_set_enabled_m506,
	Collider_t157_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m6353,
	MeshCollider_t590_CustomAttributesCacheGenerator_MeshCollider_set_sharedMesh_m4338,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m6357,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_Raycast_m2127,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg2_ParameterInfo,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg3_ParameterInfo,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg4_ParameterInfo,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_t431_Physics2D_Raycast_m6358_Arg5_ParameterInfo,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m2041,
	Physics2D_t431_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m6359,
	Collider2D_t433_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m6361,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_Internal_CreateWebCamTexture_m6378,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_t682_WebCamTexture_Internal_CreateWebCamTexture_m6378_Arg0_ParameterInfo,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Play_m6379,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Stop_m6380,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_isPlaying_m4446,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_deviceName_m4448,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedFPS_m4449,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedWidth_m4450,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_set_requestedHeight_m4451,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_devices_m4602,
	WebCamTexture_t682_CustomAttributesCacheGenerator_WebCamTexture_get_didUpdateThisFrame_m4445,
	AnimationEvent_t1233_CustomAttributesCacheGenerator_AnimationEvent_t1233____data_PropertyInfo,
	AnimationCurve_t1237_CustomAttributesCacheGenerator,
	AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_t1237_AnimationCurve__ctor_m6404_Arg0_ParameterInfo,
	AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m6406,
	AnimationCurve_t1237_CustomAttributesCacheGenerator_AnimationCurve_Init_m6408,
	AnimatorStateInfo_t1234_CustomAttributesCacheGenerator_AnimatorStateInfo_t1234____nameHash_PropertyInfo,
	Animator_t415_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m2343,
	Animator_t415_CustomAttributesCacheGenerator_Animator_StringToHash_m6427,
	Animator_t415_CustomAttributesCacheGenerator_Animator_SetTriggerString_m6428,
	Animator_t415_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m6429,
	CharacterInfo_t1243_CustomAttributesCacheGenerator_uv,
	CharacterInfo_t1243_CustomAttributesCacheGenerator_vert,
	CharacterInfo_t1243_CustomAttributesCacheGenerator_width,
	CharacterInfo_t1243_CustomAttributesCacheGenerator_flipped,
	Font_t266_CustomAttributesCacheGenerator_Font_get_material_m2356,
	Font_t266_CustomAttributesCacheGenerator_Font_HasCharacter_m2234,
	Font_t266_CustomAttributesCacheGenerator_Font_get_dynamic_m2359,
	Font_t266_CustomAttributesCacheGenerator_Font_get_fontSize_m2361,
	FontTextureRebuildCallback_t1244_CustomAttributesCacheGenerator,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_Init_m6457,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m6458,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6461,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m2250,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m6462,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m6463,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m6464,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m6465,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m6466,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m6467,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m2227,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m6468,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m6469,
	TextGenerator_t314_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m2270,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_renderMode_m2123,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m2378,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m2131,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m2360,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m2382,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m2149,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m2383,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m2108,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m2125,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m2124,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m2130,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m2090,
	Canvas_t274_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m2355,
	CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m2334,
	CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m6481,
	CanvasGroup_t442_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m2107,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m6484,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m2111,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m2417,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m2101,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m6485,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m6486,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m2094,
	CanvasRenderer_t273_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m2091,
	RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6488,
	RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6490,
	RectTransformUtility_t444_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m2110,
	Request_t1248_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField,
	Request_t1248_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField,
	Request_t1248_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField,
	Request_t1248_CustomAttributesCacheGenerator_Request_get_sourceId_m6495,
	Request_t1248_CustomAttributesCacheGenerator_Request_get_appId_m6496,
	Request_t1248_CustomAttributesCacheGenerator_Request_get_domain_m6497,
	Response_t1250_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField,
	Response_t1250_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField,
	Response_t1250_CustomAttributesCacheGenerator_Response_get_success_m6506,
	Response_t1250_CustomAttributesCacheGenerator_Response_set_success_m6507,
	Response_t1250_CustomAttributesCacheGenerator_Response_get_extendedInfo_m6508,
	Response_t1250_CustomAttributesCacheGenerator_Response_set_extendedInfo_m6509,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m6514,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m6515,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m6516,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m6517,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m6518,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m6519,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m6520,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m6521,
	CreateMatchRequest_t1253_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m6522,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m6525,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m6526,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m6527,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m6528,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m6529,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m6530,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m6531,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m6532,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m6533,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m6534,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m6535,
	CreateMatchResponse_t1254_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m6536,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m6540,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m6541,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m6542,
	JoinMatchRequest_t1255_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m6543,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m6546,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m6547,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m6548,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m6549,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m6550,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m6551,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m6552,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m6553,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m6554,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m6555,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m6556,
	JoinMatchResponse_t1256_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m6557,
	DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m6561,
	DestroyMatchRequest_t1257_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m6562,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m6565,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m6566,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m6567,
	DropConnectionRequest_t1258_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m6568,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m6571,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m6572,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m6573,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m6574,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m6575,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m6576,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m6577,
	ListMatchRequest_t1259_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m6578,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m6581,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m6582,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m6583,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m6584,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m6585,
	MatchDirectConnectInfo_t1260_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m6586,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m6590,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m6591,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_name_m6592,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_name_m6593,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m6594,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m6595,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m6596,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m6597,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m6598,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m6599,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m6600,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m6601,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m6602,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m6603,
	MatchDesc_t1262_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m6604,
	ListMatchResponse_t1264_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField,
	ListMatchResponse_t1264_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m6608,
	ListMatchResponse_t1264_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m6609,
	AppID_t1265_CustomAttributesCacheGenerator,
	SourceID_t1266_CustomAttributesCacheGenerator,
	NetworkID_t1267_CustomAttributesCacheGenerator,
	NodeID_t1268_CustomAttributesCacheGenerator,
	NetworkMatch_t1274_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m7071,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7077,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7078,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m7080,
	JsonArray_t1275_CustomAttributesCacheGenerator,
	JsonObject_t1277_CustomAttributesCacheGenerator,
	SimpleJson_t1280_CustomAttributesCacheGenerator,
	SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m6653,
	SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_NextToken_m6665,
	SimpleJson_t1280_CustomAttributesCacheGenerator_SimpleJson_t1280____PocoJsonSerializerStrategy_PropertyInfo,
	IJsonSerializerStrategy_t1278_CustomAttributesCacheGenerator,
	IJsonSerializerStrategy_t1278_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m7084,
	PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator,
	PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6682,
	PocoJsonSerializerStrategy_t1279_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6683,
	ReflectionUtils_t1293_CustomAttributesCacheGenerator,
	ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetConstructorInfo_m6708_Arg1_ParameterInfo,
	ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetContructor_m6713_Arg1_ParameterInfo,
	ReflectionUtils_t1293_CustomAttributesCacheGenerator_ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6715_Arg1_ParameterInfo,
	ThreadSafeDictionary_2_t1450_CustomAttributesCacheGenerator,
	ConstructorDelegate_t1286_CustomAttributesCacheGenerator_ConstructorDelegate_t1286_ConstructorDelegate_Invoke_m6693_Arg0_ParameterInfo,
	ConstructorDelegate_t1286_CustomAttributesCacheGenerator_ConstructorDelegate_t1286_ConstructorDelegate_BeginInvoke_m6694_Arg0_ParameterInfo,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_CustomAttributesCacheGenerator,
	IL2CPPStructAlignmentAttribute_t1295_CustomAttributesCacheGenerator,
	DisallowMultipleComponent_t501_CustomAttributesCacheGenerator,
	RequireComponent_t163_CustomAttributesCacheGenerator,
	WritableAttribute_t1301_CustomAttributesCacheGenerator,
	AssemblyIsEditorAssembly_t1302_CustomAttributesCacheGenerator,
	Achievement_t1317_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t1317_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t1317_CustomAttributesCacheGenerator_Achievement_get_id_m6766,
	Achievement_t1317_CustomAttributesCacheGenerator_Achievement_set_id_m6767,
	Achievement_t1317_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6768,
	Achievement_t1317_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6769,
	AchievementDescription_t1318_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t1318_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6776,
	AchievementDescription_t1318_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6777,
	Score_t1319_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t1319_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t1319_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6786,
	Score_t1319_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6787,
	Score_t1319_CustomAttributesCacheGenerator_Score_get_value_m6788,
	Score_t1319_CustomAttributesCacheGenerator_Score_set_value_m6789,
	Leaderboard_t1154_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t1154_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t1154_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t1154_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_id_m6797,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_id_m6798,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6799,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6800,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_range_m6801,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_range_m6802,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6803,
	Leaderboard_t1154_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6804,
	PropertyAttribute_t1329_CustomAttributesCacheGenerator,
	TooltipAttribute_t505_CustomAttributesCacheGenerator,
	SpaceAttribute_t503_CustomAttributesCacheGenerator,
	RangeAttribute_t499_CustomAttributesCacheGenerator,
	TextAreaAttribute_t506_CustomAttributesCacheGenerator,
	SelectionBaseAttribute_t504_CustomAttributesCacheGenerator,
	StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6817,
	StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6820,
	StackTraceUtility_t1331_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6822,
	SharedBetweenAnimatorsAttribute_t1332_CustomAttributesCacheGenerator,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t1338_CustomAttributesCacheGenerator_m_BoolArgument,
	PersistentCall_t1342_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t1342_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t1342_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t1342_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t1342_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t1344_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t1347_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t1347_CustomAttributesCacheGenerator_m_TypeName,
	UserAuthorizationDialog_t1348_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t1349_CustomAttributesCacheGenerator,
	ExcludeFromDocsAttribute_t1350_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t492_CustomAttributesCacheGenerator,
	TypeInferenceRuleAttribute_t1352_CustomAttributesCacheGenerator,
};
