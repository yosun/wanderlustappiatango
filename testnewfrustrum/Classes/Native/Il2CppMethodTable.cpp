﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m1961_gshared ();
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_Execute_TisObject_t_m1944_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m2017_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m27818_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m27814_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m27843_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m1998_gshared ();
extern "C" void EventFunction_1__ctor_m15306_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_Invoke_m15308_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m15310_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_EndInvoke_m15312_gshared ();
void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m2138_gshared ();
void* RuntimeInvoker_Boolean_t169_ObjectU26_t1516_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisObject_t_m2401_gshared ();
void* RuntimeInvoker_Void_t168_ObjectU26_t1516_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Count_m16506_gshared ();
void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_IsReadOnly_m16508_gshared ();
void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Item_m16516_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_set_Item_m16518_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1__ctor_m16490_gshared ();
void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared ();
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Add_m16494_gshared ();
extern "C" void IndexedSet_1_Remove_m16496_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m16498_gshared ();
extern "C" void IndexedSet_1_Clear_m16500_gshared ();
extern "C" void IndexedSet_1_Contains_m16502_gshared ();
extern "C" void IndexedSet_1_CopyTo_m16504_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_IndexOf_m16510_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Insert_m16512_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m16514_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_RemoveAll_m16519_gshared ();
extern "C" void IndexedSet_1_Sort_m16520_gshared ();
extern "C" void ObjectPool_1_get_countAll_m15409_gshared ();
extern "C" void ObjectPool_1_set_countAll_m15411_gshared ();
extern "C" void ObjectPool_1_get_countActive_m15413_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m15415_gshared ();
extern "C" void ObjectPool_1__ctor_m15407_gshared ();
extern "C" void ObjectPool_1_Get_m15417_gshared ();
extern "C" void ObjectPool_1_Release_m15419_gshared ();
extern "C" void NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m28070_gshared ();
extern "C" void SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m28137_gshared ();
extern "C" void TrackerManagerImpl_GetTracker_TisObject_t_m28245_gshared ();
extern "C" void TrackerManagerImpl_InitTracker_TisObject_t_m28246_gshared ();
extern "C" void TrackerManagerImpl_DeinitTracker_TisObject_t_m28247_gshared ();
extern "C" void QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340_gshared ();
extern "C" void TweenSettingsExtensions_SetTarget_TisObject_t_m5519_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenSettingsExtensions_OnComplete_TisObject_t_m236_gshared ();
extern "C" void TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared ();
extern "C" void TweenCallback_1__ctor_m23599_gshared ();
extern "C" void TweenCallback_1_Invoke_m23600_gshared ();
extern "C" void TweenCallback_1_BeginInvoke_m23601_gshared ();
extern "C" void TweenCallback_1_EndInvoke_m23602_gshared ();
extern "C" void DOGetter_1__ctor_m23603_gshared ();
extern "C" void DOGetter_1_Invoke_m23604_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23605_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23606_gshared ();
extern "C" void DOSetter_1__ctor_m23607_gshared ();
extern "C" void DOSetter_1_Invoke_m23608_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23609_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23610_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisObject_t_m28367_gshared ();
extern "C" void Object_Instantiate_TisObject_t_m381_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m262_gshared ();
extern "C" void Component_GetComponentInChildren_TisObject_t_m4326_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m343_gshared ();
void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m27998_gshared ();
void* RuntimeInvoker_Void_t168_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m4438_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m2416_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m1942_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m2036_gshared ();
extern "C" void GameObject_GetComponentInChildren_TisObject_t_m4614_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m27817_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m27769_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m27999_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m479_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m2083_gshared ();
extern "C" void GameObject_AddComponent_TisObject_t_m437_gshared ();
extern "C" void ResponseBase_ParseJSONList_TisObject_t_m6958_gshared ();
extern "C" void NetworkMatch_ProcessMatchResponse_TisObject_t_m6965_gshared ();
extern "C" void ResponseDelegate_1__ctor_m25832_gshared ();
extern "C" void ResponseDelegate_1_Invoke_m25834_gshared ();
extern "C" void ResponseDelegate_1_BeginInvoke_m25836_gshared ();
extern "C" void ResponseDelegate_1_EndInvoke_m25838_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25840_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25841_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25839_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25842_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25843_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Keys_m25983_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Values_m25989_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Item_m25991_gshared ();
extern "C" void ThreadSafeDictionary_2_set_Item_m25993_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Count_m26003_gshared ();
extern "C" void ThreadSafeDictionary_2_get_IsReadOnly_m26005_gshared ();
extern "C" void ThreadSafeDictionary_2__ctor_m25973_gshared ();
extern "C" void ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975_gshared ();
extern "C" void ThreadSafeDictionary_2_Get_m25977_gshared ();
extern "C" void ThreadSafeDictionary_2_AddValue_m25979_gshared ();
extern "C" void ThreadSafeDictionary_2_Add_m25981_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m25985_gshared ();
extern "C" void ThreadSafeDictionary_2_TryGetValue_m25987_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Add_m25995_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Clear_m25997_gshared ();
extern "C" void ThreadSafeDictionary_2_Contains_m25999_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_CopyTo_m26001_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m26007_gshared ();
extern "C" void ThreadSafeDictionary_2_GetEnumerator_m26009_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2__ctor_m25966_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_Invoke_m25968_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_BeginInvoke_m25970_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_EndInvoke_m25972_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m27842_gshared ();
extern "C" void InvokableCall_1__ctor_m15888_gshared ();
extern "C" void InvokableCall_1__ctor_m15889_gshared ();
extern "C" void InvokableCall_1_Invoke_m15890_gshared ();
extern "C" void InvokableCall_1_Find_m15891_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InvokableCall_2__ctor_m26704_gshared ();
extern "C" void InvokableCall_2_Invoke_m26705_gshared ();
extern "C" void InvokableCall_2_Find_m26706_gshared ();
extern "C" void InvokableCall_3__ctor_m26711_gshared ();
extern "C" void InvokableCall_3_Invoke_m26712_gshared ();
extern "C" void InvokableCall_3_Find_m26713_gshared ();
extern "C" void InvokableCall_4__ctor_m26718_gshared ();
extern "C" void InvokableCall_4_Invoke_m26719_gshared ();
extern "C" void InvokableCall_4_Find_m26720_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m26725_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m26726_gshared ();
extern "C" void UnityEvent_1__ctor_m15878_gshared ();
extern "C" void UnityEvent_1_AddListener_m15880_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m15882_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m15883_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m15884_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m15886_gshared ();
extern "C" void UnityEvent_1_Invoke_m15887_gshared ();
extern "C" void UnityEvent_2__ctor_m26929_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m26930_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m26931_gshared ();
extern "C" void UnityEvent_3__ctor_m26932_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m26933_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m26934_gshared ();
extern "C" void UnityEvent_4__ctor_m26935_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m26936_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m26937_gshared ();
extern "C" void UnityAction_1__ctor_m15436_gshared ();
extern "C" void UnityAction_1_Invoke_m15437_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m15438_gshared ();
extern "C" void UnityAction_1_EndInvoke_m15439_gshared ();
extern "C" void UnityAction_2__ctor_m26707_gshared ();
extern "C" void UnityAction_2_Invoke_m26708_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m26709_gshared ();
extern "C" void UnityAction_2_EndInvoke_m26710_gshared ();
extern "C" void UnityAction_3__ctor_m26714_gshared ();
extern "C" void UnityAction_3_Invoke_m26715_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m26716_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_3_EndInvoke_m26717_gshared ();
extern "C" void UnityAction_4__ctor_m26721_gshared ();
extern "C" void UnityAction_4_Invoke_m26722_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_BeginInvoke_m26723_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_EndInvoke_m26724_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23147_gshared ();
extern "C" void HashSet_1_get_Count_m23155_gshared ();
extern "C" void HashSet_1__ctor_m23141_gshared ();
extern "C" void HashSet_1__ctor_m23143_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23145_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23149_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23151_gshared ();
extern "C" void HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23153_gshared ();
extern "C" void HashSet_1_Init_m23157_gshared ();
extern "C" void HashSet_1_InitArrays_m23159_gshared ();
extern "C" void HashSet_1_SlotsContainsAt_m23161_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_CopyTo_m23163_gshared ();
extern "C" void HashSet_1_CopyTo_m23165_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_Resize_m23167_gshared ();
extern "C" void HashSet_1_GetLinkHashCode_m23169_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_GetItemHashCode_m23171_gshared ();
extern "C" void HashSet_1_Add_m23172_gshared ();
extern "C" void HashSet_1_Clear_m23174_gshared ();
extern "C" void HashSet_1_Contains_m23176_gshared ();
extern "C" void HashSet_1_Remove_m23178_gshared ();
extern "C" void HashSet_1_GetObjectData_m23180_gshared ();
extern "C" void HashSet_1_OnDeserialization_m23182_gshared ();
extern "C" void HashSet_1_GetEnumerator_m23183_gshared ();
void* RuntimeInvoker_Enumerator_t3656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23190_gshared ();
extern "C" void Enumerator_get_Current_m23192_gshared ();
extern "C" void Enumerator__ctor_m23189_gshared ();
extern "C" void Enumerator_MoveNext_m23191_gshared ();
extern "C" void Enumerator_Dispose_m23193_gshared ();
extern "C" void Enumerator_CheckState_m23194_gshared ();
extern "C" void PrimeHelper__cctor_m23195_gshared ();
extern "C" void PrimeHelper_TestPrime_m23196_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void PrimeHelper_CalcPrime_m23197_gshared ();
extern "C" void PrimeHelper_ToPrime_m23198_gshared ();
extern "C" void Enumerable_Any_TisObject_t_m4407_gshared ();
extern "C" void Enumerable_Cast_TisObject_t_m4383_gshared ();
extern "C" void Enumerable_CreateCastIterator_TisObject_t_m28068_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m4304_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m28022_gshared ();
extern "C" void Enumerable_First_TisObject_t_m4459_gshared ();
extern "C" void Enumerable_ToArray_TisObject_t_m4455_gshared ();
extern "C" void Enumerable_ToList_TisObject_t_m428_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m2369_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m27997_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19569_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19570_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19568_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19571_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19572_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19573_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19574_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18148_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18149_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18147_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18150_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18151_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18152_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18153_gshared ();
extern "C" void Func_2__ctor_m26938_gshared ();
extern "C" void Func_2_Invoke_m26939_gshared ();
extern "C" void Func_2_BeginInvoke_m26940_gshared ();
extern "C" void Func_2_EndInvoke_m26941_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26973_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26974_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26975_gshared ();
extern "C" void LinkedList_1_get_Count_m26988_gshared ();
extern "C" void LinkedList_1_get_First_m26989_gshared ();
extern "C" void LinkedList_1__ctor_m26967_gshared ();
extern "C" void LinkedList_1__ctor_m26968_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26969_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m26970_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26971_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26972_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m26976_gshared ();
extern "C" void LinkedList_1_AddLast_m26977_gshared ();
extern "C" void LinkedList_1_Clear_m26978_gshared ();
extern "C" void LinkedList_1_Contains_m26979_gshared ();
extern "C" void LinkedList_1_CopyTo_m26980_gshared ();
extern "C" void LinkedList_1_Find_m26981_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m26982_gshared ();
void* RuntimeInvoker_Enumerator_t3928 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1_GetObjectData_m26983_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m26984_gshared ();
extern "C" void LinkedList_1_Remove_m26985_gshared ();
extern "C" void LinkedList_1_Remove_m26986_gshared ();
extern "C" void LinkedList_1_RemoveLast_m26987_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26997_gshared ();
extern "C" void Enumerator_get_Current_m26998_gshared ();
extern "C" void Enumerator__ctor_m26996_gshared ();
extern "C" void Enumerator_MoveNext_m26999_gshared ();
extern "C" void Enumerator_Dispose_m27000_gshared ();
extern "C" void LinkedListNode_1_get_List_m26993_gshared ();
extern "C" void LinkedListNode_1_get_Next_m26994_gshared ();
extern "C" void LinkedListNode_1_get_Value_m26995_gshared ();
extern "C" void LinkedListNode_1__ctor_m26990_gshared ();
extern "C" void LinkedListNode_1__ctor_m26991_gshared ();
extern "C" void LinkedListNode_1_Detach_m26992_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m15421_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m15422_gshared ();
extern "C" void Stack_1_get_Count_m15429_gshared ();
extern "C" void Stack_1__ctor_m15420_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m15423_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15424_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m15425_gshared ();
extern "C" void Stack_1_Peek_m15426_gshared ();
extern "C" void Stack_1_Pop_m15427_gshared ();
extern "C" void Stack_1_Push_m15428_gshared ();
extern "C" void Stack_1_GetEnumerator_m15430_gshared ();
void* RuntimeInvoker_Enumerator_t3148 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15432_gshared ();
extern "C" void Enumerator_get_Current_m15435_gshared ();
extern "C" void Enumerator__ctor_m15431_gshared ();
extern "C" void Enumerator_Dispose_m15433_gshared ();
extern "C" void Enumerator_MoveNext_m15434_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m27746_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m27738_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m27741_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m27739_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m27740_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m27743_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m27742_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m27737_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m27745_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_get_swapper_TisObject_t_m27796_gshared ();
extern "C" void Array_Sort_TisObject_t_m28646_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28647_gshared ();
extern "C" void Array_Sort_TisObject_t_m28648_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28649_gshared ();
extern "C" void Array_Sort_TisObject_t_m14001_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m28650_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m27795_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m27794_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m28651_gshared ();
extern "C" void Array_Sort_TisObject_t_m27812_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_TisObject_t_m27797_gshared ();
extern "C" void Array_compare_TisObject_t_m27809_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_m27811_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m27810_gshared ();
extern "C" void Array_swap_TisObject_t_m27813_gshared ();
extern "C" void Array_Resize_TisObject_t_m5535_gshared ();
void* RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisObject_t_m27793_gshared ();
void* RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_TrueForAll_TisObject_t_m28652_gshared ();
extern "C" void Array_ForEach_TisObject_t_m28653_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m28654_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m28656_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m28657_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m28655_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindIndex_TisObject_t_m28659_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m28660_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m28658_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28662_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28663_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28664_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m28661_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14003_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m28665_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14000_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_LastIndexOf_TisObject_t_m28667_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m28666_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m28668_gshared ();
extern "C" void Array_FindAll_TisObject_t_m28669_gshared ();
extern "C" void Array_Exists_TisObject_t_m28670_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m28671_gshared ();
extern "C" void Array_Find_TisObject_t_m28672_gshared ();
extern "C" void Array_FindLast_TisObject_t_m28673_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14866_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14858_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14862_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14864_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m27419_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m27420_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m27421_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m27422_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m27417_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27418_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m27423_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m27424_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m27425_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m27426_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m27427_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m27428_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m27429_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m27430_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m27431_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m27432_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27434_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27435_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m27433_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27436_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27437_gshared ();
extern "C" void Comparer_1_get_Default_m15141_gshared ();
extern "C" void Comparer_1__ctor_m15138_gshared ();
extern "C" void Comparer_1__cctor_m15139_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15140_gshared ();
extern "C" void DefaultComparer__ctor_m15142_gshared ();
extern "C" void DefaultComparer_Compare_m15143_gshared ();
extern "C" void GenericComparer_1__ctor_m27463_gshared ();
extern "C" void GenericComparer_1_Compare_m27464_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16772_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16774_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16776_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16778_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16786_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16788_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16790_gshared ();
extern "C" void Dictionary_2_get_Count_m16808_gshared ();
extern "C" void Dictionary_2_get_Item_m16810_gshared ();
extern "C" void Dictionary_2_set_Item_m16812_gshared ();
extern "C" void Dictionary_2_get_Keys_m16846_gshared ();
extern "C" void Dictionary_2_get_Values_m16848_gshared ();
extern "C" void Dictionary_2__ctor_m16760_gshared ();
extern "C" void Dictionary_2__ctor_m16762_gshared ();
extern "C" void Dictionary_2__ctor_m16764_gshared ();
extern "C" void Dictionary_2__ctor_m16766_gshared ();
extern "C" void Dictionary_2__ctor_m16768_gshared ();
extern "C" void Dictionary_2__ctor_m16770_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16780_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16782_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16784_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16792_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16794_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16798_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16800_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16802_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16804_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16806_gshared ();
extern "C" void Dictionary_2_Init_m16814_gshared ();
extern "C" void Dictionary_2_InitArrays_m16816_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16818_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27943_gshared ();
extern "C" void Dictionary_2_make_pair_m16820_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16822_gshared ();
extern "C" void Dictionary_2_pick_value_m16824_gshared ();
extern "C" void Dictionary_2_CopyTo_m16826_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27944_gshared ();
extern "C" void Dictionary_2_Resize_m16828_gshared ();
extern "C" void Dictionary_2_Add_m16830_gshared ();
extern "C" void Dictionary_2_Clear_m16832_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16834_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16836_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16838_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16840_gshared ();
extern "C" void Dictionary_2_Remove_m16842_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16844_gshared ();
extern "C" void Dictionary_2_ToTKey_m16850_gshared ();
extern "C" void Dictionary_2_ToTValue_m16852_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16854_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16856_gshared ();
void* RuntimeInvoker_Enumerator_t3258 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16858_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Entry_m16978_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Key_m16979_gshared ();
extern "C" void ShimEnumerator_get_Value_m16980_gshared ();
extern "C" void ShimEnumerator_get_Current_m16981_gshared ();
extern "C" void ShimEnumerator__ctor_m16976_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16977_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared ();
extern "C" void Enumerator_get_Current_m16939_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_CurrentKey_m16940_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16941_gshared ();
extern "C" void Enumerator__ctor_m16933_gshared ();
extern "C" void Enumerator_MoveNext_m16938_gshared ();
extern "C" void Enumerator_VerifyState_m16942_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16943_gshared ();
extern "C" void Enumerator_Dispose_m16944_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16922_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16923_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16924_gshared ();
extern "C" void KeyCollection_get_Count_m16927_gshared ();
extern "C" void KeyCollection__ctor_m16914_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16915_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16916_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16917_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16918_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16919_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16920_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16921_gshared ();
extern "C" void KeyCollection_CopyTo_m16925_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16926_gshared ();
void* RuntimeInvoker_Enumerator_t3257 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16929_gshared ();
extern "C" void Enumerator_get_Current_m16932_gshared ();
extern "C" void Enumerator__ctor_m16928_gshared ();
extern "C" void Enumerator_Dispose_m16930_gshared ();
extern "C" void Enumerator_MoveNext_m16931_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16957_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16958_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16959_gshared ();
extern "C" void ValueCollection_get_Count_m16962_gshared ();
extern "C" void ValueCollection__ctor_m16949_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16950_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16951_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16952_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16953_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16954_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16955_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16956_gshared ();
extern "C" void ValueCollection_CopyTo_m16960_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16961_gshared ();
void* RuntimeInvoker_Enumerator_t3261 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16964_gshared ();
extern "C" void Enumerator_get_Current_m16967_gshared ();
extern "C" void Enumerator__ctor_m16963_gshared ();
extern "C" void Enumerator_Dispose_m16965_gshared ();
extern "C" void Enumerator_MoveNext_m16966_gshared ();
extern "C" void Transform_1__ctor_m16945_gshared ();
extern "C" void Transform_1_Invoke_m16946_gshared ();
extern "C" void Transform_1_BeginInvoke_m16947_gshared ();
extern "C" void Transform_1_EndInvoke_m16948_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15125_gshared ();
extern "C" void EqualityComparer_1__ctor_m15121_gshared ();
extern "C" void EqualityComparer_1__cctor_m15122_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15123_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15124_gshared ();
extern "C" void DefaultComparer__ctor_m15131_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15132_gshared ();
extern "C" void DefaultComparer_Equals_m15133_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m27465_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27466_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27467_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16909_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16910_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16911_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16912_gshared ();
extern "C" void KeyValuePair_2__ctor_m16908_gshared ();
extern "C" void KeyValuePair_2_ToString_m16913_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m7195_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m7196_gshared ();
extern "C" void List_1_get_Capacity_m15043_gshared ();
extern "C" void List_1_set_Capacity_m15045_gshared ();
extern "C" void List_1_get_Count_m7189_gshared ();
extern "C" void List_1_get_Item_m7212_gshared ();
extern "C" void List_1_set_Item_m7213_gshared ();
extern "C" void List_1__ctor_m6974_gshared ();
extern "C" void List_1__ctor_m14980_gshared ();
extern "C" void List_1__ctor_m14982_gshared ();
extern "C" void List_1__cctor_m14984_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m7192_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m7197_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m7199_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m7200_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m7201_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m7202_gshared ();
extern "C" void List_1_Add_m7205_gshared ();
extern "C" void List_1_GrowIfNeeded_m15002_gshared ();
extern "C" void List_1_AddCollection_m15004_gshared ();
extern "C" void List_1_AddEnumerable_m15006_gshared ();
extern "C" void List_1_AddRange_m15007_gshared ();
extern "C" void List_1_AsReadOnly_m15009_gshared ();
extern "C" void List_1_Clear_m7198_gshared ();
extern "C" void List_1_Contains_m7206_gshared ();
extern "C" void List_1_CopyTo_m7207_gshared ();
extern "C" void List_1_Find_m15014_gshared ();
extern "C" void List_1_CheckMatch_m15016_gshared ();
extern "C" void List_1_GetIndex_m15018_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GetEnumerator_m15019_gshared ();
void* RuntimeInvoker_Enumerator_t3115 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m7210_gshared ();
extern "C" void List_1_Shift_m15022_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckIndex_m15024_gshared ();
extern "C" void List_1_Insert_m7211_gshared ();
extern "C" void List_1_CheckCollection_m15027_gshared ();
extern "C" void List_1_Remove_m7208_gshared ();
extern "C" void List_1_RemoveAll_m15030_gshared ();
extern "C" void List_1_RemoveAt_m7203_gshared ();
extern "C" void List_1_Reverse_m15033_gshared ();
extern "C" void List_1_Sort_m15035_gshared ();
extern "C" void List_1_Sort_m15037_gshared ();
extern "C" void List_1_ToArray_m15039_gshared ();
extern "C" void List_1_TrimExcess_m15041_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared ();
extern "C" void Enumerator_get_Current_m15054_gshared ();
extern "C" void Enumerator__ctor_m15049_gshared ();
extern "C" void Enumerator_Dispose_m15051_gshared ();
extern "C" void Enumerator_VerifyState_m15052_gshared ();
extern "C" void Enumerator_MoveNext_m15053_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15086_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15094_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15095_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15096_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15097_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15098_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15099_gshared ();
extern "C" void Collection_1_get_Count_m15112_gshared ();
extern "C" void Collection_1_get_Item_m15113_gshared ();
extern "C" void Collection_1_set_Item_m15114_gshared ();
extern "C" void Collection_1__ctor_m15085_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15087_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15088_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15089_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15090_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15091_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15092_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15093_gshared ();
extern "C" void Collection_1_Add_m15100_gshared ();
extern "C" void Collection_1_Clear_m15101_gshared ();
extern "C" void Collection_1_ClearItems_m15102_gshared ();
extern "C" void Collection_1_Contains_m15103_gshared ();
extern "C" void Collection_1_CopyTo_m15104_gshared ();
extern "C" void Collection_1_GetEnumerator_m15105_gshared ();
extern "C" void Collection_1_IndexOf_m15106_gshared ();
extern "C" void Collection_1_Insert_m15107_gshared ();
extern "C" void Collection_1_InsertItem_m15108_gshared ();
extern "C" void Collection_1_Remove_m15109_gshared ();
extern "C" void Collection_1_RemoveAt_m15110_gshared ();
extern "C" void Collection_1_RemoveItem_m15111_gshared ();
extern "C" void Collection_1_SetItem_m15115_gshared ();
extern "C" void Collection_1_IsValidItem_m15116_gshared ();
extern "C" void Collection_1_ConvertItem_m15117_gshared ();
extern "C" void Collection_1_CheckWritable_m15118_gshared ();
extern "C" void Collection_1_IsSynchronized_m15119_gshared ();
extern "C" void Collection_1_IsFixedSize_m15120_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15061_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15062_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15063_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15073_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15074_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15075_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15076_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15077_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15078_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15083_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15084_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15055_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15056_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15057_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15058_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15059_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15060_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15064_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15065_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15066_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15067_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15068_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15069_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15070_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15071_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15072_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15079_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15080_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15081_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15082_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28707_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28708_gshared ();
extern "C" void Getter_2__ctor_m27523_gshared ();
extern "C" void Getter_2_Invoke_m27524_gshared ();
extern "C" void Getter_2_BeginInvoke_m27525_gshared ();
extern "C" void Getter_2_EndInvoke_m27526_gshared ();
extern "C" void StaticGetter_1__ctor_m27527_gshared ();
extern "C" void StaticGetter_1_Invoke_m27528_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m27529_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m27530_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m27815_gshared ();
extern "C" void Action_1__ctor_m14968_gshared ();
extern "C" void Action_1_Invoke_m14970_gshared ();
extern "C" void Action_1_BeginInvoke_m14972_gshared ();
extern "C" void Action_1_EndInvoke_m14974_gshared ();
extern "C" void Comparison_1__ctor_m15149_gshared ();
extern "C" void Comparison_1_Invoke_m15150_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15151_gshared ();
extern "C" void Comparison_1_EndInvoke_m15152_gshared ();
extern "C" void Converter_2__ctor_m27413_gshared ();
extern "C" void Converter_2_Invoke_m27414_gshared ();
extern "C" void Converter_2_BeginInvoke_m27415_gshared ();
extern "C" void Converter_2_EndInvoke_m27416_gshared ();
extern "C" void Predicate_1__ctor_m15134_gshared ();
extern "C" void Predicate_1_Invoke_m15135_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15136_gshared ();
extern "C" void Predicate_1_EndInvoke_m15137_gshared ();
extern "C" void Nullable_1__ctor_m14897_gshared ();
void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1__ctor_m14908_gshared ();
extern "C" void DOGetter_1__ctor_m361_gshared ();
extern "C" void DOSetter_1__ctor_m362_gshared ();
extern "C" void Action_1__ctor_m14961_gshared ();
extern "C" void Comparison_1__ctor_m1947_gshared ();
extern "C" void List_1_Sort_m1952_gshared ();
extern "C" void List_1__ctor_m1992_gshared ();
extern "C" void Dictionary_2__ctor_m16127_gshared ();
extern "C" void Dictionary_2_get_Values_m16214_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16287_gshared ();
void* RuntimeInvoker_Enumerator_t3205 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m16293_gshared ();
extern "C" void Enumerator_MoveNext_m16292_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16221_gshared ();
void* RuntimeInvoker_Enumerator_t3202 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m16265_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3197 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Value_m16232_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16230_gshared ();
extern "C" void Enumerator_MoveNext_m16264_gshared ();
extern "C" void KeyValuePair_2_ToString_m16234_gshared ();
extern "C" void Comparison_1__ctor_m2051_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t94_m2048_gshared ();
extern "C" void UnityEvent_1__ctor_m2055_gshared ();
extern "C" void UnityEvent_1_Invoke_m2057_gshared ();
void* RuntimeInvoker_Void_t168_Color_t90 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m2058_gshared ();
extern "C" void TweenRunner_1__ctor_m2086_gshared ();
extern "C" void TweenRunner_1_Init_m2087_gshared ();
extern "C" void UnityAction_1__ctor_m2114_gshared ();
extern "C" void TweenRunner_1_StartTween_m2115_gshared ();
void* RuntimeInvoker_Void_t168_ColorTween_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_get_Capacity_m2119_gshared ();
extern "C" void List_1_set_Capacity_m2120_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t127_m2140_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisByte_t449_m2142_gshared ();
void* RuntimeInvoker_Boolean_t169_ByteU26_t1840_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t151_m2144_gshared ();
void* RuntimeInvoker_Boolean_t169_SingleU26_t924_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m2201_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisUInt16_t454_m2197_gshared ();
void* RuntimeInvoker_Boolean_t169_UInt16U26_t2720_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ToArray_m2256_gshared ();
extern "C" void UnityEvent_1__ctor_m2289_gshared ();
extern "C" void UnityEvent_1_Invoke_m2295_gshared ();
void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m2300_gshared ();
extern "C" void UnityAction_1__ctor_m2301_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m2302_gshared ();
extern "C" void UnityEvent_1_AddListener_m2303_gshared ();
extern "C" void UnityEvent_1_Invoke_m2309_gshared ();
void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t320_m2324_gshared ();
void* RuntimeInvoker_Boolean_t169_NavigationU26_t4540_Navigation_t320 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t265_m2326_gshared ();
void* RuntimeInvoker_Boolean_t169_ColorBlockU26_t4541_ColorBlock_t265 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t339_m2327_gshared ();
void* RuntimeInvoker_Boolean_t169_SpriteStateU26_t4542_SpriteState_t339 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m18023_gshared ();
extern "C" void UnityEvent_1_Invoke_m18032_gshared ();
extern "C" void Func_2__ctor_m18136_gshared ();
extern "C" void LayoutGroup_SetProperty_TisInt32_t127_m2386_gshared ();
void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisVector2_t10_m2388_gshared ();
void* RuntimeInvoker_Void_t168_Vector2U26_t923_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisSingle_t151_m2395_gshared ();
void* RuntimeInvoker_Void_t168_SingleU26_t924_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisByte_t449_m2397_gshared ();
void* RuntimeInvoker_Void_t168_ByteU26_t1840_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m18250_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2569_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2570_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2578_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2579_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2580_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2581_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m18028_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18029_gshared ();
extern "C" void Action_1__ctor_m18483_gshared ();
extern "C" void List_1__ctor_m4477_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16203_gshared ();
extern "C" void LinkedList_1__ctor_m4408_gshared ();
extern "C" void LinkedList_1_AddLast_m4417_gshared ();
extern "C" void List_1__ctor_m4418_gshared ();
extern "C" void List_1_GetEnumerator_m4419_gshared ();
void* RuntimeInvoker_Enumerator_t816 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m4420_gshared ();
extern "C" void Predicate_1__ctor_m4421_gshared ();
extern "C" void Array_Exists_TisTrackableResultData_t642_m4405_gshared ();
extern "C" void Enumerator_MoveNext_m4422_gshared ();
extern "C" void LinkedList_1_get_First_m4423_gshared ();
extern "C" void LinkedListNode_1_get_Value_m4424_gshared ();
extern "C" void Dictionary_2__ctor_m20021_gshared ();
extern "C" void Dictionary_2_get_Keys_m16213_gshared ();
extern "C" void Enumerable_ToList_TisInt32_t127_m4457_gshared ();
extern "C" void Enumerable_ToArray_TisInt32_t127_m4553_gshared ();
extern "C" void LinkedListNode_1_get_Next_m4559_gshared ();
extern "C" void LinkedList_1_Remove_m4560_gshared ();
extern "C" void Dictionary_2__ctor_m4561_gshared ();
extern "C" void Dictionary_2__ctor_m4562_gshared ();
extern "C" void List_1__ctor_m4576_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5507_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5508_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m14955_gshared ();
extern "C" void DOGetter_1__ctor_m5521_gshared ();
extern "C" void DOSetter_1__ctor_m5522_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m23611_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5531_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5532_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5533_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5542_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5549_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5550_gshared ();
extern "C" void Nullable_1_get_HasValue_m14898_gshared ();
extern "C" void Nullable_1_get_Value_m14899_gshared ();
void* RuntimeInvoker_Byte_t449 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m14909_gshared ();
extern "C" void Nullable_1_get_Value_m14910_gshared ();
extern "C" void DOTween_ApplyTo_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m5551_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOTween_ApplyTo_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m5552_gshared ();
extern "C" void Array_IndexOf_TisUInt16_t454_m5560_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void ABSTweenPlugin_3__ctor_m23701_gshared ();
extern "C" void List_1__ctor_m23722_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5579_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5582_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5583_gshared ();
extern "C" void ABSTweenPlugin_3__ctor_m5584_gshared ();
extern "C" void List_1__ctor_m6948_gshared ();
extern "C" void List_1__ctor_m6949_gshared ();
extern "C" void List_1__ctor_m6950_gshared ();
extern "C" void Dictionary_2__ctor_m25583_gshared ();
extern "C" void Dictionary_2__ctor_m26317_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m7032_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m7033_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m26742_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m16524_gshared ();
extern "C" void Dictionary_2__ctor_m27003_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t127_m9370_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m14005_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14006_gshared ();
extern "C" void GenericComparer_1__ctor_m14007_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14008_gshared ();
extern "C" void Nullable_1__ctor_m14009_gshared ();
void* RuntimeInvoker_Void_t168_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m14010_gshared ();
extern "C" void Nullable_1_get_Value_m14011_gshared ();
void* RuntimeInvoker_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m14012_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14013_gshared ();
extern "C" void GenericComparer_1__ctor_m14014_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14015_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t454_m27748_gshared ();
void* RuntimeInvoker_UInt16_t454_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t454_m27749_gshared ();
void* RuntimeInvoker_Void_t168_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t454_m27750_gshared ();
void* RuntimeInvoker_Boolean_t169_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t454_m27751_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t454_m27752_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t454_m27753_gshared ();
void* RuntimeInvoker_Int32_t127_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt16_t454_m27754_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt16_t454_m27756_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t454_m27757_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t127_m27759_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t127_m27760_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t127_m27761_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t127_m27762_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t127_m27763_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t127_m27764_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t127_m27765_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t127_m27767_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t127_m27768_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27770_gshared ();
void* RuntimeInvoker_Single_t151_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void Tweener_DoStartup_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27773_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27771_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27774_gshared ();
extern "C" void Tweener_DoStartup_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27777_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27775_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27778_gshared ();
extern "C" void Tweener_DoStartup_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27781_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27779_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t15_m27783_gshared ();
void* RuntimeInvoker_Vector3_t15_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t15_m27784_gshared ();
void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t15_m27785_gshared ();
void* RuntimeInvoker_Boolean_t169_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t15_m27786_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t15_m27787_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t15_m27788_gshared ();
void* RuntimeInvoker_Int32_t127_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector3_t15_m27789_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector3_t15_m27791_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t15_m27792_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27780_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27776_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27772_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t1407_m27799_gshared ();
void* RuntimeInvoker_Double_t1407_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t1407_m27800_gshared ();
void* RuntimeInvoker_Void_t168_Double_t1407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t1407_m27801_gshared ();
void* RuntimeInvoker_Boolean_t169_Double_t1407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t1407_m27802_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t1407_m27803_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t1407_m27804_gshared ();
void* RuntimeInvoker_Int32_t127_Double_t1407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDouble_t1407_m27805_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Double_t1407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDouble_t1407_m27807_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1407_m27808_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t231_m27820_gshared ();
void* RuntimeInvoker_RaycastResult_t231_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t231_m27821_gshared ();
void* RuntimeInvoker_Void_t168_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t231_m27822_gshared ();
void* RuntimeInvoker_Boolean_t169_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t231_m27823_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t231_m27824_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t231_m27825_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t231_m27826_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t231_m27828_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t231_m27829_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t231_m27831_gshared ();
void* RuntimeInvoker_Void_t168_RaycastResultU5BU5DU26_t4543_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisRaycastResult_t231_m27830_gshared ();
void* RuntimeInvoker_Void_t168_RaycastResultU5BU5DU26_t4543_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisRaycastResult_t231_m27832_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_RaycastResult_t231_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisRaycastResult_t231_m27834_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t231_TisRaycastResult_t231_m27833_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t231_m27835_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t231_TisRaycastResult_t231_m27836_gshared ();
extern "C" void Array_compare_TisRaycastResult_t231_m27837_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastResult_t231_RaycastResult_t231_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisRaycastResult_t231_TisRaycastResult_t231_m27838_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t231_m27840_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t231_m27839_gshared ();
extern "C" void Array_swap_TisRaycastResult_t231_m27841_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3197_m27845_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3197_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3197_m27846_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3197_m27847_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3197_m27848_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3197_m27849_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3197_m27850_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3197_m27851_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3197_m27853_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3197_m27854_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t2136_m27856_gshared ();
void* RuntimeInvoker_Link_t2136_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t2136_m27857_gshared ();
void* RuntimeInvoker_Void_t168_Link_t2136 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t2136_m27858_gshared ();
void* RuntimeInvoker_Boolean_t169_Link_t2136 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t2136_m27859_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t2136_m27860_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t2136_m27861_gshared ();
void* RuntimeInvoker_Int32_t127_Link_t2136 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t2136_m27862_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Link_t2136 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t2136_m27864_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t2136_m27865_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m27867_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m27866_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m27868_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27870_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27869_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t1996_m27872_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1996_m27873_gshared ();
void* RuntimeInvoker_Void_t168_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1996_m27874_gshared ();
void* RuntimeInvoker_Boolean_t169_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1996_m27875_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1996_m27876_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t1996_m27877_gshared ();
void* RuntimeInvoker_Int32_t127_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t1996_m27878_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t1996_m27880_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1996_m27881_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27882_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3197_m27884_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3197_TisObject_t_m27883_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3197_TisKeyValuePair_2_t3197_m27885_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t432_m27887_gshared ();
void* RuntimeInvoker_RaycastHit2D_t432_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t432_m27888_gshared ();
void* RuntimeInvoker_Void_t168_RaycastHit2D_t432 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t432_m27889_gshared ();
void* RuntimeInvoker_Boolean_t169_RaycastHit2D_t432 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t432_m27890_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t432_m27891_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t432_m27892_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastHit2D_t432 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t432_m27893_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_RaycastHit2D_t432 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t432_m27895_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t432_m27896_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t94_m27898_gshared ();
void* RuntimeInvoker_RaycastHit_t94_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t94_m27899_gshared ();
void* RuntimeInvoker_Void_t168_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t94_m27900_gshared ();
void* RuntimeInvoker_Boolean_t169_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t94_m27901_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t94_m27902_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t94_m27903_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t94_m27904_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t94_m27906_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t94_m27907_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t94_m27908_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t94_m27909_gshared ();
extern "C" void Array_swap_TisRaycastHit_t94_m27910_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t90_m27911_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3228_m27913_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3228_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3228_m27914_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3228_m27915_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3228_m27916_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3228_m27917_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3228_m27918_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3228_m27919_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3228_m27921_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3228_m27922_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27924_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27923_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m27926_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m27925_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m27927_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27928_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3228_m27930_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3228_TisObject_t_m27929_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3228_TisKeyValuePair_2_t3228_m27931_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3254_m27933_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3254_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3254_m27934_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3254_m27935_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3254_m27936_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3254_m27937_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3254_m27938_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3254_m27939_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3254_m27941_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3254_m27942_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27945_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3254_m27947_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisObject_t_m27946_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisKeyValuePair_2_t3254_m27948_gshared ();
extern "C" void Array_Resize_TisUIVertex_t313_m27950_gshared ();
void* RuntimeInvoker_Void_t168_UIVertexU5BU5DU26_t4544_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUIVertex_t313_m27949_gshared ();
void* RuntimeInvoker_Void_t168_UIVertexU5BU5DU26_t4544_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUIVertex_t313_m27951_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_UIVertex_t313_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUIVertex_t313_m27953_gshared ();
extern "C" void Array_Sort_TisUIVertex_t313_TisUIVertex_t313_m27952_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t313_m27954_gshared ();
extern "C" void Array_qsort_TisUIVertex_t313_TisUIVertex_t313_m27955_gshared ();
extern "C" void Array_compare_TisUIVertex_t313_m27956_gshared ();
void* RuntimeInvoker_Int32_t127_UIVertex_t313_UIVertex_t313_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUIVertex_t313_TisUIVertex_t313_m27957_gshared ();
extern "C" void Array_Sort_TisUIVertex_t313_m27959_gshared ();
extern "C" void Array_qsort_TisUIVertex_t313_m27958_gshared ();
extern "C" void Array_swap_TisUIVertex_t313_m27960_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t10_m27962_gshared ();
void* RuntimeInvoker_Vector2_t10_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t10_m27963_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t10_m27964_gshared ();
void* RuntimeInvoker_Boolean_t169_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t10_m27965_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t10_m27966_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t10_m27967_gshared ();
void* RuntimeInvoker_Int32_t127_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector2_t10_m27968_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector2_t10_m27970_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t10_m27971_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t458_m27973_gshared ();
void* RuntimeInvoker_UILineInfo_t458_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t458_m27974_gshared ();
void* RuntimeInvoker_Void_t168_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t458_m27975_gshared ();
void* RuntimeInvoker_Boolean_t169_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t458_m27976_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t458_m27977_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t458_m27978_gshared ();
void* RuntimeInvoker_Int32_t127_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t458_m27979_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t458_m27981_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t458_m27982_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t460_m27984_gshared ();
void* RuntimeInvoker_UICharInfo_t460_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t460_m27985_gshared ();
void* RuntimeInvoker_Void_t168_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t460_m27986_gshared ();
void* RuntimeInvoker_Boolean_t169_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t460_m27987_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t460_m27988_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t460_m27989_gshared ();
void* RuntimeInvoker_Int32_t127_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t460_m27990_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t460_m27992_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t460_m27993_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t151_m27994_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t10_m27995_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisByte_t449_m27996_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t151_m28001_gshared ();
void* RuntimeInvoker_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t151_m28002_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t151_m28003_gshared ();
void* RuntimeInvoker_Boolean_t169_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t151_m28004_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t151_m28005_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t151_m28006_gshared ();
void* RuntimeInvoker_Int32_t127_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSingle_t151_m28007_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSingle_t151_m28009_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t151_m28010_gshared ();
extern "C" void Array_InternalArray__get_Item_TisEyewearCalibrationReading_t586_m28012_gshared ();
void* RuntimeInvoker_EyewearCalibrationReading_t586_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t586_m28013_gshared ();
void* RuntimeInvoker_Void_t168_EyewearCalibrationReading_t586 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t586_m28014_gshared ();
void* RuntimeInvoker_Boolean_t169_EyewearCalibrationReading_t586 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t586_m28015_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t586_m28016_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t586_m28017_gshared ();
void* RuntimeInvoker_Int32_t127_EyewearCalibrationReading_t586 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisEyewearCalibrationReading_t586_m28018_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_EyewearCalibrationReading_t586 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisEyewearCalibrationReading_t586_m28020_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t586_m28021_gshared ();
extern "C" void Array_Resize_TisInt32_t127_m28024_gshared ();
void* RuntimeInvoker_Void_t168_Int32U5BU5DU26_t4545_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisInt32_t127_m28023_gshared ();
void* RuntimeInvoker_Void_t168_Int32U5BU5DU26_t4545_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisInt32_t127_m28025_gshared ();
extern "C" void Array_Sort_TisInt32_t127_m28027_gshared ();
extern "C" void Array_Sort_TisInt32_t127_TisInt32_t127_m28026_gshared ();
extern "C" void Array_get_swapper_TisInt32_t127_m28028_gshared ();
extern "C" void Array_qsort_TisInt32_t127_TisInt32_t127_m28029_gshared ();
extern "C" void Array_compare_TisInt32_t127_m28030_gshared ();
extern "C" void Array_swap_TisInt32_t127_TisInt32_t127_m28031_gshared ();
extern "C" void Array_Sort_TisInt32_t127_m28033_gshared ();
extern "C" void Array_qsort_TisInt32_t127_m28032_gshared ();
extern "C" void Array_swap_TisInt32_t127_m28034_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t449_m28036_gshared ();
void* RuntimeInvoker_Byte_t449_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t449_m28037_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t449_m28038_gshared ();
void* RuntimeInvoker_Boolean_t169_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t449_m28039_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t449_m28040_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t449_m28041_gshared ();
void* RuntimeInvoker_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisByte_t449_m28042_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisByte_t449_m28044_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t449_m28045_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor32_t421_m28047_gshared ();
void* RuntimeInvoker_Color32_t421_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisColor32_t421_m28048_gshared ();
void* RuntimeInvoker_Void_t168_Color32_t421 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisColor32_t421_m28049_gshared ();
void* RuntimeInvoker_Boolean_t169_Color32_t421 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor32_t421_m28050_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor32_t421_m28051_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor32_t421_m28052_gshared ();
void* RuntimeInvoker_Int32_t127_Color32_t421 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisColor32_t421_m28053_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Color32_t421 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisColor32_t421_m28055_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t421_m28056_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor_t90_m28058_gshared ();
void* RuntimeInvoker_Color_t90_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisColor_t90_m28059_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisColor_t90_m28060_gshared ();
void* RuntimeInvoker_Boolean_t169_Color_t90 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor_t90_m28061_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor_t90_m28062_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor_t90_m28063_gshared ();
void* RuntimeInvoker_Int32_t127_Color_t90 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisColor_t90_m28064_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Color_t90 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisColor_t90_m28066_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t90_m28067_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTrackableResultData_t642_m28072_gshared ();
void* RuntimeInvoker_TrackableResultData_t642_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTrackableResultData_t642_m28073_gshared ();
void* RuntimeInvoker_Void_t168_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTrackableResultData_t642_m28074_gshared ();
void* RuntimeInvoker_Boolean_t169_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t642_m28075_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTrackableResultData_t642_m28076_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTrackableResultData_t642_m28077_gshared ();
void* RuntimeInvoker_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTrackableResultData_t642_m28078_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTrackableResultData_t642_m28080_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t642_m28081_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordData_t647_m28083_gshared ();
void* RuntimeInvoker_WordData_t647_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWordData_t647_m28084_gshared ();
void* RuntimeInvoker_Void_t168_WordData_t647 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWordData_t647_m28085_gshared ();
void* RuntimeInvoker_Boolean_t169_WordData_t647 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordData_t647_m28086_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordData_t647_m28087_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordData_t647_m28088_gshared ();
void* RuntimeInvoker_Int32_t127_WordData_t647 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWordData_t647_m28089_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_WordData_t647 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWordData_t647_m28091_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t647_m28092_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWordResultData_t646_m28094_gshared ();
void* RuntimeInvoker_WordResultData_t646_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWordResultData_t646_m28095_gshared ();
void* RuntimeInvoker_Void_t168_WordResultData_t646 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWordResultData_t646_m28096_gshared ();
void* RuntimeInvoker_Boolean_t169_WordResultData_t646 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWordResultData_t646_m28097_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWordResultData_t646_m28098_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWordResultData_t646_m28099_gshared ();
void* RuntimeInvoker_Int32_t127_WordResultData_t646 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWordResultData_t646_m28100_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_WordResultData_t646 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWordResultData_t646_m28102_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t646_m28103_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t650_m28105_gshared ();
void* RuntimeInvoker_SmartTerrainRevisionData_t650_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t650_m28106_gshared ();
void* RuntimeInvoker_Void_t168_SmartTerrainRevisionData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t650_m28107_gshared ();
void* RuntimeInvoker_Boolean_t169_SmartTerrainRevisionData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t650_m28108_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t650_m28109_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t650_m28110_gshared ();
void* RuntimeInvoker_Int32_t127_SmartTerrainRevisionData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSmartTerrainRevisionData_t650_m28111_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_SmartTerrainRevisionData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t650_m28113_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t650_m28114_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSurfaceData_t651_m28116_gshared ();
void* RuntimeInvoker_SurfaceData_t651_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSurfaceData_t651_m28117_gshared ();
void* RuntimeInvoker_Void_t168_SurfaceData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSurfaceData_t651_m28118_gshared ();
void* RuntimeInvoker_Boolean_t169_SurfaceData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t651_m28119_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSurfaceData_t651_m28120_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSurfaceData_t651_m28121_gshared ();
void* RuntimeInvoker_Int32_t127_SurfaceData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSurfaceData_t651_m28122_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_SurfaceData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSurfaceData_t651_m28124_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t651_m28125_gshared ();
extern "C" void Array_InternalArray__get_Item_TisPropData_t652_m28127_gshared ();
void* RuntimeInvoker_PropData_t652_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisPropData_t652_m28128_gshared ();
void* RuntimeInvoker_Void_t168_PropData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisPropData_t652_m28129_gshared ();
void* RuntimeInvoker_Boolean_t169_PropData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisPropData_t652_m28130_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisPropData_t652_m28131_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisPropData_t652_m28132_gshared ();
void* RuntimeInvoker_Int32_t127_PropData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisPropData_t652_m28133_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_PropData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisPropData_t652_m28135_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t652_m28136_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3462_m28139_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3462_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3462_m28140_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3462 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3462_m28141_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3462 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3462_m28142_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3462_m28143_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3462_m28144_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3462 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3462_m28145_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3462 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3462_m28147_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3462_m28148_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28150_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28149_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t454_m28152_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t454_TisObject_t_m28151_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt16_t454_TisUInt16_t454_m28153_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28154_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3462_m28156_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3462_TisObject_t_m28155_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3462_TisKeyValuePair_2_t3462_m28157_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRectangleData_t596_m28159_gshared ();
void* RuntimeInvoker_RectangleData_t596_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRectangleData_t596_m28160_gshared ();
void* RuntimeInvoker_Void_t168_RectangleData_t596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRectangleData_t596_m28161_gshared ();
void* RuntimeInvoker_Boolean_t169_RectangleData_t596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRectangleData_t596_m28162_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRectangleData_t596_m28163_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRectangleData_t596_m28164_gshared ();
void* RuntimeInvoker_Int32_t127_RectangleData_t596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRectangleData_t596_m28165_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_RectangleData_t596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRectangleData_t596_m28167_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t596_m28168_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3550_m28170_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3550_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3550_m28171_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3550 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3550_m28172_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3550 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3550_m28173_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3550_m28174_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3550_m28175_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3550 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3550_m28176_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3550 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3550_m28178_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3550_m28179_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28181_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28180_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28182_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t642_m28184_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t642_TisObject_t_m28183_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisTrackableResultData_t642_TisTrackableResultData_t642_m28185_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28186_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3550_m28188_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3550_TisObject_t_m28187_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3550_TisKeyValuePair_2_t3550_m28189_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3565_m28191_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3565_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3565_m28192_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3565 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3565_m28193_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3565 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3565_m28194_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3565_m28195_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3565_m28196_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3565 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3565_m28197_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3565 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3565_m28199_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3565_m28200_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVirtualButtonData_t643_m28202_gshared ();
void* RuntimeInvoker_VirtualButtonData_t643_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVirtualButtonData_t643_m28203_gshared ();
void* RuntimeInvoker_Void_t168_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t643_m28204_gshared ();
void* RuntimeInvoker_Boolean_t169_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t643_m28205_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t643_m28206_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVirtualButtonData_t643_m28207_gshared ();
void* RuntimeInvoker_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVirtualButtonData_t643_m28208_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVirtualButtonData_t643_m28210_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t643_m28211_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28213_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28212_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28214_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t643_m28216_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t643_TisObject_t_m28215_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisVirtualButtonData_t643_TisVirtualButtonData_t643_m28217_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28218_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3565_m28220_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3565_TisObject_t_m28219_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3565_TisKeyValuePair_2_t3565_m28221_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTargetSearchResult_t720_m28223_gshared ();
void* RuntimeInvoker_TargetSearchResult_t720_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTargetSearchResult_t720_m28224_gshared ();
void* RuntimeInvoker_Void_t168_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t720_m28225_gshared ();
void* RuntimeInvoker_Boolean_t169_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t720_m28226_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t720_m28227_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTargetSearchResult_t720_m28228_gshared ();
void* RuntimeInvoker_Int32_t127_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTargetSearchResult_t720_m28229_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTargetSearchResult_t720_m28231_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t720_m28232_gshared ();
extern "C" void Array_Resize_TisTargetSearchResult_t720_m28234_gshared ();
void* RuntimeInvoker_Void_t168_TargetSearchResultU5BU5DU26_t4546_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisTargetSearchResult_t720_m28233_gshared ();
void* RuntimeInvoker_Void_t168_TargetSearchResultU5BU5DU26_t4546_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisTargetSearchResult_t720_m28235_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_TargetSearchResult_t720_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisTargetSearchResult_t720_m28237_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28236_gshared ();
extern "C" void Array_get_swapper_TisTargetSearchResult_t720_m28238_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28239_gshared ();
extern "C" void Array_compare_TisTargetSearchResult_t720_m28240_gshared ();
void* RuntimeInvoker_Int32_t127_TargetSearchResult_t720_TargetSearchResult_t720_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28241_gshared ();
extern "C" void Array_Sort_TisTargetSearchResult_t720_m28243_gshared ();
extern "C" void Array_qsort_TisTargetSearchResult_t720_m28242_gshared ();
extern "C" void Array_swap_TisTargetSearchResult_t720_m28244_gshared ();
extern "C" void Array_InternalArray__get_Item_TisWebCamDevice_t879_m28249_gshared ();
void* RuntimeInvoker_WebCamDevice_t879_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisWebCamDevice_t879_m28250_gshared ();
void* RuntimeInvoker_Void_t168_WebCamDevice_t879 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisWebCamDevice_t879_m28251_gshared ();
void* RuntimeInvoker_Boolean_t169_WebCamDevice_t879 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t879_m28252_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisWebCamDevice_t879_m28253_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisWebCamDevice_t879_m28254_gshared ();
void* RuntimeInvoker_Int32_t127_WebCamDevice_t879 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisWebCamDevice_t879_m28255_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_WebCamDevice_t879 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisWebCamDevice_t879_m28257_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t879_m28258_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3604_m28260_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3604_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3604_m28261_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3604 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3604_m28262_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3604 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3604_m28263_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3604_m28264_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3604_m28265_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3604 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3604_m28266_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3604 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3604_m28268_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3604_m28269_gshared ();
extern "C" void Array_InternalArray__get_Item_TisProfileData_t734_m28271_gshared ();
void* RuntimeInvoker_ProfileData_t734_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisProfileData_t734_m28272_gshared ();
void* RuntimeInvoker_Void_t168_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisProfileData_t734_m28273_gshared ();
void* RuntimeInvoker_Boolean_t169_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisProfileData_t734_m28274_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisProfileData_t734_m28275_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisProfileData_t734_m28276_gshared ();
void* RuntimeInvoker_Int32_t127_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisProfileData_t734_m28277_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisProfileData_t734_m28279_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t734_m28280_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28282_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28281_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t734_m28284_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t734_TisObject_t_m28283_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t734_TisProfileData_t734_m28285_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28286_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3604_m28288_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3604_TisObject_t_m28287_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3604_TisKeyValuePair_2_t3604_m28289_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t3653_m28291_gshared ();
void* RuntimeInvoker_Link_t3653_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t3653_m28292_gshared ();
void* RuntimeInvoker_Void_t168_Link_t3653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t3653_m28293_gshared ();
void* RuntimeInvoker_Boolean_t169_Link_t3653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t3653_m28294_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t3653_m28295_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t3653_m28296_gshared ();
void* RuntimeInvoker_Int32_t127_Link_t3653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t3653_m28297_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Link_t3653 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t3653_m28299_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3653_m28300_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28301_gshared ();
extern "C" void Tweener_DoStartup_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28304_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28302_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28303_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28305_gshared ();
extern "C" void Tweener_DoStartup_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28308_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28306_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28307_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t933_m28309_gshared ();
extern "C" void Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t933_m28312_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisNoOptions_t933_m28310_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisNoOptions_t933_m28311_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28313_gshared ();
extern "C" void Tweener_DoStartup_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28316_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28314_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28315_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28317_gshared ();
extern "C" void Tweener_DoStartup_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28320_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28318_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28319_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28321_gshared ();
extern "C" void Tweener_DoStartup_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28324_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28322_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28323_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28325_gshared ();
extern "C" void Tweener_DoStartup_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28328_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28326_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28327_gshared ();
extern "C" void TweenManager_GetTweener_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28329_gshared ();
extern "C" void Tweener_Setup_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28330_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t_Vector3_t15_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void PluginsManager_GetDefaultPlugin_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28331_gshared ();
extern "C" void TweenManager_GetTweener_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28332_gshared ();
extern "C" void Tweener_Setup_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28333_gshared ();
extern "C" void PluginsManager_GetDefaultPlugin_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28334_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1003_m28335_gshared ();
extern "C" void Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1003_m28338_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisStringOptions_t1003_m28336_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisStringOptions_t1003_m28337_gshared ();
extern "C" void Array_Resize_TisUInt16_t454_m28340_gshared ();
void* RuntimeInvoker_Void_t168_UInt16U5BU5DU26_t4547_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUInt16_t454_m28339_gshared ();
void* RuntimeInvoker_Void_t168_UInt16U5BU5DU26_t4547_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUInt16_t454_m28341_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int16_t534_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUInt16_t454_m28343_gshared ();
extern "C" void Array_Sort_TisUInt16_t454_TisUInt16_t454_m28342_gshared ();
extern "C" void Array_get_swapper_TisUInt16_t454_m28344_gshared ();
extern "C" void Array_qsort_TisUInt16_t454_TisUInt16_t454_m28345_gshared ();
extern "C" void Array_compare_TisUInt16_t454_m28346_gshared ();
void* RuntimeInvoker_Int32_t127_Int16_t534_Int16_t534_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUInt16_t454_TisUInt16_t454_m28347_gshared ();
extern "C" void Array_Sort_TisUInt16_t454_m28349_gshared ();
extern "C" void Array_qsort_TisUInt16_t454_m28348_gshared ();
extern "C" void Array_swap_TisUInt16_t454_m28350_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28351_gshared ();
extern "C" void Tweener_DoStartup_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28354_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28352_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28353_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28355_gshared ();
extern "C" void Tweener_DoStartup_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28358_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28356_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28357_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28359_gshared ();
extern "C" void Tweener_DoStartup_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28362_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28360_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28361_gshared ();
extern "C" void Tweener_DoUpdateDelay_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28363_gshared ();
extern "C" void Tweener_DoStartup_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28366_gshared ();
extern "C" void Tweener_DOStartupSpecials_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28364_gshared ();
extern "C" void Tweener_DOStartupDurationBased_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28365_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t1305_m28369_gshared ();
void* RuntimeInvoker_GcAchievementData_t1305_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t1305_m28370_gshared ();
void* RuntimeInvoker_Void_t168_GcAchievementData_t1305 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1305_m28371_gshared ();
void* RuntimeInvoker_Boolean_t169_GcAchievementData_t1305 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1305_m28372_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1305_m28373_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t1305_m28374_gshared ();
void* RuntimeInvoker_Int32_t127_GcAchievementData_t1305 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t1305_m28375_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_GcAchievementData_t1305 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t1305_m28377_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1305_m28378_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t1306_m28380_gshared ();
void* RuntimeInvoker_GcScoreData_t1306_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t1306_m28381_gshared ();
void* RuntimeInvoker_Void_t168_GcScoreData_t1306 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t1306_m28382_gshared ();
void* RuntimeInvoker_Boolean_t169_GcScoreData_t1306 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1306_m28383_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t1306_m28384_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t1306_m28385_gshared ();
void* RuntimeInvoker_Int32_t127_GcScoreData_t1306 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t1306_m28386_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_GcScoreData_t1306 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t1306_m28388_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1306_m28389_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m28391_gshared ();
void* RuntimeInvoker_IntPtr_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m28392_gshared ();
void* RuntimeInvoker_Void_t168_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m28393_gshared ();
void* RuntimeInvoker_Boolean_t169_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m28394_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m28395_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m28396_gshared ();
void* RuntimeInvoker_Int32_t127_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m28397_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m28399_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m28400_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t1236_m28402_gshared ();
void* RuntimeInvoker_Keyframe_t1236_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t1236_m28403_gshared ();
void* RuntimeInvoker_Void_t168_Keyframe_t1236 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t1236_m28404_gshared ();
void* RuntimeInvoker_Boolean_t169_Keyframe_t1236 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1236_m28405_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t1236_m28406_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t1236_m28407_gshared ();
void* RuntimeInvoker_Int32_t127_Keyframe_t1236 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyframe_t1236_m28408_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Keyframe_t1236 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t1236_m28410_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1236_m28411_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t460_m28413_gshared ();
void* RuntimeInvoker_Void_t168_UICharInfoU5BU5DU26_t4548_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUICharInfo_t460_m28412_gshared ();
void* RuntimeInvoker_Void_t168_UICharInfoU5BU5DU26_t4548_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUICharInfo_t460_m28414_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_UICharInfo_t460_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUICharInfo_t460_m28416_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t460_TisUICharInfo_t460_m28415_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t460_m28417_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t460_TisUICharInfo_t460_m28418_gshared ();
extern "C" void Array_compare_TisUICharInfo_t460_m28419_gshared ();
void* RuntimeInvoker_Int32_t127_UICharInfo_t460_UICharInfo_t460_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUICharInfo_t460_TisUICharInfo_t460_m28420_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t460_m28422_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t460_m28421_gshared ();
extern "C" void Array_swap_TisUICharInfo_t460_m28423_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t458_m28425_gshared ();
void* RuntimeInvoker_Void_t168_UILineInfoU5BU5DU26_t4549_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUILineInfo_t458_m28424_gshared ();
void* RuntimeInvoker_Void_t168_UILineInfoU5BU5DU26_t4549_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUILineInfo_t458_m28426_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_UILineInfo_t458_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUILineInfo_t458_m28428_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t458_TisUILineInfo_t458_m28427_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t458_m28429_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t458_TisUILineInfo_t458_m28430_gshared ();
extern "C" void Array_compare_TisUILineInfo_t458_m28431_gshared ();
void* RuntimeInvoker_Int32_t127_UILineInfo_t458_UILineInfo_t458_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUILineInfo_t458_TisUILineInfo_t458_m28432_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t458_m28434_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t458_m28433_gshared ();
extern "C" void Array_swap_TisUILineInfo_t458_m28435_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3777_m28437_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3777_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3777_m28438_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3777 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3777_m28439_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3777 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3777_m28440_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3777_m28441_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3777_m28442_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3777 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3777_m28443_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3777 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3777_m28445_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3777_m28446_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t1092_m28448_gshared ();
void* RuntimeInvoker_Int64_t1092_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t1092_m28449_gshared ();
void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t1092_m28450_gshared ();
void* RuntimeInvoker_Boolean_t169_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t1092_m28451_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t1092_m28452_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t1092_m28453_gshared ();
void* RuntimeInvoker_Int32_t127_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisInt64_t1092_m28454_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisInt64_t1092_m28456_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1092_m28457_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28459_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28458_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1092_m28461_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1092_TisObject_t_m28460_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1092_TisInt64_t1092_m28462_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28463_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3777_m28465_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3777_TisObject_t_m28464_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3777_TisKeyValuePair_2_t3777_m28466_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3815_m28468_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3815_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3815_m28469_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3815_m28470_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3815_m28471_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3815_m28472_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3815_m28473_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3815_m28474_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3815_m28476_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3815_m28477_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t1091_m28479_gshared ();
void* RuntimeInvoker_UInt64_t1091_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t1091_m28480_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t1091_m28481_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t1091_m28482_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t1091_m28483_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t1091_m28484_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt64_t1091_m28485_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt64_t1091_m28487_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1091_m28488_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1091_m28490_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1091_TisObject_t_m28489_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1091_TisUInt64_t1091_m28491_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28493_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28492_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28494_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3815_m28496_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3815_TisObject_t_m28495_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3815_TisKeyValuePair_2_t3815_m28497_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3836_m28499_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3836_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3836_m28500_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3836_m28501_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3836_m28502_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3836_m28503_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3836_m28504_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3836_m28505_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3836_m28507_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3836_m28508_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28510_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28509_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3254_m28512_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisObject_t_m28511_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisKeyValuePair_2_t3254_m28513_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28514_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3836_m28516_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3836_TisObject_t_m28515_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3836_TisKeyValuePair_2_t3836_m28517_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t2260_m28519_gshared ();
void* RuntimeInvoker_ParameterModifier_t2260_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t2260_m28520_gshared ();
void* RuntimeInvoker_Void_t168_ParameterModifier_t2260 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t2260_m28521_gshared ();
void* RuntimeInvoker_Boolean_t169_ParameterModifier_t2260 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2260_m28522_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t2260_m28523_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t2260_m28524_gshared ();
void* RuntimeInvoker_Int32_t127_ParameterModifier_t2260 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t2260_m28525_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_ParameterModifier_t2260 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t2260_m28527_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2260_m28528_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t1323_m28530_gshared ();
void* RuntimeInvoker_HitInfo_t1323_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t1323_m28531_gshared ();
void* RuntimeInvoker_Void_t168_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t1323_m28532_gshared ();
void* RuntimeInvoker_Boolean_t169_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1323_m28533_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t1323_m28534_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t1323_m28535_gshared ();
void* RuntimeInvoker_Int32_t127_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisHitInfo_t1323_m28536_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t1323_m28538_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1323_m28539_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t127_m28540_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t1075_m28542_gshared ();
void* RuntimeInvoker_UInt32_t1075_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t1075_m28543_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t1075_m28544_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t1075_m28545_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t1075_m28546_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t1075_m28547_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t1075_m28548_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t1075_m28550_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1075_m28551_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3932_m28553_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3932_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3932_m28554_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3932 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3932_m28555_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3932 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3932_m28556_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3932_m28557_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3932_m28558_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3932 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3932_m28559_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3932 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3932_m28561_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3932_m28562_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28564_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28563_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t449_m28566_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t449_TisObject_t_m28565_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t449_TisByte_t449_m28567_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28568_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3932_m28570_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3932_TisObject_t_m28569_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3932_TisKeyValuePair_2_t3932_m28571_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t1902_m28573_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1902_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1902_m28574_gshared ();
void* RuntimeInvoker_Void_t168_X509ChainStatus_t1902 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1902_m28575_gshared ();
void* RuntimeInvoker_Boolean_t169_X509ChainStatus_t1902 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1902_m28576_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1902_m28577_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t1902_m28578_gshared ();
void* RuntimeInvoker_Int32_t127_X509ChainStatus_t1902 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t1902_m28579_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_X509ChainStatus_t1902 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t1902_m28581_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1902_m28582_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3954_m28584_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3954_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3954_m28585_gshared ();
void* RuntimeInvoker_Void_t168_KeyValuePair_2_t3954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3954_m28586_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3954_m28587_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3954_m28588_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3954_m28589_gshared ();
void* RuntimeInvoker_Int32_t127_KeyValuePair_2_t3954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3954_m28590_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3954 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3954_m28592_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3954_m28593_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28595_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28594_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28596_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28597_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3954_m28599_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3954_TisObject_t_m28598_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3954_TisKeyValuePair_2_t3954_m28600_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t127_m28601_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisMark_t1948_m28603_gshared ();
void* RuntimeInvoker_Mark_t1948_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t1948_m28604_gshared ();
void* RuntimeInvoker_Void_t168_Mark_t1948 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t1948_m28605_gshared ();
void* RuntimeInvoker_Boolean_t169_Mark_t1948 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t1948_m28606_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t1948_m28607_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t1948_m28608_gshared ();
void* RuntimeInvoker_Int32_t127_Mark_t1948 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMark_t1948_m28609_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Mark_t1948 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMark_t1948_m28611_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1948_m28612_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t1984_m28614_gshared ();
void* RuntimeInvoker_UriScheme_t1984_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t1984_m28615_gshared ();
void* RuntimeInvoker_Void_t168_UriScheme_t1984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t1984_m28616_gshared ();
void* RuntimeInvoker_Boolean_t169_UriScheme_t1984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1984_m28617_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t1984_m28618_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t1984_m28619_gshared ();
void* RuntimeInvoker_Int32_t127_UriScheme_t1984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUriScheme_t1984_m28620_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_UriScheme_t1984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t1984_m28622_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1984_m28623_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t534_m28625_gshared ();
void* RuntimeInvoker_Int16_t534_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t534_m28626_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t534_m28627_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t534_m28628_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t534_m28629_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t534_m28630_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t534_m28631_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t534_m28633_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t534_m28634_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t170_m28636_gshared ();
void* RuntimeInvoker_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t170_m28637_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t170_m28638_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t170_m28639_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t170_m28640_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t170_m28641_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t170_m28642_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t170_m28644_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t170_m28645_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t2069_m28675_gshared ();
void* RuntimeInvoker_TableRange_t2069_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t2069_m28676_gshared ();
void* RuntimeInvoker_Void_t168_TableRange_t2069 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t2069_m28677_gshared ();
void* RuntimeInvoker_Boolean_t169_TableRange_t2069 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t2069_m28678_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t2069_m28679_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t2069_m28680_gshared ();
void* RuntimeInvoker_Int32_t127_TableRange_t2069 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTableRange_t2069_m28681_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_TableRange_t2069 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTableRange_t2069_m28683_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2069_m28684_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2146_m28686_gshared ();
void* RuntimeInvoker_Slot_t2146_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2146_m28687_gshared ();
void* RuntimeInvoker_Void_t168_Slot_t2146 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2146_m28688_gshared ();
void* RuntimeInvoker_Boolean_t169_Slot_t2146 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2146_m28689_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2146_m28690_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2146_m28691_gshared ();
void* RuntimeInvoker_Int32_t127_Slot_t2146 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2146_m28692_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Slot_t2146 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2146_m28694_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2146_m28695_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2153_m28697_gshared ();
void* RuntimeInvoker_Slot_t2153_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2153_m28698_gshared ();
void* RuntimeInvoker_Void_t168_Slot_t2153 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2153_m28699_gshared ();
void* RuntimeInvoker_Boolean_t169_Slot_t2153 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2153_m28700_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2153_m28701_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2153_m28702_gshared ();
void* RuntimeInvoker_Int32_t127_Slot_t2153 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2153_m28703_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Slot_t2153 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2153_m28705_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2153_m28706_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t111_m28710_gshared ();
void* RuntimeInvoker_DateTime_t111_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t111_m28711_gshared ();
void* RuntimeInvoker_Void_t168_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t111_m28712_gshared ();
void* RuntimeInvoker_Boolean_t169_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t111_m28713_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t111_m28714_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t111_m28715_gshared ();
void* RuntimeInvoker_Int32_t127_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDateTime_t111_m28716_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDateTime_t111_m28718_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t111_m28719_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t1059_m28721_gshared ();
void* RuntimeInvoker_Decimal_t1059_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1059_m28722_gshared ();
void* RuntimeInvoker_Void_t168_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t1059_m28723_gshared ();
void* RuntimeInvoker_Boolean_t169_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1059_m28724_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t1059_m28725_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t1059_m28726_gshared ();
void* RuntimeInvoker_Int32_t127_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDecimal_t1059_m28727_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1059_m28729_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1059_m28730_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t112_m28732_gshared ();
void* RuntimeInvoker_TimeSpan_t112_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t112_m28733_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t112_m28734_gshared ();
void* RuntimeInvoker_Boolean_t169_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t112_m28735_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t112_m28736_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t112_m28737_gshared ();
void* RuntimeInvoker_Int32_t127_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t112_m28738_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t112_m28740_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t112_m28741_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14873_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14875_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14877_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14879_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14881_gshared ();
void* RuntimeInvoker_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14882_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14883_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14884_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14885_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14886_gshared ();
extern "C" void Nullable_1_Equals_m14901_gshared ();
extern "C" void Nullable_1_Equals_m14903_gshared ();
void* RuntimeInvoker_Boolean_t169_Nullable_1_t3108 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m14905_gshared ();
extern "C" void Nullable_1_ToString_m14907_gshared ();
extern "C" void Nullable_1_Equals_m14912_gshared ();
extern "C" void Nullable_1_Equals_m14914_gshared ();
void* RuntimeInvoker_Boolean_t169_Nullable_1_t3109 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m14916_gshared ();
extern "C" void Nullable_1_ToString_m14918_gshared ();
extern "C" void DOGetter_1_Invoke_m14919_gshared ();
void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m14920_gshared ();
extern "C" void DOGetter_1_EndInvoke_m14921_gshared ();
void* RuntimeInvoker_Vector3_t15_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_Invoke_m14922_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m14923_gshared ();
void* RuntimeInvoker_Object_t_Vector3_t15_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m14924_gshared ();
extern "C" void TweenerCore_3__ctor_m14925_gshared ();
extern "C" void TweenerCore_3_Reset_m14926_gshared ();
extern "C" void TweenerCore_3_Validate_m14927_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m14928_gshared ();
void* RuntimeInvoker_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3_Startup_m14929_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m14930_gshared ();
void* RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3__ctor_m14931_gshared ();
extern "C" void TweenerCore_3_Reset_m14932_gshared ();
extern "C" void TweenerCore_3_Validate_m14933_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m14934_gshared ();
extern "C" void TweenerCore_3_Startup_m14935_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m14936_gshared ();
extern "C" void DOGetter_1_Invoke_m14937_gshared ();
void* RuntimeInvoker_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m14938_gshared ();
extern "C" void DOGetter_1_EndInvoke_m14939_gshared ();
void* RuntimeInvoker_Quaternion_t13_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_Invoke_m14940_gshared ();
void* RuntimeInvoker_Void_t168_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m14941_gshared ();
void* RuntimeInvoker_Object_t_Quaternion_t13_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m14942_gshared ();
extern "C" void TweenerCore_3__ctor_m14944_gshared ();
extern "C" void TweenerCore_3_Reset_m14946_gshared ();
extern "C" void TweenerCore_3_Validate_m14948_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m14950_gshared ();
extern "C" void TweenerCore_3_Startup_m14952_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m14954_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14956_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14957_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14958_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14959_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14960_gshared ();
extern "C" void Action_1_Invoke_m14963_gshared ();
extern "C" void Action_1_BeginInvoke_m14965_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m14967_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15144_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15146_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15147_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15148_gshared ();
void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m15303_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastResult_t231_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m15304_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t231_RaycastResult_t231_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15305_gshared ();
extern "C" void List_1__ctor_m15549_gshared ();
extern "C" void List_1__ctor_m15550_gshared ();
extern "C" void List_1__cctor_m15551_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15552_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15553_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15554_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15555_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15556_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15557_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15558_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15559_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15560_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15561_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15562_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15563_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15564_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15565_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15566_gshared ();
extern "C" void List_1_Add_m15567_gshared ();
extern "C" void List_1_GrowIfNeeded_m15568_gshared ();
extern "C" void List_1_AddCollection_m15569_gshared ();
extern "C" void List_1_AddEnumerable_m15570_gshared ();
extern "C" void List_1_AddRange_m15571_gshared ();
extern "C" void List_1_AsReadOnly_m15572_gshared ();
extern "C" void List_1_Clear_m15573_gshared ();
extern "C" void List_1_Contains_m15574_gshared ();
extern "C" void List_1_CopyTo_m15575_gshared ();
extern "C" void List_1_Find_m15576_gshared ();
void* RuntimeInvoker_RaycastResult_t231_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m15577_gshared ();
extern "C" void List_1_GetIndex_m15578_gshared ();
extern "C" void List_1_GetEnumerator_m15579_gshared ();
void* RuntimeInvoker_Enumerator_t3157 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15580_gshared ();
extern "C" void List_1_Shift_m15581_gshared ();
extern "C" void List_1_CheckIndex_m15582_gshared ();
extern "C" void List_1_Insert_m15583_gshared ();
extern "C" void List_1_CheckCollection_m15584_gshared ();
extern "C" void List_1_Remove_m15585_gshared ();
extern "C" void List_1_RemoveAll_m15586_gshared ();
extern "C" void List_1_RemoveAt_m15587_gshared ();
extern "C" void List_1_Reverse_m15588_gshared ();
extern "C" void List_1_Sort_m15589_gshared ();
extern "C" void List_1_ToArray_m15590_gshared ();
extern "C" void List_1_TrimExcess_m15591_gshared ();
extern "C" void List_1_get_Capacity_m15592_gshared ();
extern "C" void List_1_set_Capacity_m15593_gshared ();
extern "C" void List_1_get_Count_m15594_gshared ();
extern "C" void List_1_get_Item_m15595_gshared ();
extern "C" void List_1_set_Item_m15596_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15597_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15598_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15599_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15600_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15601_gshared ();
void* RuntimeInvoker_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m15602_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15603_gshared ();
extern "C" void Enumerator_Dispose_m15604_gshared ();
extern "C" void Enumerator_VerifyState_m15605_gshared ();
extern "C" void Enumerator_MoveNext_m15606_gshared ();
extern "C" void Enumerator_get_Current_m15607_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15608_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15609_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15610_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15611_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15612_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15613_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15614_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15615_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15616_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15617_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15618_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15619_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15620_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15621_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15622_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15623_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15624_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15625_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15626_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15627_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15628_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15629_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15630_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15631_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15632_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15633_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15634_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15635_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15636_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15637_gshared ();
extern "C" void Collection_1__ctor_m15638_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15639_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15640_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15641_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15642_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15643_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15644_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15645_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15646_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15647_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15648_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15649_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15650_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15651_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15652_gshared ();
extern "C" void Collection_1_Add_m15653_gshared ();
extern "C" void Collection_1_Clear_m15654_gshared ();
extern "C" void Collection_1_ClearItems_m15655_gshared ();
extern "C" void Collection_1_Contains_m15656_gshared ();
extern "C" void Collection_1_CopyTo_m15657_gshared ();
extern "C" void Collection_1_GetEnumerator_m15658_gshared ();
extern "C" void Collection_1_IndexOf_m15659_gshared ();
extern "C" void Collection_1_Insert_m15660_gshared ();
extern "C" void Collection_1_InsertItem_m15661_gshared ();
extern "C" void Collection_1_Remove_m15662_gshared ();
extern "C" void Collection_1_RemoveAt_m15663_gshared ();
extern "C" void Collection_1_RemoveItem_m15664_gshared ();
extern "C" void Collection_1_get_Count_m15665_gshared ();
extern "C" void Collection_1_get_Item_m15666_gshared ();
extern "C" void Collection_1_set_Item_m15667_gshared ();
extern "C" void Collection_1_SetItem_m15668_gshared ();
extern "C" void Collection_1_IsValidItem_m15669_gshared ();
extern "C" void Collection_1_ConvertItem_m15670_gshared ();
extern "C" void Collection_1_CheckWritable_m15671_gshared ();
extern "C" void Collection_1_IsSynchronized_m15672_gshared ();
extern "C" void Collection_1_IsFixedSize_m15673_gshared ();
extern "C" void EqualityComparer_1__ctor_m15674_gshared ();
extern "C" void EqualityComparer_1__cctor_m15675_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15676_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15677_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15678_gshared ();
extern "C" void DefaultComparer__ctor_m15679_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15680_gshared ();
extern "C" void DefaultComparer_Equals_m15681_gshared ();
void* RuntimeInvoker_Boolean_t169_RaycastResult_t231_RaycastResult_t231 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m15682_gshared ();
extern "C" void Predicate_1_Invoke_m15683_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15684_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t231_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m15685_gshared ();
extern "C" void Comparer_1__ctor_m15686_gshared ();
extern "C" void Comparer_1__cctor_m15687_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15688_gshared ();
extern "C" void Comparer_1_get_Default_m15689_gshared ();
extern "C" void DefaultComparer__ctor_m15690_gshared ();
extern "C" void DefaultComparer_Compare_m15691_gshared ();
extern "C" void Dictionary_2__ctor_m16129_gshared ();
extern "C" void Dictionary_2__ctor_m16131_gshared ();
extern "C" void Dictionary_2__ctor_m16133_gshared ();
extern "C" void Dictionary_2__ctor_m16135_gshared ();
extern "C" void Dictionary_2__ctor_m16137_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16139_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16141_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16143_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16145_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16147_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16149_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16151_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16153_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16155_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16157_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16159_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16161_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16163_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16165_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16167_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16169_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16171_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16173_gshared ();
extern "C" void Dictionary_2_get_Count_m16175_gshared ();
extern "C" void Dictionary_2_get_Item_m16177_gshared ();
extern "C" void Dictionary_2_set_Item_m16179_gshared ();
extern "C" void Dictionary_2_Init_m16181_gshared ();
extern "C" void Dictionary_2_InitArrays_m16183_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16185_gshared ();
extern "C" void Dictionary_2_make_pair_m16187_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3197_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16189_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16191_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16193_gshared ();
extern "C" void Dictionary_2_Resize_m16195_gshared ();
extern "C" void Dictionary_2_Add_m16197_gshared ();
extern "C" void Dictionary_2_Clear_m16199_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16201_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16205_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16207_gshared ();
extern "C" void Dictionary_2_Remove_m16209_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16211_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ToTKey_m16216_gshared ();
extern "C" void Dictionary_2_ToTValue_m16218_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16220_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16223_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16224_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16225_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16226_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16227_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16228_gshared ();
extern "C" void KeyValuePair_2__ctor_m16229_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16231_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16233_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16235_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16236_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16237_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16238_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16239_gshared ();
void* RuntimeInvoker_Link_t2136 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m16240_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16241_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16242_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16243_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16244_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16245_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16246_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16247_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16248_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16249_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16250_gshared ();
extern "C" void KeyCollection_CopyTo_m16251_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16252_gshared ();
void* RuntimeInvoker_Enumerator_t3201 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16253_gshared ();
extern "C" void Enumerator__ctor_m16254_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16255_gshared ();
extern "C" void Enumerator_Dispose_m16256_gshared ();
extern "C" void Enumerator_MoveNext_m16257_gshared ();
extern "C" void Enumerator_get_Current_m16258_gshared ();
extern "C" void Enumerator__ctor_m16259_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16266_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16267_gshared ();
extern "C" void Enumerator_VerifyState_m16268_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16269_gshared ();
extern "C" void Enumerator_Dispose_m16270_gshared ();
extern "C" void Transform_1__ctor_m16271_gshared ();
extern "C" void Transform_1_Invoke_m16272_gshared ();
extern "C" void Transform_1_BeginInvoke_m16273_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16274_gshared ();
extern "C" void ValueCollection__ctor_m16275_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16276_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16277_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16278_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16279_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16280_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16281_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16282_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16283_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16284_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16285_gshared ();
extern "C" void ValueCollection_CopyTo_m16286_gshared ();
extern "C" void ValueCollection_get_Count_m16288_gshared ();
extern "C" void Enumerator__ctor_m16289_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared ();
extern "C" void Enumerator_Dispose_m16291_gshared ();
extern "C" void Transform_1__ctor_m16294_gshared ();
extern "C" void Transform_1_Invoke_m16295_gshared ();
extern "C" void Transform_1_BeginInvoke_m16296_gshared ();
extern "C" void Transform_1_EndInvoke_m16297_gshared ();
extern "C" void Transform_1__ctor_m16298_gshared ();
extern "C" void Transform_1_Invoke_m16299_gshared ();
extern "C" void Transform_1_BeginInvoke_m16300_gshared ();
extern "C" void Transform_1_EndInvoke_m16301_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16302_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16303_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16304_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16305_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16306_gshared ();
extern "C" void Transform_1__ctor_m16307_gshared ();
extern "C" void Transform_1_Invoke_m16308_gshared ();
extern "C" void Transform_1_BeginInvoke_m16309_gshared ();
extern "C" void Transform_1_EndInvoke_m16310_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3197_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16311_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16312_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16313_gshared ();
extern "C" void ShimEnumerator_get_Key_m16314_gshared ();
extern "C" void ShimEnumerator_get_Value_m16315_gshared ();
extern "C" void ShimEnumerator_get_Current_m16316_gshared ();
extern "C" void EqualityComparer_1__ctor_m16317_gshared ();
extern "C" void EqualityComparer_1__cctor_m16318_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16319_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16320_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16321_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16322_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16323_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16324_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m16325_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16326_gshared ();
extern "C" void DefaultComparer_Equals_m16327_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16468_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16469_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16470_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16471_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16472_gshared ();
void* RuntimeInvoker_RaycastHit2D_t432 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m16473_gshared ();
void* RuntimeInvoker_Int32_t127_RaycastHit_t94_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m16474_gshared ();
void* RuntimeInvoker_Object_t_RaycastHit_t94_RaycastHit_t94_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m16475_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16476_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16477_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16478_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16479_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16480_gshared ();
void* RuntimeInvoker_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_RemoveListener_m16481_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16482_gshared ();
extern "C" void UnityAction_1_Invoke_m16483_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m16484_gshared ();
void* RuntimeInvoker_Object_t_Color_t90_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m16485_gshared ();
extern "C" void InvokableCall_1__ctor_m16486_gshared ();
extern "C" void InvokableCall_1__ctor_m16487_gshared ();
extern "C" void InvokableCall_1_Invoke_m16488_gshared ();
extern "C" void InvokableCall_1_Find_m16489_gshared ();
extern "C" void Dictionary_2__ctor_m16521_gshared ();
extern "C" void Dictionary_2__ctor_m16522_gshared ();
extern "C" void Dictionary_2__ctor_m16523_gshared ();
extern "C" void Dictionary_2__ctor_m16525_gshared ();
extern "C" void Dictionary_2__ctor_m16526_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16529_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16530_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16531_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16532_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16533_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16541_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544_gshared ();
extern "C" void Dictionary_2_get_Count_m16545_gshared ();
extern "C" void Dictionary_2_get_Item_m16546_gshared ();
extern "C" void Dictionary_2_set_Item_m16547_gshared ();
extern "C" void Dictionary_2_Init_m16548_gshared ();
extern "C" void Dictionary_2_InitArrays_m16549_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16550_gshared ();
extern "C" void Dictionary_2_make_pair_m16551_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3228_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16552_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16553_gshared ();
void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16554_gshared ();
extern "C" void Dictionary_2_Resize_m16555_gshared ();
extern "C" void Dictionary_2_Add_m16556_gshared ();
extern "C" void Dictionary_2_Clear_m16557_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16558_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16559_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16560_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16561_gshared ();
extern "C" void Dictionary_2_Remove_m16562_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16563_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m16564_gshared ();
extern "C" void Dictionary_2_get_Values_m16565_gshared ();
extern "C" void Dictionary_2_ToTKey_m16566_gshared ();
extern "C" void Dictionary_2_ToTValue_m16567_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16568_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16569_gshared ();
void* RuntimeInvoker_Enumerator_t3232 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16570_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16571_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16572_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16573_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16574_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16575_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3228 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m16576_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16577_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16578_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16579_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16580_gshared ();
extern "C" void KeyValuePair_2_ToString_m16581_gshared ();
extern "C" void KeyCollection__ctor_m16582_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16583_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16584_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16585_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16586_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16587_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16588_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16589_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16590_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16591_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16592_gshared ();
extern "C" void KeyCollection_CopyTo_m16593_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16594_gshared ();
void* RuntimeInvoker_Enumerator_t3231 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16595_gshared ();
extern "C" void Enumerator__ctor_m16596_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16597_gshared ();
extern "C" void Enumerator_Dispose_m16598_gshared ();
extern "C" void Enumerator_MoveNext_m16599_gshared ();
extern "C" void Enumerator_get_Current_m16600_gshared ();
extern "C" void Enumerator__ctor_m16601_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16602_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16603_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16604_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16605_gshared ();
extern "C" void Enumerator_MoveNext_m16606_gshared ();
extern "C" void Enumerator_get_Current_m16607_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16608_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16609_gshared ();
extern "C" void Enumerator_VerifyState_m16610_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16611_gshared ();
extern "C" void Enumerator_Dispose_m16612_gshared ();
extern "C" void Transform_1__ctor_m16613_gshared ();
extern "C" void Transform_1_Invoke_m16614_gshared ();
extern "C" void Transform_1_BeginInvoke_m16615_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16616_gshared ();
extern "C" void ValueCollection__ctor_m16617_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16618_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16619_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16620_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16621_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16622_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16623_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16624_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16625_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16626_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16627_gshared ();
extern "C" void ValueCollection_CopyTo_m16628_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16629_gshared ();
void* RuntimeInvoker_Enumerator_t3235 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m16630_gshared ();
extern "C" void Enumerator__ctor_m16631_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16632_gshared ();
extern "C" void Enumerator_Dispose_m16633_gshared ();
extern "C" void Enumerator_MoveNext_m16634_gshared ();
extern "C" void Enumerator_get_Current_m16635_gshared ();
extern "C" void Transform_1__ctor_m16636_gshared ();
extern "C" void Transform_1_Invoke_m16637_gshared ();
extern "C" void Transform_1_BeginInvoke_m16638_gshared ();
extern "C" void Transform_1_EndInvoke_m16639_gshared ();
extern "C" void Transform_1__ctor_m16640_gshared ();
extern "C" void Transform_1_Invoke_m16641_gshared ();
extern "C" void Transform_1_BeginInvoke_m16642_gshared ();
extern "C" void Transform_1_EndInvoke_m16643_gshared ();
extern "C" void Transform_1__ctor_m16644_gshared ();
extern "C" void Transform_1_Invoke_m16645_gshared ();
extern "C" void Transform_1_BeginInvoke_m16646_gshared ();
extern "C" void Transform_1_EndInvoke_m16647_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3228_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16648_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16649_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16650_gshared ();
extern "C" void ShimEnumerator_get_Key_m16651_gshared ();
extern "C" void ShimEnumerator_get_Value_m16652_gshared ();
extern "C" void ShimEnumerator_get_Current_m16653_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16903_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16905_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16906_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16907_gshared ();
extern "C" void Transform_1__ctor_m16968_gshared ();
extern "C" void Transform_1_Invoke_m16969_gshared ();
extern "C" void Transform_1_BeginInvoke_m16970_gshared ();
extern "C" void Transform_1_EndInvoke_m16971_gshared ();
extern "C" void Transform_1__ctor_m16972_gshared ();
extern "C" void Transform_1_Invoke_m16973_gshared ();
extern "C" void Transform_1_BeginInvoke_m16974_gshared ();
extern "C" void Transform_1_EndInvoke_m16975_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3254_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m17143_gshared ();
extern "C" void List_1__cctor_m17144_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17145_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17146_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m17147_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m17148_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m17149_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m17150_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m17151_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m17152_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17153_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m17154_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m17155_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m17156_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m17157_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m17158_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m17159_gshared ();
extern "C" void List_1_Add_m17160_gshared ();
void* RuntimeInvoker_Void_t168_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GrowIfNeeded_m17161_gshared ();
extern "C" void List_1_AddCollection_m17162_gshared ();
extern "C" void List_1_AddEnumerable_m17163_gshared ();
extern "C" void List_1_AddRange_m17164_gshared ();
extern "C" void List_1_AsReadOnly_m17165_gshared ();
extern "C" void List_1_Clear_m17166_gshared ();
extern "C" void List_1_Contains_m17167_gshared ();
void* RuntimeInvoker_Boolean_t169_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CopyTo_m17168_gshared ();
extern "C" void List_1_Find_m17169_gshared ();
void* RuntimeInvoker_UIVertex_t313_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m17170_gshared ();
extern "C" void List_1_GetIndex_m17171_gshared ();
extern "C" void List_1_GetEnumerator_m17172_gshared ();
void* RuntimeInvoker_Enumerator_t3271 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m17173_gshared ();
void* RuntimeInvoker_Int32_t127_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_Shift_m17174_gshared ();
extern "C" void List_1_CheckIndex_m17175_gshared ();
extern "C" void List_1_Insert_m17176_gshared ();
void* RuntimeInvoker_Void_t168_Int32_t127_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckCollection_m17177_gshared ();
extern "C" void List_1_Remove_m17178_gshared ();
extern "C" void List_1_RemoveAll_m17179_gshared ();
extern "C" void List_1_RemoveAt_m17180_gshared ();
extern "C" void List_1_Reverse_m17181_gshared ();
extern "C" void List_1_Sort_m17182_gshared ();
extern "C" void List_1_Sort_m17183_gshared ();
extern "C" void List_1_TrimExcess_m17184_gshared ();
extern "C" void List_1_get_Count_m17185_gshared ();
extern "C" void List_1_get_Item_m17186_gshared ();
void* RuntimeInvoker_UIVertex_t313_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_set_Item_m17187_gshared ();
extern "C" void Enumerator__ctor_m17122_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17123_gshared ();
extern "C" void Enumerator_Dispose_m17124_gshared ();
extern "C" void Enumerator_VerifyState_m17125_gshared ();
extern "C" void Enumerator_MoveNext_m17126_gshared ();
extern "C" void Enumerator_get_Current_m17127_gshared ();
void* RuntimeInvoker_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void ReadOnlyCollection_1__ctor_m17088_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17089_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17090_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17091_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17092_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17093_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17094_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17095_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17096_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17097_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17098_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m17099_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17100_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m17101_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17102_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17103_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17104_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17105_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17106_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17107_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17108_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17109_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m17110_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17111_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m17112_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m17113_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m17114_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m17115_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m17116_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m17117_gshared ();
extern "C" void Collection_1__ctor_m17191_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17192_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17193_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m17194_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m17195_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m17196_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m17197_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m17198_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m17199_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m17200_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m17201_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m17202_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m17203_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m17204_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m17205_gshared ();
extern "C" void Collection_1_Add_m17206_gshared ();
extern "C" void Collection_1_Clear_m17207_gshared ();
extern "C" void Collection_1_ClearItems_m17208_gshared ();
extern "C" void Collection_1_Contains_m17209_gshared ();
extern "C" void Collection_1_CopyTo_m17210_gshared ();
extern "C" void Collection_1_GetEnumerator_m17211_gshared ();
extern "C" void Collection_1_IndexOf_m17212_gshared ();
extern "C" void Collection_1_Insert_m17213_gshared ();
extern "C" void Collection_1_InsertItem_m17214_gshared ();
extern "C" void Collection_1_Remove_m17215_gshared ();
extern "C" void Collection_1_RemoveAt_m17216_gshared ();
extern "C" void Collection_1_RemoveItem_m17217_gshared ();
extern "C" void Collection_1_get_Count_m17218_gshared ();
extern "C" void Collection_1_get_Item_m17219_gshared ();
extern "C" void Collection_1_set_Item_m17220_gshared ();
extern "C" void Collection_1_SetItem_m17221_gshared ();
extern "C" void Collection_1_IsValidItem_m17222_gshared ();
extern "C" void Collection_1_ConvertItem_m17223_gshared ();
extern "C" void Collection_1_CheckWritable_m17224_gshared ();
extern "C" void Collection_1_IsSynchronized_m17225_gshared ();
extern "C" void Collection_1_IsFixedSize_m17226_gshared ();
extern "C" void EqualityComparer_1__ctor_m17227_gshared ();
extern "C" void EqualityComparer_1__cctor_m17228_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17229_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17230_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17231_gshared ();
extern "C" void DefaultComparer__ctor_m17232_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17233_gshared ();
extern "C" void DefaultComparer_Equals_m17234_gshared ();
void* RuntimeInvoker_Boolean_t169_UIVertex_t313_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m17118_gshared ();
extern "C" void Predicate_1_Invoke_m17119_gshared ();
extern "C" void Predicate_1_BeginInvoke_m17120_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t313_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m17121_gshared ();
extern "C" void Comparer_1__ctor_m17235_gshared ();
extern "C" void Comparer_1__cctor_m17236_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m17237_gshared ();
extern "C" void Comparer_1_get_Default_m17238_gshared ();
extern "C" void DefaultComparer__ctor_m17239_gshared ();
extern "C" void DefaultComparer_Compare_m17240_gshared ();
void* RuntimeInvoker_Int32_t127_UIVertex_t313_UIVertex_t313 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m17128_gshared ();
extern "C" void Comparison_1_Invoke_m17129_gshared ();
extern "C" void Comparison_1_BeginInvoke_m17130_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t313_UIVertex_t313_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m17131_gshared ();
extern "C" void TweenRunner_1_Start_m17241_gshared ();
void* RuntimeInvoker_Object_t_ColorTween_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void U3CStartU3Ec__Iterator0__ctor_m17242_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17243_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17244_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m17245_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m17246_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m17247_gshared ();
extern "C" void InternalEnumerator_1__ctor_m17698_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17699_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17700_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17701_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17702_gshared ();
void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17708_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17709_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17710_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17711_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17712_gshared ();
void* RuntimeInvoker_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17713_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17714_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17715_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17716_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17717_gshared ();
void* RuntimeInvoker_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_GetDelegate_m17725_gshared ();
extern "C" void UnityAction_1_Invoke_m17726_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17727_gshared ();
void* RuntimeInvoker_Object_t_Single_t151_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17728_gshared ();
extern "C" void InvokableCall_1__ctor_m17729_gshared ();
extern "C" void InvokableCall_1__ctor_m17730_gshared ();
extern "C" void InvokableCall_1_Invoke_m17731_gshared ();
extern "C" void InvokableCall_1_Find_m17732_gshared ();
extern "C" void UnityEvent_1_AddListener_m17733_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m17734_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m17735_gshared ();
extern "C" void UnityAction_1__ctor_m17736_gshared ();
extern "C" void UnityAction_1_Invoke_m17737_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17738_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t10_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17739_gshared ();
extern "C" void InvokableCall_1__ctor_m17740_gshared ();
extern "C" void InvokableCall_1__ctor_m17741_gshared ();
extern "C" void InvokableCall_1_Invoke_m17742_gshared ();
extern "C" void InvokableCall_1_Find_m17743_gshared ();
extern "C" void UnityEvent_1_AddListener_m18025_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m18027_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18031_gshared ();
extern "C" void UnityAction_1__ctor_m18033_gshared ();
extern "C" void UnityAction_1_Invoke_m18034_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m18035_gshared ();
void* RuntimeInvoker_Object_t_SByte_t170_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m18036_gshared ();
extern "C" void InvokableCall_1__ctor_m18037_gshared ();
extern "C" void InvokableCall_1__ctor_m18038_gshared ();
extern "C" void InvokableCall_1_Invoke_m18039_gshared ();
extern "C" void InvokableCall_1_Find_m18040_gshared ();
extern "C" void Func_2_Invoke_m18138_gshared ();
void* RuntimeInvoker_Byte_t449_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18140_gshared ();
extern "C" void Func_2_EndInvoke_m18142_gshared ();
extern "C" void Func_2_Invoke_m18252_gshared ();
void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18254_gshared ();
extern "C" void Func_2_EndInvoke_m18256_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18293_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18294_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18295_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18296_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18297_gshared ();
void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18298_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18299_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18300_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18301_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18302_gshared ();
void* RuntimeInvoker_EyewearCalibrationReading_t586 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_Invoke_m18485_gshared ();
extern "C" void Action_1_BeginInvoke_m18487_gshared ();
extern "C" void Action_1_EndInvoke_m18489_gshared ();
extern "C" void List_1__ctor_m18860_gshared ();
extern "C" void List_1__cctor_m18862_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18866_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m18868_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m18870_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m18872_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m18874_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m18876_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m18878_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m18882_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m18884_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m18886_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m18888_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m18890_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m18892_gshared ();
extern "C" void List_1_Add_m18894_gshared ();
extern "C" void List_1_GrowIfNeeded_m18896_gshared ();
extern "C" void List_1_AddCollection_m18898_gshared ();
extern "C" void List_1_AddEnumerable_m18900_gshared ();
extern "C" void List_1_AddRange_m18902_gshared ();
extern "C" void List_1_AsReadOnly_m18904_gshared ();
extern "C" void List_1_Clear_m18906_gshared ();
extern "C" void List_1_Contains_m18908_gshared ();
extern "C" void List_1_CopyTo_m18910_gshared ();
extern "C" void List_1_Find_m18912_gshared ();
extern "C" void List_1_CheckMatch_m18914_gshared ();
extern "C" void List_1_GetIndex_m18916_gshared ();
extern "C" void List_1_IndexOf_m18919_gshared ();
extern "C" void List_1_Shift_m18921_gshared ();
extern "C" void List_1_CheckIndex_m18923_gshared ();
extern "C" void List_1_Insert_m18925_gshared ();
extern "C" void List_1_CheckCollection_m18927_gshared ();
extern "C" void List_1_Remove_m18929_gshared ();
extern "C" void List_1_RemoveAll_m18931_gshared ();
extern "C" void List_1_RemoveAt_m18933_gshared ();
extern "C" void List_1_Reverse_m18935_gshared ();
extern "C" void List_1_Sort_m18937_gshared ();
extern "C" void List_1_Sort_m18939_gshared ();
extern "C" void List_1_ToArray_m18941_gshared ();
extern "C" void List_1_TrimExcess_m18943_gshared ();
extern "C" void List_1_get_Capacity_m18945_gshared ();
extern "C" void List_1_set_Capacity_m18947_gshared ();
extern "C" void List_1_get_Count_m18949_gshared ();
extern "C" void List_1_get_Item_m18951_gshared ();
extern "C" void List_1_set_Item_m18953_gshared ();
extern "C" void Enumerator__ctor_m18954_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18955_gshared ();
extern "C" void Enumerator_Dispose_m18956_gshared ();
extern "C" void Enumerator_VerifyState_m18957_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m18958_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18959_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18960_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18961_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18962_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18963_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18964_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18965_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18966_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18967_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18968_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m18969_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m18970_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m18971_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18972_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m18973_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m18974_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18975_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18976_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18977_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18978_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18979_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m18980_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m18981_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m18982_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m18983_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m18984_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m18985_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m18986_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m18987_gshared ();
extern "C" void Collection_1__ctor_m18988_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18989_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18990_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m18991_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m18992_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m18993_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m18994_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m18995_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m18996_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m18997_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m18998_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m18999_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m19000_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m19001_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m19002_gshared ();
extern "C" void Collection_1_Add_m19003_gshared ();
extern "C" void Collection_1_Clear_m19004_gshared ();
extern "C" void Collection_1_ClearItems_m19005_gshared ();
extern "C" void Collection_1_Contains_m19006_gshared ();
extern "C" void Collection_1_CopyTo_m19007_gshared ();
extern "C" void Collection_1_GetEnumerator_m19008_gshared ();
extern "C" void Collection_1_IndexOf_m19009_gshared ();
extern "C" void Collection_1_Insert_m19010_gshared ();
extern "C" void Collection_1_InsertItem_m19011_gshared ();
extern "C" void Collection_1_Remove_m19012_gshared ();
extern "C" void Collection_1_RemoveAt_m19013_gshared ();
extern "C" void Collection_1_RemoveItem_m19014_gshared ();
extern "C" void Collection_1_get_Count_m19015_gshared ();
extern "C" void Collection_1_get_Item_m19016_gshared ();
extern "C" void Collection_1_set_Item_m19017_gshared ();
extern "C" void Collection_1_SetItem_m19018_gshared ();
extern "C" void Collection_1_IsValidItem_m19019_gshared ();
extern "C" void Collection_1_ConvertItem_m19020_gshared ();
extern "C" void Collection_1_CheckWritable_m19021_gshared ();
extern "C" void Collection_1_IsSynchronized_m19022_gshared ();
extern "C" void Collection_1_IsFixedSize_m19023_gshared ();
extern "C" void Predicate_1__ctor_m19024_gshared ();
extern "C" void Predicate_1_Invoke_m19025_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19026_gshared ();
extern "C" void Predicate_1_EndInvoke_m19027_gshared ();
extern "C" void Comparer_1__ctor_m19028_gshared ();
extern "C" void Comparer_1__cctor_m19029_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m19030_gshared ();
extern "C" void Comparer_1_get_Default_m19031_gshared ();
extern "C" void GenericComparer_1__ctor_m19032_gshared ();
extern "C" void GenericComparer_1_Compare_m19033_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m19034_gshared ();
extern "C" void DefaultComparer_Compare_m19035_gshared ();
extern "C" void Comparison_1__ctor_m19036_gshared ();
extern "C" void Comparison_1_Invoke_m19037_gshared ();
extern "C" void Comparison_1_BeginInvoke_m19038_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m19039_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19271_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19273_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19274_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19275_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19276_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19277_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19278_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19279_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19280_gshared ();
void* RuntimeInvoker_Color32_t421 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19281_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19282_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19283_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19284_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19285_gshared ();
void* RuntimeInvoker_Color_t90 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19765_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19766_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19767_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19768_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19769_gshared ();
void* RuntimeInvoker_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19770_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19771_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19772_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19773_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19774_gshared ();
void* RuntimeInvoker_WordData_t647 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19775_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19776_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19777_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19778_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19779_gshared ();
void* RuntimeInvoker_WordResultData_t646 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1__ctor_m19780_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19781_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m19782_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19783_gshared ();
extern "C" void LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19784_gshared ();
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19785_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19786_gshared ();
extern "C" void LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19787_gshared ();
extern "C" void LinkedList_1_VerifyReferencedNode_m19788_gshared ();
extern "C" void LinkedList_1_Clear_m19789_gshared ();
extern "C" void LinkedList_1_Contains_m19790_gshared ();
extern "C" void LinkedList_1_CopyTo_m19791_gshared ();
extern "C" void LinkedList_1_Find_m19792_gshared ();
extern "C" void LinkedList_1_GetEnumerator_m19793_gshared ();
void* RuntimeInvoker_Enumerator_t3442 (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedList_1_GetObjectData_m19794_gshared ();
extern "C" void LinkedList_1_OnDeserialization_m19795_gshared ();
extern "C" void LinkedList_1_Remove_m19796_gshared ();
extern "C" void LinkedList_1_RemoveLast_m19797_gshared ();
extern "C" void LinkedList_1_get_Count_m19798_gshared ();
extern "C" void LinkedListNode_1__ctor_m19799_gshared ();
extern "C" void LinkedListNode_1__ctor_m19800_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LinkedListNode_1_Detach_m19801_gshared ();
extern "C" void LinkedListNode_1_get_List_m19802_gshared ();
extern "C" void Enumerator__ctor_m19803_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19804_gshared ();
extern "C" void Enumerator_get_Current_m19805_gshared ();
extern "C" void Enumerator_MoveNext_m19806_gshared ();
extern "C" void Enumerator_Dispose_m19807_gshared ();
extern "C" void Predicate_1_Invoke_m19808_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19809_gshared ();
void* RuntimeInvoker_Object_t_TrackableResultData_t642_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m19810_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19811_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19812_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19813_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19814_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19815_gshared ();
void* RuntimeInvoker_SmartTerrainRevisionData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19816_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19817_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19818_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19819_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19820_gshared ();
void* RuntimeInvoker_SurfaceData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19821_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19822_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19823_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19824_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19825_gshared ();
void* RuntimeInvoker_PropData_t652 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m20023_gshared ();
extern "C" void Dictionary_2__ctor_m20025_gshared ();
extern "C" void Dictionary_2__ctor_m20027_gshared ();
extern "C" void Dictionary_2__ctor_m20029_gshared ();
extern "C" void Dictionary_2__ctor_m20031_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m20037_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20039_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20041_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m20043_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20045_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20061_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067_gshared ();
extern "C" void Dictionary_2_get_Count_m20069_gshared ();
extern "C" void Dictionary_2_get_Item_m20071_gshared ();
void* RuntimeInvoker_UInt16_t454_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m20073_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m20075_gshared ();
extern "C" void Dictionary_2_InitArrays_m20077_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m20079_gshared ();
extern "C" void Dictionary_2_make_pair_m20081_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3462_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m20083_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m20085_gshared ();
void* RuntimeInvoker_UInt16_t454_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m20087_gshared ();
extern "C" void Dictionary_2_Resize_m20089_gshared ();
extern "C" void Dictionary_2_Add_m20091_gshared ();
extern "C" void Dictionary_2_Clear_m20093_gshared ();
extern "C" void Dictionary_2_ContainsKey_m20095_gshared ();
extern "C" void Dictionary_2_ContainsValue_m20097_gshared ();
extern "C" void Dictionary_2_GetObjectData_m20099_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m20101_gshared ();
extern "C" void Dictionary_2_Remove_m20103_gshared ();
extern "C" void Dictionary_2_TryGetValue_m20105_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_UInt16U26_t2720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m20107_gshared ();
extern "C" void Dictionary_2_get_Values_m20109_gshared ();
extern "C" void Dictionary_2_ToTKey_m20111_gshared ();
extern "C" void Dictionary_2_ToTValue_m20113_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m20115_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m20117_gshared ();
void* RuntimeInvoker_Enumerator_t3466 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m20119_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m20120_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20122_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20123_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20124_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3462 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m20125_gshared ();
extern "C" void KeyValuePair_2_get_Key_m20126_gshared ();
extern "C" void KeyValuePair_2_set_Key_m20127_gshared ();
extern "C" void KeyValuePair_2_get_Value_m20128_gshared ();
extern "C" void KeyValuePair_2_set_Value_m20129_gshared ();
extern "C" void KeyValuePair_2_ToString_m20130_gshared ();
extern "C" void KeyCollection__ctor_m20131_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20132_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20133_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20134_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20135_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20136_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m20137_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20138_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20139_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20140_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m20141_gshared ();
extern "C" void KeyCollection_CopyTo_m20142_gshared ();
extern "C" void KeyCollection_GetEnumerator_m20143_gshared ();
void* RuntimeInvoker_Enumerator_t3465 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m20144_gshared ();
extern "C" void Enumerator__ctor_m20145_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20146_gshared ();
extern "C" void Enumerator_Dispose_m20147_gshared ();
extern "C" void Enumerator_MoveNext_m20148_gshared ();
extern "C" void Enumerator_get_Current_m20149_gshared ();
extern "C" void Enumerator__ctor_m20150_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20151_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154_gshared ();
extern "C" void Enumerator_MoveNext_m20155_gshared ();
extern "C" void Enumerator_get_Current_m20156_gshared ();
extern "C" void Enumerator_get_CurrentKey_m20157_gshared ();
extern "C" void Enumerator_get_CurrentValue_m20158_gshared ();
extern "C" void Enumerator_VerifyState_m20159_gshared ();
extern "C" void Enumerator_VerifyCurrent_m20160_gshared ();
extern "C" void Enumerator_Dispose_m20161_gshared ();
extern "C" void Transform_1__ctor_m20162_gshared ();
extern "C" void Transform_1_Invoke_m20163_gshared ();
extern "C" void Transform_1_BeginInvoke_m20164_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m20165_gshared ();
extern "C" void ValueCollection__ctor_m20166_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20167_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20168_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20169_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20170_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20171_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20172_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20173_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20174_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20175_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m20176_gshared ();
extern "C" void ValueCollection_CopyTo_m20177_gshared ();
extern "C" void ValueCollection_GetEnumerator_m20178_gshared ();
void* RuntimeInvoker_Enumerator_t3469 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m20179_gshared ();
extern "C" void Enumerator__ctor_m20180_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20181_gshared ();
extern "C" void Enumerator_Dispose_m20182_gshared ();
extern "C" void Enumerator_MoveNext_m20183_gshared ();
extern "C" void Enumerator_get_Current_m20184_gshared ();
extern "C" void Transform_1__ctor_m20185_gshared ();
extern "C" void Transform_1_Invoke_m20186_gshared ();
extern "C" void Transform_1_BeginInvoke_m20187_gshared ();
extern "C" void Transform_1_EndInvoke_m20188_gshared ();
extern "C" void Transform_1__ctor_m20189_gshared ();
extern "C" void Transform_1_Invoke_m20190_gshared ();
extern "C" void Transform_1_BeginInvoke_m20191_gshared ();
extern "C" void Transform_1_EndInvoke_m20192_gshared ();
extern "C" void Transform_1__ctor_m20193_gshared ();
extern "C" void Transform_1_Invoke_m20194_gshared ();
extern "C" void Transform_1_BeginInvoke_m20195_gshared ();
extern "C" void Transform_1_EndInvoke_m20196_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3462_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m20197_gshared ();
extern "C" void ShimEnumerator_MoveNext_m20198_gshared ();
extern "C" void ShimEnumerator_get_Entry_m20199_gshared ();
extern "C" void ShimEnumerator_get_Key_m20200_gshared ();
extern "C" void ShimEnumerator_get_Value_m20201_gshared ();
extern "C" void ShimEnumerator_get_Current_m20202_gshared ();
extern "C" void EqualityComparer_1__ctor_m20203_gshared ();
extern "C" void EqualityComparer_1__cctor_m20204_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20205_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20206_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20207_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m20208_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m20209_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m20210_gshared ();
void* RuntimeInvoker_Boolean_t169_Int16_t534_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m20211_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20212_gshared ();
extern "C" void DefaultComparer_Equals_m20213_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20264_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20265_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20266_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20267_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20268_gshared ();
void* RuntimeInvoker_RectangleData_t596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1__ctor_m21101_gshared ();
extern "C" void Action_1_Invoke_m21102_gshared ();
void* RuntimeInvoker_Void_t168_SmartTerrainInitializationInfo_t588 (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_BeginInvoke_m21103_gshared ();
void* RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t588_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m21104_gshared ();
extern "C" void Dictionary_2__ctor_m21797_gshared ();
extern "C" void Dictionary_2__ctor_m21798_gshared ();
extern "C" void Dictionary_2__ctor_m21799_gshared ();
extern "C" void Dictionary_2__ctor_m21800_gshared ();
extern "C" void Dictionary_2__ctor_m21801_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21802_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21803_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21804_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21805_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21806_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m21807_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21808_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21809_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21810_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21811_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21812_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21813_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21816_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21817_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21818_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21819_gshared ();
extern "C" void Dictionary_2_get_Count_m21820_gshared ();
extern "C" void Dictionary_2_get_Item_m21821_gshared ();
extern "C" void Dictionary_2_set_Item_m21822_gshared ();
extern "C" void Dictionary_2_Init_m21823_gshared ();
extern "C" void Dictionary_2_InitArrays_m21824_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m21825_gshared ();
extern "C" void Dictionary_2_make_pair_m21826_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3550_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m21827_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m21828_gshared ();
void* RuntimeInvoker_TrackableResultData_t642_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m21829_gshared ();
extern "C" void Dictionary_2_Resize_m21830_gshared ();
extern "C" void Dictionary_2_Add_m21831_gshared ();
extern "C" void Dictionary_2_Clear_m21832_gshared ();
extern "C" void Dictionary_2_ContainsKey_m21833_gshared ();
extern "C" void Dictionary_2_ContainsValue_m21834_gshared ();
extern "C" void Dictionary_2_GetObjectData_m21835_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m21836_gshared ();
extern "C" void Dictionary_2_Remove_m21837_gshared ();
extern "C" void Dictionary_2_TryGetValue_m21838_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_TrackableResultDataU26_t4550 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m21839_gshared ();
extern "C" void Dictionary_2_get_Values_m21840_gshared ();
extern "C" void Dictionary_2_ToTKey_m21841_gshared ();
extern "C" void Dictionary_2_ToTValue_m21842_gshared ();
void* RuntimeInvoker_TrackableResultData_t642_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ContainsKeyValuePair_m21843_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m21844_gshared ();
void* RuntimeInvoker_Enumerator_t3554 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m21845_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21846_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21847_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21848_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21849_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21850_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3550 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m21851_gshared ();
extern "C" void KeyValuePair_2_get_Key_m21852_gshared ();
extern "C" void KeyValuePair_2_set_Key_m21853_gshared ();
extern "C" void KeyValuePair_2_get_Value_m21854_gshared ();
extern "C" void KeyValuePair_2_set_Value_m21855_gshared ();
extern "C" void KeyValuePair_2_ToString_m21856_gshared ();
extern "C" void KeyCollection__ctor_m21857_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21858_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21859_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21860_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21861_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21862_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m21863_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21864_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21865_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21866_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m21867_gshared ();
extern "C" void KeyCollection_CopyTo_m21868_gshared ();
extern "C" void KeyCollection_GetEnumerator_m21869_gshared ();
void* RuntimeInvoker_Enumerator_t3553 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m21870_gshared ();
extern "C" void Enumerator__ctor_m21871_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21872_gshared ();
extern "C" void Enumerator_Dispose_m21873_gshared ();
extern "C" void Enumerator_MoveNext_m21874_gshared ();
extern "C" void Enumerator_get_Current_m21875_gshared ();
extern "C" void Enumerator__ctor_m21876_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21877_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21878_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21879_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21880_gshared ();
extern "C" void Enumerator_MoveNext_m21881_gshared ();
extern "C" void Enumerator_get_Current_m21882_gshared ();
extern "C" void Enumerator_get_CurrentKey_m21883_gshared ();
extern "C" void Enumerator_get_CurrentValue_m21884_gshared ();
extern "C" void Enumerator_VerifyState_m21885_gshared ();
extern "C" void Enumerator_VerifyCurrent_m21886_gshared ();
extern "C" void Enumerator_Dispose_m21887_gshared ();
extern "C" void Transform_1__ctor_m21888_gshared ();
extern "C" void Transform_1_Invoke_m21889_gshared ();
extern "C" void Transform_1_BeginInvoke_m21890_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_TrackableResultData_t642_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m21891_gshared ();
extern "C" void ValueCollection__ctor_m21892_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21893_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21894_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21895_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21896_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21897_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m21898_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21899_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21900_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21901_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m21902_gshared ();
extern "C" void ValueCollection_CopyTo_m21903_gshared ();
extern "C" void ValueCollection_GetEnumerator_m21904_gshared ();
void* RuntimeInvoker_Enumerator_t3557 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m21905_gshared ();
extern "C" void Enumerator__ctor_m21906_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21907_gshared ();
extern "C" void Enumerator_Dispose_m21908_gshared ();
extern "C" void Enumerator_MoveNext_m21909_gshared ();
extern "C" void Enumerator_get_Current_m21910_gshared ();
extern "C" void Transform_1__ctor_m21911_gshared ();
extern "C" void Transform_1_Invoke_m21912_gshared ();
extern "C" void Transform_1_BeginInvoke_m21913_gshared ();
extern "C" void Transform_1_EndInvoke_m21914_gshared ();
extern "C" void Transform_1__ctor_m21915_gshared ();
extern "C" void Transform_1_Invoke_m21916_gshared ();
extern "C" void Transform_1_BeginInvoke_m21917_gshared ();
extern "C" void Transform_1_EndInvoke_m21918_gshared ();
extern "C" void Transform_1__ctor_m21919_gshared ();
extern "C" void Transform_1_Invoke_m21920_gshared ();
extern "C" void Transform_1_BeginInvoke_m21921_gshared ();
extern "C" void Transform_1_EndInvoke_m21922_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3550_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m21923_gshared ();
extern "C" void ShimEnumerator_MoveNext_m21924_gshared ();
extern "C" void ShimEnumerator_get_Entry_m21925_gshared ();
extern "C" void ShimEnumerator_get_Key_m21926_gshared ();
extern "C" void ShimEnumerator_get_Value_m21927_gshared ();
extern "C" void ShimEnumerator_get_Current_m21928_gshared ();
extern "C" void EqualityComparer_1__ctor_m21929_gshared ();
extern "C" void EqualityComparer_1__cctor_m21930_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21931_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21932_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21933_gshared ();
extern "C" void DefaultComparer__ctor_m21934_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21935_gshared ();
extern "C" void DefaultComparer_Equals_m21936_gshared ();
void* RuntimeInvoker_Boolean_t169_TrackableResultData_t642_TrackableResultData_t642 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m21937_gshared ();
extern "C" void Dictionary_2__ctor_m21938_gshared ();
extern "C" void Dictionary_2__ctor_m21939_gshared ();
extern "C" void Dictionary_2__ctor_m21940_gshared ();
extern "C" void Dictionary_2__ctor_m21941_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21942_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21943_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21944_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21945_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21946_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m21947_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21948_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21949_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21950_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21951_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21952_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21953_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21954_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21955_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21956_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21957_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21958_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21959_gshared ();
extern "C" void Dictionary_2_get_Count_m21960_gshared ();
extern "C" void Dictionary_2_get_Item_m21961_gshared ();
extern "C" void Dictionary_2_set_Item_m21962_gshared ();
extern "C" void Dictionary_2_Init_m21963_gshared ();
extern "C" void Dictionary_2_InitArrays_m21964_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m21965_gshared ();
extern "C" void Dictionary_2_make_pair_m21966_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3565_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m21967_gshared ();
void* RuntimeInvoker_Int32_t127_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m21968_gshared ();
void* RuntimeInvoker_VirtualButtonData_t643_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m21969_gshared ();
extern "C" void Dictionary_2_Resize_m21970_gshared ();
extern "C" void Dictionary_2_Add_m21971_gshared ();
extern "C" void Dictionary_2_Clear_m21972_gshared ();
extern "C" void Dictionary_2_ContainsKey_m21973_gshared ();
extern "C" void Dictionary_2_ContainsValue_m21974_gshared ();
extern "C" void Dictionary_2_GetObjectData_m21975_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m21976_gshared ();
extern "C" void Dictionary_2_Remove_m21977_gshared ();
extern "C" void Dictionary_2_TryGetValue_m21978_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_VirtualButtonDataU26_t4551 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m21979_gshared ();
extern "C" void Dictionary_2_get_Values_m21980_gshared ();
extern "C" void Dictionary_2_ToTKey_m21981_gshared ();
extern "C" void Dictionary_2_ToTValue_m21982_gshared ();
void* RuntimeInvoker_VirtualButtonData_t643_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ContainsKeyValuePair_m21983_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m21984_gshared ();
void* RuntimeInvoker_Enumerator_t3570 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m21985_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21986_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21987_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21988_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21989_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21990_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3565 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m21991_gshared ();
extern "C" void KeyValuePair_2_get_Key_m21992_gshared ();
extern "C" void KeyValuePair_2_set_Key_m21993_gshared ();
extern "C" void KeyValuePair_2_get_Value_m21994_gshared ();
void* RuntimeInvoker_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m21995_gshared ();
extern "C" void KeyValuePair_2_ToString_m21996_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21997_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21998_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21999_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22000_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22001_gshared ();
extern "C" void KeyCollection__ctor_m22002_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22003_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22004_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22005_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22006_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22007_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22008_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22009_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22010_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22011_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22012_gshared ();
extern "C" void KeyCollection_CopyTo_m22013_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22014_gshared ();
void* RuntimeInvoker_Enumerator_t3569 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22015_gshared ();
extern "C" void Enumerator__ctor_m22016_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22017_gshared ();
extern "C" void Enumerator_Dispose_m22018_gshared ();
extern "C" void Enumerator_MoveNext_m22019_gshared ();
extern "C" void Enumerator_get_Current_m22020_gshared ();
extern "C" void Enumerator__ctor_m22021_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22022_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22023_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22024_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22025_gshared ();
extern "C" void Enumerator_MoveNext_m22026_gshared ();
extern "C" void Enumerator_get_Current_m22027_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22028_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22029_gshared ();
extern "C" void Enumerator_VerifyState_m22030_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22031_gshared ();
extern "C" void Enumerator_Dispose_m22032_gshared ();
extern "C" void Transform_1__ctor_m22033_gshared ();
extern "C" void Transform_1_Invoke_m22034_gshared ();
extern "C" void Transform_1_BeginInvoke_m22035_gshared ();
void* RuntimeInvoker_Object_t_Int32_t127_VirtualButtonData_t643_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22036_gshared ();
extern "C" void ValueCollection__ctor_m22037_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22038_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22039_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22040_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22041_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22042_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22043_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22044_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22045_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22046_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22047_gshared ();
extern "C" void ValueCollection_CopyTo_m22048_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22049_gshared ();
void* RuntimeInvoker_Enumerator_t3573 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22050_gshared ();
extern "C" void Enumerator__ctor_m22051_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22052_gshared ();
extern "C" void Enumerator_Dispose_m22053_gshared ();
extern "C" void Enumerator_MoveNext_m22054_gshared ();
extern "C" void Enumerator_get_Current_m22055_gshared ();
extern "C" void Transform_1__ctor_m22056_gshared ();
extern "C" void Transform_1_Invoke_m22057_gshared ();
extern "C" void Transform_1_BeginInvoke_m22058_gshared ();
extern "C" void Transform_1_EndInvoke_m22059_gshared ();
extern "C" void Transform_1__ctor_m22060_gshared ();
extern "C" void Transform_1_Invoke_m22061_gshared ();
extern "C" void Transform_1_BeginInvoke_m22062_gshared ();
extern "C" void Transform_1_EndInvoke_m22063_gshared ();
extern "C" void Transform_1__ctor_m22064_gshared ();
extern "C" void Transform_1_Invoke_m22065_gshared ();
extern "C" void Transform_1_BeginInvoke_m22066_gshared ();
extern "C" void Transform_1_EndInvoke_m22067_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3565_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22068_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22069_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22070_gshared ();
extern "C" void ShimEnumerator_get_Key_m22071_gshared ();
extern "C" void ShimEnumerator_get_Value_m22072_gshared ();
extern "C" void ShimEnumerator_get_Current_m22073_gshared ();
extern "C" void EqualityComparer_1__ctor_m22074_gshared ();
extern "C" void EqualityComparer_1__cctor_m22075_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22076_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22077_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22078_gshared ();
extern "C" void DefaultComparer__ctor_m22079_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22080_gshared ();
extern "C" void DefaultComparer_Equals_m22081_gshared ();
void* RuntimeInvoker_Boolean_t169_VirtualButtonData_t643_VirtualButtonData_t643 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m22174_gshared ();
extern "C" void List_1__ctor_m22175_gshared ();
extern "C" void List_1__cctor_m22176_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22177_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22178_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m22179_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m22180_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m22181_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m22182_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m22183_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m22184_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22185_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m22186_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m22187_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m22188_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m22189_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m22190_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m22191_gshared ();
extern "C" void List_1_Add_m22192_gshared ();
extern "C" void List_1_GrowIfNeeded_m22193_gshared ();
extern "C" void List_1_AddCollection_m22194_gshared ();
extern "C" void List_1_AddEnumerable_m22195_gshared ();
extern "C" void List_1_AddRange_m22196_gshared ();
extern "C" void List_1_AsReadOnly_m22197_gshared ();
extern "C" void List_1_Clear_m22198_gshared ();
extern "C" void List_1_Contains_m22199_gshared ();
extern "C" void List_1_CopyTo_m22200_gshared ();
extern "C" void List_1_Find_m22201_gshared ();
void* RuntimeInvoker_TargetSearchResult_t720_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m22202_gshared ();
extern "C" void List_1_GetIndex_m22203_gshared ();
extern "C" void List_1_GetEnumerator_m22204_gshared ();
void* RuntimeInvoker_Enumerator_t3585 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m22205_gshared ();
extern "C" void List_1_Shift_m22206_gshared ();
extern "C" void List_1_CheckIndex_m22207_gshared ();
extern "C" void List_1_Insert_m22208_gshared ();
extern "C" void List_1_CheckCollection_m22209_gshared ();
extern "C" void List_1_Remove_m22210_gshared ();
extern "C" void List_1_RemoveAll_m22211_gshared ();
extern "C" void List_1_RemoveAt_m22212_gshared ();
extern "C" void List_1_Reverse_m22213_gshared ();
extern "C" void List_1_Sort_m22214_gshared ();
extern "C" void List_1_Sort_m22215_gshared ();
extern "C" void List_1_ToArray_m22216_gshared ();
extern "C" void List_1_TrimExcess_m22217_gshared ();
extern "C" void List_1_get_Capacity_m22218_gshared ();
extern "C" void List_1_set_Capacity_m22219_gshared ();
extern "C" void List_1_get_Count_m22220_gshared ();
extern "C" void List_1_get_Item_m22221_gshared ();
extern "C" void List_1_set_Item_m22222_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22223_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22224_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22225_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22226_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22227_gshared ();
void* RuntimeInvoker_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m22228_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22229_gshared ();
extern "C" void Enumerator_Dispose_m22230_gshared ();
extern "C" void Enumerator_VerifyState_m22231_gshared ();
extern "C" void Enumerator_MoveNext_m22232_gshared ();
extern "C" void Enumerator_get_Current_m22233_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m22234_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22235_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22236_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22237_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22238_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22239_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22240_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22241_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22242_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22243_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22244_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m22245_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m22246_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m22247_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22248_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m22249_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m22250_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22251_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22252_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22253_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22254_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22255_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m22256_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m22257_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m22258_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m22259_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m22260_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m22261_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m22262_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m22263_gshared ();
extern "C" void Collection_1__ctor_m22264_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22265_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22266_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m22267_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m22268_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m22269_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m22270_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m22271_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m22272_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m22273_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m22274_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m22275_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m22276_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m22277_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m22278_gshared ();
extern "C" void Collection_1_Add_m22279_gshared ();
extern "C" void Collection_1_Clear_m22280_gshared ();
extern "C" void Collection_1_ClearItems_m22281_gshared ();
extern "C" void Collection_1_Contains_m22282_gshared ();
extern "C" void Collection_1_CopyTo_m22283_gshared ();
extern "C" void Collection_1_GetEnumerator_m22284_gshared ();
extern "C" void Collection_1_IndexOf_m22285_gshared ();
extern "C" void Collection_1_Insert_m22286_gshared ();
extern "C" void Collection_1_InsertItem_m22287_gshared ();
extern "C" void Collection_1_Remove_m22288_gshared ();
extern "C" void Collection_1_RemoveAt_m22289_gshared ();
extern "C" void Collection_1_RemoveItem_m22290_gshared ();
extern "C" void Collection_1_get_Count_m22291_gshared ();
extern "C" void Collection_1_get_Item_m22292_gshared ();
extern "C" void Collection_1_set_Item_m22293_gshared ();
extern "C" void Collection_1_SetItem_m22294_gshared ();
extern "C" void Collection_1_IsValidItem_m22295_gshared ();
extern "C" void Collection_1_ConvertItem_m22296_gshared ();
extern "C" void Collection_1_CheckWritable_m22297_gshared ();
extern "C" void Collection_1_IsSynchronized_m22298_gshared ();
extern "C" void Collection_1_IsFixedSize_m22299_gshared ();
extern "C" void EqualityComparer_1__ctor_m22300_gshared ();
extern "C" void EqualityComparer_1__cctor_m22301_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22302_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22303_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22304_gshared ();
extern "C" void DefaultComparer__ctor_m22305_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22306_gshared ();
extern "C" void DefaultComparer_Equals_m22307_gshared ();
void* RuntimeInvoker_Boolean_t169_TargetSearchResult_t720_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m22308_gshared ();
extern "C" void Predicate_1_Invoke_m22309_gshared ();
extern "C" void Predicate_1_BeginInvoke_m22310_gshared ();
void* RuntimeInvoker_Object_t_TargetSearchResult_t720_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m22311_gshared ();
extern "C" void Comparer_1__ctor_m22312_gshared ();
extern "C" void Comparer_1__cctor_m22313_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22314_gshared ();
extern "C" void Comparer_1_get_Default_m22315_gshared ();
extern "C" void DefaultComparer__ctor_m22316_gshared ();
extern "C" void DefaultComparer_Compare_m22317_gshared ();
void* RuntimeInvoker_Int32_t127_TargetSearchResult_t720_TargetSearchResult_t720 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m22318_gshared ();
extern "C" void Comparison_1_Invoke_m22319_gshared ();
extern "C" void Comparison_1_BeginInvoke_m22320_gshared ();
void* RuntimeInvoker_Object_t_TargetSearchResult_t720_TargetSearchResult_t720_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m22321_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22422_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22423_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22424_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22425_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22426_gshared ();
void* RuntimeInvoker_WebCamDevice_t879 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m22428_gshared ();
extern "C" void Dictionary_2__ctor_m22430_gshared ();
extern "C" void Dictionary_2__ctor_m22432_gshared ();
extern "C" void Dictionary_2__ctor_m22434_gshared ();
extern "C" void Dictionary_2__ctor_m22436_gshared ();
extern "C" void Dictionary_2__ctor_m22438_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m22444_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22446_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22448_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m22450_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22452_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22468_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474_gshared ();
extern "C" void Dictionary_2_get_Count_m22476_gshared ();
extern "C" void Dictionary_2_get_Item_m22478_gshared ();
void* RuntimeInvoker_ProfileData_t734_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m22480_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m22482_gshared ();
extern "C" void Dictionary_2_InitArrays_m22484_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22486_gshared ();
extern "C" void Dictionary_2_make_pair_m22488_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3604_Object_t_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m22490_gshared ();
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m22492_gshared ();
void* RuntimeInvoker_ProfileData_t734_Object_t_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m22494_gshared ();
extern "C" void Dictionary_2_Resize_m22496_gshared ();
extern "C" void Dictionary_2_Add_m22498_gshared ();
extern "C" void Dictionary_2_Clear_m22500_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22502_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22504_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22506_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22508_gshared ();
extern "C" void Dictionary_2_Remove_m22510_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22512_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_ProfileDataU26_t4552 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m22514_gshared ();
extern "C" void Dictionary_2_get_Values_m22516_gshared ();
extern "C" void Dictionary_2_ToTKey_m22518_gshared ();
extern "C" void Dictionary_2_ToTValue_m22520_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m22522_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22524_gshared ();
void* RuntimeInvoker_Enumerator_t3609 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22526_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22527_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22528_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22529_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22530_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22531_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3604 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m22532_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22533_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22534_gshared ();
extern "C" void KeyValuePair_2_get_Value_m22535_gshared ();
void* RuntimeInvoker_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m22536_gshared ();
extern "C" void KeyValuePair_2_ToString_m22537_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22538_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22539_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22540_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22541_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22542_gshared ();
extern "C" void KeyCollection__ctor_m22543_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22544_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22545_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22546_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22547_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22548_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22549_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22550_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22551_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22552_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22553_gshared ();
extern "C" void KeyCollection_CopyTo_m22554_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22555_gshared ();
void* RuntimeInvoker_Enumerator_t3608 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22556_gshared ();
extern "C" void Enumerator__ctor_m22557_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22558_gshared ();
extern "C" void Enumerator_Dispose_m22559_gshared ();
extern "C" void Enumerator_MoveNext_m22560_gshared ();
extern "C" void Enumerator_get_Current_m22561_gshared ();
extern "C" void Enumerator__ctor_m22562_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22563_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566_gshared ();
extern "C" void Enumerator_MoveNext_m22567_gshared ();
extern "C" void Enumerator_get_Current_m22568_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22569_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22570_gshared ();
extern "C" void Enumerator_VerifyState_m22571_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22572_gshared ();
extern "C" void Enumerator_Dispose_m22573_gshared ();
extern "C" void Transform_1__ctor_m22574_gshared ();
extern "C" void Transform_1_Invoke_m22575_gshared ();
extern "C" void Transform_1_BeginInvoke_m22576_gshared ();
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t734_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22577_gshared ();
extern "C" void ValueCollection__ctor_m22578_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22579_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22580_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22581_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22582_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22583_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22584_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22585_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22586_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22587_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22588_gshared ();
extern "C" void ValueCollection_CopyTo_m22589_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22590_gshared ();
void* RuntimeInvoker_Enumerator_t3612 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22591_gshared ();
extern "C" void Enumerator__ctor_m22592_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22593_gshared ();
extern "C" void Enumerator_Dispose_m22594_gshared ();
extern "C" void Enumerator_MoveNext_m22595_gshared ();
extern "C" void Enumerator_get_Current_m22596_gshared ();
extern "C" void Transform_1__ctor_m22597_gshared ();
extern "C" void Transform_1_Invoke_m22598_gshared ();
extern "C" void Transform_1_BeginInvoke_m22599_gshared ();
extern "C" void Transform_1_EndInvoke_m22600_gshared ();
extern "C" void Transform_1__ctor_m22601_gshared ();
extern "C" void Transform_1_Invoke_m22602_gshared ();
extern "C" void Transform_1_BeginInvoke_m22603_gshared ();
extern "C" void Transform_1_EndInvoke_m22604_gshared ();
extern "C" void Transform_1__ctor_m22605_gshared ();
extern "C" void Transform_1_Invoke_m22606_gshared ();
extern "C" void Transform_1_BeginInvoke_m22607_gshared ();
extern "C" void Transform_1_EndInvoke_m22608_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3604_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22609_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22610_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22611_gshared ();
extern "C" void ShimEnumerator_get_Key_m22612_gshared ();
extern "C" void ShimEnumerator_get_Value_m22613_gshared ();
extern "C" void ShimEnumerator_get_Current_m22614_gshared ();
extern "C" void EqualityComparer_1__ctor_m22615_gshared ();
extern "C" void EqualityComparer_1__cctor_m22616_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22617_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22618_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22619_gshared ();
extern "C" void DefaultComparer__ctor_m22620_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22621_gshared ();
extern "C" void DefaultComparer_Equals_m22622_gshared ();
void* RuntimeInvoker_Boolean_t169_ProfileData_t734_ProfileData_t734 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m23184_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23185_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23186_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23187_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23188_gshared ();
void* RuntimeInvoker_Link_t3653 (const MethodInfo* method, void* obj, void** args);
extern "C" void TweenerCore_3__ctor_m23293_gshared ();
extern "C" void TweenerCore_3_Reset_m23294_gshared ();
extern "C" void TweenerCore_3_Validate_m23295_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23296_gshared ();
extern "C" void TweenerCore_3_Startup_m23297_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23298_gshared ();
extern "C" void DOGetter_1__ctor_m23299_gshared ();
extern "C" void DOGetter_1_Invoke_m23300_gshared ();
void* RuntimeInvoker_UInt32_t1075 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23301_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23302_gshared ();
void* RuntimeInvoker_UInt32_t1075_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23303_gshared ();
extern "C" void DOSetter_1_Invoke_m23304_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23305_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23306_gshared ();
extern "C" void TweenerCore_3__ctor_m23307_gshared ();
extern "C" void TweenerCore_3_Reset_m23308_gshared ();
extern "C" void TweenerCore_3_Validate_m23309_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23310_gshared ();
extern "C" void TweenerCore_3_Startup_m23311_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23312_gshared ();
extern "C" void DOGetter_1__ctor_m23313_gshared ();
extern "C" void DOGetter_1_Invoke_m23314_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23315_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23316_gshared ();
void* RuntimeInvoker_Vector2_t10_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23317_gshared ();
extern "C" void DOSetter_1_Invoke_m23318_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23319_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23320_gshared ();
extern "C" void TweenCallback_1__ctor_m23414_gshared ();
extern "C" void TweenCallback_1_Invoke_m23415_gshared ();
extern "C" void TweenCallback_1_BeginInvoke_m23416_gshared ();
extern "C" void TweenCallback_1_EndInvoke_m23417_gshared ();
extern "C" void TweenerCore_3__ctor_m23612_gshared ();
extern "C" void TweenerCore_3_Reset_m23613_gshared ();
extern "C" void TweenerCore_3_Validate_m23614_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23615_gshared ();
extern "C" void TweenerCore_3_Startup_m23616_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23617_gshared ();
extern "C" void TweenerCore_3__ctor_m23632_gshared ();
extern "C" void TweenerCore_3_Reset_m23633_gshared ();
extern "C" void TweenerCore_3_Validate_m23634_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23635_gshared ();
extern "C" void TweenerCore_3_Startup_m23636_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23637_gshared ();
extern "C" void DOGetter_1__ctor_m23638_gshared ();
extern "C" void DOGetter_1_Invoke_m23639_gshared ();
void* RuntimeInvoker_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23640_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23641_gshared ();
void* RuntimeInvoker_Color2_t1000_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23642_gshared ();
extern "C" void DOSetter_1_Invoke_m23643_gshared ();
void* RuntimeInvoker_Void_t168_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m23644_gshared ();
void* RuntimeInvoker_Object_t_Color2_t1000_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23645_gshared ();
extern "C" void TweenerCore_3__ctor_m23659_gshared ();
extern "C" void TweenerCore_3_Reset_m23660_gshared ();
extern "C" void TweenerCore_3_Validate_m23661_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23662_gshared ();
extern "C" void TweenerCore_3_Startup_m23663_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23664_gshared ();
extern "C" void DOGetter_1__ctor_m23665_gshared ();
extern "C" void DOGetter_1_Invoke_m23666_gshared ();
void* RuntimeInvoker_Rect_t124 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23667_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23668_gshared ();
void* RuntimeInvoker_Rect_t124_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23669_gshared ();
extern "C" void DOSetter_1_Invoke_m23670_gshared ();
void* RuntimeInvoker_Void_t168_Rect_t124 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m23671_gshared ();
void* RuntimeInvoker_Object_t_Rect_t124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23672_gshared ();
extern "C" void TweenerCore_3__ctor_m23673_gshared ();
extern "C" void TweenerCore_3_Reset_m23674_gshared ();
extern "C" void TweenerCore_3_Validate_m23675_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23676_gshared ();
extern "C" void TweenerCore_3_Startup_m23677_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23678_gshared ();
extern "C" void DOGetter_1__ctor_m23679_gshared ();
extern "C" void DOGetter_1_Invoke_m23680_gshared ();
void* RuntimeInvoker_UInt64_t1091 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23681_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23682_gshared ();
void* RuntimeInvoker_UInt64_t1091_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23683_gshared ();
extern "C" void DOSetter_1_Invoke_m23684_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23685_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1092_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23686_gshared ();
extern "C" void TweenerCore_3__ctor_m23687_gshared ();
extern "C" void TweenerCore_3_Reset_m23688_gshared ();
extern "C" void TweenerCore_3_Validate_m23689_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23690_gshared ();
extern "C" void TweenerCore_3_Startup_m23691_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23692_gshared ();
extern "C" void DOGetter_1__ctor_m23693_gshared ();
extern "C" void DOGetter_1_Invoke_m23694_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23695_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23696_gshared ();
extern "C" void DOSetter_1__ctor_m23697_gshared ();
extern "C" void DOSetter_1_Invoke_m23698_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23699_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23700_gshared ();
extern "C" void TweenerCore_3__ctor_m23702_gshared ();
extern "C" void TweenerCore_3_Reset_m23703_gshared ();
extern "C" void TweenerCore_3_Validate_m23704_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23705_gshared ();
extern "C" void TweenerCore_3_Startup_m23706_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23707_gshared ();
extern "C" void List_1__ctor_m23724_gshared ();
extern "C" void List_1__ctor_m23726_gshared ();
extern "C" void List_1__cctor_m23728_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23732_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m23734_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m23736_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m23738_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m23740_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m23742_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m23744_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m23748_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m23750_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m23752_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m23754_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m23756_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m23758_gshared ();
extern "C" void List_1_Add_m23760_gshared ();
extern "C" void List_1_GrowIfNeeded_m23762_gshared ();
extern "C" void List_1_AddCollection_m23764_gshared ();
extern "C" void List_1_AddEnumerable_m23766_gshared ();
extern "C" void List_1_AddRange_m23768_gshared ();
extern "C" void List_1_AsReadOnly_m23770_gshared ();
extern "C" void List_1_Clear_m23772_gshared ();
extern "C" void List_1_Contains_m23774_gshared ();
extern "C" void List_1_CopyTo_m23776_gshared ();
extern "C" void List_1_Find_m23778_gshared ();
extern "C" void List_1_CheckMatch_m23780_gshared ();
extern "C" void List_1_GetIndex_m23782_gshared ();
extern "C" void List_1_GetEnumerator_m23784_gshared ();
void* RuntimeInvoker_Enumerator_t3688 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m23786_gshared ();
extern "C" void List_1_Shift_m23788_gshared ();
extern "C" void List_1_CheckIndex_m23790_gshared ();
extern "C" void List_1_Insert_m23792_gshared ();
extern "C" void List_1_CheckCollection_m23794_gshared ();
extern "C" void List_1_Remove_m23796_gshared ();
extern "C" void List_1_RemoveAll_m23798_gshared ();
extern "C" void List_1_RemoveAt_m23800_gshared ();
extern "C" void List_1_Reverse_m23802_gshared ();
extern "C" void List_1_Sort_m23804_gshared ();
extern "C" void List_1_Sort_m23806_gshared ();
extern "C" void List_1_ToArray_m23808_gshared ();
extern "C" void List_1_TrimExcess_m23810_gshared ();
extern "C" void List_1_get_Capacity_m23812_gshared ();
extern "C" void List_1_set_Capacity_m23814_gshared ();
extern "C" void List_1_get_Count_m23816_gshared ();
extern "C" void List_1_get_Item_m23818_gshared ();
extern "C" void List_1_set_Item_m23820_gshared ();
extern "C" void Enumerator__ctor_m23821_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m23822_gshared ();
extern "C" void Enumerator_Dispose_m23823_gshared ();
extern "C" void Enumerator_VerifyState_m23824_gshared ();
extern "C" void Enumerator_MoveNext_m23825_gshared ();
extern "C" void Enumerator_get_Current_m23826_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m23827_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23828_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23829_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23830_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23831_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23832_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23833_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23834_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23835_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23836_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23837_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m23838_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23839_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m23840_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23841_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23842_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23843_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23844_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23845_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23846_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23847_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23848_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m23849_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23850_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m23851_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m23852_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m23853_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m23854_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m23855_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m23856_gshared ();
extern "C" void Collection_1__ctor_m23857_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23858_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23859_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m23860_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m23861_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m23862_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m23863_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m23864_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m23865_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m23866_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m23867_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m23868_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m23869_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m23870_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m23871_gshared ();
extern "C" void Collection_1_Add_m23872_gshared ();
extern "C" void Collection_1_Clear_m23873_gshared ();
extern "C" void Collection_1_ClearItems_m23874_gshared ();
extern "C" void Collection_1_Contains_m23875_gshared ();
extern "C" void Collection_1_CopyTo_m23876_gshared ();
extern "C" void Collection_1_GetEnumerator_m23877_gshared ();
extern "C" void Collection_1_IndexOf_m23878_gshared ();
extern "C" void Collection_1_Insert_m23879_gshared ();
extern "C" void Collection_1_InsertItem_m23880_gshared ();
extern "C" void Collection_1_Remove_m23881_gshared ();
extern "C" void Collection_1_RemoveAt_m23882_gshared ();
extern "C" void Collection_1_RemoveItem_m23883_gshared ();
extern "C" void Collection_1_get_Count_m23884_gshared ();
extern "C" void Collection_1_get_Item_m23885_gshared ();
extern "C" void Collection_1_set_Item_m23886_gshared ();
extern "C" void Collection_1_SetItem_m23887_gshared ();
extern "C" void Collection_1_IsValidItem_m23888_gshared ();
extern "C" void Collection_1_ConvertItem_m23889_gshared ();
extern "C" void Collection_1_CheckWritable_m23890_gshared ();
extern "C" void Collection_1_IsSynchronized_m23891_gshared ();
extern "C" void Collection_1_IsFixedSize_m23892_gshared ();
extern "C" void Predicate_1__ctor_m23893_gshared ();
extern "C" void Predicate_1_Invoke_m23894_gshared ();
extern "C" void Predicate_1_BeginInvoke_m23895_gshared ();
void* RuntimeInvoker_Object_t_Int16_t534_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m23896_gshared ();
extern "C" void Comparer_1__ctor_m23897_gshared ();
extern "C" void Comparer_1__cctor_m23898_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m23899_gshared ();
extern "C" void Comparer_1_get_Default_m23900_gshared ();
extern "C" void GenericComparer_1__ctor_m23901_gshared ();
extern "C" void GenericComparer_1_Compare_m23902_gshared ();
void* RuntimeInvoker_Int32_t127_Int16_t534_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m23903_gshared ();
extern "C" void DefaultComparer_Compare_m23904_gshared ();
extern "C" void Comparison_1__ctor_m23905_gshared ();
extern "C" void Comparison_1_Invoke_m23906_gshared ();
extern "C" void Comparison_1_BeginInvoke_m23907_gshared ();
void* RuntimeInvoker_Object_t_Int16_t534_Int16_t534_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m23908_gshared ();
extern "C" void TweenerCore_3__ctor_m23953_gshared ();
extern "C" void TweenerCore_3_Reset_m23954_gshared ();
extern "C" void TweenerCore_3_Validate_m23955_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23956_gshared ();
extern "C" void TweenerCore_3_Startup_m23957_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23958_gshared ();
extern "C" void DOGetter_1__ctor_m23959_gshared ();
extern "C" void DOGetter_1_Invoke_m23960_gshared ();
void* RuntimeInvoker_Vector4_t413 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m23961_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23962_gshared ();
void* RuntimeInvoker_Vector4_t413_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23963_gshared ();
extern "C" void DOSetter_1_Invoke_m23964_gshared ();
void* RuntimeInvoker_Void_t168_Vector4_t413 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_BeginInvoke_m23965_gshared ();
void* RuntimeInvoker_Object_t_Vector4_t413_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1_EndInvoke_m23966_gshared ();
extern "C" void TweenerCore_3__ctor_m23967_gshared ();
extern "C" void TweenerCore_3_Reset_m23968_gshared ();
extern "C" void TweenerCore_3_Validate_m23969_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23970_gshared ();
extern "C" void TweenerCore_3_Startup_m23971_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23972_gshared ();
extern "C" void DOGetter_1__ctor_m23973_gshared ();
extern "C" void DOGetter_1_Invoke_m23974_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23975_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23976_gshared ();
void* RuntimeInvoker_Color_t90_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m23977_gshared ();
extern "C" void DOSetter_1_Invoke_m23978_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23979_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23980_gshared ();
extern "C" void TweenerCore_3__ctor_m23981_gshared ();
extern "C" void TweenerCore_3_Reset_m23982_gshared ();
extern "C" void TweenerCore_3_Validate_m23983_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23984_gshared ();
extern "C" void TweenerCore_3_Startup_m23985_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m23986_gshared ();
extern "C" void DOGetter_1__ctor_m23987_gshared ();
extern "C" void DOGetter_1_Invoke_m23988_gshared ();
extern "C" void DOGetter_1_BeginInvoke_m23989_gshared ();
extern "C" void DOGetter_1_EndInvoke_m23990_gshared ();
extern "C" void DOSetter_1__ctor_m23991_gshared ();
extern "C" void DOSetter_1_Invoke_m23992_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m23993_gshared ();
extern "C" void DOSetter_1_EndInvoke_m23994_gshared ();
extern "C" void TweenerCore_3__ctor_m23995_gshared ();
extern "C" void TweenerCore_3_Reset_m23996_gshared ();
extern "C" void TweenerCore_3_Validate_m23997_gshared ();
extern "C" void TweenerCore_3_UpdateDelay_m23998_gshared ();
extern "C" void TweenerCore_3_Startup_m23999_gshared ();
extern "C" void TweenerCore_3_ApplyTween_m24000_gshared ();
extern "C" void DOGetter_1__ctor_m24001_gshared ();
extern "C" void DOGetter_1_Invoke_m24002_gshared ();
void* RuntimeInvoker_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void DOGetter_1_BeginInvoke_m24003_gshared ();
extern "C" void DOGetter_1_EndInvoke_m24004_gshared ();
void* RuntimeInvoker_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void DOSetter_1__ctor_m24005_gshared ();
extern "C" void DOSetter_1_Invoke_m24006_gshared ();
extern "C" void DOSetter_1_BeginInvoke_m24007_gshared ();
extern "C" void DOSetter_1_EndInvoke_m24008_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24145_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24146_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24147_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24148_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24149_gshared ();
void* RuntimeInvoker_GcAchievementData_t1305 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24155_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24156_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24157_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24158_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24159_gshared ();
void* RuntimeInvoker_GcScoreData_t1306 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24757_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24758_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24759_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24760_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24761_gshared ();
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24855_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24856_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24857_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24858_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24859_gshared ();
void* RuntimeInvoker_Keyframe_t1236 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m24860_gshared ();
extern "C" void List_1__ctor_m24861_gshared ();
extern "C" void List_1__cctor_m24862_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24863_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24864_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m24865_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m24866_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m24867_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m24868_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m24869_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m24870_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24871_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m24872_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m24873_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m24874_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m24875_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m24876_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m24877_gshared ();
extern "C" void List_1_Add_m24878_gshared ();
extern "C" void List_1_GrowIfNeeded_m24879_gshared ();
extern "C" void List_1_AddCollection_m24880_gshared ();
extern "C" void List_1_AddEnumerable_m24881_gshared ();
extern "C" void List_1_AddRange_m24882_gshared ();
extern "C" void List_1_AsReadOnly_m24883_gshared ();
extern "C" void List_1_Clear_m24884_gshared ();
extern "C" void List_1_Contains_m24885_gshared ();
extern "C" void List_1_CopyTo_m24886_gshared ();
extern "C" void List_1_Find_m24887_gshared ();
void* RuntimeInvoker_UICharInfo_t460_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m24888_gshared ();
extern "C" void List_1_GetIndex_m24889_gshared ();
extern "C" void List_1_GetEnumerator_m24890_gshared ();
void* RuntimeInvoker_Enumerator_t3756 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m24891_gshared ();
extern "C" void List_1_Shift_m24892_gshared ();
extern "C" void List_1_CheckIndex_m24893_gshared ();
extern "C" void List_1_Insert_m24894_gshared ();
extern "C" void List_1_CheckCollection_m24895_gshared ();
extern "C" void List_1_Remove_m24896_gshared ();
extern "C" void List_1_RemoveAll_m24897_gshared ();
extern "C" void List_1_RemoveAt_m24898_gshared ();
extern "C" void List_1_Reverse_m24899_gshared ();
extern "C" void List_1_Sort_m24900_gshared ();
extern "C" void List_1_Sort_m24901_gshared ();
extern "C" void List_1_ToArray_m24902_gshared ();
extern "C" void List_1_TrimExcess_m24903_gshared ();
extern "C" void List_1_get_Capacity_m24904_gshared ();
extern "C" void List_1_set_Capacity_m24905_gshared ();
extern "C" void List_1_get_Count_m24906_gshared ();
extern "C" void List_1_get_Item_m24907_gshared ();
extern "C" void List_1_set_Item_m24908_gshared ();
extern "C" void Enumerator__ctor_m24909_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24910_gshared ();
extern "C" void Enumerator_Dispose_m24911_gshared ();
extern "C" void Enumerator_VerifyState_m24912_gshared ();
extern "C" void Enumerator_MoveNext_m24913_gshared ();
extern "C" void Enumerator_get_Current_m24914_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m24915_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24916_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24917_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24918_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24919_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24920_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24921_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24922_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24923_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24924_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24925_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m24926_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m24927_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m24928_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24929_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m24930_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m24931_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24932_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24933_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24934_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24935_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24936_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m24937_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m24938_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m24939_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m24940_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m24941_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m24942_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m24943_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m24944_gshared ();
extern "C" void Collection_1__ctor_m24945_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24946_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24947_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m24948_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m24949_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m24950_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m24951_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m24952_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m24953_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m24954_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m24955_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m24956_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m24957_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m24958_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m24959_gshared ();
extern "C" void Collection_1_Add_m24960_gshared ();
extern "C" void Collection_1_Clear_m24961_gshared ();
extern "C" void Collection_1_ClearItems_m24962_gshared ();
extern "C" void Collection_1_Contains_m24963_gshared ();
extern "C" void Collection_1_CopyTo_m24964_gshared ();
extern "C" void Collection_1_GetEnumerator_m24965_gshared ();
extern "C" void Collection_1_IndexOf_m24966_gshared ();
extern "C" void Collection_1_Insert_m24967_gshared ();
extern "C" void Collection_1_InsertItem_m24968_gshared ();
extern "C" void Collection_1_Remove_m24969_gshared ();
extern "C" void Collection_1_RemoveAt_m24970_gshared ();
extern "C" void Collection_1_RemoveItem_m24971_gshared ();
extern "C" void Collection_1_get_Count_m24972_gshared ();
extern "C" void Collection_1_get_Item_m24973_gshared ();
extern "C" void Collection_1_set_Item_m24974_gshared ();
extern "C" void Collection_1_SetItem_m24975_gshared ();
extern "C" void Collection_1_IsValidItem_m24976_gshared ();
extern "C" void Collection_1_ConvertItem_m24977_gshared ();
extern "C" void Collection_1_CheckWritable_m24978_gshared ();
extern "C" void Collection_1_IsSynchronized_m24979_gshared ();
extern "C" void Collection_1_IsFixedSize_m24980_gshared ();
extern "C" void EqualityComparer_1__ctor_m24981_gshared ();
extern "C" void EqualityComparer_1__cctor_m24982_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24983_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24984_gshared ();
extern "C" void EqualityComparer_1_get_Default_m24985_gshared ();
extern "C" void DefaultComparer__ctor_m24986_gshared ();
extern "C" void DefaultComparer_GetHashCode_m24987_gshared ();
extern "C" void DefaultComparer_Equals_m24988_gshared ();
void* RuntimeInvoker_Boolean_t169_UICharInfo_t460_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m24989_gshared ();
extern "C" void Predicate_1_Invoke_m24990_gshared ();
extern "C" void Predicate_1_BeginInvoke_m24991_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t460_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m24992_gshared ();
extern "C" void Comparer_1__ctor_m24993_gshared ();
extern "C" void Comparer_1__cctor_m24994_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m24995_gshared ();
extern "C" void Comparer_1_get_Default_m24996_gshared ();
extern "C" void DefaultComparer__ctor_m24997_gshared ();
extern "C" void DefaultComparer_Compare_m24998_gshared ();
void* RuntimeInvoker_Int32_t127_UICharInfo_t460_UICharInfo_t460 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m24999_gshared ();
extern "C" void Comparison_1_Invoke_m25000_gshared ();
extern "C" void Comparison_1_BeginInvoke_m25001_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t460_UICharInfo_t460_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m25002_gshared ();
extern "C" void List_1__ctor_m25003_gshared ();
extern "C" void List_1__ctor_m25004_gshared ();
extern "C" void List_1__cctor_m25005_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25006_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25007_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m25008_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m25009_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m25010_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m25011_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m25012_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m25013_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25014_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m25015_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m25016_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m25017_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m25018_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m25019_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m25020_gshared ();
extern "C" void List_1_Add_m25021_gshared ();
extern "C" void List_1_GrowIfNeeded_m25022_gshared ();
extern "C" void List_1_AddCollection_m25023_gshared ();
extern "C" void List_1_AddEnumerable_m25024_gshared ();
extern "C" void List_1_AddRange_m25025_gshared ();
extern "C" void List_1_AsReadOnly_m25026_gshared ();
extern "C" void List_1_Clear_m25027_gshared ();
extern "C" void List_1_Contains_m25028_gshared ();
extern "C" void List_1_CopyTo_m25029_gshared ();
extern "C" void List_1_Find_m25030_gshared ();
void* RuntimeInvoker_UILineInfo_t458_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m25031_gshared ();
extern "C" void List_1_GetIndex_m25032_gshared ();
extern "C" void List_1_GetEnumerator_m25033_gshared ();
void* RuntimeInvoker_Enumerator_t3765 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m25034_gshared ();
extern "C" void List_1_Shift_m25035_gshared ();
extern "C" void List_1_CheckIndex_m25036_gshared ();
extern "C" void List_1_Insert_m25037_gshared ();
extern "C" void List_1_CheckCollection_m25038_gshared ();
extern "C" void List_1_Remove_m25039_gshared ();
extern "C" void List_1_RemoveAll_m25040_gshared ();
extern "C" void List_1_RemoveAt_m25041_gshared ();
extern "C" void List_1_Reverse_m25042_gshared ();
extern "C" void List_1_Sort_m25043_gshared ();
extern "C" void List_1_Sort_m25044_gshared ();
extern "C" void List_1_ToArray_m25045_gshared ();
extern "C" void List_1_TrimExcess_m25046_gshared ();
extern "C" void List_1_get_Capacity_m25047_gshared ();
extern "C" void List_1_set_Capacity_m25048_gshared ();
extern "C" void List_1_get_Count_m25049_gshared ();
extern "C" void List_1_get_Item_m25050_gshared ();
extern "C" void List_1_set_Item_m25051_gshared ();
extern "C" void Enumerator__ctor_m25052_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25053_gshared ();
extern "C" void Enumerator_Dispose_m25054_gshared ();
extern "C" void Enumerator_VerifyState_m25055_gshared ();
extern "C" void Enumerator_MoveNext_m25056_gshared ();
extern "C" void Enumerator_get_Current_m25057_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m25058_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25059_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25060_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25061_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25062_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25063_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25064_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25065_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25066_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25067_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25068_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m25069_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m25070_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m25071_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25072_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m25073_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m25074_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25075_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25076_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25077_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25078_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25079_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m25080_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m25081_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m25082_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m25083_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m25084_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m25085_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m25086_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m25087_gshared ();
extern "C" void Collection_1__ctor_m25088_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25089_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25090_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m25091_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m25092_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m25093_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m25094_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m25095_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m25096_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m25097_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m25098_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m25099_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m25100_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m25101_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m25102_gshared ();
extern "C" void Collection_1_Add_m25103_gshared ();
extern "C" void Collection_1_Clear_m25104_gshared ();
extern "C" void Collection_1_ClearItems_m25105_gshared ();
extern "C" void Collection_1_Contains_m25106_gshared ();
extern "C" void Collection_1_CopyTo_m25107_gshared ();
extern "C" void Collection_1_GetEnumerator_m25108_gshared ();
extern "C" void Collection_1_IndexOf_m25109_gshared ();
extern "C" void Collection_1_Insert_m25110_gshared ();
extern "C" void Collection_1_InsertItem_m25111_gshared ();
extern "C" void Collection_1_Remove_m25112_gshared ();
extern "C" void Collection_1_RemoveAt_m25113_gshared ();
extern "C" void Collection_1_RemoveItem_m25114_gshared ();
extern "C" void Collection_1_get_Count_m25115_gshared ();
extern "C" void Collection_1_get_Item_m25116_gshared ();
extern "C" void Collection_1_set_Item_m25117_gshared ();
extern "C" void Collection_1_SetItem_m25118_gshared ();
extern "C" void Collection_1_IsValidItem_m25119_gshared ();
extern "C" void Collection_1_ConvertItem_m25120_gshared ();
extern "C" void Collection_1_CheckWritable_m25121_gshared ();
extern "C" void Collection_1_IsSynchronized_m25122_gshared ();
extern "C" void Collection_1_IsFixedSize_m25123_gshared ();
extern "C" void EqualityComparer_1__ctor_m25124_gshared ();
extern "C" void EqualityComparer_1__cctor_m25125_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25126_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25127_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25128_gshared ();
extern "C" void DefaultComparer__ctor_m25129_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25130_gshared ();
extern "C" void DefaultComparer_Equals_m25131_gshared ();
void* RuntimeInvoker_Boolean_t169_UILineInfo_t458_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m25132_gshared ();
extern "C" void Predicate_1_Invoke_m25133_gshared ();
extern "C" void Predicate_1_BeginInvoke_m25134_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t458_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m25135_gshared ();
extern "C" void Comparer_1__ctor_m25136_gshared ();
extern "C" void Comparer_1__cctor_m25137_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m25138_gshared ();
extern "C" void Comparer_1_get_Default_m25139_gshared ();
extern "C" void DefaultComparer__ctor_m25140_gshared ();
extern "C" void DefaultComparer_Compare_m25141_gshared ();
void* RuntimeInvoker_Int32_t127_UILineInfo_t458_UILineInfo_t458 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m25142_gshared ();
extern "C" void Comparison_1_Invoke_m25143_gshared ();
extern "C" void Comparison_1_BeginInvoke_m25144_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t458_UILineInfo_t458_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m25145_gshared ();
extern "C" void Dictionary_2__ctor_m25147_gshared ();
extern "C" void Dictionary_2__ctor_m25149_gshared ();
extern "C" void Dictionary_2__ctor_m25151_gshared ();
extern "C" void Dictionary_2__ctor_m25153_gshared ();
extern "C" void Dictionary_2__ctor_m25155_gshared ();
extern "C" void Dictionary_2__ctor_m25157_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25159_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25161_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25163_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25165_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25167_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m25169_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25171_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25173_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25175_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25177_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25179_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25181_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25183_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25185_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25187_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25189_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25191_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25193_gshared ();
extern "C" void Dictionary_2_get_Count_m25195_gshared ();
extern "C" void Dictionary_2_get_Item_m25197_gshared ();
extern "C" void Dictionary_2_set_Item_m25199_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m25201_gshared ();
extern "C" void Dictionary_2_InitArrays_m25203_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25205_gshared ();
extern "C" void Dictionary_2_make_pair_m25207_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3777_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m25209_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m25211_gshared ();
void* RuntimeInvoker_Int64_t1092_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m25213_gshared ();
extern "C" void Dictionary_2_Resize_m25215_gshared ();
extern "C" void Dictionary_2_Add_m25217_gshared ();
extern "C" void Dictionary_2_Clear_m25219_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25221_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25223_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25225_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25227_gshared ();
extern "C" void Dictionary_2_Remove_m25229_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25231_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_Int64U26_t2703 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m25233_gshared ();
extern "C" void Dictionary_2_get_Values_m25235_gshared ();
extern "C" void Dictionary_2_ToTKey_m25237_gshared ();
extern "C" void Dictionary_2_ToTValue_m25239_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25241_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25243_gshared ();
void* RuntimeInvoker_Enumerator_t3782 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25245_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25246_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25247_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25248_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25249_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25250_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3777 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25251_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25252_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25253_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25254_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25255_gshared ();
extern "C" void KeyValuePair_2_ToString_m25256_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25257_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25258_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25259_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25260_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25261_gshared ();
extern "C" void KeyCollection__ctor_m25262_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25263_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25264_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25265_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25266_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25267_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25268_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25269_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25270_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25271_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25272_gshared ();
extern "C" void KeyCollection_CopyTo_m25273_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25274_gshared ();
void* RuntimeInvoker_Enumerator_t3781 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m25275_gshared ();
extern "C" void Enumerator__ctor_m25276_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25277_gshared ();
extern "C" void Enumerator_Dispose_m25278_gshared ();
extern "C" void Enumerator_MoveNext_m25279_gshared ();
extern "C" void Enumerator_get_Current_m25280_gshared ();
extern "C" void Enumerator__ctor_m25281_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25282_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25283_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25284_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25285_gshared ();
extern "C" void Enumerator_MoveNext_m25286_gshared ();
extern "C" void Enumerator_get_Current_m25287_gshared ();
extern "C" void Enumerator_get_CurrentKey_m25288_gshared ();
extern "C" void Enumerator_get_CurrentValue_m25289_gshared ();
extern "C" void Enumerator_VerifyState_m25290_gshared ();
extern "C" void Enumerator_VerifyCurrent_m25291_gshared ();
extern "C" void Enumerator_Dispose_m25292_gshared ();
extern "C" void Transform_1__ctor_m25293_gshared ();
extern "C" void Transform_1_Invoke_m25294_gshared ();
extern "C" void Transform_1_BeginInvoke_m25295_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m25296_gshared ();
extern "C" void ValueCollection__ctor_m25297_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25298_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25299_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25300_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25301_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25302_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25303_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25304_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25305_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25306_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m25307_gshared ();
extern "C" void ValueCollection_CopyTo_m25308_gshared ();
extern "C" void ValueCollection_GetEnumerator_m25309_gshared ();
void* RuntimeInvoker_Enumerator_t3785 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m25310_gshared ();
extern "C" void Enumerator__ctor_m25311_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25312_gshared ();
extern "C" void Enumerator_Dispose_m25313_gshared ();
extern "C" void Enumerator_MoveNext_m25314_gshared ();
extern "C" void Enumerator_get_Current_m25315_gshared ();
extern "C" void Transform_1__ctor_m25316_gshared ();
extern "C" void Transform_1_Invoke_m25317_gshared ();
extern "C" void Transform_1_BeginInvoke_m25318_gshared ();
extern "C" void Transform_1_EndInvoke_m25319_gshared ();
extern "C" void Transform_1__ctor_m25320_gshared ();
extern "C" void Transform_1_Invoke_m25321_gshared ();
extern "C" void Transform_1_BeginInvoke_m25322_gshared ();
extern "C" void Transform_1_EndInvoke_m25323_gshared ();
extern "C" void Transform_1__ctor_m25324_gshared ();
extern "C" void Transform_1_Invoke_m25325_gshared ();
extern "C" void Transform_1_BeginInvoke_m25326_gshared ();
extern "C" void Transform_1_EndInvoke_m25327_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3777_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m25328_gshared ();
extern "C" void ShimEnumerator_MoveNext_m25329_gshared ();
extern "C" void ShimEnumerator_get_Entry_m25330_gshared ();
extern "C" void ShimEnumerator_get_Key_m25331_gshared ();
extern "C" void ShimEnumerator_get_Value_m25332_gshared ();
extern "C" void ShimEnumerator_get_Current_m25333_gshared ();
extern "C" void EqualityComparer_1__ctor_m25334_gshared ();
extern "C" void EqualityComparer_1__cctor_m25335_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25336_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25337_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25338_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25339_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25340_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25341_gshared ();
void* RuntimeInvoker_Boolean_t169_Int64_t1092_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m25342_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25343_gshared ();
extern "C" void DefaultComparer_Equals_m25344_gshared ();
extern "C" void Dictionary_2__ctor_m25585_gshared ();
extern "C" void Dictionary_2__ctor_m25587_gshared ();
extern "C" void Dictionary_2__ctor_m25589_gshared ();
extern "C" void Dictionary_2__ctor_m25591_gshared ();
extern "C" void Dictionary_2__ctor_m25593_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25595_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25597_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25599_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25601_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25603_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m25605_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25607_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25609_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25611_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25613_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25615_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25617_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25619_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25621_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25623_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25625_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25627_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25629_gshared ();
extern "C" void Dictionary_2_get_Count_m25631_gshared ();
extern "C" void Dictionary_2_get_Item_m25633_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m25635_gshared ();
void* RuntimeInvoker_Void_t168_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m25637_gshared ();
extern "C" void Dictionary_2_InitArrays_m25639_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25641_gshared ();
extern "C" void Dictionary_2_make_pair_m25643_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3815_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m25645_gshared ();
void* RuntimeInvoker_UInt64_t1091_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m25647_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m25649_gshared ();
extern "C" void Dictionary_2_Resize_m25651_gshared ();
extern "C" void Dictionary_2_Add_m25653_gshared ();
extern "C" void Dictionary_2_Clear_m25655_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25657_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25659_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25661_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25663_gshared ();
extern "C" void Dictionary_2_Remove_m25665_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25667_gshared ();
void* RuntimeInvoker_Boolean_t169_Int64_t1092_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m25669_gshared ();
extern "C" void Dictionary_2_get_Values_m25671_gshared ();
extern "C" void Dictionary_2_ToTKey_m25673_gshared ();
extern "C" void Dictionary_2_ToTValue_m25675_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25677_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25679_gshared ();
void* RuntimeInvoker_Enumerator_t3820 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25681_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25682_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25683_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25684_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25685_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25686_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3815 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25687_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25688_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25689_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25690_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25691_gshared ();
extern "C" void KeyValuePair_2_ToString_m25692_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25693_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25694_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25695_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25696_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25697_gshared ();
extern "C" void KeyCollection__ctor_m25698_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25699_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25700_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25701_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25702_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25703_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25704_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25705_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25706_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25707_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25708_gshared ();
extern "C" void KeyCollection_CopyTo_m25709_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25710_gshared ();
void* RuntimeInvoker_Enumerator_t3819 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m25711_gshared ();
extern "C" void Enumerator__ctor_m25712_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25713_gshared ();
extern "C" void Enumerator_Dispose_m25714_gshared ();
extern "C" void Enumerator_MoveNext_m25715_gshared ();
extern "C" void Enumerator_get_Current_m25716_gshared ();
extern "C" void Enumerator__ctor_m25717_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25718_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721_gshared ();
extern "C" void Enumerator_MoveNext_m25722_gshared ();
extern "C" void Enumerator_get_Current_m25723_gshared ();
extern "C" void Enumerator_get_CurrentKey_m25724_gshared ();
extern "C" void Enumerator_get_CurrentValue_m25725_gshared ();
extern "C" void Enumerator_VerifyState_m25726_gshared ();
extern "C" void Enumerator_VerifyCurrent_m25727_gshared ();
extern "C" void Enumerator_Dispose_m25728_gshared ();
extern "C" void Transform_1__ctor_m25729_gshared ();
extern "C" void Transform_1_Invoke_m25730_gshared ();
extern "C" void Transform_1_BeginInvoke_m25731_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1092_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m25732_gshared ();
extern "C" void ValueCollection__ctor_m25733_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25734_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25735_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25736_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25737_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25738_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25739_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25740_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25741_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25742_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m25743_gshared ();
extern "C" void ValueCollection_CopyTo_m25744_gshared ();
extern "C" void ValueCollection_GetEnumerator_m25745_gshared ();
void* RuntimeInvoker_Enumerator_t3823 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m25746_gshared ();
extern "C" void Enumerator__ctor_m25747_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25748_gshared ();
extern "C" void Enumerator_Dispose_m25749_gshared ();
extern "C" void Enumerator_MoveNext_m25750_gshared ();
extern "C" void Enumerator_get_Current_m25751_gshared ();
extern "C" void Transform_1__ctor_m25752_gshared ();
extern "C" void Transform_1_Invoke_m25753_gshared ();
extern "C" void Transform_1_BeginInvoke_m25754_gshared ();
extern "C" void Transform_1_EndInvoke_m25755_gshared ();
extern "C" void Transform_1__ctor_m25756_gshared ();
extern "C" void Transform_1_Invoke_m25757_gshared ();
extern "C" void Transform_1_BeginInvoke_m25758_gshared ();
extern "C" void Transform_1_EndInvoke_m25759_gshared ();
extern "C" void Transform_1__ctor_m25760_gshared ();
extern "C" void Transform_1_Invoke_m25761_gshared ();
extern "C" void Transform_1_BeginInvoke_m25762_gshared ();
extern "C" void Transform_1_EndInvoke_m25763_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3815_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m25764_gshared ();
extern "C" void ShimEnumerator_MoveNext_m25765_gshared ();
extern "C" void ShimEnumerator_get_Entry_m25766_gshared ();
extern "C" void ShimEnumerator_get_Key_m25767_gshared ();
extern "C" void ShimEnumerator_get_Value_m25768_gshared ();
extern "C" void ShimEnumerator_get_Current_m25769_gshared ();
extern "C" void EqualityComparer_1__ctor_m25770_gshared ();
extern "C" void EqualityComparer_1__cctor_m25771_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25772_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25773_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25774_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25775_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25776_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25777_gshared ();
extern "C" void DefaultComparer__ctor_m25778_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25779_gshared ();
extern "C" void DefaultComparer_Equals_m25780_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25955_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25956_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25957_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25958_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25959_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3836 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25960_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Key_m25961_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25962_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25963_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25964_gshared ();
extern "C" void KeyValuePair_2_ToString_m25965_gshared ();
extern "C" void Dictionary_2__ctor_m26319_gshared ();
extern "C" void Dictionary_2__ctor_m26321_gshared ();
extern "C" void Dictionary_2__ctor_m26323_gshared ();
extern "C" void Dictionary_2__ctor_m26325_gshared ();
extern "C" void Dictionary_2__ctor_m26327_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26329_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26331_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m26333_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26335_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26337_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m26339_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26341_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26343_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26345_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26347_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26349_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26351_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26353_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26355_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26357_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26359_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26361_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26363_gshared ();
extern "C" void Dictionary_2_get_Count_m26365_gshared ();
extern "C" void Dictionary_2_get_Item_m26367_gshared ();
extern "C" void Dictionary_2_set_Item_m26369_gshared ();
extern "C" void Dictionary_2_Init_m26371_gshared ();
extern "C" void Dictionary_2_InitArrays_m26373_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m26375_gshared ();
extern "C" void Dictionary_2_make_pair_m26377_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3836_Object_t_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m26379_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m26381_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3254_Object_t_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m26383_gshared ();
extern "C" void Dictionary_2_Resize_m26385_gshared ();
extern "C" void Dictionary_2_Add_m26387_gshared ();
extern "C" void Dictionary_2_Clear_m26389_gshared ();
extern "C" void Dictionary_2_ContainsKey_m26391_gshared ();
extern "C" void Dictionary_2_ContainsValue_m26393_gshared ();
extern "C" void Dictionary_2_GetObjectData_m26395_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m26397_gshared ();
extern "C" void Dictionary_2_Remove_m26399_gshared ();
extern "C" void Dictionary_2_TryGetValue_m26401_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_KeyValuePair_2U26_t4553 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m26403_gshared ();
extern "C" void Dictionary_2_get_Values_m26405_gshared ();
extern "C" void Dictionary_2_ToTKey_m26407_gshared ();
extern "C" void Dictionary_2_ToTValue_m26409_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m26411_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m26413_gshared ();
void* RuntimeInvoker_Enumerator_t3864 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m26415_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m26416_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26417_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26418_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26419_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26420_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26421_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m26422_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26423_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26424_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26425_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m26426_gshared ();
extern "C" void KeyCollection_CopyTo_m26427_gshared ();
extern "C" void KeyCollection_GetEnumerator_m26428_gshared ();
void* RuntimeInvoker_Enumerator_t3863 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m26429_gshared ();
extern "C" void Enumerator__ctor_m26430_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26431_gshared ();
extern "C" void Enumerator_Dispose_m26432_gshared ();
extern "C" void Enumerator_MoveNext_m26433_gshared ();
extern "C" void Enumerator_get_Current_m26434_gshared ();
extern "C" void Enumerator__ctor_m26435_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26436_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26437_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26438_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26439_gshared ();
extern "C" void Enumerator_MoveNext_m26440_gshared ();
extern "C" void Enumerator_get_Current_m26441_gshared ();
extern "C" void Enumerator_get_CurrentKey_m26442_gshared ();
extern "C" void Enumerator_get_CurrentValue_m26443_gshared ();
extern "C" void Enumerator_VerifyState_m26444_gshared ();
extern "C" void Enumerator_VerifyCurrent_m26445_gshared ();
extern "C" void Enumerator_Dispose_m26446_gshared ();
extern "C" void Transform_1__ctor_m26447_gshared ();
extern "C" void Transform_1_Invoke_m26448_gshared ();
extern "C" void Transform_1_BeginInvoke_m26449_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m26450_gshared ();
extern "C" void ValueCollection__ctor_m26451_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26452_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26453_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26454_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26455_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26456_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m26457_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26458_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26459_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26460_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m26461_gshared ();
extern "C" void ValueCollection_CopyTo_m26462_gshared ();
extern "C" void ValueCollection_GetEnumerator_m26463_gshared ();
void* RuntimeInvoker_Enumerator_t3867 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m26464_gshared ();
extern "C" void Enumerator__ctor_m26465_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m26466_gshared ();
extern "C" void Enumerator_Dispose_m26467_gshared ();
extern "C" void Enumerator_MoveNext_m26468_gshared ();
extern "C" void Enumerator_get_Current_m26469_gshared ();
extern "C" void Transform_1__ctor_m26470_gshared ();
extern "C" void Transform_1_Invoke_m26471_gshared ();
extern "C" void Transform_1_BeginInvoke_m26472_gshared ();
extern "C" void Transform_1_EndInvoke_m26473_gshared ();
extern "C" void Transform_1__ctor_m26474_gshared ();
extern "C" void Transform_1_Invoke_m26475_gshared ();
extern "C" void Transform_1_BeginInvoke_m26476_gshared ();
extern "C" void Transform_1_EndInvoke_m26477_gshared ();
extern "C" void Transform_1__ctor_m26478_gshared ();
extern "C" void Transform_1_Invoke_m26479_gshared ();
extern "C" void Transform_1_BeginInvoke_m26480_gshared ();
extern "C" void Transform_1_EndInvoke_m26481_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3836_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m26482_gshared ();
extern "C" void ShimEnumerator_MoveNext_m26483_gshared ();
extern "C" void ShimEnumerator_get_Entry_m26484_gshared ();
extern "C" void ShimEnumerator_get_Key_m26485_gshared ();
extern "C" void ShimEnumerator_get_Value_m26486_gshared ();
extern "C" void ShimEnumerator_get_Current_m26487_gshared ();
extern "C" void EqualityComparer_1__ctor_m26488_gshared ();
extern "C" void EqualityComparer_1__cctor_m26489_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26490_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26491_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26492_gshared ();
extern "C" void DefaultComparer__ctor_m26493_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26494_gshared ();
extern "C" void DefaultComparer_Equals_m26495_gshared ();
void* RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3254_KeyValuePair_2_t3254 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m26594_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26595_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26596_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26597_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26598_gshared ();
void* RuntimeInvoker_ParameterModifier_t2260 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m26599_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26600_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26601_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26602_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26603_gshared ();
void* RuntimeInvoker_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m26727_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m26728_gshared ();
extern "C" void InvokableCall_1__ctor_m26729_gshared ();
extern "C" void InvokableCall_1__ctor_m26730_gshared ();
extern "C" void InvokableCall_1_Invoke_m26731_gshared ();
extern "C" void InvokableCall_1_Find_m26732_gshared ();
extern "C" void UnityAction_1__ctor_m26733_gshared ();
extern "C" void UnityAction_1_Invoke_m26734_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m26735_gshared ();
extern "C" void UnityAction_1_EndInvoke_m26736_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m26744_gshared ();
extern "C" void InternalEnumerator_1__ctor_m26942_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26943_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m26944_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m26945_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m26946_gshared ();
extern "C" void Dictionary_2__ctor_m27002_gshared ();
extern "C" void Dictionary_2__ctor_m27005_gshared ();
extern "C" void Dictionary_2__ctor_m27007_gshared ();
extern "C" void Dictionary_2__ctor_m27009_gshared ();
extern "C" void Dictionary_2__ctor_m27011_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27013_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27015_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m27017_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27019_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27021_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m27023_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27025_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27027_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27029_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27031_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27033_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27035_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27037_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27039_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27041_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27043_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27045_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27047_gshared ();
extern "C" void Dictionary_2_get_Count_m27049_gshared ();
extern "C" void Dictionary_2_get_Item_m27051_gshared ();
extern "C" void Dictionary_2_set_Item_m27053_gshared ();
void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m27055_gshared ();
extern "C" void Dictionary_2_InitArrays_m27057_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m27059_gshared ();
extern "C" void Dictionary_2_make_pair_m27061_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3932_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m27063_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m27065_gshared ();
void* RuntimeInvoker_Byte_t449_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m27067_gshared ();
extern "C" void Dictionary_2_Resize_m27069_gshared ();
extern "C" void Dictionary_2_Add_m27071_gshared ();
extern "C" void Dictionary_2_Clear_m27073_gshared ();
extern "C" void Dictionary_2_ContainsKey_m27075_gshared ();
extern "C" void Dictionary_2_ContainsValue_m27077_gshared ();
extern "C" void Dictionary_2_GetObjectData_m27079_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m27081_gshared ();
extern "C" void Dictionary_2_Remove_m27083_gshared ();
extern "C" void Dictionary_2_TryGetValue_m27085_gshared ();
void* RuntimeInvoker_Boolean_t169_Object_t_ByteU26_t1840 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m27087_gshared ();
extern "C" void Dictionary_2_get_Values_m27089_gshared ();
extern "C" void Dictionary_2_ToTKey_m27091_gshared ();
extern "C" void Dictionary_2_ToTValue_m27093_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m27095_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m27097_gshared ();
void* RuntimeInvoker_Enumerator_t3936 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m27099_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27100_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27101_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27102_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27103_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27104_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3932 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m27105_gshared ();
extern "C" void KeyValuePair_2_get_Key_m27106_gshared ();
extern "C" void KeyValuePair_2_set_Key_m27107_gshared ();
extern "C" void KeyValuePair_2_get_Value_m27108_gshared ();
extern "C" void KeyValuePair_2_set_Value_m27109_gshared ();
extern "C" void KeyValuePair_2_ToString_m27110_gshared ();
extern "C" void KeyCollection__ctor_m27111_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27112_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27113_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27114_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27115_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27116_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m27117_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27118_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27119_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27120_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m27121_gshared ();
extern "C" void KeyCollection_CopyTo_m27122_gshared ();
extern "C" void KeyCollection_GetEnumerator_m27123_gshared ();
void* RuntimeInvoker_Enumerator_t3935 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m27124_gshared ();
extern "C" void Enumerator__ctor_m27125_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27126_gshared ();
extern "C" void Enumerator_Dispose_m27127_gshared ();
extern "C" void Enumerator_MoveNext_m27128_gshared ();
extern "C" void Enumerator_get_Current_m27129_gshared ();
extern "C" void Enumerator__ctor_m27130_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27131_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134_gshared ();
extern "C" void Enumerator_MoveNext_m27135_gshared ();
extern "C" void Enumerator_get_Current_m27136_gshared ();
extern "C" void Enumerator_get_CurrentKey_m27137_gshared ();
extern "C" void Enumerator_get_CurrentValue_m27138_gshared ();
extern "C" void Enumerator_VerifyState_m27139_gshared ();
extern "C" void Enumerator_VerifyCurrent_m27140_gshared ();
extern "C" void Enumerator_Dispose_m27141_gshared ();
extern "C" void Transform_1__ctor_m27142_gshared ();
extern "C" void Transform_1_Invoke_m27143_gshared ();
extern "C" void Transform_1_BeginInvoke_m27144_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m27145_gshared ();
extern "C" void ValueCollection__ctor_m27146_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27147_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27148_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27149_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27150_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27151_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m27152_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27153_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27154_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27155_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m27156_gshared ();
extern "C" void ValueCollection_CopyTo_m27157_gshared ();
extern "C" void ValueCollection_GetEnumerator_m27158_gshared ();
void* RuntimeInvoker_Enumerator_t3939 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m27159_gshared ();
extern "C" void Enumerator__ctor_m27160_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27161_gshared ();
extern "C" void Enumerator_Dispose_m27162_gshared ();
extern "C" void Enumerator_MoveNext_m27163_gshared ();
extern "C" void Enumerator_get_Current_m27164_gshared ();
extern "C" void Transform_1__ctor_m27165_gshared ();
extern "C" void Transform_1_Invoke_m27166_gshared ();
extern "C" void Transform_1_BeginInvoke_m27167_gshared ();
extern "C" void Transform_1_EndInvoke_m27168_gshared ();
extern "C" void Transform_1__ctor_m27169_gshared ();
extern "C" void Transform_1_Invoke_m27170_gshared ();
extern "C" void Transform_1_BeginInvoke_m27171_gshared ();
extern "C" void Transform_1_EndInvoke_m27172_gshared ();
extern "C" void Transform_1__ctor_m27173_gshared ();
extern "C" void Transform_1_Invoke_m27174_gshared ();
extern "C" void Transform_1_BeginInvoke_m27175_gshared ();
extern "C" void Transform_1_EndInvoke_m27176_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3932_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m27177_gshared ();
extern "C" void ShimEnumerator_MoveNext_m27178_gshared ();
extern "C" void ShimEnumerator_get_Entry_m27179_gshared ();
extern "C" void ShimEnumerator_get_Key_m27180_gshared ();
extern "C" void ShimEnumerator_get_Value_m27181_gshared ();
extern "C" void ShimEnumerator_get_Current_m27182_gshared ();
extern "C" void EqualityComparer_1__ctor_m27183_gshared ();
extern "C" void EqualityComparer_1__cctor_m27184_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27185_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27186_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27187_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m27188_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27189_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27190_gshared ();
void* RuntimeInvoker_Boolean_t169_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m27191_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27192_gshared ();
extern "C" void DefaultComparer_Equals_m27193_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27244_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27245_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27246_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27247_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27248_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1902 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m27259_gshared ();
extern "C" void Dictionary_2__ctor_m27260_gshared ();
extern "C" void Dictionary_2__ctor_m27261_gshared ();
extern "C" void Dictionary_2__ctor_m27262_gshared ();
extern "C" void Dictionary_2__ctor_m27263_gshared ();
extern "C" void Dictionary_2__ctor_m27264_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27265_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27266_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m27267_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27268_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27269_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m27270_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27271_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27272_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27273_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27274_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27275_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27276_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27277_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27278_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27279_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27280_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27281_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27282_gshared ();
extern "C" void Dictionary_2_get_Count_m27283_gshared ();
extern "C" void Dictionary_2_get_Item_m27284_gshared ();
extern "C" void Dictionary_2_set_Item_m27285_gshared ();
extern "C" void Dictionary_2_Init_m27286_gshared ();
extern "C" void Dictionary_2_InitArrays_m27287_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m27288_gshared ();
extern "C" void Dictionary_2_make_pair_m27289_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3954_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m27290_gshared ();
extern "C" void Dictionary_2_pick_value_m27291_gshared ();
extern "C" void Dictionary_2_CopyTo_m27292_gshared ();
extern "C" void Dictionary_2_Resize_m27293_gshared ();
extern "C" void Dictionary_2_Add_m27294_gshared ();
extern "C" void Dictionary_2_Clear_m27295_gshared ();
extern "C" void Dictionary_2_ContainsKey_m27296_gshared ();
extern "C" void Dictionary_2_ContainsValue_m27297_gshared ();
extern "C" void Dictionary_2_GetObjectData_m27298_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m27299_gshared ();
extern "C" void Dictionary_2_Remove_m27300_gshared ();
extern "C" void Dictionary_2_TryGetValue_m27301_gshared ();
void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m27302_gshared ();
extern "C" void Dictionary_2_get_Values_m27303_gshared ();
extern "C" void Dictionary_2_ToTKey_m27304_gshared ();
extern "C" void Dictionary_2_ToTValue_m27305_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m27306_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m27307_gshared ();
void* RuntimeInvoker_Enumerator_t3958 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m27308_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27309_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27310_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27311_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27312_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27313_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3954 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m27314_gshared ();
extern "C" void KeyValuePair_2_get_Key_m27315_gshared ();
extern "C" void KeyValuePair_2_set_Key_m27316_gshared ();
extern "C" void KeyValuePair_2_get_Value_m27317_gshared ();
extern "C" void KeyValuePair_2_set_Value_m27318_gshared ();
extern "C" void KeyValuePair_2_ToString_m27319_gshared ();
extern "C" void KeyCollection__ctor_m27320_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27321_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27322_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27323_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27324_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27325_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m27326_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27327_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27328_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27329_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m27330_gshared ();
extern "C" void KeyCollection_CopyTo_m27331_gshared ();
extern "C" void KeyCollection_GetEnumerator_m27332_gshared ();
void* RuntimeInvoker_Enumerator_t3957 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m27333_gshared ();
extern "C" void Enumerator__ctor_m27334_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27335_gshared ();
extern "C" void Enumerator_Dispose_m27336_gshared ();
extern "C" void Enumerator_MoveNext_m27337_gshared ();
extern "C" void Enumerator_get_Current_m27338_gshared ();
extern "C" void Enumerator__ctor_m27339_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27340_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27341_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27342_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27343_gshared ();
extern "C" void Enumerator_MoveNext_m27344_gshared ();
extern "C" void Enumerator_get_Current_m27345_gshared ();
extern "C" void Enumerator_get_CurrentKey_m27346_gshared ();
extern "C" void Enumerator_get_CurrentValue_m27347_gshared ();
extern "C" void Enumerator_VerifyState_m27348_gshared ();
extern "C" void Enumerator_VerifyCurrent_m27349_gshared ();
extern "C" void Enumerator_Dispose_m27350_gshared ();
extern "C" void Transform_1__ctor_m27351_gshared ();
extern "C" void Transform_1_Invoke_m27352_gshared ();
extern "C" void Transform_1_BeginInvoke_m27353_gshared ();
extern "C" void Transform_1_EndInvoke_m27354_gshared ();
extern "C" void ValueCollection__ctor_m27355_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27356_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27357_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27358_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27359_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27360_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m27361_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27362_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27363_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27364_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m27365_gshared ();
extern "C" void ValueCollection_CopyTo_m27366_gshared ();
extern "C" void ValueCollection_GetEnumerator_m27367_gshared ();
void* RuntimeInvoker_Enumerator_t3961 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m27368_gshared ();
extern "C" void Enumerator__ctor_m27369_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m27370_gshared ();
extern "C" void Enumerator_Dispose_m27371_gshared ();
extern "C" void Enumerator_MoveNext_m27372_gshared ();
extern "C" void Enumerator_get_Current_m27373_gshared ();
extern "C" void Transform_1__ctor_m27374_gshared ();
extern "C" void Transform_1_Invoke_m27375_gshared ();
extern "C" void Transform_1_BeginInvoke_m27376_gshared ();
extern "C" void Transform_1_EndInvoke_m27377_gshared ();
extern "C" void Transform_1__ctor_m27378_gshared ();
extern "C" void Transform_1_Invoke_m27379_gshared ();
extern "C" void Transform_1_BeginInvoke_m27380_gshared ();
extern "C" void Transform_1_EndInvoke_m27381_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3954_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m27382_gshared ();
extern "C" void ShimEnumerator_MoveNext_m27383_gshared ();
extern "C" void ShimEnumerator_get_Entry_m27384_gshared ();
extern "C" void ShimEnumerator_get_Key_m27385_gshared ();
extern "C" void ShimEnumerator_get_Value_m27386_gshared ();
extern "C" void ShimEnumerator_get_Current_m27387_gshared ();
extern "C" void InternalEnumerator_1__ctor_m27388_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27389_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27390_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27391_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27392_gshared ();
void* RuntimeInvoker_Mark_t1948 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27393_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27394_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27395_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27396_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27397_gshared ();
void* RuntimeInvoker_UriScheme_t1984 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27403_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27404_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27405_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27406_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27407_gshared ();
void* RuntimeInvoker_Int16_t534 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27408_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27409_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27410_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27411_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27412_gshared ();
void* RuntimeInvoker_SByte_t170 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27438_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27439_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27440_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27441_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27442_gshared ();
void* RuntimeInvoker_TableRange_t2069 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27468_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27469_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27470_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27471_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27472_gshared ();
void* RuntimeInvoker_Slot_t2146 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27473_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27474_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27475_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27476_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27477_gshared ();
void* RuntimeInvoker_Slot_t2153 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27546_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27547_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27548_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27549_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27550_gshared ();
void* RuntimeInvoker_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27551_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27552_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27553_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27554_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27555_gshared ();
void* RuntimeInvoker_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m27556_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27557_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m27558_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m27559_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m27560_gshared ();
extern "C" void GenericComparer_1_Compare_m27664_gshared ();
void* RuntimeInvoker_Int32_t127_DateTime_t111_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27665_gshared ();
extern "C" void Comparer_1__cctor_m27666_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27667_gshared ();
extern "C" void Comparer_1_get_Default_m27668_gshared ();
extern "C" void DefaultComparer__ctor_m27669_gshared ();
extern "C" void DefaultComparer_Compare_m27670_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27671_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27672_gshared ();
void* RuntimeInvoker_Boolean_t169_DateTime_t111_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27673_gshared ();
extern "C" void EqualityComparer_1__cctor_m27674_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27675_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27676_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27677_gshared ();
extern "C" void DefaultComparer__ctor_m27678_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27679_gshared ();
extern "C" void DefaultComparer_Equals_m27680_gshared ();
extern "C" void GenericComparer_1_Compare_m27681_gshared ();
void* RuntimeInvoker_Int32_t127_DateTimeOffset_t1420_DateTimeOffset_t1420 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27682_gshared ();
extern "C" void Comparer_1__cctor_m27683_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27684_gshared ();
extern "C" void Comparer_1_get_Default_m27685_gshared ();
extern "C" void DefaultComparer__ctor_m27686_gshared ();
extern "C" void DefaultComparer_Compare_m27687_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27688_gshared ();
void* RuntimeInvoker_Int32_t127_DateTimeOffset_t1420 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m27689_gshared ();
void* RuntimeInvoker_Boolean_t169_DateTimeOffset_t1420_DateTimeOffset_t1420 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27690_gshared ();
extern "C" void EqualityComparer_1__cctor_m27691_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27692_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27693_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27694_gshared ();
extern "C" void DefaultComparer__ctor_m27695_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27696_gshared ();
extern "C" void DefaultComparer_Equals_m27697_gshared ();
extern "C" void Nullable_1_Equals_m27698_gshared ();
extern "C" void Nullable_1_Equals_m27699_gshared ();
void* RuntimeInvoker_Boolean_t169_Nullable_1_t2598 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m27700_gshared ();
extern "C" void Nullable_1_ToString_m27701_gshared ();
extern "C" void GenericComparer_1_Compare_m27702_gshared ();
void* RuntimeInvoker_Int32_t127_Guid_t108_Guid_t108 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27703_gshared ();
extern "C" void Comparer_1__cctor_m27704_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27705_gshared ();
extern "C" void Comparer_1_get_Default_m27706_gshared ();
extern "C" void DefaultComparer__ctor_m27707_gshared ();
extern "C" void DefaultComparer_Compare_m27708_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27709_gshared ();
void* RuntimeInvoker_Int32_t127_Guid_t108 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m27710_gshared ();
void* RuntimeInvoker_Boolean_t169_Guid_t108_Guid_t108 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27711_gshared ();
extern "C" void EqualityComparer_1__cctor_m27712_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27713_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27714_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27715_gshared ();
extern "C" void DefaultComparer__ctor_m27716_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27717_gshared ();
extern "C" void DefaultComparer_Equals_m27718_gshared ();
extern "C" void GenericComparer_1_Compare_m27719_gshared ();
void* RuntimeInvoker_Int32_t127_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m27720_gshared ();
extern "C" void Comparer_1__cctor_m27721_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m27722_gshared ();
extern "C" void Comparer_1_get_Default_m27723_gshared ();
extern "C" void DefaultComparer__ctor_m27724_gshared ();
extern "C" void DefaultComparer_Compare_m27725_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m27726_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m27727_gshared ();
void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m27728_gshared ();
extern "C" void EqualityComparer_1__cctor_m27729_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27730_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27731_gshared ();
extern "C" void EqualityComparer_1_get_Default_m27732_gshared ();
extern "C" void DefaultComparer__ctor_m27733_gshared ();
extern "C" void DefaultComparer_GetHashCode_m27734_gshared ();
extern "C" void DefaultComparer_Equals_m27735_gshared ();
extern const Il2CppGenericInst GenInst_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_t99_0_0_0;
extern const Il2CppGenericInst GenInst_Renderer_t8_0_0_0;
extern const Il2CppGenericInst GenInst_Transform_t11_0_0_0;
extern const Il2CppGenericInst GenInst_Raycaster_t18_0_0_0;
extern const Il2CppGenericInst GenInst_Camera_t3_0_0_0;
extern const Il2CppGenericInst GenInst_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_LogBehaviour_t956_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t15_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t116_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t15_0_0_0_Vector3_t15_0_0_0_VectorOptions_t1002_0_0_0;
extern const Il2CppGenericInst GenInst_Sequence_t122_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_t2_0_0_0;
extern const Il2CppGenericInst GenInst_InitError_t176_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t40_0_0_0;
extern const Il2CppGenericInst GenInst_Prop_t97_0_0_0;
extern const Il2CppGenericInst GenInst_Surface_t98_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t44_0_0_0;
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t59_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t85_0_0_0;
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t76_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t49_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t57_0_0_0;
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t61_0_0_0;
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t35_0_0_0;
extern const Il2CppGenericInst GenInst_WordBehaviour_t92_0_0_0;
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t74_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t63_0_0_0;
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t52_0_0_0;
extern const Il2CppGenericInst GenInst_MeshRenderer_t148_0_0_0;
extern const Il2CppGenericInst GenInst_MeshFilter_t149_0_0_0;
extern const Il2CppGenericInst GenInst_Collider_t157_0_0_0;
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t89_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInputModule_t196_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t231_0_0_0;
extern const Il2CppGenericInst GenInst_IDeselectHandler_t403_0_0_0;
extern const Il2CppGenericInst GenInst_ISelectHandler_t402_0_0_0;
extern const Il2CppGenericInst GenInst_BaseEventData_t197_0_0_0;
extern const Il2CppGenericInst GenInst_Entry_t202_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t390_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t391_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t392_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t393_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t394_0_0_0;
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t395_0_0_0;
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t396_0_0_0;
extern const Il2CppGenericInst GenInst_IDragHandler_t397_0_0_0;
extern const Il2CppGenericInst GenInst_IEndDragHandler_t398_0_0_0;
extern const Il2CppGenericInst GenInst_IDropHandler_t399_0_0_0;
extern const Il2CppGenericInst GenInst_IScrollHandler_t400_0_0_0;
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t401_0_0_0;
extern const Il2CppGenericInst GenInst_IMoveHandler_t404_0_0_0;
extern const Il2CppGenericInst GenInst_ISubmitHandler_t405_0_0_0;
extern const Il2CppGenericInst GenInst_ICancelHandler_t406_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t408_0_0_0;
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t490_0_0_0;
extern const Il2CppGenericInst GenInst_PointerEventData_t236_0_0_0;
extern const Il2CppGenericInst GenInst_AxisEventData_t232_0_0_0;
extern const Il2CppGenericInst GenInst_BaseRaycaster_t230_0_0_0;
extern const Il2CppGenericInst GenInst_EventSystem_t106_0_0_0;
extern const Il2CppGenericInst GenInst_ButtonState_t239_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_PointerEventData_t236_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteRenderer_t429_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit_t94_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t90_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t411_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t266_0_0_0_List_1_t437_0_0_0;
extern const Il2CppGenericInst GenInst_Text_t23_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t266_0_0_0;
extern const Il2CppGenericInst GenInst_ColorTween_t253_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t315_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t313_0_0_0;
extern const Il2CppGenericInst GenInst_RectTransform_t272_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t274_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasRenderer_t273_0_0_0;
extern const Il2CppGenericInst GenInst_Component_t103_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t278_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t274_0_0_0_IndexedSet_1_t448_0_0_0;
extern const Il2CppGenericInst GenInst_Sprite_t292_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t285_0_0_0;
extern const Il2CppGenericInst GenInst_FillMethod_t286_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t151_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_SubmitEvent_t300_0_0_0;
extern const Il2CppGenericInst GenInst_OnChangeEvent_t302_0_0_0;
extern const Il2CppGenericInst GenInst_OnValidateInput_t306_0_0_0;
extern const Il2CppGenericInst GenInst_ContentType_t296_0_0_0;
extern const Il2CppGenericInst GenInst_LineType_t299_0_0_0;
extern const Il2CppGenericInst GenInst_InputType_t297_0_0_0;
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t453_0_0_0;
extern const Il2CppGenericInst GenInst_CharacterValidation_t298_0_0_0;
extern const Il2CppGenericInst GenInst_Char_t451_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t458_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t460_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutElement_t370_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t323_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t10_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasGroup_t442_0_0_0;
extern const Il2CppGenericInst GenInst_Selectable_t259_0_0_0;
extern const Il2CppGenericInst GenInst_Navigation_t320_0_0_0;
extern const Il2CppGenericInst GenInst_Transition_t335_0_0_0;
extern const Il2CppGenericInst GenInst_ColorBlock_t265_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteState_t339_0_0_0;
extern const Il2CppGenericInst GenInst_AnimationTriggers_t254_0_0_0;
extern const Il2CppGenericInst GenInst_Animator_t415_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t341_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t294_0_0_0;
extern const Il2CppGenericInst GenInst_MatEntry_t344_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t351_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t351_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_AspectMode_t355_0_0_0;
extern const Il2CppGenericInst GenInst_FitMode_t361_0_0_0;
extern const Il2CppGenericInst GenInst_Corner_t363_0_0_0;
extern const Il2CppGenericInst GenInst_Axis_t364_0_0_0;
extern const Il2CppGenericInst GenInst_Constraint_t365_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t371_0_0_0;
extern const Il2CppGenericInst GenInst_TextAnchor_t471_0_0_0;
extern const Il2CppGenericInst GenInst_ILayoutElement_t419_0_0_0_Single_t151_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t420_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t418_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m2460_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2461_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2463_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m2464_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m2465_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenRunner_1_t496_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t375_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t507_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t507_gp_0_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectPool_1_t509_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t178_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t674_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t68_0_0_0;
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t761_0_0_0;
extern const Il2CppGenericInst GenInst_TargetSearchResult_t720_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectTracker_t574_0_0_0;
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t32_0_0_0;
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t582_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButton_t731_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t614_0_0_0_Image_t615_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t614_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Trackable_t565_0_0_0;
extern const Il2CppGenericInst GenInst_Trackable_t565_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButton_t731_0_0_0;
extern const Il2CppGenericInst GenInst_DataSet_t594_0_0_0;
extern const Il2CppGenericInst GenInst_DataSetImpl_t578_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Marker_t739_0_0_0;
extern const Il2CppGenericInst GenInst_Marker_t739_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t642_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t589_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t591_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t454_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_WordResult_t695_0_0_0;
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t93_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t692_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_WordAbstractBehaviour_t93_0_0_0;
extern const Il2CppGenericInst GenInst_WordData_t647_0_0_0;
extern const Il2CppGenericInst GenInst_WordResultData_t646_0_0_0;
extern const Il2CppGenericInst GenInst_Word_t696_0_0_0;
extern const Il2CppGenericInst GenInst_WordResult_t695_0_0_0;
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t776_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t588_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Prop_t97_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Surface_t98_0_0_0;
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t777_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_PropAbstractBehaviour_t65_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_SurfaceAbstractBehaviour_t73_0_0_0;
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t65_0_0_0;
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t73_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableBehaviour_t44_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerTracker_t629_0_0_0;
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t567_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableResultData_t642_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonData_t643_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t86_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_ImageTarget_t732_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTarget_t732_0_0_0;
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t50_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonAbstractBehaviour_t86_0_0_0;
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t171_0_0_0;
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t82_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t788_0_0_0;
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t88_0_0_0;
extern const Il2CppGenericInst GenInst_TextTracker_t676_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0;
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t789_0_0_0;
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t790_0_0_0;
extern const Il2CppGenericInst GenInst_QCARAbstractBehaviour_t67_0_0_0;
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t791_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m5013_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t734_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t1075_0_0_0_UInt32_t1075_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t10_0_0_0_Vector2_t10_0_0_0_VectorOptions_t1002_0_0_0;
extern const Il2CppGenericInst GenInst_DOTweenComponent_t935_0_0_0;
extern const Il2CppGenericInst GenInst_Tween_t934_0_0_0;
extern const Il2CppGenericInst GenInst_ABSSequentiable_t943_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t15_0_0_0_Vector3U5BU5D_t154_0_0_0_Vector3ArrayOptions_t951_0_0_0;
extern const Il2CppGenericInst GenInst_Quaternion_t13_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1028_0_0_0;
extern const Il2CppGenericInst GenInst_Quaternion_t13_0_0_0_Vector3_t15_0_0_0_QuaternionOptions_t971_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t371_0_0_0_RectOffset_t371_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_Color2_t1000_0_0_0_Color2_t1000_0_0_0_ColorOptions_t1011_0_0_0;
extern const Il2CppGenericInst GenInst_Rect_t124_0_0_0_Rect_t124_0_0_0_RectOptions_t1004_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_UInt64_t1091_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Int32_t127_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_TweenCallback_t101_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t1003_0_0_0;
extern const Il2CppGenericInst GenInst_Vector4_t413_0_0_0_Vector4_t413_0_0_0_VectorOptions_t1002_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t90_0_0_0_Color_t90_0_0_0_ColorOptions_t1011_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t151_0_0_0_Single_t151_0_0_0_FloatOptions_t996_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1092_0_0_0_Int64_t1092_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t1064_gp_0_0_0_0_ABSTweenPlugin_3_t1064_gp_1_0_0_0_ABSTweenPlugin_3_t1064_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t1064_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t1075_0_0_0;
extern const Il2CppGenericInst GenInst_PluginsManager_GetDefaultPlugin_m5611_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m5611_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m5611_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Color2_t1000_0_0_0;
extern const Il2CppGenericInst GenInst_TweenManager_GetTweener_m5612_gp_0_0_0_0_TweenManager_GetTweener_m5612_gp_1_0_0_0_TweenManager_GetTweener_m5612_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Rect_t124_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0;
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m5613_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m5613_gp_0_0_0_0_DOTween_ApplyTo_m5613_gp_1_0_0_0_DOTween_ApplyTo_m5613_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Vector4_t413_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_Setup_m5614_gp_0_0_0_0_Tweener_Setup_m5614_gp_1_0_0_0_Tweener_Setup_m5614_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_Setup_m5614_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoUpdateDelay_m5615_gp_0_0_0_0_Tweener_DoUpdateDelay_m5615_gp_1_0_0_0_Tweener_DoUpdateDelay_m5615_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m5616_gp_0_0_0_0_Tweener_DoStartup_m5616_gp_1_0_0_0_Tweener_DoStartup_m5616_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m5616_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DOStartupSpecials_m5617_gp_0_0_0_0_Tweener_DOStartupSpecials_m5617_gp_1_0_0_0_Tweener_DOStartupSpecials_m5617_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Tweener_DOStartupDurationBased_m5618_gp_0_0_0_0_Tweener_DOStartupDurationBased_m5618_gp_1_0_0_0_Tweener_DOStartupDurationBased_m5618_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1068_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1068_gp_0_0_0_0_TweenerCore_3_t1068_gp_1_0_0_0_TweenerCore_3_t1068_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1092_0_0_0;
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1155_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_LayoutCache_t1170_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1174_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1172_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t616_0_0_0;
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1225_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t1260_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDesc_t1262_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1267_0_0_0_NetworkAccessToken_t1269_0_0_0;
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t1254_0_0_0;
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t1256_0_0_0;
extern const Il2CppGenericInst GenInst_BasicResponse_t1251_0_0_0;
extern const Il2CppGenericInst GenInst_ListMatchResponse_t1264_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1374_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t1286_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1376_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t1284_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1377_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1419_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t1285_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1421_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1287_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayer_t1161_0_0_0;
extern const Il2CppGenericInst GenInst_PersistentCall_t1342_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1339_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1384_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1386_0_0_0;
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t1321_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1315_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7054_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7055_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7056_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7057_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponents_m7058_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m7061_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m7063_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m7064_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m7065_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m7069_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1092_0_0_0;
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t1447_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m7071_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1450_gp_0_0_0_0_ThreadSafeDictionary_2_t1450_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1450_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1524_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t317_0_0_0_TextEditOp_t1335_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_1_t1457_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1458_gp_0_0_0_0_InvokableCall_2_t1458_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1458_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1458_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1459_gp_0_0_0_0_InvokableCall_3_t1459_gp_1_0_0_0_InvokableCall_3_t1459_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1459_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1459_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1459_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1460_gp_0_0_0_0_InvokableCall_4_t1460_gp_1_0_0_0_InvokableCall_4_t1460_gp_2_0_0_0_InvokableCall_4_t1460_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1460_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1460_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1460_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1460_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1439_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1461_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1462_gp_0_0_0_0_UnityEvent_2_t1462_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1463_gp_0_0_0_0_UnityEvent_3_t1463_gp_1_0_0_0_UnityEvent_3_t1463_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1464_gp_0_0_0_0_UnityEvent_4_t1464_gp_1_0_0_0_UnityEvent_4_t1464_gp_2_0_0_0_UnityEvent_4_t1464_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1591_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_PrimeHelper_t1592_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_HashSet_1_t1589_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_0_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Any_m7305_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m7306_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m7307_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m7308_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m7309_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_First_m7310_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m7311_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m7312_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m7313_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m7313_gp_0_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7314_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7314_gp_0_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2008_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LinkedList_1_t2007_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2009_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2011_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Stack_1_t2010_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_StrongName_t2438_0_0_0;
extern const Il2CppGenericInst GenInst_DateTime_t111_0_0_0;
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1420_0_0_0;
extern const Il2CppGenericInst GenInst_TimeSpan_t112_0_0_0;
extern const Il2CppGenericInst GenInst_Guid_t108_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t449_0_0_0;
extern const Il2CppGenericInst GenInst_SByte_t170_0_0_0;
extern const Il2CppGenericInst GenInst_Int16_t534_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t454_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2629_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Double_t1407_0_0_0;
extern const Il2CppGenericInst GenInst_Decimal_t1059_0_0_0;
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2630_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2632_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2631_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m14043_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14055_gp_0_0_0_0_Array_Sort_m14055_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14056_gp_0_0_0_0_Array_Sort_m14056_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14057_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14057_gp_0_0_0_0_Array_Sort_m14057_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14058_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14058_gp_0_0_0_0_Array_Sort_m14058_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14059_gp_0_0_0_0_Array_Sort_m14059_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14060_gp_0_0_0_0_Array_Sort_m14060_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14061_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14061_gp_0_0_0_0_Array_Sort_m14061_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14062_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14062_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14062_gp_0_0_0_0_Array_Sort_m14062_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14063_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m14064_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14065_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14065_gp_0_0_0_0_Array_qsort_m14065_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_compare_m14066_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m14067_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Resize_m14070_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m14072_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ForEach_m14073_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m14074_gp_0_0_0_0_Array_ConvertAll_m14074_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14075_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14076_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m14077_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14078_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14079_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m14080_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14081_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14082_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14083_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m14084_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14085_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14086_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m14087_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14088_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14089_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m14090_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindAll_m14091_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Exists_m14092_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m14093_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Find_m14094_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLast_m14095_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IList_1_t2633_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ICollection_1_t2634_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Nullable_1_t2602_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2641_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Comparer_1_t2640_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2642_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2644_gp_0_0_0_0_ShimEnumerator_t2644_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2645_gp_0_0_0_0_Enumerator_t2645_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2935_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2647_gp_0_0_0_0_Enumerator_t2647_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2647_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2646_gp_0_0_0_0_KeyCollection_t2646_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2646_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2646_gp_0_0_0_0_KeyCollection_t2646_gp_1_0_0_0_KeyCollection_t2646_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2646_gp_0_0_0_0_KeyCollection_t2646_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2649_gp_0_0_0_0_Enumerator_t2649_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2649_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2648_gp_0_0_0_0_ValueCollection_t2648_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2648_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2648_gp_0_0_0_0_ValueCollection_t2648_gp_1_0_0_0_ValueCollection_t2648_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2648_gp_1_0_0_0_ValueCollection_t2648_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0_Dictionary_2_t2643_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2972_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0_Dictionary_2_t2643_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m14244_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0_Dictionary_2_t2643_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m14249_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m14249_gp_0_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0_Dictionary_2_t2643_gp_1_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1996_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2643_gp_0_0_0_0_Dictionary_2_t2643_gp_1_0_0_0_KeyValuePair_2_t2972_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2972_0_0_0_KeyValuePair_2_t2972_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2652_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2651_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2653_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2655_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2655_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4554_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2655_gp_0_0_0_0_IDictionary_2_t2655_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2657_gp_0_0_0_0_KeyValuePair_2_t2657_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2659_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t2658_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Collection_1_t2660_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2661_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m14526_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m14526_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m14527_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Version_t1875_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t123_0_0_0;
extern const Il2CppGenericInst GenInst_IConvertible_t165_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_t166_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_t550_0_0_0;
extern const Il2CppGenericInst GenInst_ICloneable_t512_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2733_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2734_0_0_0;
extern const Il2CppGenericInst GenInst_IFormattable_t164_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2721_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2722_0_0_0;
extern const Il2CppGenericInst GenInst_ValueType_t524_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2729_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2730_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2699_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2700_0_0_0;
extern const Il2CppGenericInst GenInst_Material_t4_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t15_0_0_0_Object_t_0_0_0_Vector3ArrayOptions_t951_0_0_0;
extern const Il2CppGenericInst GenInst_IReflect_t2637_0_0_0;
extern const Il2CppGenericInst GenInst__Type_t2635_0_0_0;
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppGenericInst GenInst__MemberInfo_t2636_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2741_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2742_0_0_0;
extern const Il2CppGenericInst GenInst_Behaviour_t477_0_0_0;
extern const Il2CppGenericInst GenInst_MonoBehaviour_t7_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3197_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t2136_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Object_t_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Object_t_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3197_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_PointerEventData_t236_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t427_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit2D_t432_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3228_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t127_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t127_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t127_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t127_0_0_0_KeyValuePair_2_t3228_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t411_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t411_0_0_0_Int32_t127_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3254_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t266_0_0_0_List_1_t437_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t437_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3264_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t278_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t278_0_0_0_Int32_t127_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t274_0_0_0_IndexedSet_1_t448_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t448_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3295_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3299_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3303_0_0_0;
extern const Il2CppGenericInst GenInst_Enum_t167_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t449_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t169_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t151_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2738_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2739_0_0_0;
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t586_0_0_0;
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t614_0_0_0_Image_t615_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t615_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3386_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Trackable_t565_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3404_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2712_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2713_0_0_0;
extern const Il2CppGenericInst GenInst_Color32_t421_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButton_t731_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3415_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Marker_t739_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3432_0_0_0;
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t650_0_0_0;
extern const Il2CppGenericInst GenInst_SurfaceData_t651_0_0_0;
extern const Il2CppGenericInst GenInst_PropData_t652_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t174_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t454_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3462_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t454_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t454_0_0_0_UInt16_t454_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t454_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t454_0_0_0_KeyValuePair_2_t3462_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t454_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3476_0_0_0;
extern const Il2CppGenericInst GenInst_RectangleData_t596_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_WordResult_t695_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3483_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_WordAbstractBehaviour_t93_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t841_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t692_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t692_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3502_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Surface_t98_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t855_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_SurfaceAbstractBehaviour_t73_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3523_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Prop_t97_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t854_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_PropAbstractBehaviour_t65_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3529_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableBehaviour_t44_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3544_0_0_0;
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t58_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t182_0_0_0;
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t175_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t173_0_0_0;
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t191_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3550_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableResultData_t642_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableResultData_t642_0_0_0_TrackableResultData_t642_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableResultData_t642_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_TrackableResultData_t642_0_0_0_KeyValuePair_2_t3550_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3565_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t643_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonData_t643_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonData_t643_0_0_0_VirtualButtonData_t643_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonData_t643_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonData_t643_0_0_0_KeyValuePair_2_t3565_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_ImageTarget_t732_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3596_0_0_0;
extern const Il2CppGenericInst GenInst_WebCamDevice_t879_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t734_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3604_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t734_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t734_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t734_0_0_0_ProfileData_t734_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t734_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t734_0_0_0_KeyValuePair_2_t3604_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t734_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3618_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_VirtualButtonAbstractBehaviour_t86_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3623_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t3653_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_NoOptions_t933_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_StringOptions_t1003_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1456_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievement_t1358_0_0_0;
extern const Il2CppGenericInst GenInst_IScore_t1320_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfile_t1455_0_0_0;
extern const Il2CppGenericInst GenInst_AchievementDescription_t1318_0_0_0;
extern const Il2CppGenericInst GenInst_UserProfile_t1316_0_0_0;
extern const Il2CppGenericInst GenInst_GcAchievementData_t1305_0_0_0;
extern const Il2CppGenericInst GenInst_Achievement_t1317_0_0_0;
extern const Il2CppGenericInst GenInst_GcScoreData_t1306_0_0_0;
extern const Il2CppGenericInst GenInst_Score_t1319_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutOption_t1178_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_LayoutCache_t1170_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutCache_t1170_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3720_0_0_0;
extern const Il2CppGenericInst GenInst_GUIStyle_t1172_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1172_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3731_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t127_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3735_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1395_0_0_0;
extern const Il2CppGenericInst GenInst_Display_t1213_0_0_0;
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0;
extern const Il2CppGenericInst GenInst_ISerializable_t513_0_0_0;
extern const Il2CppGenericInst GenInst_Keyframe_t1236_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1092_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3777_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2704_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2705_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1092_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1092_0_0_0_Int64_t1092_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1092_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1092_0_0_0_KeyValuePair_2_t3777_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1092_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3792_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3815_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2710_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2711_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_Object_t_0_0_0_UInt64_t1091_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_Object_t_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3815_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1267_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t1267_0_0_0_NetworkAccessToken_t1269_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t1269_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3830_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t1286_0_0_0;
extern const Il2CppGenericInst GenInst_GetDelegate_t1284_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1376_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1419_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1377_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3836_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t1286_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3843_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1376_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3847_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1377_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3851_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t1284_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0_KeyValuePair_2_t3254_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3254_0_0_0_KeyValuePair_2_t3836_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1419_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3873_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2673_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBase_t1434_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBase_t2676_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterInfo_t1426_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterInfo_t2679_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyInfo_t2680_0_0_0;
extern const Il2CppGenericInst GenInst__FieldInfo_t2675_0_0_0;
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t501_0_0_0;
extern const Il2CppGenericInst GenInst_Attribute_t138_0_0_0;
extern const Il2CppGenericInst GenInst__Attribute_t901_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t500_0_0_0;
extern const Il2CppGenericInst GenInst_RequireComponent_t163_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterModifier_t2260_0_0_0;
extern const Il2CppGenericInst GenInst_HitInfo_t1323_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t317_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t317_0_0_0_TextEditOp_t1335_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_TextEditOp_t1335_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3891_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2707_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2708_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1659_0_0_0;
extern const Il2CppGenericInst GenInst_KeySizes_t1808_0_0_0;
extern const Il2CppGenericInst GenInst_X509Certificate_t1771_0_0_0;
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1609_0_0_0;
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1775_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3932_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t449_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t449_0_0_0_Byte_t449_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t449_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t449_0_0_0_KeyValuePair_2_t3932_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t169_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3946_0_0_0;
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1902_0_0_0;
extern const Il2CppGenericInst GenInst_Capture_t1923_0_0_0;
extern const Il2CppGenericInst GenInst_Group_t1063_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3954_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Int32_t127_0_0_0_Int32_t127_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Int32_t127_0_0_0_DictionaryEntry_t1996_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t127_0_0_0_Int32_t127_0_0_0_KeyValuePair_2_t3954_0_0_0;
extern const Il2CppGenericInst GenInst_Mark_t1948_0_0_0;
extern const Il2CppGenericInst GenInst_UriScheme_t1984_0_0_0;
extern const Il2CppGenericInst GenInst_Delegate_t143_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2718_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2719_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2715_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2716_0_0_0;
extern const Il2CppGenericInst GenInst_TableRange_t2069_0_0_0;
extern const Il2CppGenericInst GenInst_TailoringInfo_t2072_0_0_0;
extern const Il2CppGenericInst GenInst_Contraction_t2073_0_0_0;
extern const Il2CppGenericInst GenInst_Level2Map_t2075_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t2095_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t2146_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t2153_0_0_0;
extern const Il2CppGenericInst GenInst_StackFrame_t1433_0_0_0;
extern const Il2CppGenericInst GenInst_Calendar_t2165_0_0_0;
extern const Il2CppGenericInst GenInst_ModuleBuilder_t2225_0_0_0;
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2667_0_0_0;
extern const Il2CppGenericInst GenInst_Module_t2226_0_0_0;
extern const Il2CppGenericInst GenInst__Module_t2678_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterBuilder_t2227_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2668_0_0_0;
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2223_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBuilder_t2222_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBuilder_t2666_0_0_0;
extern const Il2CppGenericInst GenInst__MethodInfo_t2677_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2218_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2663_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyBuilder_t2228_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2669_0_0_0;
extern const Il2CppGenericInst GenInst_FieldBuilder_t2221_0_0_0;
extern const Il2CppGenericInst GenInst__FieldBuilder_t2665_0_0_0;
extern const Il2CppGenericInst GenInst_Header_t2322_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2610_0_0_0;
extern const Il2CppGenericInst GenInst_IContextAttribute_t2605_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t3068_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t3069_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2744_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2745_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t3088_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t3089_0_0_0;
extern const Il2CppGenericInst GenInst_TypeTag_t2360_0_0_0;
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t231_0_0_0_RaycastResult_t231_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3197_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3197_0_0_0_KeyValuePair_2_t3197_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3228_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3228_0_0_0_KeyValuePair_2_t3228_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3254_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3254_0_0_0_KeyValuePair_2_t3254_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t313_0_0_0_UIVertex_t313_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t454_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t454_0_0_0_UInt16_t454_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3462_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3462_0_0_0_KeyValuePair_2_t3462_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t642_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_TrackableResultData_t642_0_0_0_TrackableResultData_t642_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3550_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3550_0_0_0_KeyValuePair_2_t3550_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t643_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButtonData_t643_0_0_0_VirtualButtonData_t643_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3565_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3565_0_0_0_KeyValuePair_2_t3565_0_0_0;
extern const Il2CppGenericInst GenInst_TargetSearchResult_t720_0_0_0_TargetSearchResult_t720_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t734_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_ProfileData_t734_0_0_0_ProfileData_t734_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3604_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3604_0_0_0_KeyValuePair_2_t3604_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t460_0_0_0_UICharInfo_t460_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t458_0_0_0_UILineInfo_t458_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1092_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1092_0_0_0_Int64_t1092_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3777_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3777_0_0_0_KeyValuePair_2_t3777_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1091_0_0_0_UInt64_t1091_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3815_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3815_0_0_0_KeyValuePair_2_t3815_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3836_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3836_0_0_0_KeyValuePair_2_t3836_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t449_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t449_0_0_0_Byte_t449_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3932_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3932_0_0_0_KeyValuePair_2_t3932_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3954_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3954_0_0_0_KeyValuePair_2_t3954_0_0_0;
const methodPointerType g_Il2CppMethodPointers[4711] = 
{
	NULL/* 0*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m1961_gshared/* 1*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m1944_gshared/* 2*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m2017_gshared/* 3*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m27818_gshared/* 4*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m27814_gshared/* 5*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m27843_gshared/* 6*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m1998_gshared/* 7*/,
	(methodPointerType)&EventFunction_1__ctor_m15306_gshared/* 8*/,
	(methodPointerType)&EventFunction_1_Invoke_m15308_gshared/* 9*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m15310_gshared/* 10*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m15312_gshared/* 11*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m2138_gshared/* 12*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m2401_gshared/* 13*/,
	(methodPointerType)&IndexedSet_1_get_Count_m16506_gshared/* 14*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m16508_gshared/* 15*/,
	(methodPointerType)&IndexedSet_1_get_Item_m16516_gshared/* 16*/,
	(methodPointerType)&IndexedSet_1_set_Item_m16518_gshared/* 17*/,
	(methodPointerType)&IndexedSet_1__ctor_m16490_gshared/* 18*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared/* 19*/,
	(methodPointerType)&IndexedSet_1_Add_m16494_gshared/* 20*/,
	(methodPointerType)&IndexedSet_1_Remove_m16496_gshared/* 21*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m16498_gshared/* 22*/,
	(methodPointerType)&IndexedSet_1_Clear_m16500_gshared/* 23*/,
	(methodPointerType)&IndexedSet_1_Contains_m16502_gshared/* 24*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m16504_gshared/* 25*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m16510_gshared/* 26*/,
	(methodPointerType)&IndexedSet_1_Insert_m16512_gshared/* 27*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m16514_gshared/* 28*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m16519_gshared/* 29*/,
	(methodPointerType)&IndexedSet_1_Sort_m16520_gshared/* 30*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m15409_gshared/* 31*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m15411_gshared/* 32*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m15413_gshared/* 33*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m15415_gshared/* 34*/,
	(methodPointerType)&ObjectPool_1__ctor_m15407_gshared/* 35*/,
	(methodPointerType)&ObjectPool_1_Get_m15417_gshared/* 36*/,
	(methodPointerType)&ObjectPool_1_Release_m15419_gshared/* 37*/,
	(methodPointerType)&NullPremiumObjectFactory_CreateReconstruction_TisObject_t_m28070_gshared/* 38*/,
	(methodPointerType)&SmartTerrainBuilderImpl_CreateReconstruction_TisObject_t_m28137_gshared/* 39*/,
	(methodPointerType)&TrackerManagerImpl_GetTracker_TisObject_t_m28245_gshared/* 40*/,
	(methodPointerType)&TrackerManagerImpl_InitTracker_TisObject_t_m28246_gshared/* 41*/,
	(methodPointerType)&TrackerManagerImpl_DeinitTracker_TisObject_t_m28247_gshared/* 42*/,
	(methodPointerType)&QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340_gshared/* 43*/,
	(methodPointerType)&TweenSettingsExtensions_SetTarget_TisObject_t_m5519_gshared/* 44*/,
	(methodPointerType)&TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared/* 45*/,
	(methodPointerType)&TweenSettingsExtensions_OnComplete_TisObject_t_m236_gshared/* 46*/,
	(methodPointerType)&TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared/* 47*/,
	(methodPointerType)&TweenCallback_1__ctor_m23599_gshared/* 48*/,
	(methodPointerType)&TweenCallback_1_Invoke_m23600_gshared/* 49*/,
	(methodPointerType)&TweenCallback_1_BeginInvoke_m23601_gshared/* 50*/,
	(methodPointerType)&TweenCallback_1_EndInvoke_m23602_gshared/* 51*/,
	(methodPointerType)&DOGetter_1__ctor_m23603_gshared/* 52*/,
	(methodPointerType)&DOGetter_1_Invoke_m23604_gshared/* 53*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23605_gshared/* 54*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23606_gshared/* 55*/,
	(methodPointerType)&DOSetter_1__ctor_m23607_gshared/* 56*/,
	(methodPointerType)&DOSetter_1_Invoke_m23608_gshared/* 57*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23609_gshared/* 58*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23610_gshared/* 59*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisObject_t_m28367_gshared/* 60*/,
	(methodPointerType)&Object_Instantiate_TisObject_t_m381_gshared/* 61*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m262_gshared/* 62*/,
	(methodPointerType)&Component_GetComponentInChildren_TisObject_t_m4326_gshared/* 63*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m343_gshared/* 64*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m27998_gshared/* 65*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m4438_gshared/* 66*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m2416_gshared/* 67*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m1942_gshared/* 68*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m2036_gshared/* 69*/,
	(methodPointerType)&GameObject_GetComponentInChildren_TisObject_t_m4614_gshared/* 70*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m27817_gshared/* 71*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m27769_gshared/* 72*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m27999_gshared/* 73*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m479_gshared/* 74*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m2083_gshared/* 75*/,
	(methodPointerType)&GameObject_AddComponent_TisObject_t_m437_gshared/* 76*/,
	(methodPointerType)&ResponseBase_ParseJSONList_TisObject_t_m6958_gshared/* 77*/,
	(methodPointerType)&NetworkMatch_ProcessMatchResponse_TisObject_t_m6965_gshared/* 78*/,
	(methodPointerType)&ResponseDelegate_1__ctor_m25832_gshared/* 79*/,
	(methodPointerType)&ResponseDelegate_1_Invoke_m25834_gshared/* 80*/,
	(methodPointerType)&ResponseDelegate_1_BeginInvoke_m25836_gshared/* 81*/,
	(methodPointerType)&ResponseDelegate_1_EndInvoke_m25838_gshared/* 82*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25840_gshared/* 83*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25841_gshared/* 84*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25839_gshared/* 85*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25842_gshared/* 86*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25843_gshared/* 87*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Keys_m25983_gshared/* 88*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Values_m25989_gshared/* 89*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Item_m25991_gshared/* 90*/,
	(methodPointerType)&ThreadSafeDictionary_2_set_Item_m25993_gshared/* 91*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Count_m26003_gshared/* 92*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_IsReadOnly_m26005_gshared/* 93*/,
	(methodPointerType)&ThreadSafeDictionary_2__ctor_m25973_gshared/* 94*/,
	(methodPointerType)&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975_gshared/* 95*/,
	(methodPointerType)&ThreadSafeDictionary_2_Get_m25977_gshared/* 96*/,
	(methodPointerType)&ThreadSafeDictionary_2_AddValue_m25979_gshared/* 97*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m25981_gshared/* 98*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m25985_gshared/* 99*/,
	(methodPointerType)&ThreadSafeDictionary_2_TryGetValue_m25987_gshared/* 100*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m25995_gshared/* 101*/,
	(methodPointerType)&ThreadSafeDictionary_2_Clear_m25997_gshared/* 102*/,
	(methodPointerType)&ThreadSafeDictionary_2_Contains_m25999_gshared/* 103*/,
	(methodPointerType)&ThreadSafeDictionary_2_CopyTo_m26001_gshared/* 104*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m26007_gshared/* 105*/,
	(methodPointerType)&ThreadSafeDictionary_2_GetEnumerator_m26009_gshared/* 106*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2__ctor_m25966_gshared/* 107*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_Invoke_m25968_gshared/* 108*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m25970_gshared/* 109*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_EndInvoke_m25972_gshared/* 110*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m27842_gshared/* 111*/,
	(methodPointerType)&InvokableCall_1__ctor_m15888_gshared/* 112*/,
	(methodPointerType)&InvokableCall_1__ctor_m15889_gshared/* 113*/,
	(methodPointerType)&InvokableCall_1_Invoke_m15890_gshared/* 114*/,
	(methodPointerType)&InvokableCall_1_Find_m15891_gshared/* 115*/,
	(methodPointerType)&InvokableCall_2__ctor_m26704_gshared/* 116*/,
	(methodPointerType)&InvokableCall_2_Invoke_m26705_gshared/* 117*/,
	(methodPointerType)&InvokableCall_2_Find_m26706_gshared/* 118*/,
	(methodPointerType)&InvokableCall_3__ctor_m26711_gshared/* 119*/,
	(methodPointerType)&InvokableCall_3_Invoke_m26712_gshared/* 120*/,
	(methodPointerType)&InvokableCall_3_Find_m26713_gshared/* 121*/,
	(methodPointerType)&InvokableCall_4__ctor_m26718_gshared/* 122*/,
	(methodPointerType)&InvokableCall_4_Invoke_m26719_gshared/* 123*/,
	(methodPointerType)&InvokableCall_4_Find_m26720_gshared/* 124*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m26725_gshared/* 125*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26726_gshared/* 126*/,
	(methodPointerType)&UnityEvent_1__ctor_m15878_gshared/* 127*/,
	(methodPointerType)&UnityEvent_1_AddListener_m15880_gshared/* 128*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m15882_gshared/* 129*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m15883_gshared/* 130*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m15884_gshared/* 131*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m15886_gshared/* 132*/,
	(methodPointerType)&UnityEvent_1_Invoke_m15887_gshared/* 133*/,
	(methodPointerType)&UnityEvent_2__ctor_m26929_gshared/* 134*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m26930_gshared/* 135*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m26931_gshared/* 136*/,
	(methodPointerType)&UnityEvent_3__ctor_m26932_gshared/* 137*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m26933_gshared/* 138*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m26934_gshared/* 139*/,
	(methodPointerType)&UnityEvent_4__ctor_m26935_gshared/* 140*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m26936_gshared/* 141*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m26937_gshared/* 142*/,
	(methodPointerType)&UnityAction_1__ctor_m15436_gshared/* 143*/,
	(methodPointerType)&UnityAction_1_Invoke_m15437_gshared/* 144*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m15438_gshared/* 145*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m15439_gshared/* 146*/,
	(methodPointerType)&UnityAction_2__ctor_m26707_gshared/* 147*/,
	(methodPointerType)&UnityAction_2_Invoke_m26708_gshared/* 148*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m26709_gshared/* 149*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m26710_gshared/* 150*/,
	(methodPointerType)&UnityAction_3__ctor_m26714_gshared/* 151*/,
	(methodPointerType)&UnityAction_3_Invoke_m26715_gshared/* 152*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m26716_gshared/* 153*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m26717_gshared/* 154*/,
	(methodPointerType)&UnityAction_4__ctor_m26721_gshared/* 155*/,
	(methodPointerType)&UnityAction_4_Invoke_m26722_gshared/* 156*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m26723_gshared/* 157*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m26724_gshared/* 158*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23147_gshared/* 159*/,
	(methodPointerType)&HashSet_1_get_Count_m23155_gshared/* 160*/,
	(methodPointerType)&HashSet_1__ctor_m23141_gshared/* 161*/,
	(methodPointerType)&HashSet_1__ctor_m23143_gshared/* 162*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23145_gshared/* 163*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23149_gshared/* 164*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23151_gshared/* 165*/,
	(methodPointerType)&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23153_gshared/* 166*/,
	(methodPointerType)&HashSet_1_Init_m23157_gshared/* 167*/,
	(methodPointerType)&HashSet_1_InitArrays_m23159_gshared/* 168*/,
	(methodPointerType)&HashSet_1_SlotsContainsAt_m23161_gshared/* 169*/,
	(methodPointerType)&HashSet_1_CopyTo_m23163_gshared/* 170*/,
	(methodPointerType)&HashSet_1_CopyTo_m23165_gshared/* 171*/,
	(methodPointerType)&HashSet_1_Resize_m23167_gshared/* 172*/,
	(methodPointerType)&HashSet_1_GetLinkHashCode_m23169_gshared/* 173*/,
	(methodPointerType)&HashSet_1_GetItemHashCode_m23171_gshared/* 174*/,
	(methodPointerType)&HashSet_1_Add_m23172_gshared/* 175*/,
	(methodPointerType)&HashSet_1_Clear_m23174_gshared/* 176*/,
	(methodPointerType)&HashSet_1_Contains_m23176_gshared/* 177*/,
	(methodPointerType)&HashSet_1_Remove_m23178_gshared/* 178*/,
	(methodPointerType)&HashSet_1_GetObjectData_m23180_gshared/* 179*/,
	(methodPointerType)&HashSet_1_OnDeserialization_m23182_gshared/* 180*/,
	(methodPointerType)&HashSet_1_GetEnumerator_m23183_gshared/* 181*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23190_gshared/* 182*/,
	(methodPointerType)&Enumerator_get_Current_m23192_gshared/* 183*/,
	(methodPointerType)&Enumerator__ctor_m23189_gshared/* 184*/,
	(methodPointerType)&Enumerator_MoveNext_m23191_gshared/* 185*/,
	(methodPointerType)&Enumerator_Dispose_m23193_gshared/* 186*/,
	(methodPointerType)&Enumerator_CheckState_m23194_gshared/* 187*/,
	(methodPointerType)&PrimeHelper__cctor_m23195_gshared/* 188*/,
	(methodPointerType)&PrimeHelper_TestPrime_m23196_gshared/* 189*/,
	(methodPointerType)&PrimeHelper_CalcPrime_m23197_gshared/* 190*/,
	(methodPointerType)&PrimeHelper_ToPrime_m23198_gshared/* 191*/,
	(methodPointerType)&Enumerable_Any_TisObject_t_m4407_gshared/* 192*/,
	(methodPointerType)&Enumerable_Cast_TisObject_t_m4383_gshared/* 193*/,
	(methodPointerType)&Enumerable_CreateCastIterator_TisObject_t_m28068_gshared/* 194*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m4304_gshared/* 195*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m28022_gshared/* 196*/,
	(methodPointerType)&Enumerable_First_TisObject_t_m4459_gshared/* 197*/,
	(methodPointerType)&Enumerable_ToArray_TisObject_t_m4455_gshared/* 198*/,
	(methodPointerType)&Enumerable_ToList_TisObject_t_m428_gshared/* 199*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m2369_gshared/* 200*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m27997_gshared/* 201*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19569_gshared/* 202*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19570_gshared/* 203*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19568_gshared/* 204*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19571_gshared/* 205*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19572_gshared/* 206*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19573_gshared/* 207*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19574_gshared/* 208*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18148_gshared/* 209*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18149_gshared/* 210*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18147_gshared/* 211*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18150_gshared/* 212*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18151_gshared/* 213*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18152_gshared/* 214*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18153_gshared/* 215*/,
	(methodPointerType)&Func_2__ctor_m26938_gshared/* 216*/,
	(methodPointerType)&Func_2_Invoke_m26939_gshared/* 217*/,
	(methodPointerType)&Func_2_BeginInvoke_m26940_gshared/* 218*/,
	(methodPointerType)&Func_2_EndInvoke_m26941_gshared/* 219*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26973_gshared/* 220*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26974_gshared/* 221*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26975_gshared/* 222*/,
	(methodPointerType)&LinkedList_1_get_Count_m26988_gshared/* 223*/,
	(methodPointerType)&LinkedList_1_get_First_m26989_gshared/* 224*/,
	(methodPointerType)&LinkedList_1__ctor_m26967_gshared/* 225*/,
	(methodPointerType)&LinkedList_1__ctor_m26968_gshared/* 226*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26969_gshared/* 227*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m26970_gshared/* 228*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26971_gshared/* 229*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26972_gshared/* 230*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m26976_gshared/* 231*/,
	(methodPointerType)&LinkedList_1_AddLast_m26977_gshared/* 232*/,
	(methodPointerType)&LinkedList_1_Clear_m26978_gshared/* 233*/,
	(methodPointerType)&LinkedList_1_Contains_m26979_gshared/* 234*/,
	(methodPointerType)&LinkedList_1_CopyTo_m26980_gshared/* 235*/,
	(methodPointerType)&LinkedList_1_Find_m26981_gshared/* 236*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m26982_gshared/* 237*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m26983_gshared/* 238*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m26984_gshared/* 239*/,
	(methodPointerType)&LinkedList_1_Remove_m26985_gshared/* 240*/,
	(methodPointerType)&LinkedList_1_Remove_m26986_gshared/* 241*/,
	(methodPointerType)&LinkedList_1_RemoveLast_m26987_gshared/* 242*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26997_gshared/* 243*/,
	(methodPointerType)&Enumerator_get_Current_m26998_gshared/* 244*/,
	(methodPointerType)&Enumerator__ctor_m26996_gshared/* 245*/,
	(methodPointerType)&Enumerator_MoveNext_m26999_gshared/* 246*/,
	(methodPointerType)&Enumerator_Dispose_m27000_gshared/* 247*/,
	(methodPointerType)&LinkedListNode_1_get_List_m26993_gshared/* 248*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m26994_gshared/* 249*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m26995_gshared/* 250*/,
	(methodPointerType)&LinkedListNode_1__ctor_m26990_gshared/* 251*/,
	(methodPointerType)&LinkedListNode_1__ctor_m26991_gshared/* 252*/,
	(methodPointerType)&LinkedListNode_1_Detach_m26992_gshared/* 253*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m15421_gshared/* 254*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m15422_gshared/* 255*/,
	(methodPointerType)&Stack_1_get_Count_m15429_gshared/* 256*/,
	(methodPointerType)&Stack_1__ctor_m15420_gshared/* 257*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m15423_gshared/* 258*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15424_gshared/* 259*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m15425_gshared/* 260*/,
	(methodPointerType)&Stack_1_Peek_m15426_gshared/* 261*/,
	(methodPointerType)&Stack_1_Pop_m15427_gshared/* 262*/,
	(methodPointerType)&Stack_1_Push_m15428_gshared/* 263*/,
	(methodPointerType)&Stack_1_GetEnumerator_m15430_gshared/* 264*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15432_gshared/* 265*/,
	(methodPointerType)&Enumerator_get_Current_m15435_gshared/* 266*/,
	(methodPointerType)&Enumerator__ctor_m15431_gshared/* 267*/,
	(methodPointerType)&Enumerator_Dispose_m15433_gshared/* 268*/,
	(methodPointerType)&Enumerator_MoveNext_m15434_gshared/* 269*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m27746_gshared/* 270*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m27738_gshared/* 271*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m27741_gshared/* 272*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m27739_gshared/* 273*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m27740_gshared/* 274*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m27743_gshared/* 275*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m27742_gshared/* 276*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m27737_gshared/* 277*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m27745_gshared/* 278*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m27796_gshared/* 279*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28646_gshared/* 280*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28647_gshared/* 281*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28648_gshared/* 282*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28649_gshared/* 283*/,
	(methodPointerType)&Array_Sort_TisObject_t_m14001_gshared/* 284*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m28650_gshared/* 285*/,
	(methodPointerType)&Array_Sort_TisObject_t_m27795_gshared/* 286*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m27794_gshared/* 287*/,
	(methodPointerType)&Array_Sort_TisObject_t_m28651_gshared/* 288*/,
	(methodPointerType)&Array_Sort_TisObject_t_m27812_gshared/* 289*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m27797_gshared/* 290*/,
	(methodPointerType)&Array_compare_TisObject_t_m27809_gshared/* 291*/,
	(methodPointerType)&Array_qsort_TisObject_t_m27811_gshared/* 292*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m27810_gshared/* 293*/,
	(methodPointerType)&Array_swap_TisObject_t_m27813_gshared/* 294*/,
	(methodPointerType)&Array_Resize_TisObject_t_m5535_gshared/* 295*/,
	(methodPointerType)&Array_Resize_TisObject_t_m27793_gshared/* 296*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m28652_gshared/* 297*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m28653_gshared/* 298*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m28654_gshared/* 299*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28656_gshared/* 300*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28657_gshared/* 301*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m28655_gshared/* 302*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28659_gshared/* 303*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28660_gshared/* 304*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m28658_gshared/* 305*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28662_gshared/* 306*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28663_gshared/* 307*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28664_gshared/* 308*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m28661_gshared/* 309*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14003_gshared/* 310*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m28665_gshared/* 311*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14000_gshared/* 312*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28667_gshared/* 313*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28666_gshared/* 314*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m28668_gshared/* 315*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m28669_gshared/* 316*/,
	(methodPointerType)&Array_Exists_TisObject_t_m28670_gshared/* 317*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m28671_gshared/* 318*/,
	(methodPointerType)&Array_Find_TisObject_t_m28672_gshared/* 319*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m28673_gshared/* 320*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared/* 321*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14866_gshared/* 322*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14858_gshared/* 323*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14862_gshared/* 324*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14864_gshared/* 325*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m27419_gshared/* 326*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m27420_gshared/* 327*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m27421_gshared/* 328*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m27422_gshared/* 329*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m27417_gshared/* 330*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27418_gshared/* 331*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m27423_gshared/* 332*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m27424_gshared/* 333*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m27425_gshared/* 334*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m27426_gshared/* 335*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m27427_gshared/* 336*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m27428_gshared/* 337*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m27429_gshared/* 338*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m27430_gshared/* 339*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m27431_gshared/* 340*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m27432_gshared/* 341*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27434_gshared/* 342*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27435_gshared/* 343*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m27433_gshared/* 344*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27436_gshared/* 345*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27437_gshared/* 346*/,
	(methodPointerType)&Comparer_1_get_Default_m15141_gshared/* 347*/,
	(methodPointerType)&Comparer_1__ctor_m15138_gshared/* 348*/,
	(methodPointerType)&Comparer_1__cctor_m15139_gshared/* 349*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15140_gshared/* 350*/,
	(methodPointerType)&DefaultComparer__ctor_m15142_gshared/* 351*/,
	(methodPointerType)&DefaultComparer_Compare_m15143_gshared/* 352*/,
	(methodPointerType)&GenericComparer_1__ctor_m27463_gshared/* 353*/,
	(methodPointerType)&GenericComparer_1_Compare_m27464_gshared/* 354*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16772_gshared/* 355*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16774_gshared/* 356*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16776_gshared/* 357*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16778_gshared/* 358*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16786_gshared/* 359*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16788_gshared/* 360*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16790_gshared/* 361*/,
	(methodPointerType)&Dictionary_2_get_Count_m16808_gshared/* 362*/,
	(methodPointerType)&Dictionary_2_get_Item_m16810_gshared/* 363*/,
	(methodPointerType)&Dictionary_2_set_Item_m16812_gshared/* 364*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16846_gshared/* 365*/,
	(methodPointerType)&Dictionary_2_get_Values_m16848_gshared/* 366*/,
	(methodPointerType)&Dictionary_2__ctor_m16760_gshared/* 367*/,
	(methodPointerType)&Dictionary_2__ctor_m16762_gshared/* 368*/,
	(methodPointerType)&Dictionary_2__ctor_m16764_gshared/* 369*/,
	(methodPointerType)&Dictionary_2__ctor_m16766_gshared/* 370*/,
	(methodPointerType)&Dictionary_2__ctor_m16768_gshared/* 371*/,
	(methodPointerType)&Dictionary_2__ctor_m16770_gshared/* 372*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16780_gshared/* 373*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16782_gshared/* 374*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16784_gshared/* 375*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16792_gshared/* 376*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16794_gshared/* 377*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared/* 378*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16798_gshared/* 379*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16800_gshared/* 380*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16802_gshared/* 381*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16804_gshared/* 382*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16806_gshared/* 383*/,
	(methodPointerType)&Dictionary_2_Init_m16814_gshared/* 384*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16816_gshared/* 385*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16818_gshared/* 386*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27943_gshared/* 387*/,
	(methodPointerType)&Dictionary_2_make_pair_m16820_gshared/* 388*/,
	(methodPointerType)&Dictionary_2_pick_key_m16822_gshared/* 389*/,
	(methodPointerType)&Dictionary_2_pick_value_m16824_gshared/* 390*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16826_gshared/* 391*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27944_gshared/* 392*/,
	(methodPointerType)&Dictionary_2_Resize_m16828_gshared/* 393*/,
	(methodPointerType)&Dictionary_2_Add_m16830_gshared/* 394*/,
	(methodPointerType)&Dictionary_2_Clear_m16832_gshared/* 395*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16834_gshared/* 396*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16836_gshared/* 397*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16838_gshared/* 398*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16840_gshared/* 399*/,
	(methodPointerType)&Dictionary_2_Remove_m16842_gshared/* 400*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16844_gshared/* 401*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16850_gshared/* 402*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16852_gshared/* 403*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16854_gshared/* 404*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16856_gshared/* 405*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16858_gshared/* 406*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16978_gshared/* 407*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16979_gshared/* 408*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16980_gshared/* 409*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16981_gshared/* 410*/,
	(methodPointerType)&ShimEnumerator__ctor_m16976_gshared/* 411*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16977_gshared/* 412*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared/* 413*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared/* 414*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared/* 415*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared/* 416*/,
	(methodPointerType)&Enumerator_get_Current_m16939_gshared/* 417*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16940_gshared/* 418*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16941_gshared/* 419*/,
	(methodPointerType)&Enumerator__ctor_m16933_gshared/* 420*/,
	(methodPointerType)&Enumerator_MoveNext_m16938_gshared/* 421*/,
	(methodPointerType)&Enumerator_VerifyState_m16942_gshared/* 422*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16943_gshared/* 423*/,
	(methodPointerType)&Enumerator_Dispose_m16944_gshared/* 424*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16922_gshared/* 425*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16923_gshared/* 426*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16924_gshared/* 427*/,
	(methodPointerType)&KeyCollection_get_Count_m16927_gshared/* 428*/,
	(methodPointerType)&KeyCollection__ctor_m16914_gshared/* 429*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16915_gshared/* 430*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16916_gshared/* 431*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16917_gshared/* 432*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16918_gshared/* 433*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16919_gshared/* 434*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16920_gshared/* 435*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16921_gshared/* 436*/,
	(methodPointerType)&KeyCollection_CopyTo_m16925_gshared/* 437*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16926_gshared/* 438*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16929_gshared/* 439*/,
	(methodPointerType)&Enumerator_get_Current_m16932_gshared/* 440*/,
	(methodPointerType)&Enumerator__ctor_m16928_gshared/* 441*/,
	(methodPointerType)&Enumerator_Dispose_m16930_gshared/* 442*/,
	(methodPointerType)&Enumerator_MoveNext_m16931_gshared/* 443*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16957_gshared/* 444*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16958_gshared/* 445*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16959_gshared/* 446*/,
	(methodPointerType)&ValueCollection_get_Count_m16962_gshared/* 447*/,
	(methodPointerType)&ValueCollection__ctor_m16949_gshared/* 448*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16950_gshared/* 449*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16951_gshared/* 450*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16952_gshared/* 451*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16953_gshared/* 452*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16954_gshared/* 453*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16955_gshared/* 454*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16956_gshared/* 455*/,
	(methodPointerType)&ValueCollection_CopyTo_m16960_gshared/* 456*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16961_gshared/* 457*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16964_gshared/* 458*/,
	(methodPointerType)&Enumerator_get_Current_m16967_gshared/* 459*/,
	(methodPointerType)&Enumerator__ctor_m16963_gshared/* 460*/,
	(methodPointerType)&Enumerator_Dispose_m16965_gshared/* 461*/,
	(methodPointerType)&Enumerator_MoveNext_m16966_gshared/* 462*/,
	(methodPointerType)&Transform_1__ctor_m16945_gshared/* 463*/,
	(methodPointerType)&Transform_1_Invoke_m16946_gshared/* 464*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16947_gshared/* 465*/,
	(methodPointerType)&Transform_1_EndInvoke_m16948_gshared/* 466*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15125_gshared/* 467*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15121_gshared/* 468*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15122_gshared/* 469*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15123_gshared/* 470*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15124_gshared/* 471*/,
	(methodPointerType)&DefaultComparer__ctor_m15131_gshared/* 472*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15132_gshared/* 473*/,
	(methodPointerType)&DefaultComparer_Equals_m15133_gshared/* 474*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m27465_gshared/* 475*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27466_gshared/* 476*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27467_gshared/* 477*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16909_gshared/* 478*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16910_gshared/* 479*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16911_gshared/* 480*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16912_gshared/* 481*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16908_gshared/* 482*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16913_gshared/* 483*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared/* 484*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared/* 485*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared/* 486*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared/* 487*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared/* 488*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m7195_gshared/* 489*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m7196_gshared/* 490*/,
	(methodPointerType)&List_1_get_Capacity_m15043_gshared/* 491*/,
	(methodPointerType)&List_1_set_Capacity_m15045_gshared/* 492*/,
	(methodPointerType)&List_1_get_Count_m7189_gshared/* 493*/,
	(methodPointerType)&List_1_get_Item_m7212_gshared/* 494*/,
	(methodPointerType)&List_1_set_Item_m7213_gshared/* 495*/,
	(methodPointerType)&List_1__ctor_m6974_gshared/* 496*/,
	(methodPointerType)&List_1__ctor_m14980_gshared/* 497*/,
	(methodPointerType)&List_1__ctor_m14982_gshared/* 498*/,
	(methodPointerType)&List_1__cctor_m14984_gshared/* 499*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared/* 500*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m7192_gshared/* 501*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared/* 502*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m7197_gshared/* 503*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m7199_gshared/* 504*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m7200_gshared/* 505*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m7201_gshared/* 506*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m7202_gshared/* 507*/,
	(methodPointerType)&List_1_Add_m7205_gshared/* 508*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15002_gshared/* 509*/,
	(methodPointerType)&List_1_AddCollection_m15004_gshared/* 510*/,
	(methodPointerType)&List_1_AddEnumerable_m15006_gshared/* 511*/,
	(methodPointerType)&List_1_AddRange_m15007_gshared/* 512*/,
	(methodPointerType)&List_1_AsReadOnly_m15009_gshared/* 513*/,
	(methodPointerType)&List_1_Clear_m7198_gshared/* 514*/,
	(methodPointerType)&List_1_Contains_m7206_gshared/* 515*/,
	(methodPointerType)&List_1_CopyTo_m7207_gshared/* 516*/,
	(methodPointerType)&List_1_Find_m15014_gshared/* 517*/,
	(methodPointerType)&List_1_CheckMatch_m15016_gshared/* 518*/,
	(methodPointerType)&List_1_GetIndex_m15018_gshared/* 519*/,
	(methodPointerType)&List_1_GetEnumerator_m15019_gshared/* 520*/,
	(methodPointerType)&List_1_IndexOf_m7210_gshared/* 521*/,
	(methodPointerType)&List_1_Shift_m15022_gshared/* 522*/,
	(methodPointerType)&List_1_CheckIndex_m15024_gshared/* 523*/,
	(methodPointerType)&List_1_Insert_m7211_gshared/* 524*/,
	(methodPointerType)&List_1_CheckCollection_m15027_gshared/* 525*/,
	(methodPointerType)&List_1_Remove_m7208_gshared/* 526*/,
	(methodPointerType)&List_1_RemoveAll_m15030_gshared/* 527*/,
	(methodPointerType)&List_1_RemoveAt_m7203_gshared/* 528*/,
	(methodPointerType)&List_1_Reverse_m15033_gshared/* 529*/,
	(methodPointerType)&List_1_Sort_m15035_gshared/* 530*/,
	(methodPointerType)&List_1_Sort_m15037_gshared/* 531*/,
	(methodPointerType)&List_1_ToArray_m15039_gshared/* 532*/,
	(methodPointerType)&List_1_TrimExcess_m15041_gshared/* 533*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared/* 534*/,
	(methodPointerType)&Enumerator_get_Current_m15054_gshared/* 535*/,
	(methodPointerType)&Enumerator__ctor_m15049_gshared/* 536*/,
	(methodPointerType)&Enumerator_Dispose_m15051_gshared/* 537*/,
	(methodPointerType)&Enumerator_VerifyState_m15052_gshared/* 538*/,
	(methodPointerType)&Enumerator_MoveNext_m15053_gshared/* 539*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15086_gshared/* 540*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15094_gshared/* 541*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15095_gshared/* 542*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15096_gshared/* 543*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15097_gshared/* 544*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15098_gshared/* 545*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15099_gshared/* 546*/,
	(methodPointerType)&Collection_1_get_Count_m15112_gshared/* 547*/,
	(methodPointerType)&Collection_1_get_Item_m15113_gshared/* 548*/,
	(methodPointerType)&Collection_1_set_Item_m15114_gshared/* 549*/,
	(methodPointerType)&Collection_1__ctor_m15085_gshared/* 550*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15087_gshared/* 551*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15088_gshared/* 552*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15089_gshared/* 553*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15090_gshared/* 554*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15091_gshared/* 555*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15092_gshared/* 556*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15093_gshared/* 557*/,
	(methodPointerType)&Collection_1_Add_m15100_gshared/* 558*/,
	(methodPointerType)&Collection_1_Clear_m15101_gshared/* 559*/,
	(methodPointerType)&Collection_1_ClearItems_m15102_gshared/* 560*/,
	(methodPointerType)&Collection_1_Contains_m15103_gshared/* 561*/,
	(methodPointerType)&Collection_1_CopyTo_m15104_gshared/* 562*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15105_gshared/* 563*/,
	(methodPointerType)&Collection_1_IndexOf_m15106_gshared/* 564*/,
	(methodPointerType)&Collection_1_Insert_m15107_gshared/* 565*/,
	(methodPointerType)&Collection_1_InsertItem_m15108_gshared/* 566*/,
	(methodPointerType)&Collection_1_Remove_m15109_gshared/* 567*/,
	(methodPointerType)&Collection_1_RemoveAt_m15110_gshared/* 568*/,
	(methodPointerType)&Collection_1_RemoveItem_m15111_gshared/* 569*/,
	(methodPointerType)&Collection_1_SetItem_m15115_gshared/* 570*/,
	(methodPointerType)&Collection_1_IsValidItem_m15116_gshared/* 571*/,
	(methodPointerType)&Collection_1_ConvertItem_m15117_gshared/* 572*/,
	(methodPointerType)&Collection_1_CheckWritable_m15118_gshared/* 573*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15119_gshared/* 574*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15120_gshared/* 575*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15061_gshared/* 576*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15062_gshared/* 577*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15063_gshared/* 578*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15073_gshared/* 579*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15074_gshared/* 580*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15075_gshared/* 581*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15076_gshared/* 582*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15077_gshared/* 583*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15078_gshared/* 584*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15083_gshared/* 585*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15084_gshared/* 586*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15055_gshared/* 587*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15056_gshared/* 588*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15057_gshared/* 589*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15058_gshared/* 590*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15059_gshared/* 591*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15060_gshared/* 592*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15064_gshared/* 593*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15065_gshared/* 594*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15066_gshared/* 595*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15067_gshared/* 596*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15068_gshared/* 597*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15069_gshared/* 598*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15070_gshared/* 599*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15071_gshared/* 600*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15072_gshared/* 601*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15079_gshared/* 602*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15080_gshared/* 603*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15081_gshared/* 604*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15082_gshared/* 605*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m28707_gshared/* 606*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m28708_gshared/* 607*/,
	(methodPointerType)&Getter_2__ctor_m27523_gshared/* 608*/,
	(methodPointerType)&Getter_2_Invoke_m27524_gshared/* 609*/,
	(methodPointerType)&Getter_2_BeginInvoke_m27525_gshared/* 610*/,
	(methodPointerType)&Getter_2_EndInvoke_m27526_gshared/* 611*/,
	(methodPointerType)&StaticGetter_1__ctor_m27527_gshared/* 612*/,
	(methodPointerType)&StaticGetter_1_Invoke_m27528_gshared/* 613*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m27529_gshared/* 614*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m27530_gshared/* 615*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m27815_gshared/* 616*/,
	(methodPointerType)&Action_1__ctor_m14968_gshared/* 617*/,
	(methodPointerType)&Action_1_Invoke_m14970_gshared/* 618*/,
	(methodPointerType)&Action_1_BeginInvoke_m14972_gshared/* 619*/,
	(methodPointerType)&Action_1_EndInvoke_m14974_gshared/* 620*/,
	(methodPointerType)&Comparison_1__ctor_m15149_gshared/* 621*/,
	(methodPointerType)&Comparison_1_Invoke_m15150_gshared/* 622*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15151_gshared/* 623*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15152_gshared/* 624*/,
	(methodPointerType)&Converter_2__ctor_m27413_gshared/* 625*/,
	(methodPointerType)&Converter_2_Invoke_m27414_gshared/* 626*/,
	(methodPointerType)&Converter_2_BeginInvoke_m27415_gshared/* 627*/,
	(methodPointerType)&Converter_2_EndInvoke_m27416_gshared/* 628*/,
	(methodPointerType)&Predicate_1__ctor_m15134_gshared/* 629*/,
	(methodPointerType)&Predicate_1_Invoke_m15135_gshared/* 630*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15136_gshared/* 631*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15137_gshared/* 632*/,
	(methodPointerType)&Nullable_1__ctor_m14897_gshared/* 633*/,
	(methodPointerType)&Nullable_1__ctor_m14908_gshared/* 634*/,
	(methodPointerType)&DOGetter_1__ctor_m361_gshared/* 635*/,
	(methodPointerType)&DOSetter_1__ctor_m362_gshared/* 636*/,
	(methodPointerType)&Action_1__ctor_m14961_gshared/* 637*/,
	(methodPointerType)&Comparison_1__ctor_m1947_gshared/* 638*/,
	(methodPointerType)&List_1_Sort_m1952_gshared/* 639*/,
	(methodPointerType)&List_1__ctor_m1992_gshared/* 640*/,
	(methodPointerType)&Dictionary_2__ctor_m16127_gshared/* 641*/,
	(methodPointerType)&Dictionary_2_get_Values_m16214_gshared/* 642*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16287_gshared/* 643*/,
	(methodPointerType)&Enumerator_get_Current_m16293_gshared/* 644*/,
	(methodPointerType)&Enumerator_MoveNext_m16292_gshared/* 645*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16221_gshared/* 646*/,
	(methodPointerType)&Enumerator_get_Current_m16265_gshared/* 647*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16232_gshared/* 648*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16230_gshared/* 649*/,
	(methodPointerType)&Enumerator_MoveNext_m16264_gshared/* 650*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16234_gshared/* 651*/,
	(methodPointerType)&Comparison_1__ctor_m2051_gshared/* 652*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t94_m2048_gshared/* 653*/,
	(methodPointerType)&UnityEvent_1__ctor_m2055_gshared/* 654*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2057_gshared/* 655*/,
	(methodPointerType)&UnityEvent_1_AddListener_m2058_gshared/* 656*/,
	(methodPointerType)&TweenRunner_1__ctor_m2086_gshared/* 657*/,
	(methodPointerType)&TweenRunner_1_Init_m2087_gshared/* 658*/,
	(methodPointerType)&UnityAction_1__ctor_m2114_gshared/* 659*/,
	(methodPointerType)&TweenRunner_1_StartTween_m2115_gshared/* 660*/,
	(methodPointerType)&List_1_get_Capacity_m2119_gshared/* 661*/,
	(methodPointerType)&List_1_set_Capacity_m2120_gshared/* 662*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t127_m2140_gshared/* 663*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisByte_t449_m2142_gshared/* 664*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t151_m2144_gshared/* 665*/,
	(methodPointerType)&List_1__ctor_m2201_gshared/* 666*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisUInt16_t454_m2197_gshared/* 667*/,
	(methodPointerType)&List_1_ToArray_m2256_gshared/* 668*/,
	(methodPointerType)&UnityEvent_1__ctor_m2289_gshared/* 669*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2295_gshared/* 670*/,
	(methodPointerType)&UnityEvent_1__ctor_m2300_gshared/* 671*/,
	(methodPointerType)&UnityAction_1__ctor_m2301_gshared/* 672*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m2302_gshared/* 673*/,
	(methodPointerType)&UnityEvent_1_AddListener_m2303_gshared/* 674*/,
	(methodPointerType)&UnityEvent_1_Invoke_m2309_gshared/* 675*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t320_m2324_gshared/* 676*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t265_m2326_gshared/* 677*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t339_m2327_gshared/* 678*/,
	(methodPointerType)&UnityEvent_1__ctor_m18023_gshared/* 679*/,
	(methodPointerType)&UnityEvent_1_Invoke_m18032_gshared/* 680*/,
	(methodPointerType)&Func_2__ctor_m18136_gshared/* 681*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t127_m2386_gshared/* 682*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t10_m2388_gshared/* 683*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t151_m2395_gshared/* 684*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisByte_t449_m2397_gshared/* 685*/,
	(methodPointerType)&Func_2__ctor_m18250_gshared/* 686*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2569_gshared/* 687*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2570_gshared/* 688*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2578_gshared/* 689*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2579_gshared/* 690*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2580_gshared/* 691*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2581_gshared/* 692*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m18028_gshared/* 693*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18029_gshared/* 694*/,
	(methodPointerType)&Action_1__ctor_m18483_gshared/* 695*/,
	(methodPointerType)&List_1__ctor_m4477_gshared/* 696*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16203_gshared/* 697*/,
	(methodPointerType)&LinkedList_1__ctor_m4408_gshared/* 698*/,
	(methodPointerType)&LinkedList_1_AddLast_m4417_gshared/* 699*/,
	(methodPointerType)&List_1__ctor_m4418_gshared/* 700*/,
	(methodPointerType)&List_1_GetEnumerator_m4419_gshared/* 701*/,
	(methodPointerType)&Enumerator_get_Current_m4420_gshared/* 702*/,
	(methodPointerType)&Predicate_1__ctor_m4421_gshared/* 703*/,
	(methodPointerType)&Array_Exists_TisTrackableResultData_t642_m4405_gshared/* 704*/,
	(methodPointerType)&Enumerator_MoveNext_m4422_gshared/* 705*/,
	(methodPointerType)&LinkedList_1_get_First_m4423_gshared/* 706*/,
	(methodPointerType)&LinkedListNode_1_get_Value_m4424_gshared/* 707*/,
	(methodPointerType)&Dictionary_2__ctor_m20021_gshared/* 708*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16213_gshared/* 709*/,
	(methodPointerType)&Enumerable_ToList_TisInt32_t127_m4457_gshared/* 710*/,
	(methodPointerType)&Enumerable_ToArray_TisInt32_t127_m4553_gshared/* 711*/,
	(methodPointerType)&LinkedListNode_1_get_Next_m4559_gshared/* 712*/,
	(methodPointerType)&LinkedList_1_Remove_m4560_gshared/* 713*/,
	(methodPointerType)&Dictionary_2__ctor_m4561_gshared/* 714*/,
	(methodPointerType)&Dictionary_2__ctor_m4562_gshared/* 715*/,
	(methodPointerType)&List_1__ctor_m4576_gshared/* 716*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5507_gshared/* 717*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5508_gshared/* 718*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m14955_gshared/* 719*/,
	(methodPointerType)&DOGetter_1__ctor_m5521_gshared/* 720*/,
	(methodPointerType)&DOSetter_1__ctor_m5522_gshared/* 721*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m23611_gshared/* 722*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5531_gshared/* 723*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5532_gshared/* 724*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5533_gshared/* 725*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5542_gshared/* 726*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5549_gshared/* 727*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5550_gshared/* 728*/,
	(methodPointerType)&Nullable_1_get_HasValue_m14898_gshared/* 729*/,
	(methodPointerType)&Nullable_1_get_Value_m14899_gshared/* 730*/,
	(methodPointerType)&Nullable_1_get_HasValue_m14909_gshared/* 731*/,
	(methodPointerType)&Nullable_1_get_Value_m14910_gshared/* 732*/,
	(methodPointerType)&DOTween_ApplyTo_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m5551_gshared/* 733*/,
	(methodPointerType)&DOTween_ApplyTo_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m5552_gshared/* 734*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t454_m5560_gshared/* 735*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m23701_gshared/* 736*/,
	(methodPointerType)&List_1__ctor_m23722_gshared/* 737*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5579_gshared/* 738*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5582_gshared/* 739*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5583_gshared/* 740*/,
	(methodPointerType)&ABSTweenPlugin_3__ctor_m5584_gshared/* 741*/,
	(methodPointerType)&List_1__ctor_m6948_gshared/* 742*/,
	(methodPointerType)&List_1__ctor_m6949_gshared/* 743*/,
	(methodPointerType)&List_1__ctor_m6950_gshared/* 744*/,
	(methodPointerType)&Dictionary_2__ctor_m25583_gshared/* 745*/,
	(methodPointerType)&Dictionary_2__ctor_m26317_gshared/* 746*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7032_gshared/* 747*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7033_gshared/* 748*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m26742_gshared/* 749*/,
	(methodPointerType)&Dictionary_2__ctor_m16524_gshared/* 750*/,
	(methodPointerType)&Dictionary_2__ctor_m27003_gshared/* 751*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t127_m9370_gshared/* 752*/,
	(methodPointerType)&GenericComparer_1__ctor_m14005_gshared/* 753*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14006_gshared/* 754*/,
	(methodPointerType)&GenericComparer_1__ctor_m14007_gshared/* 755*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14008_gshared/* 756*/,
	(methodPointerType)&Nullable_1__ctor_m14009_gshared/* 757*/,
	(methodPointerType)&Nullable_1_get_HasValue_m14010_gshared/* 758*/,
	(methodPointerType)&Nullable_1_get_Value_m14011_gshared/* 759*/,
	(methodPointerType)&GenericComparer_1__ctor_m14012_gshared/* 760*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14013_gshared/* 761*/,
	(methodPointerType)&GenericComparer_1__ctor_m14014_gshared/* 762*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14015_gshared/* 763*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t454_m27748_gshared/* 764*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t454_m27749_gshared/* 765*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t454_m27750_gshared/* 766*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t454_m27751_gshared/* 767*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t454_m27752_gshared/* 768*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t454_m27753_gshared/* 769*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t454_m27754_gshared/* 770*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t454_m27756_gshared/* 771*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t454_m27757_gshared/* 772*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t127_m27759_gshared/* 773*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t127_m27760_gshared/* 774*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t127_m27761_gshared/* 775*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t127_m27762_gshared/* 776*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t127_m27763_gshared/* 777*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t127_m27764_gshared/* 778*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t127_m27765_gshared/* 779*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t127_m27767_gshared/* 780*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t127_m27768_gshared/* 781*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27770_gshared/* 782*/,
	(methodPointerType)&Tweener_DoStartup_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27773_gshared/* 783*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27771_gshared/* 784*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27774_gshared/* 785*/,
	(methodPointerType)&Tweener_DoStartup_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27777_gshared/* 786*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27775_gshared/* 787*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27778_gshared/* 788*/,
	(methodPointerType)&Tweener_DoStartup_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27781_gshared/* 789*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27779_gshared/* 790*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t15_m27783_gshared/* 791*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t15_m27784_gshared/* 792*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t15_m27785_gshared/* 793*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t15_m27786_gshared/* 794*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t15_m27787_gshared/* 795*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t15_m27788_gshared/* 796*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t15_m27789_gshared/* 797*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t15_m27791_gshared/* 798*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t15_m27792_gshared/* 799*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector3_t15_TisObject_t_TisVector3ArrayOptions_t951_m27780_gshared/* 800*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m27776_gshared/* 801*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m27772_gshared/* 802*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t1407_m27799_gshared/* 803*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t1407_m27800_gshared/* 804*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t1407_m27801_gshared/* 805*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t1407_m27802_gshared/* 806*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t1407_m27803_gshared/* 807*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t1407_m27804_gshared/* 808*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t1407_m27805_gshared/* 809*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t1407_m27807_gshared/* 810*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1407_m27808_gshared/* 811*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t231_m27820_gshared/* 812*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t231_m27821_gshared/* 813*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t231_m27822_gshared/* 814*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t231_m27823_gshared/* 815*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t231_m27824_gshared/* 816*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t231_m27825_gshared/* 817*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t231_m27826_gshared/* 818*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t231_m27828_gshared/* 819*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t231_m27829_gshared/* 820*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t231_m27831_gshared/* 821*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t231_m27830_gshared/* 822*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t231_m27832_gshared/* 823*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t231_m27834_gshared/* 824*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t231_TisRaycastResult_t231_m27833_gshared/* 825*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t231_m27835_gshared/* 826*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t231_TisRaycastResult_t231_m27836_gshared/* 827*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t231_m27837_gshared/* 828*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t231_TisRaycastResult_t231_m27838_gshared/* 829*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t231_m27840_gshared/* 830*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t231_m27839_gshared/* 831*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t231_m27841_gshared/* 832*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3197_m27845_gshared/* 833*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3197_m27846_gshared/* 834*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3197_m27847_gshared/* 835*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3197_m27848_gshared/* 836*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3197_m27849_gshared/* 837*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3197_m27850_gshared/* 838*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3197_m27851_gshared/* 839*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3197_m27853_gshared/* 840*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3197_m27854_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t2136_m27856_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t2136_m27857_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t2136_m27858_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t2136_m27859_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t2136_m27860_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t2136_m27861_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t2136_m27862_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t2136_m27864_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t2136_m27865_gshared/* 850*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m27867_gshared/* 851*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m27866_gshared/* 852*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m27868_gshared/* 853*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27870_gshared/* 854*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27869_gshared/* 855*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t1996_m27872_gshared/* 856*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1996_m27873_gshared/* 857*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1996_m27874_gshared/* 858*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1996_m27875_gshared/* 859*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1996_m27876_gshared/* 860*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t1996_m27877_gshared/* 861*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t1996_m27878_gshared/* 862*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t1996_m27880_gshared/* 863*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1996_m27881_gshared/* 864*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27882_gshared/* 865*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3197_m27884_gshared/* 866*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3197_TisObject_t_m27883_gshared/* 867*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3197_TisKeyValuePair_2_t3197_m27885_gshared/* 868*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t432_m27887_gshared/* 869*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t432_m27888_gshared/* 870*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t432_m27889_gshared/* 871*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t432_m27890_gshared/* 872*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t432_m27891_gshared/* 873*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t432_m27892_gshared/* 874*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t432_m27893_gshared/* 875*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t432_m27895_gshared/* 876*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t432_m27896_gshared/* 877*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t94_m27898_gshared/* 878*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t94_m27899_gshared/* 879*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t94_m27900_gshared/* 880*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t94_m27901_gshared/* 881*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t94_m27902_gshared/* 882*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t94_m27903_gshared/* 883*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t94_m27904_gshared/* 884*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t94_m27906_gshared/* 885*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t94_m27907_gshared/* 886*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t94_m27908_gshared/* 887*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t94_m27909_gshared/* 888*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t94_m27910_gshared/* 889*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t90_m27911_gshared/* 890*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3228_m27913_gshared/* 891*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3228_m27914_gshared/* 892*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3228_m27915_gshared/* 893*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3228_m27916_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3228_m27917_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3228_m27918_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3228_m27919_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3228_m27921_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3228_m27922_gshared/* 899*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m27924_gshared/* 900*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m27923_gshared/* 901*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m27926_gshared/* 902*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m27925_gshared/* 903*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m27927_gshared/* 904*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27928_gshared/* 905*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3228_m27930_gshared/* 906*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3228_TisObject_t_m27929_gshared/* 907*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3228_TisKeyValuePair_2_t3228_m27931_gshared/* 908*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3254_m27933_gshared/* 909*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3254_m27934_gshared/* 910*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3254_m27935_gshared/* 911*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3254_m27936_gshared/* 912*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3254_m27937_gshared/* 913*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3254_m27938_gshared/* 914*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3254_m27939_gshared/* 915*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3254_m27941_gshared/* 916*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3254_m27942_gshared/* 917*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m27945_gshared/* 918*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3254_m27947_gshared/* 919*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisObject_t_m27946_gshared/* 920*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisKeyValuePair_2_t3254_m27948_gshared/* 921*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t313_m27950_gshared/* 922*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t313_m27949_gshared/* 923*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t313_m27951_gshared/* 924*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t313_m27953_gshared/* 925*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t313_TisUIVertex_t313_m27952_gshared/* 926*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t313_m27954_gshared/* 927*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t313_TisUIVertex_t313_m27955_gshared/* 928*/,
	(methodPointerType)&Array_compare_TisUIVertex_t313_m27956_gshared/* 929*/,
	(methodPointerType)&Array_swap_TisUIVertex_t313_TisUIVertex_t313_m27957_gshared/* 930*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t313_m27959_gshared/* 931*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t313_m27958_gshared/* 932*/,
	(methodPointerType)&Array_swap_TisUIVertex_t313_m27960_gshared/* 933*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t10_m27962_gshared/* 934*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t10_m27963_gshared/* 935*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t10_m27964_gshared/* 936*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t10_m27965_gshared/* 937*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t10_m27966_gshared/* 938*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t10_m27967_gshared/* 939*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t10_m27968_gshared/* 940*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t10_m27970_gshared/* 941*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t10_m27971_gshared/* 942*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t458_m27973_gshared/* 943*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t458_m27974_gshared/* 944*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t458_m27975_gshared/* 945*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t458_m27976_gshared/* 946*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t458_m27977_gshared/* 947*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t458_m27978_gshared/* 948*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t458_m27979_gshared/* 949*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t458_m27981_gshared/* 950*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t458_m27982_gshared/* 951*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t460_m27984_gshared/* 952*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t460_m27985_gshared/* 953*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t460_m27986_gshared/* 954*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t460_m27987_gshared/* 955*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t460_m27988_gshared/* 956*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t460_m27989_gshared/* 957*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t460_m27990_gshared/* 958*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t460_m27992_gshared/* 959*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t460_m27993_gshared/* 960*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t151_m27994_gshared/* 961*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t10_m27995_gshared/* 962*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisByte_t449_m27996_gshared/* 963*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t151_m28001_gshared/* 964*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t151_m28002_gshared/* 965*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t151_m28003_gshared/* 966*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t151_m28004_gshared/* 967*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t151_m28005_gshared/* 968*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t151_m28006_gshared/* 969*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t151_m28007_gshared/* 970*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t151_m28009_gshared/* 971*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t151_m28010_gshared/* 972*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisEyewearCalibrationReading_t586_m28012_gshared/* 973*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisEyewearCalibrationReading_t586_m28013_gshared/* 974*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisEyewearCalibrationReading_t586_m28014_gshared/* 975*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisEyewearCalibrationReading_t586_m28015_gshared/* 976*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisEyewearCalibrationReading_t586_m28016_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisEyewearCalibrationReading_t586_m28017_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__Insert_TisEyewearCalibrationReading_t586_m28018_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisEyewearCalibrationReading_t586_m28020_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisEyewearCalibrationReading_t586_m28021_gshared/* 981*/,
	(methodPointerType)&Array_Resize_TisInt32_t127_m28024_gshared/* 982*/,
	(methodPointerType)&Array_Resize_TisInt32_t127_m28023_gshared/* 983*/,
	(methodPointerType)&Array_IndexOf_TisInt32_t127_m28025_gshared/* 984*/,
	(methodPointerType)&Array_Sort_TisInt32_t127_m28027_gshared/* 985*/,
	(methodPointerType)&Array_Sort_TisInt32_t127_TisInt32_t127_m28026_gshared/* 986*/,
	(methodPointerType)&Array_get_swapper_TisInt32_t127_m28028_gshared/* 987*/,
	(methodPointerType)&Array_qsort_TisInt32_t127_TisInt32_t127_m28029_gshared/* 988*/,
	(methodPointerType)&Array_compare_TisInt32_t127_m28030_gshared/* 989*/,
	(methodPointerType)&Array_swap_TisInt32_t127_TisInt32_t127_m28031_gshared/* 990*/,
	(methodPointerType)&Array_Sort_TisInt32_t127_m28033_gshared/* 991*/,
	(methodPointerType)&Array_qsort_TisInt32_t127_m28032_gshared/* 992*/,
	(methodPointerType)&Array_swap_TisInt32_t127_m28034_gshared/* 993*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t449_m28036_gshared/* 994*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t449_m28037_gshared/* 995*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t449_m28038_gshared/* 996*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t449_m28039_gshared/* 997*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t449_m28040_gshared/* 998*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t449_m28041_gshared/* 999*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t449_m28042_gshared/* 1000*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t449_m28044_gshared/* 1001*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t449_m28045_gshared/* 1002*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor32_t421_m28047_gshared/* 1003*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor32_t421_m28048_gshared/* 1004*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor32_t421_m28049_gshared/* 1005*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor32_t421_m28050_gshared/* 1006*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor32_t421_m28051_gshared/* 1007*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor32_t421_m28052_gshared/* 1008*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor32_t421_m28053_gshared/* 1009*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor32_t421_m28055_gshared/* 1010*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor32_t421_m28056_gshared/* 1011*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor_t90_m28058_gshared/* 1012*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor_t90_m28059_gshared/* 1013*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor_t90_m28060_gshared/* 1014*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor_t90_m28061_gshared/* 1015*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor_t90_m28062_gshared/* 1016*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor_t90_m28063_gshared/* 1017*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor_t90_m28064_gshared/* 1018*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor_t90_m28066_gshared/* 1019*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t90_m28067_gshared/* 1020*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTrackableResultData_t642_m28072_gshared/* 1021*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTrackableResultData_t642_m28073_gshared/* 1022*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTrackableResultData_t642_m28074_gshared/* 1023*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t642_m28075_gshared/* 1024*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTrackableResultData_t642_m28076_gshared/* 1025*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTrackableResultData_t642_m28077_gshared/* 1026*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTrackableResultData_t642_m28078_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTrackableResultData_t642_m28080_gshared/* 1028*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t642_m28081_gshared/* 1029*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordData_t647_m28083_gshared/* 1030*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordData_t647_m28084_gshared/* 1031*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordData_t647_m28085_gshared/* 1032*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordData_t647_m28086_gshared/* 1033*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordData_t647_m28087_gshared/* 1034*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordData_t647_m28088_gshared/* 1035*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordData_t647_m28089_gshared/* 1036*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordData_t647_m28091_gshared/* 1037*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t647_m28092_gshared/* 1038*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWordResultData_t646_m28094_gshared/* 1039*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWordResultData_t646_m28095_gshared/* 1040*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWordResultData_t646_m28096_gshared/* 1041*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWordResultData_t646_m28097_gshared/* 1042*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWordResultData_t646_m28098_gshared/* 1043*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWordResultData_t646_m28099_gshared/* 1044*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWordResultData_t646_m28100_gshared/* 1045*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWordResultData_t646_m28102_gshared/* 1046*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t646_m28103_gshared/* 1047*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t650_m28105_gshared/* 1048*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t650_m28106_gshared/* 1049*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t650_m28107_gshared/* 1050*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t650_m28108_gshared/* 1051*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t650_m28109_gshared/* 1052*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t650_m28110_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSmartTerrainRevisionData_t650_m28111_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t650_m28113_gshared/* 1055*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t650_m28114_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSurfaceData_t651_m28116_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSurfaceData_t651_m28117_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSurfaceData_t651_m28118_gshared/* 1059*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t651_m28119_gshared/* 1060*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSurfaceData_t651_m28120_gshared/* 1061*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSurfaceData_t651_m28121_gshared/* 1062*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSurfaceData_t651_m28122_gshared/* 1063*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSurfaceData_t651_m28124_gshared/* 1064*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t651_m28125_gshared/* 1065*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisPropData_t652_m28127_gshared/* 1066*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisPropData_t652_m28128_gshared/* 1067*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisPropData_t652_m28129_gshared/* 1068*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisPropData_t652_m28130_gshared/* 1069*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisPropData_t652_m28131_gshared/* 1070*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisPropData_t652_m28132_gshared/* 1071*/,
	(methodPointerType)&Array_InternalArray__Insert_TisPropData_t652_m28133_gshared/* 1072*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisPropData_t652_m28135_gshared/* 1073*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t652_m28136_gshared/* 1074*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3462_m28139_gshared/* 1075*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3462_m28140_gshared/* 1076*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3462_m28141_gshared/* 1077*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3462_m28142_gshared/* 1078*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3462_m28143_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3462_m28144_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3462_m28145_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3462_m28147_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3462_m28148_gshared/* 1083*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28150_gshared/* 1084*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28149_gshared/* 1085*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt16_t454_m28152_gshared/* 1086*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t454_TisObject_t_m28151_gshared/* 1087*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt16_t454_TisUInt16_t454_m28153_gshared/* 1088*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28154_gshared/* 1089*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3462_m28156_gshared/* 1090*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3462_TisObject_t_m28155_gshared/* 1091*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3462_TisKeyValuePair_2_t3462_m28157_gshared/* 1092*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRectangleData_t596_m28159_gshared/* 1093*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRectangleData_t596_m28160_gshared/* 1094*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRectangleData_t596_m28161_gshared/* 1095*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRectangleData_t596_m28162_gshared/* 1096*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRectangleData_t596_m28163_gshared/* 1097*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRectangleData_t596_m28164_gshared/* 1098*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRectangleData_t596_m28165_gshared/* 1099*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRectangleData_t596_m28167_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t596_m28168_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3550_m28170_gshared/* 1102*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3550_m28171_gshared/* 1103*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3550_m28172_gshared/* 1104*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3550_m28173_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3550_m28174_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3550_m28175_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3550_m28176_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3550_m28178_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3550_m28179_gshared/* 1110*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28181_gshared/* 1111*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28180_gshared/* 1112*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28182_gshared/* 1113*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisTrackableResultData_t642_m28184_gshared/* 1114*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t642_TisObject_t_m28183_gshared/* 1115*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisTrackableResultData_t642_TisTrackableResultData_t642_m28185_gshared/* 1116*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28186_gshared/* 1117*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3550_m28188_gshared/* 1118*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3550_TisObject_t_m28187_gshared/* 1119*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3550_TisKeyValuePair_2_t3550_m28189_gshared/* 1120*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3565_m28191_gshared/* 1121*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3565_m28192_gshared/* 1122*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3565_m28193_gshared/* 1123*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3565_m28194_gshared/* 1124*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3565_m28195_gshared/* 1125*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3565_m28196_gshared/* 1126*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3565_m28197_gshared/* 1127*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3565_m28199_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3565_m28200_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVirtualButtonData_t643_m28202_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVirtualButtonData_t643_m28203_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t643_m28204_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t643_m28205_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t643_m28206_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVirtualButtonData_t643_m28207_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVirtualButtonData_t643_m28208_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVirtualButtonData_t643_m28210_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t643_m28211_gshared/* 1138*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28213_gshared/* 1139*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28212_gshared/* 1140*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28214_gshared/* 1141*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisVirtualButtonData_t643_m28216_gshared/* 1142*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t643_TisObject_t_m28215_gshared/* 1143*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisVirtualButtonData_t643_TisVirtualButtonData_t643_m28217_gshared/* 1144*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28218_gshared/* 1145*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3565_m28220_gshared/* 1146*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3565_TisObject_t_m28219_gshared/* 1147*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3565_TisKeyValuePair_2_t3565_m28221_gshared/* 1148*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTargetSearchResult_t720_m28223_gshared/* 1149*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTargetSearchResult_t720_m28224_gshared/* 1150*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t720_m28225_gshared/* 1151*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t720_m28226_gshared/* 1152*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t720_m28227_gshared/* 1153*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTargetSearchResult_t720_m28228_gshared/* 1154*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTargetSearchResult_t720_m28229_gshared/* 1155*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTargetSearchResult_t720_m28231_gshared/* 1156*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t720_m28232_gshared/* 1157*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t720_m28234_gshared/* 1158*/,
	(methodPointerType)&Array_Resize_TisTargetSearchResult_t720_m28233_gshared/* 1159*/,
	(methodPointerType)&Array_IndexOf_TisTargetSearchResult_t720_m28235_gshared/* 1160*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t720_m28237_gshared/* 1161*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28236_gshared/* 1162*/,
	(methodPointerType)&Array_get_swapper_TisTargetSearchResult_t720_m28238_gshared/* 1163*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28239_gshared/* 1164*/,
	(methodPointerType)&Array_compare_TisTargetSearchResult_t720_m28240_gshared/* 1165*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t720_TisTargetSearchResult_t720_m28241_gshared/* 1166*/,
	(methodPointerType)&Array_Sort_TisTargetSearchResult_t720_m28243_gshared/* 1167*/,
	(methodPointerType)&Array_qsort_TisTargetSearchResult_t720_m28242_gshared/* 1168*/,
	(methodPointerType)&Array_swap_TisTargetSearchResult_t720_m28244_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisWebCamDevice_t879_m28249_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisWebCamDevice_t879_m28250_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisWebCamDevice_t879_m28251_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t879_m28252_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisWebCamDevice_t879_m28253_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisWebCamDevice_t879_m28254_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__Insert_TisWebCamDevice_t879_m28255_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisWebCamDevice_t879_m28257_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t879_m28258_gshared/* 1178*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3604_m28260_gshared/* 1179*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3604_m28261_gshared/* 1180*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3604_m28262_gshared/* 1181*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3604_m28263_gshared/* 1182*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3604_m28264_gshared/* 1183*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3604_m28265_gshared/* 1184*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3604_m28266_gshared/* 1185*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3604_m28268_gshared/* 1186*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3604_m28269_gshared/* 1187*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisProfileData_t734_m28271_gshared/* 1188*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisProfileData_t734_m28272_gshared/* 1189*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisProfileData_t734_m28273_gshared/* 1190*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisProfileData_t734_m28274_gshared/* 1191*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisProfileData_t734_m28275_gshared/* 1192*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisProfileData_t734_m28276_gshared/* 1193*/,
	(methodPointerType)&Array_InternalArray__Insert_TisProfileData_t734_m28277_gshared/* 1194*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisProfileData_t734_m28279_gshared/* 1195*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t734_m28280_gshared/* 1196*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28282_gshared/* 1197*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28281_gshared/* 1198*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t734_m28284_gshared/* 1199*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t734_TisObject_t_m28283_gshared/* 1200*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisProfileData_t734_TisProfileData_t734_m28285_gshared/* 1201*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28286_gshared/* 1202*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3604_m28288_gshared/* 1203*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3604_TisObject_t_m28287_gshared/* 1204*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3604_TisKeyValuePair_2_t3604_m28289_gshared/* 1205*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t3653_m28291_gshared/* 1206*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t3653_m28292_gshared/* 1207*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t3653_m28293_gshared/* 1208*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t3653_m28294_gshared/* 1209*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t3653_m28295_gshared/* 1210*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t3653_m28296_gshared/* 1211*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t3653_m28297_gshared/* 1212*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t3653_m28299_gshared/* 1213*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3653_m28300_gshared/* 1214*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28301_gshared/* 1215*/,
	(methodPointerType)&Tweener_DoStartup_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28304_gshared/* 1216*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28302_gshared/* 1217*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisUInt32_t1075_TisUInt32_t1075_TisNoOptions_t933_m28303_gshared/* 1218*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28305_gshared/* 1219*/,
	(methodPointerType)&Tweener_DoStartup_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28308_gshared/* 1220*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28306_gshared/* 1221*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector2_t10_TisVector2_t10_TisVectorOptions_t1002_m28307_gshared/* 1222*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t933_m28309_gshared/* 1223*/,
	(methodPointerType)&Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t933_m28312_gshared/* 1224*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisNoOptions_t933_m28310_gshared/* 1225*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisNoOptions_t933_m28311_gshared/* 1226*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28313_gshared/* 1227*/,
	(methodPointerType)&Tweener_DoStartup_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28316_gshared/* 1228*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28314_gshared/* 1229*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisColor2_t1000_TisColor2_t1000_TisColorOptions_t1011_m28315_gshared/* 1230*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28317_gshared/* 1231*/,
	(methodPointerType)&Tweener_DoStartup_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28320_gshared/* 1232*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28318_gshared/* 1233*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisRect_t124_TisRect_t124_TisRectOptions_t1004_m28319_gshared/* 1234*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28321_gshared/* 1235*/,
	(methodPointerType)&Tweener_DoStartup_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28324_gshared/* 1236*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28322_gshared/* 1237*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisUInt64_t1091_TisUInt64_t1091_TisNoOptions_t933_m28323_gshared/* 1238*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28325_gshared/* 1239*/,
	(methodPointerType)&Tweener_DoStartup_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28328_gshared/* 1240*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28326_gshared/* 1241*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisInt32_t127_TisInt32_t127_TisNoOptions_t933_m28327_gshared/* 1242*/,
	(methodPointerType)&TweenManager_GetTweener_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28329_gshared/* 1243*/,
	(methodPointerType)&Tweener_Setup_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28330_gshared/* 1244*/,
	(methodPointerType)&PluginsManager_GetDefaultPlugin_TisVector3_t15_TisVector3_t15_TisVectorOptions_t1002_m28331_gshared/* 1245*/,
	(methodPointerType)&TweenManager_GetTweener_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28332_gshared/* 1246*/,
	(methodPointerType)&Tweener_Setup_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28333_gshared/* 1247*/,
	(methodPointerType)&PluginsManager_GetDefaultPlugin_TisQuaternion_t13_TisVector3_t15_TisQuaternionOptions_t971_m28334_gshared/* 1248*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1003_m28335_gshared/* 1249*/,
	(methodPointerType)&Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1003_m28338_gshared/* 1250*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisObject_t_TisObject_t_TisStringOptions_t1003_m28336_gshared/* 1251*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisObject_t_TisObject_t_TisStringOptions_t1003_m28337_gshared/* 1252*/,
	(methodPointerType)&Array_Resize_TisUInt16_t454_m28340_gshared/* 1253*/,
	(methodPointerType)&Array_Resize_TisUInt16_t454_m28339_gshared/* 1254*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t454_m28341_gshared/* 1255*/,
	(methodPointerType)&Array_Sort_TisUInt16_t454_m28343_gshared/* 1256*/,
	(methodPointerType)&Array_Sort_TisUInt16_t454_TisUInt16_t454_m28342_gshared/* 1257*/,
	(methodPointerType)&Array_get_swapper_TisUInt16_t454_m28344_gshared/* 1258*/,
	(methodPointerType)&Array_qsort_TisUInt16_t454_TisUInt16_t454_m28345_gshared/* 1259*/,
	(methodPointerType)&Array_compare_TisUInt16_t454_m28346_gshared/* 1260*/,
	(methodPointerType)&Array_swap_TisUInt16_t454_TisUInt16_t454_m28347_gshared/* 1261*/,
	(methodPointerType)&Array_Sort_TisUInt16_t454_m28349_gshared/* 1262*/,
	(methodPointerType)&Array_qsort_TisUInt16_t454_m28348_gshared/* 1263*/,
	(methodPointerType)&Array_swap_TisUInt16_t454_m28350_gshared/* 1264*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28351_gshared/* 1265*/,
	(methodPointerType)&Tweener_DoStartup_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28354_gshared/* 1266*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28352_gshared/* 1267*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisVector4_t413_TisVector4_t413_TisVectorOptions_t1002_m28353_gshared/* 1268*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28355_gshared/* 1269*/,
	(methodPointerType)&Tweener_DoStartup_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28358_gshared/* 1270*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28356_gshared/* 1271*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisColor_t90_TisColor_t90_TisColorOptions_t1011_m28357_gshared/* 1272*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28359_gshared/* 1273*/,
	(methodPointerType)&Tweener_DoStartup_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28362_gshared/* 1274*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28360_gshared/* 1275*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisSingle_t151_TisSingle_t151_TisFloatOptions_t996_m28361_gshared/* 1276*/,
	(methodPointerType)&Tweener_DoUpdateDelay_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28363_gshared/* 1277*/,
	(methodPointerType)&Tweener_DoStartup_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28366_gshared/* 1278*/,
	(methodPointerType)&Tweener_DOStartupSpecials_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28364_gshared/* 1279*/,
	(methodPointerType)&Tweener_DOStartupDurationBased_TisInt64_t1092_TisInt64_t1092_TisNoOptions_t933_m28365_gshared/* 1280*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t1305_m28369_gshared/* 1281*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t1305_m28370_gshared/* 1282*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1305_m28371_gshared/* 1283*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1305_m28372_gshared/* 1284*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1305_m28373_gshared/* 1285*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t1305_m28374_gshared/* 1286*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t1305_m28375_gshared/* 1287*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t1305_m28377_gshared/* 1288*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1305_m28378_gshared/* 1289*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t1306_m28380_gshared/* 1290*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t1306_m28381_gshared/* 1291*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t1306_m28382_gshared/* 1292*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1306_m28383_gshared/* 1293*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t1306_m28384_gshared/* 1294*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t1306_m28385_gshared/* 1295*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t1306_m28386_gshared/* 1296*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t1306_m28388_gshared/* 1297*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1306_m28389_gshared/* 1298*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m28391_gshared/* 1299*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m28392_gshared/* 1300*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m28393_gshared/* 1301*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m28394_gshared/* 1302*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m28395_gshared/* 1303*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m28396_gshared/* 1304*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m28397_gshared/* 1305*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m28399_gshared/* 1306*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m28400_gshared/* 1307*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t1236_m28402_gshared/* 1308*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t1236_m28403_gshared/* 1309*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t1236_m28404_gshared/* 1310*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1236_m28405_gshared/* 1311*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t1236_m28406_gshared/* 1312*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t1236_m28407_gshared/* 1313*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t1236_m28408_gshared/* 1314*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t1236_m28410_gshared/* 1315*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1236_m28411_gshared/* 1316*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t460_m28413_gshared/* 1317*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t460_m28412_gshared/* 1318*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t460_m28414_gshared/* 1319*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t460_m28416_gshared/* 1320*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t460_TisUICharInfo_t460_m28415_gshared/* 1321*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t460_m28417_gshared/* 1322*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t460_TisUICharInfo_t460_m28418_gshared/* 1323*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t460_m28419_gshared/* 1324*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t460_TisUICharInfo_t460_m28420_gshared/* 1325*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t460_m28422_gshared/* 1326*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t460_m28421_gshared/* 1327*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t460_m28423_gshared/* 1328*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t458_m28425_gshared/* 1329*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t458_m28424_gshared/* 1330*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t458_m28426_gshared/* 1331*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t458_m28428_gshared/* 1332*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t458_TisUILineInfo_t458_m28427_gshared/* 1333*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t458_m28429_gshared/* 1334*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t458_TisUILineInfo_t458_m28430_gshared/* 1335*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t458_m28431_gshared/* 1336*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t458_TisUILineInfo_t458_m28432_gshared/* 1337*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t458_m28434_gshared/* 1338*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t458_m28433_gshared/* 1339*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t458_m28435_gshared/* 1340*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3777_m28437_gshared/* 1341*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3777_m28438_gshared/* 1342*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3777_m28439_gshared/* 1343*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3777_m28440_gshared/* 1344*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3777_m28441_gshared/* 1345*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3777_m28442_gshared/* 1346*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3777_m28443_gshared/* 1347*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3777_m28445_gshared/* 1348*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3777_m28446_gshared/* 1349*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t1092_m28448_gshared/* 1350*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t1092_m28449_gshared/* 1351*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t1092_m28450_gshared/* 1352*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t1092_m28451_gshared/* 1353*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t1092_m28452_gshared/* 1354*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t1092_m28453_gshared/* 1355*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t1092_m28454_gshared/* 1356*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t1092_m28456_gshared/* 1357*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1092_m28457_gshared/* 1358*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28459_gshared/* 1359*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28458_gshared/* 1360*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1092_m28461_gshared/* 1361*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1092_TisObject_t_m28460_gshared/* 1362*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1092_TisInt64_t1092_m28462_gshared/* 1363*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28463_gshared/* 1364*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3777_m28465_gshared/* 1365*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3777_TisObject_t_m28464_gshared/* 1366*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3777_TisKeyValuePair_2_t3777_m28466_gshared/* 1367*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3815_m28468_gshared/* 1368*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3815_m28469_gshared/* 1369*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3815_m28470_gshared/* 1370*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3815_m28471_gshared/* 1371*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3815_m28472_gshared/* 1372*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3815_m28473_gshared/* 1373*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3815_m28474_gshared/* 1374*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3815_m28476_gshared/* 1375*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3815_m28477_gshared/* 1376*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t1091_m28479_gshared/* 1377*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t1091_m28480_gshared/* 1378*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t1091_m28481_gshared/* 1379*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t1091_m28482_gshared/* 1380*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t1091_m28483_gshared/* 1381*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t1091_m28484_gshared/* 1382*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t1091_m28485_gshared/* 1383*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t1091_m28487_gshared/* 1384*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1091_m28488_gshared/* 1385*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1091_m28490_gshared/* 1386*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1091_TisObject_t_m28489_gshared/* 1387*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1091_TisUInt64_t1091_m28491_gshared/* 1388*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28493_gshared/* 1389*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28492_gshared/* 1390*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28494_gshared/* 1391*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3815_m28496_gshared/* 1392*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3815_TisObject_t_m28495_gshared/* 1393*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3815_TisKeyValuePair_2_t3815_m28497_gshared/* 1394*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3836_m28499_gshared/* 1395*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3836_m28500_gshared/* 1396*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3836_m28501_gshared/* 1397*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3836_m28502_gshared/* 1398*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3836_m28503_gshared/* 1399*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3836_m28504_gshared/* 1400*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3836_m28505_gshared/* 1401*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3836_m28507_gshared/* 1402*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3836_m28508_gshared/* 1403*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28510_gshared/* 1404*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28509_gshared/* 1405*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3254_m28512_gshared/* 1406*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisObject_t_m28511_gshared/* 1407*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3254_TisKeyValuePair_2_t3254_m28513_gshared/* 1408*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28514_gshared/* 1409*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3836_m28516_gshared/* 1410*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3836_TisObject_t_m28515_gshared/* 1411*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3836_TisKeyValuePair_2_t3836_m28517_gshared/* 1412*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t2260_m28519_gshared/* 1413*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t2260_m28520_gshared/* 1414*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t2260_m28521_gshared/* 1415*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2260_m28522_gshared/* 1416*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t2260_m28523_gshared/* 1417*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t2260_m28524_gshared/* 1418*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t2260_m28525_gshared/* 1419*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t2260_m28527_gshared/* 1420*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2260_m28528_gshared/* 1421*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t1323_m28530_gshared/* 1422*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t1323_m28531_gshared/* 1423*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t1323_m28532_gshared/* 1424*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1323_m28533_gshared/* 1425*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t1323_m28534_gshared/* 1426*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t1323_m28535_gshared/* 1427*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t1323_m28536_gshared/* 1428*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t1323_m28538_gshared/* 1429*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1323_m28539_gshared/* 1430*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t127_m28540_gshared/* 1431*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t1075_m28542_gshared/* 1432*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t1075_m28543_gshared/* 1433*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t1075_m28544_gshared/* 1434*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t1075_m28545_gshared/* 1435*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t1075_m28546_gshared/* 1436*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t1075_m28547_gshared/* 1437*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t1075_m28548_gshared/* 1438*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t1075_m28550_gshared/* 1439*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1075_m28551_gshared/* 1440*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3932_m28553_gshared/* 1441*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3932_m28554_gshared/* 1442*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3932_m28555_gshared/* 1443*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3932_m28556_gshared/* 1444*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3932_m28557_gshared/* 1445*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3932_m28558_gshared/* 1446*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3932_m28559_gshared/* 1447*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3932_m28561_gshared/* 1448*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3932_m28562_gshared/* 1449*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m28564_gshared/* 1450*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m28563_gshared/* 1451*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t449_m28566_gshared/* 1452*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t449_TisObject_t_m28565_gshared/* 1453*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t449_TisByte_t449_m28567_gshared/* 1454*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28568_gshared/* 1455*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3932_m28570_gshared/* 1456*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3932_TisObject_t_m28569_gshared/* 1457*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3932_TisKeyValuePair_2_t3932_m28571_gshared/* 1458*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t1902_m28573_gshared/* 1459*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1902_m28574_gshared/* 1460*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1902_m28575_gshared/* 1461*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1902_m28576_gshared/* 1462*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1902_m28577_gshared/* 1463*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t1902_m28578_gshared/* 1464*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t1902_m28579_gshared/* 1465*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t1902_m28581_gshared/* 1466*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1902_m28582_gshared/* 1467*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3954_m28584_gshared/* 1468*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3954_m28585_gshared/* 1469*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3954_m28586_gshared/* 1470*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3954_m28587_gshared/* 1471*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3954_m28588_gshared/* 1472*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3954_m28589_gshared/* 1473*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3954_m28590_gshared/* 1474*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3954_m28592_gshared/* 1475*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3954_m28593_gshared/* 1476*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t127_m28595_gshared/* 1477*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisObject_t_m28594_gshared/* 1478*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t127_TisInt32_t127_m28596_gshared/* 1479*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1996_TisDictionaryEntry_t1996_m28597_gshared/* 1480*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3954_m28599_gshared/* 1481*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3954_TisObject_t_m28598_gshared/* 1482*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3954_TisKeyValuePair_2_t3954_m28600_gshared/* 1483*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t127_m28601_gshared/* 1484*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t1948_m28603_gshared/* 1485*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t1948_m28604_gshared/* 1486*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t1948_m28605_gshared/* 1487*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t1948_m28606_gshared/* 1488*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t1948_m28607_gshared/* 1489*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t1948_m28608_gshared/* 1490*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t1948_m28609_gshared/* 1491*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t1948_m28611_gshared/* 1492*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1948_m28612_gshared/* 1493*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t1984_m28614_gshared/* 1494*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t1984_m28615_gshared/* 1495*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t1984_m28616_gshared/* 1496*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1984_m28617_gshared/* 1497*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t1984_m28618_gshared/* 1498*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t1984_m28619_gshared/* 1499*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t1984_m28620_gshared/* 1500*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t1984_m28622_gshared/* 1501*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1984_m28623_gshared/* 1502*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t534_m28625_gshared/* 1503*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t534_m28626_gshared/* 1504*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t534_m28627_gshared/* 1505*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t534_m28628_gshared/* 1506*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t534_m28629_gshared/* 1507*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t534_m28630_gshared/* 1508*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t534_m28631_gshared/* 1509*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t534_m28633_gshared/* 1510*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t534_m28634_gshared/* 1511*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t170_m28636_gshared/* 1512*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t170_m28637_gshared/* 1513*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t170_m28638_gshared/* 1514*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t170_m28639_gshared/* 1515*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t170_m28640_gshared/* 1516*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t170_m28641_gshared/* 1517*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t170_m28642_gshared/* 1518*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t170_m28644_gshared/* 1519*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t170_m28645_gshared/* 1520*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t2069_m28675_gshared/* 1521*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t2069_m28676_gshared/* 1522*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t2069_m28677_gshared/* 1523*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t2069_m28678_gshared/* 1524*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t2069_m28679_gshared/* 1525*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t2069_m28680_gshared/* 1526*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t2069_m28681_gshared/* 1527*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t2069_m28683_gshared/* 1528*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2069_m28684_gshared/* 1529*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2146_m28686_gshared/* 1530*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2146_m28687_gshared/* 1531*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2146_m28688_gshared/* 1532*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2146_m28689_gshared/* 1533*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2146_m28690_gshared/* 1534*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2146_m28691_gshared/* 1535*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2146_m28692_gshared/* 1536*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2146_m28694_gshared/* 1537*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2146_m28695_gshared/* 1538*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2153_m28697_gshared/* 1539*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2153_m28698_gshared/* 1540*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2153_m28699_gshared/* 1541*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2153_m28700_gshared/* 1542*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2153_m28701_gshared/* 1543*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2153_m28702_gshared/* 1544*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2153_m28703_gshared/* 1545*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2153_m28705_gshared/* 1546*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2153_m28706_gshared/* 1547*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t111_m28710_gshared/* 1548*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t111_m28711_gshared/* 1549*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t111_m28712_gshared/* 1550*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t111_m28713_gshared/* 1551*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t111_m28714_gshared/* 1552*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t111_m28715_gshared/* 1553*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t111_m28716_gshared/* 1554*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t111_m28718_gshared/* 1555*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t111_m28719_gshared/* 1556*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t1059_m28721_gshared/* 1557*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t1059_m28722_gshared/* 1558*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t1059_m28723_gshared/* 1559*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t1059_m28724_gshared/* 1560*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t1059_m28725_gshared/* 1561*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t1059_m28726_gshared/* 1562*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t1059_m28727_gshared/* 1563*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t1059_m28729_gshared/* 1564*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1059_m28730_gshared/* 1565*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t112_m28732_gshared/* 1566*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t112_m28733_gshared/* 1567*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t112_m28734_gshared/* 1568*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t112_m28735_gshared/* 1569*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t112_m28736_gshared/* 1570*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t112_m28737_gshared/* 1571*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t112_m28738_gshared/* 1572*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t112_m28740_gshared/* 1573*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t112_m28741_gshared/* 1574*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14873_gshared/* 1575*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14875_gshared/* 1576*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14877_gshared/* 1577*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14879_gshared/* 1578*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14881_gshared/* 1579*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14882_gshared/* 1580*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14883_gshared/* 1581*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14884_gshared/* 1582*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14885_gshared/* 1583*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14886_gshared/* 1584*/,
	(methodPointerType)&Nullable_1_Equals_m14901_gshared/* 1585*/,
	(methodPointerType)&Nullable_1_Equals_m14903_gshared/* 1586*/,
	(methodPointerType)&Nullable_1_GetHashCode_m14905_gshared/* 1587*/,
	(methodPointerType)&Nullable_1_ToString_m14907_gshared/* 1588*/,
	(methodPointerType)&Nullable_1_Equals_m14912_gshared/* 1589*/,
	(methodPointerType)&Nullable_1_Equals_m14914_gshared/* 1590*/,
	(methodPointerType)&Nullable_1_GetHashCode_m14916_gshared/* 1591*/,
	(methodPointerType)&Nullable_1_ToString_m14918_gshared/* 1592*/,
	(methodPointerType)&DOGetter_1_Invoke_m14919_gshared/* 1593*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m14920_gshared/* 1594*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m14921_gshared/* 1595*/,
	(methodPointerType)&DOSetter_1_Invoke_m14922_gshared/* 1596*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m14923_gshared/* 1597*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m14924_gshared/* 1598*/,
	(methodPointerType)&TweenerCore_3__ctor_m14925_gshared/* 1599*/,
	(methodPointerType)&TweenerCore_3_Reset_m14926_gshared/* 1600*/,
	(methodPointerType)&TweenerCore_3_Validate_m14927_gshared/* 1601*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m14928_gshared/* 1602*/,
	(methodPointerType)&TweenerCore_3_Startup_m14929_gshared/* 1603*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m14930_gshared/* 1604*/,
	(methodPointerType)&TweenerCore_3__ctor_m14931_gshared/* 1605*/,
	(methodPointerType)&TweenerCore_3_Reset_m14932_gshared/* 1606*/,
	(methodPointerType)&TweenerCore_3_Validate_m14933_gshared/* 1607*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m14934_gshared/* 1608*/,
	(methodPointerType)&TweenerCore_3_Startup_m14935_gshared/* 1609*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m14936_gshared/* 1610*/,
	(methodPointerType)&DOGetter_1_Invoke_m14937_gshared/* 1611*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m14938_gshared/* 1612*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m14939_gshared/* 1613*/,
	(methodPointerType)&DOSetter_1_Invoke_m14940_gshared/* 1614*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m14941_gshared/* 1615*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m14942_gshared/* 1616*/,
	(methodPointerType)&TweenerCore_3__ctor_m14944_gshared/* 1617*/,
	(methodPointerType)&TweenerCore_3_Reset_m14946_gshared/* 1618*/,
	(methodPointerType)&TweenerCore_3_Validate_m14948_gshared/* 1619*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m14950_gshared/* 1620*/,
	(methodPointerType)&TweenerCore_3_Startup_m14952_gshared/* 1621*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m14954_gshared/* 1622*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14956_gshared/* 1623*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14957_gshared/* 1624*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14958_gshared/* 1625*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14959_gshared/* 1626*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14960_gshared/* 1627*/,
	(methodPointerType)&Action_1_Invoke_m14963_gshared/* 1628*/,
	(methodPointerType)&Action_1_BeginInvoke_m14965_gshared/* 1629*/,
	(methodPointerType)&Action_1_EndInvoke_m14967_gshared/* 1630*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15144_gshared/* 1631*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_gshared/* 1632*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15146_gshared/* 1633*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15147_gshared/* 1634*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15148_gshared/* 1635*/,
	(methodPointerType)&Comparison_1_Invoke_m15303_gshared/* 1636*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15304_gshared/* 1637*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15305_gshared/* 1638*/,
	(methodPointerType)&List_1__ctor_m15549_gshared/* 1639*/,
	(methodPointerType)&List_1__ctor_m15550_gshared/* 1640*/,
	(methodPointerType)&List_1__cctor_m15551_gshared/* 1641*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15552_gshared/* 1642*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15553_gshared/* 1643*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15554_gshared/* 1644*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15555_gshared/* 1645*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15556_gshared/* 1646*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15557_gshared/* 1647*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15558_gshared/* 1648*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15559_gshared/* 1649*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15560_gshared/* 1650*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15561_gshared/* 1651*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15562_gshared/* 1652*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15563_gshared/* 1653*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15564_gshared/* 1654*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15565_gshared/* 1655*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15566_gshared/* 1656*/,
	(methodPointerType)&List_1_Add_m15567_gshared/* 1657*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15568_gshared/* 1658*/,
	(methodPointerType)&List_1_AddCollection_m15569_gshared/* 1659*/,
	(methodPointerType)&List_1_AddEnumerable_m15570_gshared/* 1660*/,
	(methodPointerType)&List_1_AddRange_m15571_gshared/* 1661*/,
	(methodPointerType)&List_1_AsReadOnly_m15572_gshared/* 1662*/,
	(methodPointerType)&List_1_Clear_m15573_gshared/* 1663*/,
	(methodPointerType)&List_1_Contains_m15574_gshared/* 1664*/,
	(methodPointerType)&List_1_CopyTo_m15575_gshared/* 1665*/,
	(methodPointerType)&List_1_Find_m15576_gshared/* 1666*/,
	(methodPointerType)&List_1_CheckMatch_m15577_gshared/* 1667*/,
	(methodPointerType)&List_1_GetIndex_m15578_gshared/* 1668*/,
	(methodPointerType)&List_1_GetEnumerator_m15579_gshared/* 1669*/,
	(methodPointerType)&List_1_IndexOf_m15580_gshared/* 1670*/,
	(methodPointerType)&List_1_Shift_m15581_gshared/* 1671*/,
	(methodPointerType)&List_1_CheckIndex_m15582_gshared/* 1672*/,
	(methodPointerType)&List_1_Insert_m15583_gshared/* 1673*/,
	(methodPointerType)&List_1_CheckCollection_m15584_gshared/* 1674*/,
	(methodPointerType)&List_1_Remove_m15585_gshared/* 1675*/,
	(methodPointerType)&List_1_RemoveAll_m15586_gshared/* 1676*/,
	(methodPointerType)&List_1_RemoveAt_m15587_gshared/* 1677*/,
	(methodPointerType)&List_1_Reverse_m15588_gshared/* 1678*/,
	(methodPointerType)&List_1_Sort_m15589_gshared/* 1679*/,
	(methodPointerType)&List_1_ToArray_m15590_gshared/* 1680*/,
	(methodPointerType)&List_1_TrimExcess_m15591_gshared/* 1681*/,
	(methodPointerType)&List_1_get_Capacity_m15592_gshared/* 1682*/,
	(methodPointerType)&List_1_set_Capacity_m15593_gshared/* 1683*/,
	(methodPointerType)&List_1_get_Count_m15594_gshared/* 1684*/,
	(methodPointerType)&List_1_get_Item_m15595_gshared/* 1685*/,
	(methodPointerType)&List_1_set_Item_m15596_gshared/* 1686*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15597_gshared/* 1687*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15598_gshared/* 1688*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15599_gshared/* 1689*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15600_gshared/* 1690*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15601_gshared/* 1691*/,
	(methodPointerType)&Enumerator__ctor_m15602_gshared/* 1692*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15603_gshared/* 1693*/,
	(methodPointerType)&Enumerator_Dispose_m15604_gshared/* 1694*/,
	(methodPointerType)&Enumerator_VerifyState_m15605_gshared/* 1695*/,
	(methodPointerType)&Enumerator_MoveNext_m15606_gshared/* 1696*/,
	(methodPointerType)&Enumerator_get_Current_m15607_gshared/* 1697*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15608_gshared/* 1698*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15609_gshared/* 1699*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15610_gshared/* 1700*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15611_gshared/* 1701*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15612_gshared/* 1702*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15613_gshared/* 1703*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15614_gshared/* 1704*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15615_gshared/* 1705*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15616_gshared/* 1706*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15617_gshared/* 1707*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15618_gshared/* 1708*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15619_gshared/* 1709*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15620_gshared/* 1710*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15621_gshared/* 1711*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15622_gshared/* 1712*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15623_gshared/* 1713*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15624_gshared/* 1714*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15625_gshared/* 1715*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15626_gshared/* 1716*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15627_gshared/* 1717*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15628_gshared/* 1718*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15629_gshared/* 1719*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15630_gshared/* 1720*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15631_gshared/* 1721*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15632_gshared/* 1722*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15633_gshared/* 1723*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15634_gshared/* 1724*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15635_gshared/* 1725*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15636_gshared/* 1726*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15637_gshared/* 1727*/,
	(methodPointerType)&Collection_1__ctor_m15638_gshared/* 1728*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15639_gshared/* 1729*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15640_gshared/* 1730*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15641_gshared/* 1731*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15642_gshared/* 1732*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15643_gshared/* 1733*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15644_gshared/* 1734*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15645_gshared/* 1735*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15646_gshared/* 1736*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15647_gshared/* 1737*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15648_gshared/* 1738*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15649_gshared/* 1739*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15650_gshared/* 1740*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15651_gshared/* 1741*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15652_gshared/* 1742*/,
	(methodPointerType)&Collection_1_Add_m15653_gshared/* 1743*/,
	(methodPointerType)&Collection_1_Clear_m15654_gshared/* 1744*/,
	(methodPointerType)&Collection_1_ClearItems_m15655_gshared/* 1745*/,
	(methodPointerType)&Collection_1_Contains_m15656_gshared/* 1746*/,
	(methodPointerType)&Collection_1_CopyTo_m15657_gshared/* 1747*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15658_gshared/* 1748*/,
	(methodPointerType)&Collection_1_IndexOf_m15659_gshared/* 1749*/,
	(methodPointerType)&Collection_1_Insert_m15660_gshared/* 1750*/,
	(methodPointerType)&Collection_1_InsertItem_m15661_gshared/* 1751*/,
	(methodPointerType)&Collection_1_Remove_m15662_gshared/* 1752*/,
	(methodPointerType)&Collection_1_RemoveAt_m15663_gshared/* 1753*/,
	(methodPointerType)&Collection_1_RemoveItem_m15664_gshared/* 1754*/,
	(methodPointerType)&Collection_1_get_Count_m15665_gshared/* 1755*/,
	(methodPointerType)&Collection_1_get_Item_m15666_gshared/* 1756*/,
	(methodPointerType)&Collection_1_set_Item_m15667_gshared/* 1757*/,
	(methodPointerType)&Collection_1_SetItem_m15668_gshared/* 1758*/,
	(methodPointerType)&Collection_1_IsValidItem_m15669_gshared/* 1759*/,
	(methodPointerType)&Collection_1_ConvertItem_m15670_gshared/* 1760*/,
	(methodPointerType)&Collection_1_CheckWritable_m15671_gshared/* 1761*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15672_gshared/* 1762*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15673_gshared/* 1763*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15674_gshared/* 1764*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15675_gshared/* 1765*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15676_gshared/* 1766*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15677_gshared/* 1767*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15678_gshared/* 1768*/,
	(methodPointerType)&DefaultComparer__ctor_m15679_gshared/* 1769*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15680_gshared/* 1770*/,
	(methodPointerType)&DefaultComparer_Equals_m15681_gshared/* 1771*/,
	(methodPointerType)&Predicate_1__ctor_m15682_gshared/* 1772*/,
	(methodPointerType)&Predicate_1_Invoke_m15683_gshared/* 1773*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15684_gshared/* 1774*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15685_gshared/* 1775*/,
	(methodPointerType)&Comparer_1__ctor_m15686_gshared/* 1776*/,
	(methodPointerType)&Comparer_1__cctor_m15687_gshared/* 1777*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15688_gshared/* 1778*/,
	(methodPointerType)&Comparer_1_get_Default_m15689_gshared/* 1779*/,
	(methodPointerType)&DefaultComparer__ctor_m15690_gshared/* 1780*/,
	(methodPointerType)&DefaultComparer_Compare_m15691_gshared/* 1781*/,
	(methodPointerType)&Dictionary_2__ctor_m16129_gshared/* 1782*/,
	(methodPointerType)&Dictionary_2__ctor_m16131_gshared/* 1783*/,
	(methodPointerType)&Dictionary_2__ctor_m16133_gshared/* 1784*/,
	(methodPointerType)&Dictionary_2__ctor_m16135_gshared/* 1785*/,
	(methodPointerType)&Dictionary_2__ctor_m16137_gshared/* 1786*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16139_gshared/* 1787*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16141_gshared/* 1788*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16143_gshared/* 1789*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16145_gshared/* 1790*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16147_gshared/* 1791*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16149_gshared/* 1792*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16151_gshared/* 1793*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16153_gshared/* 1794*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16155_gshared/* 1795*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16157_gshared/* 1796*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16159_gshared/* 1797*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16161_gshared/* 1798*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16163_gshared/* 1799*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16165_gshared/* 1800*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16167_gshared/* 1801*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16169_gshared/* 1802*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16171_gshared/* 1803*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16173_gshared/* 1804*/,
	(methodPointerType)&Dictionary_2_get_Count_m16175_gshared/* 1805*/,
	(methodPointerType)&Dictionary_2_get_Item_m16177_gshared/* 1806*/,
	(methodPointerType)&Dictionary_2_set_Item_m16179_gshared/* 1807*/,
	(methodPointerType)&Dictionary_2_Init_m16181_gshared/* 1808*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16183_gshared/* 1809*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16185_gshared/* 1810*/,
	(methodPointerType)&Dictionary_2_make_pair_m16187_gshared/* 1811*/,
	(methodPointerType)&Dictionary_2_pick_key_m16189_gshared/* 1812*/,
	(methodPointerType)&Dictionary_2_pick_value_m16191_gshared/* 1813*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16193_gshared/* 1814*/,
	(methodPointerType)&Dictionary_2_Resize_m16195_gshared/* 1815*/,
	(methodPointerType)&Dictionary_2_Add_m16197_gshared/* 1816*/,
	(methodPointerType)&Dictionary_2_Clear_m16199_gshared/* 1817*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16201_gshared/* 1818*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16205_gshared/* 1819*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16207_gshared/* 1820*/,
	(methodPointerType)&Dictionary_2_Remove_m16209_gshared/* 1821*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16211_gshared/* 1822*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16216_gshared/* 1823*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16218_gshared/* 1824*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16220_gshared/* 1825*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16223_gshared/* 1826*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16224_gshared/* 1827*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16225_gshared/* 1828*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16226_gshared/* 1829*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16227_gshared/* 1830*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16228_gshared/* 1831*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16229_gshared/* 1832*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16231_gshared/* 1833*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16233_gshared/* 1834*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16235_gshared/* 1835*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16236_gshared/* 1836*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16237_gshared/* 1837*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16238_gshared/* 1838*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16239_gshared/* 1839*/,
	(methodPointerType)&KeyCollection__ctor_m16240_gshared/* 1840*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16241_gshared/* 1841*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16242_gshared/* 1842*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16243_gshared/* 1843*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16244_gshared/* 1844*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16245_gshared/* 1845*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16246_gshared/* 1846*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16247_gshared/* 1847*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16248_gshared/* 1848*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16249_gshared/* 1849*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16250_gshared/* 1850*/,
	(methodPointerType)&KeyCollection_CopyTo_m16251_gshared/* 1851*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16252_gshared/* 1852*/,
	(methodPointerType)&KeyCollection_get_Count_m16253_gshared/* 1853*/,
	(methodPointerType)&Enumerator__ctor_m16254_gshared/* 1854*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16255_gshared/* 1855*/,
	(methodPointerType)&Enumerator_Dispose_m16256_gshared/* 1856*/,
	(methodPointerType)&Enumerator_MoveNext_m16257_gshared/* 1857*/,
	(methodPointerType)&Enumerator_get_Current_m16258_gshared/* 1858*/,
	(methodPointerType)&Enumerator__ctor_m16259_gshared/* 1859*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared/* 1860*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared/* 1861*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared/* 1862*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared/* 1863*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16266_gshared/* 1864*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16267_gshared/* 1865*/,
	(methodPointerType)&Enumerator_VerifyState_m16268_gshared/* 1866*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16269_gshared/* 1867*/,
	(methodPointerType)&Enumerator_Dispose_m16270_gshared/* 1868*/,
	(methodPointerType)&Transform_1__ctor_m16271_gshared/* 1869*/,
	(methodPointerType)&Transform_1_Invoke_m16272_gshared/* 1870*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16273_gshared/* 1871*/,
	(methodPointerType)&Transform_1_EndInvoke_m16274_gshared/* 1872*/,
	(methodPointerType)&ValueCollection__ctor_m16275_gshared/* 1873*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16276_gshared/* 1874*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16277_gshared/* 1875*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16278_gshared/* 1876*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16279_gshared/* 1877*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16280_gshared/* 1878*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16281_gshared/* 1879*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16282_gshared/* 1880*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16283_gshared/* 1881*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16284_gshared/* 1882*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16285_gshared/* 1883*/,
	(methodPointerType)&ValueCollection_CopyTo_m16286_gshared/* 1884*/,
	(methodPointerType)&ValueCollection_get_Count_m16288_gshared/* 1885*/,
	(methodPointerType)&Enumerator__ctor_m16289_gshared/* 1886*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared/* 1887*/,
	(methodPointerType)&Enumerator_Dispose_m16291_gshared/* 1888*/,
	(methodPointerType)&Transform_1__ctor_m16294_gshared/* 1889*/,
	(methodPointerType)&Transform_1_Invoke_m16295_gshared/* 1890*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16296_gshared/* 1891*/,
	(methodPointerType)&Transform_1_EndInvoke_m16297_gshared/* 1892*/,
	(methodPointerType)&Transform_1__ctor_m16298_gshared/* 1893*/,
	(methodPointerType)&Transform_1_Invoke_m16299_gshared/* 1894*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16300_gshared/* 1895*/,
	(methodPointerType)&Transform_1_EndInvoke_m16301_gshared/* 1896*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16302_gshared/* 1897*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16303_gshared/* 1898*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16304_gshared/* 1899*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16305_gshared/* 1900*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16306_gshared/* 1901*/,
	(methodPointerType)&Transform_1__ctor_m16307_gshared/* 1902*/,
	(methodPointerType)&Transform_1_Invoke_m16308_gshared/* 1903*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16309_gshared/* 1904*/,
	(methodPointerType)&Transform_1_EndInvoke_m16310_gshared/* 1905*/,
	(methodPointerType)&ShimEnumerator__ctor_m16311_gshared/* 1906*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16312_gshared/* 1907*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16313_gshared/* 1908*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16314_gshared/* 1909*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16315_gshared/* 1910*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16316_gshared/* 1911*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16317_gshared/* 1912*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16318_gshared/* 1913*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16319_gshared/* 1914*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16320_gshared/* 1915*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16321_gshared/* 1916*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16322_gshared/* 1917*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16323_gshared/* 1918*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16324_gshared/* 1919*/,
	(methodPointerType)&DefaultComparer__ctor_m16325_gshared/* 1920*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16326_gshared/* 1921*/,
	(methodPointerType)&DefaultComparer_Equals_m16327_gshared/* 1922*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16468_gshared/* 1923*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16469_gshared/* 1924*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16470_gshared/* 1925*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16471_gshared/* 1926*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16472_gshared/* 1927*/,
	(methodPointerType)&Comparison_1_Invoke_m16473_gshared/* 1928*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m16474_gshared/* 1929*/,
	(methodPointerType)&Comparison_1_EndInvoke_m16475_gshared/* 1930*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16476_gshared/* 1931*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16477_gshared/* 1932*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16478_gshared/* 1933*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16479_gshared/* 1934*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16480_gshared/* 1935*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m16481_gshared/* 1936*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16482_gshared/* 1937*/,
	(methodPointerType)&UnityAction_1_Invoke_m16483_gshared/* 1938*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m16484_gshared/* 1939*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m16485_gshared/* 1940*/,
	(methodPointerType)&InvokableCall_1__ctor_m16486_gshared/* 1941*/,
	(methodPointerType)&InvokableCall_1__ctor_m16487_gshared/* 1942*/,
	(methodPointerType)&InvokableCall_1_Invoke_m16488_gshared/* 1943*/,
	(methodPointerType)&InvokableCall_1_Find_m16489_gshared/* 1944*/,
	(methodPointerType)&Dictionary_2__ctor_m16521_gshared/* 1945*/,
	(methodPointerType)&Dictionary_2__ctor_m16522_gshared/* 1946*/,
	(methodPointerType)&Dictionary_2__ctor_m16523_gshared/* 1947*/,
	(methodPointerType)&Dictionary_2__ctor_m16525_gshared/* 1948*/,
	(methodPointerType)&Dictionary_2__ctor_m16526_gshared/* 1949*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527_gshared/* 1950*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528_gshared/* 1951*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16529_gshared/* 1952*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16530_gshared/* 1953*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16531_gshared/* 1954*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16532_gshared/* 1955*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16533_gshared/* 1956*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534_gshared/* 1957*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535_gshared/* 1958*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536_gshared/* 1959*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537_gshared/* 1960*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538_gshared/* 1961*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539_gshared/* 1962*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540_gshared/* 1963*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16541_gshared/* 1964*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542_gshared/* 1965*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543_gshared/* 1966*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544_gshared/* 1967*/,
	(methodPointerType)&Dictionary_2_get_Count_m16545_gshared/* 1968*/,
	(methodPointerType)&Dictionary_2_get_Item_m16546_gshared/* 1969*/,
	(methodPointerType)&Dictionary_2_set_Item_m16547_gshared/* 1970*/,
	(methodPointerType)&Dictionary_2_Init_m16548_gshared/* 1971*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16549_gshared/* 1972*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16550_gshared/* 1973*/,
	(methodPointerType)&Dictionary_2_make_pair_m16551_gshared/* 1974*/,
	(methodPointerType)&Dictionary_2_pick_key_m16552_gshared/* 1975*/,
	(methodPointerType)&Dictionary_2_pick_value_m16553_gshared/* 1976*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16554_gshared/* 1977*/,
	(methodPointerType)&Dictionary_2_Resize_m16555_gshared/* 1978*/,
	(methodPointerType)&Dictionary_2_Add_m16556_gshared/* 1979*/,
	(methodPointerType)&Dictionary_2_Clear_m16557_gshared/* 1980*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16558_gshared/* 1981*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16559_gshared/* 1982*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16560_gshared/* 1983*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16561_gshared/* 1984*/,
	(methodPointerType)&Dictionary_2_Remove_m16562_gshared/* 1985*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16563_gshared/* 1986*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16564_gshared/* 1987*/,
	(methodPointerType)&Dictionary_2_get_Values_m16565_gshared/* 1988*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16566_gshared/* 1989*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16567_gshared/* 1990*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16568_gshared/* 1991*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16569_gshared/* 1992*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16570_gshared/* 1993*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16571_gshared/* 1994*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16572_gshared/* 1995*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16573_gshared/* 1996*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16574_gshared/* 1997*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16575_gshared/* 1998*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16576_gshared/* 1999*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16577_gshared/* 2000*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16578_gshared/* 2001*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16579_gshared/* 2002*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16580_gshared/* 2003*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16581_gshared/* 2004*/,
	(methodPointerType)&KeyCollection__ctor_m16582_gshared/* 2005*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16583_gshared/* 2006*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16584_gshared/* 2007*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16585_gshared/* 2008*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16586_gshared/* 2009*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16587_gshared/* 2010*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16588_gshared/* 2011*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16589_gshared/* 2012*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16590_gshared/* 2013*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16591_gshared/* 2014*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16592_gshared/* 2015*/,
	(methodPointerType)&KeyCollection_CopyTo_m16593_gshared/* 2016*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16594_gshared/* 2017*/,
	(methodPointerType)&KeyCollection_get_Count_m16595_gshared/* 2018*/,
	(methodPointerType)&Enumerator__ctor_m16596_gshared/* 2019*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16597_gshared/* 2020*/,
	(methodPointerType)&Enumerator_Dispose_m16598_gshared/* 2021*/,
	(methodPointerType)&Enumerator_MoveNext_m16599_gshared/* 2022*/,
	(methodPointerType)&Enumerator_get_Current_m16600_gshared/* 2023*/,
	(methodPointerType)&Enumerator__ctor_m16601_gshared/* 2024*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16602_gshared/* 2025*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16603_gshared/* 2026*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16604_gshared/* 2027*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16605_gshared/* 2028*/,
	(methodPointerType)&Enumerator_MoveNext_m16606_gshared/* 2029*/,
	(methodPointerType)&Enumerator_get_Current_m16607_gshared/* 2030*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16608_gshared/* 2031*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16609_gshared/* 2032*/,
	(methodPointerType)&Enumerator_VerifyState_m16610_gshared/* 2033*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16611_gshared/* 2034*/,
	(methodPointerType)&Enumerator_Dispose_m16612_gshared/* 2035*/,
	(methodPointerType)&Transform_1__ctor_m16613_gshared/* 2036*/,
	(methodPointerType)&Transform_1_Invoke_m16614_gshared/* 2037*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16615_gshared/* 2038*/,
	(methodPointerType)&Transform_1_EndInvoke_m16616_gshared/* 2039*/,
	(methodPointerType)&ValueCollection__ctor_m16617_gshared/* 2040*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16618_gshared/* 2041*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16619_gshared/* 2042*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16620_gshared/* 2043*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16621_gshared/* 2044*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16622_gshared/* 2045*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16623_gshared/* 2046*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16624_gshared/* 2047*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16625_gshared/* 2048*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16626_gshared/* 2049*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16627_gshared/* 2050*/,
	(methodPointerType)&ValueCollection_CopyTo_m16628_gshared/* 2051*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16629_gshared/* 2052*/,
	(methodPointerType)&ValueCollection_get_Count_m16630_gshared/* 2053*/,
	(methodPointerType)&Enumerator__ctor_m16631_gshared/* 2054*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16632_gshared/* 2055*/,
	(methodPointerType)&Enumerator_Dispose_m16633_gshared/* 2056*/,
	(methodPointerType)&Enumerator_MoveNext_m16634_gshared/* 2057*/,
	(methodPointerType)&Enumerator_get_Current_m16635_gshared/* 2058*/,
	(methodPointerType)&Transform_1__ctor_m16636_gshared/* 2059*/,
	(methodPointerType)&Transform_1_Invoke_m16637_gshared/* 2060*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16638_gshared/* 2061*/,
	(methodPointerType)&Transform_1_EndInvoke_m16639_gshared/* 2062*/,
	(methodPointerType)&Transform_1__ctor_m16640_gshared/* 2063*/,
	(methodPointerType)&Transform_1_Invoke_m16641_gshared/* 2064*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16642_gshared/* 2065*/,
	(methodPointerType)&Transform_1_EndInvoke_m16643_gshared/* 2066*/,
	(methodPointerType)&Transform_1__ctor_m16644_gshared/* 2067*/,
	(methodPointerType)&Transform_1_Invoke_m16645_gshared/* 2068*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16646_gshared/* 2069*/,
	(methodPointerType)&Transform_1_EndInvoke_m16647_gshared/* 2070*/,
	(methodPointerType)&ShimEnumerator__ctor_m16648_gshared/* 2071*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16649_gshared/* 2072*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16650_gshared/* 2073*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16651_gshared/* 2074*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16652_gshared/* 2075*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16653_gshared/* 2076*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16903_gshared/* 2077*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_gshared/* 2078*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16905_gshared/* 2079*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16906_gshared/* 2080*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16907_gshared/* 2081*/,
	(methodPointerType)&Transform_1__ctor_m16968_gshared/* 2082*/,
	(methodPointerType)&Transform_1_Invoke_m16969_gshared/* 2083*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16970_gshared/* 2084*/,
	(methodPointerType)&Transform_1_EndInvoke_m16971_gshared/* 2085*/,
	(methodPointerType)&Transform_1__ctor_m16972_gshared/* 2086*/,
	(methodPointerType)&Transform_1_Invoke_m16973_gshared/* 2087*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16974_gshared/* 2088*/,
	(methodPointerType)&Transform_1_EndInvoke_m16975_gshared/* 2089*/,
	(methodPointerType)&List_1__ctor_m17143_gshared/* 2090*/,
	(methodPointerType)&List_1__cctor_m17144_gshared/* 2091*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17145_gshared/* 2092*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m17146_gshared/* 2093*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m17147_gshared/* 2094*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m17148_gshared/* 2095*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m17149_gshared/* 2096*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m17150_gshared/* 2097*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m17151_gshared/* 2098*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m17152_gshared/* 2099*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17153_gshared/* 2100*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m17154_gshared/* 2101*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m17155_gshared/* 2102*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m17156_gshared/* 2103*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m17157_gshared/* 2104*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m17158_gshared/* 2105*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m17159_gshared/* 2106*/,
	(methodPointerType)&List_1_Add_m17160_gshared/* 2107*/,
	(methodPointerType)&List_1_GrowIfNeeded_m17161_gshared/* 2108*/,
	(methodPointerType)&List_1_AddCollection_m17162_gshared/* 2109*/,
	(methodPointerType)&List_1_AddEnumerable_m17163_gshared/* 2110*/,
	(methodPointerType)&List_1_AddRange_m17164_gshared/* 2111*/,
	(methodPointerType)&List_1_AsReadOnly_m17165_gshared/* 2112*/,
	(methodPointerType)&List_1_Clear_m17166_gshared/* 2113*/,
	(methodPointerType)&List_1_Contains_m17167_gshared/* 2114*/,
	(methodPointerType)&List_1_CopyTo_m17168_gshared/* 2115*/,
	(methodPointerType)&List_1_Find_m17169_gshared/* 2116*/,
	(methodPointerType)&List_1_CheckMatch_m17170_gshared/* 2117*/,
	(methodPointerType)&List_1_GetIndex_m17171_gshared/* 2118*/,
	(methodPointerType)&List_1_GetEnumerator_m17172_gshared/* 2119*/,
	(methodPointerType)&List_1_IndexOf_m17173_gshared/* 2120*/,
	(methodPointerType)&List_1_Shift_m17174_gshared/* 2121*/,
	(methodPointerType)&List_1_CheckIndex_m17175_gshared/* 2122*/,
	(methodPointerType)&List_1_Insert_m17176_gshared/* 2123*/,
	(methodPointerType)&List_1_CheckCollection_m17177_gshared/* 2124*/,
	(methodPointerType)&List_1_Remove_m17178_gshared/* 2125*/,
	(methodPointerType)&List_1_RemoveAll_m17179_gshared/* 2126*/,
	(methodPointerType)&List_1_RemoveAt_m17180_gshared/* 2127*/,
	(methodPointerType)&List_1_Reverse_m17181_gshared/* 2128*/,
	(methodPointerType)&List_1_Sort_m17182_gshared/* 2129*/,
	(methodPointerType)&List_1_Sort_m17183_gshared/* 2130*/,
	(methodPointerType)&List_1_TrimExcess_m17184_gshared/* 2131*/,
	(methodPointerType)&List_1_get_Count_m17185_gshared/* 2132*/,
	(methodPointerType)&List_1_get_Item_m17186_gshared/* 2133*/,
	(methodPointerType)&List_1_set_Item_m17187_gshared/* 2134*/,
	(methodPointerType)&Enumerator__ctor_m17122_gshared/* 2135*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17123_gshared/* 2136*/,
	(methodPointerType)&Enumerator_Dispose_m17124_gshared/* 2137*/,
	(methodPointerType)&Enumerator_VerifyState_m17125_gshared/* 2138*/,
	(methodPointerType)&Enumerator_MoveNext_m17126_gshared/* 2139*/,
	(methodPointerType)&Enumerator_get_Current_m17127_gshared/* 2140*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m17088_gshared/* 2141*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17089_gshared/* 2142*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17090_gshared/* 2143*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17091_gshared/* 2144*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17092_gshared/* 2145*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17093_gshared/* 2146*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17094_gshared/* 2147*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17095_gshared/* 2148*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17096_gshared/* 2149*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17097_gshared/* 2150*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17098_gshared/* 2151*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m17099_gshared/* 2152*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m17100_gshared/* 2153*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m17101_gshared/* 2154*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17102_gshared/* 2155*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m17103_gshared/* 2156*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m17104_gshared/* 2157*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17105_gshared/* 2158*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17106_gshared/* 2159*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17107_gshared/* 2160*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17108_gshared/* 2161*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17109_gshared/* 2162*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m17110_gshared/* 2163*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m17111_gshared/* 2164*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m17112_gshared/* 2165*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m17113_gshared/* 2166*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m17114_gshared/* 2167*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m17115_gshared/* 2168*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m17116_gshared/* 2169*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m17117_gshared/* 2170*/,
	(methodPointerType)&Collection_1__ctor_m17191_gshared/* 2171*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17192_gshared/* 2172*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m17193_gshared/* 2173*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m17194_gshared/* 2174*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m17195_gshared/* 2175*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m17196_gshared/* 2176*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m17197_gshared/* 2177*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m17198_gshared/* 2178*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m17199_gshared/* 2179*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m17200_gshared/* 2180*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m17201_gshared/* 2181*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m17202_gshared/* 2182*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m17203_gshared/* 2183*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m17204_gshared/* 2184*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m17205_gshared/* 2185*/,
	(methodPointerType)&Collection_1_Add_m17206_gshared/* 2186*/,
	(methodPointerType)&Collection_1_Clear_m17207_gshared/* 2187*/,
	(methodPointerType)&Collection_1_ClearItems_m17208_gshared/* 2188*/,
	(methodPointerType)&Collection_1_Contains_m17209_gshared/* 2189*/,
	(methodPointerType)&Collection_1_CopyTo_m17210_gshared/* 2190*/,
	(methodPointerType)&Collection_1_GetEnumerator_m17211_gshared/* 2191*/,
	(methodPointerType)&Collection_1_IndexOf_m17212_gshared/* 2192*/,
	(methodPointerType)&Collection_1_Insert_m17213_gshared/* 2193*/,
	(methodPointerType)&Collection_1_InsertItem_m17214_gshared/* 2194*/,
	(methodPointerType)&Collection_1_Remove_m17215_gshared/* 2195*/,
	(methodPointerType)&Collection_1_RemoveAt_m17216_gshared/* 2196*/,
	(methodPointerType)&Collection_1_RemoveItem_m17217_gshared/* 2197*/,
	(methodPointerType)&Collection_1_get_Count_m17218_gshared/* 2198*/,
	(methodPointerType)&Collection_1_get_Item_m17219_gshared/* 2199*/,
	(methodPointerType)&Collection_1_set_Item_m17220_gshared/* 2200*/,
	(methodPointerType)&Collection_1_SetItem_m17221_gshared/* 2201*/,
	(methodPointerType)&Collection_1_IsValidItem_m17222_gshared/* 2202*/,
	(methodPointerType)&Collection_1_ConvertItem_m17223_gshared/* 2203*/,
	(methodPointerType)&Collection_1_CheckWritable_m17224_gshared/* 2204*/,
	(methodPointerType)&Collection_1_IsSynchronized_m17225_gshared/* 2205*/,
	(methodPointerType)&Collection_1_IsFixedSize_m17226_gshared/* 2206*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17227_gshared/* 2207*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17228_gshared/* 2208*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17229_gshared/* 2209*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17230_gshared/* 2210*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17231_gshared/* 2211*/,
	(methodPointerType)&DefaultComparer__ctor_m17232_gshared/* 2212*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17233_gshared/* 2213*/,
	(methodPointerType)&DefaultComparer_Equals_m17234_gshared/* 2214*/,
	(methodPointerType)&Predicate_1__ctor_m17118_gshared/* 2215*/,
	(methodPointerType)&Predicate_1_Invoke_m17119_gshared/* 2216*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m17120_gshared/* 2217*/,
	(methodPointerType)&Predicate_1_EndInvoke_m17121_gshared/* 2218*/,
	(methodPointerType)&Comparer_1__ctor_m17235_gshared/* 2219*/,
	(methodPointerType)&Comparer_1__cctor_m17236_gshared/* 2220*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m17237_gshared/* 2221*/,
	(methodPointerType)&Comparer_1_get_Default_m17238_gshared/* 2222*/,
	(methodPointerType)&DefaultComparer__ctor_m17239_gshared/* 2223*/,
	(methodPointerType)&DefaultComparer_Compare_m17240_gshared/* 2224*/,
	(methodPointerType)&Comparison_1__ctor_m17128_gshared/* 2225*/,
	(methodPointerType)&Comparison_1_Invoke_m17129_gshared/* 2226*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m17130_gshared/* 2227*/,
	(methodPointerType)&Comparison_1_EndInvoke_m17131_gshared/* 2228*/,
	(methodPointerType)&TweenRunner_1_Start_m17241_gshared/* 2229*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m17242_gshared/* 2230*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17243_gshared/* 2231*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17244_gshared/* 2232*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m17245_gshared/* 2233*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m17246_gshared/* 2234*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m17247_gshared/* 2235*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17698_gshared/* 2236*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17699_gshared/* 2237*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17700_gshared/* 2238*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17701_gshared/* 2239*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17702_gshared/* 2240*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17708_gshared/* 2241*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17709_gshared/* 2242*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17710_gshared/* 2243*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17711_gshared/* 2244*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17712_gshared/* 2245*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17713_gshared/* 2246*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17714_gshared/* 2247*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17715_gshared/* 2248*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17716_gshared/* 2249*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17717_gshared/* 2250*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m17725_gshared/* 2251*/,
	(methodPointerType)&UnityAction_1_Invoke_m17726_gshared/* 2252*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17727_gshared/* 2253*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17728_gshared/* 2254*/,
	(methodPointerType)&InvokableCall_1__ctor_m17729_gshared/* 2255*/,
	(methodPointerType)&InvokableCall_1__ctor_m17730_gshared/* 2256*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17731_gshared/* 2257*/,
	(methodPointerType)&InvokableCall_1_Find_m17732_gshared/* 2258*/,
	(methodPointerType)&UnityEvent_1_AddListener_m17733_gshared/* 2259*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m17734_gshared/* 2260*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m17735_gshared/* 2261*/,
	(methodPointerType)&UnityAction_1__ctor_m17736_gshared/* 2262*/,
	(methodPointerType)&UnityAction_1_Invoke_m17737_gshared/* 2263*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17738_gshared/* 2264*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17739_gshared/* 2265*/,
	(methodPointerType)&InvokableCall_1__ctor_m17740_gshared/* 2266*/,
	(methodPointerType)&InvokableCall_1__ctor_m17741_gshared/* 2267*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17742_gshared/* 2268*/,
	(methodPointerType)&InvokableCall_1_Find_m17743_gshared/* 2269*/,
	(methodPointerType)&UnityEvent_1_AddListener_m18025_gshared/* 2270*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m18027_gshared/* 2271*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18031_gshared/* 2272*/,
	(methodPointerType)&UnityAction_1__ctor_m18033_gshared/* 2273*/,
	(methodPointerType)&UnityAction_1_Invoke_m18034_gshared/* 2274*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m18035_gshared/* 2275*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m18036_gshared/* 2276*/,
	(methodPointerType)&InvokableCall_1__ctor_m18037_gshared/* 2277*/,
	(methodPointerType)&InvokableCall_1__ctor_m18038_gshared/* 2278*/,
	(methodPointerType)&InvokableCall_1_Invoke_m18039_gshared/* 2279*/,
	(methodPointerType)&InvokableCall_1_Find_m18040_gshared/* 2280*/,
	(methodPointerType)&Func_2_Invoke_m18138_gshared/* 2281*/,
	(methodPointerType)&Func_2_BeginInvoke_m18140_gshared/* 2282*/,
	(methodPointerType)&Func_2_EndInvoke_m18142_gshared/* 2283*/,
	(methodPointerType)&Func_2_Invoke_m18252_gshared/* 2284*/,
	(methodPointerType)&Func_2_BeginInvoke_m18254_gshared/* 2285*/,
	(methodPointerType)&Func_2_EndInvoke_m18256_gshared/* 2286*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18293_gshared/* 2287*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18294_gshared/* 2288*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18295_gshared/* 2289*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18296_gshared/* 2290*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18297_gshared/* 2291*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18298_gshared/* 2292*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18299_gshared/* 2293*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18300_gshared/* 2294*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18301_gshared/* 2295*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18302_gshared/* 2296*/,
	(methodPointerType)&Action_1_Invoke_m18485_gshared/* 2297*/,
	(methodPointerType)&Action_1_BeginInvoke_m18487_gshared/* 2298*/,
	(methodPointerType)&Action_1_EndInvoke_m18489_gshared/* 2299*/,
	(methodPointerType)&List_1__ctor_m18860_gshared/* 2300*/,
	(methodPointerType)&List_1__cctor_m18862_gshared/* 2301*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864_gshared/* 2302*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m18866_gshared/* 2303*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m18868_gshared/* 2304*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m18870_gshared/* 2305*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m18872_gshared/* 2306*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m18874_gshared/* 2307*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m18876_gshared/* 2308*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m18878_gshared/* 2309*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880_gshared/* 2310*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m18882_gshared/* 2311*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m18884_gshared/* 2312*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m18886_gshared/* 2313*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m18888_gshared/* 2314*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m18890_gshared/* 2315*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m18892_gshared/* 2316*/,
	(methodPointerType)&List_1_Add_m18894_gshared/* 2317*/,
	(methodPointerType)&List_1_GrowIfNeeded_m18896_gshared/* 2318*/,
	(methodPointerType)&List_1_AddCollection_m18898_gshared/* 2319*/,
	(methodPointerType)&List_1_AddEnumerable_m18900_gshared/* 2320*/,
	(methodPointerType)&List_1_AddRange_m18902_gshared/* 2321*/,
	(methodPointerType)&List_1_AsReadOnly_m18904_gshared/* 2322*/,
	(methodPointerType)&List_1_Clear_m18906_gshared/* 2323*/,
	(methodPointerType)&List_1_Contains_m18908_gshared/* 2324*/,
	(methodPointerType)&List_1_CopyTo_m18910_gshared/* 2325*/,
	(methodPointerType)&List_1_Find_m18912_gshared/* 2326*/,
	(methodPointerType)&List_1_CheckMatch_m18914_gshared/* 2327*/,
	(methodPointerType)&List_1_GetIndex_m18916_gshared/* 2328*/,
	(methodPointerType)&List_1_IndexOf_m18919_gshared/* 2329*/,
	(methodPointerType)&List_1_Shift_m18921_gshared/* 2330*/,
	(methodPointerType)&List_1_CheckIndex_m18923_gshared/* 2331*/,
	(methodPointerType)&List_1_Insert_m18925_gshared/* 2332*/,
	(methodPointerType)&List_1_CheckCollection_m18927_gshared/* 2333*/,
	(methodPointerType)&List_1_Remove_m18929_gshared/* 2334*/,
	(methodPointerType)&List_1_RemoveAll_m18931_gshared/* 2335*/,
	(methodPointerType)&List_1_RemoveAt_m18933_gshared/* 2336*/,
	(methodPointerType)&List_1_Reverse_m18935_gshared/* 2337*/,
	(methodPointerType)&List_1_Sort_m18937_gshared/* 2338*/,
	(methodPointerType)&List_1_Sort_m18939_gshared/* 2339*/,
	(methodPointerType)&List_1_ToArray_m18941_gshared/* 2340*/,
	(methodPointerType)&List_1_TrimExcess_m18943_gshared/* 2341*/,
	(methodPointerType)&List_1_get_Capacity_m18945_gshared/* 2342*/,
	(methodPointerType)&List_1_set_Capacity_m18947_gshared/* 2343*/,
	(methodPointerType)&List_1_get_Count_m18949_gshared/* 2344*/,
	(methodPointerType)&List_1_get_Item_m18951_gshared/* 2345*/,
	(methodPointerType)&List_1_set_Item_m18953_gshared/* 2346*/,
	(methodPointerType)&Enumerator__ctor_m18954_gshared/* 2347*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18955_gshared/* 2348*/,
	(methodPointerType)&Enumerator_Dispose_m18956_gshared/* 2349*/,
	(methodPointerType)&Enumerator_VerifyState_m18957_gshared/* 2350*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m18958_gshared/* 2351*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18959_gshared/* 2352*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18960_gshared/* 2353*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18961_gshared/* 2354*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18962_gshared/* 2355*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18963_gshared/* 2356*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18964_gshared/* 2357*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18965_gshared/* 2358*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18966_gshared/* 2359*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18967_gshared/* 2360*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18968_gshared/* 2361*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m18969_gshared/* 2362*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m18970_gshared/* 2363*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m18971_gshared/* 2364*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18972_gshared/* 2365*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m18973_gshared/* 2366*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m18974_gshared/* 2367*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18975_gshared/* 2368*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18976_gshared/* 2369*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18977_gshared/* 2370*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18978_gshared/* 2371*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18979_gshared/* 2372*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m18980_gshared/* 2373*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m18981_gshared/* 2374*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m18982_gshared/* 2375*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m18983_gshared/* 2376*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m18984_gshared/* 2377*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m18985_gshared/* 2378*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m18986_gshared/* 2379*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m18987_gshared/* 2380*/,
	(methodPointerType)&Collection_1__ctor_m18988_gshared/* 2381*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18989_gshared/* 2382*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m18990_gshared/* 2383*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m18991_gshared/* 2384*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m18992_gshared/* 2385*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m18993_gshared/* 2386*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m18994_gshared/* 2387*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m18995_gshared/* 2388*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m18996_gshared/* 2389*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m18997_gshared/* 2390*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m18998_gshared/* 2391*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m18999_gshared/* 2392*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m19000_gshared/* 2393*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m19001_gshared/* 2394*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m19002_gshared/* 2395*/,
	(methodPointerType)&Collection_1_Add_m19003_gshared/* 2396*/,
	(methodPointerType)&Collection_1_Clear_m19004_gshared/* 2397*/,
	(methodPointerType)&Collection_1_ClearItems_m19005_gshared/* 2398*/,
	(methodPointerType)&Collection_1_Contains_m19006_gshared/* 2399*/,
	(methodPointerType)&Collection_1_CopyTo_m19007_gshared/* 2400*/,
	(methodPointerType)&Collection_1_GetEnumerator_m19008_gshared/* 2401*/,
	(methodPointerType)&Collection_1_IndexOf_m19009_gshared/* 2402*/,
	(methodPointerType)&Collection_1_Insert_m19010_gshared/* 2403*/,
	(methodPointerType)&Collection_1_InsertItem_m19011_gshared/* 2404*/,
	(methodPointerType)&Collection_1_Remove_m19012_gshared/* 2405*/,
	(methodPointerType)&Collection_1_RemoveAt_m19013_gshared/* 2406*/,
	(methodPointerType)&Collection_1_RemoveItem_m19014_gshared/* 2407*/,
	(methodPointerType)&Collection_1_get_Count_m19015_gshared/* 2408*/,
	(methodPointerType)&Collection_1_get_Item_m19016_gshared/* 2409*/,
	(methodPointerType)&Collection_1_set_Item_m19017_gshared/* 2410*/,
	(methodPointerType)&Collection_1_SetItem_m19018_gshared/* 2411*/,
	(methodPointerType)&Collection_1_IsValidItem_m19019_gshared/* 2412*/,
	(methodPointerType)&Collection_1_ConvertItem_m19020_gshared/* 2413*/,
	(methodPointerType)&Collection_1_CheckWritable_m19021_gshared/* 2414*/,
	(methodPointerType)&Collection_1_IsSynchronized_m19022_gshared/* 2415*/,
	(methodPointerType)&Collection_1_IsFixedSize_m19023_gshared/* 2416*/,
	(methodPointerType)&Predicate_1__ctor_m19024_gshared/* 2417*/,
	(methodPointerType)&Predicate_1_Invoke_m19025_gshared/* 2418*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19026_gshared/* 2419*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19027_gshared/* 2420*/,
	(methodPointerType)&Comparer_1__ctor_m19028_gshared/* 2421*/,
	(methodPointerType)&Comparer_1__cctor_m19029_gshared/* 2422*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m19030_gshared/* 2423*/,
	(methodPointerType)&Comparer_1_get_Default_m19031_gshared/* 2424*/,
	(methodPointerType)&GenericComparer_1__ctor_m19032_gshared/* 2425*/,
	(methodPointerType)&GenericComparer_1_Compare_m19033_gshared/* 2426*/,
	(methodPointerType)&DefaultComparer__ctor_m19034_gshared/* 2427*/,
	(methodPointerType)&DefaultComparer_Compare_m19035_gshared/* 2428*/,
	(methodPointerType)&Comparison_1__ctor_m19036_gshared/* 2429*/,
	(methodPointerType)&Comparison_1_Invoke_m19037_gshared/* 2430*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m19038_gshared/* 2431*/,
	(methodPointerType)&Comparison_1_EndInvoke_m19039_gshared/* 2432*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19271_gshared/* 2433*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272_gshared/* 2434*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19273_gshared/* 2435*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19274_gshared/* 2436*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19275_gshared/* 2437*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19276_gshared/* 2438*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19277_gshared/* 2439*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19278_gshared/* 2440*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19279_gshared/* 2441*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19280_gshared/* 2442*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19281_gshared/* 2443*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19282_gshared/* 2444*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19283_gshared/* 2445*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19284_gshared/* 2446*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19285_gshared/* 2447*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19765_gshared/* 2448*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19766_gshared/* 2449*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19767_gshared/* 2450*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19768_gshared/* 2451*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19769_gshared/* 2452*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19770_gshared/* 2453*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19771_gshared/* 2454*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19772_gshared/* 2455*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19773_gshared/* 2456*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19774_gshared/* 2457*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19775_gshared/* 2458*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19776_gshared/* 2459*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19777_gshared/* 2460*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19778_gshared/* 2461*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19779_gshared/* 2462*/,
	(methodPointerType)&LinkedList_1__ctor_m19780_gshared/* 2463*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19781_gshared/* 2464*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m19782_gshared/* 2465*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19783_gshared/* 2466*/,
	(methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19784_gshared/* 2467*/,
	(methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19785_gshared/* 2468*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19786_gshared/* 2469*/,
	(methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19787_gshared/* 2470*/,
	(methodPointerType)&LinkedList_1_VerifyReferencedNode_m19788_gshared/* 2471*/,
	(methodPointerType)&LinkedList_1_Clear_m19789_gshared/* 2472*/,
	(methodPointerType)&LinkedList_1_Contains_m19790_gshared/* 2473*/,
	(methodPointerType)&LinkedList_1_CopyTo_m19791_gshared/* 2474*/,
	(methodPointerType)&LinkedList_1_Find_m19792_gshared/* 2475*/,
	(methodPointerType)&LinkedList_1_GetEnumerator_m19793_gshared/* 2476*/,
	(methodPointerType)&LinkedList_1_GetObjectData_m19794_gshared/* 2477*/,
	(methodPointerType)&LinkedList_1_OnDeserialization_m19795_gshared/* 2478*/,
	(methodPointerType)&LinkedList_1_Remove_m19796_gshared/* 2479*/,
	(methodPointerType)&LinkedList_1_RemoveLast_m19797_gshared/* 2480*/,
	(methodPointerType)&LinkedList_1_get_Count_m19798_gshared/* 2481*/,
	(methodPointerType)&LinkedListNode_1__ctor_m19799_gshared/* 2482*/,
	(methodPointerType)&LinkedListNode_1__ctor_m19800_gshared/* 2483*/,
	(methodPointerType)&LinkedListNode_1_Detach_m19801_gshared/* 2484*/,
	(methodPointerType)&LinkedListNode_1_get_List_m19802_gshared/* 2485*/,
	(methodPointerType)&Enumerator__ctor_m19803_gshared/* 2486*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19804_gshared/* 2487*/,
	(methodPointerType)&Enumerator_get_Current_m19805_gshared/* 2488*/,
	(methodPointerType)&Enumerator_MoveNext_m19806_gshared/* 2489*/,
	(methodPointerType)&Enumerator_Dispose_m19807_gshared/* 2490*/,
	(methodPointerType)&Predicate_1_Invoke_m19808_gshared/* 2491*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19809_gshared/* 2492*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19810_gshared/* 2493*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19811_gshared/* 2494*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19812_gshared/* 2495*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19813_gshared/* 2496*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19814_gshared/* 2497*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19815_gshared/* 2498*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19816_gshared/* 2499*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19817_gshared/* 2500*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19818_gshared/* 2501*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19819_gshared/* 2502*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19820_gshared/* 2503*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19821_gshared/* 2504*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19822_gshared/* 2505*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19823_gshared/* 2506*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19824_gshared/* 2507*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19825_gshared/* 2508*/,
	(methodPointerType)&Dictionary_2__ctor_m20023_gshared/* 2509*/,
	(methodPointerType)&Dictionary_2__ctor_m20025_gshared/* 2510*/,
	(methodPointerType)&Dictionary_2__ctor_m20027_gshared/* 2511*/,
	(methodPointerType)&Dictionary_2__ctor_m20029_gshared/* 2512*/,
	(methodPointerType)&Dictionary_2__ctor_m20031_gshared/* 2513*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033_gshared/* 2514*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035_gshared/* 2515*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m20037_gshared/* 2516*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m20039_gshared/* 2517*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m20041_gshared/* 2518*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m20043_gshared/* 2519*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m20045_gshared/* 2520*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047_gshared/* 2521*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049_gshared/* 2522*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051_gshared/* 2523*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053_gshared/* 2524*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055_gshared/* 2525*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057_gshared/* 2526*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059_gshared/* 2527*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m20061_gshared/* 2528*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063_gshared/* 2529*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065_gshared/* 2530*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067_gshared/* 2531*/,
	(methodPointerType)&Dictionary_2_get_Count_m20069_gshared/* 2532*/,
	(methodPointerType)&Dictionary_2_get_Item_m20071_gshared/* 2533*/,
	(methodPointerType)&Dictionary_2_set_Item_m20073_gshared/* 2534*/,
	(methodPointerType)&Dictionary_2_Init_m20075_gshared/* 2535*/,
	(methodPointerType)&Dictionary_2_InitArrays_m20077_gshared/* 2536*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m20079_gshared/* 2537*/,
	(methodPointerType)&Dictionary_2_make_pair_m20081_gshared/* 2538*/,
	(methodPointerType)&Dictionary_2_pick_key_m20083_gshared/* 2539*/,
	(methodPointerType)&Dictionary_2_pick_value_m20085_gshared/* 2540*/,
	(methodPointerType)&Dictionary_2_CopyTo_m20087_gshared/* 2541*/,
	(methodPointerType)&Dictionary_2_Resize_m20089_gshared/* 2542*/,
	(methodPointerType)&Dictionary_2_Add_m20091_gshared/* 2543*/,
	(methodPointerType)&Dictionary_2_Clear_m20093_gshared/* 2544*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m20095_gshared/* 2545*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m20097_gshared/* 2546*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m20099_gshared/* 2547*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m20101_gshared/* 2548*/,
	(methodPointerType)&Dictionary_2_Remove_m20103_gshared/* 2549*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m20105_gshared/* 2550*/,
	(methodPointerType)&Dictionary_2_get_Keys_m20107_gshared/* 2551*/,
	(methodPointerType)&Dictionary_2_get_Values_m20109_gshared/* 2552*/,
	(methodPointerType)&Dictionary_2_ToTKey_m20111_gshared/* 2553*/,
	(methodPointerType)&Dictionary_2_ToTValue_m20113_gshared/* 2554*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m20115_gshared/* 2555*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m20117_gshared/* 2556*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m20119_gshared/* 2557*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20120_gshared/* 2558*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_gshared/* 2559*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20122_gshared/* 2560*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20123_gshared/* 2561*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20124_gshared/* 2562*/,
	(methodPointerType)&KeyValuePair_2__ctor_m20125_gshared/* 2563*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m20126_gshared/* 2564*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m20127_gshared/* 2565*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m20128_gshared/* 2566*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m20129_gshared/* 2567*/,
	(methodPointerType)&KeyValuePair_2_ToString_m20130_gshared/* 2568*/,
	(methodPointerType)&KeyCollection__ctor_m20131_gshared/* 2569*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20132_gshared/* 2570*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20133_gshared/* 2571*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20134_gshared/* 2572*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20135_gshared/* 2573*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20136_gshared/* 2574*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m20137_gshared/* 2575*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20138_gshared/* 2576*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20139_gshared/* 2577*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20140_gshared/* 2578*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m20141_gshared/* 2579*/,
	(methodPointerType)&KeyCollection_CopyTo_m20142_gshared/* 2580*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m20143_gshared/* 2581*/,
	(methodPointerType)&KeyCollection_get_Count_m20144_gshared/* 2582*/,
	(methodPointerType)&Enumerator__ctor_m20145_gshared/* 2583*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20146_gshared/* 2584*/,
	(methodPointerType)&Enumerator_Dispose_m20147_gshared/* 2585*/,
	(methodPointerType)&Enumerator_MoveNext_m20148_gshared/* 2586*/,
	(methodPointerType)&Enumerator_get_Current_m20149_gshared/* 2587*/,
	(methodPointerType)&Enumerator__ctor_m20150_gshared/* 2588*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20151_gshared/* 2589*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152_gshared/* 2590*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153_gshared/* 2591*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154_gshared/* 2592*/,
	(methodPointerType)&Enumerator_MoveNext_m20155_gshared/* 2593*/,
	(methodPointerType)&Enumerator_get_Current_m20156_gshared/* 2594*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m20157_gshared/* 2595*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m20158_gshared/* 2596*/,
	(methodPointerType)&Enumerator_VerifyState_m20159_gshared/* 2597*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m20160_gshared/* 2598*/,
	(methodPointerType)&Enumerator_Dispose_m20161_gshared/* 2599*/,
	(methodPointerType)&Transform_1__ctor_m20162_gshared/* 2600*/,
	(methodPointerType)&Transform_1_Invoke_m20163_gshared/* 2601*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20164_gshared/* 2602*/,
	(methodPointerType)&Transform_1_EndInvoke_m20165_gshared/* 2603*/,
	(methodPointerType)&ValueCollection__ctor_m20166_gshared/* 2604*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20167_gshared/* 2605*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20168_gshared/* 2606*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20169_gshared/* 2607*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20170_gshared/* 2608*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20171_gshared/* 2609*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m20172_gshared/* 2610*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20173_gshared/* 2611*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20174_gshared/* 2612*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20175_gshared/* 2613*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m20176_gshared/* 2614*/,
	(methodPointerType)&ValueCollection_CopyTo_m20177_gshared/* 2615*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m20178_gshared/* 2616*/,
	(methodPointerType)&ValueCollection_get_Count_m20179_gshared/* 2617*/,
	(methodPointerType)&Enumerator__ctor_m20180_gshared/* 2618*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20181_gshared/* 2619*/,
	(methodPointerType)&Enumerator_Dispose_m20182_gshared/* 2620*/,
	(methodPointerType)&Enumerator_MoveNext_m20183_gshared/* 2621*/,
	(methodPointerType)&Enumerator_get_Current_m20184_gshared/* 2622*/,
	(methodPointerType)&Transform_1__ctor_m20185_gshared/* 2623*/,
	(methodPointerType)&Transform_1_Invoke_m20186_gshared/* 2624*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20187_gshared/* 2625*/,
	(methodPointerType)&Transform_1_EndInvoke_m20188_gshared/* 2626*/,
	(methodPointerType)&Transform_1__ctor_m20189_gshared/* 2627*/,
	(methodPointerType)&Transform_1_Invoke_m20190_gshared/* 2628*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20191_gshared/* 2629*/,
	(methodPointerType)&Transform_1_EndInvoke_m20192_gshared/* 2630*/,
	(methodPointerType)&Transform_1__ctor_m20193_gshared/* 2631*/,
	(methodPointerType)&Transform_1_Invoke_m20194_gshared/* 2632*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20195_gshared/* 2633*/,
	(methodPointerType)&Transform_1_EndInvoke_m20196_gshared/* 2634*/,
	(methodPointerType)&ShimEnumerator__ctor_m20197_gshared/* 2635*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m20198_gshared/* 2636*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m20199_gshared/* 2637*/,
	(methodPointerType)&ShimEnumerator_get_Key_m20200_gshared/* 2638*/,
	(methodPointerType)&ShimEnumerator_get_Value_m20201_gshared/* 2639*/,
	(methodPointerType)&ShimEnumerator_get_Current_m20202_gshared/* 2640*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20203_gshared/* 2641*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20204_gshared/* 2642*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20205_gshared/* 2643*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20206_gshared/* 2644*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20207_gshared/* 2645*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m20208_gshared/* 2646*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m20209_gshared/* 2647*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m20210_gshared/* 2648*/,
	(methodPointerType)&DefaultComparer__ctor_m20211_gshared/* 2649*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20212_gshared/* 2650*/,
	(methodPointerType)&DefaultComparer_Equals_m20213_gshared/* 2651*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20264_gshared/* 2652*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20265_gshared/* 2653*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20266_gshared/* 2654*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20267_gshared/* 2655*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20268_gshared/* 2656*/,
	(methodPointerType)&Action_1__ctor_m21101_gshared/* 2657*/,
	(methodPointerType)&Action_1_Invoke_m21102_gshared/* 2658*/,
	(methodPointerType)&Action_1_BeginInvoke_m21103_gshared/* 2659*/,
	(methodPointerType)&Action_1_EndInvoke_m21104_gshared/* 2660*/,
	(methodPointerType)&Dictionary_2__ctor_m21797_gshared/* 2661*/,
	(methodPointerType)&Dictionary_2__ctor_m21798_gshared/* 2662*/,
	(methodPointerType)&Dictionary_2__ctor_m21799_gshared/* 2663*/,
	(methodPointerType)&Dictionary_2__ctor_m21800_gshared/* 2664*/,
	(methodPointerType)&Dictionary_2__ctor_m21801_gshared/* 2665*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21802_gshared/* 2666*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21803_gshared/* 2667*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21804_gshared/* 2668*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m21805_gshared/* 2669*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m21806_gshared/* 2670*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m21807_gshared/* 2671*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m21808_gshared/* 2672*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21809_gshared/* 2673*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21810_gshared/* 2674*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21811_gshared/* 2675*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21812_gshared/* 2676*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21813_gshared/* 2677*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared/* 2678*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815_gshared/* 2679*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m21816_gshared/* 2680*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21817_gshared/* 2681*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21818_gshared/* 2682*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21819_gshared/* 2683*/,
	(methodPointerType)&Dictionary_2_get_Count_m21820_gshared/* 2684*/,
	(methodPointerType)&Dictionary_2_get_Item_m21821_gshared/* 2685*/,
	(methodPointerType)&Dictionary_2_set_Item_m21822_gshared/* 2686*/,
	(methodPointerType)&Dictionary_2_Init_m21823_gshared/* 2687*/,
	(methodPointerType)&Dictionary_2_InitArrays_m21824_gshared/* 2688*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m21825_gshared/* 2689*/,
	(methodPointerType)&Dictionary_2_make_pair_m21826_gshared/* 2690*/,
	(methodPointerType)&Dictionary_2_pick_key_m21827_gshared/* 2691*/,
	(methodPointerType)&Dictionary_2_pick_value_m21828_gshared/* 2692*/,
	(methodPointerType)&Dictionary_2_CopyTo_m21829_gshared/* 2693*/,
	(methodPointerType)&Dictionary_2_Resize_m21830_gshared/* 2694*/,
	(methodPointerType)&Dictionary_2_Add_m21831_gshared/* 2695*/,
	(methodPointerType)&Dictionary_2_Clear_m21832_gshared/* 2696*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m21833_gshared/* 2697*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m21834_gshared/* 2698*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m21835_gshared/* 2699*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m21836_gshared/* 2700*/,
	(methodPointerType)&Dictionary_2_Remove_m21837_gshared/* 2701*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m21838_gshared/* 2702*/,
	(methodPointerType)&Dictionary_2_get_Keys_m21839_gshared/* 2703*/,
	(methodPointerType)&Dictionary_2_get_Values_m21840_gshared/* 2704*/,
	(methodPointerType)&Dictionary_2_ToTKey_m21841_gshared/* 2705*/,
	(methodPointerType)&Dictionary_2_ToTValue_m21842_gshared/* 2706*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m21843_gshared/* 2707*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m21844_gshared/* 2708*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m21845_gshared/* 2709*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21846_gshared/* 2710*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21847_gshared/* 2711*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21848_gshared/* 2712*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21849_gshared/* 2713*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21850_gshared/* 2714*/,
	(methodPointerType)&KeyValuePair_2__ctor_m21851_gshared/* 2715*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m21852_gshared/* 2716*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m21853_gshared/* 2717*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m21854_gshared/* 2718*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m21855_gshared/* 2719*/,
	(methodPointerType)&KeyValuePair_2_ToString_m21856_gshared/* 2720*/,
	(methodPointerType)&KeyCollection__ctor_m21857_gshared/* 2721*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21858_gshared/* 2722*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21859_gshared/* 2723*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21860_gshared/* 2724*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21861_gshared/* 2725*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21862_gshared/* 2726*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m21863_gshared/* 2727*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21864_gshared/* 2728*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21865_gshared/* 2729*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21866_gshared/* 2730*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m21867_gshared/* 2731*/,
	(methodPointerType)&KeyCollection_CopyTo_m21868_gshared/* 2732*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m21869_gshared/* 2733*/,
	(methodPointerType)&KeyCollection_get_Count_m21870_gshared/* 2734*/,
	(methodPointerType)&Enumerator__ctor_m21871_gshared/* 2735*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21872_gshared/* 2736*/,
	(methodPointerType)&Enumerator_Dispose_m21873_gshared/* 2737*/,
	(methodPointerType)&Enumerator_MoveNext_m21874_gshared/* 2738*/,
	(methodPointerType)&Enumerator_get_Current_m21875_gshared/* 2739*/,
	(methodPointerType)&Enumerator__ctor_m21876_gshared/* 2740*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21877_gshared/* 2741*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21878_gshared/* 2742*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21879_gshared/* 2743*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21880_gshared/* 2744*/,
	(methodPointerType)&Enumerator_MoveNext_m21881_gshared/* 2745*/,
	(methodPointerType)&Enumerator_get_Current_m21882_gshared/* 2746*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m21883_gshared/* 2747*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m21884_gshared/* 2748*/,
	(methodPointerType)&Enumerator_VerifyState_m21885_gshared/* 2749*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m21886_gshared/* 2750*/,
	(methodPointerType)&Enumerator_Dispose_m21887_gshared/* 2751*/,
	(methodPointerType)&Transform_1__ctor_m21888_gshared/* 2752*/,
	(methodPointerType)&Transform_1_Invoke_m21889_gshared/* 2753*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21890_gshared/* 2754*/,
	(methodPointerType)&Transform_1_EndInvoke_m21891_gshared/* 2755*/,
	(methodPointerType)&ValueCollection__ctor_m21892_gshared/* 2756*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21893_gshared/* 2757*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21894_gshared/* 2758*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21895_gshared/* 2759*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21896_gshared/* 2760*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21897_gshared/* 2761*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m21898_gshared/* 2762*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21899_gshared/* 2763*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21900_gshared/* 2764*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21901_gshared/* 2765*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m21902_gshared/* 2766*/,
	(methodPointerType)&ValueCollection_CopyTo_m21903_gshared/* 2767*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m21904_gshared/* 2768*/,
	(methodPointerType)&ValueCollection_get_Count_m21905_gshared/* 2769*/,
	(methodPointerType)&Enumerator__ctor_m21906_gshared/* 2770*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21907_gshared/* 2771*/,
	(methodPointerType)&Enumerator_Dispose_m21908_gshared/* 2772*/,
	(methodPointerType)&Enumerator_MoveNext_m21909_gshared/* 2773*/,
	(methodPointerType)&Enumerator_get_Current_m21910_gshared/* 2774*/,
	(methodPointerType)&Transform_1__ctor_m21911_gshared/* 2775*/,
	(methodPointerType)&Transform_1_Invoke_m21912_gshared/* 2776*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21913_gshared/* 2777*/,
	(methodPointerType)&Transform_1_EndInvoke_m21914_gshared/* 2778*/,
	(methodPointerType)&Transform_1__ctor_m21915_gshared/* 2779*/,
	(methodPointerType)&Transform_1_Invoke_m21916_gshared/* 2780*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21917_gshared/* 2781*/,
	(methodPointerType)&Transform_1_EndInvoke_m21918_gshared/* 2782*/,
	(methodPointerType)&Transform_1__ctor_m21919_gshared/* 2783*/,
	(methodPointerType)&Transform_1_Invoke_m21920_gshared/* 2784*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21921_gshared/* 2785*/,
	(methodPointerType)&Transform_1_EndInvoke_m21922_gshared/* 2786*/,
	(methodPointerType)&ShimEnumerator__ctor_m21923_gshared/* 2787*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m21924_gshared/* 2788*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m21925_gshared/* 2789*/,
	(methodPointerType)&ShimEnumerator_get_Key_m21926_gshared/* 2790*/,
	(methodPointerType)&ShimEnumerator_get_Value_m21927_gshared/* 2791*/,
	(methodPointerType)&ShimEnumerator_get_Current_m21928_gshared/* 2792*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21929_gshared/* 2793*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21930_gshared/* 2794*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21931_gshared/* 2795*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21932_gshared/* 2796*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21933_gshared/* 2797*/,
	(methodPointerType)&DefaultComparer__ctor_m21934_gshared/* 2798*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21935_gshared/* 2799*/,
	(methodPointerType)&DefaultComparer_Equals_m21936_gshared/* 2800*/,
	(methodPointerType)&Dictionary_2__ctor_m21937_gshared/* 2801*/,
	(methodPointerType)&Dictionary_2__ctor_m21938_gshared/* 2802*/,
	(methodPointerType)&Dictionary_2__ctor_m21939_gshared/* 2803*/,
	(methodPointerType)&Dictionary_2__ctor_m21940_gshared/* 2804*/,
	(methodPointerType)&Dictionary_2__ctor_m21941_gshared/* 2805*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21942_gshared/* 2806*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21943_gshared/* 2807*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21944_gshared/* 2808*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m21945_gshared/* 2809*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m21946_gshared/* 2810*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m21947_gshared/* 2811*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m21948_gshared/* 2812*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21949_gshared/* 2813*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21950_gshared/* 2814*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21951_gshared/* 2815*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21952_gshared/* 2816*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21953_gshared/* 2817*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21954_gshared/* 2818*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21955_gshared/* 2819*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m21956_gshared/* 2820*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21957_gshared/* 2821*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21958_gshared/* 2822*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21959_gshared/* 2823*/,
	(methodPointerType)&Dictionary_2_get_Count_m21960_gshared/* 2824*/,
	(methodPointerType)&Dictionary_2_get_Item_m21961_gshared/* 2825*/,
	(methodPointerType)&Dictionary_2_set_Item_m21962_gshared/* 2826*/,
	(methodPointerType)&Dictionary_2_Init_m21963_gshared/* 2827*/,
	(methodPointerType)&Dictionary_2_InitArrays_m21964_gshared/* 2828*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m21965_gshared/* 2829*/,
	(methodPointerType)&Dictionary_2_make_pair_m21966_gshared/* 2830*/,
	(methodPointerType)&Dictionary_2_pick_key_m21967_gshared/* 2831*/,
	(methodPointerType)&Dictionary_2_pick_value_m21968_gshared/* 2832*/,
	(methodPointerType)&Dictionary_2_CopyTo_m21969_gshared/* 2833*/,
	(methodPointerType)&Dictionary_2_Resize_m21970_gshared/* 2834*/,
	(methodPointerType)&Dictionary_2_Add_m21971_gshared/* 2835*/,
	(methodPointerType)&Dictionary_2_Clear_m21972_gshared/* 2836*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m21973_gshared/* 2837*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m21974_gshared/* 2838*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m21975_gshared/* 2839*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m21976_gshared/* 2840*/,
	(methodPointerType)&Dictionary_2_Remove_m21977_gshared/* 2841*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m21978_gshared/* 2842*/,
	(methodPointerType)&Dictionary_2_get_Keys_m21979_gshared/* 2843*/,
	(methodPointerType)&Dictionary_2_get_Values_m21980_gshared/* 2844*/,
	(methodPointerType)&Dictionary_2_ToTKey_m21981_gshared/* 2845*/,
	(methodPointerType)&Dictionary_2_ToTValue_m21982_gshared/* 2846*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m21983_gshared/* 2847*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m21984_gshared/* 2848*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m21985_gshared/* 2849*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21986_gshared/* 2850*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21987_gshared/* 2851*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21988_gshared/* 2852*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21989_gshared/* 2853*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21990_gshared/* 2854*/,
	(methodPointerType)&KeyValuePair_2__ctor_m21991_gshared/* 2855*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m21992_gshared/* 2856*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m21993_gshared/* 2857*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m21994_gshared/* 2858*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m21995_gshared/* 2859*/,
	(methodPointerType)&KeyValuePair_2_ToString_m21996_gshared/* 2860*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21997_gshared/* 2861*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21998_gshared/* 2862*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21999_gshared/* 2863*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22000_gshared/* 2864*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22001_gshared/* 2865*/,
	(methodPointerType)&KeyCollection__ctor_m22002_gshared/* 2866*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22003_gshared/* 2867*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22004_gshared/* 2868*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22005_gshared/* 2869*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22006_gshared/* 2870*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22007_gshared/* 2871*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22008_gshared/* 2872*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22009_gshared/* 2873*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22010_gshared/* 2874*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22011_gshared/* 2875*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22012_gshared/* 2876*/,
	(methodPointerType)&KeyCollection_CopyTo_m22013_gshared/* 2877*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22014_gshared/* 2878*/,
	(methodPointerType)&KeyCollection_get_Count_m22015_gshared/* 2879*/,
	(methodPointerType)&Enumerator__ctor_m22016_gshared/* 2880*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22017_gshared/* 2881*/,
	(methodPointerType)&Enumerator_Dispose_m22018_gshared/* 2882*/,
	(methodPointerType)&Enumerator_MoveNext_m22019_gshared/* 2883*/,
	(methodPointerType)&Enumerator_get_Current_m22020_gshared/* 2884*/,
	(methodPointerType)&Enumerator__ctor_m22021_gshared/* 2885*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22022_gshared/* 2886*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22023_gshared/* 2887*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22024_gshared/* 2888*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22025_gshared/* 2889*/,
	(methodPointerType)&Enumerator_MoveNext_m22026_gshared/* 2890*/,
	(methodPointerType)&Enumerator_get_Current_m22027_gshared/* 2891*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22028_gshared/* 2892*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22029_gshared/* 2893*/,
	(methodPointerType)&Enumerator_VerifyState_m22030_gshared/* 2894*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22031_gshared/* 2895*/,
	(methodPointerType)&Enumerator_Dispose_m22032_gshared/* 2896*/,
	(methodPointerType)&Transform_1__ctor_m22033_gshared/* 2897*/,
	(methodPointerType)&Transform_1_Invoke_m22034_gshared/* 2898*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22035_gshared/* 2899*/,
	(methodPointerType)&Transform_1_EndInvoke_m22036_gshared/* 2900*/,
	(methodPointerType)&ValueCollection__ctor_m22037_gshared/* 2901*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22038_gshared/* 2902*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22039_gshared/* 2903*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22040_gshared/* 2904*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22041_gshared/* 2905*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22042_gshared/* 2906*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22043_gshared/* 2907*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22044_gshared/* 2908*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22045_gshared/* 2909*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22046_gshared/* 2910*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22047_gshared/* 2911*/,
	(methodPointerType)&ValueCollection_CopyTo_m22048_gshared/* 2912*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22049_gshared/* 2913*/,
	(methodPointerType)&ValueCollection_get_Count_m22050_gshared/* 2914*/,
	(methodPointerType)&Enumerator__ctor_m22051_gshared/* 2915*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22052_gshared/* 2916*/,
	(methodPointerType)&Enumerator_Dispose_m22053_gshared/* 2917*/,
	(methodPointerType)&Enumerator_MoveNext_m22054_gshared/* 2918*/,
	(methodPointerType)&Enumerator_get_Current_m22055_gshared/* 2919*/,
	(methodPointerType)&Transform_1__ctor_m22056_gshared/* 2920*/,
	(methodPointerType)&Transform_1_Invoke_m22057_gshared/* 2921*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22058_gshared/* 2922*/,
	(methodPointerType)&Transform_1_EndInvoke_m22059_gshared/* 2923*/,
	(methodPointerType)&Transform_1__ctor_m22060_gshared/* 2924*/,
	(methodPointerType)&Transform_1_Invoke_m22061_gshared/* 2925*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22062_gshared/* 2926*/,
	(methodPointerType)&Transform_1_EndInvoke_m22063_gshared/* 2927*/,
	(methodPointerType)&Transform_1__ctor_m22064_gshared/* 2928*/,
	(methodPointerType)&Transform_1_Invoke_m22065_gshared/* 2929*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22066_gshared/* 2930*/,
	(methodPointerType)&Transform_1_EndInvoke_m22067_gshared/* 2931*/,
	(methodPointerType)&ShimEnumerator__ctor_m22068_gshared/* 2932*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22069_gshared/* 2933*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22070_gshared/* 2934*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22071_gshared/* 2935*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22072_gshared/* 2936*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22073_gshared/* 2937*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22074_gshared/* 2938*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22075_gshared/* 2939*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22076_gshared/* 2940*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22077_gshared/* 2941*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22078_gshared/* 2942*/,
	(methodPointerType)&DefaultComparer__ctor_m22079_gshared/* 2943*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22080_gshared/* 2944*/,
	(methodPointerType)&DefaultComparer_Equals_m22081_gshared/* 2945*/,
	(methodPointerType)&List_1__ctor_m22174_gshared/* 2946*/,
	(methodPointerType)&List_1__ctor_m22175_gshared/* 2947*/,
	(methodPointerType)&List_1__cctor_m22176_gshared/* 2948*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22177_gshared/* 2949*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m22178_gshared/* 2950*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m22179_gshared/* 2951*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m22180_gshared/* 2952*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m22181_gshared/* 2953*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m22182_gshared/* 2954*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m22183_gshared/* 2955*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m22184_gshared/* 2956*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22185_gshared/* 2957*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m22186_gshared/* 2958*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m22187_gshared/* 2959*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m22188_gshared/* 2960*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m22189_gshared/* 2961*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m22190_gshared/* 2962*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m22191_gshared/* 2963*/,
	(methodPointerType)&List_1_Add_m22192_gshared/* 2964*/,
	(methodPointerType)&List_1_GrowIfNeeded_m22193_gshared/* 2965*/,
	(methodPointerType)&List_1_AddCollection_m22194_gshared/* 2966*/,
	(methodPointerType)&List_1_AddEnumerable_m22195_gshared/* 2967*/,
	(methodPointerType)&List_1_AddRange_m22196_gshared/* 2968*/,
	(methodPointerType)&List_1_AsReadOnly_m22197_gshared/* 2969*/,
	(methodPointerType)&List_1_Clear_m22198_gshared/* 2970*/,
	(methodPointerType)&List_1_Contains_m22199_gshared/* 2971*/,
	(methodPointerType)&List_1_CopyTo_m22200_gshared/* 2972*/,
	(methodPointerType)&List_1_Find_m22201_gshared/* 2973*/,
	(methodPointerType)&List_1_CheckMatch_m22202_gshared/* 2974*/,
	(methodPointerType)&List_1_GetIndex_m22203_gshared/* 2975*/,
	(methodPointerType)&List_1_GetEnumerator_m22204_gshared/* 2976*/,
	(methodPointerType)&List_1_IndexOf_m22205_gshared/* 2977*/,
	(methodPointerType)&List_1_Shift_m22206_gshared/* 2978*/,
	(methodPointerType)&List_1_CheckIndex_m22207_gshared/* 2979*/,
	(methodPointerType)&List_1_Insert_m22208_gshared/* 2980*/,
	(methodPointerType)&List_1_CheckCollection_m22209_gshared/* 2981*/,
	(methodPointerType)&List_1_Remove_m22210_gshared/* 2982*/,
	(methodPointerType)&List_1_RemoveAll_m22211_gshared/* 2983*/,
	(methodPointerType)&List_1_RemoveAt_m22212_gshared/* 2984*/,
	(methodPointerType)&List_1_Reverse_m22213_gshared/* 2985*/,
	(methodPointerType)&List_1_Sort_m22214_gshared/* 2986*/,
	(methodPointerType)&List_1_Sort_m22215_gshared/* 2987*/,
	(methodPointerType)&List_1_ToArray_m22216_gshared/* 2988*/,
	(methodPointerType)&List_1_TrimExcess_m22217_gshared/* 2989*/,
	(methodPointerType)&List_1_get_Capacity_m22218_gshared/* 2990*/,
	(methodPointerType)&List_1_set_Capacity_m22219_gshared/* 2991*/,
	(methodPointerType)&List_1_get_Count_m22220_gshared/* 2992*/,
	(methodPointerType)&List_1_get_Item_m22221_gshared/* 2993*/,
	(methodPointerType)&List_1_set_Item_m22222_gshared/* 2994*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22223_gshared/* 2995*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22224_gshared/* 2996*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22225_gshared/* 2997*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22226_gshared/* 2998*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22227_gshared/* 2999*/,
	(methodPointerType)&Enumerator__ctor_m22228_gshared/* 3000*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22229_gshared/* 3001*/,
	(methodPointerType)&Enumerator_Dispose_m22230_gshared/* 3002*/,
	(methodPointerType)&Enumerator_VerifyState_m22231_gshared/* 3003*/,
	(methodPointerType)&Enumerator_MoveNext_m22232_gshared/* 3004*/,
	(methodPointerType)&Enumerator_get_Current_m22233_gshared/* 3005*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m22234_gshared/* 3006*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22235_gshared/* 3007*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22236_gshared/* 3008*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22237_gshared/* 3009*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22238_gshared/* 3010*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22239_gshared/* 3011*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22240_gshared/* 3012*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22241_gshared/* 3013*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22242_gshared/* 3014*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22243_gshared/* 3015*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22244_gshared/* 3016*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m22245_gshared/* 3017*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m22246_gshared/* 3018*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m22247_gshared/* 3019*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22248_gshared/* 3020*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m22249_gshared/* 3021*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m22250_gshared/* 3022*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22251_gshared/* 3023*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22252_gshared/* 3024*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22253_gshared/* 3025*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22254_gshared/* 3026*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22255_gshared/* 3027*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m22256_gshared/* 3028*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m22257_gshared/* 3029*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m22258_gshared/* 3030*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m22259_gshared/* 3031*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m22260_gshared/* 3032*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m22261_gshared/* 3033*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m22262_gshared/* 3034*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m22263_gshared/* 3035*/,
	(methodPointerType)&Collection_1__ctor_m22264_gshared/* 3036*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22265_gshared/* 3037*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m22266_gshared/* 3038*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m22267_gshared/* 3039*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m22268_gshared/* 3040*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m22269_gshared/* 3041*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m22270_gshared/* 3042*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m22271_gshared/* 3043*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m22272_gshared/* 3044*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m22273_gshared/* 3045*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m22274_gshared/* 3046*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m22275_gshared/* 3047*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m22276_gshared/* 3048*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m22277_gshared/* 3049*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m22278_gshared/* 3050*/,
	(methodPointerType)&Collection_1_Add_m22279_gshared/* 3051*/,
	(methodPointerType)&Collection_1_Clear_m22280_gshared/* 3052*/,
	(methodPointerType)&Collection_1_ClearItems_m22281_gshared/* 3053*/,
	(methodPointerType)&Collection_1_Contains_m22282_gshared/* 3054*/,
	(methodPointerType)&Collection_1_CopyTo_m22283_gshared/* 3055*/,
	(methodPointerType)&Collection_1_GetEnumerator_m22284_gshared/* 3056*/,
	(methodPointerType)&Collection_1_IndexOf_m22285_gshared/* 3057*/,
	(methodPointerType)&Collection_1_Insert_m22286_gshared/* 3058*/,
	(methodPointerType)&Collection_1_InsertItem_m22287_gshared/* 3059*/,
	(methodPointerType)&Collection_1_Remove_m22288_gshared/* 3060*/,
	(methodPointerType)&Collection_1_RemoveAt_m22289_gshared/* 3061*/,
	(methodPointerType)&Collection_1_RemoveItem_m22290_gshared/* 3062*/,
	(methodPointerType)&Collection_1_get_Count_m22291_gshared/* 3063*/,
	(methodPointerType)&Collection_1_get_Item_m22292_gshared/* 3064*/,
	(methodPointerType)&Collection_1_set_Item_m22293_gshared/* 3065*/,
	(methodPointerType)&Collection_1_SetItem_m22294_gshared/* 3066*/,
	(methodPointerType)&Collection_1_IsValidItem_m22295_gshared/* 3067*/,
	(methodPointerType)&Collection_1_ConvertItem_m22296_gshared/* 3068*/,
	(methodPointerType)&Collection_1_CheckWritable_m22297_gshared/* 3069*/,
	(methodPointerType)&Collection_1_IsSynchronized_m22298_gshared/* 3070*/,
	(methodPointerType)&Collection_1_IsFixedSize_m22299_gshared/* 3071*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22300_gshared/* 3072*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22301_gshared/* 3073*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22302_gshared/* 3074*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22303_gshared/* 3075*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22304_gshared/* 3076*/,
	(methodPointerType)&DefaultComparer__ctor_m22305_gshared/* 3077*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22306_gshared/* 3078*/,
	(methodPointerType)&DefaultComparer_Equals_m22307_gshared/* 3079*/,
	(methodPointerType)&Predicate_1__ctor_m22308_gshared/* 3080*/,
	(methodPointerType)&Predicate_1_Invoke_m22309_gshared/* 3081*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m22310_gshared/* 3082*/,
	(methodPointerType)&Predicate_1_EndInvoke_m22311_gshared/* 3083*/,
	(methodPointerType)&Comparer_1__ctor_m22312_gshared/* 3084*/,
	(methodPointerType)&Comparer_1__cctor_m22313_gshared/* 3085*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22314_gshared/* 3086*/,
	(methodPointerType)&Comparer_1_get_Default_m22315_gshared/* 3087*/,
	(methodPointerType)&DefaultComparer__ctor_m22316_gshared/* 3088*/,
	(methodPointerType)&DefaultComparer_Compare_m22317_gshared/* 3089*/,
	(methodPointerType)&Comparison_1__ctor_m22318_gshared/* 3090*/,
	(methodPointerType)&Comparison_1_Invoke_m22319_gshared/* 3091*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m22320_gshared/* 3092*/,
	(methodPointerType)&Comparison_1_EndInvoke_m22321_gshared/* 3093*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22422_gshared/* 3094*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22423_gshared/* 3095*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22424_gshared/* 3096*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22425_gshared/* 3097*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22426_gshared/* 3098*/,
	(methodPointerType)&Dictionary_2__ctor_m22428_gshared/* 3099*/,
	(methodPointerType)&Dictionary_2__ctor_m22430_gshared/* 3100*/,
	(methodPointerType)&Dictionary_2__ctor_m22432_gshared/* 3101*/,
	(methodPointerType)&Dictionary_2__ctor_m22434_gshared/* 3102*/,
	(methodPointerType)&Dictionary_2__ctor_m22436_gshared/* 3103*/,
	(methodPointerType)&Dictionary_2__ctor_m22438_gshared/* 3104*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440_gshared/* 3105*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442_gshared/* 3106*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m22444_gshared/* 3107*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22446_gshared/* 3108*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22448_gshared/* 3109*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m22450_gshared/* 3110*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22452_gshared/* 3111*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454_gshared/* 3112*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456_gshared/* 3113*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458_gshared/* 3114*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460_gshared/* 3115*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462_gshared/* 3116*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464_gshared/* 3117*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466_gshared/* 3118*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22468_gshared/* 3119*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470_gshared/* 3120*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472_gshared/* 3121*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474_gshared/* 3122*/,
	(methodPointerType)&Dictionary_2_get_Count_m22476_gshared/* 3123*/,
	(methodPointerType)&Dictionary_2_get_Item_m22478_gshared/* 3124*/,
	(methodPointerType)&Dictionary_2_set_Item_m22480_gshared/* 3125*/,
	(methodPointerType)&Dictionary_2_Init_m22482_gshared/* 3126*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22484_gshared/* 3127*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22486_gshared/* 3128*/,
	(methodPointerType)&Dictionary_2_make_pair_m22488_gshared/* 3129*/,
	(methodPointerType)&Dictionary_2_pick_key_m22490_gshared/* 3130*/,
	(methodPointerType)&Dictionary_2_pick_value_m22492_gshared/* 3131*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22494_gshared/* 3132*/,
	(methodPointerType)&Dictionary_2_Resize_m22496_gshared/* 3133*/,
	(methodPointerType)&Dictionary_2_Add_m22498_gshared/* 3134*/,
	(methodPointerType)&Dictionary_2_Clear_m22500_gshared/* 3135*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22502_gshared/* 3136*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22504_gshared/* 3137*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22506_gshared/* 3138*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22508_gshared/* 3139*/,
	(methodPointerType)&Dictionary_2_Remove_m22510_gshared/* 3140*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22512_gshared/* 3141*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22514_gshared/* 3142*/,
	(methodPointerType)&Dictionary_2_get_Values_m22516_gshared/* 3143*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22518_gshared/* 3144*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22520_gshared/* 3145*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22522_gshared/* 3146*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22524_gshared/* 3147*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22526_gshared/* 3148*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22527_gshared/* 3149*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22528_gshared/* 3150*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22529_gshared/* 3151*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22530_gshared/* 3152*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22531_gshared/* 3153*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22532_gshared/* 3154*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22533_gshared/* 3155*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22534_gshared/* 3156*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22535_gshared/* 3157*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22536_gshared/* 3158*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22537_gshared/* 3159*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22538_gshared/* 3160*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22539_gshared/* 3161*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22540_gshared/* 3162*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22541_gshared/* 3163*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22542_gshared/* 3164*/,
	(methodPointerType)&KeyCollection__ctor_m22543_gshared/* 3165*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22544_gshared/* 3166*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22545_gshared/* 3167*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22546_gshared/* 3168*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22547_gshared/* 3169*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22548_gshared/* 3170*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22549_gshared/* 3171*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22550_gshared/* 3172*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22551_gshared/* 3173*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22552_gshared/* 3174*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22553_gshared/* 3175*/,
	(methodPointerType)&KeyCollection_CopyTo_m22554_gshared/* 3176*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22555_gshared/* 3177*/,
	(methodPointerType)&KeyCollection_get_Count_m22556_gshared/* 3178*/,
	(methodPointerType)&Enumerator__ctor_m22557_gshared/* 3179*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22558_gshared/* 3180*/,
	(methodPointerType)&Enumerator_Dispose_m22559_gshared/* 3181*/,
	(methodPointerType)&Enumerator_MoveNext_m22560_gshared/* 3182*/,
	(methodPointerType)&Enumerator_get_Current_m22561_gshared/* 3183*/,
	(methodPointerType)&Enumerator__ctor_m22562_gshared/* 3184*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22563_gshared/* 3185*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564_gshared/* 3186*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565_gshared/* 3187*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566_gshared/* 3188*/,
	(methodPointerType)&Enumerator_MoveNext_m22567_gshared/* 3189*/,
	(methodPointerType)&Enumerator_get_Current_m22568_gshared/* 3190*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22569_gshared/* 3191*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22570_gshared/* 3192*/,
	(methodPointerType)&Enumerator_VerifyState_m22571_gshared/* 3193*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22572_gshared/* 3194*/,
	(methodPointerType)&Enumerator_Dispose_m22573_gshared/* 3195*/,
	(methodPointerType)&Transform_1__ctor_m22574_gshared/* 3196*/,
	(methodPointerType)&Transform_1_Invoke_m22575_gshared/* 3197*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22576_gshared/* 3198*/,
	(methodPointerType)&Transform_1_EndInvoke_m22577_gshared/* 3199*/,
	(methodPointerType)&ValueCollection__ctor_m22578_gshared/* 3200*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22579_gshared/* 3201*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22580_gshared/* 3202*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22581_gshared/* 3203*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22582_gshared/* 3204*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22583_gshared/* 3205*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22584_gshared/* 3206*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22585_gshared/* 3207*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22586_gshared/* 3208*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22587_gshared/* 3209*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22588_gshared/* 3210*/,
	(methodPointerType)&ValueCollection_CopyTo_m22589_gshared/* 3211*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22590_gshared/* 3212*/,
	(methodPointerType)&ValueCollection_get_Count_m22591_gshared/* 3213*/,
	(methodPointerType)&Enumerator__ctor_m22592_gshared/* 3214*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22593_gshared/* 3215*/,
	(methodPointerType)&Enumerator_Dispose_m22594_gshared/* 3216*/,
	(methodPointerType)&Enumerator_MoveNext_m22595_gshared/* 3217*/,
	(methodPointerType)&Enumerator_get_Current_m22596_gshared/* 3218*/,
	(methodPointerType)&Transform_1__ctor_m22597_gshared/* 3219*/,
	(methodPointerType)&Transform_1_Invoke_m22598_gshared/* 3220*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22599_gshared/* 3221*/,
	(methodPointerType)&Transform_1_EndInvoke_m22600_gshared/* 3222*/,
	(methodPointerType)&Transform_1__ctor_m22601_gshared/* 3223*/,
	(methodPointerType)&Transform_1_Invoke_m22602_gshared/* 3224*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22603_gshared/* 3225*/,
	(methodPointerType)&Transform_1_EndInvoke_m22604_gshared/* 3226*/,
	(methodPointerType)&Transform_1__ctor_m22605_gshared/* 3227*/,
	(methodPointerType)&Transform_1_Invoke_m22606_gshared/* 3228*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22607_gshared/* 3229*/,
	(methodPointerType)&Transform_1_EndInvoke_m22608_gshared/* 3230*/,
	(methodPointerType)&ShimEnumerator__ctor_m22609_gshared/* 3231*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22610_gshared/* 3232*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22611_gshared/* 3233*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22612_gshared/* 3234*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22613_gshared/* 3235*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22614_gshared/* 3236*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22615_gshared/* 3237*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22616_gshared/* 3238*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22617_gshared/* 3239*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22618_gshared/* 3240*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22619_gshared/* 3241*/,
	(methodPointerType)&DefaultComparer__ctor_m22620_gshared/* 3242*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22621_gshared/* 3243*/,
	(methodPointerType)&DefaultComparer_Equals_m22622_gshared/* 3244*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23184_gshared/* 3245*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23185_gshared/* 3246*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23186_gshared/* 3247*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23187_gshared/* 3248*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23188_gshared/* 3249*/,
	(methodPointerType)&TweenerCore_3__ctor_m23293_gshared/* 3250*/,
	(methodPointerType)&TweenerCore_3_Reset_m23294_gshared/* 3251*/,
	(methodPointerType)&TweenerCore_3_Validate_m23295_gshared/* 3252*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23296_gshared/* 3253*/,
	(methodPointerType)&TweenerCore_3_Startup_m23297_gshared/* 3254*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23298_gshared/* 3255*/,
	(methodPointerType)&DOGetter_1__ctor_m23299_gshared/* 3256*/,
	(methodPointerType)&DOGetter_1_Invoke_m23300_gshared/* 3257*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23301_gshared/* 3258*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23302_gshared/* 3259*/,
	(methodPointerType)&DOSetter_1__ctor_m23303_gshared/* 3260*/,
	(methodPointerType)&DOSetter_1_Invoke_m23304_gshared/* 3261*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23305_gshared/* 3262*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23306_gshared/* 3263*/,
	(methodPointerType)&TweenerCore_3__ctor_m23307_gshared/* 3264*/,
	(methodPointerType)&TweenerCore_3_Reset_m23308_gshared/* 3265*/,
	(methodPointerType)&TweenerCore_3_Validate_m23309_gshared/* 3266*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23310_gshared/* 3267*/,
	(methodPointerType)&TweenerCore_3_Startup_m23311_gshared/* 3268*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23312_gshared/* 3269*/,
	(methodPointerType)&DOGetter_1__ctor_m23313_gshared/* 3270*/,
	(methodPointerType)&DOGetter_1_Invoke_m23314_gshared/* 3271*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23315_gshared/* 3272*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23316_gshared/* 3273*/,
	(methodPointerType)&DOSetter_1__ctor_m23317_gshared/* 3274*/,
	(methodPointerType)&DOSetter_1_Invoke_m23318_gshared/* 3275*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23319_gshared/* 3276*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23320_gshared/* 3277*/,
	(methodPointerType)&TweenCallback_1__ctor_m23414_gshared/* 3278*/,
	(methodPointerType)&TweenCallback_1_Invoke_m23415_gshared/* 3279*/,
	(methodPointerType)&TweenCallback_1_BeginInvoke_m23416_gshared/* 3280*/,
	(methodPointerType)&TweenCallback_1_EndInvoke_m23417_gshared/* 3281*/,
	(methodPointerType)&TweenerCore_3__ctor_m23612_gshared/* 3282*/,
	(methodPointerType)&TweenerCore_3_Reset_m23613_gshared/* 3283*/,
	(methodPointerType)&TweenerCore_3_Validate_m23614_gshared/* 3284*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23615_gshared/* 3285*/,
	(methodPointerType)&TweenerCore_3_Startup_m23616_gshared/* 3286*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23617_gshared/* 3287*/,
	(methodPointerType)&TweenerCore_3__ctor_m23632_gshared/* 3288*/,
	(methodPointerType)&TweenerCore_3_Reset_m23633_gshared/* 3289*/,
	(methodPointerType)&TweenerCore_3_Validate_m23634_gshared/* 3290*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23635_gshared/* 3291*/,
	(methodPointerType)&TweenerCore_3_Startup_m23636_gshared/* 3292*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23637_gshared/* 3293*/,
	(methodPointerType)&DOGetter_1__ctor_m23638_gshared/* 3294*/,
	(methodPointerType)&DOGetter_1_Invoke_m23639_gshared/* 3295*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23640_gshared/* 3296*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23641_gshared/* 3297*/,
	(methodPointerType)&DOSetter_1__ctor_m23642_gshared/* 3298*/,
	(methodPointerType)&DOSetter_1_Invoke_m23643_gshared/* 3299*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23644_gshared/* 3300*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23645_gshared/* 3301*/,
	(methodPointerType)&TweenerCore_3__ctor_m23659_gshared/* 3302*/,
	(methodPointerType)&TweenerCore_3_Reset_m23660_gshared/* 3303*/,
	(methodPointerType)&TweenerCore_3_Validate_m23661_gshared/* 3304*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23662_gshared/* 3305*/,
	(methodPointerType)&TweenerCore_3_Startup_m23663_gshared/* 3306*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23664_gshared/* 3307*/,
	(methodPointerType)&DOGetter_1__ctor_m23665_gshared/* 3308*/,
	(methodPointerType)&DOGetter_1_Invoke_m23666_gshared/* 3309*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23667_gshared/* 3310*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23668_gshared/* 3311*/,
	(methodPointerType)&DOSetter_1__ctor_m23669_gshared/* 3312*/,
	(methodPointerType)&DOSetter_1_Invoke_m23670_gshared/* 3313*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23671_gshared/* 3314*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23672_gshared/* 3315*/,
	(methodPointerType)&TweenerCore_3__ctor_m23673_gshared/* 3316*/,
	(methodPointerType)&TweenerCore_3_Reset_m23674_gshared/* 3317*/,
	(methodPointerType)&TweenerCore_3_Validate_m23675_gshared/* 3318*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23676_gshared/* 3319*/,
	(methodPointerType)&TweenerCore_3_Startup_m23677_gshared/* 3320*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23678_gshared/* 3321*/,
	(methodPointerType)&DOGetter_1__ctor_m23679_gshared/* 3322*/,
	(methodPointerType)&DOGetter_1_Invoke_m23680_gshared/* 3323*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23681_gshared/* 3324*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23682_gshared/* 3325*/,
	(methodPointerType)&DOSetter_1__ctor_m23683_gshared/* 3326*/,
	(methodPointerType)&DOSetter_1_Invoke_m23684_gshared/* 3327*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23685_gshared/* 3328*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23686_gshared/* 3329*/,
	(methodPointerType)&TweenerCore_3__ctor_m23687_gshared/* 3330*/,
	(methodPointerType)&TweenerCore_3_Reset_m23688_gshared/* 3331*/,
	(methodPointerType)&TweenerCore_3_Validate_m23689_gshared/* 3332*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23690_gshared/* 3333*/,
	(methodPointerType)&TweenerCore_3_Startup_m23691_gshared/* 3334*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23692_gshared/* 3335*/,
	(methodPointerType)&DOGetter_1__ctor_m23693_gshared/* 3336*/,
	(methodPointerType)&DOGetter_1_Invoke_m23694_gshared/* 3337*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23695_gshared/* 3338*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23696_gshared/* 3339*/,
	(methodPointerType)&DOSetter_1__ctor_m23697_gshared/* 3340*/,
	(methodPointerType)&DOSetter_1_Invoke_m23698_gshared/* 3341*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23699_gshared/* 3342*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23700_gshared/* 3343*/,
	(methodPointerType)&TweenerCore_3__ctor_m23702_gshared/* 3344*/,
	(methodPointerType)&TweenerCore_3_Reset_m23703_gshared/* 3345*/,
	(methodPointerType)&TweenerCore_3_Validate_m23704_gshared/* 3346*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23705_gshared/* 3347*/,
	(methodPointerType)&TweenerCore_3_Startup_m23706_gshared/* 3348*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23707_gshared/* 3349*/,
	(methodPointerType)&List_1__ctor_m23724_gshared/* 3350*/,
	(methodPointerType)&List_1__ctor_m23726_gshared/* 3351*/,
	(methodPointerType)&List_1__cctor_m23728_gshared/* 3352*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730_gshared/* 3353*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m23732_gshared/* 3354*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m23734_gshared/* 3355*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m23736_gshared/* 3356*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m23738_gshared/* 3357*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m23740_gshared/* 3358*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m23742_gshared/* 3359*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m23744_gshared/* 3360*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746_gshared/* 3361*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m23748_gshared/* 3362*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m23750_gshared/* 3363*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m23752_gshared/* 3364*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m23754_gshared/* 3365*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m23756_gshared/* 3366*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m23758_gshared/* 3367*/,
	(methodPointerType)&List_1_Add_m23760_gshared/* 3368*/,
	(methodPointerType)&List_1_GrowIfNeeded_m23762_gshared/* 3369*/,
	(methodPointerType)&List_1_AddCollection_m23764_gshared/* 3370*/,
	(methodPointerType)&List_1_AddEnumerable_m23766_gshared/* 3371*/,
	(methodPointerType)&List_1_AddRange_m23768_gshared/* 3372*/,
	(methodPointerType)&List_1_AsReadOnly_m23770_gshared/* 3373*/,
	(methodPointerType)&List_1_Clear_m23772_gshared/* 3374*/,
	(methodPointerType)&List_1_Contains_m23774_gshared/* 3375*/,
	(methodPointerType)&List_1_CopyTo_m23776_gshared/* 3376*/,
	(methodPointerType)&List_1_Find_m23778_gshared/* 3377*/,
	(methodPointerType)&List_1_CheckMatch_m23780_gshared/* 3378*/,
	(methodPointerType)&List_1_GetIndex_m23782_gshared/* 3379*/,
	(methodPointerType)&List_1_GetEnumerator_m23784_gshared/* 3380*/,
	(methodPointerType)&List_1_IndexOf_m23786_gshared/* 3381*/,
	(methodPointerType)&List_1_Shift_m23788_gshared/* 3382*/,
	(methodPointerType)&List_1_CheckIndex_m23790_gshared/* 3383*/,
	(methodPointerType)&List_1_Insert_m23792_gshared/* 3384*/,
	(methodPointerType)&List_1_CheckCollection_m23794_gshared/* 3385*/,
	(methodPointerType)&List_1_Remove_m23796_gshared/* 3386*/,
	(methodPointerType)&List_1_RemoveAll_m23798_gshared/* 3387*/,
	(methodPointerType)&List_1_RemoveAt_m23800_gshared/* 3388*/,
	(methodPointerType)&List_1_Reverse_m23802_gshared/* 3389*/,
	(methodPointerType)&List_1_Sort_m23804_gshared/* 3390*/,
	(methodPointerType)&List_1_Sort_m23806_gshared/* 3391*/,
	(methodPointerType)&List_1_ToArray_m23808_gshared/* 3392*/,
	(methodPointerType)&List_1_TrimExcess_m23810_gshared/* 3393*/,
	(methodPointerType)&List_1_get_Capacity_m23812_gshared/* 3394*/,
	(methodPointerType)&List_1_set_Capacity_m23814_gshared/* 3395*/,
	(methodPointerType)&List_1_get_Count_m23816_gshared/* 3396*/,
	(methodPointerType)&List_1_get_Item_m23818_gshared/* 3397*/,
	(methodPointerType)&List_1_set_Item_m23820_gshared/* 3398*/,
	(methodPointerType)&Enumerator__ctor_m23821_gshared/* 3399*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m23822_gshared/* 3400*/,
	(methodPointerType)&Enumerator_Dispose_m23823_gshared/* 3401*/,
	(methodPointerType)&Enumerator_VerifyState_m23824_gshared/* 3402*/,
	(methodPointerType)&Enumerator_MoveNext_m23825_gshared/* 3403*/,
	(methodPointerType)&Enumerator_get_Current_m23826_gshared/* 3404*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m23827_gshared/* 3405*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23828_gshared/* 3406*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23829_gshared/* 3407*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23830_gshared/* 3408*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23831_gshared/* 3409*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23832_gshared/* 3410*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23833_gshared/* 3411*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23834_gshared/* 3412*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23835_gshared/* 3413*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23836_gshared/* 3414*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23837_gshared/* 3415*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m23838_gshared/* 3416*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m23839_gshared/* 3417*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m23840_gshared/* 3418*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23841_gshared/* 3419*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m23842_gshared/* 3420*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m23843_gshared/* 3421*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23844_gshared/* 3422*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23845_gshared/* 3423*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23846_gshared/* 3424*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23847_gshared/* 3425*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23848_gshared/* 3426*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m23849_gshared/* 3427*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m23850_gshared/* 3428*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m23851_gshared/* 3429*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m23852_gshared/* 3430*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m23853_gshared/* 3431*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m23854_gshared/* 3432*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m23855_gshared/* 3433*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m23856_gshared/* 3434*/,
	(methodPointerType)&Collection_1__ctor_m23857_gshared/* 3435*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23858_gshared/* 3436*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m23859_gshared/* 3437*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m23860_gshared/* 3438*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m23861_gshared/* 3439*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m23862_gshared/* 3440*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m23863_gshared/* 3441*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m23864_gshared/* 3442*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m23865_gshared/* 3443*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m23866_gshared/* 3444*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m23867_gshared/* 3445*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m23868_gshared/* 3446*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m23869_gshared/* 3447*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m23870_gshared/* 3448*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m23871_gshared/* 3449*/,
	(methodPointerType)&Collection_1_Add_m23872_gshared/* 3450*/,
	(methodPointerType)&Collection_1_Clear_m23873_gshared/* 3451*/,
	(methodPointerType)&Collection_1_ClearItems_m23874_gshared/* 3452*/,
	(methodPointerType)&Collection_1_Contains_m23875_gshared/* 3453*/,
	(methodPointerType)&Collection_1_CopyTo_m23876_gshared/* 3454*/,
	(methodPointerType)&Collection_1_GetEnumerator_m23877_gshared/* 3455*/,
	(methodPointerType)&Collection_1_IndexOf_m23878_gshared/* 3456*/,
	(methodPointerType)&Collection_1_Insert_m23879_gshared/* 3457*/,
	(methodPointerType)&Collection_1_InsertItem_m23880_gshared/* 3458*/,
	(methodPointerType)&Collection_1_Remove_m23881_gshared/* 3459*/,
	(methodPointerType)&Collection_1_RemoveAt_m23882_gshared/* 3460*/,
	(methodPointerType)&Collection_1_RemoveItem_m23883_gshared/* 3461*/,
	(methodPointerType)&Collection_1_get_Count_m23884_gshared/* 3462*/,
	(methodPointerType)&Collection_1_get_Item_m23885_gshared/* 3463*/,
	(methodPointerType)&Collection_1_set_Item_m23886_gshared/* 3464*/,
	(methodPointerType)&Collection_1_SetItem_m23887_gshared/* 3465*/,
	(methodPointerType)&Collection_1_IsValidItem_m23888_gshared/* 3466*/,
	(methodPointerType)&Collection_1_ConvertItem_m23889_gshared/* 3467*/,
	(methodPointerType)&Collection_1_CheckWritable_m23890_gshared/* 3468*/,
	(methodPointerType)&Collection_1_IsSynchronized_m23891_gshared/* 3469*/,
	(methodPointerType)&Collection_1_IsFixedSize_m23892_gshared/* 3470*/,
	(methodPointerType)&Predicate_1__ctor_m23893_gshared/* 3471*/,
	(methodPointerType)&Predicate_1_Invoke_m23894_gshared/* 3472*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m23895_gshared/* 3473*/,
	(methodPointerType)&Predicate_1_EndInvoke_m23896_gshared/* 3474*/,
	(methodPointerType)&Comparer_1__ctor_m23897_gshared/* 3475*/,
	(methodPointerType)&Comparer_1__cctor_m23898_gshared/* 3476*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m23899_gshared/* 3477*/,
	(methodPointerType)&Comparer_1_get_Default_m23900_gshared/* 3478*/,
	(methodPointerType)&GenericComparer_1__ctor_m23901_gshared/* 3479*/,
	(methodPointerType)&GenericComparer_1_Compare_m23902_gshared/* 3480*/,
	(methodPointerType)&DefaultComparer__ctor_m23903_gshared/* 3481*/,
	(methodPointerType)&DefaultComparer_Compare_m23904_gshared/* 3482*/,
	(methodPointerType)&Comparison_1__ctor_m23905_gshared/* 3483*/,
	(methodPointerType)&Comparison_1_Invoke_m23906_gshared/* 3484*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m23907_gshared/* 3485*/,
	(methodPointerType)&Comparison_1_EndInvoke_m23908_gshared/* 3486*/,
	(methodPointerType)&TweenerCore_3__ctor_m23953_gshared/* 3487*/,
	(methodPointerType)&TweenerCore_3_Reset_m23954_gshared/* 3488*/,
	(methodPointerType)&TweenerCore_3_Validate_m23955_gshared/* 3489*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23956_gshared/* 3490*/,
	(methodPointerType)&TweenerCore_3_Startup_m23957_gshared/* 3491*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23958_gshared/* 3492*/,
	(methodPointerType)&DOGetter_1__ctor_m23959_gshared/* 3493*/,
	(methodPointerType)&DOGetter_1_Invoke_m23960_gshared/* 3494*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23961_gshared/* 3495*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23962_gshared/* 3496*/,
	(methodPointerType)&DOSetter_1__ctor_m23963_gshared/* 3497*/,
	(methodPointerType)&DOSetter_1_Invoke_m23964_gshared/* 3498*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23965_gshared/* 3499*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23966_gshared/* 3500*/,
	(methodPointerType)&TweenerCore_3__ctor_m23967_gshared/* 3501*/,
	(methodPointerType)&TweenerCore_3_Reset_m23968_gshared/* 3502*/,
	(methodPointerType)&TweenerCore_3_Validate_m23969_gshared/* 3503*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23970_gshared/* 3504*/,
	(methodPointerType)&TweenerCore_3_Startup_m23971_gshared/* 3505*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23972_gshared/* 3506*/,
	(methodPointerType)&DOGetter_1__ctor_m23973_gshared/* 3507*/,
	(methodPointerType)&DOGetter_1_Invoke_m23974_gshared/* 3508*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23975_gshared/* 3509*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23976_gshared/* 3510*/,
	(methodPointerType)&DOSetter_1__ctor_m23977_gshared/* 3511*/,
	(methodPointerType)&DOSetter_1_Invoke_m23978_gshared/* 3512*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23979_gshared/* 3513*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23980_gshared/* 3514*/,
	(methodPointerType)&TweenerCore_3__ctor_m23981_gshared/* 3515*/,
	(methodPointerType)&TweenerCore_3_Reset_m23982_gshared/* 3516*/,
	(methodPointerType)&TweenerCore_3_Validate_m23983_gshared/* 3517*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23984_gshared/* 3518*/,
	(methodPointerType)&TweenerCore_3_Startup_m23985_gshared/* 3519*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m23986_gshared/* 3520*/,
	(methodPointerType)&DOGetter_1__ctor_m23987_gshared/* 3521*/,
	(methodPointerType)&DOGetter_1_Invoke_m23988_gshared/* 3522*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m23989_gshared/* 3523*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m23990_gshared/* 3524*/,
	(methodPointerType)&DOSetter_1__ctor_m23991_gshared/* 3525*/,
	(methodPointerType)&DOSetter_1_Invoke_m23992_gshared/* 3526*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m23993_gshared/* 3527*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m23994_gshared/* 3528*/,
	(methodPointerType)&TweenerCore_3__ctor_m23995_gshared/* 3529*/,
	(methodPointerType)&TweenerCore_3_Reset_m23996_gshared/* 3530*/,
	(methodPointerType)&TweenerCore_3_Validate_m23997_gshared/* 3531*/,
	(methodPointerType)&TweenerCore_3_UpdateDelay_m23998_gshared/* 3532*/,
	(methodPointerType)&TweenerCore_3_Startup_m23999_gshared/* 3533*/,
	(methodPointerType)&TweenerCore_3_ApplyTween_m24000_gshared/* 3534*/,
	(methodPointerType)&DOGetter_1__ctor_m24001_gshared/* 3535*/,
	(methodPointerType)&DOGetter_1_Invoke_m24002_gshared/* 3536*/,
	(methodPointerType)&DOGetter_1_BeginInvoke_m24003_gshared/* 3537*/,
	(methodPointerType)&DOGetter_1_EndInvoke_m24004_gshared/* 3538*/,
	(methodPointerType)&DOSetter_1__ctor_m24005_gshared/* 3539*/,
	(methodPointerType)&DOSetter_1_Invoke_m24006_gshared/* 3540*/,
	(methodPointerType)&DOSetter_1_BeginInvoke_m24007_gshared/* 3541*/,
	(methodPointerType)&DOSetter_1_EndInvoke_m24008_gshared/* 3542*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24145_gshared/* 3543*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24146_gshared/* 3544*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24147_gshared/* 3545*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24148_gshared/* 3546*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24149_gshared/* 3547*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24155_gshared/* 3548*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24156_gshared/* 3549*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24157_gshared/* 3550*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24158_gshared/* 3551*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24159_gshared/* 3552*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24757_gshared/* 3553*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24758_gshared/* 3554*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24759_gshared/* 3555*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24760_gshared/* 3556*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24761_gshared/* 3557*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24855_gshared/* 3558*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24856_gshared/* 3559*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24857_gshared/* 3560*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24858_gshared/* 3561*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24859_gshared/* 3562*/,
	(methodPointerType)&List_1__ctor_m24860_gshared/* 3563*/,
	(methodPointerType)&List_1__ctor_m24861_gshared/* 3564*/,
	(methodPointerType)&List_1__cctor_m24862_gshared/* 3565*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24863_gshared/* 3566*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m24864_gshared/* 3567*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m24865_gshared/* 3568*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m24866_gshared/* 3569*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m24867_gshared/* 3570*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m24868_gshared/* 3571*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m24869_gshared/* 3572*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m24870_gshared/* 3573*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24871_gshared/* 3574*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m24872_gshared/* 3575*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m24873_gshared/* 3576*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m24874_gshared/* 3577*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m24875_gshared/* 3578*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m24876_gshared/* 3579*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m24877_gshared/* 3580*/,
	(methodPointerType)&List_1_Add_m24878_gshared/* 3581*/,
	(methodPointerType)&List_1_GrowIfNeeded_m24879_gshared/* 3582*/,
	(methodPointerType)&List_1_AddCollection_m24880_gshared/* 3583*/,
	(methodPointerType)&List_1_AddEnumerable_m24881_gshared/* 3584*/,
	(methodPointerType)&List_1_AddRange_m24882_gshared/* 3585*/,
	(methodPointerType)&List_1_AsReadOnly_m24883_gshared/* 3586*/,
	(methodPointerType)&List_1_Clear_m24884_gshared/* 3587*/,
	(methodPointerType)&List_1_Contains_m24885_gshared/* 3588*/,
	(methodPointerType)&List_1_CopyTo_m24886_gshared/* 3589*/,
	(methodPointerType)&List_1_Find_m24887_gshared/* 3590*/,
	(methodPointerType)&List_1_CheckMatch_m24888_gshared/* 3591*/,
	(methodPointerType)&List_1_GetIndex_m24889_gshared/* 3592*/,
	(methodPointerType)&List_1_GetEnumerator_m24890_gshared/* 3593*/,
	(methodPointerType)&List_1_IndexOf_m24891_gshared/* 3594*/,
	(methodPointerType)&List_1_Shift_m24892_gshared/* 3595*/,
	(methodPointerType)&List_1_CheckIndex_m24893_gshared/* 3596*/,
	(methodPointerType)&List_1_Insert_m24894_gshared/* 3597*/,
	(methodPointerType)&List_1_CheckCollection_m24895_gshared/* 3598*/,
	(methodPointerType)&List_1_Remove_m24896_gshared/* 3599*/,
	(methodPointerType)&List_1_RemoveAll_m24897_gshared/* 3600*/,
	(methodPointerType)&List_1_RemoveAt_m24898_gshared/* 3601*/,
	(methodPointerType)&List_1_Reverse_m24899_gshared/* 3602*/,
	(methodPointerType)&List_1_Sort_m24900_gshared/* 3603*/,
	(methodPointerType)&List_1_Sort_m24901_gshared/* 3604*/,
	(methodPointerType)&List_1_ToArray_m24902_gshared/* 3605*/,
	(methodPointerType)&List_1_TrimExcess_m24903_gshared/* 3606*/,
	(methodPointerType)&List_1_get_Capacity_m24904_gshared/* 3607*/,
	(methodPointerType)&List_1_set_Capacity_m24905_gshared/* 3608*/,
	(methodPointerType)&List_1_get_Count_m24906_gshared/* 3609*/,
	(methodPointerType)&List_1_get_Item_m24907_gshared/* 3610*/,
	(methodPointerType)&List_1_set_Item_m24908_gshared/* 3611*/,
	(methodPointerType)&Enumerator__ctor_m24909_gshared/* 3612*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24910_gshared/* 3613*/,
	(methodPointerType)&Enumerator_Dispose_m24911_gshared/* 3614*/,
	(methodPointerType)&Enumerator_VerifyState_m24912_gshared/* 3615*/,
	(methodPointerType)&Enumerator_MoveNext_m24913_gshared/* 3616*/,
	(methodPointerType)&Enumerator_get_Current_m24914_gshared/* 3617*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m24915_gshared/* 3618*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24916_gshared/* 3619*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24917_gshared/* 3620*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24918_gshared/* 3621*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24919_gshared/* 3622*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24920_gshared/* 3623*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24921_gshared/* 3624*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24922_gshared/* 3625*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24923_gshared/* 3626*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24924_gshared/* 3627*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24925_gshared/* 3628*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m24926_gshared/* 3629*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m24927_gshared/* 3630*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m24928_gshared/* 3631*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24929_gshared/* 3632*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m24930_gshared/* 3633*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m24931_gshared/* 3634*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24932_gshared/* 3635*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24933_gshared/* 3636*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24934_gshared/* 3637*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24935_gshared/* 3638*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24936_gshared/* 3639*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m24937_gshared/* 3640*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m24938_gshared/* 3641*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m24939_gshared/* 3642*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m24940_gshared/* 3643*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m24941_gshared/* 3644*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m24942_gshared/* 3645*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m24943_gshared/* 3646*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m24944_gshared/* 3647*/,
	(methodPointerType)&Collection_1__ctor_m24945_gshared/* 3648*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24946_gshared/* 3649*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m24947_gshared/* 3650*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m24948_gshared/* 3651*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m24949_gshared/* 3652*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m24950_gshared/* 3653*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m24951_gshared/* 3654*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m24952_gshared/* 3655*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m24953_gshared/* 3656*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m24954_gshared/* 3657*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m24955_gshared/* 3658*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m24956_gshared/* 3659*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m24957_gshared/* 3660*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m24958_gshared/* 3661*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m24959_gshared/* 3662*/,
	(methodPointerType)&Collection_1_Add_m24960_gshared/* 3663*/,
	(methodPointerType)&Collection_1_Clear_m24961_gshared/* 3664*/,
	(methodPointerType)&Collection_1_ClearItems_m24962_gshared/* 3665*/,
	(methodPointerType)&Collection_1_Contains_m24963_gshared/* 3666*/,
	(methodPointerType)&Collection_1_CopyTo_m24964_gshared/* 3667*/,
	(methodPointerType)&Collection_1_GetEnumerator_m24965_gshared/* 3668*/,
	(methodPointerType)&Collection_1_IndexOf_m24966_gshared/* 3669*/,
	(methodPointerType)&Collection_1_Insert_m24967_gshared/* 3670*/,
	(methodPointerType)&Collection_1_InsertItem_m24968_gshared/* 3671*/,
	(methodPointerType)&Collection_1_Remove_m24969_gshared/* 3672*/,
	(methodPointerType)&Collection_1_RemoveAt_m24970_gshared/* 3673*/,
	(methodPointerType)&Collection_1_RemoveItem_m24971_gshared/* 3674*/,
	(methodPointerType)&Collection_1_get_Count_m24972_gshared/* 3675*/,
	(methodPointerType)&Collection_1_get_Item_m24973_gshared/* 3676*/,
	(methodPointerType)&Collection_1_set_Item_m24974_gshared/* 3677*/,
	(methodPointerType)&Collection_1_SetItem_m24975_gshared/* 3678*/,
	(methodPointerType)&Collection_1_IsValidItem_m24976_gshared/* 3679*/,
	(methodPointerType)&Collection_1_ConvertItem_m24977_gshared/* 3680*/,
	(methodPointerType)&Collection_1_CheckWritable_m24978_gshared/* 3681*/,
	(methodPointerType)&Collection_1_IsSynchronized_m24979_gshared/* 3682*/,
	(methodPointerType)&Collection_1_IsFixedSize_m24980_gshared/* 3683*/,
	(methodPointerType)&EqualityComparer_1__ctor_m24981_gshared/* 3684*/,
	(methodPointerType)&EqualityComparer_1__cctor_m24982_gshared/* 3685*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24983_gshared/* 3686*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24984_gshared/* 3687*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m24985_gshared/* 3688*/,
	(methodPointerType)&DefaultComparer__ctor_m24986_gshared/* 3689*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m24987_gshared/* 3690*/,
	(methodPointerType)&DefaultComparer_Equals_m24988_gshared/* 3691*/,
	(methodPointerType)&Predicate_1__ctor_m24989_gshared/* 3692*/,
	(methodPointerType)&Predicate_1_Invoke_m24990_gshared/* 3693*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m24991_gshared/* 3694*/,
	(methodPointerType)&Predicate_1_EndInvoke_m24992_gshared/* 3695*/,
	(methodPointerType)&Comparer_1__ctor_m24993_gshared/* 3696*/,
	(methodPointerType)&Comparer_1__cctor_m24994_gshared/* 3697*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m24995_gshared/* 3698*/,
	(methodPointerType)&Comparer_1_get_Default_m24996_gshared/* 3699*/,
	(methodPointerType)&DefaultComparer__ctor_m24997_gshared/* 3700*/,
	(methodPointerType)&DefaultComparer_Compare_m24998_gshared/* 3701*/,
	(methodPointerType)&Comparison_1__ctor_m24999_gshared/* 3702*/,
	(methodPointerType)&Comparison_1_Invoke_m25000_gshared/* 3703*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m25001_gshared/* 3704*/,
	(methodPointerType)&Comparison_1_EndInvoke_m25002_gshared/* 3705*/,
	(methodPointerType)&List_1__ctor_m25003_gshared/* 3706*/,
	(methodPointerType)&List_1__ctor_m25004_gshared/* 3707*/,
	(methodPointerType)&List_1__cctor_m25005_gshared/* 3708*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25006_gshared/* 3709*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m25007_gshared/* 3710*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m25008_gshared/* 3711*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m25009_gshared/* 3712*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m25010_gshared/* 3713*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m25011_gshared/* 3714*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m25012_gshared/* 3715*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m25013_gshared/* 3716*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25014_gshared/* 3717*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m25015_gshared/* 3718*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m25016_gshared/* 3719*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m25017_gshared/* 3720*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m25018_gshared/* 3721*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m25019_gshared/* 3722*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m25020_gshared/* 3723*/,
	(methodPointerType)&List_1_Add_m25021_gshared/* 3724*/,
	(methodPointerType)&List_1_GrowIfNeeded_m25022_gshared/* 3725*/,
	(methodPointerType)&List_1_AddCollection_m25023_gshared/* 3726*/,
	(methodPointerType)&List_1_AddEnumerable_m25024_gshared/* 3727*/,
	(methodPointerType)&List_1_AddRange_m25025_gshared/* 3728*/,
	(methodPointerType)&List_1_AsReadOnly_m25026_gshared/* 3729*/,
	(methodPointerType)&List_1_Clear_m25027_gshared/* 3730*/,
	(methodPointerType)&List_1_Contains_m25028_gshared/* 3731*/,
	(methodPointerType)&List_1_CopyTo_m25029_gshared/* 3732*/,
	(methodPointerType)&List_1_Find_m25030_gshared/* 3733*/,
	(methodPointerType)&List_1_CheckMatch_m25031_gshared/* 3734*/,
	(methodPointerType)&List_1_GetIndex_m25032_gshared/* 3735*/,
	(methodPointerType)&List_1_GetEnumerator_m25033_gshared/* 3736*/,
	(methodPointerType)&List_1_IndexOf_m25034_gshared/* 3737*/,
	(methodPointerType)&List_1_Shift_m25035_gshared/* 3738*/,
	(methodPointerType)&List_1_CheckIndex_m25036_gshared/* 3739*/,
	(methodPointerType)&List_1_Insert_m25037_gshared/* 3740*/,
	(methodPointerType)&List_1_CheckCollection_m25038_gshared/* 3741*/,
	(methodPointerType)&List_1_Remove_m25039_gshared/* 3742*/,
	(methodPointerType)&List_1_RemoveAll_m25040_gshared/* 3743*/,
	(methodPointerType)&List_1_RemoveAt_m25041_gshared/* 3744*/,
	(methodPointerType)&List_1_Reverse_m25042_gshared/* 3745*/,
	(methodPointerType)&List_1_Sort_m25043_gshared/* 3746*/,
	(methodPointerType)&List_1_Sort_m25044_gshared/* 3747*/,
	(methodPointerType)&List_1_ToArray_m25045_gshared/* 3748*/,
	(methodPointerType)&List_1_TrimExcess_m25046_gshared/* 3749*/,
	(methodPointerType)&List_1_get_Capacity_m25047_gshared/* 3750*/,
	(methodPointerType)&List_1_set_Capacity_m25048_gshared/* 3751*/,
	(methodPointerType)&List_1_get_Count_m25049_gshared/* 3752*/,
	(methodPointerType)&List_1_get_Item_m25050_gshared/* 3753*/,
	(methodPointerType)&List_1_set_Item_m25051_gshared/* 3754*/,
	(methodPointerType)&Enumerator__ctor_m25052_gshared/* 3755*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25053_gshared/* 3756*/,
	(methodPointerType)&Enumerator_Dispose_m25054_gshared/* 3757*/,
	(methodPointerType)&Enumerator_VerifyState_m25055_gshared/* 3758*/,
	(methodPointerType)&Enumerator_MoveNext_m25056_gshared/* 3759*/,
	(methodPointerType)&Enumerator_get_Current_m25057_gshared/* 3760*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m25058_gshared/* 3761*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25059_gshared/* 3762*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25060_gshared/* 3763*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25061_gshared/* 3764*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25062_gshared/* 3765*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25063_gshared/* 3766*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25064_gshared/* 3767*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25065_gshared/* 3768*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25066_gshared/* 3769*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25067_gshared/* 3770*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25068_gshared/* 3771*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m25069_gshared/* 3772*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m25070_gshared/* 3773*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m25071_gshared/* 3774*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25072_gshared/* 3775*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m25073_gshared/* 3776*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m25074_gshared/* 3777*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25075_gshared/* 3778*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25076_gshared/* 3779*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25077_gshared/* 3780*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25078_gshared/* 3781*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25079_gshared/* 3782*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m25080_gshared/* 3783*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m25081_gshared/* 3784*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m25082_gshared/* 3785*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m25083_gshared/* 3786*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m25084_gshared/* 3787*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m25085_gshared/* 3788*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m25086_gshared/* 3789*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m25087_gshared/* 3790*/,
	(methodPointerType)&Collection_1__ctor_m25088_gshared/* 3791*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25089_gshared/* 3792*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m25090_gshared/* 3793*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m25091_gshared/* 3794*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m25092_gshared/* 3795*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m25093_gshared/* 3796*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m25094_gshared/* 3797*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m25095_gshared/* 3798*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m25096_gshared/* 3799*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m25097_gshared/* 3800*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m25098_gshared/* 3801*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m25099_gshared/* 3802*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m25100_gshared/* 3803*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m25101_gshared/* 3804*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m25102_gshared/* 3805*/,
	(methodPointerType)&Collection_1_Add_m25103_gshared/* 3806*/,
	(methodPointerType)&Collection_1_Clear_m25104_gshared/* 3807*/,
	(methodPointerType)&Collection_1_ClearItems_m25105_gshared/* 3808*/,
	(methodPointerType)&Collection_1_Contains_m25106_gshared/* 3809*/,
	(methodPointerType)&Collection_1_CopyTo_m25107_gshared/* 3810*/,
	(methodPointerType)&Collection_1_GetEnumerator_m25108_gshared/* 3811*/,
	(methodPointerType)&Collection_1_IndexOf_m25109_gshared/* 3812*/,
	(methodPointerType)&Collection_1_Insert_m25110_gshared/* 3813*/,
	(methodPointerType)&Collection_1_InsertItem_m25111_gshared/* 3814*/,
	(methodPointerType)&Collection_1_Remove_m25112_gshared/* 3815*/,
	(methodPointerType)&Collection_1_RemoveAt_m25113_gshared/* 3816*/,
	(methodPointerType)&Collection_1_RemoveItem_m25114_gshared/* 3817*/,
	(methodPointerType)&Collection_1_get_Count_m25115_gshared/* 3818*/,
	(methodPointerType)&Collection_1_get_Item_m25116_gshared/* 3819*/,
	(methodPointerType)&Collection_1_set_Item_m25117_gshared/* 3820*/,
	(methodPointerType)&Collection_1_SetItem_m25118_gshared/* 3821*/,
	(methodPointerType)&Collection_1_IsValidItem_m25119_gshared/* 3822*/,
	(methodPointerType)&Collection_1_ConvertItem_m25120_gshared/* 3823*/,
	(methodPointerType)&Collection_1_CheckWritable_m25121_gshared/* 3824*/,
	(methodPointerType)&Collection_1_IsSynchronized_m25122_gshared/* 3825*/,
	(methodPointerType)&Collection_1_IsFixedSize_m25123_gshared/* 3826*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25124_gshared/* 3827*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25125_gshared/* 3828*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25126_gshared/* 3829*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25127_gshared/* 3830*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25128_gshared/* 3831*/,
	(methodPointerType)&DefaultComparer__ctor_m25129_gshared/* 3832*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25130_gshared/* 3833*/,
	(methodPointerType)&DefaultComparer_Equals_m25131_gshared/* 3834*/,
	(methodPointerType)&Predicate_1__ctor_m25132_gshared/* 3835*/,
	(methodPointerType)&Predicate_1_Invoke_m25133_gshared/* 3836*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m25134_gshared/* 3837*/,
	(methodPointerType)&Predicate_1_EndInvoke_m25135_gshared/* 3838*/,
	(methodPointerType)&Comparer_1__ctor_m25136_gshared/* 3839*/,
	(methodPointerType)&Comparer_1__cctor_m25137_gshared/* 3840*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m25138_gshared/* 3841*/,
	(methodPointerType)&Comparer_1_get_Default_m25139_gshared/* 3842*/,
	(methodPointerType)&DefaultComparer__ctor_m25140_gshared/* 3843*/,
	(methodPointerType)&DefaultComparer_Compare_m25141_gshared/* 3844*/,
	(methodPointerType)&Comparison_1__ctor_m25142_gshared/* 3845*/,
	(methodPointerType)&Comparison_1_Invoke_m25143_gshared/* 3846*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m25144_gshared/* 3847*/,
	(methodPointerType)&Comparison_1_EndInvoke_m25145_gshared/* 3848*/,
	(methodPointerType)&Dictionary_2__ctor_m25147_gshared/* 3849*/,
	(methodPointerType)&Dictionary_2__ctor_m25149_gshared/* 3850*/,
	(methodPointerType)&Dictionary_2__ctor_m25151_gshared/* 3851*/,
	(methodPointerType)&Dictionary_2__ctor_m25153_gshared/* 3852*/,
	(methodPointerType)&Dictionary_2__ctor_m25155_gshared/* 3853*/,
	(methodPointerType)&Dictionary_2__ctor_m25157_gshared/* 3854*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25159_gshared/* 3855*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25161_gshared/* 3856*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25163_gshared/* 3857*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25165_gshared/* 3858*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25167_gshared/* 3859*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m25169_gshared/* 3860*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25171_gshared/* 3861*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25173_gshared/* 3862*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25175_gshared/* 3863*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25177_gshared/* 3864*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25179_gshared/* 3865*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25181_gshared/* 3866*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25183_gshared/* 3867*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25185_gshared/* 3868*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25187_gshared/* 3869*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25189_gshared/* 3870*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25191_gshared/* 3871*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25193_gshared/* 3872*/,
	(methodPointerType)&Dictionary_2_get_Count_m25195_gshared/* 3873*/,
	(methodPointerType)&Dictionary_2_get_Item_m25197_gshared/* 3874*/,
	(methodPointerType)&Dictionary_2_set_Item_m25199_gshared/* 3875*/,
	(methodPointerType)&Dictionary_2_Init_m25201_gshared/* 3876*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25203_gshared/* 3877*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25205_gshared/* 3878*/,
	(methodPointerType)&Dictionary_2_make_pair_m25207_gshared/* 3879*/,
	(methodPointerType)&Dictionary_2_pick_key_m25209_gshared/* 3880*/,
	(methodPointerType)&Dictionary_2_pick_value_m25211_gshared/* 3881*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25213_gshared/* 3882*/,
	(methodPointerType)&Dictionary_2_Resize_m25215_gshared/* 3883*/,
	(methodPointerType)&Dictionary_2_Add_m25217_gshared/* 3884*/,
	(methodPointerType)&Dictionary_2_Clear_m25219_gshared/* 3885*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25221_gshared/* 3886*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25223_gshared/* 3887*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25225_gshared/* 3888*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25227_gshared/* 3889*/,
	(methodPointerType)&Dictionary_2_Remove_m25229_gshared/* 3890*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25231_gshared/* 3891*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25233_gshared/* 3892*/,
	(methodPointerType)&Dictionary_2_get_Values_m25235_gshared/* 3893*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25237_gshared/* 3894*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25239_gshared/* 3895*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25241_gshared/* 3896*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25243_gshared/* 3897*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25245_gshared/* 3898*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25246_gshared/* 3899*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25247_gshared/* 3900*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25248_gshared/* 3901*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25249_gshared/* 3902*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25250_gshared/* 3903*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25251_gshared/* 3904*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25252_gshared/* 3905*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25253_gshared/* 3906*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25254_gshared/* 3907*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25255_gshared/* 3908*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25256_gshared/* 3909*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25257_gshared/* 3910*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25258_gshared/* 3911*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25259_gshared/* 3912*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25260_gshared/* 3913*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25261_gshared/* 3914*/,
	(methodPointerType)&KeyCollection__ctor_m25262_gshared/* 3915*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25263_gshared/* 3916*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25264_gshared/* 3917*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25265_gshared/* 3918*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25266_gshared/* 3919*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25267_gshared/* 3920*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25268_gshared/* 3921*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25269_gshared/* 3922*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25270_gshared/* 3923*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25271_gshared/* 3924*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25272_gshared/* 3925*/,
	(methodPointerType)&KeyCollection_CopyTo_m25273_gshared/* 3926*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25274_gshared/* 3927*/,
	(methodPointerType)&KeyCollection_get_Count_m25275_gshared/* 3928*/,
	(methodPointerType)&Enumerator__ctor_m25276_gshared/* 3929*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25277_gshared/* 3930*/,
	(methodPointerType)&Enumerator_Dispose_m25278_gshared/* 3931*/,
	(methodPointerType)&Enumerator_MoveNext_m25279_gshared/* 3932*/,
	(methodPointerType)&Enumerator_get_Current_m25280_gshared/* 3933*/,
	(methodPointerType)&Enumerator__ctor_m25281_gshared/* 3934*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25282_gshared/* 3935*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25283_gshared/* 3936*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25284_gshared/* 3937*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25285_gshared/* 3938*/,
	(methodPointerType)&Enumerator_MoveNext_m25286_gshared/* 3939*/,
	(methodPointerType)&Enumerator_get_Current_m25287_gshared/* 3940*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m25288_gshared/* 3941*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m25289_gshared/* 3942*/,
	(methodPointerType)&Enumerator_VerifyState_m25290_gshared/* 3943*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m25291_gshared/* 3944*/,
	(methodPointerType)&Enumerator_Dispose_m25292_gshared/* 3945*/,
	(methodPointerType)&Transform_1__ctor_m25293_gshared/* 3946*/,
	(methodPointerType)&Transform_1_Invoke_m25294_gshared/* 3947*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25295_gshared/* 3948*/,
	(methodPointerType)&Transform_1_EndInvoke_m25296_gshared/* 3949*/,
	(methodPointerType)&ValueCollection__ctor_m25297_gshared/* 3950*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25298_gshared/* 3951*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25299_gshared/* 3952*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25300_gshared/* 3953*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25301_gshared/* 3954*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25302_gshared/* 3955*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m25303_gshared/* 3956*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25304_gshared/* 3957*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25305_gshared/* 3958*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25306_gshared/* 3959*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m25307_gshared/* 3960*/,
	(methodPointerType)&ValueCollection_CopyTo_m25308_gshared/* 3961*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m25309_gshared/* 3962*/,
	(methodPointerType)&ValueCollection_get_Count_m25310_gshared/* 3963*/,
	(methodPointerType)&Enumerator__ctor_m25311_gshared/* 3964*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25312_gshared/* 3965*/,
	(methodPointerType)&Enumerator_Dispose_m25313_gshared/* 3966*/,
	(methodPointerType)&Enumerator_MoveNext_m25314_gshared/* 3967*/,
	(methodPointerType)&Enumerator_get_Current_m25315_gshared/* 3968*/,
	(methodPointerType)&Transform_1__ctor_m25316_gshared/* 3969*/,
	(methodPointerType)&Transform_1_Invoke_m25317_gshared/* 3970*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25318_gshared/* 3971*/,
	(methodPointerType)&Transform_1_EndInvoke_m25319_gshared/* 3972*/,
	(methodPointerType)&Transform_1__ctor_m25320_gshared/* 3973*/,
	(methodPointerType)&Transform_1_Invoke_m25321_gshared/* 3974*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25322_gshared/* 3975*/,
	(methodPointerType)&Transform_1_EndInvoke_m25323_gshared/* 3976*/,
	(methodPointerType)&Transform_1__ctor_m25324_gshared/* 3977*/,
	(methodPointerType)&Transform_1_Invoke_m25325_gshared/* 3978*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25326_gshared/* 3979*/,
	(methodPointerType)&Transform_1_EndInvoke_m25327_gshared/* 3980*/,
	(methodPointerType)&ShimEnumerator__ctor_m25328_gshared/* 3981*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m25329_gshared/* 3982*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m25330_gshared/* 3983*/,
	(methodPointerType)&ShimEnumerator_get_Key_m25331_gshared/* 3984*/,
	(methodPointerType)&ShimEnumerator_get_Value_m25332_gshared/* 3985*/,
	(methodPointerType)&ShimEnumerator_get_Current_m25333_gshared/* 3986*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25334_gshared/* 3987*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25335_gshared/* 3988*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25336_gshared/* 3989*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25337_gshared/* 3990*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25338_gshared/* 3991*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25339_gshared/* 3992*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25340_gshared/* 3993*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25341_gshared/* 3994*/,
	(methodPointerType)&DefaultComparer__ctor_m25342_gshared/* 3995*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25343_gshared/* 3996*/,
	(methodPointerType)&DefaultComparer_Equals_m25344_gshared/* 3997*/,
	(methodPointerType)&Dictionary_2__ctor_m25585_gshared/* 3998*/,
	(methodPointerType)&Dictionary_2__ctor_m25587_gshared/* 3999*/,
	(methodPointerType)&Dictionary_2__ctor_m25589_gshared/* 4000*/,
	(methodPointerType)&Dictionary_2__ctor_m25591_gshared/* 4001*/,
	(methodPointerType)&Dictionary_2__ctor_m25593_gshared/* 4002*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25595_gshared/* 4003*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25597_gshared/* 4004*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25599_gshared/* 4005*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25601_gshared/* 4006*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25603_gshared/* 4007*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m25605_gshared/* 4008*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25607_gshared/* 4009*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25609_gshared/* 4010*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25611_gshared/* 4011*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25613_gshared/* 4012*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25615_gshared/* 4013*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25617_gshared/* 4014*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25619_gshared/* 4015*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25621_gshared/* 4016*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25623_gshared/* 4017*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25625_gshared/* 4018*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25627_gshared/* 4019*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25629_gshared/* 4020*/,
	(methodPointerType)&Dictionary_2_get_Count_m25631_gshared/* 4021*/,
	(methodPointerType)&Dictionary_2_get_Item_m25633_gshared/* 4022*/,
	(methodPointerType)&Dictionary_2_set_Item_m25635_gshared/* 4023*/,
	(methodPointerType)&Dictionary_2_Init_m25637_gshared/* 4024*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25639_gshared/* 4025*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25641_gshared/* 4026*/,
	(methodPointerType)&Dictionary_2_make_pair_m25643_gshared/* 4027*/,
	(methodPointerType)&Dictionary_2_pick_key_m25645_gshared/* 4028*/,
	(methodPointerType)&Dictionary_2_pick_value_m25647_gshared/* 4029*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25649_gshared/* 4030*/,
	(methodPointerType)&Dictionary_2_Resize_m25651_gshared/* 4031*/,
	(methodPointerType)&Dictionary_2_Add_m25653_gshared/* 4032*/,
	(methodPointerType)&Dictionary_2_Clear_m25655_gshared/* 4033*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25657_gshared/* 4034*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25659_gshared/* 4035*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25661_gshared/* 4036*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25663_gshared/* 4037*/,
	(methodPointerType)&Dictionary_2_Remove_m25665_gshared/* 4038*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25667_gshared/* 4039*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25669_gshared/* 4040*/,
	(methodPointerType)&Dictionary_2_get_Values_m25671_gshared/* 4041*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25673_gshared/* 4042*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25675_gshared/* 4043*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25677_gshared/* 4044*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25679_gshared/* 4045*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25681_gshared/* 4046*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25682_gshared/* 4047*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25683_gshared/* 4048*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25684_gshared/* 4049*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25685_gshared/* 4050*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25686_gshared/* 4051*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25687_gshared/* 4052*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25688_gshared/* 4053*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25689_gshared/* 4054*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25690_gshared/* 4055*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25691_gshared/* 4056*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25692_gshared/* 4057*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25693_gshared/* 4058*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25694_gshared/* 4059*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25695_gshared/* 4060*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25696_gshared/* 4061*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25697_gshared/* 4062*/,
	(methodPointerType)&KeyCollection__ctor_m25698_gshared/* 4063*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25699_gshared/* 4064*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25700_gshared/* 4065*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25701_gshared/* 4066*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25702_gshared/* 4067*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25703_gshared/* 4068*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25704_gshared/* 4069*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25705_gshared/* 4070*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25706_gshared/* 4071*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25707_gshared/* 4072*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25708_gshared/* 4073*/,
	(methodPointerType)&KeyCollection_CopyTo_m25709_gshared/* 4074*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25710_gshared/* 4075*/,
	(methodPointerType)&KeyCollection_get_Count_m25711_gshared/* 4076*/,
	(methodPointerType)&Enumerator__ctor_m25712_gshared/* 4077*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25713_gshared/* 4078*/,
	(methodPointerType)&Enumerator_Dispose_m25714_gshared/* 4079*/,
	(methodPointerType)&Enumerator_MoveNext_m25715_gshared/* 4080*/,
	(methodPointerType)&Enumerator_get_Current_m25716_gshared/* 4081*/,
	(methodPointerType)&Enumerator__ctor_m25717_gshared/* 4082*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25718_gshared/* 4083*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719_gshared/* 4084*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720_gshared/* 4085*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721_gshared/* 4086*/,
	(methodPointerType)&Enumerator_MoveNext_m25722_gshared/* 4087*/,
	(methodPointerType)&Enumerator_get_Current_m25723_gshared/* 4088*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m25724_gshared/* 4089*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m25725_gshared/* 4090*/,
	(methodPointerType)&Enumerator_VerifyState_m25726_gshared/* 4091*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m25727_gshared/* 4092*/,
	(methodPointerType)&Enumerator_Dispose_m25728_gshared/* 4093*/,
	(methodPointerType)&Transform_1__ctor_m25729_gshared/* 4094*/,
	(methodPointerType)&Transform_1_Invoke_m25730_gshared/* 4095*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25731_gshared/* 4096*/,
	(methodPointerType)&Transform_1_EndInvoke_m25732_gshared/* 4097*/,
	(methodPointerType)&ValueCollection__ctor_m25733_gshared/* 4098*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25734_gshared/* 4099*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25735_gshared/* 4100*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25736_gshared/* 4101*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25737_gshared/* 4102*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25738_gshared/* 4103*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m25739_gshared/* 4104*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25740_gshared/* 4105*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25741_gshared/* 4106*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25742_gshared/* 4107*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m25743_gshared/* 4108*/,
	(methodPointerType)&ValueCollection_CopyTo_m25744_gshared/* 4109*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m25745_gshared/* 4110*/,
	(methodPointerType)&ValueCollection_get_Count_m25746_gshared/* 4111*/,
	(methodPointerType)&Enumerator__ctor_m25747_gshared/* 4112*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25748_gshared/* 4113*/,
	(methodPointerType)&Enumerator_Dispose_m25749_gshared/* 4114*/,
	(methodPointerType)&Enumerator_MoveNext_m25750_gshared/* 4115*/,
	(methodPointerType)&Enumerator_get_Current_m25751_gshared/* 4116*/,
	(methodPointerType)&Transform_1__ctor_m25752_gshared/* 4117*/,
	(methodPointerType)&Transform_1_Invoke_m25753_gshared/* 4118*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25754_gshared/* 4119*/,
	(methodPointerType)&Transform_1_EndInvoke_m25755_gshared/* 4120*/,
	(methodPointerType)&Transform_1__ctor_m25756_gshared/* 4121*/,
	(methodPointerType)&Transform_1_Invoke_m25757_gshared/* 4122*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25758_gshared/* 4123*/,
	(methodPointerType)&Transform_1_EndInvoke_m25759_gshared/* 4124*/,
	(methodPointerType)&Transform_1__ctor_m25760_gshared/* 4125*/,
	(methodPointerType)&Transform_1_Invoke_m25761_gshared/* 4126*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25762_gshared/* 4127*/,
	(methodPointerType)&Transform_1_EndInvoke_m25763_gshared/* 4128*/,
	(methodPointerType)&ShimEnumerator__ctor_m25764_gshared/* 4129*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m25765_gshared/* 4130*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m25766_gshared/* 4131*/,
	(methodPointerType)&ShimEnumerator_get_Key_m25767_gshared/* 4132*/,
	(methodPointerType)&ShimEnumerator_get_Value_m25768_gshared/* 4133*/,
	(methodPointerType)&ShimEnumerator_get_Current_m25769_gshared/* 4134*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25770_gshared/* 4135*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25771_gshared/* 4136*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25772_gshared/* 4137*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25773_gshared/* 4138*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25774_gshared/* 4139*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25775_gshared/* 4140*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25776_gshared/* 4141*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25777_gshared/* 4142*/,
	(methodPointerType)&DefaultComparer__ctor_m25778_gshared/* 4143*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25779_gshared/* 4144*/,
	(methodPointerType)&DefaultComparer_Equals_m25780_gshared/* 4145*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25955_gshared/* 4146*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25956_gshared/* 4147*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25957_gshared/* 4148*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25958_gshared/* 4149*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25959_gshared/* 4150*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25960_gshared/* 4151*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25961_gshared/* 4152*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25962_gshared/* 4153*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25963_gshared/* 4154*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25964_gshared/* 4155*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25965_gshared/* 4156*/,
	(methodPointerType)&Dictionary_2__ctor_m26319_gshared/* 4157*/,
	(methodPointerType)&Dictionary_2__ctor_m26321_gshared/* 4158*/,
	(methodPointerType)&Dictionary_2__ctor_m26323_gshared/* 4159*/,
	(methodPointerType)&Dictionary_2__ctor_m26325_gshared/* 4160*/,
	(methodPointerType)&Dictionary_2__ctor_m26327_gshared/* 4161*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26329_gshared/* 4162*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26331_gshared/* 4163*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m26333_gshared/* 4164*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m26335_gshared/* 4165*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m26337_gshared/* 4166*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m26339_gshared/* 4167*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m26341_gshared/* 4168*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26343_gshared/* 4169*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26345_gshared/* 4170*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26347_gshared/* 4171*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26349_gshared/* 4172*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26351_gshared/* 4173*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26353_gshared/* 4174*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26355_gshared/* 4175*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m26357_gshared/* 4176*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26359_gshared/* 4177*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26361_gshared/* 4178*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26363_gshared/* 4179*/,
	(methodPointerType)&Dictionary_2_get_Count_m26365_gshared/* 4180*/,
	(methodPointerType)&Dictionary_2_get_Item_m26367_gshared/* 4181*/,
	(methodPointerType)&Dictionary_2_set_Item_m26369_gshared/* 4182*/,
	(methodPointerType)&Dictionary_2_Init_m26371_gshared/* 4183*/,
	(methodPointerType)&Dictionary_2_InitArrays_m26373_gshared/* 4184*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m26375_gshared/* 4185*/,
	(methodPointerType)&Dictionary_2_make_pair_m26377_gshared/* 4186*/,
	(methodPointerType)&Dictionary_2_pick_key_m26379_gshared/* 4187*/,
	(methodPointerType)&Dictionary_2_pick_value_m26381_gshared/* 4188*/,
	(methodPointerType)&Dictionary_2_CopyTo_m26383_gshared/* 4189*/,
	(methodPointerType)&Dictionary_2_Resize_m26385_gshared/* 4190*/,
	(methodPointerType)&Dictionary_2_Add_m26387_gshared/* 4191*/,
	(methodPointerType)&Dictionary_2_Clear_m26389_gshared/* 4192*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m26391_gshared/* 4193*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m26393_gshared/* 4194*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m26395_gshared/* 4195*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m26397_gshared/* 4196*/,
	(methodPointerType)&Dictionary_2_Remove_m26399_gshared/* 4197*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m26401_gshared/* 4198*/,
	(methodPointerType)&Dictionary_2_get_Keys_m26403_gshared/* 4199*/,
	(methodPointerType)&Dictionary_2_get_Values_m26405_gshared/* 4200*/,
	(methodPointerType)&Dictionary_2_ToTKey_m26407_gshared/* 4201*/,
	(methodPointerType)&Dictionary_2_ToTValue_m26409_gshared/* 4202*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m26411_gshared/* 4203*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m26413_gshared/* 4204*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m26415_gshared/* 4205*/,
	(methodPointerType)&KeyCollection__ctor_m26416_gshared/* 4206*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26417_gshared/* 4207*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26418_gshared/* 4208*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26419_gshared/* 4209*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26420_gshared/* 4210*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26421_gshared/* 4211*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m26422_gshared/* 4212*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26423_gshared/* 4213*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26424_gshared/* 4214*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26425_gshared/* 4215*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m26426_gshared/* 4216*/,
	(methodPointerType)&KeyCollection_CopyTo_m26427_gshared/* 4217*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m26428_gshared/* 4218*/,
	(methodPointerType)&KeyCollection_get_Count_m26429_gshared/* 4219*/,
	(methodPointerType)&Enumerator__ctor_m26430_gshared/* 4220*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26431_gshared/* 4221*/,
	(methodPointerType)&Enumerator_Dispose_m26432_gshared/* 4222*/,
	(methodPointerType)&Enumerator_MoveNext_m26433_gshared/* 4223*/,
	(methodPointerType)&Enumerator_get_Current_m26434_gshared/* 4224*/,
	(methodPointerType)&Enumerator__ctor_m26435_gshared/* 4225*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26436_gshared/* 4226*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26437_gshared/* 4227*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26438_gshared/* 4228*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26439_gshared/* 4229*/,
	(methodPointerType)&Enumerator_MoveNext_m26440_gshared/* 4230*/,
	(methodPointerType)&Enumerator_get_Current_m26441_gshared/* 4231*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m26442_gshared/* 4232*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m26443_gshared/* 4233*/,
	(methodPointerType)&Enumerator_VerifyState_m26444_gshared/* 4234*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m26445_gshared/* 4235*/,
	(methodPointerType)&Enumerator_Dispose_m26446_gshared/* 4236*/,
	(methodPointerType)&Transform_1__ctor_m26447_gshared/* 4237*/,
	(methodPointerType)&Transform_1_Invoke_m26448_gshared/* 4238*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26449_gshared/* 4239*/,
	(methodPointerType)&Transform_1_EndInvoke_m26450_gshared/* 4240*/,
	(methodPointerType)&ValueCollection__ctor_m26451_gshared/* 4241*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26452_gshared/* 4242*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26453_gshared/* 4243*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26454_gshared/* 4244*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26455_gshared/* 4245*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26456_gshared/* 4246*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m26457_gshared/* 4247*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26458_gshared/* 4248*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26459_gshared/* 4249*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26460_gshared/* 4250*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m26461_gshared/* 4251*/,
	(methodPointerType)&ValueCollection_CopyTo_m26462_gshared/* 4252*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m26463_gshared/* 4253*/,
	(methodPointerType)&ValueCollection_get_Count_m26464_gshared/* 4254*/,
	(methodPointerType)&Enumerator__ctor_m26465_gshared/* 4255*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m26466_gshared/* 4256*/,
	(methodPointerType)&Enumerator_Dispose_m26467_gshared/* 4257*/,
	(methodPointerType)&Enumerator_MoveNext_m26468_gshared/* 4258*/,
	(methodPointerType)&Enumerator_get_Current_m26469_gshared/* 4259*/,
	(methodPointerType)&Transform_1__ctor_m26470_gshared/* 4260*/,
	(methodPointerType)&Transform_1_Invoke_m26471_gshared/* 4261*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26472_gshared/* 4262*/,
	(methodPointerType)&Transform_1_EndInvoke_m26473_gshared/* 4263*/,
	(methodPointerType)&Transform_1__ctor_m26474_gshared/* 4264*/,
	(methodPointerType)&Transform_1_Invoke_m26475_gshared/* 4265*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26476_gshared/* 4266*/,
	(methodPointerType)&Transform_1_EndInvoke_m26477_gshared/* 4267*/,
	(methodPointerType)&Transform_1__ctor_m26478_gshared/* 4268*/,
	(methodPointerType)&Transform_1_Invoke_m26479_gshared/* 4269*/,
	(methodPointerType)&Transform_1_BeginInvoke_m26480_gshared/* 4270*/,
	(methodPointerType)&Transform_1_EndInvoke_m26481_gshared/* 4271*/,
	(methodPointerType)&ShimEnumerator__ctor_m26482_gshared/* 4272*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m26483_gshared/* 4273*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m26484_gshared/* 4274*/,
	(methodPointerType)&ShimEnumerator_get_Key_m26485_gshared/* 4275*/,
	(methodPointerType)&ShimEnumerator_get_Value_m26486_gshared/* 4276*/,
	(methodPointerType)&ShimEnumerator_get_Current_m26487_gshared/* 4277*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26488_gshared/* 4278*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26489_gshared/* 4279*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26490_gshared/* 4280*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26491_gshared/* 4281*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26492_gshared/* 4282*/,
	(methodPointerType)&DefaultComparer__ctor_m26493_gshared/* 4283*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26494_gshared/* 4284*/,
	(methodPointerType)&DefaultComparer_Equals_m26495_gshared/* 4285*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26594_gshared/* 4286*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26595_gshared/* 4287*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26596_gshared/* 4288*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26597_gshared/* 4289*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26598_gshared/* 4290*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26599_gshared/* 4291*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26600_gshared/* 4292*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26601_gshared/* 4293*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26602_gshared/* 4294*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26603_gshared/* 4295*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26727_gshared/* 4296*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26728_gshared/* 4297*/,
	(methodPointerType)&InvokableCall_1__ctor_m26729_gshared/* 4298*/,
	(methodPointerType)&InvokableCall_1__ctor_m26730_gshared/* 4299*/,
	(methodPointerType)&InvokableCall_1_Invoke_m26731_gshared/* 4300*/,
	(methodPointerType)&InvokableCall_1_Find_m26732_gshared/* 4301*/,
	(methodPointerType)&UnityAction_1__ctor_m26733_gshared/* 4302*/,
	(methodPointerType)&UnityAction_1_Invoke_m26734_gshared/* 4303*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m26735_gshared/* 4304*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m26736_gshared/* 4305*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m26744_gshared/* 4306*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m26942_gshared/* 4307*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26943_gshared/* 4308*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m26944_gshared/* 4309*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m26945_gshared/* 4310*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m26946_gshared/* 4311*/,
	(methodPointerType)&Dictionary_2__ctor_m27002_gshared/* 4312*/,
	(methodPointerType)&Dictionary_2__ctor_m27005_gshared/* 4313*/,
	(methodPointerType)&Dictionary_2__ctor_m27007_gshared/* 4314*/,
	(methodPointerType)&Dictionary_2__ctor_m27009_gshared/* 4315*/,
	(methodPointerType)&Dictionary_2__ctor_m27011_gshared/* 4316*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27013_gshared/* 4317*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27015_gshared/* 4318*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m27017_gshared/* 4319*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m27019_gshared/* 4320*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m27021_gshared/* 4321*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m27023_gshared/* 4322*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m27025_gshared/* 4323*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27027_gshared/* 4324*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27029_gshared/* 4325*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27031_gshared/* 4326*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27033_gshared/* 4327*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27035_gshared/* 4328*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27037_gshared/* 4329*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27039_gshared/* 4330*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m27041_gshared/* 4331*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27043_gshared/* 4332*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27045_gshared/* 4333*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27047_gshared/* 4334*/,
	(methodPointerType)&Dictionary_2_get_Count_m27049_gshared/* 4335*/,
	(methodPointerType)&Dictionary_2_get_Item_m27051_gshared/* 4336*/,
	(methodPointerType)&Dictionary_2_set_Item_m27053_gshared/* 4337*/,
	(methodPointerType)&Dictionary_2_Init_m27055_gshared/* 4338*/,
	(methodPointerType)&Dictionary_2_InitArrays_m27057_gshared/* 4339*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m27059_gshared/* 4340*/,
	(methodPointerType)&Dictionary_2_make_pair_m27061_gshared/* 4341*/,
	(methodPointerType)&Dictionary_2_pick_key_m27063_gshared/* 4342*/,
	(methodPointerType)&Dictionary_2_pick_value_m27065_gshared/* 4343*/,
	(methodPointerType)&Dictionary_2_CopyTo_m27067_gshared/* 4344*/,
	(methodPointerType)&Dictionary_2_Resize_m27069_gshared/* 4345*/,
	(methodPointerType)&Dictionary_2_Add_m27071_gshared/* 4346*/,
	(methodPointerType)&Dictionary_2_Clear_m27073_gshared/* 4347*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m27075_gshared/* 4348*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m27077_gshared/* 4349*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m27079_gshared/* 4350*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m27081_gshared/* 4351*/,
	(methodPointerType)&Dictionary_2_Remove_m27083_gshared/* 4352*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m27085_gshared/* 4353*/,
	(methodPointerType)&Dictionary_2_get_Keys_m27087_gshared/* 4354*/,
	(methodPointerType)&Dictionary_2_get_Values_m27089_gshared/* 4355*/,
	(methodPointerType)&Dictionary_2_ToTKey_m27091_gshared/* 4356*/,
	(methodPointerType)&Dictionary_2_ToTValue_m27093_gshared/* 4357*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m27095_gshared/* 4358*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m27097_gshared/* 4359*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m27099_gshared/* 4360*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27100_gshared/* 4361*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27101_gshared/* 4362*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27102_gshared/* 4363*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27103_gshared/* 4364*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27104_gshared/* 4365*/,
	(methodPointerType)&KeyValuePair_2__ctor_m27105_gshared/* 4366*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m27106_gshared/* 4367*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m27107_gshared/* 4368*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m27108_gshared/* 4369*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m27109_gshared/* 4370*/,
	(methodPointerType)&KeyValuePair_2_ToString_m27110_gshared/* 4371*/,
	(methodPointerType)&KeyCollection__ctor_m27111_gshared/* 4372*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27112_gshared/* 4373*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27113_gshared/* 4374*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27114_gshared/* 4375*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27115_gshared/* 4376*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27116_gshared/* 4377*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m27117_gshared/* 4378*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27118_gshared/* 4379*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27119_gshared/* 4380*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27120_gshared/* 4381*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m27121_gshared/* 4382*/,
	(methodPointerType)&KeyCollection_CopyTo_m27122_gshared/* 4383*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m27123_gshared/* 4384*/,
	(methodPointerType)&KeyCollection_get_Count_m27124_gshared/* 4385*/,
	(methodPointerType)&Enumerator__ctor_m27125_gshared/* 4386*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27126_gshared/* 4387*/,
	(methodPointerType)&Enumerator_Dispose_m27127_gshared/* 4388*/,
	(methodPointerType)&Enumerator_MoveNext_m27128_gshared/* 4389*/,
	(methodPointerType)&Enumerator_get_Current_m27129_gshared/* 4390*/,
	(methodPointerType)&Enumerator__ctor_m27130_gshared/* 4391*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27131_gshared/* 4392*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132_gshared/* 4393*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133_gshared/* 4394*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134_gshared/* 4395*/,
	(methodPointerType)&Enumerator_MoveNext_m27135_gshared/* 4396*/,
	(methodPointerType)&Enumerator_get_Current_m27136_gshared/* 4397*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m27137_gshared/* 4398*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m27138_gshared/* 4399*/,
	(methodPointerType)&Enumerator_VerifyState_m27139_gshared/* 4400*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m27140_gshared/* 4401*/,
	(methodPointerType)&Enumerator_Dispose_m27141_gshared/* 4402*/,
	(methodPointerType)&Transform_1__ctor_m27142_gshared/* 4403*/,
	(methodPointerType)&Transform_1_Invoke_m27143_gshared/* 4404*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27144_gshared/* 4405*/,
	(methodPointerType)&Transform_1_EndInvoke_m27145_gshared/* 4406*/,
	(methodPointerType)&ValueCollection__ctor_m27146_gshared/* 4407*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27147_gshared/* 4408*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27148_gshared/* 4409*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27149_gshared/* 4410*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27150_gshared/* 4411*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27151_gshared/* 4412*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m27152_gshared/* 4413*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27153_gshared/* 4414*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27154_gshared/* 4415*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27155_gshared/* 4416*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m27156_gshared/* 4417*/,
	(methodPointerType)&ValueCollection_CopyTo_m27157_gshared/* 4418*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m27158_gshared/* 4419*/,
	(methodPointerType)&ValueCollection_get_Count_m27159_gshared/* 4420*/,
	(methodPointerType)&Enumerator__ctor_m27160_gshared/* 4421*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27161_gshared/* 4422*/,
	(methodPointerType)&Enumerator_Dispose_m27162_gshared/* 4423*/,
	(methodPointerType)&Enumerator_MoveNext_m27163_gshared/* 4424*/,
	(methodPointerType)&Enumerator_get_Current_m27164_gshared/* 4425*/,
	(methodPointerType)&Transform_1__ctor_m27165_gshared/* 4426*/,
	(methodPointerType)&Transform_1_Invoke_m27166_gshared/* 4427*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27167_gshared/* 4428*/,
	(methodPointerType)&Transform_1_EndInvoke_m27168_gshared/* 4429*/,
	(methodPointerType)&Transform_1__ctor_m27169_gshared/* 4430*/,
	(methodPointerType)&Transform_1_Invoke_m27170_gshared/* 4431*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27171_gshared/* 4432*/,
	(methodPointerType)&Transform_1_EndInvoke_m27172_gshared/* 4433*/,
	(methodPointerType)&Transform_1__ctor_m27173_gshared/* 4434*/,
	(methodPointerType)&Transform_1_Invoke_m27174_gshared/* 4435*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27175_gshared/* 4436*/,
	(methodPointerType)&Transform_1_EndInvoke_m27176_gshared/* 4437*/,
	(methodPointerType)&ShimEnumerator__ctor_m27177_gshared/* 4438*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m27178_gshared/* 4439*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m27179_gshared/* 4440*/,
	(methodPointerType)&ShimEnumerator_get_Key_m27180_gshared/* 4441*/,
	(methodPointerType)&ShimEnumerator_get_Value_m27181_gshared/* 4442*/,
	(methodPointerType)&ShimEnumerator_get_Current_m27182_gshared/* 4443*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27183_gshared/* 4444*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27184_gshared/* 4445*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27185_gshared/* 4446*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27186_gshared/* 4447*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27187_gshared/* 4448*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m27188_gshared/* 4449*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27189_gshared/* 4450*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27190_gshared/* 4451*/,
	(methodPointerType)&DefaultComparer__ctor_m27191_gshared/* 4452*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27192_gshared/* 4453*/,
	(methodPointerType)&DefaultComparer_Equals_m27193_gshared/* 4454*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27244_gshared/* 4455*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27245_gshared/* 4456*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27246_gshared/* 4457*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27247_gshared/* 4458*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27248_gshared/* 4459*/,
	(methodPointerType)&Dictionary_2__ctor_m27259_gshared/* 4460*/,
	(methodPointerType)&Dictionary_2__ctor_m27260_gshared/* 4461*/,
	(methodPointerType)&Dictionary_2__ctor_m27261_gshared/* 4462*/,
	(methodPointerType)&Dictionary_2__ctor_m27262_gshared/* 4463*/,
	(methodPointerType)&Dictionary_2__ctor_m27263_gshared/* 4464*/,
	(methodPointerType)&Dictionary_2__ctor_m27264_gshared/* 4465*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27265_gshared/* 4466*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27266_gshared/* 4467*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m27267_gshared/* 4468*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m27268_gshared/* 4469*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m27269_gshared/* 4470*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m27270_gshared/* 4471*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m27271_gshared/* 4472*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27272_gshared/* 4473*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27273_gshared/* 4474*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27274_gshared/* 4475*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27275_gshared/* 4476*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27276_gshared/* 4477*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27277_gshared/* 4478*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27278_gshared/* 4479*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m27279_gshared/* 4480*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27280_gshared/* 4481*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27281_gshared/* 4482*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27282_gshared/* 4483*/,
	(methodPointerType)&Dictionary_2_get_Count_m27283_gshared/* 4484*/,
	(methodPointerType)&Dictionary_2_get_Item_m27284_gshared/* 4485*/,
	(methodPointerType)&Dictionary_2_set_Item_m27285_gshared/* 4486*/,
	(methodPointerType)&Dictionary_2_Init_m27286_gshared/* 4487*/,
	(methodPointerType)&Dictionary_2_InitArrays_m27287_gshared/* 4488*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m27288_gshared/* 4489*/,
	(methodPointerType)&Dictionary_2_make_pair_m27289_gshared/* 4490*/,
	(methodPointerType)&Dictionary_2_pick_key_m27290_gshared/* 4491*/,
	(methodPointerType)&Dictionary_2_pick_value_m27291_gshared/* 4492*/,
	(methodPointerType)&Dictionary_2_CopyTo_m27292_gshared/* 4493*/,
	(methodPointerType)&Dictionary_2_Resize_m27293_gshared/* 4494*/,
	(methodPointerType)&Dictionary_2_Add_m27294_gshared/* 4495*/,
	(methodPointerType)&Dictionary_2_Clear_m27295_gshared/* 4496*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m27296_gshared/* 4497*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m27297_gshared/* 4498*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m27298_gshared/* 4499*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m27299_gshared/* 4500*/,
	(methodPointerType)&Dictionary_2_Remove_m27300_gshared/* 4501*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m27301_gshared/* 4502*/,
	(methodPointerType)&Dictionary_2_get_Keys_m27302_gshared/* 4503*/,
	(methodPointerType)&Dictionary_2_get_Values_m27303_gshared/* 4504*/,
	(methodPointerType)&Dictionary_2_ToTKey_m27304_gshared/* 4505*/,
	(methodPointerType)&Dictionary_2_ToTValue_m27305_gshared/* 4506*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m27306_gshared/* 4507*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m27307_gshared/* 4508*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m27308_gshared/* 4509*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27309_gshared/* 4510*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27310_gshared/* 4511*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27311_gshared/* 4512*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27312_gshared/* 4513*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27313_gshared/* 4514*/,
	(methodPointerType)&KeyValuePair_2__ctor_m27314_gshared/* 4515*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m27315_gshared/* 4516*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m27316_gshared/* 4517*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m27317_gshared/* 4518*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m27318_gshared/* 4519*/,
	(methodPointerType)&KeyValuePair_2_ToString_m27319_gshared/* 4520*/,
	(methodPointerType)&KeyCollection__ctor_m27320_gshared/* 4521*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m27321_gshared/* 4522*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m27322_gshared/* 4523*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m27323_gshared/* 4524*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m27324_gshared/* 4525*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m27325_gshared/* 4526*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m27326_gshared/* 4527*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m27327_gshared/* 4528*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m27328_gshared/* 4529*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m27329_gshared/* 4530*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m27330_gshared/* 4531*/,
	(methodPointerType)&KeyCollection_CopyTo_m27331_gshared/* 4532*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m27332_gshared/* 4533*/,
	(methodPointerType)&KeyCollection_get_Count_m27333_gshared/* 4534*/,
	(methodPointerType)&Enumerator__ctor_m27334_gshared/* 4535*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27335_gshared/* 4536*/,
	(methodPointerType)&Enumerator_Dispose_m27336_gshared/* 4537*/,
	(methodPointerType)&Enumerator_MoveNext_m27337_gshared/* 4538*/,
	(methodPointerType)&Enumerator_get_Current_m27338_gshared/* 4539*/,
	(methodPointerType)&Enumerator__ctor_m27339_gshared/* 4540*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27340_gshared/* 4541*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27341_gshared/* 4542*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27342_gshared/* 4543*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27343_gshared/* 4544*/,
	(methodPointerType)&Enumerator_MoveNext_m27344_gshared/* 4545*/,
	(methodPointerType)&Enumerator_get_Current_m27345_gshared/* 4546*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m27346_gshared/* 4547*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m27347_gshared/* 4548*/,
	(methodPointerType)&Enumerator_VerifyState_m27348_gshared/* 4549*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m27349_gshared/* 4550*/,
	(methodPointerType)&Enumerator_Dispose_m27350_gshared/* 4551*/,
	(methodPointerType)&Transform_1__ctor_m27351_gshared/* 4552*/,
	(methodPointerType)&Transform_1_Invoke_m27352_gshared/* 4553*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27353_gshared/* 4554*/,
	(methodPointerType)&Transform_1_EndInvoke_m27354_gshared/* 4555*/,
	(methodPointerType)&ValueCollection__ctor_m27355_gshared/* 4556*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m27356_gshared/* 4557*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m27357_gshared/* 4558*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m27358_gshared/* 4559*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m27359_gshared/* 4560*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m27360_gshared/* 4561*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m27361_gshared/* 4562*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m27362_gshared/* 4563*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27363_gshared/* 4564*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m27364_gshared/* 4565*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m27365_gshared/* 4566*/,
	(methodPointerType)&ValueCollection_CopyTo_m27366_gshared/* 4567*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m27367_gshared/* 4568*/,
	(methodPointerType)&ValueCollection_get_Count_m27368_gshared/* 4569*/,
	(methodPointerType)&Enumerator__ctor_m27369_gshared/* 4570*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m27370_gshared/* 4571*/,
	(methodPointerType)&Enumerator_Dispose_m27371_gshared/* 4572*/,
	(methodPointerType)&Enumerator_MoveNext_m27372_gshared/* 4573*/,
	(methodPointerType)&Enumerator_get_Current_m27373_gshared/* 4574*/,
	(methodPointerType)&Transform_1__ctor_m27374_gshared/* 4575*/,
	(methodPointerType)&Transform_1_Invoke_m27375_gshared/* 4576*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27376_gshared/* 4577*/,
	(methodPointerType)&Transform_1_EndInvoke_m27377_gshared/* 4578*/,
	(methodPointerType)&Transform_1__ctor_m27378_gshared/* 4579*/,
	(methodPointerType)&Transform_1_Invoke_m27379_gshared/* 4580*/,
	(methodPointerType)&Transform_1_BeginInvoke_m27380_gshared/* 4581*/,
	(methodPointerType)&Transform_1_EndInvoke_m27381_gshared/* 4582*/,
	(methodPointerType)&ShimEnumerator__ctor_m27382_gshared/* 4583*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m27383_gshared/* 4584*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m27384_gshared/* 4585*/,
	(methodPointerType)&ShimEnumerator_get_Key_m27385_gshared/* 4586*/,
	(methodPointerType)&ShimEnumerator_get_Value_m27386_gshared/* 4587*/,
	(methodPointerType)&ShimEnumerator_get_Current_m27387_gshared/* 4588*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27388_gshared/* 4589*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27389_gshared/* 4590*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27390_gshared/* 4591*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27391_gshared/* 4592*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27392_gshared/* 4593*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27393_gshared/* 4594*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27394_gshared/* 4595*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27395_gshared/* 4596*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27396_gshared/* 4597*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27397_gshared/* 4598*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27403_gshared/* 4599*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27404_gshared/* 4600*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27405_gshared/* 4601*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27406_gshared/* 4602*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27407_gshared/* 4603*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27408_gshared/* 4604*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27409_gshared/* 4605*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27410_gshared/* 4606*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27411_gshared/* 4607*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27412_gshared/* 4608*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27438_gshared/* 4609*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27439_gshared/* 4610*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27440_gshared/* 4611*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27441_gshared/* 4612*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27442_gshared/* 4613*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27468_gshared/* 4614*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27469_gshared/* 4615*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27470_gshared/* 4616*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27471_gshared/* 4617*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27472_gshared/* 4618*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27473_gshared/* 4619*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27474_gshared/* 4620*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27475_gshared/* 4621*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27476_gshared/* 4622*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27477_gshared/* 4623*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27546_gshared/* 4624*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27547_gshared/* 4625*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27548_gshared/* 4626*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27549_gshared/* 4627*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27550_gshared/* 4628*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27551_gshared/* 4629*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27552_gshared/* 4630*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27553_gshared/* 4631*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27554_gshared/* 4632*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27555_gshared/* 4633*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m27556_gshared/* 4634*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27557_gshared/* 4635*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m27558_gshared/* 4636*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m27559_gshared/* 4637*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m27560_gshared/* 4638*/,
	(methodPointerType)&GenericComparer_1_Compare_m27664_gshared/* 4639*/,
	(methodPointerType)&Comparer_1__ctor_m27665_gshared/* 4640*/,
	(methodPointerType)&Comparer_1__cctor_m27666_gshared/* 4641*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27667_gshared/* 4642*/,
	(methodPointerType)&Comparer_1_get_Default_m27668_gshared/* 4643*/,
	(methodPointerType)&DefaultComparer__ctor_m27669_gshared/* 4644*/,
	(methodPointerType)&DefaultComparer_Compare_m27670_gshared/* 4645*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27671_gshared/* 4646*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27672_gshared/* 4647*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27673_gshared/* 4648*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27674_gshared/* 4649*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27675_gshared/* 4650*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27676_gshared/* 4651*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27677_gshared/* 4652*/,
	(methodPointerType)&DefaultComparer__ctor_m27678_gshared/* 4653*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27679_gshared/* 4654*/,
	(methodPointerType)&DefaultComparer_Equals_m27680_gshared/* 4655*/,
	(methodPointerType)&GenericComparer_1_Compare_m27681_gshared/* 4656*/,
	(methodPointerType)&Comparer_1__ctor_m27682_gshared/* 4657*/,
	(methodPointerType)&Comparer_1__cctor_m27683_gshared/* 4658*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27684_gshared/* 4659*/,
	(methodPointerType)&Comparer_1_get_Default_m27685_gshared/* 4660*/,
	(methodPointerType)&DefaultComparer__ctor_m27686_gshared/* 4661*/,
	(methodPointerType)&DefaultComparer_Compare_m27687_gshared/* 4662*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27688_gshared/* 4663*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27689_gshared/* 4664*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27690_gshared/* 4665*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27691_gshared/* 4666*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27692_gshared/* 4667*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27693_gshared/* 4668*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27694_gshared/* 4669*/,
	(methodPointerType)&DefaultComparer__ctor_m27695_gshared/* 4670*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27696_gshared/* 4671*/,
	(methodPointerType)&DefaultComparer_Equals_m27697_gshared/* 4672*/,
	(methodPointerType)&Nullable_1_Equals_m27698_gshared/* 4673*/,
	(methodPointerType)&Nullable_1_Equals_m27699_gshared/* 4674*/,
	(methodPointerType)&Nullable_1_GetHashCode_m27700_gshared/* 4675*/,
	(methodPointerType)&Nullable_1_ToString_m27701_gshared/* 4676*/,
	(methodPointerType)&GenericComparer_1_Compare_m27702_gshared/* 4677*/,
	(methodPointerType)&Comparer_1__ctor_m27703_gshared/* 4678*/,
	(methodPointerType)&Comparer_1__cctor_m27704_gshared/* 4679*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27705_gshared/* 4680*/,
	(methodPointerType)&Comparer_1_get_Default_m27706_gshared/* 4681*/,
	(methodPointerType)&DefaultComparer__ctor_m27707_gshared/* 4682*/,
	(methodPointerType)&DefaultComparer_Compare_m27708_gshared/* 4683*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27709_gshared/* 4684*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27710_gshared/* 4685*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27711_gshared/* 4686*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27712_gshared/* 4687*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27713_gshared/* 4688*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27714_gshared/* 4689*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27715_gshared/* 4690*/,
	(methodPointerType)&DefaultComparer__ctor_m27716_gshared/* 4691*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27717_gshared/* 4692*/,
	(methodPointerType)&DefaultComparer_Equals_m27718_gshared/* 4693*/,
	(methodPointerType)&GenericComparer_1_Compare_m27719_gshared/* 4694*/,
	(methodPointerType)&Comparer_1__ctor_m27720_gshared/* 4695*/,
	(methodPointerType)&Comparer_1__cctor_m27721_gshared/* 4696*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m27722_gshared/* 4697*/,
	(methodPointerType)&Comparer_1_get_Default_m27723_gshared/* 4698*/,
	(methodPointerType)&DefaultComparer__ctor_m27724_gshared/* 4699*/,
	(methodPointerType)&DefaultComparer_Compare_m27725_gshared/* 4700*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m27726_gshared/* 4701*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m27727_gshared/* 4702*/,
	(methodPointerType)&EqualityComparer_1__ctor_m27728_gshared/* 4703*/,
	(methodPointerType)&EqualityComparer_1__cctor_m27729_gshared/* 4704*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27730_gshared/* 4705*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27731_gshared/* 4706*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m27732_gshared/* 4707*/,
	(methodPointerType)&DefaultComparer__ctor_m27733_gshared/* 4708*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m27734_gshared/* 4709*/,
	(methodPointerType)&DefaultComparer_Equals_m27735_gshared/* 4710*/,
};
const InvokerMethod g_Il2CppInvokerPointers[676] = 
{
	NULL/* 0*/,
	RuntimeInvoker_Object_t_Object_t/* 1*/,
	RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t/* 2*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* 3*/,
	RuntimeInvoker_Boolean_t169_Object_t/* 4*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t/* 5*/,
	RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* 6*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* 7*/,
	RuntimeInvoker_Void_t168_Object_t/* 8*/,
	RuntimeInvoker_Boolean_t169_ObjectU26_t1516_Object_t/* 9*/,
	RuntimeInvoker_Void_t168_ObjectU26_t1516_Object_t/* 10*/,
	RuntimeInvoker_Int32_t127/* 11*/,
	RuntimeInvoker_Boolean_t169/* 12*/,
	RuntimeInvoker_Object_t_Int32_t127/* 13*/,
	RuntimeInvoker_Void_t168_Int32_t127_Object_t/* 14*/,
	RuntimeInvoker_Void_t168/* 15*/,
	RuntimeInvoker_Object_t/* 16*/,
	RuntimeInvoker_Void_t168_Object_t_Int32_t127/* 17*/,
	RuntimeInvoker_Int32_t127_Object_t/* 18*/,
	RuntimeInvoker_Void_t168_Int32_t127/* 19*/,
	RuntimeInvoker_Object_t_Object_t_Object_t/* 20*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127/* 21*/,
	RuntimeInvoker_Object_t_SByte_t170/* 22*/,
	RuntimeInvoker_Void_t168_SByte_t170_Object_t/* 23*/,
	RuntimeInvoker_Boolean_t169_Object_t_ObjectU26_t1516/* 24*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3254/* 25*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3254/* 26*/,
	RuntimeInvoker_Boolean_t169_Object_t_Object_t/* 27*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t/* 28*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 29*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t/* 30*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 31*/,
	RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* 32*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127_Object_t/* 33*/,
	RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* 34*/,
	RuntimeInvoker_Int32_t127_Int32_t127/* 35*/,
	RuntimeInvoker_Enumerator_t3656/* 36*/,
	RuntimeInvoker_Boolean_t169_Int32_t127/* 37*/,
	RuntimeInvoker_Enumerator_t3928/* 38*/,
	RuntimeInvoker_Enumerator_t3148/* 39*/,
	RuntimeInvoker_Void_t168_Int32_t127_ObjectU26_t1516/* 40*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Int32_t127/* 41*/,
	RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_Object_t/* 42*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Int32_t127_Object_t/* 43*/,
	RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t/* 44*/,
	RuntimeInvoker_Int32_t127_Object_t_Object_t_Object_t/* 45*/,
	RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Int32_t127/* 46*/,
	RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Int32_t127_Int32_t127/* 47*/,
	RuntimeInvoker_Int32_t127_Object_t_Object_t/* 48*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Object_t/* 49*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Object_t/* 50*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Object_t_Object_t/* 51*/,
	RuntimeInvoker_Int32_t127_Object_t_Object_t_Int32_t127/* 52*/,
	RuntimeInvoker_Int32_t127_Object_t_Object_t_Int32_t127_Int32_t127/* 53*/,
	RuntimeInvoker_KeyValuePair_2_t3254_Object_t_Object_t/* 54*/,
	RuntimeInvoker_Enumerator_t3258/* 55*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_Object_t/* 56*/,
	RuntimeInvoker_DictionaryEntry_t1996/* 57*/,
	RuntimeInvoker_KeyValuePair_2_t3254/* 58*/,
	RuntimeInvoker_Enumerator_t3257/* 59*/,
	RuntimeInvoker_Enumerator_t3261/* 60*/,
	RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127_Object_t/* 61*/,
	RuntimeInvoker_Enumerator_t3115/* 62*/,
	RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* 63*/,
	RuntimeInvoker_Void_t168_SByte_t170/* 64*/,
	RuntimeInvoker_Enumerator_t3205/* 65*/,
	RuntimeInvoker_Enumerator_t3202/* 66*/,
	RuntimeInvoker_KeyValuePair_2_t3197/* 67*/,
	RuntimeInvoker_Void_t168_Color_t90/* 68*/,
	RuntimeInvoker_Void_t168_ColorTween_t253/* 69*/,
	RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32_t127/* 70*/,
	RuntimeInvoker_Boolean_t169_ByteU26_t1840_SByte_t170/* 71*/,
	RuntimeInvoker_Boolean_t169_SingleU26_t924_Single_t151/* 72*/,
	RuntimeInvoker_Boolean_t169_UInt16U26_t2720_Int16_t534/* 73*/,
	RuntimeInvoker_Void_t168_Single_t151/* 74*/,
	RuntimeInvoker_Void_t168_Vector2_t10/* 75*/,
	RuntimeInvoker_Boolean_t169_NavigationU26_t4540_Navigation_t320/* 76*/,
	RuntimeInvoker_Boolean_t169_ColorBlockU26_t4541_ColorBlock_t265/* 77*/,
	RuntimeInvoker_Boolean_t169_SpriteStateU26_t4542_SpriteState_t339/* 78*/,
	RuntimeInvoker_Void_t168_Int32U26_t535_Int32_t127/* 79*/,
	RuntimeInvoker_Void_t168_Vector2U26_t923_Vector2_t10/* 80*/,
	RuntimeInvoker_Void_t168_SingleU26_t924_Single_t151/* 81*/,
	RuntimeInvoker_Void_t168_ByteU26_t1840_SByte_t170/* 82*/,
	RuntimeInvoker_Enumerator_t816/* 83*/,
	RuntimeInvoker_Byte_t449/* 84*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151_Object_t/* 85*/,
	RuntimeInvoker_Int32_t127_Object_t_Int16_t534/* 86*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Single_t151/* 87*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127/* 88*/,
	RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170/* 89*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Int32_t127/* 90*/,
	RuntimeInvoker_Void_t168_TimeSpan_t112/* 91*/,
	RuntimeInvoker_TimeSpan_t112/* 92*/,
	RuntimeInvoker_UInt16_t454_Int32_t127/* 93*/,
	RuntimeInvoker_Void_t168_Int16_t534/* 94*/,
	RuntimeInvoker_Boolean_t169_Int16_t534/* 95*/,
	RuntimeInvoker_Int32_t127_Int16_t534/* 96*/,
	RuntimeInvoker_Void_t168_Int32_t127_Int16_t534/* 97*/,
	RuntimeInvoker_Single_t151_Object_t_Single_t151/* 98*/,
	RuntimeInvoker_Vector3_t15_Int32_t127/* 99*/,
	RuntimeInvoker_Void_t168_Vector3_t15/* 100*/,
	RuntimeInvoker_Boolean_t169_Vector3_t15/* 101*/,
	RuntimeInvoker_Int32_t127_Vector3_t15/* 102*/,
	RuntimeInvoker_Void_t168_Int32_t127_Vector3_t15/* 103*/,
	RuntimeInvoker_Double_t1407_Int32_t127/* 104*/,
	RuntimeInvoker_Void_t168_Double_t1407/* 105*/,
	RuntimeInvoker_Boolean_t169_Double_t1407/* 106*/,
	RuntimeInvoker_Int32_t127_Double_t1407/* 107*/,
	RuntimeInvoker_Void_t168_Int32_t127_Double_t1407/* 108*/,
	RuntimeInvoker_RaycastResult_t231_Int32_t127/* 109*/,
	RuntimeInvoker_Void_t168_RaycastResult_t231/* 110*/,
	RuntimeInvoker_Boolean_t169_RaycastResult_t231/* 111*/,
	RuntimeInvoker_Int32_t127_RaycastResult_t231/* 112*/,
	RuntimeInvoker_Void_t168_Int32_t127_RaycastResult_t231/* 113*/,
	RuntimeInvoker_Void_t168_RaycastResultU5BU5DU26_t4543_Int32_t127/* 114*/,
	RuntimeInvoker_Void_t168_RaycastResultU5BU5DU26_t4543_Int32_t127_Int32_t127/* 115*/,
	RuntimeInvoker_Int32_t127_Object_t_RaycastResult_t231_Int32_t127_Int32_t127/* 116*/,
	RuntimeInvoker_Int32_t127_RaycastResult_t231_RaycastResult_t231_Object_t/* 117*/,
	RuntimeInvoker_KeyValuePair_2_t3197_Int32_t127/* 118*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3197/* 119*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3197/* 120*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3197/* 121*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3197/* 122*/,
	RuntimeInvoker_Link_t2136_Int32_t127/* 123*/,
	RuntimeInvoker_Void_t168_Link_t2136/* 124*/,
	RuntimeInvoker_Boolean_t169_Link_t2136/* 125*/,
	RuntimeInvoker_Int32_t127_Link_t2136/* 126*/,
	RuntimeInvoker_Void_t168_Int32_t127_Link_t2136/* 127*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int32_t127/* 128*/,
	RuntimeInvoker_Void_t168_DictionaryEntry_t1996/* 129*/,
	RuntimeInvoker_Boolean_t169_DictionaryEntry_t1996/* 130*/,
	RuntimeInvoker_Int32_t127_DictionaryEntry_t1996/* 131*/,
	RuntimeInvoker_Void_t168_Int32_t127_DictionaryEntry_t1996/* 132*/,
	RuntimeInvoker_RaycastHit2D_t432_Int32_t127/* 133*/,
	RuntimeInvoker_Void_t168_RaycastHit2D_t432/* 134*/,
	RuntimeInvoker_Boolean_t169_RaycastHit2D_t432/* 135*/,
	RuntimeInvoker_Int32_t127_RaycastHit2D_t432/* 136*/,
	RuntimeInvoker_Void_t168_Int32_t127_RaycastHit2D_t432/* 137*/,
	RuntimeInvoker_RaycastHit_t94_Int32_t127/* 138*/,
	RuntimeInvoker_Void_t168_RaycastHit_t94/* 139*/,
	RuntimeInvoker_Boolean_t169_RaycastHit_t94/* 140*/,
	RuntimeInvoker_Int32_t127_RaycastHit_t94/* 141*/,
	RuntimeInvoker_Void_t168_Int32_t127_RaycastHit_t94/* 142*/,
	RuntimeInvoker_KeyValuePair_2_t3228_Int32_t127/* 143*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3228/* 144*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3228/* 145*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3228/* 146*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3228/* 147*/,
	RuntimeInvoker_KeyValuePair_2_t3254_Int32_t127/* 148*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3254/* 149*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3254/* 150*/,
	RuntimeInvoker_Void_t168_UIVertexU5BU5DU26_t4544_Int32_t127/* 151*/,
	RuntimeInvoker_Void_t168_UIVertexU5BU5DU26_t4544_Int32_t127_Int32_t127/* 152*/,
	RuntimeInvoker_Int32_t127_Object_t_UIVertex_t313_Int32_t127_Int32_t127/* 153*/,
	RuntimeInvoker_Int32_t127_UIVertex_t313_UIVertex_t313_Object_t/* 154*/,
	RuntimeInvoker_Vector2_t10_Int32_t127/* 155*/,
	RuntimeInvoker_Boolean_t169_Vector2_t10/* 156*/,
	RuntimeInvoker_Int32_t127_Vector2_t10/* 157*/,
	RuntimeInvoker_Void_t168_Int32_t127_Vector2_t10/* 158*/,
	RuntimeInvoker_UILineInfo_t458_Int32_t127/* 159*/,
	RuntimeInvoker_Void_t168_UILineInfo_t458/* 160*/,
	RuntimeInvoker_Boolean_t169_UILineInfo_t458/* 161*/,
	RuntimeInvoker_Int32_t127_UILineInfo_t458/* 162*/,
	RuntimeInvoker_Void_t168_Int32_t127_UILineInfo_t458/* 163*/,
	RuntimeInvoker_UICharInfo_t460_Int32_t127/* 164*/,
	RuntimeInvoker_Void_t168_UICharInfo_t460/* 165*/,
	RuntimeInvoker_Boolean_t169_UICharInfo_t460/* 166*/,
	RuntimeInvoker_Int32_t127_UICharInfo_t460/* 167*/,
	RuntimeInvoker_Void_t168_Int32_t127_UICharInfo_t460/* 168*/,
	RuntimeInvoker_Single_t151_Int32_t127/* 169*/,
	RuntimeInvoker_Boolean_t169_Single_t151/* 170*/,
	RuntimeInvoker_Int32_t127_Single_t151/* 171*/,
	RuntimeInvoker_Void_t168_Int32_t127_Single_t151/* 172*/,
	RuntimeInvoker_EyewearCalibrationReading_t586_Int32_t127/* 173*/,
	RuntimeInvoker_Void_t168_EyewearCalibrationReading_t586/* 174*/,
	RuntimeInvoker_Boolean_t169_EyewearCalibrationReading_t586/* 175*/,
	RuntimeInvoker_Int32_t127_EyewearCalibrationReading_t586/* 176*/,
	RuntimeInvoker_Void_t168_Int32_t127_EyewearCalibrationReading_t586/* 177*/,
	RuntimeInvoker_Void_t168_Int32U5BU5DU26_t4545_Int32_t127/* 178*/,
	RuntimeInvoker_Void_t168_Int32U5BU5DU26_t4545_Int32_t127_Int32_t127/* 179*/,
	RuntimeInvoker_Byte_t449_Int32_t127/* 180*/,
	RuntimeInvoker_Boolean_t169_SByte_t170/* 181*/,
	RuntimeInvoker_Int32_t127_SByte_t170/* 182*/,
	RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* 183*/,
	RuntimeInvoker_Color32_t421_Int32_t127/* 184*/,
	RuntimeInvoker_Void_t168_Color32_t421/* 185*/,
	RuntimeInvoker_Boolean_t169_Color32_t421/* 186*/,
	RuntimeInvoker_Int32_t127_Color32_t421/* 187*/,
	RuntimeInvoker_Void_t168_Int32_t127_Color32_t421/* 188*/,
	RuntimeInvoker_Color_t90_Int32_t127/* 189*/,
	RuntimeInvoker_Boolean_t169_Color_t90/* 190*/,
	RuntimeInvoker_Int32_t127_Color_t90/* 191*/,
	RuntimeInvoker_Void_t168_Int32_t127_Color_t90/* 192*/,
	RuntimeInvoker_TrackableResultData_t642_Int32_t127/* 193*/,
	RuntimeInvoker_Void_t168_TrackableResultData_t642/* 194*/,
	RuntimeInvoker_Boolean_t169_TrackableResultData_t642/* 195*/,
	RuntimeInvoker_Int32_t127_TrackableResultData_t642/* 196*/,
	RuntimeInvoker_Void_t168_Int32_t127_TrackableResultData_t642/* 197*/,
	RuntimeInvoker_WordData_t647_Int32_t127/* 198*/,
	RuntimeInvoker_Void_t168_WordData_t647/* 199*/,
	RuntimeInvoker_Boolean_t169_WordData_t647/* 200*/,
	RuntimeInvoker_Int32_t127_WordData_t647/* 201*/,
	RuntimeInvoker_Void_t168_Int32_t127_WordData_t647/* 202*/,
	RuntimeInvoker_WordResultData_t646_Int32_t127/* 203*/,
	RuntimeInvoker_Void_t168_WordResultData_t646/* 204*/,
	RuntimeInvoker_Boolean_t169_WordResultData_t646/* 205*/,
	RuntimeInvoker_Int32_t127_WordResultData_t646/* 206*/,
	RuntimeInvoker_Void_t168_Int32_t127_WordResultData_t646/* 207*/,
	RuntimeInvoker_SmartTerrainRevisionData_t650_Int32_t127/* 208*/,
	RuntimeInvoker_Void_t168_SmartTerrainRevisionData_t650/* 209*/,
	RuntimeInvoker_Boolean_t169_SmartTerrainRevisionData_t650/* 210*/,
	RuntimeInvoker_Int32_t127_SmartTerrainRevisionData_t650/* 211*/,
	RuntimeInvoker_Void_t168_Int32_t127_SmartTerrainRevisionData_t650/* 212*/,
	RuntimeInvoker_SurfaceData_t651_Int32_t127/* 213*/,
	RuntimeInvoker_Void_t168_SurfaceData_t651/* 214*/,
	RuntimeInvoker_Boolean_t169_SurfaceData_t651/* 215*/,
	RuntimeInvoker_Int32_t127_SurfaceData_t651/* 216*/,
	RuntimeInvoker_Void_t168_Int32_t127_SurfaceData_t651/* 217*/,
	RuntimeInvoker_PropData_t652_Int32_t127/* 218*/,
	RuntimeInvoker_Void_t168_PropData_t652/* 219*/,
	RuntimeInvoker_Boolean_t169_PropData_t652/* 220*/,
	RuntimeInvoker_Int32_t127_PropData_t652/* 221*/,
	RuntimeInvoker_Void_t168_Int32_t127_PropData_t652/* 222*/,
	RuntimeInvoker_KeyValuePair_2_t3462_Int32_t127/* 223*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3462/* 224*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3462/* 225*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3462/* 226*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3462/* 227*/,
	RuntimeInvoker_RectangleData_t596_Int32_t127/* 228*/,
	RuntimeInvoker_Void_t168_RectangleData_t596/* 229*/,
	RuntimeInvoker_Boolean_t169_RectangleData_t596/* 230*/,
	RuntimeInvoker_Int32_t127_RectangleData_t596/* 231*/,
	RuntimeInvoker_Void_t168_Int32_t127_RectangleData_t596/* 232*/,
	RuntimeInvoker_KeyValuePair_2_t3550_Int32_t127/* 233*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3550/* 234*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3550/* 235*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3550/* 236*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3550/* 237*/,
	RuntimeInvoker_KeyValuePair_2_t3565_Int32_t127/* 238*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3565/* 239*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3565/* 240*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3565/* 241*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3565/* 242*/,
	RuntimeInvoker_VirtualButtonData_t643_Int32_t127/* 243*/,
	RuntimeInvoker_Void_t168_VirtualButtonData_t643/* 244*/,
	RuntimeInvoker_Boolean_t169_VirtualButtonData_t643/* 245*/,
	RuntimeInvoker_Int32_t127_VirtualButtonData_t643/* 246*/,
	RuntimeInvoker_Void_t168_Int32_t127_VirtualButtonData_t643/* 247*/,
	RuntimeInvoker_TargetSearchResult_t720_Int32_t127/* 248*/,
	RuntimeInvoker_Void_t168_TargetSearchResult_t720/* 249*/,
	RuntimeInvoker_Boolean_t169_TargetSearchResult_t720/* 250*/,
	RuntimeInvoker_Int32_t127_TargetSearchResult_t720/* 251*/,
	RuntimeInvoker_Void_t168_Int32_t127_TargetSearchResult_t720/* 252*/,
	RuntimeInvoker_Void_t168_TargetSearchResultU5BU5DU26_t4546_Int32_t127/* 253*/,
	RuntimeInvoker_Void_t168_TargetSearchResultU5BU5DU26_t4546_Int32_t127_Int32_t127/* 254*/,
	RuntimeInvoker_Int32_t127_Object_t_TargetSearchResult_t720_Int32_t127_Int32_t127/* 255*/,
	RuntimeInvoker_Int32_t127_TargetSearchResult_t720_TargetSearchResult_t720_Object_t/* 256*/,
	RuntimeInvoker_WebCamDevice_t879_Int32_t127/* 257*/,
	RuntimeInvoker_Void_t168_WebCamDevice_t879/* 258*/,
	RuntimeInvoker_Boolean_t169_WebCamDevice_t879/* 259*/,
	RuntimeInvoker_Int32_t127_WebCamDevice_t879/* 260*/,
	RuntimeInvoker_Void_t168_Int32_t127_WebCamDevice_t879/* 261*/,
	RuntimeInvoker_KeyValuePair_2_t3604_Int32_t127/* 262*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3604/* 263*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3604/* 264*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3604/* 265*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3604/* 266*/,
	RuntimeInvoker_ProfileData_t734_Int32_t127/* 267*/,
	RuntimeInvoker_Void_t168_ProfileData_t734/* 268*/,
	RuntimeInvoker_Boolean_t169_ProfileData_t734/* 269*/,
	RuntimeInvoker_Int32_t127_ProfileData_t734/* 270*/,
	RuntimeInvoker_Void_t168_Int32_t127_ProfileData_t734/* 271*/,
	RuntimeInvoker_Link_t3653_Int32_t127/* 272*/,
	RuntimeInvoker_Void_t168_Link_t3653/* 273*/,
	RuntimeInvoker_Boolean_t169_Link_t3653/* 274*/,
	RuntimeInvoker_Int32_t127_Link_t3653/* 275*/,
	RuntimeInvoker_Void_t168_Int32_t127_Link_t3653/* 276*/,
	RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t_Vector3_t15_Single_t151_Object_t/* 277*/,
	RuntimeInvoker_Void_t168_UInt16U5BU5DU26_t4547_Int32_t127/* 278*/,
	RuntimeInvoker_Void_t168_UInt16U5BU5DU26_t4547_Int32_t127_Int32_t127/* 279*/,
	RuntimeInvoker_Int32_t127_Object_t_Int16_t534_Int32_t127_Int32_t127/* 280*/,
	RuntimeInvoker_Int32_t127_Int16_t534_Int16_t534_Object_t/* 281*/,
	RuntimeInvoker_GcAchievementData_t1305_Int32_t127/* 282*/,
	RuntimeInvoker_Void_t168_GcAchievementData_t1305/* 283*/,
	RuntimeInvoker_Boolean_t169_GcAchievementData_t1305/* 284*/,
	RuntimeInvoker_Int32_t127_GcAchievementData_t1305/* 285*/,
	RuntimeInvoker_Void_t168_Int32_t127_GcAchievementData_t1305/* 286*/,
	RuntimeInvoker_GcScoreData_t1306_Int32_t127/* 287*/,
	RuntimeInvoker_Void_t168_GcScoreData_t1306/* 288*/,
	RuntimeInvoker_Boolean_t169_GcScoreData_t1306/* 289*/,
	RuntimeInvoker_Int32_t127_GcScoreData_t1306/* 290*/,
	RuntimeInvoker_Void_t168_Int32_t127_GcScoreData_t1306/* 291*/,
	RuntimeInvoker_IntPtr_t_Int32_t127/* 292*/,
	RuntimeInvoker_Void_t168_IntPtr_t/* 293*/,
	RuntimeInvoker_Boolean_t169_IntPtr_t/* 294*/,
	RuntimeInvoker_Int32_t127_IntPtr_t/* 295*/,
	RuntimeInvoker_Void_t168_Int32_t127_IntPtr_t/* 296*/,
	RuntimeInvoker_Keyframe_t1236_Int32_t127/* 297*/,
	RuntimeInvoker_Void_t168_Keyframe_t1236/* 298*/,
	RuntimeInvoker_Boolean_t169_Keyframe_t1236/* 299*/,
	RuntimeInvoker_Int32_t127_Keyframe_t1236/* 300*/,
	RuntimeInvoker_Void_t168_Int32_t127_Keyframe_t1236/* 301*/,
	RuntimeInvoker_Void_t168_UICharInfoU5BU5DU26_t4548_Int32_t127/* 302*/,
	RuntimeInvoker_Void_t168_UICharInfoU5BU5DU26_t4548_Int32_t127_Int32_t127/* 303*/,
	RuntimeInvoker_Int32_t127_Object_t_UICharInfo_t460_Int32_t127_Int32_t127/* 304*/,
	RuntimeInvoker_Int32_t127_UICharInfo_t460_UICharInfo_t460_Object_t/* 305*/,
	RuntimeInvoker_Void_t168_UILineInfoU5BU5DU26_t4549_Int32_t127/* 306*/,
	RuntimeInvoker_Void_t168_UILineInfoU5BU5DU26_t4549_Int32_t127_Int32_t127/* 307*/,
	RuntimeInvoker_Int32_t127_Object_t_UILineInfo_t458_Int32_t127_Int32_t127/* 308*/,
	RuntimeInvoker_Int32_t127_UILineInfo_t458_UILineInfo_t458_Object_t/* 309*/,
	RuntimeInvoker_KeyValuePair_2_t3777_Int32_t127/* 310*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3777/* 311*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3777/* 312*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3777/* 313*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3777/* 314*/,
	RuntimeInvoker_Int64_t1092_Int32_t127/* 315*/,
	RuntimeInvoker_Void_t168_Int64_t1092/* 316*/,
	RuntimeInvoker_Boolean_t169_Int64_t1092/* 317*/,
	RuntimeInvoker_Int32_t127_Int64_t1092/* 318*/,
	RuntimeInvoker_Void_t168_Int32_t127_Int64_t1092/* 319*/,
	RuntimeInvoker_KeyValuePair_2_t3815_Int32_t127/* 320*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3815/* 321*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3815/* 322*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3815/* 323*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3815/* 324*/,
	RuntimeInvoker_UInt64_t1091_Int32_t127/* 325*/,
	RuntimeInvoker_KeyValuePair_2_t3836_Int32_t127/* 326*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3836/* 327*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3836/* 328*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3836/* 329*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3836/* 330*/,
	RuntimeInvoker_ParameterModifier_t2260_Int32_t127/* 331*/,
	RuntimeInvoker_Void_t168_ParameterModifier_t2260/* 332*/,
	RuntimeInvoker_Boolean_t169_ParameterModifier_t2260/* 333*/,
	RuntimeInvoker_Int32_t127_ParameterModifier_t2260/* 334*/,
	RuntimeInvoker_Void_t168_Int32_t127_ParameterModifier_t2260/* 335*/,
	RuntimeInvoker_HitInfo_t1323_Int32_t127/* 336*/,
	RuntimeInvoker_Void_t168_HitInfo_t1323/* 337*/,
	RuntimeInvoker_Boolean_t169_HitInfo_t1323/* 338*/,
	RuntimeInvoker_Int32_t127_HitInfo_t1323/* 339*/,
	RuntimeInvoker_Void_t168_Int32_t127_HitInfo_t1323/* 340*/,
	RuntimeInvoker_UInt32_t1075_Int32_t127/* 341*/,
	RuntimeInvoker_KeyValuePair_2_t3932_Int32_t127/* 342*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3932/* 343*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3932/* 344*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3932/* 345*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3932/* 346*/,
	RuntimeInvoker_X509ChainStatus_t1902_Int32_t127/* 347*/,
	RuntimeInvoker_Void_t168_X509ChainStatus_t1902/* 348*/,
	RuntimeInvoker_Boolean_t169_X509ChainStatus_t1902/* 349*/,
	RuntimeInvoker_Int32_t127_X509ChainStatus_t1902/* 350*/,
	RuntimeInvoker_Void_t168_Int32_t127_X509ChainStatus_t1902/* 351*/,
	RuntimeInvoker_KeyValuePair_2_t3954_Int32_t127/* 352*/,
	RuntimeInvoker_Void_t168_KeyValuePair_2_t3954/* 353*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3954/* 354*/,
	RuntimeInvoker_Int32_t127_KeyValuePair_2_t3954/* 355*/,
	RuntimeInvoker_Void_t168_Int32_t127_KeyValuePair_2_t3954/* 356*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127_Int32_t127_Object_t/* 357*/,
	RuntimeInvoker_Mark_t1948_Int32_t127/* 358*/,
	RuntimeInvoker_Void_t168_Mark_t1948/* 359*/,
	RuntimeInvoker_Boolean_t169_Mark_t1948/* 360*/,
	RuntimeInvoker_Int32_t127_Mark_t1948/* 361*/,
	RuntimeInvoker_Void_t168_Int32_t127_Mark_t1948/* 362*/,
	RuntimeInvoker_UriScheme_t1984_Int32_t127/* 363*/,
	RuntimeInvoker_Void_t168_UriScheme_t1984/* 364*/,
	RuntimeInvoker_Boolean_t169_UriScheme_t1984/* 365*/,
	RuntimeInvoker_Int32_t127_UriScheme_t1984/* 366*/,
	RuntimeInvoker_Void_t168_Int32_t127_UriScheme_t1984/* 367*/,
	RuntimeInvoker_Int16_t534_Int32_t127/* 368*/,
	RuntimeInvoker_SByte_t170_Int32_t127/* 369*/,
	RuntimeInvoker_TableRange_t2069_Int32_t127/* 370*/,
	RuntimeInvoker_Void_t168_TableRange_t2069/* 371*/,
	RuntimeInvoker_Boolean_t169_TableRange_t2069/* 372*/,
	RuntimeInvoker_Int32_t127_TableRange_t2069/* 373*/,
	RuntimeInvoker_Void_t168_Int32_t127_TableRange_t2069/* 374*/,
	RuntimeInvoker_Slot_t2146_Int32_t127/* 375*/,
	RuntimeInvoker_Void_t168_Slot_t2146/* 376*/,
	RuntimeInvoker_Boolean_t169_Slot_t2146/* 377*/,
	RuntimeInvoker_Int32_t127_Slot_t2146/* 378*/,
	RuntimeInvoker_Void_t168_Int32_t127_Slot_t2146/* 379*/,
	RuntimeInvoker_Slot_t2153_Int32_t127/* 380*/,
	RuntimeInvoker_Void_t168_Slot_t2153/* 381*/,
	RuntimeInvoker_Boolean_t169_Slot_t2153/* 382*/,
	RuntimeInvoker_Int32_t127_Slot_t2153/* 383*/,
	RuntimeInvoker_Void_t168_Int32_t127_Slot_t2153/* 384*/,
	RuntimeInvoker_DateTime_t111_Int32_t127/* 385*/,
	RuntimeInvoker_Void_t168_DateTime_t111/* 386*/,
	RuntimeInvoker_Boolean_t169_DateTime_t111/* 387*/,
	RuntimeInvoker_Int32_t127_DateTime_t111/* 388*/,
	RuntimeInvoker_Void_t168_Int32_t127_DateTime_t111/* 389*/,
	RuntimeInvoker_Decimal_t1059_Int32_t127/* 390*/,
	RuntimeInvoker_Void_t168_Decimal_t1059/* 391*/,
	RuntimeInvoker_Boolean_t169_Decimal_t1059/* 392*/,
	RuntimeInvoker_Int32_t127_Decimal_t1059/* 393*/,
	RuntimeInvoker_Void_t168_Int32_t127_Decimal_t1059/* 394*/,
	RuntimeInvoker_TimeSpan_t112_Int32_t127/* 395*/,
	RuntimeInvoker_Boolean_t169_TimeSpan_t112/* 396*/,
	RuntimeInvoker_Int32_t127_TimeSpan_t112/* 397*/,
	RuntimeInvoker_Void_t168_Int32_t127_TimeSpan_t112/* 398*/,
	RuntimeInvoker_UInt16_t454/* 399*/,
	RuntimeInvoker_Boolean_t169_Nullable_1_t3108/* 400*/,
	RuntimeInvoker_Boolean_t169_Nullable_1_t3109/* 401*/,
	RuntimeInvoker_Vector3_t15/* 402*/,
	RuntimeInvoker_Vector3_t15_Object_t/* 403*/,
	RuntimeInvoker_Object_t_Vector3_t15_Object_t_Object_t/* 404*/,
	RuntimeInvoker_Single_t151_Single_t151/* 405*/,
	RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127/* 406*/,
	RuntimeInvoker_Quaternion_t13/* 407*/,
	RuntimeInvoker_Quaternion_t13_Object_t/* 408*/,
	RuntimeInvoker_Void_t168_Quaternion_t13/* 409*/,
	RuntimeInvoker_Object_t_Quaternion_t13_Object_t_Object_t/* 410*/,
	RuntimeInvoker_Object_t_Int32_t127_Object_t_Object_t/* 411*/,
	RuntimeInvoker_Double_t1407/* 412*/,
	RuntimeInvoker_Int32_t127_RaycastResult_t231_RaycastResult_t231/* 413*/,
	RuntimeInvoker_Object_t_RaycastResult_t231_RaycastResult_t231_Object_t_Object_t/* 414*/,
	RuntimeInvoker_RaycastResult_t231_Object_t/* 415*/,
	RuntimeInvoker_Enumerator_t3157/* 416*/,
	RuntimeInvoker_RaycastResult_t231/* 417*/,
	RuntimeInvoker_Boolean_t169_RaycastResult_t231_RaycastResult_t231/* 418*/,
	RuntimeInvoker_Object_t_RaycastResult_t231_Object_t_Object_t/* 419*/,
	RuntimeInvoker_KeyValuePair_2_t3197_Int32_t127_Object_t/* 420*/,
	RuntimeInvoker_Int32_t127_Int32_t127_Object_t/* 421*/,
	RuntimeInvoker_Object_t_Int32_t127_Object_t/* 422*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_ObjectU26_t1516/* 423*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_Object_t/* 424*/,
	RuntimeInvoker_Link_t2136/* 425*/,
	RuntimeInvoker_Enumerator_t3201/* 426*/,
	RuntimeInvoker_Object_t_Int32_t127_Object_t_Object_t_Object_t/* 427*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t/* 428*/,
	RuntimeInvoker_KeyValuePair_2_t3197_Object_t/* 429*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127/* 430*/,
	RuntimeInvoker_RaycastHit2D_t432/* 431*/,
	RuntimeInvoker_Int32_t127_RaycastHit_t94_RaycastHit_t94/* 432*/,
	RuntimeInvoker_Object_t_RaycastHit_t94_RaycastHit_t94_Object_t_Object_t/* 433*/,
	RuntimeInvoker_RaycastHit_t94/* 434*/,
	RuntimeInvoker_Object_t_Color_t90_Object_t_Object_t/* 435*/,
	RuntimeInvoker_KeyValuePair_2_t3228_Object_t_Int32_t127/* 436*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t127/* 437*/,
	RuntimeInvoker_Int32_t127_Object_t_Int32_t127/* 438*/,
	RuntimeInvoker_Boolean_t169_Object_t_Int32U26_t535/* 439*/,
	RuntimeInvoker_Enumerator_t3232/* 440*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int32_t127/* 441*/,
	RuntimeInvoker_KeyValuePair_2_t3228/* 442*/,
	RuntimeInvoker_Enumerator_t3231/* 443*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t/* 444*/,
	RuntimeInvoker_Enumerator_t3235/* 445*/,
	RuntimeInvoker_KeyValuePair_2_t3228_Object_t/* 446*/,
	RuntimeInvoker_KeyValuePair_2_t3254_Object_t/* 447*/,
	RuntimeInvoker_Void_t168_UIVertex_t313/* 448*/,
	RuntimeInvoker_Boolean_t169_UIVertex_t313/* 449*/,
	RuntimeInvoker_UIVertex_t313_Object_t/* 450*/,
	RuntimeInvoker_Enumerator_t3271/* 451*/,
	RuntimeInvoker_Int32_t127_UIVertex_t313/* 452*/,
	RuntimeInvoker_Void_t168_Int32_t127_UIVertex_t313/* 453*/,
	RuntimeInvoker_UIVertex_t313_Int32_t127/* 454*/,
	RuntimeInvoker_UIVertex_t313/* 455*/,
	RuntimeInvoker_Boolean_t169_UIVertex_t313_UIVertex_t313/* 456*/,
	RuntimeInvoker_Object_t_UIVertex_t313_Object_t_Object_t/* 457*/,
	RuntimeInvoker_Int32_t127_UIVertex_t313_UIVertex_t313/* 458*/,
	RuntimeInvoker_Object_t_UIVertex_t313_UIVertex_t313_Object_t_Object_t/* 459*/,
	RuntimeInvoker_Object_t_ColorTween_t253/* 460*/,
	RuntimeInvoker_Vector2_t10/* 461*/,
	RuntimeInvoker_UILineInfo_t458/* 462*/,
	RuntimeInvoker_UICharInfo_t460/* 463*/,
	RuntimeInvoker_Object_t_Single_t151_Object_t_Object_t/* 464*/,
	RuntimeInvoker_Object_t_Vector2_t10_Object_t_Object_t/* 465*/,
	RuntimeInvoker_Object_t_SByte_t170_Object_t_Object_t/* 466*/,
	RuntimeInvoker_Byte_t449_Object_t/* 467*/,
	RuntimeInvoker_Single_t151_Object_t/* 468*/,
	RuntimeInvoker_Single_t151/* 469*/,
	RuntimeInvoker_EyewearCalibrationReading_t586/* 470*/,
	RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127/* 471*/,
	RuntimeInvoker_Object_t_Int32_t127_Int32_t127_Object_t_Object_t/* 472*/,
	RuntimeInvoker_Color32_t421/* 473*/,
	RuntimeInvoker_Color_t90/* 474*/,
	RuntimeInvoker_TrackableResultData_t642/* 475*/,
	RuntimeInvoker_WordData_t647/* 476*/,
	RuntimeInvoker_WordResultData_t646/* 477*/,
	RuntimeInvoker_Enumerator_t3442/* 478*/,
	RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t_Object_t/* 479*/,
	RuntimeInvoker_Object_t_TrackableResultData_t642_Object_t_Object_t/* 480*/,
	RuntimeInvoker_SmartTerrainRevisionData_t650/* 481*/,
	RuntimeInvoker_SurfaceData_t651/* 482*/,
	RuntimeInvoker_PropData_t652/* 483*/,
	RuntimeInvoker_UInt16_t454_Object_t/* 484*/,
	RuntimeInvoker_Void_t168_Object_t_Int16_t534/* 485*/,
	RuntimeInvoker_KeyValuePair_2_t3462_Object_t_Int16_t534/* 486*/,
	RuntimeInvoker_Object_t_Object_t_Int16_t534/* 487*/,
	RuntimeInvoker_UInt16_t454_Object_t_Int16_t534/* 488*/,
	RuntimeInvoker_Boolean_t169_Object_t_UInt16U26_t2720/* 489*/,
	RuntimeInvoker_Enumerator_t3466/* 490*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int16_t534/* 491*/,
	RuntimeInvoker_KeyValuePair_2_t3462/* 492*/,
	RuntimeInvoker_Enumerator_t3465/* 493*/,
	RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t_Object_t/* 494*/,
	RuntimeInvoker_Enumerator_t3469/* 495*/,
	RuntimeInvoker_KeyValuePair_2_t3462_Object_t/* 496*/,
	RuntimeInvoker_Boolean_t169_Int16_t534_Int16_t534/* 497*/,
	RuntimeInvoker_RectangleData_t596/* 498*/,
	RuntimeInvoker_Void_t168_SmartTerrainInitializationInfo_t588/* 499*/,
	RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t588_Object_t_Object_t/* 500*/,
	RuntimeInvoker_KeyValuePair_2_t3550_Int32_t127_TrackableResultData_t642/* 501*/,
	RuntimeInvoker_Int32_t127_Int32_t127_TrackableResultData_t642/* 502*/,
	RuntimeInvoker_TrackableResultData_t642_Int32_t127_TrackableResultData_t642/* 503*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_TrackableResultDataU26_t4550/* 504*/,
	RuntimeInvoker_TrackableResultData_t642_Object_t/* 505*/,
	RuntimeInvoker_Enumerator_t3554/* 506*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_TrackableResultData_t642/* 507*/,
	RuntimeInvoker_KeyValuePair_2_t3550/* 508*/,
	RuntimeInvoker_Enumerator_t3553/* 509*/,
	RuntimeInvoker_Object_t_Int32_t127_TrackableResultData_t642_Object_t_Object_t/* 510*/,
	RuntimeInvoker_Enumerator_t3557/* 511*/,
	RuntimeInvoker_KeyValuePair_2_t3550_Object_t/* 512*/,
	RuntimeInvoker_Boolean_t169_TrackableResultData_t642_TrackableResultData_t642/* 513*/,
	RuntimeInvoker_KeyValuePair_2_t3565_Int32_t127_VirtualButtonData_t643/* 514*/,
	RuntimeInvoker_Int32_t127_Int32_t127_VirtualButtonData_t643/* 515*/,
	RuntimeInvoker_VirtualButtonData_t643_Int32_t127_VirtualButtonData_t643/* 516*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_VirtualButtonDataU26_t4551/* 517*/,
	RuntimeInvoker_VirtualButtonData_t643_Object_t/* 518*/,
	RuntimeInvoker_Enumerator_t3570/* 519*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_VirtualButtonData_t643/* 520*/,
	RuntimeInvoker_KeyValuePair_2_t3565/* 521*/,
	RuntimeInvoker_VirtualButtonData_t643/* 522*/,
	RuntimeInvoker_Enumerator_t3569/* 523*/,
	RuntimeInvoker_Object_t_Int32_t127_VirtualButtonData_t643_Object_t_Object_t/* 524*/,
	RuntimeInvoker_Enumerator_t3573/* 525*/,
	RuntimeInvoker_KeyValuePair_2_t3565_Object_t/* 526*/,
	RuntimeInvoker_Boolean_t169_VirtualButtonData_t643_VirtualButtonData_t643/* 527*/,
	RuntimeInvoker_TargetSearchResult_t720_Object_t/* 528*/,
	RuntimeInvoker_Enumerator_t3585/* 529*/,
	RuntimeInvoker_TargetSearchResult_t720/* 530*/,
	RuntimeInvoker_Boolean_t169_TargetSearchResult_t720_TargetSearchResult_t720/* 531*/,
	RuntimeInvoker_Object_t_TargetSearchResult_t720_Object_t_Object_t/* 532*/,
	RuntimeInvoker_Int32_t127_TargetSearchResult_t720_TargetSearchResult_t720/* 533*/,
	RuntimeInvoker_Object_t_TargetSearchResult_t720_TargetSearchResult_t720_Object_t_Object_t/* 534*/,
	RuntimeInvoker_WebCamDevice_t879/* 535*/,
	RuntimeInvoker_ProfileData_t734_Object_t/* 536*/,
	RuntimeInvoker_Void_t168_Object_t_ProfileData_t734/* 537*/,
	RuntimeInvoker_KeyValuePair_2_t3604_Object_t_ProfileData_t734/* 538*/,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t734/* 539*/,
	RuntimeInvoker_ProfileData_t734_Object_t_ProfileData_t734/* 540*/,
	RuntimeInvoker_Boolean_t169_Object_t_ProfileDataU26_t4552/* 541*/,
	RuntimeInvoker_Enumerator_t3609/* 542*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_ProfileData_t734/* 543*/,
	RuntimeInvoker_KeyValuePair_2_t3604/* 544*/,
	RuntimeInvoker_ProfileData_t734/* 545*/,
	RuntimeInvoker_Enumerator_t3608/* 546*/,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t734_Object_t_Object_t/* 547*/,
	RuntimeInvoker_Enumerator_t3612/* 548*/,
	RuntimeInvoker_KeyValuePair_2_t3604_Object_t/* 549*/,
	RuntimeInvoker_Boolean_t169_ProfileData_t734_ProfileData_t734/* 550*/,
	RuntimeInvoker_Link_t3653/* 551*/,
	RuntimeInvoker_UInt32_t1075/* 552*/,
	RuntimeInvoker_UInt32_t1075_Object_t/* 553*/,
	RuntimeInvoker_Vector2_t10_Object_t/* 554*/,
	RuntimeInvoker_Color2_t1000/* 555*/,
	RuntimeInvoker_Color2_t1000_Object_t/* 556*/,
	RuntimeInvoker_Void_t168_Color2_t1000/* 557*/,
	RuntimeInvoker_Object_t_Color2_t1000_Object_t_Object_t/* 558*/,
	RuntimeInvoker_Rect_t124/* 559*/,
	RuntimeInvoker_Rect_t124_Object_t/* 560*/,
	RuntimeInvoker_Void_t168_Rect_t124/* 561*/,
	RuntimeInvoker_Object_t_Rect_t124_Object_t_Object_t/* 562*/,
	RuntimeInvoker_UInt64_t1091/* 563*/,
	RuntimeInvoker_UInt64_t1091_Object_t/* 564*/,
	RuntimeInvoker_Object_t_Int64_t1092_Object_t_Object_t/* 565*/,
	RuntimeInvoker_Enumerator_t3688/* 566*/,
	RuntimeInvoker_Object_t_Int16_t534_Object_t_Object_t/* 567*/,
	RuntimeInvoker_Int32_t127_Int16_t534_Int16_t534/* 568*/,
	RuntimeInvoker_Object_t_Int16_t534_Int16_t534_Object_t_Object_t/* 569*/,
	RuntimeInvoker_Vector4_t413/* 570*/,
	RuntimeInvoker_Vector4_t413_Object_t/* 571*/,
	RuntimeInvoker_Void_t168_Vector4_t413/* 572*/,
	RuntimeInvoker_Object_t_Vector4_t413_Object_t_Object_t/* 573*/,
	RuntimeInvoker_Color_t90_Object_t/* 574*/,
	RuntimeInvoker_Int64_t1092/* 575*/,
	RuntimeInvoker_Int64_t1092_Object_t/* 576*/,
	RuntimeInvoker_GcAchievementData_t1305/* 577*/,
	RuntimeInvoker_GcScoreData_t1306/* 578*/,
	RuntimeInvoker_IntPtr_t/* 579*/,
	RuntimeInvoker_Keyframe_t1236/* 580*/,
	RuntimeInvoker_UICharInfo_t460_Object_t/* 581*/,
	RuntimeInvoker_Enumerator_t3756/* 582*/,
	RuntimeInvoker_Boolean_t169_UICharInfo_t460_UICharInfo_t460/* 583*/,
	RuntimeInvoker_Object_t_UICharInfo_t460_Object_t_Object_t/* 584*/,
	RuntimeInvoker_Int32_t127_UICharInfo_t460_UICharInfo_t460/* 585*/,
	RuntimeInvoker_Object_t_UICharInfo_t460_UICharInfo_t460_Object_t_Object_t/* 586*/,
	RuntimeInvoker_UILineInfo_t458_Object_t/* 587*/,
	RuntimeInvoker_Enumerator_t3765/* 588*/,
	RuntimeInvoker_Boolean_t169_UILineInfo_t458_UILineInfo_t458/* 589*/,
	RuntimeInvoker_Object_t_UILineInfo_t458_Object_t_Object_t/* 590*/,
	RuntimeInvoker_Int32_t127_UILineInfo_t458_UILineInfo_t458/* 591*/,
	RuntimeInvoker_Object_t_UILineInfo_t458_UILineInfo_t458_Object_t_Object_t/* 592*/,
	RuntimeInvoker_Void_t168_Object_t_Int64_t1092/* 593*/,
	RuntimeInvoker_KeyValuePair_2_t3777_Object_t_Int64_t1092/* 594*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1092/* 595*/,
	RuntimeInvoker_Int64_t1092_Object_t_Int64_t1092/* 596*/,
	RuntimeInvoker_Boolean_t169_Object_t_Int64U26_t2703/* 597*/,
	RuntimeInvoker_Enumerator_t3782/* 598*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_Int64_t1092/* 599*/,
	RuntimeInvoker_KeyValuePair_2_t3777/* 600*/,
	RuntimeInvoker_Enumerator_t3781/* 601*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t_Object_t/* 602*/,
	RuntimeInvoker_Enumerator_t3785/* 603*/,
	RuntimeInvoker_KeyValuePair_2_t3777_Object_t/* 604*/,
	RuntimeInvoker_Boolean_t169_Int64_t1092_Int64_t1092/* 605*/,
	RuntimeInvoker_Object_t_Int64_t1092/* 606*/,
	RuntimeInvoker_Void_t168_Int64_t1092_Object_t/* 607*/,
	RuntimeInvoker_KeyValuePair_2_t3815_Int64_t1092_Object_t/* 608*/,
	RuntimeInvoker_UInt64_t1091_Int64_t1092_Object_t/* 609*/,
	RuntimeInvoker_Object_t_Int64_t1092_Object_t/* 610*/,
	RuntimeInvoker_Boolean_t169_Int64_t1092_ObjectU26_t1516/* 611*/,
	RuntimeInvoker_Enumerator_t3820/* 612*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int64_t1092_Object_t/* 613*/,
	RuntimeInvoker_KeyValuePair_2_t3815/* 614*/,
	RuntimeInvoker_Enumerator_t3819/* 615*/,
	RuntimeInvoker_Object_t_Int64_t1092_Object_t_Object_t_Object_t/* 616*/,
	RuntimeInvoker_Enumerator_t3823/* 617*/,
	RuntimeInvoker_KeyValuePair_2_t3815_Object_t/* 618*/,
	RuntimeInvoker_KeyValuePair_2_t3836/* 619*/,
	RuntimeInvoker_Void_t168_Object_t_KeyValuePair_2_t3254/* 620*/,
	RuntimeInvoker_KeyValuePair_2_t3836_Object_t_KeyValuePair_2_t3254/* 621*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3254/* 622*/,
	RuntimeInvoker_KeyValuePair_2_t3254_Object_t_KeyValuePair_2_t3254/* 623*/,
	RuntimeInvoker_Boolean_t169_Object_t_KeyValuePair_2U26_t4553/* 624*/,
	RuntimeInvoker_Enumerator_t3864/* 625*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_KeyValuePair_2_t3254/* 626*/,
	RuntimeInvoker_Enumerator_t3863/* 627*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t3254_Object_t_Object_t/* 628*/,
	RuntimeInvoker_Enumerator_t3867/* 629*/,
	RuntimeInvoker_KeyValuePair_2_t3836_Object_t/* 630*/,
	RuntimeInvoker_Boolean_t169_KeyValuePair_2_t3254_KeyValuePair_2_t3254/* 631*/,
	RuntimeInvoker_ParameterModifier_t2260/* 632*/,
	RuntimeInvoker_HitInfo_t1323/* 633*/,
	RuntimeInvoker_Void_t168_Object_t_SByte_t170/* 634*/,
	RuntimeInvoker_KeyValuePair_2_t3932_Object_t_SByte_t170/* 635*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t170/* 636*/,
	RuntimeInvoker_Byte_t449_Object_t_SByte_t170/* 637*/,
	RuntimeInvoker_Boolean_t169_Object_t_ByteU26_t1840/* 638*/,
	RuntimeInvoker_Enumerator_t3936/* 639*/,
	RuntimeInvoker_DictionaryEntry_t1996_Object_t_SByte_t170/* 640*/,
	RuntimeInvoker_KeyValuePair_2_t3932/* 641*/,
	RuntimeInvoker_Enumerator_t3935/* 642*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t_Object_t/* 643*/,
	RuntimeInvoker_Enumerator_t3939/* 644*/,
	RuntimeInvoker_KeyValuePair_2_t3932_Object_t/* 645*/,
	RuntimeInvoker_Boolean_t169_SByte_t170_SByte_t170/* 646*/,
	RuntimeInvoker_X509ChainStatus_t1902/* 647*/,
	RuntimeInvoker_KeyValuePair_2_t3954_Int32_t127_Int32_t127/* 648*/,
	RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535/* 649*/,
	RuntimeInvoker_Enumerator_t3958/* 650*/,
	RuntimeInvoker_DictionaryEntry_t1996_Int32_t127_Int32_t127/* 651*/,
	RuntimeInvoker_KeyValuePair_2_t3954/* 652*/,
	RuntimeInvoker_Enumerator_t3957/* 653*/,
	RuntimeInvoker_Enumerator_t3961/* 654*/,
	RuntimeInvoker_KeyValuePair_2_t3954_Object_t/* 655*/,
	RuntimeInvoker_Mark_t1948/* 656*/,
	RuntimeInvoker_UriScheme_t1984/* 657*/,
	RuntimeInvoker_Int16_t534/* 658*/,
	RuntimeInvoker_SByte_t170/* 659*/,
	RuntimeInvoker_TableRange_t2069/* 660*/,
	RuntimeInvoker_Slot_t2146/* 661*/,
	RuntimeInvoker_Slot_t2153/* 662*/,
	RuntimeInvoker_DateTime_t111/* 663*/,
	RuntimeInvoker_Decimal_t1059/* 664*/,
	RuntimeInvoker_Int32_t127_DateTime_t111_DateTime_t111/* 665*/,
	RuntimeInvoker_Boolean_t169_DateTime_t111_DateTime_t111/* 666*/,
	RuntimeInvoker_Int32_t127_DateTimeOffset_t1420_DateTimeOffset_t1420/* 667*/,
	RuntimeInvoker_Int32_t127_DateTimeOffset_t1420/* 668*/,
	RuntimeInvoker_Boolean_t169_DateTimeOffset_t1420_DateTimeOffset_t1420/* 669*/,
	RuntimeInvoker_Boolean_t169_Nullable_1_t2598/* 670*/,
	RuntimeInvoker_Int32_t127_Guid_t108_Guid_t108/* 671*/,
	RuntimeInvoker_Int32_t127_Guid_t108/* 672*/,
	RuntimeInvoker_Boolean_t169_Guid_t108_Guid_t108/* 673*/,
	RuntimeInvoker_Int32_t127_TimeSpan_t112_TimeSpan_t112/* 674*/,
	RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* 675*/,
};
const Il2CppCodeRegistration g_CodeRegistration = 
{
	4711,
	g_Il2CppMethodPointers,
	676,
	g_Il2CppInvokerPointers,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
