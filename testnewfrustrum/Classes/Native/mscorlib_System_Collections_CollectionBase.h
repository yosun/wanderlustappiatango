﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1668;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CollectionBase
struct  CollectionBase_t1701  : public Object_t
{
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t1668 * ___list_0;
};
