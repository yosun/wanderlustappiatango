﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3257;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3253;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16928_gshared (Enumerator_t3257 * __this, Dictionary_2_t3253 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16928(__this, ___host, method) (( void (*) (Enumerator_t3257 *, Dictionary_2_t3253 *, const MethodInfo*))Enumerator__ctor_m16928_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16929_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16929(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16929_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16930_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16930(__this, method) (( void (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_Dispose_m16930_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16931_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16931(__this, method) (( bool (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_MoveNext_m16931_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16932_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16932(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_get_Current_m16932_gshared)(__this, method)
