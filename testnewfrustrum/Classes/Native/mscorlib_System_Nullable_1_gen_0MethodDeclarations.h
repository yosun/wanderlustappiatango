﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<DG.Tweening.LogBehaviour>
struct Nullable_1_t118;
// System.Object
struct Object_t;
// System.String
struct String_t;
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0.h"

// System.Void System.Nullable`1<DG.Tweening.LogBehaviour>::.ctor(T)
// System.Nullable`1<System.Int32>
#include "mscorlib_System_Nullable_1_gen_3MethodDeclarations.h"
#define Nullable_1__ctor_m358(__this, ___value, method) (( void (*) (Nullable_1_t118 *, int32_t, const MethodInfo*))Nullable_1__ctor_m14908_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::get_HasValue()
#define Nullable_1_get_HasValue_m5557(__this, method) (( bool (*) (Nullable_1_t118 *, const MethodInfo*))Nullable_1_get_HasValue_m14909_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LogBehaviour>::get_Value()
#define Nullable_1_get_Value_m5558(__this, method) (( int32_t (*) (Nullable_1_t118 *, const MethodInfo*))Nullable_1_get_Value_m14910_gshared)(__this, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Object)
#define Nullable_1_Equals_m14911(__this, ___other, method) (( bool (*) (Nullable_1_t118 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m14912_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m14913(__this, ___other, method) (( bool (*) (Nullable_1_t118 *, Nullable_1_t118 , const MethodInfo*))Nullable_1_Equals_m14914_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<DG.Tweening.LogBehaviour>::GetHashCode()
#define Nullable_1_GetHashCode_m14915(__this, method) (( int32_t (*) (Nullable_1_t118 *, const MethodInfo*))Nullable_1_GetHashCode_m14916_gshared)(__this, method)
// System.String System.Nullable`1<DG.Tweening.LogBehaviour>::ToString()
#define Nullable_1_ToString_m14917(__this, method) (( String_t* (*) (Nullable_1_t118 *, const MethodInfo*))Nullable_1_ToString_m14918_gshared)(__this, method)
