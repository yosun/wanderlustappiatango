﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1659;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Math.BigInteger/ModulusRing
struct  ModulusRing_t1660  : public Object_t
{
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t1659 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t1659 * ___constant_1;
};
