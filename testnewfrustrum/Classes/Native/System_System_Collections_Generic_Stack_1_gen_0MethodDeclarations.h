﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1430;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4137;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Type>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m7014(__this, method) (( void (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1__ctor_m15420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m26581(__this, method) (( bool (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15421_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m26582(__this, method) (( Object_t * (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15422_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m26583(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1430 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15423_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26584(__this, method) (( Object_t* (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15424_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m26585(__this, method) (( Object_t * (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15425_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Peek()
#define Stack_1_Peek_m26586(__this, method) (( Type_t * (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_Peek_m15426_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m7016(__this, method) (( Type_t * (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_Pop_m15427_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m7015(__this, ___t, method) (( void (*) (Stack_1_t1430 *, Type_t *, const MethodInfo*))Stack_1_Push_m15428_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m26587(__this, method) (( int32_t (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_get_Count_m15429_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
#define Stack_1_GetEnumerator_m26588(__this, method) (( Enumerator_t3884  (*) (Stack_1_t1430 *, const MethodInfo*))Stack_1_GetEnumerator_m15430_gshared)(__this, method)
