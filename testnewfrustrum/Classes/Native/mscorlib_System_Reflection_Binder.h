﻿#pragma once
#include <stdint.h>
// System.Reflection.Binder
struct Binder_t1431;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Binder
struct  Binder_t1431  : public Object_t
{
};
struct Binder_t1431_StaticFields{
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t1431 * ___default_binder_0;
};
