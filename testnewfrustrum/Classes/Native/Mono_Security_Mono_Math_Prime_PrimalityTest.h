﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1659;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t1788  : public MulticastDelegate_t307
{
};
