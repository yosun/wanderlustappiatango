﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1030;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m5522_gshared (DOSetter_1_t1030 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m5522(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1030 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m5522_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m14940_gshared (DOSetter_1_t1030 * __this, Quaternion_t13  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m14940(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1030 *, Quaternion_t13 , const MethodInfo*))DOSetter_1_Invoke_m14940_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m14941_gshared (DOSetter_1_t1030 * __this, Quaternion_t13  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m14941(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1030 *, Quaternion_t13 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m14941_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m14942_gshared (DOSetter_1_t1030 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m14942(__this, ___result, method) (( void (*) (DOSetter_1_t1030 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m14942_gshared)(__this, ___result, method)
