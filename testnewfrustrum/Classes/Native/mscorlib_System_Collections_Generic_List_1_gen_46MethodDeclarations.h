﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t988;
// System.Object
struct Object_t;
// DG.Tweening.TweenCallback
struct TweenCallback_t101;
// System.Collections.Generic.IEnumerable`1<DG.Tweening.TweenCallback>
struct IEnumerable_1_t4283;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.TweenCallback>
struct IEnumerator_1_t4284;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<DG.Tweening.TweenCallback>
struct ICollection_1_t4285;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>
struct ReadOnlyCollection_1_t3665;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t3663;
// System.Predicate`1<DG.Tweening.TweenCallback>
struct Predicate_1_t3666;
// System.Comparison`1<DG.Tweening.TweenCallback>
struct Comparison_1_t3668;
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"

// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m5553(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23321(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor(System.Int32)
#define List_1__ctor_m23322(__this, ___capacity, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.cctor()
#define List_1__cctor_m23323(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23324(__this, method) (( Object_t* (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23325(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t988 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23326(__this, method) (( Object_t * (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23327(__this, ___item, method) (( int32_t (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23328(__this, ___item, method) (( bool (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23329(__this, ___item, method) (( int32_t (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23330(__this, ___index, ___item, method) (( void (*) (List_1_t988 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23331(__this, ___item, method) (( void (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23332(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23333(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23334(__this, method) (( Object_t * (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23335(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23336(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23337(__this, ___index, method) (( Object_t * (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23338(__this, ___index, ___value, method) (( void (*) (List_1_t988 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Add(T)
#define List_1_Add_m23339(__this, ___item, method) (( void (*) (List_1_t988 *, TweenCallback_t101 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23340(__this, ___newCount, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23341(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23342(__this, ___enumerable, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23343(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AsReadOnly()
#define List_1_AsReadOnly_m23344(__this, method) (( ReadOnlyCollection_1_t3665 * (*) (List_1_t988 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Clear()
#define List_1_Clear_m23345(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Contains(T)
#define List_1_Contains_m23346(__this, ___item, method) (( bool (*) (List_1_t988 *, TweenCallback_t101 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23347(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t988 *, TweenCallbackU5BU5D_t3663*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Find(System.Predicate`1<T>)
#define List_1_Find_m23348(__this, ___match, method) (( TweenCallback_t101 * (*) (List_1_t988 *, Predicate_1_t3666 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23349(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3666 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23350(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t988 *, int32_t, int32_t, Predicate_1_t3666 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GetEnumerator()
#define List_1_GetEnumerator_m23351(__this, method) (( Enumerator_t3667  (*) (List_1_t988 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::IndexOf(T)
#define List_1_IndexOf_m23352(__this, ___item, method) (( int32_t (*) (List_1_t988 *, TweenCallback_t101 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23353(__this, ___start, ___delta, method) (( void (*) (List_1_t988 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23354(__this, ___index, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Insert(System.Int32,T)
#define List_1_Insert_m23355(__this, ___index, ___item, method) (( void (*) (List_1_t988 *, int32_t, TweenCallback_t101 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23356(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Remove(T)
#define List_1_Remove_m23357(__this, ___item, method) (( bool (*) (List_1_t988 *, TweenCallback_t101 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23358(__this, ___match, method) (( int32_t (*) (List_1_t988 *, Predicate_1_t3666 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23359(__this, ___index, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Reverse()
#define List_1_Reverse_m23360(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Sort()
#define List_1_Sort_m23361(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23362(__this, ___comparison, method) (( void (*) (List_1_t988 *, Comparison_1_t3668 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::ToArray()
#define List_1_ToArray_m23363(__this, method) (( TweenCallbackU5BU5D_t3663* (*) (List_1_t988 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::TrimExcess()
#define List_1_TrimExcess_m23364(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Capacity()
#define List_1_get_Capacity_m23365(__this, method) (( int32_t (*) (List_1_t988 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23366(__this, ___value, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Count()
#define List_1_get_Count_m23367(__this, method) (( int32_t (*) (List_1_t988 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Item(System.Int32)
#define List_1_get_Item_m23368(__this, ___index, method) (( TweenCallback_t101 * (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::set_Item(System.Int32,T)
#define List_1_set_Item_m23369(__this, ___index, ___value, method) (( void (*) (List_1_t988 *, int32_t, TweenCallback_t101 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
