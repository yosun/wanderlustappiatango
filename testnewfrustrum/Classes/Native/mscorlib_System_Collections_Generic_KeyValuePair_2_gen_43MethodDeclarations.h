﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t3891;
// UnityEngine.Event
struct Event_t317;
struct Event_t317_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26658(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3891 *, Event_t317 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16576_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m26659(__this, method) (( Event_t317 * (*) (KeyValuePair_2_t3891 *, const MethodInfo*))KeyValuePair_2_get_Key_m16577_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26660(__this, ___value, method) (( void (*) (KeyValuePair_2_t3891 *, Event_t317 *, const MethodInfo*))KeyValuePair_2_set_Key_m16578_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m26661(__this, method) (( int32_t (*) (KeyValuePair_2_t3891 *, const MethodInfo*))KeyValuePair_2_get_Value_m16579_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26662(__this, ___value, method) (( void (*) (KeyValuePair_2_t3891 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16580_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m26663(__this, method) (( String_t* (*) (KeyValuePair_2_t3891 *, const MethodInfo*))KeyValuePair_2_ToString_m16581_gshared)(__this, method)
