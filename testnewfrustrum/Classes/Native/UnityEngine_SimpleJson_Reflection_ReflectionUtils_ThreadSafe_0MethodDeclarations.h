﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t1410;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4138;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t4365;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t1286;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t1409;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t4366;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t4367;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m6999(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, ThreadSafeDictionaryValueFactory_2_t1409 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m25973_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25974(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m25976(__this, ___key, method) (( ConstructorDelegate_t1286 * (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m25977_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m25978(__this, ___key, method) (( ConstructorDelegate_t1286 * (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m25979_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m25980(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m25981_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m25982(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m25983_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m25984(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m25985_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m25986(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, ConstructorDelegate_t1286 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m25987_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m25988(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m25989_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m25990(__this, ___key, method) (( ConstructorDelegate_t1286 * (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m25991_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m25992(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m25993_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m25994(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, KeyValuePair_2_t3843 , const MethodInfo*))ThreadSafeDictionary_2_Add_m25995_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m25996(__this, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m25997_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m25998(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1410 *, KeyValuePair_2_t3843 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m25999_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m26000(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t1410 *, KeyValuePair_2U5BU5D_t4366*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m26001_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m26002(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m26003_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m26004(__this, method) (( bool (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m26005_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m26006(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1410 *, KeyValuePair_2_t3843 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m26007_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m26008(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1410 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m26009_gshared)(__this, method)
