﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>
struct InternalEnumerator_1_t3714;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1317;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m24150(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3714 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24151(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3714 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>::Dispose()
#define InternalEnumerator_1_Dispose_m24152(__this, method) (( void (*) (InternalEnumerator_1_t3714 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24153(__this, method) (( bool (*) (InternalEnumerator_1_t3714 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Achievement>::get_Current()
#define InternalEnumerator_1_get_Current_m24154(__this, method) (( Achievement_t1317 * (*) (InternalEnumerator_1_t3714 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
