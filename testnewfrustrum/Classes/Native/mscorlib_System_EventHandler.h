﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1688;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.EventHandler
struct  EventHandler_t2490  : public MulticastDelegate_t307
{
};
