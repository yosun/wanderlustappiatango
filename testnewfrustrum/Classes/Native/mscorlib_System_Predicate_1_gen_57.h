﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t616;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Byte[]>
struct  Predicate_1_t3744  : public MulticastDelegate_t307
{
};
