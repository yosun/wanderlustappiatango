﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t799;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t731;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t764;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButton>
struct IEnumerator_1_t883;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.VirtualButton>
struct ICollection_1_t4140;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButton>
struct ReadOnlyCollection_1_t3378;
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3376;
// System.Predicate`1<Vuforia.VirtualButton>
struct Predicate_1_t3379;
// System.Comparison`1<Vuforia.VirtualButton>
struct Comparison_1_t3381;
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_44.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4350(__this, method) (( void (*) (List_1_t799 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18668(__this, ___collection, method) (( void (*) (List_1_t799 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Int32)
#define List_1__ctor_m18669(__this, ___capacity, method) (( void (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.cctor()
#define List_1__cctor_m18670(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18671(__this, method) (( Object_t* (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18672(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t799 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18673(__this, method) (( Object_t * (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18674(__this, ___item, method) (( int32_t (*) (List_1_t799 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18675(__this, ___item, method) (( bool (*) (List_1_t799 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18676(__this, ___item, method) (( int32_t (*) (List_1_t799 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18677(__this, ___index, ___item, method) (( void (*) (List_1_t799 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18678(__this, ___item, method) (( void (*) (List_1_t799 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18679(__this, method) (( bool (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18680(__this, method) (( bool (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18681(__this, method) (( Object_t * (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18682(__this, method) (( bool (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18683(__this, method) (( bool (*) (List_1_t799 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18684(__this, ___index, method) (( Object_t * (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18685(__this, ___index, ___value, method) (( void (*) (List_1_t799 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Add(T)
#define List_1_Add_m18686(__this, ___item, method) (( void (*) (List_1_t799 *, VirtualButton_t731 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18687(__this, ___newCount, method) (( void (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18688(__this, ___collection, method) (( void (*) (List_1_t799 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18689(__this, ___enumerable, method) (( void (*) (List_1_t799 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18690(__this, ___collection, method) (( void (*) (List_1_t799 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::AsReadOnly()
#define List_1_AsReadOnly_m18691(__this, method) (( ReadOnlyCollection_1_t3378 * (*) (List_1_t799 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Clear()
#define List_1_Clear_m18692(__this, method) (( void (*) (List_1_t799 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Contains(T)
#define List_1_Contains_m18693(__this, ___item, method) (( bool (*) (List_1_t799 *, VirtualButton_t731 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18694(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t799 *, VirtualButtonU5BU5D_t3376*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::Find(System.Predicate`1<T>)
#define List_1_Find_m18695(__this, ___match, method) (( VirtualButton_t731 * (*) (List_1_t799 *, Predicate_1_t3379 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18696(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3379 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18697(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t799 *, int32_t, int32_t, Predicate_1_t3379 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetEnumerator()
#define List_1_GetEnumerator_m18698(__this, method) (( Enumerator_t3380  (*) (List_1_t799 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::IndexOf(T)
#define List_1_IndexOf_m18699(__this, ___item, method) (( int32_t (*) (List_1_t799 *, VirtualButton_t731 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18700(__this, ___start, ___delta, method) (( void (*) (List_1_t799 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18701(__this, ___index, method) (( void (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Insert(System.Int32,T)
#define List_1_Insert_m18702(__this, ___index, ___item, method) (( void (*) (List_1_t799 *, int32_t, VirtualButton_t731 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18703(__this, ___collection, method) (( void (*) (List_1_t799 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Remove(T)
#define List_1_Remove_m18704(__this, ___item, method) (( bool (*) (List_1_t799 *, VirtualButton_t731 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18705(__this, ___match, method) (( int32_t (*) (List_1_t799 *, Predicate_1_t3379 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18706(__this, ___index, method) (( void (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Reverse()
#define List_1_Reverse_m18707(__this, method) (( void (*) (List_1_t799 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort()
#define List_1_Sort_m18708(__this, method) (( void (*) (List_1_t799 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18709(__this, ___comparison, method) (( void (*) (List_1_t799 *, Comparison_1_t3381 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::ToArray()
#define List_1_ToArray_m18710(__this, method) (( VirtualButtonU5BU5D_t3376* (*) (List_1_t799 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::TrimExcess()
#define List_1_TrimExcess_m18711(__this, method) (( void (*) (List_1_t799 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Capacity()
#define List_1_get_Capacity_m18712(__this, method) (( int32_t (*) (List_1_t799 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18713(__this, ___value, method) (( void (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Count()
#define List_1_get_Count_m18714(__this, method) (( int32_t (*) (List_1_t799 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Item(System.Int32)
#define List_1_get_Item_m18715(__this, ___index, method) (( VirtualButton_t731 * (*) (List_1_t799 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Item(System.Int32,T)
#define List_1_set_Item_m18716(__this, ___index, ___value, method) (( void (*) (List_1_t799 *, int32_t, VirtualButton_t731 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
