﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3593;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m22316_gshared (DefaultComparer_t3593 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22316(__this, method) (( void (*) (DefaultComparer_t3593 *, const MethodInfo*))DefaultComparer__ctor_m22316_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22317_gshared (DefaultComparer_t3593 * __this, TargetSearchResult_t720  ___x, TargetSearchResult_t720  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22317(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3593 *, TargetSearchResult_t720 , TargetSearchResult_t720 , const MethodInfo*))DefaultComparer_Compare_m22317_gshared)(__this, ___x, ___y, method)
