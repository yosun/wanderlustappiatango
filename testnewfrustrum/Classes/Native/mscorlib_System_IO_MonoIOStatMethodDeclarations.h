﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t2197;
struct MonoIOStat_t2197_marshaled;

void MonoIOStat_t2197_marshal(const MonoIOStat_t2197& unmarshaled, MonoIOStat_t2197_marshaled& marshaled);
void MonoIOStat_t2197_marshal_back(const MonoIOStat_t2197_marshaled& marshaled, MonoIOStat_t2197& unmarshaled);
void MonoIOStat_t2197_marshal_cleanup(MonoIOStat_t2197_marshaled& marshaled);
