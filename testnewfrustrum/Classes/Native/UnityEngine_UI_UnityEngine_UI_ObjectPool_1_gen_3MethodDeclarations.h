﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3147;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3146;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" void ObjectPool_1__ctor_m15407_gshared (ObjectPool_1_t3147 * __this, UnityAction_1_t3146 * ___actionOnGet, UnityAction_1_t3146 * ___actionOnRelease, const MethodInfo* method);
#define ObjectPool_1__ctor_m15407(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3147 *, UnityAction_1_t3146 *, UnityAction_1_t3146 *, const MethodInfo*))ObjectPool_1__ctor_m15407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" int32_t ObjectPool_1_get_countAll_m15409_gshared (ObjectPool_1_t3147 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countAll_m15409(__this, method) (( int32_t (*) (ObjectPool_1_t3147 *, const MethodInfo*))ObjectPool_1_get_countAll_m15409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" void ObjectPool_1_set_countAll_m15411_gshared (ObjectPool_1_t3147 * __this, int32_t ___value, const MethodInfo* method);
#define ObjectPool_1_set_countAll_m15411(__this, ___value, method) (( void (*) (ObjectPool_1_t3147 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" int32_t ObjectPool_1_get_countActive_m15413_gshared (ObjectPool_1_t3147 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countActive_m15413(__this, method) (( int32_t (*) (ObjectPool_1_t3147 *, const MethodInfo*))ObjectPool_1_get_countActive_m15413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" int32_t ObjectPool_1_get_countInactive_m15415_gshared (ObjectPool_1_t3147 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countInactive_m15415(__this, method) (( int32_t (*) (ObjectPool_1_t3147 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" Object_t * ObjectPool_1_Get_m15417_gshared (ObjectPool_1_t3147 * __this, const MethodInfo* method);
#define ObjectPool_1_Get_m15417(__this, method) (( Object_t * (*) (ObjectPool_1_t3147 *, const MethodInfo*))ObjectPool_1_Get_m15417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" void ObjectPool_1_Release_m15419_gshared (ObjectPool_1_t3147 * __this, Object_t * ___element, const MethodInfo* method);
#define ObjectPool_1_Release_m15419(__this, ___element, method) (( void (*) (ObjectPool_1_t3147 *, Object_t *, const MethodInfo*))ObjectPool_1_Release_m15419_gshared)(__this, ___element, method)
