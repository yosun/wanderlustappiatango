﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.Word>
struct Comparison_1_t3493;
// System.Object
struct Object_t;
// Vuforia.Word
struct Word_t696;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Vuforia.Word>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m20541(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3493 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15149_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.Word>::Invoke(T,T)
#define Comparison_1_Invoke_m20542(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3493 *, Object_t *, Object_t *, const MethodInfo*))Comparison_1_Invoke_m15150_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.Word>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20543(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3493 *, Object_t *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15151_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.Word>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20544(__this, ___result, method) (( int32_t (*) (Comparison_1_t3493 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15152_gshared)(__this, ___result, method)
