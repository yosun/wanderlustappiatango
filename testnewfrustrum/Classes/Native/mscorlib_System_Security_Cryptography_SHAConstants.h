﻿#pragma once
#include <stdint.h>
// System.UInt32[]
struct UInt32U5BU5D_t1662;
// System.UInt64[]
struct UInt64U5BU5D_t2422;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.SHAConstants
struct  SHAConstants_t2426  : public Object_t
{
};
struct SHAConstants_t2426_StaticFields{
	// System.UInt32[] System.Security.Cryptography.SHAConstants::K1
	UInt32U5BU5D_t1662* ___K1_0;
	// System.UInt64[] System.Security.Cryptography.SHAConstants::K2
	UInt64U5BU5D_t2422* ___K2_1;
};
