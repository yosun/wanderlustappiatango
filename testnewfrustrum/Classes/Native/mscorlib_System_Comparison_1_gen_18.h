﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup
struct CanvasGroup_t442;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.CanvasGroup>
struct  Comparison_1_t3326  : public MulticastDelegate_t307
{
};
