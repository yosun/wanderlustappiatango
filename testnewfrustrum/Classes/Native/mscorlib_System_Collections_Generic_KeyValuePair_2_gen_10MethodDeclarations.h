﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct KeyValuePair_2_t3264;
// UnityEngine.Font
struct Font_t266;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t437;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17035(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3264 *, Font_t266 *, List_1_t437 *, const MethodInfo*))KeyValuePair_2__ctor_m16908_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m17036(__this, method) (( Font_t266 * (*) (KeyValuePair_2_t3264 *, const MethodInfo*))KeyValuePair_2_get_Key_m16909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17037(__this, ___value, method) (( void (*) (KeyValuePair_2_t3264 *, Font_t266 *, const MethodInfo*))KeyValuePair_2_set_Key_m16910_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m17038(__this, method) (( List_1_t437 * (*) (KeyValuePair_2_t3264 *, const MethodInfo*))KeyValuePair_2_get_Value_m16911_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17039(__this, ___value, method) (( void (*) (KeyValuePair_2_t3264 *, List_1_t437 *, const MethodInfo*))KeyValuePair_2_set_Value_m16912_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m17040(__this, method) (( String_t* (*) (KeyValuePair_2_t3264 *, const MethodInfo*))KeyValuePair_2_ToString_m16913_gshared)(__this, method)
