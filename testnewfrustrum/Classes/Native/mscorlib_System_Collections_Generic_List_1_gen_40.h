﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t3627;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct  List_1_t745  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_items
	ITrackerEventHandlerU5BU5D_t3627* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t745_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::EmptyArray
	ITrackerEventHandlerU5BU5D_t3627* ___EmptyArray_4;
};
