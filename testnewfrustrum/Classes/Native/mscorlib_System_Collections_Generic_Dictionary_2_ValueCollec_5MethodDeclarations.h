﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>
struct ValueCollection_t812;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t630;
// Vuforia.Marker
struct Marker_t739;
// System.Collections.Generic.IEnumerator`1<Vuforia.Marker>
struct IEnumerator_1_t4169;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.Marker[]
struct MarkerU5BU5D_t3430;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"
#define ValueCollection__ctor_m19647(__this, ___dictionary, method) (( void (*) (ValueCollection_t812 *, Dictionary_2_t630 *, const MethodInfo*))ValueCollection__ctor_m16275_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19648(__this, ___item, method) (( void (*) (ValueCollection_t812 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16276_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19649(__this, method) (( void (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16277_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19650(__this, ___item, method) (( bool (*) (ValueCollection_t812 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16278_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19651(__this, ___item, method) (( bool (*) (ValueCollection_t812 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16279_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19652(__this, method) (( Object_t* (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m19653(__this, ___array, ___index, method) (( void (*) (ValueCollection_t812 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16281_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19654(__this, method) (( Object_t * (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19655(__this, method) (( bool (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19656(__this, method) (( bool (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16284_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m19657(__this, method) (( Object_t * (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16285_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m19658(__this, ___array, ___index, method) (( void (*) (ValueCollection_t812 *, MarkerU5BU5D_t3430*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16286_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4391(__this, method) (( Enumerator_t811  (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_GetEnumerator_m16287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>::get_Count()
#define ValueCollection_get_Count_m19659(__this, method) (( int32_t (*) (ValueCollection_t812 *, const MethodInfo*))ValueCollection_get_Count_m16288_gshared)(__this, method)
