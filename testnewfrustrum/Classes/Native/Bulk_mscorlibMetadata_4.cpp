﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition System.Runtime.InteropServices._PropertyBuilder
extern TypeInfo _PropertyBuilder_t2669_il2cpp_TypeInfo;
static const MethodInfo* _PropertyBuilder_t2669_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyBuilder_t2669_0_0_0;
extern const Il2CppType _PropertyBuilder_t2669_1_0_0;
struct _PropertyBuilder_t2669;
const Il2CppTypeDefinitionMetadata _PropertyBuilder_t2669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyBuilder_t2669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyBuilder_t2669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyBuilder_t2669_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 502/* custom_attributes_cache */
	, &_PropertyBuilder_t2669_0_0_0/* byval_arg */
	, &_PropertyBuilder_t2669_1_0_0/* this_arg */
	, &_PropertyBuilder_t2669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyInfo
extern TypeInfo _PropertyInfo_t2680_il2cpp_TypeInfo;
static const MethodInfo* _PropertyInfo_t2680_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyInfo_t2680_0_0_0;
extern const Il2CppType _PropertyInfo_t2680_1_0_0;
struct _PropertyInfo_t2680;
const Il2CppTypeDefinitionMetadata _PropertyInfo_t2680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyInfo_t2680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyInfo_t2680_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyInfo_t2680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 503/* custom_attributes_cache */
	, &_PropertyInfo_t2680_0_0_0/* byval_arg */
	, &_PropertyInfo_t2680_1_0_0/* this_arg */
	, &_PropertyInfo_t2680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t2682_il2cpp_TypeInfo;
static const MethodInfo* _Thread_t2682_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Thread_t2682_0_0_0;
extern const Il2CppType _Thread_t2682_1_0_0;
struct _Thread_t2682;
const Il2CppTypeDefinitionMetadata _Thread_t2682_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Thread_t2682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Thread_t2682_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Thread_t2682_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 504/* custom_attributes_cache */
	, &_Thread_t2682_0_0_0/* byval_arg */
	, &_Thread_t2682_1_0_0/* this_arg */
	, &_Thread_t2682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t2670_il2cpp_TypeInfo;
static const MethodInfo* _TypeBuilder_t2670_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _TypeBuilder_t2670_0_0_0;
extern const Il2CppType _TypeBuilder_t2670_1_0_0;
struct _TypeBuilder_t2670;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t2670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _TypeBuilder_t2670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _TypeBuilder_t2670_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_TypeBuilder_t2670_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 505/* custom_attributes_cache */
	, &_TypeBuilder_t2670_0_0_0/* byval_arg */
	, &_TypeBuilder_t2670_1_0_0/* this_arg */
	, &_TypeBuilder_t2670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t2293_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
extern const Il2CppType IActivator_t2292_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::get_ConstructionActivator()
extern const MethodInfo ActivationServices_get_ConstructionActivator_m12089_MethodInfo = 
{
	"get_ConstructionActivator"/* name */
	, (methodPointerType)&ActivationServices_get_ConstructionActivator_m12089/* method */
	, &ActivationServices_t2293_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2193/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo ActivationServices_t2293_ActivationServices_CreateProxyFromAttributes_m12090_ParameterInfos[] = 
{
	{"type", 0, 134221933, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134221934, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern const Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyFromAttributes(System.Type,System.Object[])
extern const MethodInfo ActivationServices_CreateProxyFromAttributes_m12090_MethodInfo = 
{
	"CreateProxyFromAttributes"/* name */
	, (methodPointerType)&ActivationServices_CreateProxyFromAttributes_m12090/* method */
	, &ActivationServices_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2293_ActivationServices_CreateProxyFromAttributes_m12090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo ActivationServices_t2293_ActivationServices_CreateConstructionCall_m12091_ParameterInfos[] = 
{
	{"type", 0, 134221935, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134221936, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134221937, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern const Il2CppType ConstructionCall_t2317_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.ConstructionCall System.Runtime.Remoting.Activation.ActivationServices::CreateConstructionCall(System.Type,System.String,System.Object[])
extern const MethodInfo ActivationServices_CreateConstructionCall_m12091_MethodInfo = 
{
	"CreateConstructionCall"/* name */
	, (methodPointerType)&ActivationServices_CreateConstructionCall_m12091/* method */
	, &ActivationServices_t2293_il2cpp_TypeInfo/* declaring_type */
	, &ConstructionCall_t2317_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2293_ActivationServices_CreateConstructionCall_m12091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ActivationServices_t2293_ActivationServices_AllocateUninitializedClassInstance_m12092_ParameterInfos[] = 
{
	{"type", 0, 134221938, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)
extern const MethodInfo ActivationServices_AllocateUninitializedClassInstance_m12092_MethodInfo = 
{
	"AllocateUninitializedClassInstance"/* name */
	, (methodPointerType)&ActivationServices_AllocateUninitializedClassInstance_m12092/* method */
	, &ActivationServices_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2293_ActivationServices_AllocateUninitializedClassInstance_m12092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivationServices_t2293_MethodInfos[] =
{
	&ActivationServices_get_ConstructionActivator_m12089_MethodInfo,
	&ActivationServices_CreateProxyFromAttributes_m12090_MethodInfo,
	&ActivationServices_CreateConstructionCall_m12091_MethodInfo,
	&ActivationServices_AllocateUninitializedClassInstance_m12092_MethodInfo,
	NULL
};
extern const MethodInfo ActivationServices_get_ConstructionActivator_m12089_MethodInfo;
static const PropertyInfo ActivationServices_t2293____ConstructionActivator_PropertyInfo = 
{
	&ActivationServices_t2293_il2cpp_TypeInfo/* parent */
	, "ConstructionActivator"/* name */
	, &ActivationServices_get_ConstructionActivator_m12089_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivationServices_t2293_PropertyInfos[] =
{
	&ActivationServices_t2293____ConstructionActivator_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference ActivationServices_t2293_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ActivationServices_t2293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationServices_t2293_0_0_0;
extern const Il2CppType ActivationServices_t2293_1_0_0;
struct ActivationServices_t2293;
const Il2CppTypeDefinitionMetadata ActivationServices_t2293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t2293_VTable/* vtableMethods */
	, ActivationServices_t2293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1487/* fieldStart */

};
TypeInfo ActivationServices_t2293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ActivationServices_t2293_MethodInfos/* methods */
	, ActivationServices_t2293_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivationServices_t2293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t2293_0_0_0/* byval_arg */
	, &ActivationServices_t2293_1_0_0/* this_arg */
	, &ActivationServices_t2293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t2293)/* instance_size */
	, sizeof (ActivationServices_t2293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t2293_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t2294_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IActivator_t2292_0_0_0;
static const ParameterInfo AppDomainLevelActivator_t2294_AppDomainLevelActivator__ctor_m12093_ParameterInfos[] = 
{
	{"activationUrl", 0, 134221939, 0, &String_t_0_0_0},
	{"next", 1, 134221940, 0, &IActivator_t2292_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo AppDomainLevelActivator__ctor_m12093_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainLevelActivator__ctor_m12093/* method */
	, &AppDomainLevelActivator_t2294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, AppDomainLevelActivator_t2294_AppDomainLevelActivator__ctor_m12093_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainLevelActivator_t2294_MethodInfos[] =
{
	&AppDomainLevelActivator__ctor_m12093_MethodInfo,
	NULL
};
static const Il2CppMethodReference AppDomainLevelActivator_t2294_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AppDomainLevelActivator_t2294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* AppDomainLevelActivator_t2294_InterfacesTypeInfos[] = 
{
	&IActivator_t2292_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t2294_InterfacesOffsets[] = 
{
	{ &IActivator_t2292_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainLevelActivator_t2294_0_0_0;
extern const Il2CppType AppDomainLevelActivator_t2294_1_0_0;
struct AppDomainLevelActivator_t2294;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t2294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t2294_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t2294_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t2294_VTable/* vtableMethods */
	, AppDomainLevelActivator_t2294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1488/* fieldStart */

};
TypeInfo AppDomainLevelActivator_t2294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, AppDomainLevelActivator_t2294_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainLevelActivator_t2294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t2294_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t2294_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t2294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t2294)/* instance_size */
	, sizeof (AppDomainLevelActivator_t2294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t2295_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern const MethodInfo ConstructionLevelActivator__ctor_m12094_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionLevelActivator__ctor_m12094/* method */
	, &ConstructionLevelActivator_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionLevelActivator_t2295_MethodInfos[] =
{
	&ConstructionLevelActivator__ctor_m12094_MethodInfo,
	NULL
};
static const Il2CppMethodReference ConstructionLevelActivator_t2295_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ConstructionLevelActivator_t2295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionLevelActivator_t2295_InterfacesTypeInfos[] = 
{
	&IActivator_t2292_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t2295_InterfacesOffsets[] = 
{
	{ &IActivator_t2292_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionLevelActivator_t2295_0_0_0;
extern const Il2CppType ConstructionLevelActivator_t2295_1_0_0;
struct ConstructionLevelActivator_t2295;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t2295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t2295_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t2295_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t2295_VTable/* vtableMethods */
	, ConstructionLevelActivator_t2295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructionLevelActivator_t2295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ConstructionLevelActivator_t2295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionLevelActivator_t2295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t2295_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t2295_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t2295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t2295)/* instance_size */
	, sizeof (ConstructionLevelActivator_t2295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t2296_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
extern const Il2CppType IActivator_t2292_0_0_0;
static const ParameterInfo ContextLevelActivator_t2296_ContextLevelActivator__ctor_m12095_ParameterInfos[] = 
{
	{"next", 0, 134221941, 0, &IActivator_t2292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ContextLevelActivator__ctor_m12095_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextLevelActivator__ctor_m12095/* method */
	, &ContextLevelActivator_t2296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ContextLevelActivator_t2296_ContextLevelActivator__ctor_m12095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextLevelActivator_t2296_MethodInfos[] =
{
	&ContextLevelActivator__ctor_m12095_MethodInfo,
	NULL
};
static const Il2CppMethodReference ContextLevelActivator_t2296_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ContextLevelActivator_t2296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextLevelActivator_t2296_InterfacesTypeInfos[] = 
{
	&IActivator_t2292_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t2296_InterfacesOffsets[] = 
{
	{ &IActivator_t2292_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextLevelActivator_t2296_0_0_0;
extern const Il2CppType ContextLevelActivator_t2296_1_0_0;
struct ContextLevelActivator_t2296;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t2296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t2296_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t2296_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t2296_VTable/* vtableMethods */
	, ContextLevelActivator_t2296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1490/* fieldStart */

};
TypeInfo ContextLevelActivator_t2296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ContextLevelActivator_t2296_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ContextLevelActivator_t2296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t2296_0_0_0/* byval_arg */
	, &ContextLevelActivator_t2296_1_0_0/* this_arg */
	, &ContextLevelActivator_t2296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t2296)/* instance_size */
	, sizeof (ContextLevelActivator_t2296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t2292_il2cpp_TypeInfo;
static const MethodInfo* IActivator_t2292_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IActivator_t2292_1_0_0;
struct IActivator_t2292;
const Il2CppTypeDefinitionMetadata IActivator_t2292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IActivator_t2292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IActivator_t2292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IActivator_t2292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 506/* custom_attributes_cache */
	, &IActivator_t2292_0_0_0/* byval_arg */
	, &IActivator_t2292_1_0_0/* this_arg */
	, &IActivator_t2292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t2592_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType()
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m14548_MethodInfo = 
{
	"get_ActivationType"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationTypeName()
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m14549_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator()
extern const MethodInfo IConstructionCallMessage_get_Activator_m14550_MethodInfo = 
{
	"get_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t2292_0_0_0;
static const ParameterInfo IConstructionCallMessage_t2592_IConstructionCallMessage_set_Activator_m14551_ParameterInfos[] = 
{
	{"value", 0, 134221942, 0, &IActivator_t2292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.IConstructionCallMessage::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo IConstructionCallMessage_set_Activator_m14551_MethodInfo = 
{
	"set_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IConstructionCallMessage_t2592_IConstructionCallMessage_set_Activator_m14551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Activation.IConstructionCallMessage::get_CallSiteActivationAttributes()
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m14552_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IList_t1514_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties()
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m14553_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* declaring_type */
	, &IList_t1514_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IConstructionCallMessage_t2592_MethodInfos[] =
{
	&IConstructionCallMessage_get_ActivationType_m14548_MethodInfo,
	&IConstructionCallMessage_get_ActivationTypeName_m14549_MethodInfo,
	&IConstructionCallMessage_get_Activator_m14550_MethodInfo,
	&IConstructionCallMessage_set_Activator_m14551_MethodInfo,
	&IConstructionCallMessage_get_CallSiteActivationAttributes_m14552_MethodInfo,
	&IConstructionCallMessage_get_ContextProperties_m14553_MethodInfo,
	NULL
};
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m14548_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2592____ActivationType_PropertyInfo = 
{
	&IConstructionCallMessage_t2592_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &IConstructionCallMessage_get_ActivationType_m14548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m14549_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2592____ActivationTypeName_PropertyInfo = 
{
	&IConstructionCallMessage_t2592_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &IConstructionCallMessage_get_ActivationTypeName_m14549_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_Activator_m14550_MethodInfo;
extern const MethodInfo IConstructionCallMessage_set_Activator_m14551_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2592____Activator_PropertyInfo = 
{
	&IConstructionCallMessage_t2592_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &IConstructionCallMessage_get_Activator_m14550_MethodInfo/* get */
	, &IConstructionCallMessage_set_Activator_m14551_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m14552_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2592____CallSiteActivationAttributes_PropertyInfo = 
{
	&IConstructionCallMessage_t2592_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &IConstructionCallMessage_get_CallSiteActivationAttributes_m14552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m14553_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t2592____ContextProperties_PropertyInfo = 
{
	&IConstructionCallMessage_t2592_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &IConstructionCallMessage_get_ContextProperties_m14553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IConstructionCallMessage_t2592_PropertyInfos[] =
{
	&IConstructionCallMessage_t2592____ActivationType_PropertyInfo,
	&IConstructionCallMessage_t2592____ActivationTypeName_PropertyInfo,
	&IConstructionCallMessage_t2592____Activator_PropertyInfo,
	&IConstructionCallMessage_t2592____CallSiteActivationAttributes_PropertyInfo,
	&IConstructionCallMessage_t2592____ContextProperties_PropertyInfo,
	NULL
};
extern const Il2CppType IMessage_t2315_0_0_0;
extern const Il2CppType IMethodCallMessage_t2596_0_0_0;
extern const Il2CppType IMethodMessage_t2327_0_0_0;
static const Il2CppType* IConstructionCallMessage_t2592_InterfacesTypeInfos[] = 
{
	&IMessage_t2315_0_0_0,
	&IMethodCallMessage_t2596_0_0_0,
	&IMethodMessage_t2327_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_1_0_0;
struct IConstructionCallMessage_t2592;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t2592_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t2592_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IConstructionCallMessage_t2592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IConstructionCallMessage_t2592_MethodInfos/* methods */
	, IConstructionCallMessage_t2592_PropertyInfos/* properties */
	, NULL/* events */
	, &IConstructionCallMessage_t2592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 507/* custom_attributes_cache */
	, &IConstructionCallMessage_t2592_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t2592_1_0_0/* this_arg */
	, &IConstructionCallMessage_t2592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t2297_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
static const MethodInfo* RemoteActivator_t2297_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RemoteActivator_t2297_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RemoteActivator_t2297_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemoteActivator_t2297_InterfacesTypeInfos[] = 
{
	&IActivator_t2292_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t2297_InterfacesOffsets[] = 
{
	{ &IActivator_t2292_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemoteActivator_t2297_0_0_0;
extern const Il2CppType RemoteActivator_t2297_1_0_0;
extern const Il2CppType MarshalByRefObject_t1885_0_0_0;
struct RemoteActivator_t2297;
const Il2CppTypeDefinitionMetadata RemoteActivator_t2297_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t2297_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t2297_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1885_0_0_0/* parent */
	, RemoteActivator_t2297_VTable/* vtableMethods */
	, RemoteActivator_t2297_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteActivator_t2297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, RemoteActivator_t2297_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteActivator_t2297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t2297_0_0_0/* byval_arg */
	, &RemoteActivator_t2297_1_0_0/* this_arg */
	, &RemoteActivator_t2297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t2297)/* instance_size */
	, sizeof (RemoteActivator_t2297)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t2298_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.UrlAttribute::get_UrlValue()
extern const MethodInfo UrlAttribute_get_UrlValue_m12096_MethodInfo = 
{
	"get_UrlValue"/* name */
	, (methodPointerType)&UrlAttribute_get_UrlValue_m12096/* method */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UrlAttribute_t2298_UrlAttribute_Equals_m12097_ParameterInfos[] = 
{
	{"o", 0, 134221943, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t169_0_0_0;
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::Equals(System.Object)
extern const MethodInfo UrlAttribute_Equals_m12097_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&UrlAttribute_Equals_m12097/* method */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, UrlAttribute_t2298_UrlAttribute_Equals_m12097_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Activation.UrlAttribute::GetHashCode()
extern const MethodInfo UrlAttribute_GetHashCode_m12098_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&UrlAttribute_GetHashCode_m12098/* method */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo UrlAttribute_t2298_UrlAttribute_GetPropertiesForNewContext_m12099_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221944, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m12099_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&UrlAttribute_GetPropertiesForNewContext_m12099/* method */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UrlAttribute_t2298_UrlAttribute_GetPropertiesForNewContext_m12099_ParameterInfos/* parameters */
	, 509/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo UrlAttribute_t2298_UrlAttribute_IsContextOK_m12100_ParameterInfos[] = 
{
	{"ctx", 0, 134221945, 0, &Context_t2306_0_0_0},
	{"msg", 1, 134221946, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_IsContextOK_m12100_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&UrlAttribute_IsContextOK_m12100/* method */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, UrlAttribute_t2298_UrlAttribute_IsContextOK_m12100_ParameterInfos/* parameters */
	, 510/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UrlAttribute_t2298_MethodInfos[] =
{
	&UrlAttribute_get_UrlValue_m12096_MethodInfo,
	&UrlAttribute_Equals_m12097_MethodInfo,
	&UrlAttribute_GetHashCode_m12098_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12099_MethodInfo,
	&UrlAttribute_IsContextOK_m12100_MethodInfo,
	NULL
};
extern const MethodInfo UrlAttribute_get_UrlValue_m12096_MethodInfo;
static const PropertyInfo UrlAttribute_t2298____UrlValue_PropertyInfo = 
{
	&UrlAttribute_t2298_il2cpp_TypeInfo/* parent */
	, "UrlValue"/* name */
	, &UrlAttribute_get_UrlValue_m12096_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UrlAttribute_t2298_PropertyInfos[] =
{
	&UrlAttribute_t2298____UrlValue_PropertyInfo,
	NULL
};
extern const MethodInfo UrlAttribute_Equals_m12097_MethodInfo;
extern const MethodInfo UrlAttribute_GetHashCode_m12098_MethodInfo;
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m12099_MethodInfo;
extern const MethodInfo UrlAttribute_IsContextOK_m12100_MethodInfo;
extern const MethodInfo ContextAttribute_get_Name_m12124_MethodInfo;
static const Il2CppMethodReference UrlAttribute_t2298_VTable[] =
{
	&UrlAttribute_Equals_m12097_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&UrlAttribute_GetHashCode_m12098_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12099_MethodInfo,
	&UrlAttribute_IsContextOK_m12100_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12099_MethodInfo,
	&UrlAttribute_IsContextOK_m12100_MethodInfo,
};
static bool UrlAttribute_t2298_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IContextAttribute_t2605_0_0_0;
extern const Il2CppType IContextProperty_t2594_0_0_0;
extern const Il2CppType _Attribute_t901_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t2298_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2605_0_0_0, 4},
	{ &IContextProperty_t2594_0_0_0, 6},
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UrlAttribute_t2298_0_0_0;
extern const Il2CppType UrlAttribute_t2298_1_0_0;
extern const Il2CppType ContextAttribute_t2299_0_0_0;
struct UrlAttribute_t2298;
const Il2CppTypeDefinitionMetadata UrlAttribute_t2298_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t2298_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t2299_0_0_0/* parent */
	, UrlAttribute_t2298_VTable/* vtableMethods */
	, UrlAttribute_t2298_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1491/* fieldStart */

};
TypeInfo UrlAttribute_t2298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, UrlAttribute_t2298_MethodInfos/* methods */
	, UrlAttribute_t2298_PropertyInfos/* properties */
	, NULL/* events */
	, &UrlAttribute_t2298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 508/* custom_attributes_cache */
	, &UrlAttribute_t2298_0_0_0/* byval_arg */
	, &UrlAttribute_t2298_1_0_0/* this_arg */
	, &UrlAttribute_t2298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t2298)/* instance_size */
	, sizeof (UrlAttribute_t2298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t2300_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern const MethodInfo ChannelInfo__ctor_m12101_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ChannelInfo__ctor_m12101/* method */
	, &ChannelInfo_t2300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern const MethodInfo ChannelInfo_get_ChannelData_m12102_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&ChannelInfo_get_ChannelData_m12102/* method */
	, &ChannelInfo_t2300_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelInfo_t2300_MethodInfos[] =
{
	&ChannelInfo__ctor_m12101_MethodInfo,
	&ChannelInfo_get_ChannelData_m12102_MethodInfo,
	NULL
};
extern const MethodInfo ChannelInfo_get_ChannelData_m12102_MethodInfo;
static const PropertyInfo ChannelInfo_t2300____ChannelData_PropertyInfo = 
{
	&ChannelInfo_t2300_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &ChannelInfo_get_ChannelData_m12102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ChannelInfo_t2300_PropertyInfos[] =
{
	&ChannelInfo_t2300____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ChannelInfo_t2300_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ChannelInfo_get_ChannelData_m12102_MethodInfo,
};
static bool ChannelInfo_t2300_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelInfo_t2346_0_0_0;
static const Il2CppType* ChannelInfo_t2300_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t2346_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t2300_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t2346_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelInfo_t2300_0_0_0;
extern const Il2CppType ChannelInfo_t2300_1_0_0;
struct ChannelInfo_t2300;
const Il2CppTypeDefinitionMetadata ChannelInfo_t2300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t2300_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t2300_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t2300_VTable/* vtableMethods */
	, ChannelInfo_t2300_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1492/* fieldStart */

};
TypeInfo ChannelInfo_t2300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ChannelInfo_t2300_MethodInfos/* methods */
	, ChannelInfo_t2300_PropertyInfos/* properties */
	, NULL/* events */
	, &ChannelInfo_t2300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t2300_0_0_0/* byval_arg */
	, &ChannelInfo_t2300_1_0_0/* this_arg */
	, &ChannelInfo_t2300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t2300)/* instance_size */
	, sizeof (ChannelInfo_t2300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t2302_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
extern const MethodInfo ChannelServices__cctor_m12103_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ChannelServices__cctor_m12103/* method */
	, &ChannelServices_t2302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t2593_0_0_0;
extern const Il2CppType IChannel_t2593_0_0_0;
static const ParameterInfo ChannelServices_t2302_ChannelServices_RegisterChannel_m12104_ParameterInfos[] = 
{
	{"chnl", 0, 134221947, 0, &IChannel_t2593_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern const MethodInfo ChannelServices_RegisterChannel_m12104_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m12104/* method */
	, &ChannelServices_t2302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ChannelServices_t2302_ChannelServices_RegisterChannel_m12104_ParameterInfos/* parameters */
	, 512/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t2593_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ChannelServices_t2302_ChannelServices_RegisterChannel_m12105_ParameterInfos[] = 
{
	{"chnl", 0, 134221948, 0, &IChannel_t2593_0_0_0},
	{"ensureSecurity", 1, 134221949, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
extern const MethodInfo ChannelServices_RegisterChannel_m12105_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m12105/* method */
	, &ChannelServices_t2302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, ChannelServices_t2302_ChannelServices_RegisterChannel_m12105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
extern const MethodInfo ChannelServices_GetCurrentChannelInfo_m12106_MethodInfo = 
{
	"GetCurrentChannelInfo"/* name */
	, (methodPointerType)&ChannelServices_GetCurrentChannelInfo_m12106/* method */
	, &ChannelServices_t2302_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelServices_t2302_MethodInfos[] =
{
	&ChannelServices__cctor_m12103_MethodInfo,
	&ChannelServices_RegisterChannel_m12104_MethodInfo,
	&ChannelServices_RegisterChannel_m12105_MethodInfo,
	&ChannelServices_GetCurrentChannelInfo_m12106_MethodInfo,
	NULL
};
static const Il2CppMethodReference ChannelServices_t2302_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ChannelServices_t2302_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelServices_t2302_0_0_0;
extern const Il2CppType ChannelServices_t2302_1_0_0;
struct ChannelServices_t2302;
const Il2CppTypeDefinitionMetadata ChannelServices_t2302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t2302_VTable/* vtableMethods */
	, ChannelServices_t2302_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1493/* fieldStart */

};
TypeInfo ChannelServices_t2302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ChannelServices_t2302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ChannelServices_t2302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 511/* custom_attributes_cache */
	, &ChannelServices_t2302_0_0_0/* byval_arg */
	, &ChannelServices_t2302_1_0_0/* this_arg */
	, &ChannelServices_t2302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t2302)/* instance_size */
	, sizeof (ChannelServices_t2302)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t2302_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t2303_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo CrossAppDomainData_t2303_CrossAppDomainData__ctor_m12107_ParameterInfos[] = 
{
	{"domainId", 0, 134221950, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
extern const MethodInfo CrossAppDomainData__ctor_m12107_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainData__ctor_m12107/* method */
	, &CrossAppDomainData_t2303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, CrossAppDomainData_t2303_CrossAppDomainData__ctor_m12107_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainData_t2303_MethodInfos[] =
{
	&CrossAppDomainData__ctor_m12107_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainData_t2303_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CrossAppDomainData_t2303_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainData_t2303_0_0_0;
extern const Il2CppType CrossAppDomainData_t2303_1_0_0;
struct CrossAppDomainData_t2303;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t2303_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t2303_VTable/* vtableMethods */
	, CrossAppDomainData_t2303_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1498/* fieldStart */

};
TypeInfo CrossAppDomainData_t2303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainData_t2303_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossAppDomainData_t2303_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t2303_0_0_0/* byval_arg */
	, &CrossAppDomainData_t2303_1_0_0/* this_arg */
	, &CrossAppDomainData_t2303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t2303)/* instance_size */
	, sizeof (CrossAppDomainData_t2303)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t2304_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
extern const MethodInfo CrossAppDomainChannel__ctor_m12108_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__ctor_m12108/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
extern const MethodInfo CrossAppDomainChannel__cctor_m12109_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__cctor_m12109/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
extern const MethodInfo CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12110_MethodInfo = 
{
	"RegisterCrossAppDomainChannel"/* name */
	, (methodPointerType)&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12110/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo = 
{
	"get_ChannelName"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelName_m12111/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelPriority_m12112/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelData_m12113/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CrossAppDomainChannel_t2304_CrossAppDomainChannel_StartListening_m12114_ParameterInfos[] = 
{
	{"data", 0, 134221951, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
extern const MethodInfo CrossAppDomainChannel_StartListening_m12114_MethodInfo = 
{
	"StartListening"/* name */
	, (methodPointerType)&CrossAppDomainChannel_StartListening_m12114/* method */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CrossAppDomainChannel_t2304_CrossAppDomainChannel_StartListening_m12114_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainChannel_t2304_MethodInfos[] =
{
	&CrossAppDomainChannel__ctor_m12108_MethodInfo,
	&CrossAppDomainChannel__cctor_m12109_MethodInfo,
	&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12110_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12114_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t2304____ChannelName_PropertyInfo = 
{
	&CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t2304____ChannelPriority_PropertyInfo = 
{
	&CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t2304____ChannelData_PropertyInfo = 
{
	&CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainChannel_t2304_PropertyInfos[] =
{
	&CrossAppDomainChannel_t2304____ChannelName_PropertyInfo,
	&CrossAppDomainChannel_t2304____ChannelPriority_PropertyInfo,
	&CrossAppDomainChannel_t2304____ChannelData_PropertyInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_StartListening_m12114_MethodInfo;
static const Il2CppMethodReference CrossAppDomainChannel_t2304_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12114_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12111_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12112_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12113_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12114_MethodInfo,
};
static bool CrossAppDomainChannel_t2304_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelReceiver_t2607_0_0_0;
extern const Il2CppType IChannelSender_t2683_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t2304_InterfacesTypeInfos[] = 
{
	&IChannel_t2593_0_0_0,
	&IChannelReceiver_t2607_0_0_0,
	&IChannelSender_t2683_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t2304_InterfacesOffsets[] = 
{
	{ &IChannel_t2593_0_0_0, 4},
	{ &IChannelReceiver_t2607_0_0_0, 6},
	{ &IChannelSender_t2683_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainChannel_t2304_0_0_0;
extern const Il2CppType CrossAppDomainChannel_t2304_1_0_0;
struct CrossAppDomainChannel_t2304;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t2304_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t2304_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t2304_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t2304_VTable/* vtableMethods */
	, CrossAppDomainChannel_t2304_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1501/* fieldStart */

};
TypeInfo CrossAppDomainChannel_t2304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainChannel_t2304_MethodInfos/* methods */
	, CrossAppDomainChannel_t2304_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainChannel_t2304_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t2304_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t2304_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t2304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t2304)/* instance_size */
	, sizeof (CrossAppDomainChannel_t2304)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t2304_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t2305_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.cctor()
extern const MethodInfo CrossAppDomainSink__cctor_m12115_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainSink__cctor_m12115/* method */
	, &CrossAppDomainSink_t2305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainSink::get_TargetDomainId()
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m12116_MethodInfo = 
{
	"get_TargetDomainId"/* name */
	, (methodPointerType)&CrossAppDomainSink_get_TargetDomainId_m12116/* method */
	, &CrossAppDomainSink_t2305_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainSink_t2305_MethodInfos[] =
{
	&CrossAppDomainSink__cctor_m12115_MethodInfo,
	&CrossAppDomainSink_get_TargetDomainId_m12116_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m12116_MethodInfo;
static const PropertyInfo CrossAppDomainSink_t2305____TargetDomainId_PropertyInfo = 
{
	&CrossAppDomainSink_t2305_il2cpp_TypeInfo/* parent */
	, "TargetDomainId"/* name */
	, &CrossAppDomainSink_get_TargetDomainId_m12116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainSink_t2305_PropertyInfos[] =
{
	&CrossAppDomainSink_t2305____TargetDomainId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainSink_t2305_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CrossAppDomainSink_t2305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType IMessageSink_t1130_0_0_0;
static const Il2CppType* CrossAppDomainSink_t2305_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1130_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t2305_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1130_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainSink_t2305_0_0_0;
extern const Il2CppType CrossAppDomainSink_t2305_1_0_0;
struct CrossAppDomainSink_t2305;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t2305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t2305_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t2305_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t2305_VTable/* vtableMethods */
	, CrossAppDomainSink_t2305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1502/* fieldStart */

};
TypeInfo CrossAppDomainSink_t2305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainSink_t2305_MethodInfos/* methods */
	, CrossAppDomainSink_t2305_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainSink_t2305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 513/* custom_attributes_cache */
	, &CrossAppDomainSink_t2305_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t2305_1_0_0/* this_arg */
	, &CrossAppDomainSink_t2305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t2305)/* instance_size */
	, sizeof (CrossAppDomainSink_t2305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t2305_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t2593_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName()
extern const MethodInfo IChannel_get_ChannelName_m14554_MethodInfo = 
{
	"get_ChannelName"/* name */
	, NULL/* method */
	, &IChannel_t2593_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority()
extern const MethodInfo IChannel_get_ChannelPriority_m14555_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, NULL/* method */
	, &IChannel_t2593_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannel_t2593_MethodInfos[] =
{
	&IChannel_get_ChannelName_m14554_MethodInfo,
	&IChannel_get_ChannelPriority_m14555_MethodInfo,
	NULL
};
extern const MethodInfo IChannel_get_ChannelName_m14554_MethodInfo;
static const PropertyInfo IChannel_t2593____ChannelName_PropertyInfo = 
{
	&IChannel_t2593_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &IChannel_get_ChannelName_m14554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IChannel_get_ChannelPriority_m14555_MethodInfo;
static const PropertyInfo IChannel_t2593____ChannelPriority_PropertyInfo = 
{
	&IChannel_t2593_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &IChannel_get_ChannelPriority_m14555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannel_t2593_PropertyInfos[] =
{
	&IChannel_t2593____ChannelName_PropertyInfo,
	&IChannel_t2593____ChannelPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannel_t2593_1_0_0;
struct IChannel_t2593;
const Il2CppTypeDefinitionMetadata IChannel_t2593_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannel_t2593_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannel_t2593_MethodInfos/* methods */
	, IChannel_t2593_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannel_t2593_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 514/* custom_attributes_cache */
	, &IChannel_t2593_0_0_0/* byval_arg */
	, &IChannel_t2593_1_0_0/* this_arg */
	, &IChannel_t2593_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t2607_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.IChannelReceiver::get_ChannelData()
extern const MethodInfo IChannelReceiver_get_ChannelData_m14556_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IChannelReceiver_t2607_IChannelReceiver_StartListening_m14557_ParameterInfos[] = 
{
	{"data", 0, 134221952, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StartListening(System.Object)
extern const MethodInfo IChannelReceiver_StartListening_m14557_MethodInfo = 
{
	"StartListening"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IChannelReceiver_t2607_IChannelReceiver_StartListening_m14557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelReceiver_t2607_MethodInfos[] =
{
	&IChannelReceiver_get_ChannelData_m14556_MethodInfo,
	&IChannelReceiver_StartListening_m14557_MethodInfo,
	NULL
};
extern const MethodInfo IChannelReceiver_get_ChannelData_m14556_MethodInfo;
static const PropertyInfo IChannelReceiver_t2607____ChannelData_PropertyInfo = 
{
	&IChannelReceiver_t2607_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelReceiver_get_ChannelData_m14556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelReceiver_t2607_PropertyInfos[] =
{
	&IChannelReceiver_t2607____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppType* IChannelReceiver_t2607_InterfacesTypeInfos[] = 
{
	&IChannel_t2593_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelReceiver_t2607_1_0_0;
struct IChannelReceiver_t2607;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t2607_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t2607_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelReceiver_t2607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelReceiver_t2607_MethodInfos/* methods */
	, IChannelReceiver_t2607_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelReceiver_t2607_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 515/* custom_attributes_cache */
	, &IChannelReceiver_t2607_0_0_0/* byval_arg */
	, &IChannelReceiver_t2607_1_0_0/* this_arg */
	, &IChannelReceiver_t2607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t2683_il2cpp_TypeInfo;
static const MethodInfo* IChannelSender_t2683_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IChannelSender_t2683_InterfacesTypeInfos[] = 
{
	&IChannel_t2593_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelSender_t2683_1_0_0;
struct IChannelSender_t2683;
const Il2CppTypeDefinitionMetadata IChannelSender_t2683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t2683_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelSender_t2683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelSender_t2683_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IChannelSender_t2683_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 516/* custom_attributes_cache */
	, &IChannelSender_t2683_0_0_0/* byval_arg */
	, &IChannelSender_t2683_1_0_0/* this_arg */
	, &IChannelSender_t2683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t2606_il2cpp_TypeInfo;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ISecurableChannel_t2606_ISecurableChannel_set_IsSecured_m14558_ParameterInfos[] = 
{
	{"value", 0, 134221953, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ISecurableChannel::set_IsSecured(System.Boolean)
extern const MethodInfo ISecurableChannel_set_IsSecured_m14558_MethodInfo = 
{
	"set_IsSecured"/* name */
	, NULL/* method */
	, &ISecurableChannel_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ISecurableChannel_t2606_ISecurableChannel_set_IsSecured_m14558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISecurableChannel_t2606_MethodInfos[] =
{
	&ISecurableChannel_set_IsSecured_m14558_MethodInfo,
	NULL
};
extern const MethodInfo ISecurableChannel_set_IsSecured_m14558_MethodInfo;
static const PropertyInfo ISecurableChannel_t2606____IsSecured_PropertyInfo = 
{
	&ISecurableChannel_t2606_il2cpp_TypeInfo/* parent */
	, "IsSecured"/* name */
	, NULL/* get */
	, &ISecurableChannel_set_IsSecured_m14558_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ISecurableChannel_t2606_PropertyInfos[] =
{
	&ISecurableChannel_t2606____IsSecured_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurableChannel_t2606_0_0_0;
extern const Il2CppType ISecurableChannel_t2606_1_0_0;
struct ISecurableChannel_t2606;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t2606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISecurableChannel_t2606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ISecurableChannel_t2606_MethodInfos/* methods */
	, ISecurableChannel_t2606_PropertyInfos/* properties */
	, NULL/* events */
	, &ISecurableChannel_t2606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t2606_0_0_0/* byval_arg */
	, &ISecurableChannel_t2606_1_0_0/* this_arg */
	, &ISecurableChannel_t2606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t2306_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::.cctor()
extern const MethodInfo Context__cctor_m12117_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Context__cctor_m12117/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::Finalize()
extern const MethodInfo Context_Finalize_m12118_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&Context_Finalize_m12118/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::get_DefaultContext()
extern const MethodInfo Context_get_DefaultContext_m12119_MethodInfo = 
{
	"get_DefaultContext"/* name */
	, (methodPointerType)&Context_get_DefaultContext_m12119/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &Context_t2306_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_IsDefaultContext()
extern const MethodInfo Context_get_IsDefaultContext_m12120_MethodInfo = 
{
	"get_IsDefaultContext"/* name */
	, (methodPointerType)&Context_get_IsDefaultContext_m12120/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Context_t2306_Context_GetProperty_m12121_ParameterInfos[] = 
{
	{"name", 0, 134221954, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String)
extern const MethodInfo Context_GetProperty_m12121_MethodInfo = 
{
	"GetProperty"/* name */
	, (methodPointerType)&Context_GetProperty_m12121/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2594_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Context_t2306_Context_GetProperty_m12121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.Context::ToString()
extern const MethodInfo Context_ToString_m12122_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Context_ToString_m12122/* method */
	, &Context_t2306_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Context_t2306_MethodInfos[] =
{
	&Context__cctor_m12117_MethodInfo,
	&Context_Finalize_m12118_MethodInfo,
	&Context_get_DefaultContext_m12119_MethodInfo,
	&Context_get_IsDefaultContext_m12120_MethodInfo,
	&Context_GetProperty_m12121_MethodInfo,
	&Context_ToString_m12122_MethodInfo,
	NULL
};
extern const MethodInfo Context_get_DefaultContext_m12119_MethodInfo;
static const PropertyInfo Context_t2306____DefaultContext_PropertyInfo = 
{
	&Context_t2306_il2cpp_TypeInfo/* parent */
	, "DefaultContext"/* name */
	, &Context_get_DefaultContext_m12119_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Context_get_IsDefaultContext_m12120_MethodInfo;
static const PropertyInfo Context_t2306____IsDefaultContext_PropertyInfo = 
{
	&Context_t2306_il2cpp_TypeInfo/* parent */
	, "IsDefaultContext"/* name */
	, &Context_get_IsDefaultContext_m12120_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Context_t2306_PropertyInfos[] =
{
	&Context_t2306____DefaultContext_PropertyInfo,
	&Context_t2306____IsDefaultContext_PropertyInfo,
	NULL
};
extern const MethodInfo Context_Finalize_m12118_MethodInfo;
extern const MethodInfo Context_ToString_m12122_MethodInfo;
extern const MethodInfo Context_GetProperty_m12121_MethodInfo;
static const Il2CppMethodReference Context_t2306_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Context_Finalize_m12118_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Context_ToString_m12122_MethodInfo,
	&Context_GetProperty_m12121_MethodInfo,
};
static bool Context_t2306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t2306_1_0_0;
struct Context_t2306;
const Il2CppTypeDefinitionMetadata Context_t2306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t2306_VTable/* vtableMethods */
	, Context_t2306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1505/* fieldStart */

};
TypeInfo Context_t2306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, Context_t2306_MethodInfos/* methods */
	, Context_t2306_PropertyInfos/* properties */
	, NULL/* events */
	, &Context_t2306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 517/* custom_attributes_cache */
	, &Context_t2306_0_0_0/* byval_arg */
	, &Context_t2306_1_0_0/* this_arg */
	, &Context_t2306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t2306)/* instance_size */
	, sizeof (Context_t2306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t2306_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t2299_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ContextAttribute_t2299_ContextAttribute__ctor_m12123_ParameterInfos[] = 
{
	{"name", 0, 134221955, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::.ctor(System.String)
extern const MethodInfo ContextAttribute__ctor_m12123_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextAttribute__ctor_m12123/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ContextAttribute_t2299_ContextAttribute__ctor_m12123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.ContextAttribute::get_Name()
extern const MethodInfo ContextAttribute_get_Name_m12124_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ContextAttribute_get_Name_m12124/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ContextAttribute_t2299_ContextAttribute_Equals_m12125_ParameterInfos[] = 
{
	{"o", 0, 134221956, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::Equals(System.Object)
extern const MethodInfo ContextAttribute_Equals_m12125_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&ContextAttribute_Equals_m12125/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, ContextAttribute_t2299_ContextAttribute_Equals_m12125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Contexts.ContextAttribute::GetHashCode()
extern const MethodInfo ContextAttribute_GetHashCode_m12126_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&ContextAttribute_GetHashCode_m12126/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo ContextAttribute_t2299_ContextAttribute_GetPropertiesForNewContext_m12127_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221957, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m12127_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ContextAttribute_GetPropertiesForNewContext_m12127/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ContextAttribute_t2299_ContextAttribute_GetPropertiesForNewContext_m12127_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo ContextAttribute_t2299_ContextAttribute_IsContextOK_m12128_ParameterInfos[] = 
{
	{"ctx", 0, 134221958, 0, &Context_t2306_0_0_0},
	{"ctorMsg", 1, 134221959, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_IsContextOK_m12128_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ContextAttribute_IsContextOK_m12128/* method */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, ContextAttribute_t2299_ContextAttribute_IsContextOK_m12128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextAttribute_t2299_MethodInfos[] =
{
	&ContextAttribute__ctor_m12123_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&ContextAttribute_Equals_m12125_MethodInfo,
	&ContextAttribute_GetHashCode_m12126_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12127_MethodInfo,
	&ContextAttribute_IsContextOK_m12128_MethodInfo,
	NULL
};
static const PropertyInfo ContextAttribute_t2299____Name_PropertyInfo = 
{
	&ContextAttribute_t2299_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ContextAttribute_get_Name_m12124_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContextAttribute_t2299_PropertyInfos[] =
{
	&ContextAttribute_t2299____Name_PropertyInfo,
	NULL
};
extern const MethodInfo ContextAttribute_Equals_m12125_MethodInfo;
extern const MethodInfo ContextAttribute_GetHashCode_m12126_MethodInfo;
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m12127_MethodInfo;
extern const MethodInfo ContextAttribute_IsContextOK_m12128_MethodInfo;
static const Il2CppMethodReference ContextAttribute_t2299_VTable[] =
{
	&ContextAttribute_Equals_m12125_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ContextAttribute_GetHashCode_m12126_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12127_MethodInfo,
	&ContextAttribute_IsContextOK_m12128_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12127_MethodInfo,
	&ContextAttribute_IsContextOK_m12128_MethodInfo,
};
static bool ContextAttribute_t2299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextAttribute_t2299_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2605_0_0_0,
	&IContextProperty_t2594_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t2299_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
	{ &IContextAttribute_t2605_0_0_0, 4},
	{ &IContextProperty_t2594_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextAttribute_t2299_1_0_0;
extern const Il2CppType Attribute_t138_0_0_0;
struct ContextAttribute_t2299;
const Il2CppTypeDefinitionMetadata ContextAttribute_t2299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t2299_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t2299_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ContextAttribute_t2299_VTable/* vtableMethods */
	, ContextAttribute_t2299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1508/* fieldStart */

};
TypeInfo ContextAttribute_t2299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, ContextAttribute_t2299_MethodInfos/* methods */
	, ContextAttribute_t2299_PropertyInfos/* properties */
	, NULL/* events */
	, &ContextAttribute_t2299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 518/* custom_attributes_cache */
	, &ContextAttribute_t2299_0_0_0/* byval_arg */
	, &ContextAttribute_t2299_1_0_0/* this_arg */
	, &ContextAttribute_t2299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t2299)/* instance_size */
	, sizeof (ContextAttribute_t2299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t2301_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern const MethodInfo CrossContextChannel__ctor_m12129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossContextChannel__ctor_m12129/* method */
	, &CrossContextChannel_t2301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossContextChannel_t2301_MethodInfos[] =
{
	&CrossContextChannel__ctor_m12129_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossContextChannel_t2301_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CrossContextChannel_t2301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CrossContextChannel_t2301_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1130_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t2301_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1130_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossContextChannel_t2301_0_0_0;
extern const Il2CppType CrossContextChannel_t2301_1_0_0;
struct CrossContextChannel_t2301;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t2301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t2301_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t2301_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t2301_VTable/* vtableMethods */
	, CrossContextChannel_t2301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CrossContextChannel_t2301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, CrossContextChannel_t2301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossContextChannel_t2301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t2301_0_0_0/* byval_arg */
	, &CrossContextChannel_t2301_1_0_0/* this_arg */
	, &CrossContextChannel_t2301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t2301)/* instance_size */
	, sizeof (CrossContextChannel_t2301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t2605_il2cpp_TypeInfo;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo IContextAttribute_t2605_IContextAttribute_GetPropertiesForNewContext_m14559_ParameterInfos[] = 
{
	{"msg", 0, 134221960, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.IContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_GetPropertiesForNewContext_m14559_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, NULL/* method */
	, &IContextAttribute_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IContextAttribute_t2605_IContextAttribute_GetPropertiesForNewContext_m14559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo IContextAttribute_t2605_IContextAttribute_IsContextOK_m14560_ParameterInfos[] = 
{
	{"ctx", 0, 134221961, 0, &Context_t2306_0_0_0},
	{"msg", 1, 134221962, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_IsContextOK_m14560_MethodInfo = 
{
	"IsContextOK"/* name */
	, NULL/* method */
	, &IContextAttribute_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, IContextAttribute_t2605_IContextAttribute_IsContextOK_m14560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextAttribute_t2605_MethodInfos[] =
{
	&IContextAttribute_GetPropertiesForNewContext_m14559_MethodInfo,
	&IContextAttribute_IsContextOK_m14560_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextAttribute_t2605_1_0_0;
struct IContextAttribute_t2605;
const Il2CppTypeDefinitionMetadata IContextAttribute_t2605_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextAttribute_t2605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextAttribute_t2605_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContextAttribute_t2605_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 519/* custom_attributes_cache */
	, &IContextAttribute_t2605_0_0_0/* byval_arg */
	, &IContextAttribute_t2605_1_0_0/* this_arg */
	, &IContextAttribute_t2605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t2594_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name()
extern const MethodInfo IContextProperty_get_Name_m14561_MethodInfo = 
{
	"get_Name"/* name */
	, NULL/* method */
	, &IContextProperty_t2594_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextProperty_t2594_MethodInfos[] =
{
	&IContextProperty_get_Name_m14561_MethodInfo,
	NULL
};
extern const MethodInfo IContextProperty_get_Name_m14561_MethodInfo;
static const PropertyInfo IContextProperty_t2594____Name_PropertyInfo = 
{
	&IContextProperty_t2594_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &IContextProperty_get_Name_m14561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IContextProperty_t2594_PropertyInfos[] =
{
	&IContextProperty_t2594____Name_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextProperty_t2594_1_0_0;
struct IContextProperty_t2594;
const Il2CppTypeDefinitionMetadata IContextProperty_t2594_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextProperty_t2594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextProperty_t2594_MethodInfos/* methods */
	, IContextProperty_t2594_PropertyInfos/* properties */
	, NULL/* events */
	, &IContextProperty_t2594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 520/* custom_attributes_cache */
	, &IContextProperty_t2594_0_0_0/* byval_arg */
	, &IContextProperty_t2594_1_0_0/* this_arg */
	, &IContextProperty_t2594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t2684_il2cpp_TypeInfo;
static const MethodInfo* IContributeClientContextSink_t2684_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeClientContextSink_t2684_0_0_0;
extern const Il2CppType IContributeClientContextSink_t2684_1_0_0;
struct IContributeClientContextSink_t2684;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t2684_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeClientContextSink_t2684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeClientContextSink_t2684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeClientContextSink_t2684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 521/* custom_attributes_cache */
	, &IContributeClientContextSink_t2684_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t2684_1_0_0/* this_arg */
	, &IContributeClientContextSink_t2684_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t2685_il2cpp_TypeInfo;
static const MethodInfo* IContributeServerContextSink_t2685_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeServerContextSink_t2685_0_0_0;
extern const Il2CppType IContributeServerContextSink_t2685_1_0_0;
struct IContributeServerContextSink_t2685;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t2685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeServerContextSink_t2685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeServerContextSink_t2685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeServerContextSink_t2685_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 522/* custom_attributes_cache */
	, &IContributeServerContextSink_t2685_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t2685_1_0_0/* this_arg */
	, &IContributeServerContextSink_t2685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t2309_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor()
extern const MethodInfo SynchronizationAttribute__ctor_m12130_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m12130/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo SynchronizationAttribute_t2309_SynchronizationAttribute__ctor_m12131_ParameterInfos[] = 
{
	{"flag", 0, 134221963, 0, &Int32_t127_0_0_0},
	{"reEntrant", 1, 134221964, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor(System.Int32,System.Boolean)
extern const MethodInfo SynchronizationAttribute__ctor_m12131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m12131/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, SynchronizationAttribute_t2309_SynchronizationAttribute__ctor_m12131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo SynchronizationAttribute_t2309_SynchronizationAttribute_set_Locked_m12132_ParameterInfos[] = 
{
	{"value", 0, 134221965, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::set_Locked(System.Boolean)
extern const MethodInfo SynchronizationAttribute_set_Locked_m12132_MethodInfo = 
{
	"set_Locked"/* name */
	, (methodPointerType)&SynchronizationAttribute_set_Locked_m12132/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, SynchronizationAttribute_t2309_SynchronizationAttribute_set_Locked_m12132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ReleaseLock()
extern const MethodInfo SynchronizationAttribute_ReleaseLock_m12133_MethodInfo = 
{
	"ReleaseLock"/* name */
	, (methodPointerType)&SynchronizationAttribute_ReleaseLock_m12133/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo SynchronizationAttribute_t2309_SynchronizationAttribute_GetPropertiesForNewContext_m12134_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221966, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m12134_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_GetPropertiesForNewContext_m12134/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SynchronizationAttribute_t2309_SynchronizationAttribute_GetPropertiesForNewContext_m12134_ParameterInfos/* parameters */
	, 524/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo SynchronizationAttribute_t2309_SynchronizationAttribute_IsContextOK_m12135_ParameterInfos[] = 
{
	{"ctx", 0, 134221967, 0, &Context_t2306_0_0_0},
	{"msg", 1, 134221968, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.SynchronizationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_IsContextOK_m12135_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&SynchronizationAttribute_IsContextOK_m12135/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, SynchronizationAttribute_t2309_SynchronizationAttribute_IsContextOK_m12135_ParameterInfos/* parameters */
	, 525/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ExitContext()
extern const MethodInfo SynchronizationAttribute_ExitContext_m12136_MethodInfo = 
{
	"ExitContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_ExitContext_m12136/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::EnterContext()
extern const MethodInfo SynchronizationAttribute_EnterContext_m12137_MethodInfo = 
{
	"EnterContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_EnterContext_m12137/* method */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SynchronizationAttribute_t2309_MethodInfos[] =
{
	&SynchronizationAttribute__ctor_m12130_MethodInfo,
	&SynchronizationAttribute__ctor_m12131_MethodInfo,
	&SynchronizationAttribute_set_Locked_m12132_MethodInfo,
	&SynchronizationAttribute_ReleaseLock_m12133_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12134_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12135_MethodInfo,
	&SynchronizationAttribute_ExitContext_m12136_MethodInfo,
	&SynchronizationAttribute_EnterContext_m12137_MethodInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_set_Locked_m12132_MethodInfo;
static const PropertyInfo SynchronizationAttribute_t2309____Locked_PropertyInfo = 
{
	&SynchronizationAttribute_t2309_il2cpp_TypeInfo/* parent */
	, "Locked"/* name */
	, NULL/* get */
	, &SynchronizationAttribute_set_Locked_m12132_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SynchronizationAttribute_t2309_PropertyInfos[] =
{
	&SynchronizationAttribute_t2309____Locked_PropertyInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m12134_MethodInfo;
extern const MethodInfo SynchronizationAttribute_IsContextOK_m12135_MethodInfo;
static const Il2CppMethodReference SynchronizationAttribute_t2309_VTable[] =
{
	&ContextAttribute_Equals_m12125_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ContextAttribute_GetHashCode_m12126_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12134_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12135_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&ContextAttribute_get_Name_m12124_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12134_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12135_MethodInfo,
	&SynchronizationAttribute_set_Locked_m12132_MethodInfo,
};
static bool SynchronizationAttribute_t2309_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SynchronizationAttribute_t2309_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t2684_0_0_0,
	&IContributeServerContextSink_t2685_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t2309_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2605_0_0_0, 4},
	{ &IContextProperty_t2594_0_0_0, 6},
	{ &_Attribute_t901_0_0_0, 4},
	{ &IContributeClientContextSink_t2684_0_0_0, 10},
	{ &IContributeServerContextSink_t2685_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationAttribute_t2309_0_0_0;
extern const Il2CppType SynchronizationAttribute_t2309_1_0_0;
struct SynchronizationAttribute_t2309;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t2309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t2309_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t2309_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t2299_0_0_0/* parent */
	, SynchronizationAttribute_t2309_VTable/* vtableMethods */
	, SynchronizationAttribute_t2309_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1509/* fieldStart */

};
TypeInfo SynchronizationAttribute_t2309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, SynchronizationAttribute_t2309_MethodInfos/* methods */
	, SynchronizationAttribute_t2309_PropertyInfos/* properties */
	, NULL/* events */
	, &SynchronizationAttribute_t2309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 523/* custom_attributes_cache */
	, &SynchronizationAttribute_t2309_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t2309_1_0_0/* this_arg */
	, &SynchronizationAttribute_t2309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t2309)/* instance_size */
	, sizeof (SynchronizationAttribute_t2309)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t2310_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static const MethodInfo* ArgInfoType_t2310_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference ArgInfoType_t2310_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ArgInfoType_t2310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair ArgInfoType_t2310_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfoType_t2310_0_0_0;
extern const Il2CppType ArgInfoType_t2310_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t449_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t2310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t2310_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ArgInfoType_t2310_VTable/* vtableMethods */
	, ArgInfoType_t2310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1514/* fieldStart */

};
TypeInfo ArgInfoType_t2310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfoType_t2310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t2310_0_0_0/* byval_arg */
	, &ArgInfoType_t2310_1_0_0/* this_arg */
	, &ArgInfoType_t2310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t2310)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t2310)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t2311_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
extern const Il2CppType MethodBase_t1434_0_0_0;
extern const Il2CppType MethodBase_t1434_0_0_0;
extern const Il2CppType ArgInfoType_t2310_0_0_0;
static const ParameterInfo ArgInfo_t2311_ArgInfo__ctor_m12138_ParameterInfos[] = 
{
	{"method", 0, 134221969, 0, &MethodBase_t1434_0_0_0},
	{"type", 1, 134221970, 0, &ArgInfoType_t2310_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Byte_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ArgInfo::.ctor(System.Reflection.MethodBase,System.Runtime.Remoting.Messaging.ArgInfoType)
extern const MethodInfo ArgInfo__ctor_m12138_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgInfo__ctor_m12138/* method */
	, &ArgInfo_t2311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Byte_t449/* invoker_method */
	, ArgInfo_t2311_ArgInfo__ctor_m12138_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo ArgInfo_t2311_ArgInfo_GetInOutArgs_m12139_ParameterInfos[] = 
{
	{"args", 0, 134221971, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ArgInfo::GetInOutArgs(System.Object[])
extern const MethodInfo ArgInfo_GetInOutArgs_m12139_MethodInfo = 
{
	"GetInOutArgs"/* name */
	, (methodPointerType)&ArgInfo_GetInOutArgs_m12139/* method */
	, &ArgInfo_t2311_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ArgInfo_t2311_ArgInfo_GetInOutArgs_m12139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgInfo_t2311_MethodInfos[] =
{
	&ArgInfo__ctor_m12138_MethodInfo,
	&ArgInfo_GetInOutArgs_m12139_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArgInfo_t2311_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ArgInfo_t2311_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfo_t2311_0_0_0;
extern const Il2CppType ArgInfo_t2311_1_0_0;
struct ArgInfo_t2311;
const Il2CppTypeDefinitionMetadata ArgInfo_t2311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t2311_VTable/* vtableMethods */
	, ArgInfo_t2311_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1517/* fieldStart */

};
TypeInfo ArgInfo_t2311_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfo_t2311_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArgInfo_t2311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t2311_0_0_0/* byval_arg */
	, &ArgInfo_t2311_1_0_0/* this_arg */
	, &ArgInfo_t2311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t2311)/* instance_size */
	, sizeof (ArgInfo_t2311)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t2316_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::.ctor()
extern const MethodInfo AsyncResult__ctor_m12140_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsyncResult__ctor_m12140/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncState()
extern const MethodInfo AsyncResult_get_AsyncState_m12141_MethodInfo = 
{
	"get_AsyncState"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncState_m12141/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WaitHandle_t1802_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Threading.WaitHandle System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncWaitHandle()
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo = 
{
	"get_AsyncWaitHandle"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncWaitHandle_m12142/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &WaitHandle_t1802_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_CompletedSynchronously()
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m12143_MethodInfo = 
{
	"get_CompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_get_CompletedSynchronously_m12143/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_IsCompleted()
extern const MethodInfo AsyncResult_get_IsCompleted_m12144_MethodInfo = 
{
	"get_IsCompleted"/* name */
	, (methodPointerType)&AsyncResult_get_IsCompleted_m12144/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_EndInvokeCalled()
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m12145_MethodInfo = 
{
	"get_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_get_EndInvokeCalled_m12145/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_set_EndInvokeCalled_m12146_ParameterInfos[] = 
{
	{"value", 0, 134221972, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_EndInvokeCalled(System.Boolean)
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m12146_MethodInfo = 
{
	"set_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_set_EndInvokeCalled_m12146/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, AsyncResult_t2316_AsyncResult_set_EndInvokeCalled_m12146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate()
extern const MethodInfo AsyncResult_get_AsyncDelegate_m12147_MethodInfo = 
{
	"get_AsyncDelegate"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncDelegate_m12147/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.AsyncResult::get_NextSink()
extern const MethodInfo AsyncResult_get_NextSink_m12148_MethodInfo = 
{
	"get_NextSink"/* name */
	, (methodPointerType)&AsyncResult_get_NextSink_m12148/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1130_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t2315_0_0_0;
extern const Il2CppType IMessageSink_t1130_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_AsyncProcessMessage_m12149_ParameterInfos[] = 
{
	{"msg", 0, 134221973, 0, &IMessage_t2315_0_0_0},
	{"replySink", 1, 134221974, 0, &IMessageSink_t1130_0_0_0},
};
extern const Il2CppType IMessageCtrl_t2314_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.AsyncResult::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo AsyncResult_AsyncProcessMessage_m12149_MethodInfo = 
{
	"AsyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_AsyncProcessMessage_m12149/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &IMessageCtrl_t2314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, AsyncResult_t2316_AsyncResult_AsyncProcessMessage_m12149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::GetReplyMessage()
extern const MethodInfo AsyncResult_GetReplyMessage_m12150_MethodInfo = 
{
	"GetReplyMessage"/* name */
	, (methodPointerType)&AsyncResult_GetReplyMessage_m12150/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2315_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageCtrl_t2314_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_SetMessageCtrl_m12151_ParameterInfos[] = 
{
	{"mc", 0, 134221975, 0, &IMessageCtrl_t2314_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetMessageCtrl(System.Runtime.Remoting.Messaging.IMessageCtrl)
extern const MethodInfo AsyncResult_SetMessageCtrl_m12151_MethodInfo = 
{
	"SetMessageCtrl"/* name */
	, (methodPointerType)&AsyncResult_SetMessageCtrl_m12151/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AsyncResult_t2316_AsyncResult_SetMessageCtrl_m12151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_SetCompletedSynchronously_m12152_ParameterInfos[] = 
{
	{"completed", 0, 134221976, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetCompletedSynchronously(System.Boolean)
extern const MethodInfo AsyncResult_SetCompletedSynchronously_m12152_MethodInfo = 
{
	"SetCompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_SetCompletedSynchronously_m12152/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, AsyncResult_t2316_AsyncResult_SetCompletedSynchronously_m12152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::EndInvoke()
extern const MethodInfo AsyncResult_EndInvoke_m12153_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AsyncResult_EndInvoke_m12153/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2315_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t2315_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_SyncProcessMessage_m12154_ParameterInfos[] = 
{
	{"msg", 0, 134221977, 0, &IMessage_t2315_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern const MethodInfo AsyncResult_SyncProcessMessage_m12154_MethodInfo = 
{
	"SyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_SyncProcessMessage_m12154/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2315_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsyncResult_t2316_AsyncResult_SyncProcessMessage_m12154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t2313_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.MonoMethodMessage System.Runtime.Remoting.Messaging.AsyncResult::get_CallMessage()
extern const MethodInfo AsyncResult_get_CallMessage_m12155_MethodInfo = 
{
	"get_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_get_CallMessage_m12155/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodMessage_t2313_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t2313_0_0_0;
static const ParameterInfo AsyncResult_t2316_AsyncResult_set_CallMessage_m12156_ParameterInfos[] = 
{
	{"value", 0, 134221978, 0, &MonoMethodMessage_t2313_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_CallMessage(System.Runtime.Remoting.Messaging.MonoMethodMessage)
extern const MethodInfo AsyncResult_set_CallMessage_m12156_MethodInfo = 
{
	"set_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_set_CallMessage_m12156/* method */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AsyncResult_t2316_AsyncResult_set_CallMessage_m12156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsyncResult_t2316_MethodInfos[] =
{
	&AsyncResult__ctor_m12140_MethodInfo,
	&AsyncResult_get_AsyncState_m12141_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m12143_MethodInfo,
	&AsyncResult_get_IsCompleted_m12144_MethodInfo,
	&AsyncResult_get_EndInvokeCalled_m12145_MethodInfo,
	&AsyncResult_set_EndInvokeCalled_m12146_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m12147_MethodInfo,
	&AsyncResult_get_NextSink_m12148_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m12149_MethodInfo,
	&AsyncResult_GetReplyMessage_m12150_MethodInfo,
	&AsyncResult_SetMessageCtrl_m12151_MethodInfo,
	&AsyncResult_SetCompletedSynchronously_m12152_MethodInfo,
	&AsyncResult_EndInvoke_m12153_MethodInfo,
	&AsyncResult_SyncProcessMessage_m12154_MethodInfo,
	&AsyncResult_get_CallMessage_m12155_MethodInfo,
	&AsyncResult_set_CallMessage_m12156_MethodInfo,
	NULL
};
extern const MethodInfo AsyncResult_get_AsyncState_m12141_MethodInfo;
static const PropertyInfo AsyncResult_t2316____AsyncState_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "AsyncState"/* name */
	, &AsyncResult_get_AsyncState_m12141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo;
static const PropertyInfo AsyncResult_t2316____AsyncWaitHandle_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "AsyncWaitHandle"/* name */
	, &AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m12143_MethodInfo;
static const PropertyInfo AsyncResult_t2316____CompletedSynchronously_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "CompletedSynchronously"/* name */
	, &AsyncResult_get_CompletedSynchronously_m12143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_IsCompleted_m12144_MethodInfo;
static const PropertyInfo AsyncResult_t2316____IsCompleted_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "IsCompleted"/* name */
	, &AsyncResult_get_IsCompleted_m12144_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m12145_MethodInfo;
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m12146_MethodInfo;
static const PropertyInfo AsyncResult_t2316____EndInvokeCalled_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "EndInvokeCalled"/* name */
	, &AsyncResult_get_EndInvokeCalled_m12145_MethodInfo/* get */
	, &AsyncResult_set_EndInvokeCalled_m12146_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncDelegate_m12147_MethodInfo;
static const PropertyInfo AsyncResult_t2316____AsyncDelegate_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "AsyncDelegate"/* name */
	, &AsyncResult_get_AsyncDelegate_m12147_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_NextSink_m12148_MethodInfo;
static const PropertyInfo AsyncResult_t2316____NextSink_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "NextSink"/* name */
	, &AsyncResult_get_NextSink_m12148_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CallMessage_m12155_MethodInfo;
extern const MethodInfo AsyncResult_set_CallMessage_m12156_MethodInfo;
static const PropertyInfo AsyncResult_t2316____CallMessage_PropertyInfo = 
{
	&AsyncResult_t2316_il2cpp_TypeInfo/* parent */
	, "CallMessage"/* name */
	, &AsyncResult_get_CallMessage_m12155_MethodInfo/* get */
	, &AsyncResult_set_CallMessage_m12156_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AsyncResult_t2316_PropertyInfos[] =
{
	&AsyncResult_t2316____AsyncState_PropertyInfo,
	&AsyncResult_t2316____AsyncWaitHandle_PropertyInfo,
	&AsyncResult_t2316____CompletedSynchronously_PropertyInfo,
	&AsyncResult_t2316____IsCompleted_PropertyInfo,
	&AsyncResult_t2316____EndInvokeCalled_PropertyInfo,
	&AsyncResult_t2316____AsyncDelegate_PropertyInfo,
	&AsyncResult_t2316____NextSink_PropertyInfo,
	&AsyncResult_t2316____CallMessage_PropertyInfo,
	NULL
};
extern const MethodInfo AsyncResult_AsyncProcessMessage_m12149_MethodInfo;
extern const MethodInfo AsyncResult_GetReplyMessage_m12150_MethodInfo;
extern const MethodInfo AsyncResult_SetMessageCtrl_m12151_MethodInfo;
extern const MethodInfo AsyncResult_SyncProcessMessage_m12154_MethodInfo;
static const Il2CppMethodReference AsyncResult_t2316_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&AsyncResult_get_AsyncState_m12141_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo,
	&AsyncResult_get_IsCompleted_m12144_MethodInfo,
	&AsyncResult_get_AsyncState_m12141_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12142_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m12143_MethodInfo,
	&AsyncResult_get_IsCompleted_m12144_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m12147_MethodInfo,
	&AsyncResult_get_NextSink_m12148_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m12149_MethodInfo,
	&AsyncResult_GetReplyMessage_m12150_MethodInfo,
	&AsyncResult_SetMessageCtrl_m12151_MethodInfo,
	&AsyncResult_SyncProcessMessage_m12154_MethodInfo,
};
static bool AsyncResult_t2316_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const Il2CppType* AsyncResult_t2316_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t304_0_0_0,
	&IMessageSink_t1130_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t2316_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t304_0_0_0, 4},
	{ &IMessageSink_t1130_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncResult_t2316_0_0_0;
extern const Il2CppType AsyncResult_t2316_1_0_0;
struct AsyncResult_t2316;
const Il2CppTypeDefinitionMetadata AsyncResult_t2316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t2316_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t2316_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t2316_VTable/* vtableMethods */
	, AsyncResult_t2316_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1520/* fieldStart */

};
TypeInfo AsyncResult_t2316_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, AsyncResult_t2316_MethodInfos/* methods */
	, AsyncResult_t2316_PropertyInfos/* properties */
	, NULL/* events */
	, &AsyncResult_t2316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 526/* custom_attributes_cache */
	, &AsyncResult_t2316_0_0_0/* byval_arg */
	, &AsyncResult_t2316_1_0_0/* this_arg */
	, &AsyncResult_t2316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t2316)/* instance_size */
	, sizeof (AsyncResult_t2316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t2317_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall__ctor_m12157_ParameterInfos[] = 
{
	{"type", 0, 134221979, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Type)
extern const MethodInfo ConstructionCall__ctor_m12157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m12157/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall__ctor_m12157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall__ctor_m12158_ParameterInfos[] = 
{
	{"info", 0, 134221980, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134221981, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall__ctor_m12158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m12158/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall__ctor_m12158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitDictionary()
extern const MethodInfo ConstructionCall_InitDictionary_m12159_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&ConstructionCall_InitDictionary_m12159/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall_set_IsContextOk_m12160_ParameterInfos[] = 
{
	{"value", 0, 134221982, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_IsContextOk(System.Boolean)
extern const MethodInfo ConstructionCall_set_IsContextOk_m12160_MethodInfo = 
{
	"set_IsContextOk"/* name */
	, (methodPointerType)&ConstructionCall_set_IsContextOk_m12160/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall_set_IsContextOk_m12160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationType()
extern const MethodInfo ConstructionCall_get_ActivationType_m12161_MethodInfo = 
{
	"get_ActivationType"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationType_m12161/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationTypeName()
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m12162_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationTypeName_m12162/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Messaging.ConstructionCall::get_Activator()
extern const MethodInfo ConstructionCall_get_Activator_m12163_MethodInfo = 
{
	"get_Activator"/* name */
	, (methodPointerType)&ConstructionCall_get_Activator_m12163/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t2292_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall_set_Activator_m12164_ParameterInfos[] = 
{
	{"value", 0, 134221983, 0, &IActivator_t2292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ConstructionCall_set_Activator_m12164_MethodInfo = 
{
	"set_Activator"/* name */
	, (methodPointerType)&ConstructionCall_set_Activator_m12164/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall_set_Activator_m12164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ConstructionCall::get_CallSiteActivationAttributes()
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m12165_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_get_CallSiteActivationAttributes_m12165/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall_SetActivationAttributes_m12166_ParameterInfos[] = 
{
	{"attributes", 0, 134221984, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::SetActivationAttributes(System.Object[])
extern const MethodInfo ConstructionCall_SetActivationAttributes_m12166_MethodInfo = 
{
	"SetActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_SetActivationAttributes_m12166/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall_SetActivationAttributes_m12166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Messaging.ConstructionCall::get_ContextProperties()
extern const MethodInfo ConstructionCall_get_ContextProperties_m12167_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, (methodPointerType)&ConstructionCall_get_ContextProperties_m12167/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &IList_t1514_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall_InitMethodProperty_m12168_ParameterInfos[] = 
{
	{"key", 0, 134221985, 0, &String_t_0_0_0},
	{"value", 1, 134221986, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCall_InitMethodProperty_m12168_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&ConstructionCall_InitMethodProperty_m12168/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall_InitMethodProperty_m12168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ConstructionCall_t2317_ConstructionCall_GetObjectData_m12169_ParameterInfos[] = 
{
	{"info", 0, 134221987, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134221988, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall_GetObjectData_m12169_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ConstructionCall_GetObjectData_m12169/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ConstructionCall_t2317_ConstructionCall_GetObjectData_m12169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1931_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ConstructionCall::get_Properties()
extern const MethodInfo ConstructionCall_get_Properties_m12170_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ConstructionCall_get_Properties_m12170/* method */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCall_t2317_MethodInfos[] =
{
	&ConstructionCall__ctor_m12157_MethodInfo,
	&ConstructionCall__ctor_m12158_MethodInfo,
	&ConstructionCall_InitDictionary_m12159_MethodInfo,
	&ConstructionCall_set_IsContextOk_m12160_MethodInfo,
	&ConstructionCall_get_ActivationType_m12161_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m12162_MethodInfo,
	&ConstructionCall_get_Activator_m12163_MethodInfo,
	&ConstructionCall_set_Activator_m12164_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m12165_MethodInfo,
	&ConstructionCall_SetActivationAttributes_m12166_MethodInfo,
	&ConstructionCall_get_ContextProperties_m12167_MethodInfo,
	&ConstructionCall_InitMethodProperty_m12168_MethodInfo,
	&ConstructionCall_GetObjectData_m12169_MethodInfo,
	&ConstructionCall_get_Properties_m12170_MethodInfo,
	NULL
};
extern const MethodInfo ConstructionCall_set_IsContextOk_m12160_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____IsContextOk_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "IsContextOk"/* name */
	, NULL/* get */
	, &ConstructionCall_set_IsContextOk_m12160_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationType_m12161_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____ActivationType_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &ConstructionCall_get_ActivationType_m12161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m12162_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____ActivationTypeName_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &ConstructionCall_get_ActivationTypeName_m12162_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Activator_m12163_MethodInfo;
extern const MethodInfo ConstructionCall_set_Activator_m12164_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____Activator_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &ConstructionCall_get_Activator_m12163_MethodInfo/* get */
	, &ConstructionCall_set_Activator_m12164_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m12165_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____CallSiteActivationAttributes_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &ConstructionCall_get_CallSiteActivationAttributes_m12165_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ContextProperties_m12167_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____ContextProperties_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &ConstructionCall_get_ContextProperties_m12167_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Properties_m12170_MethodInfo;
static const PropertyInfo ConstructionCall_t2317____Properties_PropertyInfo = 
{
	&ConstructionCall_t2317_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ConstructionCall_get_Properties_m12170_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ConstructionCall_t2317_PropertyInfos[] =
{
	&ConstructionCall_t2317____IsContextOk_PropertyInfo,
	&ConstructionCall_t2317____ActivationType_PropertyInfo,
	&ConstructionCall_t2317____ActivationTypeName_PropertyInfo,
	&ConstructionCall_t2317____Activator_PropertyInfo,
	&ConstructionCall_t2317____CallSiteActivationAttributes_PropertyInfo,
	&ConstructionCall_t2317____ContextProperties_PropertyInfo,
	&ConstructionCall_t2317____Properties_PropertyInfo,
	NULL
};
extern const MethodInfo ConstructionCall_GetObjectData_m12169_MethodInfo;
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo;
extern const MethodInfo MethodCall_get_Args_m12191_MethodInfo;
extern const MethodInfo MethodCall_get_LogicalCallContext_m12192_MethodInfo;
extern const MethodInfo MethodCall_get_MethodBase_m12193_MethodInfo;
extern const MethodInfo MethodCall_get_MethodName_m12194_MethodInfo;
extern const MethodInfo MethodCall_get_MethodSignature_m12195_MethodInfo;
extern const MethodInfo MethodCall_get_TypeName_m12198_MethodInfo;
extern const MethodInfo MethodCall_get_Uri_m12199_MethodInfo;
extern const MethodInfo ConstructionCall_InitMethodProperty_m12168_MethodInfo;
extern const MethodInfo ConstructionCall_InitDictionary_m12159_MethodInfo;
extern const MethodInfo MethodCall_set_Uri_m12200_MethodInfo;
extern const MethodInfo MethodCall_Init_m12201_MethodInfo;
static const Il2CppMethodReference ConstructionCall_t2317_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ConstructionCall_GetObjectData_m12169_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo,
	&MethodCall_get_Args_m12191_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12192_MethodInfo,
	&MethodCall_get_MethodBase_m12193_MethodInfo,
	&MethodCall_get_MethodName_m12194_MethodInfo,
	&MethodCall_get_MethodSignature_m12195_MethodInfo,
	&MethodCall_get_TypeName_m12198_MethodInfo,
	&MethodCall_get_Uri_m12199_MethodInfo,
	&ConstructionCall_InitMethodProperty_m12168_MethodInfo,
	&ConstructionCall_GetObjectData_m12169_MethodInfo,
	&ConstructionCall_get_Properties_m12170_MethodInfo,
	&ConstructionCall_InitDictionary_m12159_MethodInfo,
	&MethodCall_set_Uri_m12200_MethodInfo,
	&MethodCall_Init_m12201_MethodInfo,
	&ConstructionCall_get_ActivationType_m12161_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m12162_MethodInfo,
	&ConstructionCall_get_Activator_m12163_MethodInfo,
	&ConstructionCall_set_Activator_m12164_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m12165_MethodInfo,
	&ConstructionCall_get_ContextProperties_m12167_MethodInfo,
};
static bool ConstructionCall_t2317_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionCall_t2317_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t2592_0_0_0,
	&IMessage_t2315_0_0_0,
	&IMethodCallMessage_t2596_0_0_0,
	&IMethodMessage_t2327_0_0_0,
};
extern const Il2CppType ISerializable_t513_0_0_0;
extern const Il2CppType IInternalMessage_t2608_0_0_0;
extern const Il2CppType ISerializationRootObject_t2687_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t2317_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &IInternalMessage_t2608_0_0_0, 5},
	{ &IMessage_t2315_0_0_0, 6},
	{ &IMethodCallMessage_t2596_0_0_0, 6},
	{ &IMethodMessage_t2327_0_0_0, 6},
	{ &ISerializationRootObject_t2687_0_0_0, 13},
	{ &IConstructionCallMessage_t2592_0_0_0, 19},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCall_t2317_1_0_0;
extern const Il2CppType MethodCall_t2318_0_0_0;
struct ConstructionCall_t2317;
const Il2CppTypeDefinitionMetadata ConstructionCall_t2317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t2317_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t2317_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t2318_0_0_0/* parent */
	, ConstructionCall_t2317_VTable/* vtableMethods */
	, ConstructionCall_t2317_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1535/* fieldStart */

};
TypeInfo ConstructionCall_t2317_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCall_t2317_MethodInfos/* methods */
	, ConstructionCall_t2317_PropertyInfos/* properties */
	, NULL/* events */
	, &ConstructionCall_t2317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 527/* custom_attributes_cache */
	, &ConstructionCall_t2317_0_0_0/* byval_arg */
	, &ConstructionCall_t2317_1_0_0/* this_arg */
	, &ConstructionCall_t2317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t2317)/* instance_size */
	, sizeof (ConstructionCall_t2317)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t2317_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t2319_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t2319_ConstructionCallDictionary__ctor_m12171_ParameterInfos[] = 
{
	{"message", 0, 134221989, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.ctor(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ConstructionCallDictionary__ctor_m12171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__ctor_m12171/* method */
	, &ConstructionCallDictionary_t2319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2319_ConstructionCallDictionary__ctor_m12171_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.cctor()
extern const MethodInfo ConstructionCallDictionary__cctor_m12172_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__cctor_m12172/* method */
	, &ConstructionCallDictionary_t2319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t2319_ConstructionCallDictionary_GetMethodProperty_m12173_ParameterInfos[] = 
{
	{"key", 0, 134221990, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ConstructionCallDictionary::GetMethodProperty(System.String)
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m12173_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_GetMethodProperty_m12173/* method */
	, &ConstructionCallDictionary_t2319_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2319_ConstructionCallDictionary_GetMethodProperty_m12173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t2319_ConstructionCallDictionary_SetMethodProperty_m12174_ParameterInfos[] = 
{
	{"key", 0, 134221991, 0, &String_t_0_0_0},
	{"value", 1, 134221992, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m12174_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_SetMethodProperty_m12174/* method */
	, &ConstructionCallDictionary_t2319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2319_ConstructionCallDictionary_SetMethodProperty_m12174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCallDictionary_t2319_MethodInfos[] =
{
	&ConstructionCallDictionary__ctor_m12171_MethodInfo,
	&ConstructionCallDictionary__cctor_m12172_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m12173_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m12174_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo;
extern const MethodInfo MethodDictionary_get_Count_m12228_MethodInfo;
extern const MethodInfo MethodDictionary_get_IsSynchronized_m12229_MethodInfo;
extern const MethodInfo MethodDictionary_get_SyncRoot_m12230_MethodInfo;
extern const MethodInfo MethodDictionary_CopyTo_m12231_MethodInfo;
extern const MethodInfo MethodDictionary_get_Item_m12220_MethodInfo;
extern const MethodInfo MethodDictionary_set_Item_m12221_MethodInfo;
extern const MethodInfo MethodDictionary_Add_m12225_MethodInfo;
extern const MethodInfo MethodDictionary_Contains_m12226_MethodInfo;
extern const MethodInfo MethodDictionary_GetEnumerator_m12232_MethodInfo;
extern const MethodInfo MethodDictionary_Remove_m12227_MethodInfo;
extern const MethodInfo MethodDictionary_AllocInternalProperties_m12217_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m12173_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m12174_MethodInfo;
extern const MethodInfo MethodDictionary_get_Values_m12224_MethodInfo;
static const Il2CppMethodReference ConstructionCallDictionary_t2319_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo,
	&MethodDictionary_get_Count_m12228_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12229_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12230_MethodInfo,
	&MethodDictionary_CopyTo_m12231_MethodInfo,
	&MethodDictionary_get_Item_m12220_MethodInfo,
	&MethodDictionary_set_Item_m12221_MethodInfo,
	&MethodDictionary_Add_m12225_MethodInfo,
	&MethodDictionary_Contains_m12226_MethodInfo,
	&MethodDictionary_GetEnumerator_m12232_MethodInfo,
	&MethodDictionary_Remove_m12227_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12217_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m12173_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m12174_MethodInfo,
	&MethodDictionary_get_Values_m12224_MethodInfo,
};
static bool ConstructionCallDictionary_t2319_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t550_0_0_0;
extern const Il2CppType ICollection_t1513_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t2319_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICollection_t1513_0_0_0, 5},
	{ &IDictionary_t1931_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCallDictionary_t2319_0_0_0;
extern const Il2CppType ConstructionCallDictionary_t2319_1_0_0;
extern const Il2CppType MethodDictionary_t2320_0_0_0;
struct ConstructionCallDictionary_t2319;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t2319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t2319_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2320_0_0_0/* parent */
	, ConstructionCallDictionary_t2319_VTable/* vtableMethods */
	, ConstructionCallDictionary_t2319_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1542/* fieldStart */

};
TypeInfo ConstructionCallDictionary_t2319_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCallDictionary_t2319_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionCallDictionary_t2319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t2319_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t2319_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t2319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t2319)/* instance_size */
	, sizeof (ConstructionCallDictionary_t2319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t2319_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t2321_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.ctor()
extern const MethodInfo EnvoyTerminatorSink__ctor_m12175_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__ctor_m12175/* method */
	, &EnvoyTerminatorSink_t2321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.cctor()
extern const MethodInfo EnvoyTerminatorSink__cctor_m12176_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__cctor_m12176/* method */
	, &EnvoyTerminatorSink_t2321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyTerminatorSink_t2321_MethodInfos[] =
{
	&EnvoyTerminatorSink__ctor_m12175_MethodInfo,
	&EnvoyTerminatorSink__cctor_m12176_MethodInfo,
	NULL
};
static const Il2CppMethodReference EnvoyTerminatorSink_t2321_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool EnvoyTerminatorSink_t2321_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* EnvoyTerminatorSink_t2321_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1130_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t2321_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1130_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyTerminatorSink_t2321_0_0_0;
extern const Il2CppType EnvoyTerminatorSink_t2321_1_0_0;
struct EnvoyTerminatorSink_t2321;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t2321_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t2321_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t2321_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t2321_VTable/* vtableMethods */
	, EnvoyTerminatorSink_t2321_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1545/* fieldStart */

};
TypeInfo EnvoyTerminatorSink_t2321_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, EnvoyTerminatorSink_t2321_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EnvoyTerminatorSink_t2321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t2321_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t2321_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t2321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t2321)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t2321)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t2321_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t2322_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Header_t2322_Header__ctor_m12177_ParameterInfos[] = 
{
	{"_Name", 0, 134221993, 0, &String_t_0_0_0},
	{"_Value", 1, 134221994, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object)
extern const MethodInfo Header__ctor_m12177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12177/* method */
	, &Header_t2322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Header_t2322_Header__ctor_m12177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Header_t2322_Header__ctor_m12178_ParameterInfos[] = 
{
	{"_Name", 0, 134221995, 0, &String_t_0_0_0},
	{"_Value", 1, 134221996, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134221997, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean)
extern const MethodInfo Header__ctor_m12178_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12178/* method */
	, &Header_t2322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170/* invoker_method */
	, Header_t2322_Header__ctor_m12178_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Header_t2322_Header__ctor_m12179_ParameterInfos[] = 
{
	{"_Name", 0, 134221998, 0, &String_t_0_0_0},
	{"_Value", 1, 134221999, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134222000, 0, &Boolean_t169_0_0_0},
	{"_HeaderNamespace", 3, 134222001, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean,System.String)
extern const MethodInfo Header__ctor_m12179_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12179/* method */
	, &Header_t2322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_Object_t/* invoker_method */
	, Header_t2322_Header__ctor_m12179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Header_t2322_MethodInfos[] =
{
	&Header__ctor_m12177_MethodInfo,
	&Header__ctor_m12178_MethodInfo,
	&Header__ctor_m12179_MethodInfo,
	NULL
};
static const Il2CppMethodReference Header_t2322_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Header_t2322_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Header_t2322_0_0_0;
extern const Il2CppType Header_t2322_1_0_0;
struct Header_t2322;
const Il2CppTypeDefinitionMetadata Header_t2322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t2322_VTable/* vtableMethods */
	, Header_t2322_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1546/* fieldStart */

};
TypeInfo Header_t2322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, Header_t2322_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Header_t2322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 531/* custom_attributes_cache */
	, &Header_t2322_0_0_0/* byval_arg */
	, &Header_t2322_1_0_0/* this_arg */
	, &Header_t2322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t2322)/* instance_size */
	, sizeof (Header_t2322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t2608_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IInternalMessage_t2608_IInternalMessage_set_Uri_m14562_ParameterInfos[] = 
{
	{"value", 0, 134222002, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.IInternalMessage::set_Uri(System.String)
extern const MethodInfo IInternalMessage_set_Uri_m14562_MethodInfo = 
{
	"set_Uri"/* name */
	, NULL/* method */
	, &IInternalMessage_t2608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IInternalMessage_t2608_IInternalMessage_set_Uri_m14562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IInternalMessage_t2608_MethodInfos[] =
{
	&IInternalMessage_set_Uri_m14562_MethodInfo,
	NULL
};
extern const MethodInfo IInternalMessage_set_Uri_m14562_MethodInfo;
static const PropertyInfo IInternalMessage_t2608____Uri_PropertyInfo = 
{
	&IInternalMessage_t2608_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, NULL/* get */
	, &IInternalMessage_set_Uri_m14562_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IInternalMessage_t2608_PropertyInfos[] =
{
	&IInternalMessage_t2608____Uri_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IInternalMessage_t2608_1_0_0;
struct IInternalMessage_t2608;
const Il2CppTypeDefinitionMetadata IInternalMessage_t2608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IInternalMessage_t2608_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IInternalMessage_t2608_MethodInfos/* methods */
	, IInternalMessage_t2608_PropertyInfos/* properties */
	, NULL/* events */
	, &IInternalMessage_t2608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t2608_0_0_0/* byval_arg */
	, &IInternalMessage_t2608_1_0_0/* this_arg */
	, &IInternalMessage_t2608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t2315_il2cpp_TypeInfo;
static const MethodInfo* IMessage_t2315_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessage_t2315_1_0_0;
struct IMessage_t2315;
const Il2CppTypeDefinitionMetadata IMessage_t2315_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessage_t2315_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessage_t2315_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessage_t2315_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 532/* custom_attributes_cache */
	, &IMessage_t2315_0_0_0/* byval_arg */
	, &IMessage_t2315_1_0_0/* this_arg */
	, &IMessage_t2315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t2314_il2cpp_TypeInfo;
static const MethodInfo* IMessageCtrl_t2314_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageCtrl_t2314_1_0_0;
struct IMessageCtrl_t2314;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t2314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageCtrl_t2314_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageCtrl_t2314_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageCtrl_t2314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 533/* custom_attributes_cache */
	, &IMessageCtrl_t2314_0_0_0/* byval_arg */
	, &IMessageCtrl_t2314_1_0_0/* this_arg */
	, &IMessageCtrl_t2314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t1130_il2cpp_TypeInfo;
static const MethodInfo* IMessageSink_t1130_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageSink_t1130_1_0_0;
struct IMessageSink_t1130;
const Il2CppTypeDefinitionMetadata IMessageSink_t1130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageSink_t1130_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageSink_t1130_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageSink_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 534/* custom_attributes_cache */
	, &IMessageSink_t1130_0_0_0/* byval_arg */
	, &IMessageSink_t1130_1_0_0/* this_arg */
	, &IMessageSink_t1130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t2596_il2cpp_TypeInfo;
static const MethodInfo* IMethodCallMessage_t2596_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IMethodCallMessage_t2596_InterfacesTypeInfos[] = 
{
	&IMessage_t2315_0_0_0,
	&IMethodMessage_t2327_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodCallMessage_t2596_1_0_0;
struct IMethodCallMessage_t2596;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t2596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t2596_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodCallMessage_t2596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodCallMessage_t2596_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMethodCallMessage_t2596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 535/* custom_attributes_cache */
	, &IMethodCallMessage_t2596_0_0_0/* byval_arg */
	, &IMethodCallMessage_t2596_1_0_0/* this_arg */
	, &IMethodCallMessage_t2596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t2327_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args()
extern const MethodInfo IMethodMessage_get_Args_m14563_MethodInfo = 
{
	"get_Args"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogicalCallContext_t2324_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.IMethodMessage::get_LogicalCallContext()
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m14564_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2324_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase()
extern const MethodInfo IMethodMessage_get_MethodBase_m14565_MethodInfo = 
{
	"get_MethodBase"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodName()
extern const MethodInfo IMethodMessage_get_MethodName_m14566_MethodInfo = 
{
	"get_MethodName"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodSignature()
extern const MethodInfo IMethodMessage_get_MethodSignature_m14567_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_TypeName()
extern const MethodInfo IMethodMessage_get_TypeName_m14568_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri()
extern const MethodInfo IMethodMessage_get_Uri_m14569_MethodInfo = 
{
	"get_Uri"/* name */
	, NULL/* method */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodMessage_t2327_MethodInfos[] =
{
	&IMethodMessage_get_Args_m14563_MethodInfo,
	&IMethodMessage_get_LogicalCallContext_m14564_MethodInfo,
	&IMethodMessage_get_MethodBase_m14565_MethodInfo,
	&IMethodMessage_get_MethodName_m14566_MethodInfo,
	&IMethodMessage_get_MethodSignature_m14567_MethodInfo,
	&IMethodMessage_get_TypeName_m14568_MethodInfo,
	&IMethodMessage_get_Uri_m14569_MethodInfo,
	NULL
};
extern const MethodInfo IMethodMessage_get_Args_m14563_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____Args_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &IMethodMessage_get_Args_m14563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m14564_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____LogicalCallContext_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &IMethodMessage_get_LogicalCallContext_m14564_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodBase_m14565_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____MethodBase_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &IMethodMessage_get_MethodBase_m14565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodName_m14566_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____MethodName_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &IMethodMessage_get_MethodName_m14566_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodSignature_m14567_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____MethodSignature_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &IMethodMessage_get_MethodSignature_m14567_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_TypeName_m14568_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____TypeName_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IMethodMessage_get_TypeName_m14568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_Uri_m14569_MethodInfo;
static const PropertyInfo IMethodMessage_t2327____Uri_PropertyInfo = 
{
	&IMethodMessage_t2327_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &IMethodMessage_get_Uri_m14569_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodMessage_t2327_PropertyInfos[] =
{
	&IMethodMessage_t2327____Args_PropertyInfo,
	&IMethodMessage_t2327____LogicalCallContext_PropertyInfo,
	&IMethodMessage_t2327____MethodBase_PropertyInfo,
	&IMethodMessage_t2327____MethodName_PropertyInfo,
	&IMethodMessage_t2327____MethodSignature_PropertyInfo,
	&IMethodMessage_t2327____TypeName_PropertyInfo,
	&IMethodMessage_t2327____Uri_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodMessage_t2327_InterfacesTypeInfos[] = 
{
	&IMessage_t2315_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodMessage_t2327_1_0_0;
struct IMethodMessage_t2327;
const Il2CppTypeDefinitionMetadata IMethodMessage_t2327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t2327_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodMessage_t2327_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodMessage_t2327_MethodInfos/* methods */
	, IMethodMessage_t2327_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodMessage_t2327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 536/* custom_attributes_cache */
	, &IMethodMessage_t2327_0_0_0/* byval_arg */
	, &IMethodMessage_t2327_1_0_0/* this_arg */
	, &IMethodMessage_t2327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t2595_il2cpp_TypeInfo;
extern const Il2CppType Exception_t140_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception()
extern const MethodInfo IMethodReturnMessage_get_Exception_m14570_MethodInfo = 
{
	"get_Exception"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2595_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t140_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_OutArgs()
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m14571_MethodInfo = 
{
	"get_OutArgs"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2595_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_ReturnValue()
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m14572_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2595_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodReturnMessage_t2595_MethodInfos[] =
{
	&IMethodReturnMessage_get_Exception_m14570_MethodInfo,
	&IMethodReturnMessage_get_OutArgs_m14571_MethodInfo,
	&IMethodReturnMessage_get_ReturnValue_m14572_MethodInfo,
	NULL
};
extern const MethodInfo IMethodReturnMessage_get_Exception_m14570_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2595____Exception_PropertyInfo = 
{
	&IMethodReturnMessage_t2595_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &IMethodReturnMessage_get_Exception_m14570_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m14571_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2595____OutArgs_PropertyInfo = 
{
	&IMethodReturnMessage_t2595_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &IMethodReturnMessage_get_OutArgs_m14571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m14572_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t2595____ReturnValue_PropertyInfo = 
{
	&IMethodReturnMessage_t2595_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &IMethodReturnMessage_get_ReturnValue_m14572_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodReturnMessage_t2595_PropertyInfos[] =
{
	&IMethodReturnMessage_t2595____Exception_PropertyInfo,
	&IMethodReturnMessage_t2595____OutArgs_PropertyInfo,
	&IMethodReturnMessage_t2595____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodReturnMessage_t2595_InterfacesTypeInfos[] = 
{
	&IMessage_t2315_0_0_0,
	&IMethodMessage_t2327_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodReturnMessage_t2595_0_0_0;
extern const Il2CppType IMethodReturnMessage_t2595_1_0_0;
struct IMethodReturnMessage_t2595;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t2595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t2595_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodReturnMessage_t2595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodReturnMessage_t2595_MethodInfos/* methods */
	, IMethodReturnMessage_t2595_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodReturnMessage_t2595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 537/* custom_attributes_cache */
	, &IMethodReturnMessage_t2595_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t2595_1_0_0/* this_arg */
	, &IMethodReturnMessage_t2595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t2686_il2cpp_TypeInfo;
static const MethodInfo* IRemotingFormatter_t2686_MethodInfos[] =
{
	NULL
};
extern const Il2CppType IFormatter_t2688_0_0_0;
static const Il2CppType* IRemotingFormatter_t2686_InterfacesTypeInfos[] = 
{
	&IFormatter_t2688_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingFormatter_t2686_0_0_0;
extern const Il2CppType IRemotingFormatter_t2686_1_0_0;
struct IRemotingFormatter_t2686;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t2686_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t2686_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingFormatter_t2686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IRemotingFormatter_t2686_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IRemotingFormatter_t2686_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 538/* custom_attributes_cache */
	, &IRemotingFormatter_t2686_0_0_0/* byval_arg */
	, &IRemotingFormatter_t2686_1_0_0/* this_arg */
	, &IRemotingFormatter_t2686_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t2687_il2cpp_TypeInfo;
static const MethodInfo* ISerializationRootObject_t2687_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationRootObject_t2687_1_0_0;
struct ISerializationRootObject_t2687;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t2687_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISerializationRootObject_t2687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ISerializationRootObject_t2687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISerializationRootObject_t2687_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t2687_0_0_0/* byval_arg */
	, &ISerializationRootObject_t2687_1_0_0/* this_arg */
	, &ISerializationRootObject_t2687_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t2324_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor()
extern const MethodInfo LogicalCallContext__ctor_m12180_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m12180/* method */
	, &LogicalCallContext_t2324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo LogicalCallContext_t2324_LogicalCallContext__ctor_m12181_ParameterInfos[] = 
{
	{"info", 0, 134222003, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222004, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext__ctor_m12181_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m12181/* method */
	, &LogicalCallContext_t2324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, LogicalCallContext_t2324_LogicalCallContext__ctor_m12181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo LogicalCallContext_t2324_LogicalCallContext_GetObjectData_m12182_ParameterInfos[] = 
{
	{"info", 0, 134222005, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222006, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext_GetObjectData_m12182_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LogicalCallContext_GetObjectData_m12182/* method */
	, &LogicalCallContext_t2324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, LogicalCallContext_t2324_LogicalCallContext_GetObjectData_m12182_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LogicalCallContext_t2324_LogicalCallContext_SetData_m12183_ParameterInfos[] = 
{
	{"name", 0, 134222007, 0, &String_t_0_0_0},
	{"data", 1, 134222008, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::SetData(System.String,System.Object)
extern const MethodInfo LogicalCallContext_SetData_m12183_MethodInfo = 
{
	"SetData"/* name */
	, (methodPointerType)&LogicalCallContext_SetData_m12183/* method */
	, &LogicalCallContext_t2324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, LogicalCallContext_t2324_LogicalCallContext_SetData_m12183_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LogicalCallContext_t2324_MethodInfos[] =
{
	&LogicalCallContext__ctor_m12180_MethodInfo,
	&LogicalCallContext__ctor_m12181_MethodInfo,
	&LogicalCallContext_GetObjectData_m12182_MethodInfo,
	&LogicalCallContext_SetData_m12183_MethodInfo,
	NULL
};
extern const MethodInfo LogicalCallContext_GetObjectData_m12182_MethodInfo;
static const Il2CppMethodReference LogicalCallContext_t2324_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&LogicalCallContext_GetObjectData_m12182_MethodInfo,
};
static bool LogicalCallContext_t2324_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
static const Il2CppType* LogicalCallContext_t2324_InterfacesTypeInfos[] = 
{
	&ICloneable_t512_0_0_0,
	&ISerializable_t513_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t2324_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LogicalCallContext_t2324_1_0_0;
struct LogicalCallContext_t2324;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t2324_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t2324_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t2324_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t2324_VTable/* vtableMethods */
	, LogicalCallContext_t2324_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1550/* fieldStart */

};
TypeInfo LogicalCallContext_t2324_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, LogicalCallContext_t2324_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LogicalCallContext_t2324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 539/* custom_attributes_cache */
	, &LogicalCallContext_t2324_0_0_0/* byval_arg */
	, &LogicalCallContext_t2324_1_0_0/* this_arg */
	, &LogicalCallContext_t2324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t2324)/* instance_size */
	, sizeof (LogicalCallContext_t2324)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t2323_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern const MethodInfo CallContextRemotingData__ctor_m12184_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CallContextRemotingData__ctor_m12184/* method */
	, &CallContextRemotingData_t2323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CallContextRemotingData_t2323_MethodInfos[] =
{
	&CallContextRemotingData__ctor_m12184_MethodInfo,
	NULL
};
static const Il2CppMethodReference CallContextRemotingData_t2323_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CallContextRemotingData_t2323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CallContextRemotingData_t2323_InterfacesTypeInfos[] = 
{
	&ICloneable_t512_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t2323_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallContextRemotingData_t2323_0_0_0;
extern const Il2CppType CallContextRemotingData_t2323_1_0_0;
struct CallContextRemotingData_t2323;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t2323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t2323_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t2323_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t2323_VTable/* vtableMethods */
	, CallContextRemotingData_t2323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CallContextRemotingData_t2323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, CallContextRemotingData_t2323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CallContextRemotingData_t2323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t2323_0_0_0/* byval_arg */
	, &CallContextRemotingData_t2323_1_0_0/* this_arg */
	, &CallContextRemotingData_t2323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t2323)/* instance_size */
	, sizeof (CallContextRemotingData_t2323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t2318_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
extern const Il2CppType HeaderU5BU5D_t2560_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2560_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall__ctor_m12185_ParameterInfos[] = 
{
	{"h1", 0, 134222009, 0, &HeaderU5BU5D_t2560_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo MethodCall__ctor_m12185_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12185/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall__ctor_m12185_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall__ctor_m12186_ParameterInfos[] = 
{
	{"info", 0, 134222010, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222011, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall__ctor_m12186_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12186/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MethodCall_t2318_MethodCall__ctor_m12186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor()
extern const MethodInfo MethodCall__ctor_m12187_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12187/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_ParameterInfos[] = 
{
	{"value", 0, 134222012, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_InitMethodProperty_m12189_ParameterInfos[] = 
{
	{"key", 0, 134222013, 0, &String_t_0_0_0},
	{"value", 1, 134222014, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo MethodCall_InitMethodProperty_m12189_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&MethodCall_InitMethodProperty_m12189/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall_InitMethodProperty_m12189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_GetObjectData_m12190_ParameterInfos[] = 
{
	{"info", 0, 134222015, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222016, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall_GetObjectData_m12190_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MethodCall_GetObjectData_m12190/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MethodCall_t2318_MethodCall_GetObjectData_m12190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MethodCall::get_Args()
extern const MethodInfo MethodCall_get_Args_m12191_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MethodCall_get_Args_m12191/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodCall::get_LogicalCallContext()
extern const MethodInfo MethodCall_get_LogicalCallContext_m12192_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MethodCall_get_LogicalCallContext_m12192/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2324_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodCall::get_MethodBase()
extern const MethodInfo MethodCall_get_MethodBase_m12193_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MethodCall_get_MethodBase_m12193/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_MethodName()
extern const MethodInfo MethodCall_get_MethodName_m12194_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MethodCall_get_MethodName_m12194/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodCall::get_MethodSignature()
extern const MethodInfo MethodCall_get_MethodSignature_m12195_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MethodCall_get_MethodSignature_m12195/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCall::get_Properties()
extern const MethodInfo MethodCall_get_Properties_m12196_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&MethodCall_get_Properties_m12196/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitDictionary()
extern const MethodInfo MethodCall_InitDictionary_m12197_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&MethodCall_InitDictionary_m12197/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_TypeName()
extern const MethodInfo MethodCall_get_TypeName_m12198_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MethodCall_get_TypeName_m12198/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_Uri()
extern const MethodInfo MethodCall_get_Uri_m12199_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MethodCall_get_Uri_m12199/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_set_Uri_m12200_ParameterInfos[] = 
{
	{"value", 0, 134222017, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::set_Uri(System.String)
extern const MethodInfo MethodCall_set_Uri_m12200_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MethodCall_set_Uri_m12200/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall_set_Uri_m12200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::Init()
extern const MethodInfo MethodCall_Init_m12201_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&MethodCall_Init_m12201/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::ResolveMethod()
extern const MethodInfo MethodCall_ResolveMethod_m12202_MethodInfo = 
{
	"ResolveMethod"/* name */
	, (methodPointerType)&MethodCall_ResolveMethod_m12202/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_CastTo_m12203_ParameterInfos[] = 
{
	{"clientType", 0, 134222018, 0, &String_t_0_0_0},
	{"serverType", 1, 134222019, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.MethodCall::CastTo(System.String,System.Type)
extern const MethodInfo MethodCall_CastTo_m12203_MethodInfo = 
{
	"CastTo"/* name */
	, (methodPointerType)&MethodCall_CastTo_m12203/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall_CastTo_m12203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t2318_MethodCall_GetTypeNameFromAssemblyQualifiedName_m12204_ParameterInfos[] = 
{
	{"aqname", 0, 134222020, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::GetTypeNameFromAssemblyQualifiedName(System.String)
extern const MethodInfo MethodCall_GetTypeNameFromAssemblyQualifiedName_m12204_MethodInfo = 
{
	"GetTypeNameFromAssemblyQualifiedName"/* name */
	, (methodPointerType)&MethodCall_GetTypeNameFromAssemblyQualifiedName_m12204/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodCall_t2318_MethodCall_GetTypeNameFromAssemblyQualifiedName_m12204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Runtime.Remoting.Messaging.MethodCall::get_GenericArguments()
extern const MethodInfo MethodCall_get_GenericArguments_m12205_MethodInfo = 
{
	"get_GenericArguments"/* name */
	, (methodPointerType)&MethodCall_get_GenericArguments_m12205/* method */
	, &MethodCall_t2318_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t878_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCall_t2318_MethodInfos[] =
{
	&MethodCall__ctor_m12185_MethodInfo,
	&MethodCall__ctor_m12186_MethodInfo,
	&MethodCall__ctor_m12187_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo,
	&MethodCall_InitMethodProperty_m12189_MethodInfo,
	&MethodCall_GetObjectData_m12190_MethodInfo,
	&MethodCall_get_Args_m12191_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12192_MethodInfo,
	&MethodCall_get_MethodBase_m12193_MethodInfo,
	&MethodCall_get_MethodName_m12194_MethodInfo,
	&MethodCall_get_MethodSignature_m12195_MethodInfo,
	&MethodCall_get_Properties_m12196_MethodInfo,
	&MethodCall_InitDictionary_m12197_MethodInfo,
	&MethodCall_get_TypeName_m12198_MethodInfo,
	&MethodCall_get_Uri_m12199_MethodInfo,
	&MethodCall_set_Uri_m12200_MethodInfo,
	&MethodCall_Init_m12201_MethodInfo,
	&MethodCall_ResolveMethod_m12202_MethodInfo,
	&MethodCall_CastTo_m12203_MethodInfo,
	&MethodCall_GetTypeNameFromAssemblyQualifiedName_m12204_MethodInfo,
	&MethodCall_get_GenericArguments_m12205_MethodInfo,
	NULL
};
static const PropertyInfo MethodCall_t2318____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____Args_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MethodCall_get_Args_m12191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____LogicalCallContext_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MethodCall_get_LogicalCallContext_m12192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____MethodBase_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MethodCall_get_MethodBase_m12193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____MethodName_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MethodCall_get_MethodName_m12194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____MethodSignature_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MethodCall_get_MethodSignature_m12195_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_Properties_m12196_MethodInfo;
static const PropertyInfo MethodCall_t2318____Properties_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &MethodCall_get_Properties_m12196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____TypeName_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MethodCall_get_TypeName_m12198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t2318____Uri_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MethodCall_get_Uri_m12199_MethodInfo/* get */
	, &MethodCall_set_Uri_m12200_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_GenericArguments_m12205_MethodInfo;
static const PropertyInfo MethodCall_t2318____GenericArguments_PropertyInfo = 
{
	&MethodCall_t2318_il2cpp_TypeInfo/* parent */
	, "GenericArguments"/* name */
	, &MethodCall_get_GenericArguments_m12205_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodCall_t2318_PropertyInfos[] =
{
	&MethodCall_t2318____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&MethodCall_t2318____Args_PropertyInfo,
	&MethodCall_t2318____LogicalCallContext_PropertyInfo,
	&MethodCall_t2318____MethodBase_PropertyInfo,
	&MethodCall_t2318____MethodName_PropertyInfo,
	&MethodCall_t2318____MethodSignature_PropertyInfo,
	&MethodCall_t2318____Properties_PropertyInfo,
	&MethodCall_t2318____TypeName_PropertyInfo,
	&MethodCall_t2318____Uri_PropertyInfo,
	&MethodCall_t2318____GenericArguments_PropertyInfo,
	NULL
};
extern const MethodInfo MethodCall_GetObjectData_m12190_MethodInfo;
extern const MethodInfo MethodCall_InitMethodProperty_m12189_MethodInfo;
extern const MethodInfo MethodCall_InitDictionary_m12197_MethodInfo;
static const Il2CppMethodReference MethodCall_t2318_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MethodCall_GetObjectData_m12190_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12188_MethodInfo,
	&MethodCall_get_Args_m12191_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12192_MethodInfo,
	&MethodCall_get_MethodBase_m12193_MethodInfo,
	&MethodCall_get_MethodName_m12194_MethodInfo,
	&MethodCall_get_MethodSignature_m12195_MethodInfo,
	&MethodCall_get_TypeName_m12198_MethodInfo,
	&MethodCall_get_Uri_m12199_MethodInfo,
	&MethodCall_InitMethodProperty_m12189_MethodInfo,
	&MethodCall_GetObjectData_m12190_MethodInfo,
	&MethodCall_get_Properties_m12196_MethodInfo,
	&MethodCall_InitDictionary_m12197_MethodInfo,
	&MethodCall_set_Uri_m12200_MethodInfo,
	&MethodCall_Init_m12201_MethodInfo,
};
static bool MethodCall_t2318_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodCall_t2318_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
	&IInternalMessage_t2608_0_0_0,
	&IMessage_t2315_0_0_0,
	&IMethodCallMessage_t2596_0_0_0,
	&IMethodMessage_t2327_0_0_0,
	&ISerializationRootObject_t2687_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t2318_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &IInternalMessage_t2608_0_0_0, 5},
	{ &IMessage_t2315_0_0_0, 6},
	{ &IMethodCallMessage_t2596_0_0_0, 6},
	{ &IMethodMessage_t2327_0_0_0, 6},
	{ &ISerializationRootObject_t2687_0_0_0, 13},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCall_t2318_1_0_0;
struct MethodCall_t2318;
const Il2CppTypeDefinitionMetadata MethodCall_t2318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t2318_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t2318_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t2318_VTable/* vtableMethods */
	, MethodCall_t2318_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1552/* fieldStart */

};
TypeInfo MethodCall_t2318_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCall_t2318_MethodInfos/* methods */
	, MethodCall_t2318_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodCall_t2318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 540/* custom_attributes_cache */
	, &MethodCall_t2318_0_0_0/* byval_arg */
	, &MethodCall_t2318_1_0_0/* this_arg */
	, &MethodCall_t2318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t2318)/* instance_size */
	, sizeof (MethodCall_t2318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t2318_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t2325_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t2327_0_0_0;
static const ParameterInfo MethodCallDictionary_t2325_MethodCallDictionary__ctor_m12206_ParameterInfos[] = 
{
	{"message", 0, 134222021, 0, &IMethodMessage_t2327_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodCallDictionary__ctor_m12206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCallDictionary__ctor_m12206/* method */
	, &MethodCallDictionary_t2325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodCallDictionary_t2325_MethodCallDictionary__ctor_m12206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
extern const MethodInfo MethodCallDictionary__cctor_m12207_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodCallDictionary__cctor_m12207/* method */
	, &MethodCallDictionary_t2325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCallDictionary_t2325_MethodInfos[] =
{
	&MethodCallDictionary__ctor_m12206_MethodInfo,
	&MethodCallDictionary__cctor_m12207_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_GetMethodProperty_m12222_MethodInfo;
extern const MethodInfo MethodDictionary_SetMethodProperty_m12223_MethodInfo;
static const Il2CppMethodReference MethodCallDictionary_t2325_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo,
	&MethodDictionary_get_Count_m12228_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12229_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12230_MethodInfo,
	&MethodDictionary_CopyTo_m12231_MethodInfo,
	&MethodDictionary_get_Item_m12220_MethodInfo,
	&MethodDictionary_set_Item_m12221_MethodInfo,
	&MethodDictionary_Add_m12225_MethodInfo,
	&MethodDictionary_Contains_m12226_MethodInfo,
	&MethodDictionary_GetEnumerator_m12232_MethodInfo,
	&MethodDictionary_Remove_m12227_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12217_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12222_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12223_MethodInfo,
	&MethodDictionary_get_Values_m12224_MethodInfo,
};
static bool MethodCallDictionary_t2325_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t2325_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICollection_t1513_0_0_0, 5},
	{ &IDictionary_t1931_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCallDictionary_t2325_0_0_0;
extern const Il2CppType MethodCallDictionary_t2325_1_0_0;
struct MethodCallDictionary_t2325;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t2325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t2325_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2320_0_0_0/* parent */
	, MethodCallDictionary_t2325_VTable/* vtableMethods */
	, MethodCallDictionary_t2325_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1563/* fieldStart */

};
TypeInfo MethodCallDictionary_t2325_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCallDictionary_t2325_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodCallDictionary_t2325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t2325_0_0_0/* byval_arg */
	, &MethodCallDictionary_t2325_1_0_0/* this_arg */
	, &MethodCallDictionary_t2325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t2325)/* instance_size */
	, sizeof (MethodCallDictionary_t2325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t2325_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t2326_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
extern const Il2CppType MethodDictionary_t2320_0_0_0;
static const ParameterInfo DictionaryEnumerator_t2326_DictionaryEnumerator__ctor_m12208_ParameterInfos[] = 
{
	{"methodDictionary", 0, 134222037, 0, &MethodDictionary_t2320_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::.ctor(System.Runtime.Remoting.Messaging.MethodDictionary)
extern const MethodInfo DictionaryEnumerator__ctor_m12208_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DictionaryEnumerator__ctor_m12208/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, DictionaryEnumerator_t2326_DictionaryEnumerator__ctor_m12208_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Current()
extern const MethodInfo DictionaryEnumerator_get_Current_m12209_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Current_m12209/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::MoveNext()
extern const MethodInfo DictionaryEnumerator_MoveNext_m12210_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&DictionaryEnumerator_MoveNext_m12210/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DictionaryEntry_t1996_0_0_0;
extern void* RuntimeInvoker_DictionaryEntry_t1996 (const MethodInfo* method, void* obj, void** args);
// System.Collections.DictionaryEntry System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Entry()
extern const MethodInfo DictionaryEnumerator_get_Entry_m12211_MethodInfo = 
{
	"get_Entry"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Entry_m12211/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &DictionaryEntry_t1996_0_0_0/* return_type */
	, RuntimeInvoker_DictionaryEntry_t1996/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Key()
extern const MethodInfo DictionaryEnumerator_get_Key_m12212_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Key_m12212/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Value()
extern const MethodInfo DictionaryEnumerator_get_Value_m12213_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Value_m12213/* method */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DictionaryEnumerator_t2326_MethodInfos[] =
{
	&DictionaryEnumerator__ctor_m12208_MethodInfo,
	&DictionaryEnumerator_get_Current_m12209_MethodInfo,
	&DictionaryEnumerator_MoveNext_m12210_MethodInfo,
	&DictionaryEnumerator_get_Entry_m12211_MethodInfo,
	&DictionaryEnumerator_get_Key_m12212_MethodInfo,
	&DictionaryEnumerator_get_Value_m12213_MethodInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_get_Current_m12209_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t2326____Current_PropertyInfo = 
{
	&DictionaryEnumerator_t2326_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &DictionaryEnumerator_get_Current_m12209_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Entry_m12211_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t2326____Entry_PropertyInfo = 
{
	&DictionaryEnumerator_t2326_il2cpp_TypeInfo/* parent */
	, "Entry"/* name */
	, &DictionaryEnumerator_get_Entry_m12211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Key_m12212_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t2326____Key_PropertyInfo = 
{
	&DictionaryEnumerator_t2326_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &DictionaryEnumerator_get_Key_m12212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Value_m12213_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t2326____Value_PropertyInfo = 
{
	&DictionaryEnumerator_t2326_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DictionaryEnumerator_get_Value_m12213_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DictionaryEnumerator_t2326_PropertyInfos[] =
{
	&DictionaryEnumerator_t2326____Current_PropertyInfo,
	&DictionaryEnumerator_t2326____Entry_PropertyInfo,
	&DictionaryEnumerator_t2326____Key_PropertyInfo,
	&DictionaryEnumerator_t2326____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_MoveNext_m12210_MethodInfo;
static const Il2CppMethodReference DictionaryEnumerator_t2326_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&DictionaryEnumerator_get_Current_m12209_MethodInfo,
	&DictionaryEnumerator_MoveNext_m12210_MethodInfo,
	&DictionaryEnumerator_get_Entry_m12211_MethodInfo,
	&DictionaryEnumerator_get_Key_m12212_MethodInfo,
	&DictionaryEnumerator_get_Value_m12213_MethodInfo,
};
static bool DictionaryEnumerator_t2326_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t410_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t1995_0_0_0;
static const Il2CppType* DictionaryEnumerator_t2326_InterfacesTypeInfos[] = 
{
	&IEnumerator_t410_0_0_0,
	&IDictionaryEnumerator_t1995_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t2326_InterfacesOffsets[] = 
{
	{ &IEnumerator_t410_0_0_0, 4},
	{ &IDictionaryEnumerator_t1995_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEnumerator_t2326_0_0_0;
extern const Il2CppType DictionaryEnumerator_t2326_1_0_0;
extern TypeInfo MethodDictionary_t2320_il2cpp_TypeInfo;
struct DictionaryEnumerator_t2326;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t2326_DefinitionMetadata = 
{
	&MethodDictionary_t2320_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t2326_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t2326_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t2326_VTable/* vtableMethods */
	, DictionaryEnumerator_t2326_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1564/* fieldStart */

};
TypeInfo DictionaryEnumerator_t2326_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, DictionaryEnumerator_t2326_MethodInfos/* methods */
	, DictionaryEnumerator_t2326_PropertyInfos/* properties */
	, NULL/* events */
	, &DictionaryEnumerator_t2326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t2326_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t2326_1_0_0/* this_arg */
	, &DictionaryEnumerator_t2326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t2326)/* instance_size */
	, sizeof (DictionaryEnumerator_t2326)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t2327_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary__ctor_m12214_ParameterInfos[] = 
{
	{"message", 0, 134222022, 0, &IMethodMessage_t2327_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodDictionary__ctor_m12214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodDictionary__ctor_m12214/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary__ctor_m12214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
extern const Il2CppType StringU5BU5D_t109_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_set_MethodKeys_m12216_ParameterInfos[] = 
{
	{"value", 0, 134222023, 0, &StringU5BU5D_t109_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_MethodKeys(System.String[])
extern const MethodInfo MethodDictionary_set_MethodKeys_m12216_MethodInfo = 
{
	"set_MethodKeys"/* name */
	, (methodPointerType)&MethodDictionary_set_MethodKeys_m12216/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_set_MethodKeys_m12216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::AllocInternalProperties()
extern const MethodInfo MethodDictionary_AllocInternalProperties_m12217_MethodInfo = 
{
	"AllocInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_AllocInternalProperties_m12217/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::GetInternalProperties()
extern const MethodInfo MethodDictionary_GetInternalProperties_m12218_MethodInfo = 
{
	"GetInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_GetInternalProperties_m12218/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_IsOverridenKey_m12219_ParameterInfos[] = 
{
	{"key", 0, 134222024, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::IsOverridenKey(System.String)
extern const MethodInfo MethodDictionary_IsOverridenKey_m12219_MethodInfo = 
{
	"IsOverridenKey"/* name */
	, (methodPointerType)&MethodDictionary_IsOverridenKey_m12219/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_IsOverridenKey_m12219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_get_Item_m12220_ParameterInfos[] = 
{
	{"key", 0, 134222025, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_Item(System.Object)
extern const MethodInfo MethodDictionary_get_Item_m12220_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&MethodDictionary_get_Item_m12220/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_get_Item_m12220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_set_Item_m12221_ParameterInfos[] = 
{
	{"key", 0, 134222026, 0, &Object_t_0_0_0},
	{"value", 1, 134222027, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_Item(System.Object,System.Object)
extern const MethodInfo MethodDictionary_set_Item_m12221_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&MethodDictionary_set_Item_m12221/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_set_Item_m12221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_GetMethodProperty_m12222_ParameterInfos[] = 
{
	{"key", 0, 134222028, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::GetMethodProperty(System.String)
extern const MethodInfo MethodDictionary_GetMethodProperty_m12222_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_GetMethodProperty_m12222/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_GetMethodProperty_m12222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_SetMethodProperty_m12223_ParameterInfos[] = 
{
	{"key", 0, 134222029, 0, &String_t_0_0_0},
	{"value", 1, 134222030, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo MethodDictionary_SetMethodProperty_m12223_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_SetMethodProperty_m12223/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_SetMethodProperty_m12223_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.ICollection System.Runtime.Remoting.Messaging.MethodDictionary::get_Values()
extern const MethodInfo MethodDictionary_get_Values_m12224_MethodInfo = 
{
	"get_Values"/* name */
	, (methodPointerType)&MethodDictionary_get_Values_m12224/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_t1513_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_Add_m12225_ParameterInfos[] = 
{
	{"key", 0, 134222031, 0, &Object_t_0_0_0},
	{"value", 1, 134222032, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Add(System.Object,System.Object)
extern const MethodInfo MethodDictionary_Add_m12225_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&MethodDictionary_Add_m12225/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_Add_m12225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_Contains_m12226_ParameterInfos[] = 
{
	{"key", 0, 134222033, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::Contains(System.Object)
extern const MethodInfo MethodDictionary_Contains_m12226_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&MethodDictionary_Contains_m12226/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_Contains_m12226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_Remove_m12227_ParameterInfos[] = 
{
	{"key", 0, 134222034, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Remove(System.Object)
extern const MethodInfo MethodDictionary_Remove_m12227_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&MethodDictionary_Remove_m12227/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_Remove_m12227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MethodDictionary::get_Count()
extern const MethodInfo MethodDictionary_get_Count_m12228_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&MethodDictionary_get_Count_m12228/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::get_IsSynchronized()
extern const MethodInfo MethodDictionary_get_IsSynchronized_m12229_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&MethodDictionary_get_IsSynchronized_m12229/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_SyncRoot()
extern const MethodInfo MethodDictionary_get_SyncRoot_m12230_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&MethodDictionary_get_SyncRoot_m12230/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo MethodDictionary_t2320_MethodDictionary_CopyTo_m12231_ParameterInfos[] = 
{
	{"array", 0, 134222035, 0, &Array_t_0_0_0},
	{"index", 1, 134222036, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::CopyTo(System.Array,System.Int32)
extern const MethodInfo MethodDictionary_CopyTo_m12231_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&MethodDictionary_CopyTo_m12231/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, MethodDictionary_t2320_MethodDictionary_CopyTo_m12231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::GetEnumerator()
extern const MethodInfo MethodDictionary_GetEnumerator_m12232_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_GetEnumerator_m12232/* method */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* declaring_type */
	, &IDictionaryEnumerator_t1995_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodDictionary_t2320_MethodInfos[] =
{
	&MethodDictionary__ctor_m12214_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo,
	&MethodDictionary_set_MethodKeys_m12216_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12217_MethodInfo,
	&MethodDictionary_GetInternalProperties_m12218_MethodInfo,
	&MethodDictionary_IsOverridenKey_m12219_MethodInfo,
	&MethodDictionary_get_Item_m12220_MethodInfo,
	&MethodDictionary_set_Item_m12221_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12222_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12223_MethodInfo,
	&MethodDictionary_get_Values_m12224_MethodInfo,
	&MethodDictionary_Add_m12225_MethodInfo,
	&MethodDictionary_Contains_m12226_MethodInfo,
	&MethodDictionary_Remove_m12227_MethodInfo,
	&MethodDictionary_get_Count_m12228_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12229_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12230_MethodInfo,
	&MethodDictionary_CopyTo_m12231_MethodInfo,
	&MethodDictionary_GetEnumerator_m12232_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_set_MethodKeys_m12216_MethodInfo;
static const PropertyInfo MethodDictionary_t2320____MethodKeys_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "MethodKeys"/* name */
	, NULL/* get */
	, &MethodDictionary_set_MethodKeys_m12216_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t2320____Item_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &MethodDictionary_get_Item_m12220_MethodInfo/* get */
	, &MethodDictionary_set_Item_m12221_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t2320____Values_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &MethodDictionary_get_Values_m12224_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t2320____Count_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &MethodDictionary_get_Count_m12228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t2320____IsSynchronized_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &MethodDictionary_get_IsSynchronized_m12229_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t2320____SyncRoot_PropertyInfo = 
{
	&MethodDictionary_t2320_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &MethodDictionary_get_SyncRoot_m12230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodDictionary_t2320_PropertyInfos[] =
{
	&MethodDictionary_t2320____MethodKeys_PropertyInfo,
	&MethodDictionary_t2320____Item_PropertyInfo,
	&MethodDictionary_t2320____Values_PropertyInfo,
	&MethodDictionary_t2320____Count_PropertyInfo,
	&MethodDictionary_t2320____IsSynchronized_PropertyInfo,
	&MethodDictionary_t2320____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* MethodDictionary_t2320_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t2326_0_0_0,
};
static const Il2CppMethodReference MethodDictionary_t2320_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo,
	&MethodDictionary_get_Count_m12228_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12229_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12230_MethodInfo,
	&MethodDictionary_CopyTo_m12231_MethodInfo,
	&MethodDictionary_get_Item_m12220_MethodInfo,
	&MethodDictionary_set_Item_m12221_MethodInfo,
	&MethodDictionary_Add_m12225_MethodInfo,
	&MethodDictionary_Contains_m12226_MethodInfo,
	&MethodDictionary_GetEnumerator_m12232_MethodInfo,
	&MethodDictionary_Remove_m12227_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12217_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12222_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12223_MethodInfo,
	&MethodDictionary_get_Values_m12224_MethodInfo,
};
static bool MethodDictionary_t2320_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodDictionary_t2320_InterfacesTypeInfos[] = 
{
	&IEnumerable_t550_0_0_0,
	&ICollection_t1513_0_0_0,
	&IDictionary_t1931_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t2320_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICollection_t1513_0_0_0, 5},
	{ &IDictionary_t1931_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodDictionary_t2320_1_0_0;
struct MethodDictionary_t2320;
const Il2CppTypeDefinitionMetadata MethodDictionary_t2320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t2320_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t2320_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t2320_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t2320_VTable/* vtableMethods */
	, MethodDictionary_t2320_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1567/* fieldStart */

};
TypeInfo MethodDictionary_t2320_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodDictionary_t2320_MethodInfos/* methods */
	, MethodDictionary_t2320_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodDictionary_t2320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 542/* custom_attributes_cache */
	, &MethodDictionary_t2320_0_0_0/* byval_arg */
	, &MethodDictionary_t2320_1_0_0/* this_arg */
	, &MethodDictionary_t2320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t2320)/* instance_size */
	, sizeof (MethodDictionary_t2320)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t2320_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t2328_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
extern const Il2CppType IMethodReturnMessage_t2595_0_0_0;
static const ParameterInfo MethodReturnDictionary_t2328_MethodReturnDictionary__ctor_m12233_ParameterInfos[] = 
{
	{"message", 0, 134222038, 0, &IMethodReturnMessage_t2595_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern const MethodInfo MethodReturnDictionary__ctor_m12233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__ctor_m12233/* method */
	, &MethodReturnDictionary_t2328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MethodReturnDictionary_t2328_MethodReturnDictionary__ctor_m12233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern const MethodInfo MethodReturnDictionary__cctor_m12234_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__cctor_m12234/* method */
	, &MethodReturnDictionary_t2328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodReturnDictionary_t2328_MethodInfos[] =
{
	&MethodReturnDictionary__ctor_m12233_MethodInfo,
	&MethodReturnDictionary__cctor_m12234_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodReturnDictionary_t2328_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12215_MethodInfo,
	&MethodDictionary_get_Count_m12228_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12229_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12230_MethodInfo,
	&MethodDictionary_CopyTo_m12231_MethodInfo,
	&MethodDictionary_get_Item_m12220_MethodInfo,
	&MethodDictionary_set_Item_m12221_MethodInfo,
	&MethodDictionary_Add_m12225_MethodInfo,
	&MethodDictionary_Contains_m12226_MethodInfo,
	&MethodDictionary_GetEnumerator_m12232_MethodInfo,
	&MethodDictionary_Remove_m12227_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12217_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12222_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12223_MethodInfo,
	&MethodDictionary_get_Values_m12224_MethodInfo,
};
static bool MethodReturnDictionary_t2328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t2328_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICollection_t1513_0_0_0, 5},
	{ &IDictionary_t1931_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodReturnDictionary_t2328_0_0_0;
extern const Il2CppType MethodReturnDictionary_t2328_1_0_0;
struct MethodReturnDictionary_t2328;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t2328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t2328_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2320_0_0_0/* parent */
	, MethodReturnDictionary_t2328_VTable/* vtableMethods */
	, MethodReturnDictionary_t2328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1573/* fieldStart */

};
TypeInfo MethodReturnDictionary_t2328_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodReturnDictionary_t2328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodReturnDictionary_t2328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t2328_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t2328_1_0_0/* this_arg */
	, &MethodReturnDictionary_t2328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t2328)/* instance_size */
	, sizeof (MethodReturnDictionary_t2328)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t2328_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t2313_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Args()
extern const MethodInfo MonoMethodMessage_get_Args_m12235_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Args_m12235/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::get_LogicalCallContext()
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m12236_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MonoMethodMessage_get_LogicalCallContext_m12236/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2324_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodBase()
extern const MethodInfo MonoMethodMessage_get_MethodBase_m12237_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodBase_m12237/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodName()
extern const MethodInfo MonoMethodMessage_get_MethodName_m12238_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodName_m12238/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodSignature()
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m12239_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodSignature_m12239/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_TypeName()
extern const MethodInfo MonoMethodMessage_get_TypeName_m12240_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_TypeName_m12240/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Uri()
extern const MethodInfo MonoMethodMessage_get_Uri_m12241_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Uri_m12241/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MonoMethodMessage_t2313_MonoMethodMessage_set_Uri_m12242_ParameterInfos[] = 
{
	{"value", 0, 134222039, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_Uri(System.String)
extern const MethodInfo MonoMethodMessage_set_Uri_m12242_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_set_Uri_m12242/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MonoMethodMessage_t2313_MonoMethodMessage_set_Uri_m12242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Exception()
extern const MethodInfo MonoMethodMessage_get_Exception_m12243_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Exception_m12243/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t140_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgCount()
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m12244_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgCount_m12244/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgs()
extern const MethodInfo MonoMethodMessage_get_OutArgs_m12245_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgs_m12245/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ReturnValue()
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m12246_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&MonoMethodMessage_get_ReturnValue_m12246/* method */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethodMessage_t2313_MethodInfos[] =
{
	&MonoMethodMessage_get_Args_m12235_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m12236_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m12237_MethodInfo,
	&MonoMethodMessage_get_MethodName_m12238_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m12239_MethodInfo,
	&MonoMethodMessage_get_TypeName_m12240_MethodInfo,
	&MonoMethodMessage_get_Uri_m12241_MethodInfo,
	&MonoMethodMessage_set_Uri_m12242_MethodInfo,
	&MonoMethodMessage_get_Exception_m12243_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m12244_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m12245_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m12246_MethodInfo,
	NULL
};
extern const MethodInfo MonoMethodMessage_get_Args_m12235_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____Args_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MonoMethodMessage_get_Args_m12235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m12236_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____LogicalCallContext_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MonoMethodMessage_get_LogicalCallContext_m12236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodBase_m12237_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____MethodBase_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MonoMethodMessage_get_MethodBase_m12237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodName_m12238_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____MethodName_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MonoMethodMessage_get_MethodName_m12238_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m12239_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____MethodSignature_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MonoMethodMessage_get_MethodSignature_m12239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_TypeName_m12240_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____TypeName_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MonoMethodMessage_get_TypeName_m12240_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Uri_m12241_MethodInfo;
extern const MethodInfo MonoMethodMessage_set_Uri_m12242_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____Uri_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MonoMethodMessage_get_Uri_m12241_MethodInfo/* get */
	, &MonoMethodMessage_set_Uri_m12242_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Exception_m12243_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____Exception_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &MonoMethodMessage_get_Exception_m12243_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m12244_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____OutArgCount_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &MonoMethodMessage_get_OutArgCount_m12244_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgs_m12245_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____OutArgs_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &MonoMethodMessage_get_OutArgs_m12245_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m12246_MethodInfo;
static const PropertyInfo MonoMethodMessage_t2313____ReturnValue_PropertyInfo = 
{
	&MonoMethodMessage_t2313_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &MonoMethodMessage_get_ReturnValue_m12246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoMethodMessage_t2313_PropertyInfos[] =
{
	&MonoMethodMessage_t2313____Args_PropertyInfo,
	&MonoMethodMessage_t2313____LogicalCallContext_PropertyInfo,
	&MonoMethodMessage_t2313____MethodBase_PropertyInfo,
	&MonoMethodMessage_t2313____MethodName_PropertyInfo,
	&MonoMethodMessage_t2313____MethodSignature_PropertyInfo,
	&MonoMethodMessage_t2313____TypeName_PropertyInfo,
	&MonoMethodMessage_t2313____Uri_PropertyInfo,
	&MonoMethodMessage_t2313____Exception_PropertyInfo,
	&MonoMethodMessage_t2313____OutArgCount_PropertyInfo,
	&MonoMethodMessage_t2313____OutArgs_PropertyInfo,
	&MonoMethodMessage_t2313____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoMethodMessage_t2313_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MonoMethodMessage_set_Uri_m12242_MethodInfo,
	&MonoMethodMessage_get_Args_m12235_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m12236_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m12237_MethodInfo,
	&MonoMethodMessage_get_MethodName_m12238_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m12239_MethodInfo,
	&MonoMethodMessage_get_TypeName_m12240_MethodInfo,
	&MonoMethodMessage_get_Uri_m12241_MethodInfo,
	&MonoMethodMessage_get_Exception_m12243_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m12245_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m12246_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m12244_MethodInfo,
};
static bool MonoMethodMessage_t2313_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethodMessage_t2313_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2608_0_0_0,
	&IMessage_t2315_0_0_0,
	&IMethodCallMessage_t2596_0_0_0,
	&IMethodMessage_t2327_0_0_0,
	&IMethodReturnMessage_t2595_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t2313_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2608_0_0_0, 4},
	{ &IMessage_t2315_0_0_0, 5},
	{ &IMethodCallMessage_t2596_0_0_0, 5},
	{ &IMethodMessage_t2327_0_0_0, 5},
	{ &IMethodReturnMessage_t2595_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodMessage_t2313_1_0_0;
struct MonoMethodMessage_t2313;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t2313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t2313_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t2313_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t2313_VTable/* vtableMethods */
	, MonoMethodMessage_t2313_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1575/* fieldStart */

};
TypeInfo MonoMethodMessage_t2313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MonoMethodMessage_t2313_MethodInfos/* methods */
	, MonoMethodMessage_t2313_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoMethodMessage_t2313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t2313_0_0_0/* byval_arg */
	, &MonoMethodMessage_t2313_1_0_0/* this_arg */
	, &MonoMethodMessage_t2313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t2313)/* instance_size */
	, sizeof (MonoMethodMessage_t2313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 11/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t2329_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::.ctor()
extern const MethodInfo RemotingSurrogate__ctor_m12247_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogate__ctor_m12247/* method */
	, &RemotingSurrogate_t2329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType ISurrogateSelector_t2331_0_0_0;
extern const Il2CppType ISurrogateSelector_t2331_0_0_0;
static const ParameterInfo RemotingSurrogate_t2329_RemotingSurrogate_SetObjectData_m12248_ParameterInfos[] = 
{
	{"obj", 0, 134222040, 0, &Object_t_0_0_0},
	{"si", 1, 134222041, 0, &SerializationInfo_t1382_0_0_0},
	{"sc", 2, 134222042, 0, &StreamingContext_t1383_0_0_0},
	{"selector", 3, 134222043, 0, &ISurrogateSelector_t2331_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1383_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo RemotingSurrogate_SetObjectData_m12248_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&RemotingSurrogate_SetObjectData_m12248/* method */
	, &RemotingSurrogate_t2329_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1383_Object_t/* invoker_method */
	, RemotingSurrogate_t2329_RemotingSurrogate_SetObjectData_m12248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogate_t2329_MethodInfos[] =
{
	&RemotingSurrogate__ctor_m12247_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12248_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogate_SetObjectData_m12248_MethodInfo;
static const Il2CppMethodReference RemotingSurrogate_t2329_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12248_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12248_MethodInfo,
};
static bool RemotingSurrogate_t2329_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationSurrogate_t2382_0_0_0;
static const Il2CppType* RemotingSurrogate_t2329_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2382_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t2329_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2382_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogate_t2329_0_0_0;
extern const Il2CppType RemotingSurrogate_t2329_1_0_0;
struct RemotingSurrogate_t2329;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t2329_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t2329_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t2329_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t2329_VTable/* vtableMethods */
	, RemotingSurrogate_t2329_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingSurrogate_t2329_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogate_t2329_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogate_t2329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t2329_0_0_0/* byval_arg */
	, &RemotingSurrogate_t2329_1_0_0/* this_arg */
	, &RemotingSurrogate_t2329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t2329)/* instance_size */
	, sizeof (RemotingSurrogate_t2329)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t2330_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::.ctor()
extern const MethodInfo ObjRefSurrogate__ctor_m12249_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRefSurrogate__ctor_m12249/* method */
	, &ObjRefSurrogate_t2330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType ISurrogateSelector_t2331_0_0_0;
static const ParameterInfo ObjRefSurrogate_t2330_ObjRefSurrogate_SetObjectData_m12250_ParameterInfos[] = 
{
	{"obj", 0, 134222044, 0, &Object_t_0_0_0},
	{"si", 1, 134222045, 0, &SerializationInfo_t1382_0_0_0},
	{"sc", 2, 134222046, 0, &StreamingContext_t1383_0_0_0},
	{"selector", 3, 134222047, 0, &ISurrogateSelector_t2331_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1383_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ObjRefSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo ObjRefSurrogate_SetObjectData_m12250_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&ObjRefSurrogate_SetObjectData_m12250/* method */
	, &ObjRefSurrogate_t2330_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1383_Object_t/* invoker_method */
	, ObjRefSurrogate_t2330_ObjRefSurrogate_SetObjectData_m12250_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRefSurrogate_t2330_MethodInfos[] =
{
	&ObjRefSurrogate__ctor_m12249_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12250_MethodInfo,
	NULL
};
extern const MethodInfo ObjRefSurrogate_SetObjectData_m12250_MethodInfo;
static const Il2CppMethodReference ObjRefSurrogate_t2330_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12250_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12250_MethodInfo,
};
static bool ObjRefSurrogate_t2330_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ObjRefSurrogate_t2330_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2382_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t2330_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2382_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRefSurrogate_t2330_0_0_0;
extern const Il2CppType ObjRefSurrogate_t2330_1_0_0;
struct ObjRefSurrogate_t2330;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t2330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t2330_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t2330_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t2330_VTable/* vtableMethods */
	, ObjRefSurrogate_t2330_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ObjRefSurrogate_t2330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ObjRefSurrogate_t2330_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjRefSurrogate_t2330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t2330_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t2330_1_0_0/* this_arg */
	, &ObjRefSurrogate_t2330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t2330)/* instance_size */
	, sizeof (ObjRefSurrogate_t2330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t2332_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.ctor()
extern const MethodInfo RemotingSurrogateSelector__ctor_m12251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__ctor_m12251/* method */
	, &RemotingSurrogateSelector_t2332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.cctor()
extern const MethodInfo RemotingSurrogateSelector__cctor_m12252_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__cctor_m12252/* method */
	, &RemotingSurrogateSelector_t2332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType ISurrogateSelector_t2331_1_0_2;
extern const Il2CppType ISurrogateSelector_t2331_1_0_0;
static const ParameterInfo RemotingSurrogateSelector_t2332_RemotingSurrogateSelector_GetSurrogate_m12253_ParameterInfos[] = 
{
	{"type", 0, 134222048, 0, &Type_t_0_0_0},
	{"context", 1, 134222049, 0, &StreamingContext_t1383_0_0_0},
	{"ssout", 2, 134222050, 0, &ISurrogateSelector_t2331_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1383_ISurrogateSelectorU26_t3058 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m12253_MethodInfo = 
{
	"GetSurrogate"/* name */
	, (methodPointerType)&RemotingSurrogateSelector_GetSurrogate_m12253/* method */
	, &RemotingSurrogateSelector_t2332_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t2382_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1383_ISurrogateSelectorU26_t3058/* invoker_method */
	, RemotingSurrogateSelector_t2332_RemotingSurrogateSelector_GetSurrogate_m12253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogateSelector_t2332_MethodInfos[] =
{
	&RemotingSurrogateSelector__ctor_m12251_MethodInfo,
	&RemotingSurrogateSelector__cctor_m12252_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12253_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m12253_MethodInfo;
static const Il2CppMethodReference RemotingSurrogateSelector_t2332_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12253_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12253_MethodInfo,
};
static bool RemotingSurrogateSelector_t2332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemotingSurrogateSelector_t2332_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t2331_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t2332_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t2331_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogateSelector_t2332_0_0_0;
extern const Il2CppType RemotingSurrogateSelector_t2332_1_0_0;
struct RemotingSurrogateSelector_t2332;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t2332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t2332_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t2332_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t2332_VTable/* vtableMethods */
	, RemotingSurrogateSelector_t2332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1583/* fieldStart */

};
TypeInfo RemotingSurrogateSelector_t2332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogateSelector_t2332_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogateSelector_t2332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 545/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t2332_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t2332_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t2332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t2332)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t2332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t2332_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t2333_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LogicalCallContext_t2324_0_0_0;
extern const Il2CppType IMethodCallMessage_t2596_0_0_0;
static const ParameterInfo ReturnMessage_t2333_ReturnMessage__ctor_m12254_ParameterInfos[] = 
{
	{"ret", 0, 134222051, 0, &Object_t_0_0_0},
	{"outArgs", 1, 134222052, 0, &ObjectU5BU5D_t115_0_0_0},
	{"outArgsCount", 2, 134222053, 0, &Int32_t127_0_0_0},
	{"callCtx", 3, 134222054, 0, &LogicalCallContext_t2324_0_0_0},
	{"mcm", 4, 134222055, 0, &IMethodCallMessage_t2596_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Object,System.Object[],System.Int32,System.Runtime.Remoting.Messaging.LogicalCallContext,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m12254_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m12254/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t2333_ReturnMessage__ctor_m12254_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Exception_t140_0_0_0;
extern const Il2CppType IMethodCallMessage_t2596_0_0_0;
static const ParameterInfo ReturnMessage_t2333_ReturnMessage__ctor_m12255_ParameterInfos[] = 
{
	{"e", 0, 134222056, 0, &Exception_t140_0_0_0},
	{"mcm", 1, 134222057, 0, &IMethodCallMessage_t2596_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Exception,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m12255_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m12255/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t2333_ReturnMessage__ctor_m12255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t2333_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_ParameterInfos[] = 
{
	{"value", 0, 134222058, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ReturnMessage_t2333_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_Args()
extern const MethodInfo ReturnMessage_get_Args_m12257_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&ReturnMessage_get_Args_m12257/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.ReturnMessage::get_LogicalCallContext()
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m12258_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&ReturnMessage_get_LogicalCallContext_m12258/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2324_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodBase()
extern const MethodInfo ReturnMessage_get_MethodBase_m12259_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodBase_m12259/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodName()
extern const MethodInfo ReturnMessage_get_MethodName_m12260_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodName_m12260/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodSignature()
extern const MethodInfo ReturnMessage_get_MethodSignature_m12261_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodSignature_m12261/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ReturnMessage::get_Properties()
extern const MethodInfo ReturnMessage_get_Properties_m12262_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ReturnMessage_get_Properties_m12262/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_TypeName()
extern const MethodInfo ReturnMessage_get_TypeName_m12263_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&ReturnMessage_get_TypeName_m12263/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_Uri()
extern const MethodInfo ReturnMessage_get_Uri_m12264_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&ReturnMessage_get_Uri_m12264/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t2333_ReturnMessage_set_Uri_m12265_ParameterInfos[] = 
{
	{"value", 0, 134222059, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::set_Uri(System.String)
extern const MethodInfo ReturnMessage_set_Uri_m12265_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_set_Uri_m12265/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ReturnMessage_t2333_ReturnMessage_set_Uri_m12265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.ReturnMessage::get_Exception()
extern const MethodInfo ReturnMessage_get_Exception_m12266_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&ReturnMessage_get_Exception_m12266/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t140_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_OutArgs()
extern const MethodInfo ReturnMessage_get_OutArgs_m12267_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&ReturnMessage_get_OutArgs_m12267/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_ReturnValue()
extern const MethodInfo ReturnMessage_get_ReturnValue_m12268_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&ReturnMessage_get_ReturnValue_m12268/* method */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReturnMessage_t2333_MethodInfos[] =
{
	&ReturnMessage__ctor_m12254_MethodInfo,
	&ReturnMessage__ctor_m12255_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_MethodInfo,
	&ReturnMessage_get_Args_m12257_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m12258_MethodInfo,
	&ReturnMessage_get_MethodBase_m12259_MethodInfo,
	&ReturnMessage_get_MethodName_m12260_MethodInfo,
	&ReturnMessage_get_MethodSignature_m12261_MethodInfo,
	&ReturnMessage_get_Properties_m12262_MethodInfo,
	&ReturnMessage_get_TypeName_m12263_MethodInfo,
	&ReturnMessage_get_Uri_m12264_MethodInfo,
	&ReturnMessage_set_Uri_m12265_MethodInfo,
	&ReturnMessage_get_Exception_m12266_MethodInfo,
	&ReturnMessage_get_OutArgs_m12267_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12268_MethodInfo,
	NULL
};
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Args_m12257_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____Args_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &ReturnMessage_get_Args_m12257_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m12258_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____LogicalCallContext_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &ReturnMessage_get_LogicalCallContext_m12258_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodBase_m12259_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____MethodBase_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &ReturnMessage_get_MethodBase_m12259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodName_m12260_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____MethodName_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &ReturnMessage_get_MethodName_m12260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodSignature_m12261_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____MethodSignature_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &ReturnMessage_get_MethodSignature_m12261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Properties_m12262_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____Properties_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ReturnMessage_get_Properties_m12262_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_TypeName_m12263_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____TypeName_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &ReturnMessage_get_TypeName_m12263_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Uri_m12264_MethodInfo;
extern const MethodInfo ReturnMessage_set_Uri_m12265_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____Uri_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &ReturnMessage_get_Uri_m12264_MethodInfo/* get */
	, &ReturnMessage_set_Uri_m12265_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Exception_m12266_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____Exception_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &ReturnMessage_get_Exception_m12266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_OutArgs_m12267_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____OutArgs_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &ReturnMessage_get_OutArgs_m12267_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_ReturnValue_m12268_MethodInfo;
static const PropertyInfo ReturnMessage_t2333____ReturnValue_PropertyInfo = 
{
	&ReturnMessage_t2333_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &ReturnMessage_get_ReturnValue_m12268_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReturnMessage_t2333_PropertyInfos[] =
{
	&ReturnMessage_t2333____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&ReturnMessage_t2333____Args_PropertyInfo,
	&ReturnMessage_t2333____LogicalCallContext_PropertyInfo,
	&ReturnMessage_t2333____MethodBase_PropertyInfo,
	&ReturnMessage_t2333____MethodName_PropertyInfo,
	&ReturnMessage_t2333____MethodSignature_PropertyInfo,
	&ReturnMessage_t2333____Properties_PropertyInfo,
	&ReturnMessage_t2333____TypeName_PropertyInfo,
	&ReturnMessage_t2333____Uri_PropertyInfo,
	&ReturnMessage_t2333____Exception_PropertyInfo,
	&ReturnMessage_t2333____OutArgs_PropertyInfo,
	&ReturnMessage_t2333____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ReturnMessage_t2333_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12256_MethodInfo,
	&ReturnMessage_get_Args_m12257_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m12258_MethodInfo,
	&ReturnMessage_get_MethodBase_m12259_MethodInfo,
	&ReturnMessage_get_MethodName_m12260_MethodInfo,
	&ReturnMessage_get_MethodSignature_m12261_MethodInfo,
	&ReturnMessage_get_TypeName_m12263_MethodInfo,
	&ReturnMessage_get_Uri_m12264_MethodInfo,
	&ReturnMessage_get_Exception_m12266_MethodInfo,
	&ReturnMessage_get_OutArgs_m12267_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12268_MethodInfo,
	&ReturnMessage_get_Properties_m12262_MethodInfo,
	&ReturnMessage_set_Uri_m12265_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12268_MethodInfo,
};
static bool ReturnMessage_t2333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ReturnMessage_t2333_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2608_0_0_0,
	&IMessage_t2315_0_0_0,
	&IMethodMessage_t2327_0_0_0,
	&IMethodReturnMessage_t2595_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t2333_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2608_0_0_0, 4},
	{ &IMessage_t2315_0_0_0, 5},
	{ &IMethodMessage_t2327_0_0_0, 5},
	{ &IMethodReturnMessage_t2595_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnMessage_t2333_0_0_0;
extern const Il2CppType ReturnMessage_t2333_1_0_0;
struct ReturnMessage_t2333;
const Il2CppTypeDefinitionMetadata ReturnMessage_t2333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t2333_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t2333_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t2333_VTable/* vtableMethods */
	, ReturnMessage_t2333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1587/* fieldStart */

};
TypeInfo ReturnMessage_t2333_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ReturnMessage_t2333_MethodInfos/* methods */
	, ReturnMessage_t2333_PropertyInfos/* properties */
	, NULL/* events */
	, &ReturnMessage_t2333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 546/* custom_attributes_cache */
	, &ReturnMessage_t2333_0_0_0/* byval_arg */
	, &ReturnMessage_t2333_1_0_0/* this_arg */
	, &ReturnMessage_t2333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t2333)/* instance_size */
	, sizeof (ReturnMessage_t2333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t2334_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ProxyAttribute_t2334_ProxyAttribute_CreateInstance_m12269_ParameterInfos[] = 
{
	{"serverType", 0, 134222060, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.ProxyAttribute::CreateInstance(System.Type)
extern const MethodInfo ProxyAttribute_CreateInstance_m12269_MethodInfo = 
{
	"CreateInstance"/* name */
	, (methodPointerType)&ProxyAttribute_CreateInstance_m12269/* method */
	, &ProxyAttribute_t2334_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1885_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2334_ProxyAttribute_CreateInstance_m12269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Context_t2306_0_0_0;
static const ParameterInfo ProxyAttribute_t2334_ProxyAttribute_CreateProxy_m12270_ParameterInfos[] = 
{
	{"objRef", 0, 134222061, 0, &ObjRef_t2343_0_0_0},
	{"serverType", 1, 134222062, 0, &Type_t_0_0_0},
	{"serverObject", 2, 134222063, 0, &Object_t_0_0_0},
	{"serverContext", 3, 134222064, 0, &Context_t2306_0_0_0},
};
extern const Il2CppType RealProxy_t2335_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.ProxyAttribute::CreateProxy(System.Runtime.Remoting.ObjRef,System.Type,System.Object,System.Runtime.Remoting.Contexts.Context)
extern const MethodInfo ProxyAttribute_CreateProxy_m12270_MethodInfo = 
{
	"CreateProxy"/* name */
	, (methodPointerType)&ProxyAttribute_CreateProxy_m12270/* method */
	, &ProxyAttribute_t2334_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2335_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2334_ProxyAttribute_CreateProxy_m12270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo ProxyAttribute_t2334_ProxyAttribute_GetPropertiesForNewContext_m12271_ParameterInfos[] = 
{
	{"msg", 0, 134222065, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.ProxyAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m12271_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ProxyAttribute_GetPropertiesForNewContext_m12271/* method */
	, &ProxyAttribute_t2334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ProxyAttribute_t2334_ProxyAttribute_GetPropertiesForNewContext_m12271_ParameterInfos/* parameters */
	, 548/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2592_0_0_0;
static const ParameterInfo ProxyAttribute_t2334_ProxyAttribute_IsContextOK_m12272_ParameterInfos[] = 
{
	{"ctx", 0, 134222066, 0, &Context_t2306_0_0_0},
	{"msg", 1, 134222067, 0, &IConstructionCallMessage_t2592_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Proxies.ProxyAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_IsContextOK_m12272_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ProxyAttribute_IsContextOK_m12272/* method */
	, &ProxyAttribute_t2334_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2334_ProxyAttribute_IsContextOK_m12272_ParameterInfos/* parameters */
	, 549/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ProxyAttribute_t2334_MethodInfos[] =
{
	&ProxyAttribute_CreateInstance_m12269_MethodInfo,
	&ProxyAttribute_CreateProxy_m12270_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m12271_MethodInfo,
	&ProxyAttribute_IsContextOK_m12272_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5260_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5261_MethodInfo;
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m12271_MethodInfo;
extern const MethodInfo ProxyAttribute_IsContextOK_m12272_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateInstance_m12269_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateProxy_m12270_MethodInfo;
static const Il2CppMethodReference ProxyAttribute_t2334_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m12271_MethodInfo,
	&ProxyAttribute_IsContextOK_m12272_MethodInfo,
	&ProxyAttribute_CreateInstance_m12269_MethodInfo,
	&ProxyAttribute_CreateProxy_m12270_MethodInfo,
};
static bool ProxyAttribute_t2334_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ProxyAttribute_t2334_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2605_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t2334_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
	{ &IContextAttribute_t2605_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProxyAttribute_t2334_0_0_0;
extern const Il2CppType ProxyAttribute_t2334_1_0_0;
struct ProxyAttribute_t2334;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t2334_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t2334_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t2334_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ProxyAttribute_t2334_VTable/* vtableMethods */
	, ProxyAttribute_t2334_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ProxyAttribute_t2334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, ProxyAttribute_t2334_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ProxyAttribute_t2334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 547/* custom_attributes_cache */
	, &ProxyAttribute_t2334_0_0_0/* byval_arg */
	, &ProxyAttribute_t2334_1_0_0/* this_arg */
	, &ProxyAttribute_t2334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t2334)/* instance_size */
	, sizeof (ProxyAttribute_t2334)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t2336_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static const MethodInfo* TransparentProxy_t2336_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TransparentProxy_t2336_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TransparentProxy_t2336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TransparentProxy_t2336_0_0_0;
extern const Il2CppType TransparentProxy_t2336_1_0_0;
struct TransparentProxy_t2336;
const Il2CppTypeDefinitionMetadata TransparentProxy_t2336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t2336_VTable/* vtableMethods */
	, TransparentProxy_t2336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1600/* fieldStart */

};
TypeInfo TransparentProxy_t2336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, TransparentProxy_t2336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TransparentProxy_t2336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t2336_0_0_0/* byval_arg */
	, &TransparentProxy_t2336_1_0_0/* this_arg */
	, &TransparentProxy_t2336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t2336)/* instance_size */
	, sizeof (TransparentProxy_t2336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t2335_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy__ctor_m12273_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222068, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type)
extern const MethodInfo RealProxy__ctor_m12273_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12273/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RealProxy_t2335_RealProxy__ctor_m12273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t2345_0_0_0;
extern const Il2CppType ClientIdentity_t2345_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy__ctor_m12274_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222069, 0, &Type_t_0_0_0},
	{"identity", 1, 134222070, 0, &ClientIdentity_t2345_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RealProxy__ctor_m12274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12274/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, RealProxy_t2335_RealProxy__ctor_m12274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy__ctor_m12275_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222071, 0, &Type_t_0_0_0},
	{"stub", 1, 134222072, 0, &IntPtr_t_0_0_0},
	{"stubData", 2, 134222073, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.IntPtr,System.Object)
extern const MethodInfo RealProxy__ctor_m12275_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12275/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t_Object_t/* invoker_method */
	, RealProxy_t2335_RealProxy__ctor_m12275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy_InternalGetProxyType_m12276_ParameterInfos[] = 
{
	{"transparentProxy", 0, 134222074, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)
extern const MethodInfo RealProxy_InternalGetProxyType_m12276_MethodInfo = 
{
	"InternalGetProxyType"/* name */
	, (methodPointerType)&RealProxy_InternalGetProxyType_m12276/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2335_RealProxy_InternalGetProxyType_m12276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::GetProxiedType()
extern const MethodInfo RealProxy_GetProxiedType_m12277_MethodInfo = 
{
	"GetProxiedType"/* name */
	, (methodPointerType)&RealProxy_GetProxiedType_m12277/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy_InternalGetTransparentProxy_m12278_ParameterInfos[] = 
{
	{"className", 0, 134222075, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m12278_MethodInfo = 
{
	"InternalGetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_InternalGetTransparentProxy_m12278/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2335_RealProxy_InternalGetTransparentProxy_m12278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 4096/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy()
extern const MethodInfo RealProxy_GetTransparentProxy_m12279_MethodInfo = 
{
	"GetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_GetTransparentProxy_m12279/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo RealProxy_t2335_RealProxy_SetTargetDomain_m12280_ParameterInfos[] = 
{
	{"domainId", 0, 134222076, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetTargetDomain(System.Int32)
extern const MethodInfo RealProxy_SetTargetDomain_m12280_MethodInfo = 
{
	"SetTargetDomain"/* name */
	, (methodPointerType)&RealProxy_SetTargetDomain_m12280/* method */
	, &RealProxy_t2335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, RealProxy_t2335_RealProxy_SetTargetDomain_m12280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RealProxy_t2335_MethodInfos[] =
{
	&RealProxy__ctor_m12273_MethodInfo,
	&RealProxy__ctor_m12274_MethodInfo,
	&RealProxy__ctor_m12275_MethodInfo,
	&RealProxy_InternalGetProxyType_m12276_MethodInfo,
	&RealProxy_GetProxiedType_m12277_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12278_MethodInfo,
	&RealProxy_GetTransparentProxy_m12279_MethodInfo,
	&RealProxy_SetTargetDomain_m12280_MethodInfo,
	NULL
};
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m12278_MethodInfo;
extern const MethodInfo RealProxy_GetTransparentProxy_m12279_MethodInfo;
static const Il2CppMethodReference RealProxy_t2335_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12278_MethodInfo,
	&RealProxy_GetTransparentProxy_m12279_MethodInfo,
};
static bool RealProxy_t2335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RealProxy_t2335_1_0_0;
struct RealProxy_t2335;
const Il2CppTypeDefinitionMetadata RealProxy_t2335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t2335_VTable/* vtableMethods */
	, RealProxy_t2335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1601/* fieldStart */

};
TypeInfo RealProxy_t2335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RealProxy_t2335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RealProxy_t2335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 550/* custom_attributes_cache */
	, &RealProxy_t2335_0_0_0/* byval_arg */
	, &RealProxy_t2335_1_0_0/* this_arg */
	, &RealProxy_t2335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t2335)/* instance_size */
	, sizeof (RealProxy_t2335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t2338_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t2345_0_0_0;
static const ParameterInfo RemotingProxy_t2338_RemotingProxy__ctor_m12281_ParameterInfos[] = 
{
	{"type", 0, 134222077, 0, &Type_t_0_0_0},
	{"identity", 1, 134222078, 0, &ClientIdentity_t2345_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RemotingProxy__ctor_m12281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m12281/* method */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2338_RemotingProxy__ctor_m12281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo RemotingProxy_t2338_RemotingProxy__ctor_m12282_ParameterInfos[] = 
{
	{"type", 0, 134222079, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134222080, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134222081, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
extern const MethodInfo RemotingProxy__ctor_m12282_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m12282/* method */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2338_RemotingProxy__ctor_m12282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
extern const MethodInfo RemotingProxy__cctor_m12283_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingProxy__cctor_m12283/* method */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
extern const MethodInfo RemotingProxy_get_TypeName_m12284_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&RemotingProxy_get_TypeName_m12284/* method */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
extern const MethodInfo RemotingProxy_Finalize_m12285_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&RemotingProxy_Finalize_m12285/* method */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingProxy_t2338_MethodInfos[] =
{
	&RemotingProxy__ctor_m12281_MethodInfo,
	&RemotingProxy__ctor_m12282_MethodInfo,
	&RemotingProxy__cctor_m12283_MethodInfo,
	&RemotingProxy_get_TypeName_m12284_MethodInfo,
	&RemotingProxy_Finalize_m12285_MethodInfo,
	NULL
};
extern const MethodInfo RemotingProxy_get_TypeName_m12284_MethodInfo;
static const PropertyInfo RemotingProxy_t2338____TypeName_PropertyInfo = 
{
	&RemotingProxy_t2338_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &RemotingProxy_get_TypeName_m12284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingProxy_t2338_PropertyInfos[] =
{
	&RemotingProxy_t2338____TypeName_PropertyInfo,
	NULL
};
extern const MethodInfo RemotingProxy_Finalize_m12285_MethodInfo;
static const Il2CppMethodReference RemotingProxy_t2338_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&RemotingProxy_Finalize_m12285_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12278_MethodInfo,
	&RealProxy_GetTransparentProxy_m12279_MethodInfo,
	&RemotingProxy_get_TypeName_m12284_MethodInfo,
};
static bool RemotingProxy_t2338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IRemotingTypeInfo_t2347_0_0_0;
static const Il2CppType* RemotingProxy_t2338_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2347_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t2338_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2347_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingProxy_t2338_0_0_0;
extern const Il2CppType RemotingProxy_t2338_1_0_0;
struct RemotingProxy_t2338;
const Il2CppTypeDefinitionMetadata RemotingProxy_t2338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t2338_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t2338_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t2335_0_0_0/* parent */
	, RemotingProxy_t2338_VTable/* vtableMethods */
	, RemotingProxy_t2338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1606/* fieldStart */

};
TypeInfo RemotingProxy_t2338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RemotingProxy_t2338_MethodInfos/* methods */
	, RemotingProxy_t2338_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingProxy_t2338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t2338_0_0_0/* byval_arg */
	, &RemotingProxy_t2338_1_0_0/* this_arg */
	, &RemotingProxy_t2338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t2338)/* instance_size */
	, sizeof (RemotingProxy_t2338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t2338_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t2610_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2343_0_0_0;
static const ParameterInfo ITrackingHandler_t2610_ITrackingHandler_UnmarshaledObject_m14573_ParameterInfos[] = 
{
	{"obj", 0, 134222082, 0, &Object_t_0_0_0},
	{"or", 1, 134222083, 0, &ObjRef_t2343_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::UnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ITrackingHandler_UnmarshaledObject_m14573_MethodInfo = 
{
	"UnmarshaledObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ITrackingHandler_t2610_ITrackingHandler_UnmarshaledObject_m14573_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ITrackingHandler_t2610_MethodInfos[] =
{
	&ITrackingHandler_UnmarshaledObject_m14573_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ITrackingHandler_t2610_0_0_0;
extern const Il2CppType ITrackingHandler_t2610_1_0_0;
struct ITrackingHandler_t2610;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t2610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ITrackingHandler_t2610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, ITrackingHandler_t2610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ITrackingHandler_t2610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 551/* custom_attributes_cache */
	, &ITrackingHandler_t2610_0_0_0/* byval_arg */
	, &ITrackingHandler_t2610_1_0_0/* this_arg */
	, &ITrackingHandler_t2610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t2339_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
extern const MethodInfo TrackingServices__cctor_m12286_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TrackingServices__cctor_m12286/* method */
	, &TrackingServices_t2339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2343_0_0_0;
static const ParameterInfo TrackingServices_t2339_TrackingServices_NotifyUnmarshaledObject_m12287_ParameterInfos[] = 
{
	{"obj", 0, 134222084, 0, &Object_t_0_0_0},
	{"or", 1, 134222085, 0, &ObjRef_t2343_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo TrackingServices_NotifyUnmarshaledObject_m12287_MethodInfo = 
{
	"NotifyUnmarshaledObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyUnmarshaledObject_m12287/* method */
	, &TrackingServices_t2339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TrackingServices_t2339_TrackingServices_NotifyUnmarshaledObject_m12287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackingServices_t2339_MethodInfos[] =
{
	&TrackingServices__cctor_m12286_MethodInfo,
	&TrackingServices_NotifyUnmarshaledObject_m12287_MethodInfo,
	NULL
};
static const Il2CppMethodReference TrackingServices_t2339_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TrackingServices_t2339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TrackingServices_t2339_0_0_0;
extern const Il2CppType TrackingServices_t2339_1_0_0;
struct TrackingServices_t2339;
const Il2CppTypeDefinitionMetadata TrackingServices_t2339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t2339_VTable/* vtableMethods */
	, TrackingServices_t2339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1611/* fieldStart */

};
TypeInfo TrackingServices_t2339_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, TrackingServices_t2339_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackingServices_t2339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 552/* custom_attributes_cache */
	, &TrackingServices_t2339_0_0_0/* byval_arg */
	, &TrackingServices_t2339_1_0_0/* this_arg */
	, &TrackingServices_t2339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t2339)/* instance_size */
	, sizeof (TrackingServices_t2339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t2339_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::get_ApplicationUrl()
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m12288_MethodInfo = 
{
	"get_ApplicationUrl"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ApplicationUrl_m12288/* method */
	, &ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IContextAttributeU5BU5D_t2597_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedClientTypeEntry::get_ContextAttributes()
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m12289_MethodInfo = 
{
	"get_ContextAttributes"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ContextAttributes_m12289/* method */
	, &ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttributeU5BU5D_t2597_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ActivatedClientTypeEntry::get_ObjectType()
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m12290_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ObjectType_m12290/* method */
	, &ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::ToString()
extern const MethodInfo ActivatedClientTypeEntry_ToString_m12291_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_ToString_m12291/* method */
	, &ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivatedClientTypeEntry_t2340_MethodInfos[] =
{
	&ActivatedClientTypeEntry_get_ApplicationUrl_m12288_MethodInfo,
	&ActivatedClientTypeEntry_get_ContextAttributes_m12289_MethodInfo,
	&ActivatedClientTypeEntry_get_ObjectType_m12290_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m12291_MethodInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m12288_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2340____ApplicationUrl_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* parent */
	, "ApplicationUrl"/* name */
	, &ActivatedClientTypeEntry_get_ApplicationUrl_m12288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m12289_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2340____ContextAttributes_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* parent */
	, "ContextAttributes"/* name */
	, &ActivatedClientTypeEntry_get_ContextAttributes_m12289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m12290_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2340____ObjectType_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ActivatedClientTypeEntry_get_ObjectType_m12290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivatedClientTypeEntry_t2340_PropertyInfos[] =
{
	&ActivatedClientTypeEntry_t2340____ApplicationUrl_PropertyInfo,
	&ActivatedClientTypeEntry_t2340____ContextAttributes_PropertyInfo,
	&ActivatedClientTypeEntry_t2340____ObjectType_PropertyInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_ToString_m12291_MethodInfo;
static const Il2CppMethodReference ActivatedClientTypeEntry_t2340_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m12291_MethodInfo,
};
static bool ActivatedClientTypeEntry_t2340_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedClientTypeEntry_t2340_0_0_0;
extern const Il2CppType ActivatedClientTypeEntry_t2340_1_0_0;
extern const Il2CppType TypeEntry_t2341_0_0_0;
struct ActivatedClientTypeEntry_t2340;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t2340_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t2341_0_0_0/* parent */
	, ActivatedClientTypeEntry_t2340_VTable/* vtableMethods */
	, ActivatedClientTypeEntry_t2340_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1612/* fieldStart */

};
TypeInfo ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ActivatedClientTypeEntry_t2340_MethodInfos/* methods */
	, ActivatedClientTypeEntry_t2340_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivatedClientTypeEntry_t2340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 553/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t2340_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t2340_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t2340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t2340)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t2340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t2342_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
extern const Il2CppType IMessageSink_t1130_0_0_0;
static const ParameterInfo EnvoyInfo_t2342_EnvoyInfo__ctor_m12292_ParameterInfos[] = 
{
	{"sinks", 0, 134222086, 0, &IMessageSink_t1130_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.EnvoyInfo::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo EnvoyInfo__ctor_m12292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyInfo__ctor_m12292/* method */
	, &EnvoyInfo_t2342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, EnvoyInfo_t2342_EnvoyInfo__ctor_m12292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.EnvoyInfo::get_EnvoySinks()
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m12293_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, (methodPointerType)&EnvoyInfo_get_EnvoySinks_m12293/* method */
	, &EnvoyInfo_t2342_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1130_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyInfo_t2342_MethodInfos[] =
{
	&EnvoyInfo__ctor_m12292_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m12293_MethodInfo,
	NULL
};
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m12293_MethodInfo;
static const PropertyInfo EnvoyInfo_t2342____EnvoySinks_PropertyInfo = 
{
	&EnvoyInfo_t2342_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &EnvoyInfo_get_EnvoySinks_m12293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* EnvoyInfo_t2342_PropertyInfos[] =
{
	&EnvoyInfo_t2342____EnvoySinks_PropertyInfo,
	NULL
};
static const Il2CppMethodReference EnvoyInfo_t2342_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m12293_MethodInfo,
};
static bool EnvoyInfo_t2342_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnvoyInfo_t2348_0_0_0;
static const Il2CppType* EnvoyInfo_t2342_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t2348_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t2342_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t2348_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyInfo_t2342_0_0_0;
extern const Il2CppType EnvoyInfo_t2342_1_0_0;
struct EnvoyInfo_t2342;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t2342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t2342_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t2342_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t2342_VTable/* vtableMethods */
	, EnvoyInfo_t2342_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1614/* fieldStart */

};
TypeInfo EnvoyInfo_t2342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, EnvoyInfo_t2342_MethodInfos/* methods */
	, EnvoyInfo_t2342_PropertyInfos/* properties */
	, NULL/* events */
	, &EnvoyInfo_t2342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t2342_0_0_0/* byval_arg */
	, &EnvoyInfo_t2342_1_0_0/* this_arg */
	, &EnvoyInfo_t2342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t2342)/* instance_size */
	, sizeof (EnvoyInfo_t2342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t2346_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.IChannelInfo::get_ChannelData()
extern const MethodInfo IChannelInfo_get_ChannelData_m14574_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelInfo_t2346_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelInfo_t2346_MethodInfos[] =
{
	&IChannelInfo_get_ChannelData_m14574_MethodInfo,
	NULL
};
extern const MethodInfo IChannelInfo_get_ChannelData_m14574_MethodInfo;
static const PropertyInfo IChannelInfo_t2346____ChannelData_PropertyInfo = 
{
	&IChannelInfo_t2346_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelInfo_get_ChannelData_m14574_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelInfo_t2346_PropertyInfos[] =
{
	&IChannelInfo_t2346____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelInfo_t2346_1_0_0;
struct IChannelInfo_t2346;
const Il2CppTypeDefinitionMetadata IChannelInfo_t2346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelInfo_t2346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IChannelInfo_t2346_MethodInfos/* methods */
	, IChannelInfo_t2346_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelInfo_t2346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 554/* custom_attributes_cache */
	, &IChannelInfo_t2346_0_0_0/* byval_arg */
	, &IChannelInfo_t2346_1_0_0/* this_arg */
	, &IChannelInfo_t2346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t2348_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.IEnvoyInfo::get_EnvoySinks()
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m14575_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, NULL/* method */
	, &IEnvoyInfo_t2348_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1130_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IEnvoyInfo_t2348_MethodInfos[] =
{
	&IEnvoyInfo_get_EnvoySinks_m14575_MethodInfo,
	NULL
};
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m14575_MethodInfo;
static const PropertyInfo IEnvoyInfo_t2348____EnvoySinks_PropertyInfo = 
{
	&IEnvoyInfo_t2348_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &IEnvoyInfo_get_EnvoySinks_m14575_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IEnvoyInfo_t2348_PropertyInfos[] =
{
	&IEnvoyInfo_t2348____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnvoyInfo_t2348_1_0_0;
struct IEnvoyInfo_t2348;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t2348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IEnvoyInfo_t2348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IEnvoyInfo_t2348_MethodInfos/* methods */
	, IEnvoyInfo_t2348_PropertyInfos/* properties */
	, NULL/* events */
	, &IEnvoyInfo_t2348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 555/* custom_attributes_cache */
	, &IEnvoyInfo_t2348_0_0_0/* byval_arg */
	, &IEnvoyInfo_t2348_1_0_0/* this_arg */
	, &IEnvoyInfo_t2348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t2347_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.IRemotingTypeInfo::get_TypeName()
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m14576_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IRemotingTypeInfo_t2347_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IRemotingTypeInfo_t2347_MethodInfos[] =
{
	&IRemotingTypeInfo_get_TypeName_m14576_MethodInfo,
	NULL
};
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m14576_MethodInfo;
static const PropertyInfo IRemotingTypeInfo_t2347____TypeName_PropertyInfo = 
{
	&IRemotingTypeInfo_t2347_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IRemotingTypeInfo_get_TypeName_m14576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IRemotingTypeInfo_t2347_PropertyInfos[] =
{
	&IRemotingTypeInfo_t2347____TypeName_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingTypeInfo_t2347_1_0_0;
struct IRemotingTypeInfo_t2347;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t2347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingTypeInfo_t2347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IRemotingTypeInfo_t2347_MethodInfos/* methods */
	, IRemotingTypeInfo_t2347_PropertyInfos/* properties */
	, NULL/* events */
	, &IRemotingTypeInfo_t2347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 556/* custom_attributes_cache */
	, &IRemotingTypeInfo_t2347_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t2347_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t2347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t2337_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Identity_t2337_Identity__ctor_m12294_ParameterInfos[] = 
{
	{"objectUri", 0, 134222087, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::.ctor(System.String)
extern const MethodInfo Identity__ctor_m12294_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Identity__ctor_m12294/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Identity_t2337_Identity__ctor_m12294_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Identity_t2337_Identity_CreateObjRef_m14577_ParameterInfos[] = 
{
	{"requestedType", 0, 134222088, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Identity::CreateObjRef(System.Type)
extern const MethodInfo Identity_CreateObjRef_m14577_MethodInfo = 
{
	"CreateObjRef"/* name */
	, NULL/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Identity_t2337_Identity_CreateObjRef_m14577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Identity::get_ChannelSink()
extern const MethodInfo Identity_get_ChannelSink_m12295_MethodInfo = 
{
	"get_ChannelSink"/* name */
	, (methodPointerType)&Identity_get_ChannelSink_m12295/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1130_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageSink_t1130_0_0_0;
static const ParameterInfo Identity_t2337_Identity_set_ChannelSink_m12296_ParameterInfos[] = 
{
	{"value", 0, 134222089, 0, &IMessageSink_t1130_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_ChannelSink(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo Identity_set_ChannelSink_m12296_MethodInfo = 
{
	"set_ChannelSink"/* name */
	, (methodPointerType)&Identity_set_ChannelSink_m12296/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Identity_t2337_Identity_set_ChannelSink_m12296_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Identity::get_ObjectUri()
extern const MethodInfo Identity_get_ObjectUri_m12297_MethodInfo = 
{
	"get_ObjectUri"/* name */
	, (methodPointerType)&Identity_get_ObjectUri_m12297/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Identity::get_Disposed()
extern const MethodInfo Identity_get_Disposed_m12298_MethodInfo = 
{
	"get_Disposed"/* name */
	, (methodPointerType)&Identity_get_Disposed_m12298/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Identity_t2337_Identity_set_Disposed_m12299_ParameterInfos[] = 
{
	{"value", 0, 134222090, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_Disposed(System.Boolean)
extern const MethodInfo Identity_set_Disposed_m12299_MethodInfo = 
{
	"set_Disposed"/* name */
	, (methodPointerType)&Identity_set_Disposed_m12299/* method */
	, &Identity_t2337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Identity_t2337_Identity_set_Disposed_m12299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Identity_t2337_MethodInfos[] =
{
	&Identity__ctor_m12294_MethodInfo,
	&Identity_CreateObjRef_m14577_MethodInfo,
	&Identity_get_ChannelSink_m12295_MethodInfo,
	&Identity_set_ChannelSink_m12296_MethodInfo,
	&Identity_get_ObjectUri_m12297_MethodInfo,
	&Identity_get_Disposed_m12298_MethodInfo,
	&Identity_set_Disposed_m12299_MethodInfo,
	NULL
};
extern const MethodInfo Identity_get_ChannelSink_m12295_MethodInfo;
extern const MethodInfo Identity_set_ChannelSink_m12296_MethodInfo;
static const PropertyInfo Identity_t2337____ChannelSink_PropertyInfo = 
{
	&Identity_t2337_il2cpp_TypeInfo/* parent */
	, "ChannelSink"/* name */
	, &Identity_get_ChannelSink_m12295_MethodInfo/* get */
	, &Identity_set_ChannelSink_m12296_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_ObjectUri_m12297_MethodInfo;
static const PropertyInfo Identity_t2337____ObjectUri_PropertyInfo = 
{
	&Identity_t2337_il2cpp_TypeInfo/* parent */
	, "ObjectUri"/* name */
	, &Identity_get_ObjectUri_m12297_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_Disposed_m12298_MethodInfo;
extern const MethodInfo Identity_set_Disposed_m12299_MethodInfo;
static const PropertyInfo Identity_t2337____Disposed_PropertyInfo = 
{
	&Identity_t2337_il2cpp_TypeInfo/* parent */
	, "Disposed"/* name */
	, &Identity_get_Disposed_m12298_MethodInfo/* get */
	, &Identity_set_Disposed_m12299_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Identity_t2337_PropertyInfos[] =
{
	&Identity_t2337____ChannelSink_PropertyInfo,
	&Identity_t2337____ObjectUri_PropertyInfo,
	&Identity_t2337____Disposed_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Identity_t2337_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
};
static bool Identity_t2337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Identity_t2337_0_0_0;
extern const Il2CppType Identity_t2337_1_0_0;
struct Identity_t2337;
const Il2CppTypeDefinitionMetadata Identity_t2337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t2337_VTable/* vtableMethods */
	, Identity_t2337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1615/* fieldStart */

};
TypeInfo Identity_t2337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, Identity_t2337_MethodInfos/* methods */
	, Identity_t2337_PropertyInfos/* properties */
	, NULL/* events */
	, &Identity_t2337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t2337_0_0_0/* byval_arg */
	, &Identity_t2337_1_0_0/* this_arg */
	, &Identity_t2337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t2337)/* instance_size */
	, sizeof (Identity_t2337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t2345_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjRef_t2343_0_0_0;
static const ParameterInfo ClientIdentity_t2345_ClientIdentity__ctor_m12300_ParameterInfos[] = 
{
	{"objectUri", 0, 134222091, 0, &String_t_0_0_0},
	{"objRef", 1, 134222092, 0, &ObjRef_t2343_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::.ctor(System.String,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ClientIdentity__ctor_m12300_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClientIdentity__ctor_m12300/* method */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2345_ClientIdentity__ctor_m12300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientIdentity::get_ClientProxy()
extern const MethodInfo ClientIdentity_get_ClientProxy_m12301_MethodInfo = 
{
	"get_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_get_ClientProxy_m12301/* method */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1885_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1885_0_0_0;
static const ParameterInfo ClientIdentity_t2345_ClientIdentity_set_ClientProxy_m12302_ParameterInfos[] = 
{
	{"value", 0, 134222093, 0, &MarshalByRefObject_t1885_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::set_ClientProxy(System.MarshalByRefObject)
extern const MethodInfo ClientIdentity_set_ClientProxy_m12302_MethodInfo = 
{
	"set_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_set_ClientProxy_m12302/* method */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ClientIdentity_t2345_ClientIdentity_set_ClientProxy_m12302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ClientIdentity_t2345_ClientIdentity_CreateObjRef_m12303_ParameterInfos[] = 
{
	{"requestedType", 0, 134222094, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ClientIdentity::CreateObjRef(System.Type)
extern const MethodInfo ClientIdentity_CreateObjRef_m12303_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ClientIdentity_CreateObjRef_m12303/* method */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2345_ClientIdentity_CreateObjRef_m12303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ClientIdentity::get_TargetUri()
extern const MethodInfo ClientIdentity_get_TargetUri_m12304_MethodInfo = 
{
	"get_TargetUri"/* name */
	, (methodPointerType)&ClientIdentity_get_TargetUri_m12304/* method */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientIdentity_t2345_MethodInfos[] =
{
	&ClientIdentity__ctor_m12300_MethodInfo,
	&ClientIdentity_get_ClientProxy_m12301_MethodInfo,
	&ClientIdentity_set_ClientProxy_m12302_MethodInfo,
	&ClientIdentity_CreateObjRef_m12303_MethodInfo,
	&ClientIdentity_get_TargetUri_m12304_MethodInfo,
	NULL
};
extern const MethodInfo ClientIdentity_get_ClientProxy_m12301_MethodInfo;
extern const MethodInfo ClientIdentity_set_ClientProxy_m12302_MethodInfo;
static const PropertyInfo ClientIdentity_t2345____ClientProxy_PropertyInfo = 
{
	&ClientIdentity_t2345_il2cpp_TypeInfo/* parent */
	, "ClientProxy"/* name */
	, &ClientIdentity_get_ClientProxy_m12301_MethodInfo/* get */
	, &ClientIdentity_set_ClientProxy_m12302_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ClientIdentity_get_TargetUri_m12304_MethodInfo;
static const PropertyInfo ClientIdentity_t2345____TargetUri_PropertyInfo = 
{
	&ClientIdentity_t2345_il2cpp_TypeInfo/* parent */
	, "TargetUri"/* name */
	, &ClientIdentity_get_TargetUri_m12304_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ClientIdentity_t2345_PropertyInfos[] =
{
	&ClientIdentity_t2345____ClientProxy_PropertyInfo,
	&ClientIdentity_t2345____TargetUri_PropertyInfo,
	NULL
};
extern const MethodInfo ClientIdentity_CreateObjRef_m12303_MethodInfo;
static const Il2CppMethodReference ClientIdentity_t2345_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ClientIdentity_CreateObjRef_m12303_MethodInfo,
};
static bool ClientIdentity_t2345_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientIdentity_t2345_1_0_0;
struct ClientIdentity_t2345;
const Il2CppTypeDefinitionMetadata ClientIdentity_t2345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2337_0_0_0/* parent */
	, ClientIdentity_t2345_VTable/* vtableMethods */
	, ClientIdentity_t2345_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1620/* fieldStart */

};
TypeInfo ClientIdentity_t2345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientIdentity_t2345_MethodInfos/* methods */
	, ClientIdentity_t2345_PropertyInfos/* properties */
	, NULL/* events */
	, &ClientIdentity_t2345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t2345_0_0_0/* byval_arg */
	, &ClientIdentity_t2345_1_0_0/* this_arg */
	, &ClientIdentity_t2345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t2345)/* instance_size */
	, sizeof (ClientIdentity_t2345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t2343_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor()
extern const MethodInfo ObjRef__ctor_m12305_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m12305/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef__ctor_m12306_ParameterInfos[] = 
{
	{"info", 0, 134222095, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222096, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef__ctor_m12306_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m12306/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ObjRef_t2343_ObjRef__ctor_m12306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.cctor()
extern const MethodInfo ObjRef__cctor_m12307_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjRef__cctor_m12307/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.ObjRef::get_IsReferenceToWellKnow()
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m12308_MethodInfo = 
{
	"get_IsReferenceToWellKnow"/* name */
	, (methodPointerType)&ObjRef_get_IsReferenceToWellKnow_m12308/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IChannelInfo System.Runtime.Remoting.ObjRef::get_ChannelInfo()
extern const MethodInfo ObjRef_get_ChannelInfo_m12309_MethodInfo = 
{
	"get_ChannelInfo"/* name */
	, (methodPointerType)&ObjRef_get_ChannelInfo_m12309/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &IChannelInfo_t2346_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 559/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IEnvoyInfo System.Runtime.Remoting.ObjRef::get_EnvoyInfo()
extern const MethodInfo ObjRef_get_EnvoyInfo_m12310_MethodInfo = 
{
	"get_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_get_EnvoyInfo_m12310/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &IEnvoyInfo_t2348_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnvoyInfo_t2348_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef_set_EnvoyInfo_m12311_ParameterInfos[] = 
{
	{"value", 0, 134222097, 0, &IEnvoyInfo_t2348_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_EnvoyInfo(System.Runtime.Remoting.IEnvoyInfo)
extern const MethodInfo ObjRef_set_EnvoyInfo_m12311_MethodInfo = 
{
	"set_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_set_EnvoyInfo_m12311/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjRef_t2343_ObjRef_set_EnvoyInfo_m12311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IRemotingTypeInfo System.Runtime.Remoting.ObjRef::get_TypeInfo()
extern const MethodInfo ObjRef_get_TypeInfo_m12312_MethodInfo = 
{
	"get_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_get_TypeInfo_m12312/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &IRemotingTypeInfo_t2347_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IRemotingTypeInfo_t2347_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef_set_TypeInfo_m12313_ParameterInfos[] = 
{
	{"value", 0, 134222098, 0, &IRemotingTypeInfo_t2347_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_TypeInfo(System.Runtime.Remoting.IRemotingTypeInfo)
extern const MethodInfo ObjRef_set_TypeInfo_m12313_MethodInfo = 
{
	"set_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_set_TypeInfo_m12313/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjRef_t2343_ObjRef_set_TypeInfo_m12313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ObjRef::get_URI()
extern const MethodInfo ObjRef_get_URI_m12314_MethodInfo = 
{
	"get_URI"/* name */
	, (methodPointerType)&ObjRef_get_URI_m12314/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef_set_URI_m12315_ParameterInfos[] = 
{
	{"value", 0, 134222099, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_URI(System.String)
extern const MethodInfo ObjRef_set_URI_m12315_MethodInfo = 
{
	"set_URI"/* name */
	, (methodPointerType)&ObjRef_set_URI_m12315/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjRef_t2343_ObjRef_set_URI_m12315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef_GetObjectData_m12316_ParameterInfos[] = 
{
	{"info", 0, 134222100, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222101, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetObjectData_m12316_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjRef_GetObjectData_m12316/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ObjRef_t2343_ObjRef_GetObjectData_m12316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ObjRef_t2343_ObjRef_GetRealObject_m12317_ParameterInfos[] = 
{
	{"context", 0, 134222102, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.ObjRef::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetRealObject_m12317_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&ObjRef_GetRealObject_m12317/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1383/* invoker_method */
	, ObjRef_t2343_ObjRef_GetRealObject_m12317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::UpdateChannelInfo()
extern const MethodInfo ObjRef_UpdateChannelInfo_m12318_MethodInfo = 
{
	"UpdateChannelInfo"/* name */
	, (methodPointerType)&ObjRef_UpdateChannelInfo_m12318/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ObjRef::get_ServerType()
extern const MethodInfo ObjRef_get_ServerType_m12319_MethodInfo = 
{
	"get_ServerType"/* name */
	, (methodPointerType)&ObjRef_get_ServerType_m12319/* method */
	, &ObjRef_t2343_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRef_t2343_MethodInfos[] =
{
	&ObjRef__ctor_m12305_MethodInfo,
	&ObjRef__ctor_m12306_MethodInfo,
	&ObjRef__cctor_m12307_MethodInfo,
	&ObjRef_get_IsReferenceToWellKnow_m12308_MethodInfo,
	&ObjRef_get_ChannelInfo_m12309_MethodInfo,
	&ObjRef_get_EnvoyInfo_m12310_MethodInfo,
	&ObjRef_set_EnvoyInfo_m12311_MethodInfo,
	&ObjRef_get_TypeInfo_m12312_MethodInfo,
	&ObjRef_set_TypeInfo_m12313_MethodInfo,
	&ObjRef_get_URI_m12314_MethodInfo,
	&ObjRef_set_URI_m12315_MethodInfo,
	&ObjRef_GetObjectData_m12316_MethodInfo,
	&ObjRef_GetRealObject_m12317_MethodInfo,
	&ObjRef_UpdateChannelInfo_m12318_MethodInfo,
	&ObjRef_get_ServerType_m12319_MethodInfo,
	NULL
};
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m12308_MethodInfo;
static const PropertyInfo ObjRef_t2343____IsReferenceToWellKnow_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "IsReferenceToWellKnow"/* name */
	, &ObjRef_get_IsReferenceToWellKnow_m12308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ChannelInfo_m12309_MethodInfo;
static const PropertyInfo ObjRef_t2343____ChannelInfo_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "ChannelInfo"/* name */
	, &ObjRef_get_ChannelInfo_m12309_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_EnvoyInfo_m12310_MethodInfo;
extern const MethodInfo ObjRef_set_EnvoyInfo_m12311_MethodInfo;
static const PropertyInfo ObjRef_t2343____EnvoyInfo_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "EnvoyInfo"/* name */
	, &ObjRef_get_EnvoyInfo_m12310_MethodInfo/* get */
	, &ObjRef_set_EnvoyInfo_m12311_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_TypeInfo_m12312_MethodInfo;
extern const MethodInfo ObjRef_set_TypeInfo_m12313_MethodInfo;
static const PropertyInfo ObjRef_t2343____TypeInfo_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "TypeInfo"/* name */
	, &ObjRef_get_TypeInfo_m12312_MethodInfo/* get */
	, &ObjRef_set_TypeInfo_m12313_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_URI_m12314_MethodInfo;
extern const MethodInfo ObjRef_set_URI_m12315_MethodInfo;
static const PropertyInfo ObjRef_t2343____URI_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "URI"/* name */
	, &ObjRef_get_URI_m12314_MethodInfo/* get */
	, &ObjRef_set_URI_m12315_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ServerType_m12319_MethodInfo;
static const PropertyInfo ObjRef_t2343____ServerType_PropertyInfo = 
{
	&ObjRef_t2343_il2cpp_TypeInfo/* parent */
	, "ServerType"/* name */
	, &ObjRef_get_ServerType_m12319_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjRef_t2343_PropertyInfos[] =
{
	&ObjRef_t2343____IsReferenceToWellKnow_PropertyInfo,
	&ObjRef_t2343____ChannelInfo_PropertyInfo,
	&ObjRef_t2343____EnvoyInfo_PropertyInfo,
	&ObjRef_t2343____TypeInfo_PropertyInfo,
	&ObjRef_t2343____URI_PropertyInfo,
	&ObjRef_t2343____ServerType_PropertyInfo,
	NULL
};
extern const MethodInfo ObjRef_GetObjectData_m12316_MethodInfo;
extern const MethodInfo ObjRef_GetRealObject_m12317_MethodInfo;
static const Il2CppMethodReference ObjRef_t2343_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ObjRef_GetObjectData_m12316_MethodInfo,
	&ObjRef_GetRealObject_m12317_MethodInfo,
	&ObjRef_get_ChannelInfo_m12309_MethodInfo,
	&ObjRef_get_EnvoyInfo_m12310_MethodInfo,
	&ObjRef_set_EnvoyInfo_m12311_MethodInfo,
	&ObjRef_get_TypeInfo_m12312_MethodInfo,
	&ObjRef_set_TypeInfo_m12313_MethodInfo,
	&ObjRef_get_URI_m12314_MethodInfo,
	&ObjRef_set_URI_m12315_MethodInfo,
	&ObjRef_GetObjectData_m12316_MethodInfo,
	&ObjRef_GetRealObject_m12317_MethodInfo,
};
static bool ObjRef_t2343_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t2615_0_0_0;
static const Il2CppType* ObjRef_t2343_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
	&IObjectReference_t2615_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t2343_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &IObjectReference_t2615_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRef_t2343_1_0_0;
struct ObjRef_t2343;
const Il2CppTypeDefinitionMetadata ObjRef_t2343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t2343_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t2343_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t2343_VTable/* vtableMethods */
	, ObjRef_t2343_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1621/* fieldStart */

};
TypeInfo ObjRef_t2343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ObjRef_t2343_MethodInfos/* methods */
	, ObjRef_t2343_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjRef_t2343_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 557/* custom_attributes_cache */
	, &ObjRef_t2343_0_0_0/* byval_arg */
	, &ObjRef_t2343_1_0_0/* this_arg */
	, &ObjRef_t2343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t2343)/* instance_size */
	, sizeof (ObjRef_t2343)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t2343_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t2349_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingConfiguration::.cctor()
extern const MethodInfo RemotingConfiguration__cctor_m12320_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingConfiguration__cctor_m12320/* method */
	, &RemotingConfiguration_t2349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationName()
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m12321_MethodInfo = 
{
	"get_ApplicationName"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ApplicationName_m12321/* method */
	, &RemotingConfiguration_t2349_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ProcessId()
extern const MethodInfo RemotingConfiguration_get_ProcessId_m12322_MethodInfo = 
{
	"get_ProcessId"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ProcessId_m12322/* method */
	, &RemotingConfiguration_t2349_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingConfiguration_t2349_RemotingConfiguration_IsRemotelyActivatedClientType_m12323_ParameterInfos[] = 
{
	{"svrType", 0, 134222103, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.Type)
extern const MethodInfo RemotingConfiguration_IsRemotelyActivatedClientType_m12323_MethodInfo = 
{
	"IsRemotelyActivatedClientType"/* name */
	, (methodPointerType)&RemotingConfiguration_IsRemotelyActivatedClientType_m12323/* method */
	, &RemotingConfiguration_t2349_il2cpp_TypeInfo/* declaring_type */
	, &ActivatedClientTypeEntry_t2340_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingConfiguration_t2349_RemotingConfiguration_IsRemotelyActivatedClientType_m12323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingConfiguration_t2349_MethodInfos[] =
{
	&RemotingConfiguration__cctor_m12320_MethodInfo,
	&RemotingConfiguration_get_ApplicationName_m12321_MethodInfo,
	&RemotingConfiguration_get_ProcessId_m12322_MethodInfo,
	&RemotingConfiguration_IsRemotelyActivatedClientType_m12323_MethodInfo,
	NULL
};
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m12321_MethodInfo;
static const PropertyInfo RemotingConfiguration_t2349____ApplicationName_PropertyInfo = 
{
	&RemotingConfiguration_t2349_il2cpp_TypeInfo/* parent */
	, "ApplicationName"/* name */
	, &RemotingConfiguration_get_ApplicationName_m12321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RemotingConfiguration_get_ProcessId_m12322_MethodInfo;
static const PropertyInfo RemotingConfiguration_t2349____ProcessId_PropertyInfo = 
{
	&RemotingConfiguration_t2349_il2cpp_TypeInfo/* parent */
	, "ProcessId"/* name */
	, &RemotingConfiguration_get_ProcessId_m12322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingConfiguration_t2349_PropertyInfos[] =
{
	&RemotingConfiguration_t2349____ApplicationName_PropertyInfo,
	&RemotingConfiguration_t2349____ProcessId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RemotingConfiguration_t2349_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RemotingConfiguration_t2349_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingConfiguration_t2349_0_0_0;
extern const Il2CppType RemotingConfiguration_t2349_1_0_0;
struct RemotingConfiguration_t2349;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t2349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t2349_VTable/* vtableMethods */
	, RemotingConfiguration_t2349_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1630/* fieldStart */

};
TypeInfo RemotingConfiguration_t2349_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingConfiguration_t2349_MethodInfos/* methods */
	, RemotingConfiguration_t2349_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingConfiguration_t2349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 560/* custom_attributes_cache */
	, &RemotingConfiguration_t2349_0_0_0/* byval_arg */
	, &RemotingConfiguration_t2349_1_0_0/* this_arg */
	, &RemotingConfiguration_t2349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t2349)/* instance_size */
	, sizeof (RemotingConfiguration_t2349)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t2349_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t2350_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor()
extern const MethodInfo RemotingException__ctor_m12324_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12324/* method */
	, &RemotingException_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingException_t2350_RemotingException__ctor_m12325_ParameterInfos[] = 
{
	{"message", 0, 134222104, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.String)
extern const MethodInfo RemotingException__ctor_m12325_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12325/* method */
	, &RemotingException_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RemotingException_t2350_RemotingException__ctor_m12325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo RemotingException_t2350_RemotingException__ctor_m12326_ParameterInfos[] = 
{
	{"info", 0, 134222105, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134222106, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RemotingException__ctor_m12326_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12326/* method */
	, &RemotingException_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, RemotingException_t2350_RemotingException__ctor_m12326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingException_t2350_MethodInfos[] =
{
	&RemotingException__ctor_m12324_MethodInfo,
	&RemotingException__ctor_m12325_MethodInfo,
	&RemotingException__ctor_m12326_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m7170_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7171_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7172_MethodInfo;
extern const MethodInfo Exception_get_Message_m7173_MethodInfo;
extern const MethodInfo Exception_get_Source_m7174_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7175_MethodInfo;
extern const MethodInfo Exception_GetType_m7176_MethodInfo;
static const Il2CppMethodReference RemotingException_t2350_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool RemotingException_t2350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t1473_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t2350_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingException_t2350_0_0_0;
extern const Il2CppType RemotingException_t2350_1_0_0;
extern const Il2CppType SystemException_t2004_0_0_0;
struct RemotingException_t2350;
const Il2CppTypeDefinitionMetadata RemotingException_t2350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t2350_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, RemotingException_t2350_VTable/* vtableMethods */
	, RemotingException_t2350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingException_t2350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingException_t2350_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingException_t2350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 561/* custom_attributes_cache */
	, &RemotingException_t2350_0_0_0/* byval_arg */
	, &RemotingException_t2350_1_0_0/* this_arg */
	, &RemotingException_t2350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t2350)/* instance_size */
	, sizeof (RemotingException_t2350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t2352_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::.cctor()
extern const MethodInfo RemotingServices__cctor_m12327_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingServices__cctor_m12327/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType MethodBase_t1434_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetVirtualMethod_m12328_ParameterInfos[] = 
{
	{"type", 0, 134222107, 0, &Type_t_0_0_0},
	{"method", 1, 134222108, 0, &MethodBase_t1434_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)
extern const MethodInfo RemotingServices_GetVirtualMethod_m12328_MethodInfo = 
{
	"GetVirtualMethod"/* name */
	, (methodPointerType)&RemotingServices_GetVirtualMethod_m12328/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetVirtualMethod_m12328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_IsTransparentProxy_m12329_ParameterInfos[] = 
{
	{"proxy", 0, 134222109, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)
extern const MethodInfo RemotingServices_IsTransparentProxy_m12329_MethodInfo = 
{
	"IsTransparentProxy"/* name */
	, (methodPointerType)&RemotingServices_IsTransparentProxy_m12329/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_IsTransparentProxy_m12329_ParameterInfos/* parameters */
	, 563/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetServerTypeForUri_m12330_ParameterInfos[] = 
{
	{"URI", 0, 134222110, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.RemotingServices::GetServerTypeForUri(System.String)
extern const MethodInfo RemotingServices_GetServerTypeForUri_m12330_MethodInfo = 
{
	"GetServerTypeForUri"/* name */
	, (methodPointerType)&RemotingServices_GetServerTypeForUri_m12330/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetServerTypeForUri_m12330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_Unmarshal_m12331_ParameterInfos[] = 
{
	{"objectRef", 0, 134222111, 0, &ObjRef_t2343_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef)
extern const MethodInfo RemotingServices_Unmarshal_m12331_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m12331/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_Unmarshal_m12331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_Unmarshal_m12332_ParameterInfos[] = 
{
	{"objectRef", 0, 134222112, 0, &ObjRef_t2343_0_0_0},
	{"fRefine", 1, 134222113, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef,System.Boolean)
extern const MethodInfo RemotingServices_Unmarshal_m12332_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m12332/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, RemotingServices_t2352_RemotingServices_Unmarshal_m12332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetRealProxy_m12333_ParameterInfos[] = 
{
	{"proxy", 0, 134222114, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.RemotingServices::GetRealProxy(System.Object)
extern const MethodInfo RemotingServices_GetRealProxy_m12333_MethodInfo = 
{
	"GetRealProxy"/* name */
	, (methodPointerType)&RemotingServices_GetRealProxy_m12333/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2335_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetRealProxy_m12333_ParameterInfos/* parameters */
	, 564/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMethodMessage_t2327_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetMethodBaseFromMethodMessage_m12334_ParameterInfos[] = 
{
	{"msg", 0, 134222115, 0, &IMethodMessage_t2327_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo RemotingServices_GetMethodBaseFromMethodMessage_m12334_MethodInfo = 
{
	"GetMethodBaseFromMethodMessage"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromMethodMessage_m12334/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetMethodBaseFromMethodMessage_m12334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetMethodBaseFromName_m12335_ParameterInfos[] = 
{
	{"type", 0, 134222116, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222117, 0, &String_t_0_0_0},
	{"signature", 2, 134222118, 0, &TypeU5BU5D_t878_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromName(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_GetMethodBaseFromName_m12335_MethodInfo = 
{
	"GetMethodBaseFromName"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromName_m12335/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetMethodBaseFromName_m12335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_FindInterfaceMethod_m12336_ParameterInfos[] = 
{
	{"type", 0, 134222119, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222120, 0, &String_t_0_0_0},
	{"signature", 2, 134222121, 0, &TypeU5BU5D_t878_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::FindInterfaceMethod(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_FindInterfaceMethod_m12336_MethodInfo = 
{
	"FindInterfaceMethod"/* name */
	, (methodPointerType)&RemotingServices_FindInterfaceMethod_m12336/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_FindInterfaceMethod_m12336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ActivatedClientTypeEntry_t2340_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_CreateClientProxy_m12337_ParameterInfos[] = 
{
	{"entry", 0, 134222122, 0, &ActivatedClientTypeEntry_t2340_0_0_0},
	{"activationAttributes", 1, 134222123, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.ActivatedClientTypeEntry,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxy_m12337_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m12337/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_CreateClientProxy_m12337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_CreateClientProxyForContextBound_m12338_ParameterInfos[] = 
{
	{"type", 0, 134222124, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134222125, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxyForContextBound(System.Type,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxyForContextBound_m12338_MethodInfo = 
{
	"CreateClientProxyForContextBound"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxyForContextBound_m12338/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_CreateClientProxyForContextBound_m12338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetIdentityForUri_m12339_ParameterInfos[] = 
{
	{"uri", 0, 134222126, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetIdentityForUri(System.String)
extern const MethodInfo RemotingServices_GetIdentityForUri_m12339_MethodInfo = 
{
	"GetIdentityForUri"/* name */
	, (methodPointerType)&RemotingServices_GetIdentityForUri_m12339/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Identity_t2337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetIdentityForUri_m12339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_RemoveAppNameFromUri_m12340_ParameterInfos[] = 
{
	{"uri", 0, 134222127, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::RemoveAppNameFromUri(System.String)
extern const MethodInfo RemotingServices_RemoveAppNameFromUri_m12340_MethodInfo = 
{
	"RemoveAppNameFromUri"/* name */
	, (methodPointerType)&RemotingServices_RemoveAppNameFromUri_m12340/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_RemoveAppNameFromUri_m12340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetOrCreateClientIdentity_m12341_ParameterInfos[] = 
{
	{"objRef", 0, 134222128, 0, &ObjRef_t2343_0_0_0},
	{"proxyType", 1, 134222129, 0, &Type_t_0_0_0},
	{"clientProxy", 2, 134222130, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ClientIdentity System.Runtime.Remoting.RemotingServices::GetOrCreateClientIdentity(System.Runtime.Remoting.ObjRef,System.Type,System.Object&)
extern const MethodInfo RemotingServices_GetOrCreateClientIdentity_m12341_MethodInfo = 
{
	"GetOrCreateClientIdentity"/* name */
	, (methodPointerType)&RemotingServices_GetOrCreateClientIdentity_m12341/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &ClientIdentity_t2345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1516/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetOrCreateClientIdentity_m12341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType WellKnownObjectMode_t2357_0_0_0;
extern const Il2CppType WellKnownObjectMode_t2357_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_CreateWellKnownServerIdentity_m12342_ParameterInfos[] = 
{
	{"objectType", 0, 134222131, 0, &Type_t_0_0_0},
	{"objectUri", 1, 134222132, 0, &String_t_0_0_0},
	{"mode", 2, 134222133, 0, &WellKnownObjectMode_t2357_0_0_0},
};
extern const Il2CppType ServerIdentity_t2061_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.RemotingServices::CreateWellKnownServerIdentity(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
extern const MethodInfo RemotingServices_CreateWellKnownServerIdentity_m12342_MethodInfo = 
{
	"CreateWellKnownServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_CreateWellKnownServerIdentity_m12342/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &ServerIdentity_t2061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127/* invoker_method */
	, RemotingServices_t2352_RemotingServices_CreateWellKnownServerIdentity_m12342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ServerIdentity_t2061_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_RegisterServerIdentity_m12343_ParameterInfos[] = 
{
	{"identity", 0, 134222134, 0, &ServerIdentity_t2061_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterServerIdentity(System.Runtime.Remoting.ServerIdentity)
extern const MethodInfo RemotingServices_RegisterServerIdentity_m12343_MethodInfo = 
{
	"RegisterServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_RegisterServerIdentity_m12343/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_RegisterServerIdentity_m12343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetProxyForRemoteObject_m12344_ParameterInfos[] = 
{
	{"objref", 0, 134222135, 0, &ObjRef_t2343_0_0_0},
	{"classToProxy", 1, 134222136, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetProxyForRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetProxyForRemoteObject_m12344_MethodInfo = 
{
	"GetProxyForRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetProxyForRemoteObject_m12344/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetProxyForRemoteObject_m12344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2343_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetRemoteObject_m12345_ParameterInfos[] = 
{
	{"objRef", 0, 134222137, 0, &ObjRef_t2343_0_0_0},
	{"proxyType", 1, 134222138, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetRemoteObject_m12345_MethodInfo = 
{
	"GetRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetRemoteObject_m12345/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetRemoteObject_m12345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterInternalChannels()
extern const MethodInfo RemotingServices_RegisterInternalChannels_m12346_MethodInfo = 
{
	"RegisterInternalChannels"/* name */
	, (methodPointerType)&RemotingServices_RegisterInternalChannels_m12346/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Identity_t2337_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_DisposeIdentity_m12347_ParameterInfos[] = 
{
	{"ident", 0, 134222139, 0, &Identity_t2337_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::DisposeIdentity(System.Runtime.Remoting.Identity)
extern const MethodInfo RemotingServices_DisposeIdentity_m12347_MethodInfo = 
{
	"DisposeIdentity"/* name */
	, (methodPointerType)&RemotingServices_DisposeIdentity_m12347/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_DisposeIdentity_m12347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2352_RemotingServices_GetNormalizedUri_m12348_ParameterInfos[] = 
{
	{"uri", 0, 134222140, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::GetNormalizedUri(System.String)
extern const MethodInfo RemotingServices_GetNormalizedUri_m12348_MethodInfo = 
{
	"GetNormalizedUri"/* name */
	, (methodPointerType)&RemotingServices_GetNormalizedUri_m12348/* method */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2352_RemotingServices_GetNormalizedUri_m12348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingServices_t2352_MethodInfos[] =
{
	&RemotingServices__cctor_m12327_MethodInfo,
	&RemotingServices_GetVirtualMethod_m12328_MethodInfo,
	&RemotingServices_IsTransparentProxy_m12329_MethodInfo,
	&RemotingServices_GetServerTypeForUri_m12330_MethodInfo,
	&RemotingServices_Unmarshal_m12331_MethodInfo,
	&RemotingServices_Unmarshal_m12332_MethodInfo,
	&RemotingServices_GetRealProxy_m12333_MethodInfo,
	&RemotingServices_GetMethodBaseFromMethodMessage_m12334_MethodInfo,
	&RemotingServices_GetMethodBaseFromName_m12335_MethodInfo,
	&RemotingServices_FindInterfaceMethod_m12336_MethodInfo,
	&RemotingServices_CreateClientProxy_m12337_MethodInfo,
	&RemotingServices_CreateClientProxyForContextBound_m12338_MethodInfo,
	&RemotingServices_GetIdentityForUri_m12339_MethodInfo,
	&RemotingServices_RemoveAppNameFromUri_m12340_MethodInfo,
	&RemotingServices_GetOrCreateClientIdentity_m12341_MethodInfo,
	&RemotingServices_CreateWellKnownServerIdentity_m12342_MethodInfo,
	&RemotingServices_RegisterServerIdentity_m12343_MethodInfo,
	&RemotingServices_GetProxyForRemoteObject_m12344_MethodInfo,
	&RemotingServices_GetRemoteObject_m12345_MethodInfo,
	&RemotingServices_RegisterInternalChannels_m12346_MethodInfo,
	&RemotingServices_DisposeIdentity_m12347_MethodInfo,
	&RemotingServices_GetNormalizedUri_m12348_MethodInfo,
	NULL
};
static const Il2CppMethodReference RemotingServices_t2352_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RemotingServices_t2352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingServices_t2352_0_0_0;
extern const Il2CppType RemotingServices_t2352_1_0_0;
struct RemotingServices_t2352;
const Il2CppTypeDefinitionMetadata RemotingServices_t2352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t2352_VTable/* vtableMethods */
	, RemotingServices_t2352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1642/* fieldStart */

};
TypeInfo RemotingServices_t2352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingServices_t2352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingServices_t2352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 562/* custom_attributes_cache */
	, &RemotingServices_t2352_0_0_0/* byval_arg */
	, &RemotingServices_t2352_1_0_0/* this_arg */
	, &RemotingServices_t2352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t2352)/* instance_size */
	, sizeof (RemotingServices_t2352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t2352_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t2061_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t2061_ServerIdentity__ctor_m12349_ParameterInfos[] = 
{
	{"objectUri", 0, 134222141, 0, &String_t_0_0_0},
	{"context", 1, 134222142, 0, &Context_t2306_0_0_0},
	{"objectType", 2, 134222143, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo ServerIdentity__ctor_m12349_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ServerIdentity__ctor_m12349/* method */
	, &ServerIdentity_t2061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t2061_ServerIdentity__ctor_m12349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ServerIdentity::get_ObjectType()
extern const MethodInfo ServerIdentity_get_ObjectType_m12350_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ServerIdentity_get_ObjectType_m12350/* method */
	, &ServerIdentity_t2061_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t2061_ServerIdentity_CreateObjRef_m12351_ParameterInfos[] = 
{
	{"requestedType", 0, 134222144, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ServerIdentity::CreateObjRef(System.Type)
extern const MethodInfo ServerIdentity_CreateObjRef_m12351_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ServerIdentity_CreateObjRef_m12351/* method */
	, &ServerIdentity_t2061_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t2061_ServerIdentity_CreateObjRef_m12351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ServerIdentity_t2061_MethodInfos[] =
{
	&ServerIdentity__ctor_m12349_MethodInfo,
	&ServerIdentity_get_ObjectType_m12350_MethodInfo,
	&ServerIdentity_CreateObjRef_m12351_MethodInfo,
	NULL
};
extern const MethodInfo ServerIdentity_get_ObjectType_m12350_MethodInfo;
static const PropertyInfo ServerIdentity_t2061____ObjectType_PropertyInfo = 
{
	&ServerIdentity_t2061_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ServerIdentity_get_ObjectType_m12350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ServerIdentity_t2061_PropertyInfos[] =
{
	&ServerIdentity_t2061____ObjectType_PropertyInfo,
	NULL
};
extern const MethodInfo ServerIdentity_CreateObjRef_m12351_MethodInfo;
static const Il2CppMethodReference ServerIdentity_t2061_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ServerIdentity_CreateObjRef_m12351_MethodInfo,
};
static bool ServerIdentity_t2061_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ServerIdentity_t2061_1_0_0;
struct ServerIdentity_t2061;
const Il2CppTypeDefinitionMetadata ServerIdentity_t2061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2337_0_0_0/* parent */
	, ServerIdentity_t2061_VTable/* vtableMethods */
	, ServerIdentity_t2061_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1650/* fieldStart */

};
TypeInfo ServerIdentity_t2061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ServerIdentity_t2061_MethodInfos/* methods */
	, ServerIdentity_t2061_PropertyInfos/* properties */
	, NULL/* events */
	, &ServerIdentity_t2061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t2061_0_0_0/* byval_arg */
	, &ServerIdentity_t2061_1_0_0/* this_arg */
	, &ServerIdentity_t2061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t2061)/* instance_size */
	, sizeof (ServerIdentity_t2061)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t2353_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern const MethodInfo ClientActivatedIdentity_GetServerObject_m12352_MethodInfo = 
{
	"GetServerObject"/* name */
	, (methodPointerType)&ClientActivatedIdentity_GetServerObject_m12352/* method */
	, &ClientActivatedIdentity_t2353_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1885_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientActivatedIdentity_t2353_MethodInfos[] =
{
	&ClientActivatedIdentity_GetServerObject_m12352_MethodInfo,
	NULL
};
static const Il2CppMethodReference ClientActivatedIdentity_t2353_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ServerIdentity_CreateObjRef_m12351_MethodInfo,
};
static bool ClientActivatedIdentity_t2353_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientActivatedIdentity_t2353_0_0_0;
extern const Il2CppType ClientActivatedIdentity_t2353_1_0_0;
struct ClientActivatedIdentity_t2353;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t2353_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2061_0_0_0/* parent */
	, ClientActivatedIdentity_t2353_VTable/* vtableMethods */
	, ClientActivatedIdentity_t2353_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ClientActivatedIdentity_t2353_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientActivatedIdentity_t2353_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ClientActivatedIdentity_t2353_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t2353_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t2353_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t2353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t2353)/* instance_size */
	, sizeof (ClientActivatedIdentity_t2353)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t2354_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingletonIdentity_t2354_SingletonIdentity__ctor_m12353_ParameterInfos[] = 
{
	{"objectUri", 0, 134222145, 0, &String_t_0_0_0},
	{"context", 1, 134222146, 0, &Context_t2306_0_0_0},
	{"objectType", 2, 134222147, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingletonIdentity__ctor_m12353_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingletonIdentity__ctor_m12353/* method */
	, &SingletonIdentity_t2354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t/* invoker_method */
	, SingletonIdentity_t2354_SingletonIdentity__ctor_m12353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingletonIdentity_t2354_MethodInfos[] =
{
	&SingletonIdentity__ctor_m12353_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingletonIdentity_t2354_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ServerIdentity_CreateObjRef_m12351_MethodInfo,
};
static bool SingletonIdentity_t2354_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingletonIdentity_t2354_0_0_0;
extern const Il2CppType SingletonIdentity_t2354_1_0_0;
struct SingletonIdentity_t2354;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t2354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2061_0_0_0/* parent */
	, SingletonIdentity_t2354_VTable/* vtableMethods */
	, SingletonIdentity_t2354_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingletonIdentity_t2354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingletonIdentity_t2354_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingletonIdentity_t2354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t2354_0_0_0/* byval_arg */
	, &SingletonIdentity_t2354_1_0_0/* this_arg */
	, &SingletonIdentity_t2354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t2354)/* instance_size */
	, sizeof (SingletonIdentity_t2354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t2355_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t2306_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingleCallIdentity_t2355_SingleCallIdentity__ctor_m12354_ParameterInfos[] = 
{
	{"objectUri", 0, 134222148, 0, &String_t_0_0_0},
	{"context", 1, 134222149, 0, &Context_t2306_0_0_0},
	{"objectType", 2, 134222150, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingleCallIdentity__ctor_m12354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingleCallIdentity__ctor_m12354/* method */
	, &SingleCallIdentity_t2355_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t/* invoker_method */
	, SingleCallIdentity_t2355_SingleCallIdentity__ctor_m12354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingleCallIdentity_t2355_MethodInfos[] =
{
	&SingleCallIdentity__ctor_m12354_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingleCallIdentity_t2355_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ServerIdentity_CreateObjRef_m12351_MethodInfo,
};
static bool SingleCallIdentity_t2355_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingleCallIdentity_t2355_0_0_0;
extern const Il2CppType SingleCallIdentity_t2355_1_0_0;
struct SingleCallIdentity_t2355;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t2355_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2061_0_0_0/* parent */
	, SingleCallIdentity_t2355_VTable/* vtableMethods */
	, SingleCallIdentity_t2355_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingleCallIdentity_t2355_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingleCallIdentity_t2355_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingleCallIdentity_t2355_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t2355_0_0_0/* byval_arg */
	, &SingleCallIdentity_t2355_1_0_0/* this_arg */
	, &SingleCallIdentity_t2355_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t2355)/* instance_size */
	, sizeof (SingleCallIdentity_t2355)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t2341_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
extern const MethodInfo TypeEntry_get_AssemblyName_m12355_MethodInfo = 
{
	"get_AssemblyName"/* name */
	, (methodPointerType)&TypeEntry_get_AssemblyName_m12355/* method */
	, &TypeEntry_t2341_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
extern const MethodInfo TypeEntry_get_TypeName_m12356_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeEntry_get_TypeName_m12356/* method */
	, &TypeEntry_t2341_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeEntry_t2341_MethodInfos[] =
{
	&TypeEntry_get_AssemblyName_m12355_MethodInfo,
	&TypeEntry_get_TypeName_m12356_MethodInfo,
	NULL
};
extern const MethodInfo TypeEntry_get_AssemblyName_m12355_MethodInfo;
static const PropertyInfo TypeEntry_t2341____AssemblyName_PropertyInfo = 
{
	&TypeEntry_t2341_il2cpp_TypeInfo/* parent */
	, "AssemblyName"/* name */
	, &TypeEntry_get_AssemblyName_m12355_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TypeEntry_get_TypeName_m12356_MethodInfo;
static const PropertyInfo TypeEntry_t2341____TypeName_PropertyInfo = 
{
	&TypeEntry_t2341_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeEntry_get_TypeName_m12356_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeEntry_t2341_PropertyInfos[] =
{
	&TypeEntry_t2341____AssemblyName_PropertyInfo,
	&TypeEntry_t2341____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeEntry_t2341_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TypeEntry_t2341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeEntry_t2341_1_0_0;
struct TypeEntry_t2341;
const Il2CppTypeDefinitionMetadata TypeEntry_t2341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t2341_VTable/* vtableMethods */
	, TypeEntry_t2341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1653/* fieldStart */

};
TypeInfo TypeEntry_t2341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeEntry_t2341_MethodInfos/* methods */
	, TypeEntry_t2341_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeEntry_t2341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 565/* custom_attributes_cache */
	, &TypeEntry_t2341_0_0_0/* byval_arg */
	, &TypeEntry_t2341_1_0_0/* this_arg */
	, &TypeEntry_t2341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t2341)/* instance_size */
	, sizeof (TypeEntry_t2341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t2356_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeInfo_t2356_TypeInfo__ctor_m12357_ParameterInfos[] = 
{
	{"type", 0, 134222151, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.TypeInfo::.ctor(System.Type)
extern const MethodInfo TypeInfo__ctor_m12357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInfo__ctor_m12357/* method */
	, &TypeInfo_t2356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TypeInfo_t2356_TypeInfo__ctor_m12357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeInfo::get_TypeName()
extern const MethodInfo TypeInfo_get_TypeName_m12358_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeInfo_get_TypeName_m12358/* method */
	, &TypeInfo_t2356_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInfo_t2356_MethodInfos[] =
{
	&TypeInfo__ctor_m12357_MethodInfo,
	&TypeInfo_get_TypeName_m12358_MethodInfo,
	NULL
};
extern const MethodInfo TypeInfo_get_TypeName_m12358_MethodInfo;
static const PropertyInfo TypeInfo_t2356____TypeName_PropertyInfo = 
{
	&TypeInfo_t2356_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeInfo_get_TypeName_m12358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeInfo_t2356_PropertyInfos[] =
{
	&TypeInfo_t2356____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeInfo_t2356_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&TypeInfo_get_TypeName_m12358_MethodInfo,
};
static bool TypeInfo_t2356_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* TypeInfo_t2356_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2347_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t2356_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2347_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t2356_0_0_0;
extern const Il2CppType TypeInfo_t2356_1_0_0;
struct TypeInfo_t2356;
const Il2CppTypeDefinitionMetadata TypeInfo_t2356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t2356_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t2356_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t2356_VTable/* vtableMethods */
	, TypeInfo_t2356_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1655/* fieldStart */

};
TypeInfo TypeInfo_t2356_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeInfo_t2356_MethodInfos/* methods */
	, TypeInfo_t2356_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeInfo_t2356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t2356_0_0_0/* byval_arg */
	, &TypeInfo_t2356_1_0_0/* this_arg */
	, &TypeInfo_t2356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t2356)/* instance_size */
	, sizeof (TypeInfo_t2356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t2357_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static const MethodInfo* WellKnownObjectMode_t2357_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference WellKnownObjectMode_t2357_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool WellKnownObjectMode_t2357_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t2357_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownObjectMode_t2357_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t2357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t2357_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, WellKnownObjectMode_t2357_VTable/* vtableMethods */
	, WellKnownObjectMode_t2357_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1658/* fieldStart */

};
TypeInfo WellKnownObjectMode_t2357_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, WellKnownObjectMode_t2357_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 566/* custom_attributes_cache */
	, &WellKnownObjectMode_t2357_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t2357_1_0_0/* this_arg */
	, &WellKnownObjectMode_t2357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t2357)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t2357)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t2358_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::.cctor()
extern const MethodInfo BinaryCommon__cctor_m12359_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&BinaryCommon__cctor_m12359/* method */
	, &BinaryCommon_t2358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo BinaryCommon_t2358_BinaryCommon_IsPrimitive_m12360_ParameterInfos[] = 
{
	{"type", 0, 134222152, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryCommon::IsPrimitive(System.Type)
extern const MethodInfo BinaryCommon_IsPrimitive_m12360_MethodInfo = 
{
	"IsPrimitive"/* name */
	, (methodPointerType)&BinaryCommon_IsPrimitive_m12360/* method */
	, &BinaryCommon_t2358_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, BinaryCommon_t2358_BinaryCommon_IsPrimitive_m12360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo BinaryCommon_t2358_BinaryCommon_GetTypeFromCode_m12361_ParameterInfos[] = 
{
	{"code", 0, 134222153, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryCommon::GetTypeFromCode(System.Int32)
extern const MethodInfo BinaryCommon_GetTypeFromCode_m12361_MethodInfo = 
{
	"GetTypeFromCode"/* name */
	, (methodPointerType)&BinaryCommon_GetTypeFromCode_m12361/* method */
	, &BinaryCommon_t2358_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, BinaryCommon_t2358_BinaryCommon_GetTypeFromCode_m12361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo BinaryCommon_t2358_BinaryCommon_SwapBytes_m12362_ParameterInfos[] = 
{
	{"byteArray", 0, 134222154, 0, &ByteU5BU5D_t616_0_0_0},
	{"size", 1, 134222155, 0, &Int32_t127_0_0_0},
	{"dataSize", 2, 134222156, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::SwapBytes(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo BinaryCommon_SwapBytes_m12362_MethodInfo = 
{
	"SwapBytes"/* name */
	, (methodPointerType)&BinaryCommon_SwapBytes_m12362/* method */
	, &BinaryCommon_t2358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, BinaryCommon_t2358_BinaryCommon_SwapBytes_m12362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryCommon_t2358_MethodInfos[] =
{
	&BinaryCommon__cctor_m12359_MethodInfo,
	&BinaryCommon_IsPrimitive_m12360_MethodInfo,
	&BinaryCommon_GetTypeFromCode_m12361_MethodInfo,
	&BinaryCommon_SwapBytes_m12362_MethodInfo,
	NULL
};
static const Il2CppMethodReference BinaryCommon_t2358_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool BinaryCommon_t2358_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryCommon_t2358_0_0_0;
extern const Il2CppType BinaryCommon_t2358_1_0_0;
struct BinaryCommon_t2358;
const Il2CppTypeDefinitionMetadata BinaryCommon_t2358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t2358_VTable/* vtableMethods */
	, BinaryCommon_t2358_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1661/* fieldStart */

};
TypeInfo BinaryCommon_t2358_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryCommon_t2358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BinaryCommon_t2358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t2358_0_0_0/* byval_arg */
	, &BinaryCommon_t2358_1_0_0/* this_arg */
	, &BinaryCommon_t2358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t2358)/* instance_size */
	, sizeof (BinaryCommon_t2358)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t2358_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t2359_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
static const MethodInfo* BinaryElement_t2359_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BinaryElement_t2359_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool BinaryElement_t2359_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BinaryElement_t2359_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryElement_t2359_1_0_0;
const Il2CppTypeDefinitionMetadata BinaryElement_t2359_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t2359_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, BinaryElement_t2359_VTable/* vtableMethods */
	, BinaryElement_t2359_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1665/* fieldStart */

};
TypeInfo BinaryElement_t2359_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryElement_t2359_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t2359_0_0_0/* byval_arg */
	, &BinaryElement_t2359_1_0_0/* this_arg */
	, &BinaryElement_t2359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t2359)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t2359)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t2360_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static const MethodInfo* TypeTag_t2360_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeTag_t2360_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TypeTag_t2360_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeTag_t2360_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeTag_t2360_0_0_0;
extern const Il2CppType TypeTag_t2360_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t2360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t2360_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TypeTag_t2360_VTable/* vtableMethods */
	, TypeTag_t2360_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1689/* fieldStart */

};
TypeInfo TypeTag_t2360_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, TypeTag_t2360_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t2360_0_0_0/* byval_arg */
	, &TypeTag_t2360_1_0_0/* this_arg */
	, &TypeTag_t2360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t2360)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t2360)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t2361_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static const MethodInfo* MethodFlags_t2361_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodFlags_t2361_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool MethodFlags_t2361_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodFlags_t2361_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodFlags_t2361_0_0_0;
extern const Il2CppType MethodFlags_t2361_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t2361_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t2361_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, MethodFlags_t2361_VTable/* vtableMethods */
	, MethodFlags_t2361_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1698/* fieldStart */

};
TypeInfo MethodFlags_t2361_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MethodFlags_t2361_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t2361_0_0_0/* byval_arg */
	, &MethodFlags_t2361_1_0_0/* this_arg */
	, &MethodFlags_t2361_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t2361)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t2361)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t2362_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static const MethodInfo* ReturnTypeTag_t2362_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReturnTypeTag_t2362_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ReturnTypeTag_t2362_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t2362_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnTypeTag_t2362_0_0_0;
extern const Il2CppType ReturnTypeTag_t2362_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t2362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t2362_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ReturnTypeTag_t2362_VTable/* vtableMethods */
	, ReturnTypeTag_t2362_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1709/* fieldStart */

};
TypeInfo ReturnTypeTag_t2362_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ReturnTypeTag_t2362_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t2362_0_0_0/* byval_arg */
	, &ReturnTypeTag_t2362_1_0_0/* this_arg */
	, &ReturnTypeTag_t2362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t2362)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t2362)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t2351_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern const MethodInfo BinaryFormatter__ctor_m12363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m12363/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ISurrogateSelector_t2331_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo BinaryFormatter_t2351_BinaryFormatter__ctor_m12364_ParameterInfos[] = 
{
	{"selector", 0, 134222157, 0, &ISurrogateSelector_t2331_0_0_0},
	{"context", 1, 134222158, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo BinaryFormatter__ctor_m12364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m12364/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, BinaryFormatter_t2351_BinaryFormatter__ctor_m12364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_DefaultSurrogateSelector()
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m12365_MethodInfo = 
{
	"get_DefaultSurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_DefaultSurrogateSelector_m12365/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t2331_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 569/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FormatterAssemblyStyle_t2370_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t2370_0_0_0;
static const ParameterInfo BinaryFormatter_t2351_BinaryFormatter_set_AssemblyFormat_m12366_ParameterInfos[] = 
{
	{"value", 0, 134222159, 0, &FormatterAssemblyStyle_t2370_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_AssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m12366_MethodInfo = 
{
	"set_AssemblyFormat"/* name */
	, (methodPointerType)&BinaryFormatter_set_AssemblyFormat_m12366/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, BinaryFormatter_t2351_BinaryFormatter_set_AssemblyFormat_m12366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationBinder_t2363_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Binder()
extern const MethodInfo BinaryFormatter_get_Binder_m12367_MethodInfo = 
{
	"get_Binder"/* name */
	, (methodPointerType)&BinaryFormatter_get_Binder_m12367/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &SerializationBinder_t2363_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Context()
extern const MethodInfo BinaryFormatter_get_Context_m12368_MethodInfo = 
{
	"get_Context"/* name */
	, (methodPointerType)&BinaryFormatter_get_Context_m12368/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &StreamingContext_t1383_0_0_0/* return_type */
	, RuntimeInvoker_StreamingContext_t1383/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_SurrogateSelector()
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m12369_MethodInfo = 
{
	"get_SurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_SurrogateSelector_m12369/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t2331_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeFilterLevel_t2372_0_0_0;
extern void* RuntimeInvoker_TypeFilterLevel_t2372 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_FilterLevel()
extern const MethodInfo BinaryFormatter_get_FilterLevel_m12370_MethodInfo = 
{
	"get_FilterLevel"/* name */
	, (methodPointerType)&BinaryFormatter_get_FilterLevel_m12370/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &TypeFilterLevel_t2372_0_0_0/* return_type */
	, RuntimeInvoker_TypeFilterLevel_t2372/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t1751_0_0_0;
extern const Il2CppType Stream_t1751_0_0_0;
static const ParameterInfo BinaryFormatter_t2351_BinaryFormatter_Deserialize_m12371_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222160, 0, &Stream_t1751_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern const MethodInfo BinaryFormatter_Deserialize_m12371_MethodInfo = 
{
	"Deserialize"/* name */
	, (methodPointerType)&BinaryFormatter_Deserialize_m12371/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t2351_BinaryFormatter_Deserialize_m12371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t1751_0_0_0;
extern const Il2CppType HeaderHandler_t2561_0_0_0;
extern const Il2CppType HeaderHandler_t2561_0_0_0;
static const ParameterInfo BinaryFormatter_t2351_BinaryFormatter_NoCheckDeserialize_m12372_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222161, 0, &Stream_t1751_0_0_0},
	{"handler", 1, 134222162, 0, &HeaderHandler_t2561_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern const MethodInfo BinaryFormatter_NoCheckDeserialize_m12372_MethodInfo = 
{
	"NoCheckDeserialize"/* name */
	, (methodPointerType)&BinaryFormatter_NoCheckDeserialize_m12372/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t2351_BinaryFormatter_NoCheckDeserialize_m12372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_1_0_2;
extern const Il2CppType Boolean_t169_1_0_0;
static const ParameterInfo BinaryFormatter_t2351_BinaryFormatter_ReadBinaryHeader_m12373_ParameterInfos[] = 
{
	{"reader", 0, 134222163, 0, &BinaryReader_t2182_0_0_0},
	{"hasHeaders", 1, 134222164, 0, &Boolean_t169_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_BooleanU26_t526 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::ReadBinaryHeader(System.IO.BinaryReader,System.Boolean&)
extern const MethodInfo BinaryFormatter_ReadBinaryHeader_m12373_MethodInfo = 
{
	"ReadBinaryHeader"/* name */
	, (methodPointerType)&BinaryFormatter_ReadBinaryHeader_m12373/* method */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_BooleanU26_t526/* invoker_method */
	, BinaryFormatter_t2351_BinaryFormatter_ReadBinaryHeader_m12373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryFormatter_t2351_MethodInfos[] =
{
	&BinaryFormatter__ctor_m12363_MethodInfo,
	&BinaryFormatter__ctor_m12364_MethodInfo,
	&BinaryFormatter_get_DefaultSurrogateSelector_m12365_MethodInfo,
	&BinaryFormatter_set_AssemblyFormat_m12366_MethodInfo,
	&BinaryFormatter_get_Binder_m12367_MethodInfo,
	&BinaryFormatter_get_Context_m12368_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m12369_MethodInfo,
	&BinaryFormatter_get_FilterLevel_m12370_MethodInfo,
	&BinaryFormatter_Deserialize_m12371_MethodInfo,
	&BinaryFormatter_NoCheckDeserialize_m12372_MethodInfo,
	&BinaryFormatter_ReadBinaryHeader_m12373_MethodInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m12365_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____DefaultSurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "DefaultSurrogateSelector"/* name */
	, &BinaryFormatter_get_DefaultSurrogateSelector_m12365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m12366_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____AssemblyFormat_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "AssemblyFormat"/* name */
	, NULL/* get */
	, &BinaryFormatter_set_AssemblyFormat_m12366_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Binder_m12367_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____Binder_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "Binder"/* name */
	, &BinaryFormatter_get_Binder_m12367_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Context_m12368_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____Context_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "Context"/* name */
	, &BinaryFormatter_get_Context_m12368_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m12369_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____SurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "SurrogateSelector"/* name */
	, &BinaryFormatter_get_SurrogateSelector_m12369_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_FilterLevel_m12370_MethodInfo;
static const PropertyInfo BinaryFormatter_t2351____FilterLevel_PropertyInfo = 
{
	&BinaryFormatter_t2351_il2cpp_TypeInfo/* parent */
	, "FilterLevel"/* name */
	, &BinaryFormatter_get_FilterLevel_m12370_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BinaryFormatter_t2351_PropertyInfos[] =
{
	&BinaryFormatter_t2351____DefaultSurrogateSelector_PropertyInfo,
	&BinaryFormatter_t2351____AssemblyFormat_PropertyInfo,
	&BinaryFormatter_t2351____Binder_PropertyInfo,
	&BinaryFormatter_t2351____Context_PropertyInfo,
	&BinaryFormatter_t2351____SurrogateSelector_PropertyInfo,
	&BinaryFormatter_t2351____FilterLevel_PropertyInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_Deserialize_m12371_MethodInfo;
static const Il2CppMethodReference BinaryFormatter_t2351_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&BinaryFormatter_get_Binder_m12367_MethodInfo,
	&BinaryFormatter_get_Context_m12368_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m12369_MethodInfo,
	&BinaryFormatter_Deserialize_m12371_MethodInfo,
};
static bool BinaryFormatter_t2351_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* BinaryFormatter_t2351_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t2686_0_0_0,
	&IFormatter_t2688_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t2351_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t2686_0_0_0, 4},
	{ &IFormatter_t2688_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryFormatter_t2351_0_0_0;
extern const Il2CppType BinaryFormatter_t2351_1_0_0;
struct BinaryFormatter_t2351;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t2351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t2351_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t2351_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t2351_VTable/* vtableMethods */
	, BinaryFormatter_t2351_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1714/* fieldStart */

};
TypeInfo BinaryFormatter_t2351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryFormatter_t2351_MethodInfos/* methods */
	, BinaryFormatter_t2351_PropertyInfos/* properties */
	, NULL/* events */
	, &BinaryFormatter_t2351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 567/* custom_attributes_cache */
	, &BinaryFormatter_t2351_0_0_0/* byval_arg */
	, &BinaryFormatter_t2351_1_0_0/* this_arg */
	, &BinaryFormatter_t2351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t2351)/* instance_size */
	, sizeof (BinaryFormatter_t2351)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t2351_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t2364_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType HeaderHandler_t2561_0_0_0;
extern const Il2CppType BinaryFormatter_t2351_0_0_0;
static const ParameterInfo MessageFormatter_t2364_MessageFormatter_ReadMethodCall_m12374_ParameterInfos[] = 
{
	{"elem", 0, 134222165, 0, &BinaryElement_t2359_0_0_0},
	{"reader", 1, 134222166, 0, &BinaryReader_t2182_0_0_0},
	{"hasHeaders", 2, 134222167, 0, &Boolean_t169_0_0_0},
	{"headerHandler", 3, 134222168, 0, &HeaderHandler_t2561_0_0_0},
	{"formatter", 4, 134222169, 0, &BinaryFormatter_t2351_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t449_Object_t_SByte_t170_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodCall_m12374_MethodInfo = 
{
	"ReadMethodCall"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodCall_m12374/* method */
	, &MessageFormatter_t2364_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t449_Object_t_SByte_t170_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2364_MessageFormatter_ReadMethodCall_m12374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType HeaderHandler_t2561_0_0_0;
extern const Il2CppType IMethodCallMessage_t2596_0_0_0;
extern const Il2CppType BinaryFormatter_t2351_0_0_0;
static const ParameterInfo MessageFormatter_t2364_MessageFormatter_ReadMethodResponse_m12375_ParameterInfos[] = 
{
	{"elem", 0, 134222170, 0, &BinaryElement_t2359_0_0_0},
	{"reader", 1, 134222171, 0, &BinaryReader_t2182_0_0_0},
	{"hasHeaders", 2, 134222172, 0, &Boolean_t169_0_0_0},
	{"headerHandler", 3, 134222173, 0, &HeaderHandler_t2561_0_0_0},
	{"methodCallMessage", 4, 134222174, 0, &IMethodCallMessage_t2596_0_0_0},
	{"formatter", 5, 134222175, 0, &BinaryFormatter_t2351_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t449_Object_t_SByte_t170_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodResponse_m12375_MethodInfo = 
{
	"ReadMethodResponse"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodResponse_m12375/* method */
	, &MessageFormatter_t2364_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t449_Object_t_SByte_t170_Object_t_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2364_MessageFormatter_ReadMethodResponse_m12375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MessageFormatter_t2364_MethodInfos[] =
{
	&MessageFormatter_ReadMethodCall_m12374_MethodInfo,
	&MessageFormatter_ReadMethodResponse_m12375_MethodInfo,
	NULL
};
static const Il2CppMethodReference MessageFormatter_t2364_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MessageFormatter_t2364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MessageFormatter_t2364_0_0_0;
extern const Il2CppType MessageFormatter_t2364_1_0_0;
struct MessageFormatter_t2364;
const Il2CppTypeDefinitionMetadata MessageFormatter_t2364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t2364_VTable/* vtableMethods */
	, MessageFormatter_t2364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MessageFormatter_t2364_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MessageFormatter_t2364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MessageFormatter_t2364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t2364_0_0_0/* byval_arg */
	, &MessageFormatter_t2364_1_0_0/* this_arg */
	, &MessageFormatter_t2364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t2364)/* instance_size */
	, sizeof (MessageFormatter_t2364)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t2366_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata::.ctor()
extern const MethodInfo TypeMetadata__ctor_m12376_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeMetadata__ctor_m12376/* method */
	, &TypeMetadata_t2366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeMetadata_t2366_MethodInfos[] =
{
	&TypeMetadata__ctor_m12376_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeMetadata_t2366_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TypeMetadata_t2366_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t2366_0_0_0;
extern const Il2CppType TypeMetadata_t2366_1_0_0;
extern TypeInfo ObjectReader_t2369_il2cpp_TypeInfo;
extern const Il2CppType ObjectReader_t2369_0_0_0;
struct TypeMetadata_t2366;
const Il2CppTypeDefinitionMetadata TypeMetadata_t2366_DefinitionMetadata = 
{
	&ObjectReader_t2369_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t2366_VTable/* vtableMethods */
	, TypeMetadata_t2366_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1721/* fieldStart */

};
TypeInfo TypeMetadata_t2366_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, TypeMetadata_t2366_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeMetadata_t2366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t2366_0_0_0/* byval_arg */
	, &TypeMetadata_t2366_1_0_0/* this_arg */
	, &TypeMetadata_t2366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t2366)/* instance_size */
	, sizeof (TypeMetadata_t2366)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t2367_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ArrayNullFiller_t2367_ArrayNullFiller__ctor_m12377_ParameterInfos[] = 
{
	{"count", 0, 134222270, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
extern const MethodInfo ArrayNullFiller__ctor_m12377_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArrayNullFiller__ctor_m12377/* method */
	, &ArrayNullFiller_t2367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ArrayNullFiller_t2367_ArrayNullFiller__ctor_m12377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArrayNullFiller_t2367_MethodInfos[] =
{
	&ArrayNullFiller__ctor_m12377_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArrayNullFiller_t2367_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ArrayNullFiller_t2367_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayNullFiller_t2367_0_0_0;
extern const Il2CppType ArrayNullFiller_t2367_1_0_0;
struct ArrayNullFiller_t2367;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t2367_DefinitionMetadata = 
{
	&ObjectReader_t2369_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t2367_VTable/* vtableMethods */
	, ArrayNullFiller_t2367_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1727/* fieldStart */

};
TypeInfo ArrayNullFiller_t2367_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, ArrayNullFiller_t2367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArrayNullFiller_t2367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t2367_0_0_0/* byval_arg */
	, &ArrayNullFiller_t2367_1_0_0/* this_arg */
	, &ArrayNullFiller_t2367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t2367)/* instance_size */
	, sizeof (ArrayNullFiller_t2367)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern const Il2CppType BinaryFormatter_t2351_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader__ctor_m12378_ParameterInfos[] = 
{
	{"formatter", 0, 134222176, 0, &BinaryFormatter_t2351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::.ctor(System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo ObjectReader__ctor_m12378_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectReader__ctor_m12378/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader__ctor_m12378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2560_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2560_1_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadObjectGraph_m12379_ParameterInfos[] = 
{
	{"reader", 0, 134222177, 0, &BinaryReader_t2182_0_0_0},
	{"readHeaders", 1, 134222178, 0, &Boolean_t169_0_0_0},
	{"result", 2, 134222179, 0, &Object_t_1_0_2},
	{"headers", 3, 134222180, 0, &HeaderU5BU5D_t2560_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170_ObjectU26_t1516_HeaderU5BU5DU26_t3059 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m12379_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m12379/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170_ObjectU26_t1516_HeaderU5BU5DU26_t3059/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadObjectGraph_m12379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2560_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadObjectGraph_m12380_ParameterInfos[] = 
{
	{"elem", 0, 134222181, 0, &BinaryElement_t2359_0_0_0},
	{"reader", 1, 134222182, 0, &BinaryReader_t2182_0_0_0},
	{"readHeaders", 2, 134222183, 0, &Boolean_t169_0_0_0},
	{"result", 3, 134222184, 0, &Object_t_1_0_2},
	{"headers", 4, 134222185, 0, &HeaderU5BU5D_t2560_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Byte_t449_Object_t_SByte_t170_ObjectU26_t1516_HeaderU5BU5DU26_t3059 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m12380_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m12380/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Byte_t449_Object_t_SByte_t170_ObjectU26_t1516_HeaderU5BU5DU26_t3059/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadObjectGraph_m12380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadNextObject_m12381_ParameterInfos[] = 
{
	{"element", 0, 134222186, 0, &BinaryElement_t2359_0_0_0},
	{"reader", 1, 134222187, 0, &BinaryReader_t2182_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Byte_t449_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m12381_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m12381/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Byte_t449_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadNextObject_m12381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadNextObject_m12382_ParameterInfos[] = 
{
	{"reader", 0, 134222188, 0, &BinaryReader_t2182_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m12382_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m12382/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadNextObject_m12382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::get_CurrentObject()
extern const MethodInfo ObjectReader_get_CurrentObject_m12383_MethodInfo = 
{
	"get_CurrentObject"/* name */
	, (methodPointerType)&ObjectReader_get_CurrentObject_m12383/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2359_0_0_0;
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Int64_t1092_1_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1382_1_0_2;
extern const Il2CppType SerializationInfo_t1382_1_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadObject_m12384_ParameterInfos[] = 
{
	{"element", 0, 134222189, 0, &BinaryElement_t2359_0_0_0},
	{"reader", 1, 134222190, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 2, 134222191, 0, &Int64_t1092_1_0_2},
	{"value", 3, 134222192, 0, &Object_t_1_0_2},
	{"info", 4, 134222193, 0, &SerializationInfo_t1382_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Byte_t449_Object_t_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObject_m12384_MethodInfo = 
{
	"ReadObject"/* name */
	, (methodPointerType)&ObjectReader_ReadObject_m12384/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Byte_t449_Object_t_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadObject_m12384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadAssembly_m12385_ParameterInfos[] = 
{
	{"reader", 0, 134222194, 0, &BinaryReader_t2182_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadAssembly(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadAssembly_m12385_MethodInfo = 
{
	"ReadAssembly"/* name */
	, (methodPointerType)&ObjectReader_ReadAssembly_m12385/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadAssembly_m12385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1382_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadObjectInstance_m12386_ParameterInfos[] = 
{
	{"reader", 0, 134222195, 0, &BinaryReader_t2182_0_0_0},
	{"isRuntimeObject", 1, 134222196, 0, &Boolean_t169_0_0_0},
	{"hasTypeInfo", 2, 134222197, 0, &Boolean_t169_0_0_0},
	{"objectId", 3, 134222198, 0, &Int64_t1092_1_0_2},
	{"value", 4, 134222199, 0, &Object_t_1_0_2},
	{"info", 5, 134222200, 0, &SerializationInfo_t1382_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectInstance(System.IO.BinaryReader,System.Boolean,System.Boolean,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectInstance_m12386_MethodInfo = 
{
	"ReadObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectInstance_m12386/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadObjectInstance_m12386_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1382_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadRefTypeObjectInstance_m12387_ParameterInfos[] = 
{
	{"reader", 0, 134222201, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222202, 0, &Int64_t1092_1_0_2},
	{"value", 2, 134222203, 0, &Object_t_1_0_2},
	{"info", 3, 134222204, 0, &SerializationInfo_t1382_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadRefTypeObjectInstance(System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadRefTypeObjectInstance_m12387_MethodInfo = 
{
	"ReadRefTypeObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadRefTypeObjectInstance_m12387/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516_SerializationInfoU26_t3060/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadRefTypeObjectInstance_m12387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType TypeMetadata_t2366_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1382_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadObjectContent_m12388_ParameterInfos[] = 
{
	{"reader", 0, 134222205, 0, &BinaryReader_t2182_0_0_0},
	{"metadata", 1, 134222206, 0, &TypeMetadata_t2366_0_0_0},
	{"objectId", 2, 134222207, 0, &Int64_t1092_0_0_0},
	{"objectInstance", 3, 134222208, 0, &Object_t_1_0_2},
	{"info", 4, 134222209, 0, &SerializationInfo_t1382_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int64_t1092_ObjectU26_t1516_SerializationInfoU26_t3060 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectContent(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata,System.Int64,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectContent_m12388_MethodInfo = 
{
	"ReadObjectContent"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectContent_m12388/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int64_t1092_ObjectU26_t1516_SerializationInfoU26_t3060/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadObjectContent_m12388_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_RegisterObject_m12389_ParameterInfos[] = 
{
	{"objectId", 0, 134222210, 0, &Int64_t1092_0_0_0},
	{"objectInstance", 1, 134222211, 0, &Object_t_0_0_0},
	{"info", 2, 134222212, 0, &SerializationInfo_t1382_0_0_0},
	{"parentObjectId", 3, 134222213, 0, &Int64_t1092_0_0_0},
	{"parentObjectMemeber", 4, 134222214, 0, &MemberInfo_t_0_0_0},
	{"indices", 5, 134222215, 0, &Int32U5BU5D_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092_Object_t_Object_t_Int64_t1092_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RegisterObject(System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.Int64,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RegisterObject_m12389_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&ObjectReader_RegisterObject_m12389/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092_Object_t_Object_t_Int64_t1092_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_RegisterObject_m12389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadStringIntance_m12390_ParameterInfos[] = 
{
	{"reader", 0, 134222216, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222217, 0, &Int64_t1092_1_0_2},
	{"value", 2, 134222218, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadStringIntance(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadStringIntance_m12390_MethodInfo = 
{
	"ReadStringIntance"/* name */
	, (methodPointerType)&ObjectReader_ReadStringIntance_m12390/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadStringIntance_m12390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadGenericArray_m12391_ParameterInfos[] = 
{
	{"reader", 0, 134222219, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222220, 0, &Int64_t1092_1_0_2},
	{"val", 2, 134222221, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadGenericArray(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadGenericArray_m12391_MethodInfo = 
{
	"ReadGenericArray"/* name */
	, (methodPointerType)&ObjectReader_ReadGenericArray_m12391/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadGenericArray_m12391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadBoxedPrimitiveTypeValue_m12392_ParameterInfos[] = 
{
	{"reader", 0, 134222222, 0, &BinaryReader_t2182_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadBoxedPrimitiveTypeValue(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadBoxedPrimitiveTypeValue_m12392_MethodInfo = 
{
	"ReadBoxedPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadBoxedPrimitiveTypeValue_m12392/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadBoxedPrimitiveTypeValue_m12392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadArrayOfPrimitiveType_m12393_ParameterInfos[] = 
{
	{"reader", 0, 134222223, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222224, 0, &Int64_t1092_1_0_2},
	{"val", 2, 134222225, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfPrimitiveType(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfPrimitiveType_m12393_MethodInfo = 
{
	"ReadArrayOfPrimitiveType"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfPrimitiveType_m12393/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadArrayOfPrimitiveType_m12393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_BlockRead_m12394_ParameterInfos[] = 
{
	{"reader", 0, 134222226, 0, &BinaryReader_t2182_0_0_0},
	{"array", 1, 134222227, 0, &Array_t_0_0_0},
	{"dataSize", 2, 134222228, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::BlockRead(System.IO.BinaryReader,System.Array,System.Int32)
extern const MethodInfo ObjectReader_BlockRead_m12394_MethodInfo = 
{
	"BlockRead"/* name */
	, (methodPointerType)&ObjectReader_BlockRead_m12394/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127/* invoker_method */
	, ObjectReader_t2369_ObjectReader_BlockRead_m12394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadArrayOfObject_m12395_ParameterInfos[] = 
{
	{"reader", 0, 134222229, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222230, 0, &Int64_t1092_1_0_2},
	{"array", 2, 134222231, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfObject(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfObject_m12395_MethodInfo = 
{
	"ReadArrayOfObject"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfObject_m12395/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadArrayOfObject_m12395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadArrayOfString_m12396_ParameterInfos[] = 
{
	{"reader", 0, 134222232, 0, &BinaryReader_t2182_0_0_0},
	{"objectId", 1, 134222233, 0, &Int64_t1092_1_0_2},
	{"array", 2, 134222234, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfString(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfString_m12396_MethodInfo = 
{
	"ReadArrayOfString"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfString_m12396/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadArrayOfString_m12396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int64_t1092_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadSimpleArray_m12397_ParameterInfos[] = 
{
	{"reader", 0, 134222235, 0, &BinaryReader_t2182_0_0_0},
	{"elementType", 1, 134222236, 0, &Type_t_0_0_0},
	{"objectId", 2, 134222237, 0, &Int64_t1092_1_0_2},
	{"val", 3, 134222238, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int64U26_t2703_ObjectU26_t1516 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadSimpleArray(System.IO.BinaryReader,System.Type,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadSimpleArray_m12397_MethodInfo = 
{
	"ReadSimpleArray"/* name */
	, (methodPointerType)&ObjectReader_ReadSimpleArray_m12397/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int64U26_t2703_ObjectU26_t1516/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadSimpleArray_m12397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadTypeMetadata_m12398_ParameterInfos[] = 
{
	{"reader", 0, 134222239, 0, &BinaryReader_t2182_0_0_0},
	{"isRuntimeObject", 1, 134222240, 0, &Boolean_t169_0_0_0},
	{"hasTypeInfo", 2, 134222241, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadTypeMetadata(System.IO.BinaryReader,System.Boolean,System.Boolean)
extern const MethodInfo ObjectReader_ReadTypeMetadata_m12398_MethodInfo = 
{
	"ReadTypeMetadata"/* name */
	, (methodPointerType)&ObjectReader_ReadTypeMetadata_m12398/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &TypeMetadata_t2366_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadTypeMetadata_m12398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadValue_m12399_ParameterInfos[] = 
{
	{"reader", 0, 134222242, 0, &BinaryReader_t2182_0_0_0},
	{"parentObject", 1, 134222243, 0, &Object_t_0_0_0},
	{"parentObjectId", 2, 134222244, 0, &Int64_t1092_0_0_0},
	{"info", 3, 134222245, 0, &SerializationInfo_t1382_0_0_0},
	{"valueType", 4, 134222246, 0, &Type_t_0_0_0},
	{"fieldName", 5, 134222247, 0, &String_t_0_0_0},
	{"memberInfo", 6, 134222248, 0, &MemberInfo_t_0_0_0},
	{"indices", 7, 134222249, 0, &Int32U5BU5D_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int64_t1092_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadValue(System.IO.BinaryReader,System.Object,System.Int64,System.Runtime.Serialization.SerializationInfo,System.Type,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_ReadValue_m12399_MethodInfo = 
{
	"ReadValue"/* name */
	, (methodPointerType)&ObjectReader_ReadValue_m12399/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int64_t1092_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadValue_m12399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_SetObjectValue_m12400_ParameterInfos[] = 
{
	{"parentObject", 0, 134222250, 0, &Object_t_0_0_0},
	{"fieldName", 1, 134222251, 0, &String_t_0_0_0},
	{"memberInfo", 2, 134222252, 0, &MemberInfo_t_0_0_0},
	{"info", 3, 134222253, 0, &SerializationInfo_t1382_0_0_0},
	{"value", 4, 134222254, 0, &Object_t_0_0_0},
	{"valueType", 5, 134222255, 0, &Type_t_0_0_0},
	{"indices", 6, 134222256, 0, &Int32U5BU5D_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::SetObjectValue(System.Object,System.String,System.Reflection.MemberInfo,System.Runtime.Serialization.SerializationInfo,System.Object,System.Type,System.Int32[])
extern const MethodInfo ObjectReader_SetObjectValue_m12400_MethodInfo = 
{
	"SetObjectValue"/* name */
	, (methodPointerType)&ObjectReader_SetObjectValue_m12400/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_SetObjectValue_m12400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_RecordFixup_m12401_ParameterInfos[] = 
{
	{"parentObjectId", 0, 134222257, 0, &Int64_t1092_0_0_0},
	{"childObjectId", 1, 134222258, 0, &Int64_t1092_0_0_0},
	{"parentObject", 2, 134222259, 0, &Object_t_0_0_0},
	{"info", 3, 134222260, 0, &SerializationInfo_t1382_0_0_0},
	{"fieldName", 4, 134222261, 0, &String_t_0_0_0},
	{"memberInfo", 5, 134222262, 0, &MemberInfo_t_0_0_0},
	{"indices", 6, 134222263, 0, &Int32U5BU5D_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092_Int64_t1092_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RecordFixup(System.Int64,System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RecordFixup_m12401_MethodInfo = 
{
	"RecordFixup"/* name */
	, (methodPointerType)&ObjectReader_RecordFixup_m12401/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092_Int64_t1092_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_RecordFixup_m12401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_GetDeserializationType_m12402_ParameterInfos[] = 
{
	{"assemblyId", 0, 134222264, 0, &Int64_t1092_0_0_0},
	{"className", 1, 134222265, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::GetDeserializationType(System.Int64,System.String)
extern const MethodInfo ObjectReader_GetDeserializationType_m12402_MethodInfo = 
{
	"GetDeserializationType"/* name */
	, (methodPointerType)&ObjectReader_GetDeserializationType_m12402/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1092_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_GetDeserializationType_m12402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType TypeTag_t2360_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadType_m12403_ParameterInfos[] = 
{
	{"reader", 0, 134222266, 0, &BinaryReader_t2182_0_0_0},
	{"code", 1, 134222267, 0, &TypeTag_t2360_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Byte_t449 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadType(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.TypeTag)
extern const MethodInfo ObjectReader_ReadType_m12403_MethodInfo = 
{
	"ReadType"/* name */
	, (methodPointerType)&ObjectReader_ReadType_m12403/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Byte_t449/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadType_m12403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t2182_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectReader_t2369_ObjectReader_ReadPrimitiveTypeValue_m12404_ParameterInfos[] = 
{
	{"reader", 0, 134222268, 0, &BinaryReader_t2182_0_0_0},
	{"type", 1, 134222269, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadPrimitiveTypeValue(System.IO.BinaryReader,System.Type)
extern const MethodInfo ObjectReader_ReadPrimitiveTypeValue_m12404_MethodInfo = 
{
	"ReadPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadPrimitiveTypeValue_m12404/* method */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2369_ObjectReader_ReadPrimitiveTypeValue_m12404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectReader_t2369_MethodInfos[] =
{
	&ObjectReader__ctor_m12378_MethodInfo,
	&ObjectReader_ReadObjectGraph_m12379_MethodInfo,
	&ObjectReader_ReadObjectGraph_m12380_MethodInfo,
	&ObjectReader_ReadNextObject_m12381_MethodInfo,
	&ObjectReader_ReadNextObject_m12382_MethodInfo,
	&ObjectReader_get_CurrentObject_m12383_MethodInfo,
	&ObjectReader_ReadObject_m12384_MethodInfo,
	&ObjectReader_ReadAssembly_m12385_MethodInfo,
	&ObjectReader_ReadObjectInstance_m12386_MethodInfo,
	&ObjectReader_ReadRefTypeObjectInstance_m12387_MethodInfo,
	&ObjectReader_ReadObjectContent_m12388_MethodInfo,
	&ObjectReader_RegisterObject_m12389_MethodInfo,
	&ObjectReader_ReadStringIntance_m12390_MethodInfo,
	&ObjectReader_ReadGenericArray_m12391_MethodInfo,
	&ObjectReader_ReadBoxedPrimitiveTypeValue_m12392_MethodInfo,
	&ObjectReader_ReadArrayOfPrimitiveType_m12393_MethodInfo,
	&ObjectReader_BlockRead_m12394_MethodInfo,
	&ObjectReader_ReadArrayOfObject_m12395_MethodInfo,
	&ObjectReader_ReadArrayOfString_m12396_MethodInfo,
	&ObjectReader_ReadSimpleArray_m12397_MethodInfo,
	&ObjectReader_ReadTypeMetadata_m12398_MethodInfo,
	&ObjectReader_ReadValue_m12399_MethodInfo,
	&ObjectReader_SetObjectValue_m12400_MethodInfo,
	&ObjectReader_RecordFixup_m12401_MethodInfo,
	&ObjectReader_GetDeserializationType_m12402_MethodInfo,
	&ObjectReader_ReadType_m12403_MethodInfo,
	&ObjectReader_ReadPrimitiveTypeValue_m12404_MethodInfo,
	NULL
};
extern const MethodInfo ObjectReader_get_CurrentObject_m12383_MethodInfo;
static const PropertyInfo ObjectReader_t2369____CurrentObject_PropertyInfo = 
{
	&ObjectReader_t2369_il2cpp_TypeInfo/* parent */
	, "CurrentObject"/* name */
	, &ObjectReader_get_CurrentObject_m12383_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectReader_t2369_PropertyInfos[] =
{
	&ObjectReader_t2369____CurrentObject_PropertyInfo,
	NULL
};
static const Il2CppType* ObjectReader_t2369_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t2366_0_0_0,
	&ArrayNullFiller_t2367_0_0_0,
};
static const Il2CppMethodReference ObjectReader_t2369_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ObjectReader_t2369_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectReader_t2369_1_0_0;
struct ObjectReader_t2369;
const Il2CppTypeDefinitionMetadata ObjectReader_t2369_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t2369_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t2369_VTable/* vtableMethods */
	, ObjectReader_t2369_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1728/* fieldStart */

};
TypeInfo ObjectReader_t2369_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ObjectReader_t2369_MethodInfos/* methods */
	, ObjectReader_t2369_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectReader_t2369_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t2369_0_0_0/* byval_arg */
	, &ObjectReader_t2369_1_0_0/* this_arg */
	, &ObjectReader_t2369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t2369)/* instance_size */
	, sizeof (ObjectReader_t2369)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t2370_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static const MethodInfo* FormatterAssemblyStyle_t2370_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterAssemblyStyle_t2370_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool FormatterAssemblyStyle_t2370_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t2370_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterAssemblyStyle_t2370_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t2370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t2370_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, FormatterAssemblyStyle_t2370_VTable/* vtableMethods */
	, FormatterAssemblyStyle_t2370_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1740/* fieldStart */

};
TypeInfo FormatterAssemblyStyle_t2370_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterAssemblyStyle_t2370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 570/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t2370_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t2370_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t2370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t2370)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t2370)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t2371_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static const MethodInfo* FormatterTypeStyle_t2371_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterTypeStyle_t2371_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool FormatterTypeStyle_t2371_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t2371_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterTypeStyle_t2371_0_0_0;
extern const Il2CppType FormatterTypeStyle_t2371_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t2371_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t2371_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, FormatterTypeStyle_t2371_VTable/* vtableMethods */
	, FormatterTypeStyle_t2371_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1743/* fieldStart */

};
TypeInfo FormatterTypeStyle_t2371_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterTypeStyle_t2371_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 571/* custom_attributes_cache */
	, &FormatterTypeStyle_t2371_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t2371_1_0_0/* this_arg */
	, &FormatterTypeStyle_t2371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t2371)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t2371)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t2372_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static const MethodInfo* TypeFilterLevel_t2372_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeFilterLevel_t2372_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TypeFilterLevel_t2372_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t2372_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilterLevel_t2372_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t2372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t2372_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TypeFilterLevel_t2372_VTable/* vtableMethods */
	, TypeFilterLevel_t2372_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1747/* fieldStart */

};
TypeInfo TypeFilterLevel_t2372_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, TypeFilterLevel_t2372_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 572/* custom_attributes_cache */
	, &TypeFilterLevel_t2372_0_0_0/* byval_arg */
	, &TypeFilterLevel_t2372_1_0_0/* this_arg */
	, &TypeFilterLevel_t2372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t2372)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t2372)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t2373_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
extern const MethodInfo FormatterConverter__ctor_m12405_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatterConverter__ctor_m12405/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_Convert_m12406_ParameterInfos[] = 
{
	{"value", 0, 134222271, 0, &Object_t_0_0_0},
	{"type", 1, 134222272, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo FormatterConverter_Convert_m12406_MethodInfo = 
{
	"Convert"/* name */
	, (methodPointerType)&FormatterConverter_Convert_m12406/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_Convert_m12406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToBoolean_m12407_ParameterInfos[] = 
{
	{"value", 0, 134222273, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
extern const MethodInfo FormatterConverter_ToBoolean_m12407_MethodInfo = 
{
	"ToBoolean"/* name */
	, (methodPointerType)&FormatterConverter_ToBoolean_m12407/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToBoolean_m12407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToInt16_m12408_ParameterInfos[] = 
{
	{"value", 0, 134222274, 0, &Object_t_0_0_0},
};
extern const Il2CppType Int16_t534_0_0_0;
extern void* RuntimeInvoker_Int16_t534_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
extern const MethodInfo FormatterConverter_ToInt16_m12408_MethodInfo = 
{
	"ToInt16"/* name */
	, (methodPointerType)&FormatterConverter_ToInt16_m12408/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t534_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t534_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToInt16_m12408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToInt32_m12409_ParameterInfos[] = 
{
	{"value", 0, 134222275, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
extern const MethodInfo FormatterConverter_ToInt32_m12409_MethodInfo = 
{
	"ToInt32"/* name */
	, (methodPointerType)&FormatterConverter_ToInt32_m12409/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToInt32_m12409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToInt64_m12410_ParameterInfos[] = 
{
	{"value", 0, 134222276, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
extern const MethodInfo FormatterConverter_ToInt64_m12410_MethodInfo = 
{
	"ToInt64"/* name */
	, (methodPointerType)&FormatterConverter_ToInt64_m12410/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToInt64_m12410_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToString_m12411_ParameterInfos[] = 
{
	{"value", 0, 134222277, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
extern const MethodInfo FormatterConverter_ToString_m12411_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&FormatterConverter_ToString_m12411/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToString_m12411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2373_FormatterConverter_ToUInt32_m12412_ParameterInfos[] = 
{
	{"value", 0, 134222278, 0, &Object_t_0_0_0},
};
extern const Il2CppType UInt32_t1075_0_0_0;
extern void* RuntimeInvoker_UInt32_t1075_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.Runtime.Serialization.FormatterConverter::ToUInt32(System.Object)
extern const MethodInfo FormatterConverter_ToUInt32_m12412_MethodInfo = 
{
	"ToUInt32"/* name */
	, (methodPointerType)&FormatterConverter_ToUInt32_m12412/* method */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Object_t/* invoker_method */
	, FormatterConverter_t2373_FormatterConverter_ToUInt32_m12412_ParameterInfos/* parameters */
	, 574/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterConverter_t2373_MethodInfos[] =
{
	&FormatterConverter__ctor_m12405_MethodInfo,
	&FormatterConverter_Convert_m12406_MethodInfo,
	&FormatterConverter_ToBoolean_m12407_MethodInfo,
	&FormatterConverter_ToInt16_m12408_MethodInfo,
	&FormatterConverter_ToInt32_m12409_MethodInfo,
	&FormatterConverter_ToInt64_m12410_MethodInfo,
	&FormatterConverter_ToString_m12411_MethodInfo,
	&FormatterConverter_ToUInt32_m12412_MethodInfo,
	NULL
};
extern const MethodInfo FormatterConverter_Convert_m12406_MethodInfo;
extern const MethodInfo FormatterConverter_ToBoolean_m12407_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt16_m12408_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt32_m12409_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt64_m12410_MethodInfo;
extern const MethodInfo FormatterConverter_ToString_m12411_MethodInfo;
extern const MethodInfo FormatterConverter_ToUInt32_m12412_MethodInfo;
static const Il2CppMethodReference FormatterConverter_t2373_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&FormatterConverter_Convert_m12406_MethodInfo,
	&FormatterConverter_ToBoolean_m12407_MethodInfo,
	&FormatterConverter_ToInt16_m12408_MethodInfo,
	&FormatterConverter_ToInt32_m12409_MethodInfo,
	&FormatterConverter_ToInt64_m12410_MethodInfo,
	&FormatterConverter_ToString_m12411_MethodInfo,
	&FormatterConverter_ToUInt32_m12412_MethodInfo,
};
static bool FormatterConverter_t2373_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormatterConverter_t2390_0_0_0;
static const Il2CppType* FormatterConverter_t2373_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t2390_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t2373_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t2390_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterConverter_t2373_0_0_0;
extern const Il2CppType FormatterConverter_t2373_1_0_0;
struct FormatterConverter_t2373;
const Il2CppTypeDefinitionMetadata FormatterConverter_t2373_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t2373_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t2373_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t2373_VTable/* vtableMethods */
	, FormatterConverter_t2373_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterConverter_t2373_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterConverter_t2373_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterConverter_t2373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 573/* custom_attributes_cache */
	, &FormatterConverter_t2373_0_0_0/* byval_arg */
	, &FormatterConverter_t2373_1_0_0/* this_arg */
	, &FormatterConverter_t2373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t2373)/* instance_size */
	, sizeof (FormatterConverter_t2373)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t2374_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t2374_FormatterServices_GetUninitializedObject_m12413_ParameterInfos[] = 
{
	{"type", 0, 134222279, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetUninitializedObject_m12413_MethodInfo = 
{
	"GetUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetUninitializedObject_m12413/* method */
	, &FormatterServices_t2374_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2374_FormatterServices_GetUninitializedObject_m12413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t2374_FormatterServices_GetSafeUninitializedObject_m12414_ParameterInfos[] = 
{
	{"type", 0, 134222280, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetSafeUninitializedObject_m12414_MethodInfo = 
{
	"GetSafeUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetSafeUninitializedObject_m12414/* method */
	, &FormatterServices_t2374_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2374_FormatterServices_GetSafeUninitializedObject_m12414_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterServices_t2374_MethodInfos[] =
{
	&FormatterServices_GetUninitializedObject_m12413_MethodInfo,
	&FormatterServices_GetSafeUninitializedObject_m12414_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatterServices_t2374_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool FormatterServices_t2374_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterServices_t2374_0_0_0;
extern const Il2CppType FormatterServices_t2374_1_0_0;
struct FormatterServices_t2374;
const Il2CppTypeDefinitionMetadata FormatterServices_t2374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t2374_VTable/* vtableMethods */
	, FormatterServices_t2374_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterServices_t2374_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterServices_t2374_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterServices_t2374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 575/* custom_attributes_cache */
	, &FormatterServices_t2374_0_0_0/* byval_arg */
	, &FormatterServices_t2374_1_0_0/* this_arg */
	, &FormatterServices_t2374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t2374)/* instance_size */
	, sizeof (FormatterServices_t2374)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t1609_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IDeserializationCallback_t1609_IDeserializationCallback_OnDeserialization_m14578_ParameterInfos[] = 
{
	{"sender", 0, 134222281, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object)
extern const MethodInfo IDeserializationCallback_OnDeserialization_m14578_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &IDeserializationCallback_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IDeserializationCallback_t1609_IDeserializationCallback_OnDeserialization_m14578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IDeserializationCallback_t1609_MethodInfos[] =
{
	&IDeserializationCallback_OnDeserialization_m14578_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDeserializationCallback_t1609_0_0_0;
extern const Il2CppType IDeserializationCallback_t1609_1_0_0;
struct IDeserializationCallback_t1609;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t1609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IDeserializationCallback_t1609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IDeserializationCallback_t1609_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IDeserializationCallback_t1609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 576/* custom_attributes_cache */
	, &IDeserializationCallback_t1609_0_0_0/* byval_arg */
	, &IDeserializationCallback_t1609_1_0_0/* this_arg */
	, &IDeserializationCallback_t1609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t2688_il2cpp_TypeInfo;
static const MethodInfo* IFormatter_t2688_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatter_t2688_1_0_0;
struct IFormatter_t2688;
const Il2CppTypeDefinitionMetadata IFormatter_t2688_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatter_t2688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatter_t2688_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatter_t2688_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 577/* custom_attributes_cache */
	, &IFormatter_t2688_0_0_0/* byval_arg */
	, &IFormatter_t2688_1_0_0/* this_arg */
	, &IFormatter_t2688_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t2390_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_Convert_m14579_ParameterInfos[] = 
{
	{"value", 0, 134222282, 0, &Object_t_0_0_0},
	{"type", 1, 134222283, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IFormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo IFormatterConverter_Convert_m14579_MethodInfo = 
{
	"Convert"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_Convert_m14579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToBoolean_m14580_ParameterInfos[] = 
{
	{"value", 0, 134222284, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.IFormatterConverter::ToBoolean(System.Object)
extern const MethodInfo IFormatterConverter_ToBoolean_m14580_MethodInfo = 
{
	"ToBoolean"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToBoolean_m14580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToInt16_m14581_ParameterInfos[] = 
{
	{"value", 0, 134222285, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int16_t534_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.IFormatterConverter::ToInt16(System.Object)
extern const MethodInfo IFormatterConverter_ToInt16_m14581_MethodInfo = 
{
	"ToInt16"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t534_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t534_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToInt16_m14581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToInt32_m14582_ParameterInfos[] = 
{
	{"value", 0, 134222286, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.IFormatterConverter::ToInt32(System.Object)
extern const MethodInfo IFormatterConverter_ToInt32_m14582_MethodInfo = 
{
	"ToInt32"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToInt32_m14582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToInt64_m14583_ParameterInfos[] = 
{
	{"value", 0, 134222287, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.IFormatterConverter::ToInt64(System.Object)
extern const MethodInfo IFormatterConverter_ToInt64_m14583_MethodInfo = 
{
	"ToInt64"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToInt64_m14583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToString_m14584_ParameterInfos[] = 
{
	{"value", 0, 134222288, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.IFormatterConverter::ToString(System.Object)
extern const MethodInfo IFormatterConverter_ToString_m14584_MethodInfo = 
{
	"ToString"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToString_m14584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2390_IFormatterConverter_ToUInt32_m14585_ParameterInfos[] = 
{
	{"value", 0, 134222289, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1075_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.Runtime.Serialization.IFormatterConverter::ToUInt32(System.Object)
extern const MethodInfo IFormatterConverter_ToUInt32_m14585_MethodInfo = 
{
	"ToUInt32"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Object_t/* invoker_method */
	, IFormatterConverter_t2390_IFormatterConverter_ToUInt32_m14585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatterConverter_t2390_MethodInfos[] =
{
	&IFormatterConverter_Convert_m14579_MethodInfo,
	&IFormatterConverter_ToBoolean_m14580_MethodInfo,
	&IFormatterConverter_ToInt16_m14581_MethodInfo,
	&IFormatterConverter_ToInt32_m14582_MethodInfo,
	&IFormatterConverter_ToInt64_m14583_MethodInfo,
	&IFormatterConverter_ToString_m14584_MethodInfo,
	&IFormatterConverter_ToUInt32_m14585_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatterConverter_t2390_1_0_0;
struct IFormatterConverter_t2390;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t2390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatterConverter_t2390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatterConverter_t2390_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatterConverter_t2390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 578/* custom_attributes_cache */
	, &IFormatterConverter_t2390_0_0_0/* byval_arg */
	, &IFormatterConverter_t2390_1_0_0/* this_arg */
	, &IFormatterConverter_t2390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t2615_il2cpp_TypeInfo;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo IObjectReference_t2615_IObjectReference_GetRealObject_m14586_ParameterInfos[] = 
{
	{"context", 0, 134222290, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IObjectReference::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IObjectReference_GetRealObject_m14586_MethodInfo = 
{
	"GetRealObject"/* name */
	, NULL/* method */
	, &IObjectReference_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1383/* invoker_method */
	, IObjectReference_t2615_IObjectReference_GetRealObject_m14586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IObjectReference_t2615_MethodInfos[] =
{
	&IObjectReference_GetRealObject_m14586_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IObjectReference_t2615_1_0_0;
struct IObjectReference_t2615;
const Il2CppTypeDefinitionMetadata IObjectReference_t2615_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IObjectReference_t2615_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IObjectReference_t2615_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IObjectReference_t2615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 579/* custom_attributes_cache */
	, &IObjectReference_t2615_0_0_0/* byval_arg */
	, &IObjectReference_t2615_1_0_0/* this_arg */
	, &IObjectReference_t2615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
