﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t203;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t202;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct IEnumerable_1_t4042;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct IEnumerator_1_t4043;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct ICollection_1_t4044;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct ReadOnlyCollection_1_t3174;
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t3172;
// System.Predicate`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Predicate_1_t3175;
// System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Comparison_1_t3177;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_31.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m1958(__this, method) (( void (*) (List_1_t203 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15785(__this, ___collection, method) (( void (*) (List_1_t203 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor(System.Int32)
#define List_1__ctor_m15786(__this, ___capacity, method) (( void (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::.cctor()
#define List_1__cctor_m15787(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15788(__this, method) (( Object_t* (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15789(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t203 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15790(__this, method) (( Object_t * (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15791(__this, ___item, method) (( int32_t (*) (List_1_t203 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15792(__this, ___item, method) (( bool (*) (List_1_t203 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15793(__this, ___item, method) (( int32_t (*) (List_1_t203 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15794(__this, ___index, ___item, method) (( void (*) (List_1_t203 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15795(__this, ___item, method) (( void (*) (List_1_t203 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15796(__this, method) (( bool (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15797(__this, method) (( bool (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15798(__this, method) (( Object_t * (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15799(__this, method) (( bool (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15800(__this, method) (( bool (*) (List_1_t203 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15801(__this, ___index, method) (( Object_t * (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15802(__this, ___index, ___value, method) (( void (*) (List_1_t203 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Add(T)
#define List_1_Add_m15803(__this, ___item, method) (( void (*) (List_1_t203 *, Entry_t202 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15804(__this, ___newCount, method) (( void (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15805(__this, ___collection, method) (( void (*) (List_1_t203 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15806(__this, ___enumerable, method) (( void (*) (List_1_t203 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15807(__this, ___collection, method) (( void (*) (List_1_t203 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::AsReadOnly()
#define List_1_AsReadOnly_m15808(__this, method) (( ReadOnlyCollection_1_t3174 * (*) (List_1_t203 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Clear()
#define List_1_Clear_m15809(__this, method) (( void (*) (List_1_t203 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Contains(T)
#define List_1_Contains_m15810(__this, ___item, method) (( bool (*) (List_1_t203 *, Entry_t202 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15811(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t203 *, EntryU5BU5D_t3172*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Find(System.Predicate`1<T>)
#define List_1_Find_m15812(__this, ___match, method) (( Entry_t202 * (*) (List_1_t203 *, Predicate_1_t3175 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15813(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3175 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15814(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t203 *, int32_t, int32_t, Predicate_1_t3175 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::GetEnumerator()
#define List_1_GetEnumerator_m15815(__this, method) (( Enumerator_t3176  (*) (List_1_t203 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::IndexOf(T)
#define List_1_IndexOf_m15816(__this, ___item, method) (( int32_t (*) (List_1_t203 *, Entry_t202 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15817(__this, ___start, ___delta, method) (( void (*) (List_1_t203 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15818(__this, ___index, method) (( void (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Insert(System.Int32,T)
#define List_1_Insert_m15819(__this, ___index, ___item, method) (( void (*) (List_1_t203 *, int32_t, Entry_t202 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15820(__this, ___collection, method) (( void (*) (List_1_t203 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Remove(T)
#define List_1_Remove_m15821(__this, ___item, method) (( bool (*) (List_1_t203 *, Entry_t202 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15822(__this, ___match, method) (( int32_t (*) (List_1_t203 *, Predicate_1_t3175 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15823(__this, ___index, method) (( void (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Reverse()
#define List_1_Reverse_m15824(__this, method) (( void (*) (List_1_t203 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Sort()
#define List_1_Sort_m15825(__this, method) (( void (*) (List_1_t203 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15826(__this, ___comparison, method) (( void (*) (List_1_t203 *, Comparison_1_t3177 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::ToArray()
#define List_1_ToArray_m15827(__this, method) (( EntryU5BU5D_t3172* (*) (List_1_t203 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::TrimExcess()
#define List_1_TrimExcess_m15828(__this, method) (( void (*) (List_1_t203 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::get_Capacity()
#define List_1_get_Capacity_m15829(__this, method) (( int32_t (*) (List_1_t203 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15830(__this, ___value, method) (( void (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::get_Count()
#define List_1_get_Count_m15831(__this, method) (( int32_t (*) (List_1_t203 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::get_Item(System.Int32)
#define List_1_get_Item_m15832(__this, ___index, method) (( Entry_t202 * (*) (List_1_t203 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::set_Item(System.Int32,T)
#define List_1_set_Item_m15833(__this, ___index, ___value, method) (( void (*) (List_1_t203 *, int32_t, Entry_t202 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
