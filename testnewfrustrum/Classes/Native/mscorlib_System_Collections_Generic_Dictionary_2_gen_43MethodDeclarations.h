﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1336;
// System.Collections.Generic.ICollection`1<UnityEngine.Event>
struct ICollection_1_t4398;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t4399;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t317;
struct Event_t317_marshaled;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t3892;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t3893;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
struct IEqualityComparer_1_t3889;
// System.Collections.Generic.IDictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t4400;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t4401;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t4402;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_43.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__39.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_33MethodDeclarations.h"
#define Dictionary_2__ctor_m26604(__this, method) (( void (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2__ctor_m16521_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26605(__this, ___comparer, method) (( void (*) (Dictionary_2_t1336 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16522_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m26606(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1336 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16523_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m26607(__this, ___capacity, method) (( void (*) (Dictionary_2_t1336 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16524_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26608(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1336 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16525_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m26609(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1336 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m16526_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26610(__this, method) (( Object_t* (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26611(__this, method) (( Object_t* (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26612(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16529_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26613(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1336 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16530_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m26614(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1336 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16531_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m26615(__this, ___key, method) (( bool (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16532_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m26616(__this, ___key, method) (( void (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16533_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26617(__this, method) (( bool (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26618(__this, method) (( Object_t * (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26619(__this, method) (( bool (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26620(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1336 *, KeyValuePair_2_t3891 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26621(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1336 *, KeyValuePair_2_t3891 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26622(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1336 *, KeyValuePair_2U5BU5D_t4401*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26623(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1336 *, KeyValuePair_2_t3891 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26624(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1336 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16541_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26625(__this, method) (( Object_t * (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26626(__this, method) (( Object_t* (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26627(__this, method) (( Object_t * (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Count()
#define Dictionary_2_get_Count_m26628(__this, method) (( int32_t (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_get_Count_m16545_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
#define Dictionary_2_get_Item_m26629(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1336 *, Event_t317 *, const MethodInfo*))Dictionary_2_get_Item_m16546_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m26630(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1336 *, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m16547_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m26631(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1336 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16548_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m26632(__this, ___size, method) (( void (*) (Dictionary_2_t1336 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16549_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m26633(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1336 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16550_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m26634(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3891  (*) (Object_t * /* static, unused */, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m16551_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m26635(__this /* static, unused */, ___key, ___value, method) (( Event_t317 * (*) (Object_t * /* static, unused */, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m16552_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m26636(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m16553_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m26637(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1336 *, KeyValuePair_2U5BU5D_t4401*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16554_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Resize()
#define Dictionary_2_Resize_m26638(__this, method) (( void (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_Resize_m16555_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
#define Dictionary_2_Add_m26639(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1336 *, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_Add_m16556_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Clear()
#define Dictionary_2_Clear_m26640(__this, method) (( void (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_Clear_m16557_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m26641(__this, ___key, method) (( bool (*) (Dictionary_2_t1336 *, Event_t317 *, const MethodInfo*))Dictionary_2_ContainsKey_m16558_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m26642(__this, ___value, method) (( bool (*) (Dictionary_2_t1336 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m16559_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m26643(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1336 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m16560_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m26644(__this, ___sender, method) (( void (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16561_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
#define Dictionary_2_Remove_m26645(__this, ___key, method) (( bool (*) (Dictionary_2_t1336 *, Event_t317 *, const MethodInfo*))Dictionary_2_Remove_m16562_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m26646(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1336 *, Event_t317 *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m16563_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Keys()
#define Dictionary_2_get_Keys_m26647(__this, method) (( KeyCollection_t3892 * (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_get_Keys_m16564_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Values()
#define Dictionary_2_get_Values_m26648(__this, method) (( ValueCollection_t3893 * (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_get_Values_m16565_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m26649(__this, ___key, method) (( Event_t317 * (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16566_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m26650(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1336 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16567_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m26651(__this, ___pair, method) (( bool (*) (Dictionary_2_t1336 *, KeyValuePair_2_t3891 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16568_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m26652(__this, method) (( Enumerator_t3894  (*) (Dictionary_2_t1336 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16569_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m26653(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Event_t317 *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16570_gshared)(__this /* static, unused */, ___key, ___value, method)
