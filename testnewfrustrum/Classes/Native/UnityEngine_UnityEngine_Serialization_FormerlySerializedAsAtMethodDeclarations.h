﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t492;
// System.String
struct String_t;

// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C" void FormerlySerializedAsAttribute__ctor_m2456 (FormerlySerializedAsAttribute_t492 * __this, String_t* ___oldName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
