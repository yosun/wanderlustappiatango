﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
struct Enumerator_t795;
// System.Object
struct Object_t;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t761;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t575;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18475(__this, ___l, method) (( void (*) (Enumerator_t795 *, List_1_t575 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18476(__this, method) (( Object_t * (*) (Enumerator_t795 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::Dispose()
#define Enumerator_Dispose_m18477(__this, method) (( void (*) (Enumerator_t795 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::VerifyState()
#define Enumerator_VerifyState_m18478(__this, method) (( void (*) (Enumerator_t795 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4318(__this, method) (( bool (*) (Enumerator_t795 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::get_Current()
#define Enumerator_get_Current_m4317(__this, method) (( Object_t * (*) (Enumerator_t795 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
