﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t3165;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m15690_gshared (DefaultComparer_t3165 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15690(__this, method) (( void (*) (DefaultComparer_t3165 *, const MethodInfo*))DefaultComparer__ctor_m15690_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15691_gshared (DefaultComparer_t3165 * __this, RaycastResult_t231  ___x, RaycastResult_t231  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m15691(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3165 *, RaycastResult_t231 , RaycastResult_t231 , const MethodInfo*))DefaultComparer_Compare_m15691_gshared)(__this, ___x, ___y, method)
