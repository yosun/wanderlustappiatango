﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t1143;
struct YieldInstruction_t1143_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m6329 (YieldInstruction_t1143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t1143_marshal(const YieldInstruction_t1143& unmarshaled, YieldInstruction_t1143_marshaled& marshaled);
void YieldInstruction_t1143_marshal_back(const YieldInstruction_t1143_marshaled& marshaled, YieldInstruction_t1143& unmarshaled);
void YieldInstruction_t1143_marshal_cleanup(YieldInstruction_t1143_marshaled& marshaled);
