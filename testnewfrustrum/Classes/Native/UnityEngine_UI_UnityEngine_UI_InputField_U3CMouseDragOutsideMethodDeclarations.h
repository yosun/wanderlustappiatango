﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3
struct U3CMouseDragOutsideRectU3Ec__Iterator3_t310;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::.ctor()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3__ctor_m1254 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1255 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1256 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::MoveNext()
extern "C" bool U3CMouseDragOutsideRectU3Ec__Iterator3_MoveNext_m1257 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::Dispose()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m1258 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::Reset()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m1259 (U3CMouseDragOutsideRectU3Ec__Iterator3_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
