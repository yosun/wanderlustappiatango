﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct  Predicate_1_t3163  : public MulticastDelegate_t307
{
};
