﻿#pragma once
#include <stdint.h>
// System.Boolean[]
struct BooleanU5BU5D_t1882;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Reflection.ParameterModifier
struct  ParameterModifier_t2260 
{
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t1882* ____byref_0;
};
// Native definition for marshalling of: System.Reflection.ParameterModifier
struct ParameterModifier_t2260_marshaled
{
	int32_t* ____byref_0;
};
