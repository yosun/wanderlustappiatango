﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t162;

// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
extern "C" void CompilerGeneratedAttribute__ctor_m511 (CompilerGeneratedAttribute_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
