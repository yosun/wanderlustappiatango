﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SignatureDescription
struct SignatureDescription_t2427;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.SignatureDescription::.ctor()
extern "C" void SignatureDescription__ctor_m12772 (SignatureDescription_t2427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DeformatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m12773 (SignatureDescription_t2427 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DigestAlgorithm(System.String)
extern "C" void SignatureDescription_set_DigestAlgorithm_m12774 (SignatureDescription_t2427 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_FormatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_FormatterAlgorithm_m12775 (SignatureDescription_t2427 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_KeyAlgorithm(System.String)
extern "C" void SignatureDescription_set_KeyAlgorithm_m12776 (SignatureDescription_t2427 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
