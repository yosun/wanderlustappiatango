﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
struct ReadOnlyCollection_1_t3690;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.UInt16>
struct IList_1_t3689;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.UInt16[]
struct UInt16U5BU5D_t1060;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4180;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m23827_gshared (ReadOnlyCollection_1_t3690 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m23827(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3690 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m23827_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23828_gshared (ReadOnlyCollection_1_t3690 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23828(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3690 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23828_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23829_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23829(__this, method) (( void (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23829_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23830_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23830(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23830_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23831_gshared (ReadOnlyCollection_1_t3690 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23831(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23831_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23832_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23832(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23832_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23833_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23833(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t3690 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23833_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23834_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23834(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23834_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23835_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23835(__this, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23835_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23836_gshared (ReadOnlyCollection_1_t3690 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23836(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3690 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23836_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23837_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23837(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23837_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m23838_gshared (ReadOnlyCollection_1_t3690 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23838(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3690 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m23838_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23839_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23839(__this, method) (( void (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m23839_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m23840_gshared (ReadOnlyCollection_1_t3690 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23840(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m23840_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23841_gshared (ReadOnlyCollection_1_t3690 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23841(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3690 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23841_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23842_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23842(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m23842_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23843_gshared (ReadOnlyCollection_1_t3690 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23843(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3690 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m23843_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23844_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23844(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23844_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23845_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23845(__this, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23845_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23846_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23846(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23846_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23847_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23847(__this, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23847_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23848_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23848(__this, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23848_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m23849_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23849(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3690 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m23849_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23850_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23850(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3690 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m23850_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m23851_gshared (ReadOnlyCollection_1_t3690 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m23851(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3690 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m23851_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m23852_gshared (ReadOnlyCollection_1_t3690 * __this, UInt16U5BU5D_t1060* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m23852(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3690 *, UInt16U5BU5D_t1060*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m23852_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m23853_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m23853(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m23853_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m23854_gshared (ReadOnlyCollection_1_t3690 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m23854(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3690 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m23854_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m23855_gshared (ReadOnlyCollection_1_t3690 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m23855(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3690 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m23855_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_get_Item_m23856_gshared (ReadOnlyCollection_1_t3690 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m23856(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t3690 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m23856_gshared)(__this, ___index, method)
