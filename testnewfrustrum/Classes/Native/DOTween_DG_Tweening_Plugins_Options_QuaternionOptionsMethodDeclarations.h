﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t971;
struct QuaternionOptions_t971_marshaled;

void QuaternionOptions_t971_marshal(const QuaternionOptions_t971& unmarshaled, QuaternionOptions_t971_marshaled& marshaled);
void QuaternionOptions_t971_marshal_back(const QuaternionOptions_t971_marshaled& marshaled, QuaternionOptions_t971& unmarshaled);
void QuaternionOptions_t971_marshal_cleanup(QuaternionOptions_t971_marshaled& marshaled);
