﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t4010;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m27678_gshared (DefaultComparer_t4010 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27678(__this, method) (( void (*) (DefaultComparer_t4010 *, const MethodInfo*))DefaultComparer__ctor_m27678_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m27679_gshared (DefaultComparer_t4010 * __this, DateTime_t111  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m27679(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4010 *, DateTime_t111 , const MethodInfo*))DefaultComparer_GetHashCode_m27679_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m27680_gshared (DefaultComparer_t4010 * __this, DateTime_t111  ___x, DateTime_t111  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m27680(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4010 *, DateTime_t111 , DateTime_t111 , const MethodInfo*))DefaultComparer_Equals_m27680_gshared)(__this, ___x, ___y, method)
