﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt16>
struct InternalEnumerator_1_t3104;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14873_gshared (InternalEnumerator_1_t3104 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14873(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3104 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14873_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14875_gshared (InternalEnumerator_1_t3104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14875(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3104 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14875_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14877_gshared (InternalEnumerator_1_t3104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14877(__this, method) (( void (*) (InternalEnumerator_1_t3104 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14877_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14879_gshared (InternalEnumerator_1_t3104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14879(__this, method) (( bool (*) (InternalEnumerator_1_t3104 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14879_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m14881_gshared (InternalEnumerator_1_t3104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14881(__this, method) (( uint16_t (*) (InternalEnumerator_1_t3104 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14881_gshared)(__this, method)
