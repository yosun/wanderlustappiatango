﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t935;
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
struct  U3CWaitForKillU3Ed__4_t938  : public Object_t
{
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::<>2__current
	Object_t * ___U3CU3E2__current_0;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::<>1__state
	int32_t ___U3CU3E1__state_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::t
	Tween_t934 * ___t_2;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::<>4__this
	DOTweenComponent_t935 * ___U3CU3E4__this_3;
};
