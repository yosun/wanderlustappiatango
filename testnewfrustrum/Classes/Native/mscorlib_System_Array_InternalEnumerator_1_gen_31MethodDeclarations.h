﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>
struct InternalEnumerator_1_t3440;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19770_gshared (InternalEnumerator_1_t3440 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19770(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3440 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19770_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19771_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19771(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19771_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19772_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19772(__this, method) (( void (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19772_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19773_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19773(__this, method) (( bool (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19773_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::get_Current()
extern "C" WordData_t647  InternalEnumerator_1_get_Current_m19774_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19774(__this, method) (( WordData_t647  (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19774_gshared)(__this, method)
