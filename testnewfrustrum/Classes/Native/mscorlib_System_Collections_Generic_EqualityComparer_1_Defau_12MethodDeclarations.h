﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t3872;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void DefaultComparer__ctor_m26493_gshared (DefaultComparer_t3872 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m26493(__this, method) (( void (*) (DefaultComparer_t3872 *, const MethodInfo*))DefaultComparer__ctor_m26493_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26494_gshared (DefaultComparer_t3872 * __this, KeyValuePair_2_t3254  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m26494(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3872 *, KeyValuePair_2_t3254 , const MethodInfo*))DefaultComparer_GetHashCode_m26494_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26495_gshared (DefaultComparer_t3872 * __this, KeyValuePair_2_t3254  ___x, KeyValuePair_2_t3254  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m26495(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3872 *, KeyValuePair_2_t3254 , KeyValuePair_2_t3254 , const MethodInfo*))DefaultComparer_Equals_m26495_gshared)(__this, ___x, ___y, method)
