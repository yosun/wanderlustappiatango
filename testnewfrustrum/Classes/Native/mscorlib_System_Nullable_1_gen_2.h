﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Nullable`1<System.Byte>
struct  Nullable_1_t3108 
{
	// T System.Nullable`1<System.Byte>::value
	uint8_t ___value_0;
	// System.Boolean System.Nullable`1<System.Byte>::has_value
	bool ___has_value_1;
};
