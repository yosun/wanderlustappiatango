﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Enumerator_t851;
// System.Object
struct Object_t;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t73;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t709;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m21488(__this, ___host, method) (( void (*) (Enumerator_t851 *, Dictionary_2_t709 *, const MethodInfo*))Enumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21489(__this, method) (( Object_t * (*) (Enumerator_t851 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m21490(__this, method) (( void (*) (Enumerator_t851 *, const MethodInfo*))Enumerator_Dispose_m16291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4525(__this, method) (( bool (*) (Enumerator_t851 *, const MethodInfo*))Enumerator_MoveNext_m16292_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m4524(__this, method) (( SurfaceAbstractBehaviour_t73 * (*) (Enumerator_t851 *, const MethodInfo*))Enumerator_get_Current_m16293_gshared)(__this, method)
