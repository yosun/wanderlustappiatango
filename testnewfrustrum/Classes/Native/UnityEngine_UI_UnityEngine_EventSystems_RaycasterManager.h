﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t228;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t229  : public Object_t
{
};
struct RaycasterManager_t229_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t228 * ___s_Raycasters_0;
};
