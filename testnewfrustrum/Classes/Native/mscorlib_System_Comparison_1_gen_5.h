﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Reflection.MethodInfo>
struct  Comparison_1_t3129  : public MulticastDelegate_t307
{
};
