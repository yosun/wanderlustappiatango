﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t4;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t89  : public MonoBehaviour_t7
{
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t4 * ___mLineMaterial_2;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_3;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t90  ___LineColor_4;
};
