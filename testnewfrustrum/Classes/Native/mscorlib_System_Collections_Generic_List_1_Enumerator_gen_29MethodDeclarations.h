﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t3157;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t237;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15602_gshared (Enumerator_t3157 * __this, List_1_t237 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15602(__this, ___l, method) (( void (*) (Enumerator_t3157 *, List_1_t237 *, const MethodInfo*))Enumerator__ctor_m15602_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15603_gshared (Enumerator_t3157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15603(__this, method) (( Object_t * (*) (Enumerator_t3157 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m15604_gshared (Enumerator_t3157 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15604(__this, method) (( void (*) (Enumerator_t3157 *, const MethodInfo*))Enumerator_Dispose_m15604_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m15605_gshared (Enumerator_t3157 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15605(__this, method) (( void (*) (Enumerator_t3157 *, const MethodInfo*))Enumerator_VerifyState_m15605_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15606_gshared (Enumerator_t3157 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15606(__this, method) (( bool (*) (Enumerator_t3157 *, const MethodInfo*))Enumerator_MoveNext_m15606_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t231  Enumerator_get_Current_m15607_gshared (Enumerator_t3157 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15607(__this, method) (( RaycastResult_t231  (*) (Enumerator_t3157 *, const MethodInfo*))Enumerator_get_Current_m15607_gshared)(__this, method)
