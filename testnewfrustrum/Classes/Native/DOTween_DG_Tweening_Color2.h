﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// DG.Tweening.Color2
struct  Color2_t1000 
{
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t90  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t90  ___cb_1;
};
