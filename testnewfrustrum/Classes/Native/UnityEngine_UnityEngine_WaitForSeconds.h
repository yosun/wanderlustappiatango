﻿#pragma once
#include <stdint.h>
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t452  : public YieldInstruction_t1143
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for marshalling of: UnityEngine.WaitForSeconds
struct WaitForSeconds_t452_marshaled
{
	float ___m_Seconds_0;
};
