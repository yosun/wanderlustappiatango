﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>
struct ReadOnlyCollection_1_t3449;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t589;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackable>
struct IList_1_t3448;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3447;
// System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackable>
struct IEnumerator_1_t4170;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m19883(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3449 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15055_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19884(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15056_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19885(__this, method) (( void (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15057_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19886(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15058_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19887(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15059_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19888(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15060_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19889(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15061_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19890(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15062_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19891(__this, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15063_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19892(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3449 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15064_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19893(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15065_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m19894(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15066_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m19895(__this, method) (( void (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15067_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m19896(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15068_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19897(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15069_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m19898(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15070_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m19899(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15071_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19900(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15072_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19901(__this, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15073_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19902(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15074_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19903(__this, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15075_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19904(__this, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15076_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m19905(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15077_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m19906(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3449 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15078_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::Contains(T)
#define ReadOnlyCollection_1_Contains_m19907(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15079_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m19908(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3449 *, SmartTerrainTrackableU5BU5D_t3447*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15080_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m19909(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15081_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m19910(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3449 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15082_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::get_Count()
#define ReadOnlyCollection_1_get_Count_m19911(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3449 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15083_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m19912(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15084_gshared)(__this, ___index, method)
