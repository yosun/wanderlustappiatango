﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.StringPlugin
struct StringPlugin_t990;
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t1043;
// System.String
struct String_t;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<System.String>
struct DOGetter_1_t1044;
// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t1045;
// System.Text.StringBuilder
struct StringBuilder_t423;
// System.Char[]
struct CharU5BU5D_t110;
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.StringPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern "C" void StringPlugin_Reset_m5457 (StringPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DG.Tweening.Plugins.StringPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.String)
extern "C" String_t* StringPlugin_ConvertToStartValue_m5458 (StringPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern "C" void StringPlugin_SetRelativeEndValue_m5459 (StringPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern "C" void StringPlugin_SetChangeValue_m5460 (StringPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.StringPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.StringOptions,System.Single,System.String)
extern "C" float StringPlugin_GetSpeedBasedDuration_m5461 (StringPlugin_t990 * __this, StringOptions_t1003  ___options, float ___unitsXSecond, String_t* ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.StringOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.Single,System.String,System.String,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void StringPlugin_EvaluateAndApply_m5462 (StringPlugin_t990 * __this, StringOptions_t1003  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1044 * ___getter, DOSetter_1_t1045 * ___setter, float ___elapsed, String_t* ___startValue, String_t* ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::Append(System.String,System.Int32,System.Int32,System.Boolean)
extern "C" StringBuilder_t423 * StringPlugin_Append_m5463 (StringPlugin_t990 * __this, String_t* ___value, int32_t ___startIndex, int32_t ___length, bool ___richTextEnabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] DG.Tweening.Plugins.StringPlugin::ScrambledCharsToUse(DG.Tweening.Plugins.Options.StringOptions)
extern "C" CharU5BU5D_t110* StringPlugin_ScrambledCharsToUse_m5464 (StringPlugin_t990 * __this, StringOptions_t1003  ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPlugin::.ctor()
extern "C" void StringPlugin__ctor_m5465 (StringPlugin_t990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPlugin::.cctor()
extern "C" void StringPlugin__cctor_m5466 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
