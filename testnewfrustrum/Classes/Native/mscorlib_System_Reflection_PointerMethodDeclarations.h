﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Pointer
struct Pointer_t2261;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.Pointer::.ctor()
extern "C" void Pointer__ctor_m12032 (Pointer_t2261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m12033 (Pointer_t2261 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
