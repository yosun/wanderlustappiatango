﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
struct U3CU3Ec__DisplayClassb2_t962;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::.ctor()
extern "C" void U3CU3Ec__DisplayClassb2__ctor_m5375 (U3CU3Ec__DisplayClassb2_t962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b0()
extern "C" Quaternion_t13  U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5376 (U3CU3Ec__DisplayClassb2_t962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b1(UnityEngine.Quaternion)
extern "C" void U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377 (U3CU3Ec__DisplayClassb2_t962 * __this, Quaternion_t13  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
