﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text[]
struct TextU5BU5D_t3246;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct  List_1_t437  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Text>::_items
	TextU5BU5D_t3246* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::_version
	int32_t ____version_3;
};
struct List_1_t437_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Text>::EmptyArray
	TextU5BU5D_t3246* ___EmptyArray_4;
};
