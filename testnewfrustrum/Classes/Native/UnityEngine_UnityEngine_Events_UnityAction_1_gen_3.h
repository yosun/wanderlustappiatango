﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t103;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct  UnityAction_1_t373  : public MulticastDelegate_t307
{
};
