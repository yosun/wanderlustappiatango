﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3279;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m17242_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m17242(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m17242_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17243_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17243(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17243_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17244_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17244(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17244_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m17245_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m17245(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m17245_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m17246_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m17246(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m17246_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m17247_gshared (U3CStartU3Ec__Iterator0_t3279 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m17247(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3279 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m17247_gshared)(__this, method)
