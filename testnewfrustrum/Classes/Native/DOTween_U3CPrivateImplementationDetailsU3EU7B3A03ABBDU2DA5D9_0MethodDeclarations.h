﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
struct __StaticArrayInitTypeSizeU3D50_t1014;
struct __StaticArrayInitTypeSizeU3D50_t1014_marshaled;

void __StaticArrayInitTypeSizeU3D50_t1014_marshal(const __StaticArrayInitTypeSizeU3D50_t1014& unmarshaled, __StaticArrayInitTypeSizeU3D50_t1014_marshaled& marshaled);
void __StaticArrayInitTypeSizeU3D50_t1014_marshal_back(const __StaticArrayInitTypeSizeU3D50_t1014_marshaled& marshaled, __StaticArrayInitTypeSizeU3D50_t1014& unmarshaled);
void __StaticArrayInitTypeSizeU3D50_t1014_marshal_cleanup(__StaticArrayInitTypeSizeU3D50_t1014_marshaled& marshaled);
