﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.CCMath
struct CCMath_t2166;

// System.Int32 System.Globalization.CCMath::div(System.Int32,System.Int32)
extern "C" int32_t CCMath_div_m11070 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::mod(System.Int32,System.Int32)
extern "C" int32_t CCMath_mod_m11071 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::div_mod(System.Int32&,System.Int32,System.Int32)
extern "C" int32_t CCMath_div_mod_m11072 (Object_t * __this /* static, unused */, int32_t* ___remainder, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
