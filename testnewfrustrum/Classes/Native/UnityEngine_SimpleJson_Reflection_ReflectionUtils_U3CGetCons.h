﻿#pragma once
#include <stdint.h>
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1287;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
struct  U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288  : public Object_t
{
	// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::constructorInfo
	ConstructorInfo_t1287 * ___constructorInfo_0;
};
