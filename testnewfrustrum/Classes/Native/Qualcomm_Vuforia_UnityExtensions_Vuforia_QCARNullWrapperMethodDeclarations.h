﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNullWrapper
struct QCARNullWrapper_t700;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t423;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceInitCamera(System.Int32)
extern "C" int32_t QCARNullWrapper_CameraDeviceInitCamera_m3483 (QCARNullWrapper_t700 * __this, int32_t ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceDeinitCamera()
extern "C" int32_t QCARNullWrapper_CameraDeviceDeinitCamera_m3484 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStartCamera()
extern "C" int32_t QCARNullWrapper_CameraDeviceStartCamera_m3485 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStopCamera()
extern "C" int32_t QCARNullWrapper_CameraDeviceStopCamera_m3486 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceGetNumVideoModes()
extern "C" int32_t QCARNullWrapper_CameraDeviceGetNumVideoModes_m3487 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C" void QCARNullWrapper_CameraDeviceGetVideoMode_m3488 (QCARNullWrapper_t700 * __this, int32_t ___idx, IntPtr_t ___videoMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSelectVideoMode(System.Int32)
extern "C" int32_t QCARNullWrapper_CameraDeviceSelectVideoMode_m3489 (QCARNullWrapper_t700 * __this, int32_t ___idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
extern "C" int32_t QCARNullWrapper_CameraDeviceSetFlashTorchMode_m3490 (QCARNullWrapper_t700 * __this, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFocusMode(System.Int32)
extern "C" int32_t QCARNullWrapper_CameraDeviceSetFocusMode_m3491 (QCARNullWrapper_t700 * __this, int32_t ___focusMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C" int32_t QCARNullWrapper_CameraDeviceSetCameraConfiguration_m3492 (QCARNullWrapper_t700 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
extern "C" int32_t QCARNullWrapper_QcarSetFrameFormat_m3493 (QCARNullWrapper_t700 * __this, int32_t ___format, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetExists(System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_DataSetExists_m3494 (QCARNullWrapper_t700 * __this, String_t* ___relativePath, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_DataSetLoad_m3495 (QCARNullWrapper_t700 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_DataSetGetNumTrackableType_m3496 (QCARNullWrapper_t700 * __this, int32_t ___trackableType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_DataSetGetTrackablesOfType_m3497 (QCARNullWrapper_t700 * __this, int32_t ___trackableType, IntPtr_t ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNullWrapper_DataSetGetTrackableName_m3498 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, StringBuilder_t423 * ___trackableName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_DataSetCreateTrackable_m3499 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, IntPtr_t ___trackableSourcePtr, StringBuilder_t423 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_DataSetDestroyTrackable_m3500 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
extern "C" int32_t QCARNullWrapper_DataSetHasReachedTrackableLimit_m3501 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetCameraThreadID()
extern "C" int32_t QCARNullWrapper_GetCameraThreadID_m3502 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderBuild(System.String,System.Single)
extern "C" int32_t QCARNullWrapper_ImageTargetBuilderBuild_m3503 (QCARNullWrapper_t700 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
extern "C" void QCARNullWrapper_FrameCounterGetBenchmarkingData_m3504 (QCARNullWrapper_t700 * __this, IntPtr_t ___benchmarkingData, bool ___isStereoRendering, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStartScan()
extern "C" void QCARNullWrapper_ImageTargetBuilderStartScan_m3505 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStopScan()
extern "C" void QCARNullWrapper_ImageTargetBuilderStopScan_m3506 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderGetFrameQuality()
extern "C" int32_t QCARNullWrapper_ImageTargetBuilderGetFrameQuality_m3507 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ImageTargetBuilderGetTrackableSource()
extern "C" IntPtr_t QCARNullWrapper_ImageTargetBuilderGetTrackableSource_m3508 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNullWrapper_ImageTargetCreateVirtualButton_m3509 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNullWrapper_ImageTargetDestroyVirtualButton_m3510 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNullWrapper_VirtualButtonGetId_m3511 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C" int32_t QCARNullWrapper_ImageTargetGetNumVirtualButtons_m3512 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C" int32_t QCARNullWrapper_ImageTargetGetVirtualButtons_m3513 (QCARNullWrapper_t700 * __this, IntPtr_t ___virtualButtonDataArray, IntPtr_t ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNullWrapper_ImageTargetGetVirtualButtonName_m3514 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t423 * ___vbName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNullWrapper_CylinderTargetGetDimensions_m3515 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___dimensions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNullWrapper_CylinderTargetSetSideLength_m3516 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___sideLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNullWrapper_CylinderTargetSetTopDiameter_m3517 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNullWrapper_CylinderTargetSetBottomDiameter_m3518 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNullWrapper_ObjectTargetSetSize_m3519 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNullWrapper_ObjectTargetGetSize_m3520 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerStart()
extern "C" int32_t QCARNullWrapper_ObjectTrackerStart_m3521 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ObjectTrackerStop()
extern "C" void QCARNullWrapper_ObjectTrackerStop_m3522 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ObjectTrackerCreateDataSet()
extern "C" IntPtr_t QCARNullWrapper_ObjectTrackerCreateDataSet_m3523 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
extern "C" int32_t QCARNullWrapper_ObjectTrackerDestroyDataSet_m3524 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
extern "C" int32_t QCARNullWrapper_ObjectTrackerActivateDataSet_m3525 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
extern "C" int32_t QCARNullWrapper_ObjectTrackerDeactivateDataSet_m3526 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
extern "C" int32_t QCARNullWrapper_ObjectTrackerPersistExtendedTracking_m3527 (QCARNullWrapper_t700 * __this, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerResetExtendedTracking()
extern "C" int32_t QCARNullWrapper_ObjectTrackerResetExtendedTracking_m3528 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerStart()
extern "C" int32_t QCARNullWrapper_MarkerTrackerStart_m3529 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::MarkerTrackerStop()
extern "C" void QCARNullWrapper_MarkerTrackerStop_m3530 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnSurfaceChanged(System.Int32,System.Int32)
extern "C" void QCARNullWrapper_OnSurfaceChanged_m3531 (QCARNullWrapper_t700 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnPause()
extern "C" void QCARNullWrapper_OnPause_m3532 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnResume()
extern "C" void QCARNullWrapper_OnResume_m3533 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::HasSurfaceBeenRecreated()
extern "C" bool QCARNullWrapper_HasSurfaceBeenRecreated_m3534 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerSetSize(System.Int32,System.Single)
extern "C" int32_t QCARNullWrapper_MarkerSetSize_m3535 (QCARNullWrapper_t700 * __this, int32_t ___trackableIndex, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
extern "C" int32_t QCARNullWrapper_MarkerTrackerCreateMarker_m3536 (QCARNullWrapper_t700 * __this, int32_t ___id, String_t* ___trackableName, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerDestroyMarker(System.Int32)
extern "C" int32_t QCARNullWrapper_MarkerTrackerDestroyMarker_m3537 (QCARNullWrapper_t700 * __this, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitPlatformNative()
extern "C" void QCARNullWrapper_InitPlatformNative_m3538 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitFrameState(System.IntPtr)
extern "C" void QCARNullWrapper_InitFrameState_m3539 (QCARNullWrapper_t700 * __this, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::DeinitFrameState(System.IntPtr)
extern "C" void QCARNullWrapper_DeinitFrameState_m3540 (QCARNullWrapper_t700 * __this, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_UpdateQCAR_m3541 (QCARNullWrapper_t700 * __this, IntPtr_t ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t ___frameIndex, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererEnd()
extern "C" void QCARNullWrapper_RendererEnd_m3542 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNullWrapper_QcarGetBufferSize_m3543 (QCARNullWrapper_t700 * __this, int32_t ___width, int32_t ___height, int32_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void QCARNullWrapper_QcarAddCameraFrame_m3544 (QCARNullWrapper_t700 * __this, IntPtr_t ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNullWrapper_RendererSetVideoBackgroundCfg_m3545 (QCARNullWrapper_t700 * __this, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNullWrapper_RendererGetVideoBackgroundCfg_m3546 (QCARNullWrapper_t700 * __this, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C" void QCARNullWrapper_RendererGetVideoBackgroundTextureInfo_m3547 (QCARNullWrapper_t700 * __this, IntPtr_t ___texInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
extern "C" int32_t QCARNullWrapper_RendererSetVideoBackgroundTextureID_m3548 (QCARNullWrapper_t700 * __this, int32_t ___textureID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
extern "C" int32_t QCARNullWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3549 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetHint(System.Int32,System.Int32)
extern "C" int32_t QCARNullWrapper_QcarSetHint_m3550 (QCARNullWrapper_t700 * __this, int32_t ___hint, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_GetProjectionGL_m3551 (QCARNullWrapper_t700 * __this, float ___nearClip, float ___farClip, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
extern "C" void QCARNullWrapper_SetApplicationEnvironment_m3552 (QCARNullWrapper_t700 * __this, int32_t ___major, int32_t ___minor, int32_t ___change, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetStateBufferSize(System.Int32)
extern "C" void QCARNullWrapper_SetStateBufferSize_m3553 (QCARNullWrapper_t700 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::SmartTerrainTrackerStart()
extern "C" int32_t QCARNullWrapper_SmartTerrainTrackerStart_m3554 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SmartTerrainTrackerStop()
extern "C" void QCARNullWrapper_SmartTerrainTrackerStop_m3555 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C" bool QCARNullWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3556 (QCARNullWrapper_t700 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerInitBuilder()
extern "C" bool QCARNullWrapper_SmartTerrainTrackerInitBuilder_m3557 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerDeinitBuilder()
extern "C" bool QCARNullWrapper_SmartTerrainTrackerDeinitBuilder_m3558 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
extern "C" IntPtr_t QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3559 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
extern "C" IntPtr_t QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3560 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C" bool QCARNullWrapper_SmartTerrainBuilderAddReconstruction_m3561 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C" bool QCARNullWrapper_SmartTerrainBuilderRemoveReconstruction_m3562 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
extern "C" bool QCARNullWrapper_SmartTerrainBuilderDestroyReconstruction_m3563 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStart(System.IntPtr)
extern "C" bool QCARNullWrapper_ReconstructionStart_m3564 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStop(System.IntPtr)
extern "C" bool QCARNullWrapper_ReconstructionStop_m3565 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionIsReconstructing(System.IntPtr)
extern "C" bool QCARNullWrapper_ReconstructionIsReconstructing_m3566 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionReset(System.IntPtr)
extern "C" bool QCARNullWrapper_ReconstructionReset_m3567 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C" void QCARNullWrapper_ReconstructionSetNavMeshPadding_m3568 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, float ___padding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C" bool QCARNullWrapper_ReconstructionFromTargetSetInitializationTarget_m3569 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, IntPtr_t ___dataSetPtr, int32_t ___trackableId, IntPtr_t ___occluderMin, IntPtr_t ___occluderMax, IntPtr_t ___offsetToOccluder, IntPtr_t ___rotationAxisToOccluder, float ___rotationAngleToOccluder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C" bool QCARNullWrapper_ReconstructionSetMaximumArea_m3570 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, IntPtr_t ___maximumArea, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
extern "C" int32_t QCARNullWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3571 (QCARNullWrapper_t700 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartInit(System.String,System.String)
extern "C" int32_t QCARNullWrapper_TargetFinderStartInit_m3572 (QCARNullWrapper_t700 * __this, String_t* ___userKey, String_t* ___secretKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetInitState()
extern "C" int32_t QCARNullWrapper_TargetFinderGetInitState_m3573 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderDeinit()
extern "C" int32_t QCARNullWrapper_TargetFinderDeinit_m3574 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartRecognition()
extern "C" int32_t QCARNullWrapper_TargetFinderStartRecognition_m3575 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStop()
extern "C" int32_t QCARNullWrapper_TargetFinderStop_m3576 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
extern "C" void QCARNullWrapper_TargetFinderSetUIScanlineColor_m3577 (QCARNullWrapper_t700 * __this, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
extern "C" void QCARNullWrapper_TargetFinderSetUIPointColor_m3578 (QCARNullWrapper_t700 * __this, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderUpdate(System.IntPtr)
extern "C" void QCARNullWrapper_TargetFinderUpdate_m3579 (QCARNullWrapper_t700 * __this, IntPtr_t ___targetFinderState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_TargetFinderGetResults_m3580 (QCARNullWrapper_t700 * __this, IntPtr_t ___searchResultArray, int32_t ___searchResultArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
extern "C" int32_t QCARNullWrapper_TargetFinderEnableTracking_m3581 (QCARNullWrapper_t700 * __this, IntPtr_t ___searchResult, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
extern "C" void QCARNullWrapper_TargetFinderGetImageTargets_m3582 (QCARNullWrapper_t700 * __this, IntPtr_t ___trackableIdArray, int32_t ___trackableIdArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderClearTrackables()
extern "C" void QCARNullWrapper_TargetFinderClearTrackables_m3583 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerStart()
extern "C" int32_t QCARNullWrapper_TextTrackerStart_m3584 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerStop()
extern "C" void QCARNullWrapper_TextTrackerStop_m3585 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNullWrapper_TextTrackerSetRegionOfInterest_m3586 (QCARNullWrapper_t700 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
extern "C" void QCARNullWrapper_TextTrackerGetRegionOfInterest_m3587 (QCARNullWrapper_t700 * __this, IntPtr_t ___detectionROI, IntPtr_t ___trackingROI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadWordList(System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_WordListLoadWordList_m3588 (QCARNullWrapper_t700 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordsFromFile(System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_WordListAddWordsFromFile_m3589 (QCARNullWrapper_t700 * __this, String_t* ___path, int32_t ___storagetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordU(System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordListAddWordU_m3590 (QCARNullWrapper_t700 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordU(System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordListRemoveWordU_m3591 (QCARNullWrapper_t700 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListContainsWordU(System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordListContainsWordU_m3592 (QCARNullWrapper_t700 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListUnloadAllLists()
extern "C" int32_t QCARNullWrapper_WordListUnloadAllLists_m3593 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListSetFilterMode(System.Int32)
extern "C" int32_t QCARNullWrapper_WordListSetFilterMode_m3594 (QCARNullWrapper_t700 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterMode()
extern "C" int32_t QCARNullWrapper_WordListGetFilterMode_m3595 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadFilterList(System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_WordListLoadFilterList_m3596 (QCARNullWrapper_t700 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordToFilterListU(System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordListAddWordToFilterListU_m3597 (QCARNullWrapper_t700 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordListRemoveWordFromFilterListU_m3598 (QCARNullWrapper_t700 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListClearFilterList()
extern "C" int32_t QCARNullWrapper_WordListClearFilterList_m3599 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterListWordCount()
extern "C" int32_t QCARNullWrapper_WordListGetFilterListWordCount_m3600 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::WordListGetFilterListWordU(System.Int32)
extern "C" IntPtr_t QCARNullWrapper_WordListGetFilterListWordU_m3601 (QCARNullWrapper_t700 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordGetLetterMask_m3602 (QCARNullWrapper_t700 * __this, int32_t ___wordID, IntPtr_t ___letterMaskImage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_WordGetLetterBoundingBoxes_m3603 (QCARNullWrapper_t700 * __this, int32_t ___wordID, IntPtr_t ___letterBoundingBoxes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerInitTracker(System.Int32)
extern "C" int32_t QCARNullWrapper_TrackerManagerInitTracker_m3604 (QCARNullWrapper_t700 * __this, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerDeinitTracker(System.Int32)
extern "C" int32_t QCARNullWrapper_TrackerManagerDeinitTracker_m3605 (QCARNullWrapper_t700 * __this, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_VirtualButtonSetEnabled_m3606 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNullWrapper_VirtualButtonSetSensitivity_m3607 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNullWrapper_VirtualButtonSetAreaRectangle_m3608 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarInit(System.String)
extern "C" int32_t QCARNullWrapper_QcarInit_m3609 (QCARNullWrapper_t700 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarDeinit()
extern "C" int32_t QCARNullWrapper_QcarDeinit_m3610 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_StartExtendedTracking_m3611 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_StopExtendedTracking_m3612 (QCARNullWrapper_t700 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSupportedDeviceDetected()
extern "C" bool QCARNullWrapper_EyewearIsSupportedDeviceDetected_m3613 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSeeThru()
extern "C" bool QCARNullWrapper_EyewearIsSeeThru_m3614 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetScreenOrientation()
extern "C" int32_t QCARNullWrapper_EyewearGetScreenOrientation_m3615 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoCapable()
extern "C" bool QCARNullWrapper_EyewearIsStereoCapable_m3616 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoEnabled()
extern "C" bool QCARNullWrapper_EyewearIsStereoEnabled_m3617 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoGLOnly()
extern "C" bool QCARNullWrapper_EyewearIsStereoGLOnly_m3618 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearSetStereo(System.Boolean)
extern "C" bool QCARNullWrapper_EyewearSetStereo_m3619 (QCARNullWrapper_t700 * __this, bool ___enable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
extern "C" int32_t QCARNullWrapper_EyewearGetDefaultSceneScale_m3620 (QCARNullWrapper_t700 * __this, IntPtr_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNullWrapper_EyewearGetProjectionMatrix_m3621 (QCARNullWrapper_t700 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetMaxCount()
extern "C" int32_t QCARNullWrapper_EyewearCPMGetMaxCount_m3622 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetUsedCount()
extern "C" int32_t QCARNullWrapper_EyewearCPMGetUsedCount_m3623 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMIsProfileUsed(System.Int32)
extern "C" bool QCARNullWrapper_EyewearCPMIsProfileUsed_m3624 (QCARNullWrapper_t700 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetActiveProfile()
extern "C" int32_t QCARNullWrapper_EyewearCPMGetActiveProfile_m3625 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetActiveProfile(System.Int32)
extern "C" bool QCARNullWrapper_EyewearCPMSetActiveProfile_m3626 (QCARNullWrapper_t700 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" int32_t QCARNullWrapper_EyewearCPMGetProjectionMatrix_m3627 (QCARNullWrapper_t700 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" bool QCARNullWrapper_EyewearCPMSetProjectionMatrix_m3628 (QCARNullWrapper_t700 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::EyewearCPMGetProfileName(System.Int32)
extern "C" IntPtr_t QCARNullWrapper_EyewearCPMGetProfileName_m3629 (QCARNullWrapper_t700 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
extern "C" bool QCARNullWrapper_EyewearCPMSetProfileName_m3630 (QCARNullWrapper_t700 * __this, int32_t ___profileID, IntPtr_t ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMClearProfile(System.Int32)
extern "C" bool QCARNullWrapper_EyewearCPMClearProfile_m3631 (QCARNullWrapper_t700 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool QCARNullWrapper_EyewearUserCalibratorInit_m3632 (QCARNullWrapper_t700 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMinScaleHint()
extern "C" float QCARNullWrapper_EyewearUserCalibratorGetMinScaleHint_m3633 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMaxScaleHint()
extern "C" float QCARNullWrapper_EyewearUserCalibratorGetMaxScaleHint_m3634 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorIsStereoStretched()
extern "C" bool QCARNullWrapper_EyewearUserCalibratorIsStereoStretched_m3635 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
extern "C" bool QCARNullWrapper_EyewearUserCalibratorGetProjectionMatrix_m3636 (QCARNullWrapper_t700 * __this, IntPtr_t ___readingsArray, int32_t ___numReadings, IntPtr_t ___calibrationResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::.ctor()
extern "C" void QCARNullWrapper__ctor_m3637 (QCARNullWrapper_t700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
