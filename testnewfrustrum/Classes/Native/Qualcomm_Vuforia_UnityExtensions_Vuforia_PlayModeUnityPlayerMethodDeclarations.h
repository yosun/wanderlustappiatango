﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t147;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C" void PlayModeUnityPlayer_LoadNativeLibraries_m2734 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern "C" void PlayModeUnityPlayer_InitializePlatform_m2735 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern "C" void PlayModeUnityPlayer_InitializeSurface_m2736 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern "C" int32_t PlayModeUnityPlayer_Start_m2737 (PlayModeUnityPlayer_t147 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern "C" void PlayModeUnityPlayer_Update_m2738 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C" void PlayModeUnityPlayer_Dispose_m2739 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C" void PlayModeUnityPlayer_OnPause_m2740 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C" void PlayModeUnityPlayer_OnResume_m2741 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C" void PlayModeUnityPlayer_OnDestroy_m2742 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C" void PlayModeUnityPlayer__ctor_m461 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
