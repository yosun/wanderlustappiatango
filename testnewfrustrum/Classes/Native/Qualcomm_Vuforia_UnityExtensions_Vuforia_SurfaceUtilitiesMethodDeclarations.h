﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceUtilities
struct SurfaceUtilities_t131;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern "C" void SurfaceUtilities_OnSurfaceCreated_m425 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
extern "C" void SurfaceUtilities_OnSurfaceDeinit_m4192 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern "C" bool SurfaceUtilities_HasSurfaceBeenRecreated_m420 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
extern "C" void SurfaceUtilities_OnSurfaceChanged_m4193 (Object_t * __this /* static, unused */, int32_t ___screenWidth, int32_t ___screenHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern "C" void SurfaceUtilities_SetSurfaceOrientation_m426 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
extern "C" int32_t SurfaceUtilities_GetSurfaceOrientation_m4194 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::.cctor()
extern "C" void SurfaceUtilities__cctor_m4195 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
