﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t630;
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
// Vuforia.MarkerTrackerImpl
struct  MarkerTrackerImpl_t631  : public MarkerTracker_t629
{
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker> Vuforia.MarkerTrackerImpl::mMarkerDict
	Dictionary_2_t630 * ___mMarkerDict_1;
};
