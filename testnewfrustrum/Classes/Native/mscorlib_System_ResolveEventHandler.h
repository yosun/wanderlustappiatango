﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t2003;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2544;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t2489  : public MulticastDelegate_t307
{
};
