﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t362;
// UnityEngine.RectTransform
struct RectTransform_t272;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"

// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern "C" void ContentSizeFitter__ctor_m1761 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern "C" int32_t ContentSizeFitter_get_horizontalFit_m1762 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern "C" void ContentSizeFitter_set_horizontalFit_m1763 (ContentSizeFitter_t362 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern "C" int32_t ContentSizeFitter_get_verticalFit_m1764 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern "C" void ContentSizeFitter_set_verticalFit_m1765 (ContentSizeFitter_t362 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern "C" RectTransform_t272 * ContentSizeFitter_get_rectTransform_m1766 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern "C" void ContentSizeFitter_OnEnable_m1767 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern "C" void ContentSizeFitter_OnDisable_m1768 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m1769 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m1770 (ContentSizeFitter_t362 * __this, int32_t ___axis, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m1771 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern "C" void ContentSizeFitter_SetLayoutVertical_m1772 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern "C" void ContentSizeFitter_SetDirty_m1773 (ContentSizeFitter_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
