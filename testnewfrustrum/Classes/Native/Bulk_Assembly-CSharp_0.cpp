﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
#ifndef _MSC_VER
#else
#endif
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentModeMethodDeclarations.h"



// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
#ifndef _MSC_VER
#else
#endif
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModesMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Single
#include "mscorlib_System_Single.h"
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallback.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// Gyro2
#include "AssemblyU2DCSharp_Gyro2MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensionsMethodDeclarations.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallbackMethodDeclarations.h"
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensionsMethodDeclarations.h"
// Input2
#include "AssemblyU2DCSharp_Input2MethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
struct TweenSettingsExtensions_t100;
struct Tweener_t99;
struct TweenCallback_t101;
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensions.h"
struct TweenSettingsExtensions_t100;
struct Object_t;
struct TweenCallback_t101;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C" Object_t * TweenSettingsExtensions_OnComplete_TisObject_t_m236_gshared (Object_t * __this /* static, unused */, Object_t * p0, TweenCallback_t101 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnComplete_TisObject_t_m236(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, TweenCallback_t101 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisObject_t_m236_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t99_m235(__this /* static, unused */, p0, p1, method) (( Tweener_t99 * (*) (Object_t * /* static, unused */, Tweener_t99 *, TweenCallback_t101 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisObject_t_m236_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void ARVRModes::.ctor()
extern TypeInfo* GameObjectU5BU5D_t5_il2cpp_TypeInfo_var;
extern "C" void ARVRModes__ctor_m0 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___goDisableForEntire_9 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goEnableForEntire_10 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goDisableForDoor_11 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goEnableForDoor_12 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::.cctor()
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void ARVRModes__cctor_m1 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13 = 1;
		return;
	}
}
// System.Void ARVRModes::Start()
extern "C" void ARVRModes_Start_m2 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	{
		ARVRModes_SwitchVRToAR_m7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::FlipGameObjectArray(UnityEngine.GameObject[],System.Boolean)
extern "C" void ARVRModes_FlipGameObjectArray_m3 (ARVRModes_t6 * __this, GameObjectU5BU5D_t5* ___g, bool ___flip, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0007:
	{
		GameObjectU5BU5D_t5* L_0 = ___g;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		bool L_3 = ___flip;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_0, L_2)));
		GameObject_SetActive_m238((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_0, L_2)), L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0014:
	{
		int32_t L_5 = V_0;
		GameObjectU5BU5D_t5* L_6 = ___g;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)(((Array_t *)L_6)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ARVRModes::SeeEntire()
extern "C" void ARVRModes_SeeEntire_m4 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___goDisableForEntire_9);
		ARVRModes_FlipGameObjectArray_m3(__this, L_0, 0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_1 = (__this->___goEnableForEntire_10);
		ARVRModes_FlipGameObjectArray_m3(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SeeDoor()
extern "C" void ARVRModes_SeeDoor_m5 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___goDisableForDoor_11);
		ARVRModes_FlipGameObjectArray_m3(__this, L_0, 0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_1 = (__this->___goEnableForDoor_12);
		ARVRModes_FlipGameObjectArray_m3(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SwitchARToVR()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern TypeInfo* TweenCallback_t101_il2cpp_TypeInfo_var;
extern const MethodInfo* Gyro2_ResetStaticSmoothly_m22_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t99_m235_MethodInfo_var;
extern "C" void ARVRModes_SwitchARToVR_m6 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		TweenCallback_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		Gyro2_ResetStaticSmoothly_m22_MethodInfo_var = il2cpp_codegen_method_info_from_index(1);
		TweenSettingsExtensions_OnComplete_TisTweener_t99_m235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t11 * V_0 = {0};
	Vector3_t15  V_1 = {0};
	Quaternion_t13  V_2 = {0};
	{
		GameObject_t2 * L_0 = (__this->___goMaskThese_2);
		NullCheck(L_0);
		GameObject_SetActiveRecursively_m239(L_0, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_1 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_1);
		GameObject_SetActive_m238(L_1, 0, /*hidden argument*/NULL);
		GameObject_t2 * L_2 = (__this->___goVRSet_6);
		NullCheck(L_2);
		GameObject_SetActive_m238(L_2, 1, /*hidden argument*/NULL);
		Camera_t3 * L_3 = (__this->___camVR_5);
		NullCheck(L_3);
		Behaviour_set_enabled_m240(L_3, 1, /*hidden argument*/NULL);
		Camera_t3 * L_4 = (__this->___camAR_4);
		NullCheck(L_4);
		Behaviour_set_enabled_m240(L_4, 0, /*hidden argument*/NULL);
		Material_t4 * L_5 = (__this->___matTouchMeLipstick_7);
		Color_t90  L_6 = {0};
		Color__ctor_m241(&L_6, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_color_m242(L_5, L_6, /*hidden argument*/NULL);
		Camera_t3 * L_7 = (__this->___camVR_5);
		NullCheck(L_7);
		Transform_t11 * L_8 = Component_get_transform_m243(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = Transform_get_parent_m244(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Camera_t3 * L_10 = (__this->___camAR_4);
		NullCheck(L_10);
		Transform_t11 * L_11 = Component_get_transform_m243(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t11 * L_12 = Transform_get_parent_m244(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Quaternion_t13  L_13 = Transform_get_rotation_m245(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		Vector3_t15  L_14 = Quaternion_get_eulerAngles_m246((&V_2), /*hidden argument*/NULL);
		V_1 = L_14;
		Camera_t3 * L_15 = (__this->___camAR_4);
		NullCheck(L_15);
		Transform_t11 * L_16 = Component_get_transform_m243(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t11 * L_17 = Transform_get_parent_m244(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t15  L_18 = Transform_get_position_m247(L_17, /*hidden argument*/NULL);
		Camera_t3 * L_19 = (__this->___camAR_4);
		NullCheck(L_19);
		Transform_t11 * L_20 = Component_get_transform_m243(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t11 * L_21 = Transform_get_parent_m244(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Quaternion_t13  L_22 = Transform_get_rotation_m245(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Gyro2_StationParent_m18(NULL /*static, unused*/, L_18, L_22, /*hidden argument*/NULL);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_23 = (__this->___goUI_PanelNav_8);
		NullCheck(L_23);
		GameObject_SetActive_m238(L_23, 1, /*hidden argument*/NULL);
		MonoBehaviour_print_m248(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		Camera_t3 * L_24 = (__this->___camVR_5);
		NullCheck(L_24);
		Transform_t11 * L_25 = Component_get_transform_m243(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t11 * L_26 = Transform_get_parent_m244(L_25, /*hidden argument*/NULL);
		float L_27 = ((&V_1)->___y_2);
		Vector3_t15  L_28 = {0};
		Vector3__ctor_m249(&L_28, (0.0f), L_27, (0.0f), /*hidden argument*/NULL);
		Tweener_t99 * L_29 = ShortcutExtensions_DORotate_m250(NULL /*static, unused*/, L_26, L_28, (3.14f), 0, /*hidden argument*/NULL);
		IntPtr_t L_30 = { (void*)Gyro2_ResetStaticSmoothly_m22_MethodInfo_var };
		TweenCallback_t101 * L_31 = (TweenCallback_t101 *)il2cpp_codegen_object_new (TweenCallback_t101_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m251(L_31, NULL, L_30, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t99_m235(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t99_m235_MethodInfo_var);
		return;
	}
}
// System.Void ARVRModes::SwitchVRToAR()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_SwitchVRToAR_m7 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_0);
		GameObject_SetActive_m238(L_0, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_1 = (__this->___goVRSet_6);
		NullCheck(L_1);
		GameObject_SetActive_m238(L_1, 0, /*hidden argument*/NULL);
		Camera_t3 * L_2 = (__this->___camVR_5);
		NullCheck(L_2);
		Behaviour_set_enabled_m240(L_2, 0, /*hidden argument*/NULL);
		Camera_t3 * L_3 = (__this->___camAR_4);
		NullCheck(L_3);
		Behaviour_set_enabled_m240(L_3, 1, /*hidden argument*/NULL);
		Material_t4 * L_4 = (__this->___matTouchMeLipstick_7);
		Color_t90  L_5 = {0};
		Color__ctor_m241(&L_5, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_set_color_m242(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		GameObject_t2 * L_6 = (__this->___goUI_PanelNav_8);
		NullCheck(L_6);
		GameObject_SetActive_m238(L_6, 0, /*hidden argument*/NULL);
		MonoBehaviour_print_m248(NULL /*static, unused*/, (String_t*) &_stringLiteral2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SwitchModes()
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_SwitchModes_m8 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13 = 1;
		ARVRModes_SwitchARToVR_m6(__this, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_1 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13 = 0;
		ARVRModes_SwitchVRToAR_m7(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void ARVRModes::Update()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_Update_m9 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t96  V_0 = {0};
	RaycastHit_t94  V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_0 = Input2_HasPointStarted_m43(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		Camera_t3 * L_1 = Camera_get_main_m252(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Vector3_t15  L_2 = Input_get_mousePosition_m253(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t96  L_3 = Camera_ScreenPointToRay_m254(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t96  L_4 = V_0;
		bool L_5 = Physics_Raycast_m255(NULL /*static, unused*/, L_4, (&V_1), (3000.0f), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006b;
		}
	}
	{
		Camera_t3 * L_6 = Camera_get_main_m252(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t11 * L_7 = Component_get_transform_m243(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t15  L_8 = Transform_get_position_m247(L_7, /*hidden argument*/NULL);
		Vector3_t15  L_9 = RaycastHit_get_point_m256((&V_1), /*hidden argument*/NULL);
		Debug_DrawRay_m257(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Transform_t11 * L_10 = RaycastHit_get_transform_m258((&V_1), /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m259(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m260(NULL /*static, unused*/, L_12, (String_t*) &_stringLiteral3, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006b;
		}
	}
	{
		ARVRModes_SwitchModes_m8(__this, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
#ifndef _MSC_VER
#else
#endif
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTextureMethodDeclarations.h"

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
struct Component_t103;
struct Renderer_t8;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t103;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m262_gshared (Component_t103 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m262(__this, method) (( Object_t * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t8_m261(__this, method) (( Renderer_t8 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void AnimateTexture::.ctor()
extern "C" void AnimateTexture__ctor_m10 (AnimateTexture_t9 * __this, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = Vector2_get_zero_m263(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___uvOffset_2 = L_0;
		Vector2_t10  L_1 = {0};
		Vector2__ctor_m264(&L_1, (0.1f), (0.1f), /*hidden argument*/NULL);
		__this->___uvAnimationRate_3 = L_1;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimateTexture::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var;
extern "C" void AnimateTexture_Awake_m11 (AnimateTexture_t9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t8 * L_0 = Component_GetComponent_TisRenderer_t8_m261(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var);
		__this->___renderer_4 = L_0;
		return;
	}
}
// System.Void AnimateTexture::Update()
extern "C" void AnimateTexture_Update_m12 (AnimateTexture_t9 * __this, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = (__this->___uvOffset_2);
		Vector2_t10  L_1 = (__this->___uvAnimationRate_3);
		float L_2 = Time_get_deltaTime_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t10  L_3 = Vector2_op_Multiply_m266(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t10  L_4 = Vector2_op_Addition_m267(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		__this->___uvOffset_2 = L_4;
		Renderer_t8 * L_5 = (__this->___renderer_4);
		NullCheck(L_5);
		bool L_6 = Renderer_get_enabled_m268(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		Renderer_t8 * L_7 = (__this->___renderer_4);
		NullCheck(L_7);
		Material_t4 * L_8 = Renderer_get_material_m269(L_7, /*hidden argument*/NULL);
		Vector2_t10  L_9 = (__this->___uvOffset_2);
		NullCheck(L_8);
		Material_SetTextureOffset_m270(L_8, (String_t*) &_stringLiteral4, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_Gyroscope.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_GyroscopeMethodDeclarations.h"


// System.Void Gyro2::.ctor()
extern "C" void Gyro2__ctor_m13 (Gyro2_t12 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::.cctor()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2__cctor_m14 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		Quaternion_t13  L_0 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7 = L_0;
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = (0.0f);
		return;
	}
}
// System.Void Gyro2::Awake()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Awake_m15 (Gyro2_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = GameObject_get_transform_m273(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2 = L_1;
		return;
	}
}
// System.Void Gyro2::Start()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Start_m16 (Gyro2_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = GameObject_get_transform_m273(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Gyro2_Init_m20(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" Vector3_t15  Gyro2_ClampPosInRoom_m17 (Object_t * __this /* static, unused */, Vector3_t15  ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___p)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m274(NULL /*static, unused*/, L_0, (-29.4f), (7.4f), /*hidden argument*/NULL);
		float L_2 = ((&___p)->___y_2);
		float L_3 = Mathf_Clamp_m274(NULL /*static, unused*/, L_2, (0.0f), (10.0f), /*hidden argument*/NULL);
		float L_4 = ((&___p)->___z_3);
		float L_5 = Mathf_Clamp_m274(NULL /*static, unused*/, L_4, (3.0f), (29.8f), /*hidden argument*/NULL);
		Vector3__ctor_m249((&___p), L_1, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t15  L_6 = ___p;
		return L_6;
	}
}
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_StationParent_m18 (Object_t * __this /* static, unused */, Vector3_t15  ___pos, Quaternion_t13  ___rot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Transform_t11 * L_0 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m244(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t11 * L_2 = Transform_get_parent_m244(L_1, /*hidden argument*/NULL);
		Vector3_t15  L_3 = ___pos;
		Vector3_t15  L_4 = Gyro2_ClampPosInRoom_m17(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector3_t15  L_5 = {0};
		Vector3__ctor_m249(&L_5, (0.0f), (3.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t15  L_6 = Vector3_op_Subtraction_m275(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m276(L_2, L_6, /*hidden argument*/NULL);
		Transform_t11 * L_7 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_7);
		Transform_t11 * L_8 = Transform_get_parent_m244(L_7, /*hidden argument*/NULL);
		Quaternion_t13  L_9 = ___rot;
		NullCheck(L_8);
		Transform_set_rotation_m277(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_StationParentShiftAngle_m19 (Gyro2_t12 * __this, float ___angle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Transform_t11 * L_0 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m244(L_0, /*hidden argument*/NULL);
		Transform_t11 * L_2 = L_1;
		NullCheck(L_2);
		Quaternion_t13  L_3 = Transform_get_rotation_m245(L_2, /*hidden argument*/NULL);
		float L_4 = ___angle;
		Quaternion_t13  L_5 = Quaternion_Euler_m278(NULL /*static, unused*/, (0.0f), L_4, (0.0f), /*hidden argument*/NULL);
		Quaternion_t13  L_6 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_rotation_m277(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Init(UnityEngine.Transform)
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Init_m20 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Gyroscope_t105 * L_1 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Gyroscope_set_enabled_m281(L_1, 1, /*hidden argument*/NULL);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::ResetStatic()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_ResetStatic_m21 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Gyro2_AssignCalibration_m27(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gyro2_AssignCamBaseRotation_m28(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t11 * L_0 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m244(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t13  L_2 = Transform_get_rotation_m245(L_1, /*hidden argument*/NULL);
		Transform_t11 * L_3 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_3);
		Vector3_t15  L_4 = Transform_get_forward_m282(L_3, /*hidden argument*/NULL);
		Vector3_t15  L_5 = Vector3_get_forward_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_6 = Quaternion_FromToRotation_m284(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Quaternion_t13  L_7 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_2, L_6, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7 = L_7;
		Quaternion_t13  L_8 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7;
		Quaternion_t13  L_9 = Quaternion_Inverse_m285(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Quaternion_t13  L_10 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___calibration_5;
		Quaternion_t13  L_11 = Quaternion_Inverse_m285(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Quaternion_t13  L_12 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___refRot_6 = L_12;
		return;
	}
}
// System.Void Gyro2::ResetStaticSmoothly()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_ResetStaticSmoothly_m22 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = (3.14f);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Reset()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Reset_m23 (Gyro2_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern "C" void Gyro2_DoSwipeRotation_m24 (Gyro2_t12 * __this, float ___degree, const MethodInfo* method)
{
	{
		float L_0 = ___degree;
		Gyro2_StationParentShiftAngle_m19(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::DisableSettingsButotn()
extern "C" void Gyro2_DisableSettingsButotn_m25 (Gyro2_t12 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = (__this->___goUI_SettingsButtonToggle_10);
		NullCheck(L_0);
		GameObject_SetActive_m238(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Update()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Update_m26 (Gyro2_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t10  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_0 = Input2_IsPointerOverUI_m38(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)4)))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_2 = Input2_HasPointStarted_m43(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		MonoBehaviour_print_m248(NULL /*static, unused*/, (String_t*) &_stringLiteral5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = (__this->___goUI_Directions_FiveFingers_8);
		NullCheck(L_3);
		GameObject_SetActive_m238(L_3, 0, /*hidden argument*/NULL);
	}

IL_003b:
	{
		goto IL_00b7;
	}

IL_0040:
	{
		Vector2_t10  L_4 = Vector2_get_zero_m263(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		Vector2_t10  L_5 = Input2_ContinuousSwipe_m53(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_t10  L_6 = V_0;
		bool L_7 = Input2_SwipeLeft_m49(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		Gyro2_DoSwipeRotation_m24(__this, (-1.0f), /*hidden argument*/NULL);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_8 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_8);
		GameObject_SetActive_m238(L_8, 0, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_0084:
	{
		Vector2_t10  L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_10 = Input2_SwipeRight_m50(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		Gyro2_DoSwipeRotation_m24(__this, (1.0f), /*hidden argument*/NULL);
		Gyro2_ResetStatic_m21(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_11 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_11);
		GameObject_SetActive_m238(L_11, 0, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_12 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13;
		if (L_12)
		{
			goto IL_00d9;
		}
	}
	{
		GameObject_t2 * L_13 = (__this->___goUI_Directions_FiveFingers_8);
		NullCheck(L_13);
		GameObject_SetActive_m238(L_13, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_14 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_14);
		GameObject_SetActive_m238(L_14, 1, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		bool L_15 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3;
		if (!L_15)
		{
			goto IL_017f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		float L_16 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___countdown_11;
		if ((!(((float)L_16) > ((float)(0.0f)))))
		{
			goto IL_0143;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		float L_17 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___countdown_11;
		float L_18 = Time_get_deltaTime_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = ((float)((float)L_17-(float)L_18));
		Transform_t11 * L_19 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		Transform_t11 * L_20 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_20);
		Quaternion_t13  L_21 = Transform_get_rotation_m245(L_20, /*hidden argument*/NULL);
		Quaternion_t13  L_22 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___camBase_4;
		Quaternion_t13  L_23 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___refRot_6;
		Quaternion_t13  L_24 = Gyro2_G_m29(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_25 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Quaternion_t13  L_26 = Gyro2_ConvertRotation_m30(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Quaternion_t13  L_27 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_22, L_26, /*hidden argument*/NULL);
		Quaternion_t13  L_28 = Quaternion_Slerp_m287(NULL /*static, unused*/, L_21, L_27, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_rotation_m277(L_19, L_28, /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0143:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Transform_t11 * L_29 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		Transform_t11 * L_30 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_30);
		Quaternion_t13  L_31 = Transform_get_rotation_m245(L_30, /*hidden argument*/NULL);
		Quaternion_t13  L_32 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___camBase_4;
		Quaternion_t13  L_33 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___refRot_6;
		Quaternion_t13  L_34 = Gyro2_G_m29(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_35 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Quaternion_t13  L_36 = Gyro2_ConvertRotation_m30(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		Quaternion_t13  L_37 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_32, L_36, /*hidden argument*/NULL);
		Quaternion_t13  L_38 = Quaternion_Slerp_m287(NULL /*static, unused*/, L_31, L_37, (0.9f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_rotation_m277(L_29, L_38, /*hidden argument*/NULL);
	}

IL_017f:
	{
		return;
	}
}
// System.Void Gyro2::AssignCalibration()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_AssignCalibration_m27 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Quaternion_t13  L_0 = Gyro2_G_m29(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___calibration_5 = L_0;
		return;
	}
}
// System.Void Gyro2::AssignCamBaseRotation()
extern TypeInfo* Gyro2_t12_il2cpp_TypeInfo_var;
extern "C" void Gyro2_AssignCamBaseRotation_m28 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t15  L_0 = Vector3_get_forward_m283(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t12_il2cpp_TypeInfo_var);
		Transform_t11 * L_1 = ((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_1);
		Vector3_t15  L_2 = Transform_get_forward_m282(L_1, /*hidden argument*/NULL);
		Quaternion_t13  L_3 = Quaternion_FromToRotation_m284(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((Gyro2_t12_StaticFields*)Gyro2_t12_il2cpp_TypeInfo_var->static_fields)->___camBase_4 = L_3;
		return;
	}
}
// UnityEngine.Quaternion Gyro2::G()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" Quaternion_t13  Gyro2_G_m29 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Gyroscope_t105 * L_0 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t13  L_1 = Gyroscope_get_attitude_m288(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t13  Gyro2_ConvertRotation_m30 (Object_t * __this /* static, unused */, Quaternion_t13  ___q, const MethodInfo* method)
{
	{
		float L_0 = ((&___q)->___x_1);
		float L_1 = ((&___q)->___y_2);
		float L_2 = ((&___q)->___z_3);
		float L_3 = ((&___q)->___w_4);
		Quaternion_t13  L_4 = {0};
		Quaternion__ctor_m289(&L_4, L_0, L_1, ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
#ifndef _MSC_VER
#else
#endif
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYoMethodDeclarations.h"

// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
struct Component_t103;
struct Transform_t11;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t11_m290(__this, method) (( Transform_t11 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void GyroCameraYo::.ctor()
extern "C" void GyroCameraYo__ctor_m31 (GyroCameraYo_t14 * __this, const MethodInfo* method)
{
	{
		Quaternion_t13  L_0 = {0};
		Quaternion__ctor_m289(&L_0, (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rotFix_4 = L_0;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GyroCameraYo::Start()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t11_m290_MethodInfo_var;
extern "C" void GyroCameraYo_Start_m32 (GyroCameraYo_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Component_GetComponent_TisTransform_t11_m290_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = Component_GetComponent_TisTransform_t11_m290(__this, /*hidden argument*/Component_GetComponent_TisTransform_t11_m290_MethodInfo_var);
		__this->___transform_3 = L_0;
		Screen_set_sleepTimeout_m291(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Gyroscope_t105 * L_1 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Gyroscope_set_enabled_m281(L_1, 1, /*hidden argument*/NULL);
		Gyroscope_t105 * L_2 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Gyroscope_set_updateInterval_m292(L_2, (0.01f), /*hidden argument*/NULL);
		Transform_t11 * L_3 = (__this->___parentTransform_2);
		NullCheck(L_3);
		Transform_t11 * L_4 = Component_get_transform_m243(L_3, /*hidden argument*/NULL);
		Vector3_t15  L_5 = {0};
		Vector3__ctor_m249(&L_5, (90.0f), (200.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_eulerAngles_m293(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GyroCameraYo::Update()
extern "C" void GyroCameraYo_Update_m33 (GyroCameraYo_t14 * __this, const MethodInfo* method)
{
	{
		GyroCameraYo_GyroCam_m35(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t13  GyroCameraYo_ConvertRotation_m34 (Object_t * __this /* static, unused */, Quaternion_t13  ___q, const MethodInfo* method)
{
	{
		float L_0 = ((&___q)->___x_1);
		float L_1 = ((&___q)->___y_2);
		float L_2 = ((&___q)->___z_3);
		float L_3 = ((&___q)->___w_4);
		Quaternion_t13  L_4 = {0};
		Quaternion__ctor_m289(&L_4, L_0, L_1, ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GyroCameraYo::GyroCam()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" void GyroCameraYo_GyroCam_m35 (GyroCameraYo_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t13  V_0 = {0};
	Vector3_t15  V_1 = {0};
	Quaternion_t13  V_2 = {0};
	Quaternion_t13  V_3 = {0};
	Quaternion_t13  V_4 = {0};
	Quaternion_t13  V_5 = {0};
	Vector3_t15  V_6 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Gyroscope_t105 * L_0 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t13  L_1 = Gyroscope_get_attitude_m288(L_0, /*hidden argument*/NULL);
		V_5 = L_1;
		Vector3_t15  L_2 = Quaternion_get_eulerAngles_m246((&V_5), /*hidden argument*/NULL);
		Quaternion_t13  L_3 = Quaternion_Euler_m294(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Quaternion_t13  L_4 = GyroCameraYo_ConvertRotation_m34(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t15  L_5 = Quaternion_get_eulerAngles_m246((&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		Transform_t11 * L_6 = (__this->___transform_3);
		Gyroscope_t105 * L_7 = Input_get_gyro_m280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t15  L_8 = Gyroscope_get_rotationRate_m295(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		float L_9 = ((&V_6)->___y_2);
		NullCheck(L_6);
		Transform_Rotate_m296(L_6, (0.0f), ((-L_9)), (0.0f), /*hidden argument*/NULL);
		Transform_t11 * L_10 = (__this->___transform_3);
		NullCheck(L_10);
		Quaternion_t13  L_11 = Transform_get_localRotation_m297(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Quaternion_t13  L_12 = V_2;
		Quaternion_t13  L_13 = V_0;
		float L_14 = Time_get_deltaTime_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp01_m298(NULL /*static, unused*/, ((float)((float)(5.0f)*(float)L_14)), /*hidden argument*/NULL);
		Quaternion_t13  L_16 = Quaternion_Slerp_m287(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Quaternion_t13  L_17 = V_3;
		V_4 = L_17;
		Transform_t11 * L_18 = (__this->___transform_3);
		Quaternion_t13  L_19 = V_4;
		NullCheck(L_18);
		Transform_set_localRotation_m299(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// Input2
#include "AssemblyU2DCSharp_Input2.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystemMethodDeclarations.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"


// System.Void Input2::.ctor()
extern "C" void Input2__ctor_m36 (Input2_t16 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Input2::.cctor()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" void Input2__cctor_m37 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t10  L_0 = {0};
		Vector2__ctor_m264(&L_0, (-999.0f), (-999.0f), /*hidden argument*/NULL);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___offScreen_2 = L_0;
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = (0.0f);
		Vector2_t10  L_1 = Vector2_get_zero_m263(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_1;
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5 = 0;
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshhold_6 = (50.0f);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7 = (50.0f);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8 = (50.0f);
		return;
	}
}
// System.Boolean Input2::IsPointerOverUI()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* EventSystem_t106_il2cpp_TypeInfo_var;
extern "C" bool Input2_IsPointerOverUI_m38 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		EventSystem_t106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		goto IL_0025;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t106_il2cpp_TypeInfo_var);
		EventSystem_t106 * L_1 = EventSystem_get_current_m300(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_2;
		NullCheck(L_1);
		bool L_3 = EventSystem_IsPointerOverGameObject_m301(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_2;
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}
}
// System.Boolean Input2::TouchBegin()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" bool Input2_TouchBegin_m39 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_1 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m303((&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		return 1;
	}

IL_0020:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Input2::GetFirstPoint()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" Vector2_t10  Input2_GetFirstPoint_m40 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Vector3_t15  L_1 = Input_get_mousePosition_m253(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t10  L_2 = Vector2_op_Implicit_m304(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_3 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t10  L_4 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" Vector2_t10  Input2_GetSecondPoint_m41 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Vector3_t15  L_1 = Input_get_mousePosition_m253(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t10  L_2 = Vector2_op_Implicit_m304(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_3 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t10  L_4 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Input2::GetPointerCount()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" int32_t Input2_GetPointerCount_m42 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButton_m306(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return 2;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m306(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}

IL_0024:
	{
		goto IL_002f;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_002f:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointStarted(System.Int32)
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointStarted_m43 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t107  V_1 = {0};
	Touch_t107  V_2 = {0};
	Touch_t107  V_3 = {0};
	Touch_t107  V_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonDown_m307(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_6;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonDown_m307(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_003b:
	{
		goto IL_00a5;
	}

IL_0040:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_10 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = Touch_get_phase_m303((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
	}

IL_0059:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_13 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = Touch_get_phase_m303((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
	}

IL_0072:
	{
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_008b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_16 = Input_GetTouch_m302(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		V_3 = L_16;
		int32_t L_17 = Touch_get_phase_m303((&V_3), /*hidden argument*/NULL);
		return ((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
	}

IL_008b:
	{
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)4))))
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_19 = Input_GetTouch_m302(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = Touch_get_phase_m303((&V_4), /*hidden argument*/NULL);
		return ((((int32_t)L_20) == ((int32_t)0))? 1 : 0);
	}

IL_00a5:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointEnded(System.Int32)
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointEnded_m44 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t107  V_1 = {0};
	Touch_t107  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonUp_m308(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_6;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonUp_m308(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_003b:
	{
		goto IL_0072;
	}

IL_0040:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_10 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = Touch_get_phase_m303((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_11) == ((int32_t)3))? 1 : 0);
	}

IL_0059:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_13 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = Touch_get_phase_m303((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)3))? 1 : 0);
	}

IL_0072:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointMoved(System.Int32)
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointMoved_m45 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t107  V_1 = {0};
	Touch_t107  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		return 1;
	}

IL_0028:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0031;
		}
	}
	{
		return 1;
	}

IL_0031:
	{
		goto IL_0068;
	}

IL_0036:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_8 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = Touch_get_phase_m303((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
	}

IL_004f:
	{
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_11 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = Touch_get_phase_m303((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_12) == ((int32_t)1))? 1 : 0);
	}

IL_0068:
	{
		return 0;
	}
}
// System.Single Input2::Pinch()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" float Input2_Pinch_m46 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	Touch_t107  V_1 = {0};
	Vector2_t10  V_2 = {0};
	Vector2_t10  V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector2_t10  V_7 = {0};
	Vector2_t10  V_8 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_1 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Touch_t107  L_2 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector2_t10  L_3 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		Vector2_t10  L_4 = Touch_get_deltaPosition_m309((&V_0), /*hidden argument*/NULL);
		Vector2_t10  L_5 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t10  L_6 = Touch_get_position_m305((&V_1), /*hidden argument*/NULL);
		Vector2_t10  L_7 = Touch_get_deltaPosition_m309((&V_1), /*hidden argument*/NULL);
		Vector2_t10  L_8 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		Vector2_t10  L_9 = V_2;
		Vector2_t10  L_10 = V_3;
		Vector2_t10  L_11 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		float L_12 = Vector2_get_magnitude_m311((&V_7), /*hidden argument*/NULL);
		V_4 = L_12;
		Vector2_t10  L_13 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		Vector2_t10  L_14 = Touch_get_position_m305((&V_1), /*hidden argument*/NULL);
		Vector2_t10  L_15 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_8 = L_15;
		float L_16 = Vector2_get_magnitude_m311((&V_8), /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = V_4;
		float L_18 = V_5;
		V_6 = ((float)((float)L_17-(float)L_18));
		float L_19 = V_6;
		return L_19;
	}

IL_007b:
	{
		return (0.0f);
	}
}
// System.Single Input2::Twist()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" float Input2_Twist_m47 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	Touch_t107  V_1 = {0};
	Vector2_t10  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_0 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Touch_t107  L_1 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t10  L_2 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		Vector2_t10  L_3 = Touch_get_position_m305((&V_1), /*hidden argument*/NULL);
		Vector2_t10  L_4 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___y_2);
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		float L_8 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_9 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_10 = Mathf_DeltaAngle_m312(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_3;
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = L_11;
		float L_12 = V_3;
		return L_12;
	}
}
// System.Int32 Input2::TwistInt()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" int32_t Input2_TwistInt_m48 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	Touch_t107  V_1 = {0};
	Vector2_t10  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_0 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Touch_t107  L_1 = Input_GetTouch_m302(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t10  L_2 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		Vector2_t10  L_3 = Touch_get_position_m305((&V_1), /*hidden argument*/NULL);
		Vector2_t10  L_4 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___y_2);
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		float L_8 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_9 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_10 = Mathf_DeltaAngle_m312(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_3;
		float L_12 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_13 = Mathf_Sign_m313(NULL /*static, unused*/, ((float)((float)L_11-(float)L_12)), /*hidden argument*/NULL);
		V_5 = (((int32_t)L_13));
		float L_14 = V_3;
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = L_14;
		int32_t L_15 = V_5;
		return ((-L_15));
	}
}
// System.Boolean Input2::SwipeLeft(UnityEngine.Vector2)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeLeft_m49 (Object_t * __this /* static, unused */, Vector2_t10  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___x_1);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeRight(UnityEngine.Vector2)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeRight_m50 (Object_t * __this /* static, unused */, Vector2_t10  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___x_1);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeDown(UnityEngine.Vector2)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeDown_m51 (Object_t * __this /* static, unused */, Vector2_t10  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___y_2);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeUp(UnityEngine.Vector2)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeUp_m52 (Object_t * __this /* static, unused */, Vector2_t10  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___y_2);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Input2::ContinuousSwipe()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" Vector2_t10  Input2_ContinuousSwipe_m53 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t10  V_0 = {0};
	Vector2_t10  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		Vector2_t10  L_1 = Input2_GetFirstPoint_m40(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Input2_HasPointStarted_m43(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_007f;
		}
	}
	{
		Vector2_t10  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		Vector2_t10  L_4 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___lastPos_4;
		Vector2_t10  L_5 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = Vector2_get_magnitude_m311((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_007a;
		}
	}
	{
		float L_7 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_8 = fabsf(L_7);
		float L_9 = ((&V_1)->___y_2);
		float L_10 = fabsf(L_9);
		if ((!(((float)L_8) > ((float)L_10))))
		{
			goto IL_0068;
		}
	}
	{
		float L_11 = ((&V_1)->___x_1);
		Vector2_t10  L_12 = {0};
		Vector2__ctor_m264(&L_12, L_11, (0.0f), /*hidden argument*/NULL);
		return L_12;
	}

IL_0068:
	{
		float L_13 = ((&V_1)->___y_2);
		Vector2_t10  L_14 = {0};
		Vector2__ctor_m264(&L_14, (0.0f), L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_007a:
	{
		goto IL_0085;
	}

IL_007f:
	{
		Vector2_t10  L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_15;
	}

IL_0085:
	{
		Vector2_t10  L_16 = Vector2_get_zero_m263(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Vector2 Input2::Swipe()
extern TypeInfo* Input2_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" Vector2_t10  Input2_Swipe_m54 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t10  V_0 = {0};
	Vector2_t10  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m42(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		Vector2_t10  L_1 = Input2_GetFirstPoint_m40(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Input2_HasPointEnded_m44(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007a;
		}
	}
	{
		Vector2_t10  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		Vector2_t10  L_4 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___lastPos_4;
		Vector2_t10  L_5 = Vector2_op_Subtraction_m310(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = Vector2_get_magnitude_m311((&V_1), /*hidden argument*/NULL);
		float L_7 = ((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___swipeThreshhold_6;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		float L_10 = ((&V_1)->___y_2);
		float L_11 = fabsf(L_10);
		if ((!(((float)L_9) > ((float)L_11))))
		{
			goto IL_0068;
		}
	}
	{
		float L_12 = ((&V_1)->___x_1);
		Vector2_t10  L_13 = {0};
		Vector2__ctor_m264(&L_13, L_12, (0.0f), /*hidden argument*/NULL);
		return L_13;
	}

IL_0068:
	{
		float L_14 = ((&V_1)->___y_2);
		Vector2_t10  L_15 = {0};
		Vector2__ctor_m264(&L_15, (0.0f), L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_007a:
	{
		Vector2_t10  L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t16_il2cpp_TypeInfo_var);
		((Input2_t16_StaticFields*)Input2_t16_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_16;
	}

IL_0080:
	{
		Vector2_t10  L_17 = Vector2_get_zero_m263(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
#ifndef _MSC_VER
#else
#endif
// Mathf2
#include "AssemblyU2DCSharp_Mathf2MethodDeclarations.h"

// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// System.Guid
#include "mscorlib_System_Guid.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"


// System.Void Mathf2::.ctor()
extern "C" void Mathf2__ctor_m55 (Mathf2_t17 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mathf2::.cctor()
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" void Mathf2__cctor_m56 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t15  L_0 = {0};
		Vector3__ctor_m249(&L_0, (-9999.0f), (-9999.0f), (-9999.0f), /*hidden argument*/NULL);
		((Mathf2_t17_StaticFields*)Mathf2_t17_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0 = L_0;
		return;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHitCenterScreenIgnore0_m57 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		LayerMask_t95  L_0 = LayerMask_op_Implicit_m315(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_1 = Mathf2_WhatDidWeHitCenterScreen_m58(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHitCenterScreen_m58 (Object_t * __this /* static, unused */, LayerMask_t95  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t10  L_2 = {0};
		Vector2__ctor_m264(&L_2, ((float)((float)(((float)L_0))*(float)(0.5f))), ((float)((float)(((float)L_1))*(float)(0.5f))), /*hidden argument*/NULL);
		LayerMask_t95  L_3 = ___lm;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_4 = Mathf2_WhatDidWeHit_m62(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHitCenterScreen_m59 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		LayerMask_t95  L_0 = LayerMask_op_Implicit_m315(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_1 = Mathf2_WhatDidWeHitCenterScreen_m58(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHit_m60 (Object_t * __this /* static, unused */, Vector2_t10  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t10  L_0 = ___v;
		LayerMask_t95  L_1 = LayerMask_op_Implicit_m315(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_2 = Mathf2_WhatDidWeHit_m62(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCam(UnityEngine.Vector2)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHitCam_m61 (Object_t * __this /* static, unused */, Vector2_t10  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t10  L_0 = ___v;
		LayerMask_t95  L_1 = LayerMask_op_Implicit_m315(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_2 = Mathf2_WhatDidWeHit_m62(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHit_m62 (Object_t * __this /* static, unused */, Vector2_t10  ___v, LayerMask_t95  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t96  V_0 = {0};
	{
		Camera_t3 * L_0 = Camera_get_main_m252(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t10  L_1 = ___v;
		Vector3_t15  L_2 = Vector2_op_Implicit_m318(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t96  L_3 = Camera_ScreenPointToRay_m254(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t96  L_4 = V_0;
		LayerMask_t95  L_5 = ___lm;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		RaycastHit_t94  L_6 = Mathf2_WhatDidWeHit_m63(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
extern TypeInfo* RaycastHit_t94_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t94  Mathf2_WhatDidWeHit_m63 (Object_t * __this /* static, unused */, Ray_t96  ___ray, LayerMask_t95  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RaycastHit_t94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t94  V_0 = {0};
	{
		Initobj (RaycastHit_t94_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		Vector3_t15  L_0 = ((Mathf2_t17_StaticFields*)Mathf2_t17_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0;
		RaycastHit_set_point_m319((&V_0), L_0, /*hidden argument*/NULL);
		Ray_t96  L_1 = ___ray;
		LayerMask_t95  L_2 = ___lm;
		int32_t L_3 = LayerMask_op_Implicit_m320(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Physics_Raycast_m321(NULL /*static, unused*/, L_1, (&V_0), (3000.0f), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		RaycastHit_t94  L_5 = V_0;
		return L_5;
	}

IL_002e:
	{
		RaycastHit_t94  L_6 = V_0;
		return L_6;
	}
}
// System.String Mathf2::Pad0s(System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_Pad0s_m64 (Object_t * __this /* static, unused */, int32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___n;
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = Int32_ToString_m322((&___n), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral6, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001a:
	{
		int32_t L_3 = ___n;
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_4 = Int32_ToString_m322((&___n), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral7, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0034:
	{
		String_t* L_6 = Int32_ToString_m322((&___n), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single Mathf2::Round10th(System.Single)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" float Mathf2_Round10th_m65 (Object_t * __this /* static, unused */, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_1 = roundf(((float)((float)L_0*(float)(10.0f))));
		return ((float)((float)L_1/(float)(10.0f)));
	}
}
// System.String Mathf2::GenUUID()
extern TypeInfo* Guid_t108_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_GenUUID_m66 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t108  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t108_il2cpp_TypeInfo_var);
		Guid_t108  L_0 = Guid_NewGuid_m324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Guid_ToString_m325((&V_0), (String_t*) &_stringLiteral8, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" Vector3_t15  Mathf2_RoundVector3_m67 (Object_t * __this /* static, unused */, Vector3_t15  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_1 = Mathf2_round_m69(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ((&___v)->___y_2);
		float L_3 = Mathf2_round_m69(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = ((&___v)->___z_3);
		float L_5 = Mathf2_round_m69(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector3_t15  L_6 = {0};
		Vector3__ctor_m249(&L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Quaternion Mathf2::RandRot()
extern "C" Quaternion_t13  Mathf2_RandRot_m68 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Random_Range_m326(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		int32_t L_1 = Random_Range_m326(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		int32_t L_2 = Random_Range_m326(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		Quaternion_t13  L_3 = Quaternion_Euler_m278(NULL /*static, unused*/, (((float)L_0)), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Mathf2::round(System.Single)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" float Mathf2_round_m69 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_1 = roundf(L_0);
		return L_1;
	}
}
// System.Single Mathf2::String2Float(System.String)
extern "C" float Mathf2_String2Float_m70 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		float L_1 = Single_Parse_m327(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Mathf2::isNumeric(System.String)
extern "C" bool Mathf2_isNumeric_m71 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		String_t* L_0 = ___str;
		bool L_1 = Int32_TryParse_m328(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Int32 Mathf2::String2Int(System.String)
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" int32_t Mathf2_String2Int_m72 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_1 = Mathf2_String2Float_m70(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = floorf(L_1);
		return (((int32_t)L_2));
	}
}
// UnityEngine.Color Mathf2::String2Color(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" Color_t90  Mathf2_String2Color_m73 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t109* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m329(L_0, (String_t*) &_stringLiteral9, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m329(L_3, (String_t*) &_stringLiteral10, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t110* L_7 = ((CharU5BU5D_t110*)SZArrayNew(CharU5BU5D_t110_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral11);
		uint16_t L_8 = String_get_Chars_m330((String_t*) &_stringLiteral11, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t109* L_9 = String_Split_m331(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t109* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_19 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		int32_t L_20 = 3;
		float L_21 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20)), /*hidden argument*/NULL);
		Color_t90  L_22 = {0};
		Color__ctor_m241(&L_22, L_12, L_15, L_18, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" Vector3_t15  Mathf2_String2Vector3_m74 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t109* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m329(L_0, (String_t*) &_stringLiteral12, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m329(L_3, (String_t*) &_stringLiteral10, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t110* L_7 = ((CharU5BU5D_t110*)SZArrayNew(CharU5BU5D_t110_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral11);
		uint16_t L_8 = String_get_Chars_m330((String_t*) &_stringLiteral11, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t109* L_9 = String_Split_m331(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t109* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		Vector3_t15  L_19 = {0};
		Vector3__ctor_m249(&L_19, L_12, L_15, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" Vector2_t10  Mathf2_String2Vector2_m75 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t109* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m329(L_0, (String_t*) &_stringLiteral12, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m329(L_3, (String_t*) &_stringLiteral10, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t110* L_7 = ((CharU5BU5D_t110*)SZArrayNew(CharU5BU5D_t110_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral11);
		uint16_t L_8 = String_get_Chars_m330((String_t*) &_stringLiteral11, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t109* L_9 = String_Split_m331(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t109* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		Vector2_t10  L_16 = {0};
		Vector2__ctor_m264(&L_16, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" float Mathf2_RoundFraction_m76 (Object_t * __this /* static, unused */, float ___val, float ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___val;
		float L_1 = ___denominator;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_2 = floorf(((float)((float)L_0*(float)L_1)));
		float L_3 = ___denominator;
		return ((float)((float)L_2/(float)L_3));
	}
}
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t17_il2cpp_TypeInfo_var;
extern "C" Quaternion_t13  Mathf2_String2Quat_m77 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Mathf2_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t109* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m329(L_0, (String_t*) &_stringLiteral12, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m329(L_3, (String_t*) &_stringLiteral10, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t110* L_7 = ((CharU5BU5D_t110*)SZArrayNew(CharU5BU5D_t110_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral11);
		uint16_t L_8 = String_get_Chars_m330((String_t*) &_stringLiteral11, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t109* L_9 = String_Split_m331(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t109* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t17_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		StringU5BU5D_t109* L_19 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		int32_t L_20 = 3;
		float L_21 = Mathf2_String2Float_m70(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20)), /*hidden argument*/NULL);
		Quaternion_t13  L_22 = {0};
		Quaternion__ctor_m289(&L_22, L_12, L_15, L_18, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" Vector3_t15  Mathf2_UnsignedVector3_m78 (Object_t * __this /* static, unused */, Vector3_t15  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = ((&___v)->___y_2);
		float L_3 = fabsf(L_2);
		float L_4 = ((&___v)->___z_3);
		float L_5 = fabsf(L_4);
		Vector3_t15  L_6 = {0};
		Vector3__ctor_m249(&L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String Mathf2::GetUnixTime()
extern TypeInfo* DateTime_t111_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_GetUnixTime_m79 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t111  V_0 = {0};
	double V_1 = 0.0;
	TimeSpan_t112  V_2 = {0};
	{
		DateTime__ctor_m332((&V_0), ((int32_t)1970), 1, 1, 8, 0, 0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t111_il2cpp_TypeInfo_var);
		DateTime_t111  L_0 = DateTime_get_UtcNow_m333(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t111  L_1 = V_0;
		TimeSpan_t112  L_2 = DateTime_op_Subtraction_m334(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		double L_3 = TimeSpan_get_TotalSeconds_m335((&V_2), /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Double_ToString_m336((&V_1), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetPositionString_m80 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_position_m247(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Vector3_ToString_m337((&V_0), (String_t*) &_stringLiteral13, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetRotationString_m81 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Quaternion_t13  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Quaternion_t13  L_1 = Transform_get_rotation_m245(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Quaternion_ToString_m338((&V_0), (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetScaleString_m82 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_localScale_m339(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Vector3_ToString_m337((&V_0), (String_t*) &_stringLiteral15, /*hidden argument*/NULL);
		return L_2;
	}
}
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
#ifndef _MSC_VER
#else
#endif
// Raycaster
#include "AssemblyU2DCSharp_RaycasterMethodDeclarations.h"



// System.Void Raycaster::.ctor()
extern "C" void Raycaster__ctor_m83 (Raycaster_t18 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern "C" void Raycaster_RaycastIt_m84 (Raycaster_t18 * __this, Camera_t3 * ___cam, Vector2_t10  ___pos, float ___distance, const MethodInfo* method)
{
	Ray_t96  V_0 = {0};
	RaycastHit_t94  V_1 = {0};
	String_t* V_2 = {0};
	{
		Camera_t3 * L_0 = ___cam;
		Vector2_t10  L_1 = ___pos;
		Vector3_t15  L_2 = Vector2_op_Implicit_m318(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t96  L_3 = Camera_ScreenPointToRay_m254(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t96  L_4 = V_0;
		float L_5 = ___distance;
		bool L_6 = Physics_Raycast_m255(NULL /*static, unused*/, L_4, (&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		Transform_t11 * L_7 = RaycastHit_get_transform_m258((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m259(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
	}

IL_0028:
	{
		return;
	}
}
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
#ifndef _MSC_VER
#else
#endif
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueueMethodDeclarations.h"



// System.Void SetRenderQueue::.ctor()
extern TypeInfo* Int32U5BU5D_t19_il2cpp_TypeInfo_var;
extern "C" void SetRenderQueue__ctor_m85 (SetRenderQueue_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t19* L_0 = ((Int32U5BU5D_t19*)SZArrayNew(Int32U5BU5D_t19_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueue::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var;
extern "C" void SetRenderQueue_Awake_m86 (SetRenderQueue_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	MaterialU5BU5D_t113* V_0 = {0};
	int32_t V_1 = 0;
	{
		Renderer_t8 * L_0 = Component_GetComponent_TisRenderer_t8_m261(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t113* L_1 = Renderer_get_materials_m340(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0027;
	}

IL_0013:
	{
		MaterialU5BU5D_t113* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Int32U5BU5D_t19* L_5 = (__this->___m_queues_2);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_2, L_4)));
		Material_set_renderQueue_m341((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_2, L_4)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7)), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_1;
		MaterialU5BU5D_t113* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) >= ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_11 = V_1;
		Int32U5BU5D_t19* L_12 = (__this->___m_queues_2);
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0013;
		}
	}

IL_003e:
	{
		return;
	}
}
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
#ifndef _MSC_VER
#else
#endif
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildrenMethodDeclarations.h"

struct Component_t103;
struct RendererU5BU5D_t114;
struct Component_t103;
struct ObjectU5BU5D_t115;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t115* Component_GetComponentsInChildren_TisObject_t_m343_gshared (Component_t103 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m343(__this, p0, method) (( ObjectU5BU5D_t115* (*) (Component_t103 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m343_gshared)(__this, p0, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t8_m342(__this, p0, method) (( RendererU5BU5D_t114* (*) (Component_t103 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m343_gshared)(__this, p0, method)


// System.Void SetRenderQueueChildren::.ctor()
extern TypeInfo* Int32U5BU5D_t19_il2cpp_TypeInfo_var;
extern "C" void SetRenderQueueChildren__ctor_m87 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t19* L_0 = ((Int32U5BU5D_t19*)SZArrayNew(Int32U5BU5D_t19_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueueChildren::Start()
extern "C" void SetRenderQueueChildren_Start_m88 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m259(L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m248(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueueChildren::Awake()
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var;
extern "C" void SetRenderQueueChildren_Awake_m89 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t114* V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t113* V_2 = {0};
	int32_t V_3 = 0;
	{
		RendererU5BU5D_t114* L_0 = Component_GetComponentsInChildren_TisRenderer_t8_m342(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003d;
	}

IL_000f:
	{
		RendererU5BU5D_t114* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(Renderer_t8 **)(Renderer_t8 **)SZArrayLdElema(L_1, L_3)));
		MaterialU5BU5D_t113* L_4 = Renderer_get_materials_m340((*(Renderer_t8 **)(Renderer_t8 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		V_2 = L_4;
		V_3 = 0;
		goto IL_0030;
	}

IL_001f:
	{
		MaterialU5BU5D_t113* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_5, L_7)));
		Material_set_renderQueue_m341((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_5, L_7)), ((int32_t)3020), /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_9 = V_3;
		MaterialU5BU5D_t113* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_12 = V_1;
		RendererU5BU5D_t114* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
#ifndef _MSC_VER
#else
#endif
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManuallyMethodDeclarations.h"

// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"


// System.Void SetRotationManually::.ctor()
extern "C" void SetRotationManually__ctor_m90 (SetRotationManually_t24 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderX()
extern "C" void SetRotationManually_UpdateRotationFromSliderX_m91 (SetRotationManually_t24 * __this, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	Quaternion_t13  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t13  L_1 = Transform_get_rotation_m245(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t15  L_2 = Quaternion_get_eulerAngles_m246((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		Slider_t22 * L_4 = (__this->___sliderX_2);
		NullCheck(L_4);
		float L_5 = Slider_get_value_m344(L_4, /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___y_2);
		float L_7 = ((&V_0)->___z_3);
		Quaternion_t13  L_8 = Quaternion_Euler_m278(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m277(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderY()
extern "C" void SetRotationManually_UpdateRotationFromSliderY_m92 (SetRotationManually_t24 * __this, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	Quaternion_t13  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t13  L_1 = Transform_get_rotation_m245(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t15  L_2 = Quaternion_get_eulerAngles_m246((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___x_1);
		Slider_t22 * L_5 = (__this->___sliderY_3);
		NullCheck(L_5);
		float L_6 = Slider_get_value_m344(L_5, /*hidden argument*/NULL);
		float L_7 = ((&V_0)->___z_3);
		Quaternion_t13  L_8 = Quaternion_Euler_m278(NULL /*static, unused*/, L_4, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m277(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderZ()
extern "C" void SetRotationManually_UpdateRotationFromSliderZ_m93 (SetRotationManually_t24 * __this, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	Quaternion_t13  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t13  L_1 = Transform_get_rotation_m245(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t15  L_2 = Quaternion_get_eulerAngles_m246((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___x_1);
		float L_5 = ((&V_0)->___y_2);
		Slider_t22 * L_6 = (__this->___sliderZ_4);
		NullCheck(L_6);
		float L_7 = Slider_get_value_m344(L_6, /*hidden argument*/NULL);
		Quaternion_t13  L_8 = Quaternion_Euler_m278(NULL /*static, unused*/, L_4, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m277(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::Update()
extern "C" void SetRotationManually_Update_m94 (SetRotationManually_t24 * __this, const MethodInfo* method)
{
	Quaternion_t13  V_0 = {0};
	Vector3_t15  V_1 = {0};
	{
		Text_t23 * L_0 = (__this->___rotationvalue_5);
		Transform_t11 * L_1 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t13  L_2 = Transform_get_rotation_m245(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t15  L_3 = Quaternion_get_eulerAngles_m246((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Vector3_ToString_m345((&V_1), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
#ifndef _MSC_VER
#else
#endif
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoopMethodDeclarations.h"

// WorldNav
#include "AssemblyU2DCSharp_WorldNavMethodDeclarations.h"
struct Component_t103;
struct Raycaster_t18;
// Declaration !!0 UnityEngine.Component::GetComponent<Raycaster>()
// !!0 UnityEngine.Component::GetComponent<Raycaster>()
#define Component_GetComponent_TisRaycaster_t18_m346(__this, method) (( Raycaster_t18 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void WorldLoop::.ctor()
extern "C" void WorldLoop__ctor_m95 (WorldLoop_t25 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldLoop::Awake()
extern const MethodInfo* Component_GetComponent_TisRaycaster_t18_m346_MethodInfo_var;
extern "C" void WorldLoop_Awake_m96 (WorldLoop_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRaycaster_t18_m346_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	{
		Raycaster_t18 * L_0 = Component_GetComponent_TisRaycaster_t18_m346(__this, /*hidden argument*/Component_GetComponent_TisRaycaster_t18_m346_MethodInfo_var);
		__this->___raycaster_3 = L_0;
		return;
	}
}
// System.Void WorldLoop::Start()
extern "C" void WorldLoop_Start_m97 (WorldLoop_t25 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WorldLoop::Update()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern "C" void WorldLoop_Update_m98 (WorldLoop_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t107  V_0 = {0};
	Vector2_t10  V_1 = {0};
	Touch_t107  V_2 = {0};
	Vector2_t10  V_3 = {0};
	Touch_t107  V_4 = {0};
	Touch_t107  V_5 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_1 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t10  L_2 = Touch_get_position_m305((&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___y_2);
		int32_t L_4 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)((float)((float)(((float)L_4))*(float)(0.5f)))))))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		WorldNav_Walk_m103(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_5 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t10  L_6 = Touch_get_position_m305((&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___y_2);
		int32_t L_8 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_7) < ((float)((float)((float)(((float)L_8))*(float)(0.5f)))))))
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		WorldNav_Walk_m103(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_9 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_4 = L_9;
		int32_t L_10 = Touch_get_phase_m303((&V_4), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00a3;
		}
	}
	{
		Raycaster_t18 * L_11 = (__this->___raycaster_3);
		Camera_t3 * L_12 = (__this->___cam_2);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		Touch_t107  L_13 = Input_GetTouch_m302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_13;
		Vector2_t10  L_14 = Touch_get_position_m305((&V_5), /*hidden argument*/NULL);
		NullCheck(L_11);
		Raycaster_RaycastIt_m84(L_11, L_12, L_14, (100.0f), /*hidden argument*/NULL);
	}

IL_00a3:
	{
		return;
	}
}
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
#ifndef _MSC_VER
#else
#endif

struct Component_t103;
struct Camera_t3;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m347(__this, method) (( Camera_t3 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void WorldNav::.ctor()
extern "C" void WorldNav__ctor_m99 (WorldNav_t26 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldNav::.cctor()
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern "C" void WorldNav__cctor_m100 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___coeff_2 = (0.2f);
		return;
	}
}
// System.Void WorldNav::Awake()
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t3_m347_MethodInfo_var;
extern "C" void WorldNav_Awake_m101 (WorldNav_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Component_GetComponent_TisCamera_t3_m347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = (__this->___mainCharacter_3);
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4 = L_0;
		GameObject_t2 * L_1 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m273(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t11 * L_3 = Transform_Find_m348(L_2, (String_t*) &_stringLiteral16, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = Transform_Find_m348(L_3, (String_t*) &_stringLiteral17, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_t3 * L_5 = Component_GetComponent_TisCamera_t3_m347(L_4, /*hidden argument*/Component_GetComponent_TisCamera_t3_m347_MethodInfo_var);
		((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___camCharacter_5 = L_5;
		return;
	}
}
// System.Void WorldNav::Update()
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void WorldNav_Update_m102 (WorldNav_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		bool L_0 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___walking_6;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (__this->___dirwalking_7);
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		WorldNav_Walk_m103(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_2 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_13;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t2 * L_3 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_3);
		GameObject_SetActive_m238(L_3, 1, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void WorldNav::Walk(System.Int32)
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern "C" void WorldNav_Walk_m103 (Object_t * __this /* static, unused */, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t15  V_0 = {0};
	Vector3_t15  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		Camera_t3 * L_0 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___camCharacter_5;
		NullCheck(L_0);
		Transform_t11 * L_1 = Component_get_transform_m243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t15  L_2 = Transform_get_forward_m282(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___coeff_2;
		float L_4 = ((&V_0)->___x_1);
		float L_5 = ((&V_0)->___z_3);
		Vector3_t15  L_6 = {0};
		Vector3__ctor_m249(&L_6, L_4, (0.0f), L_5, /*hidden argument*/NULL);
		Vector3_t15  L_7 = Vector3_op_Multiply_m349(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = ___dir;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		GameObject_t2 * L_9 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_9);
		Transform_t11 * L_10 = GameObject_get_transform_m273(L_9, /*hidden argument*/NULL);
		Transform_t11 * L_11 = L_10;
		NullCheck(L_11);
		Vector3_t15  L_12 = Transform_get_position_m247(L_11, /*hidden argument*/NULL);
		Vector3_t15  L_13 = V_1;
		Vector3_t15  L_14 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_position_m276(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		GameObject_t2 * L_15 = ((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_15);
		Transform_t11 * L_16 = GameObject_get_transform_m273(L_15, /*hidden argument*/NULL);
		Transform_t11 * L_17 = L_16;
		NullCheck(L_17);
		Vector3_t15  L_18 = Transform_get_position_m247(L_17, /*hidden argument*/NULL);
		Vector3_t15  L_19 = V_1;
		Vector3_t15  L_20 = Vector3_op_Subtraction_m275(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m276(L_17, L_20, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern "C" void WorldNav_WalkDirTrue_m104 (WorldNav_t26 * __this, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___walking_6 = 1;
		int32_t L_0 = ___dir;
		__this->___dirwalking_7 = L_0;
		GameObject_t2 * L_1 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_1);
		GameObject_SetActive_m238(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern TypeInfo* WorldNav_t26_il2cpp_TypeInfo_var;
extern "C" void WorldNav_WalkDirFalse_m105 (WorldNav_t26 * __this, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t26_il2cpp_TypeInfo_var);
		((WorldNav_t26_StaticFields*)WorldNav_t26_il2cpp_TypeInfo_var->static_fields)->___walking_6 = 0;
		int32_t L_0 = ___dir;
		__this->___dirwalking_7 = L_0;
		GameObject_t2 * L_1 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_1);
		GameObject_SetActive_m238(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// Basics
#include "AssemblyU2DCSharp_Basics.h"
#ifndef _MSC_VER
#else
#endif
// Basics
#include "AssemblyU2DCSharp_BasicsMethodDeclarations.h"

// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0.h"
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen.h"
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen.h"
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_genMethodDeclarations.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0MethodDeclarations.h"
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_genMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_genMethodDeclarations.h"
struct TweenSettingsExtensions_t100;
struct Tweener_t99;
struct TweenSettingsExtensions_t100;
struct Object_t;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
extern "C" Object_t * TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define TweenSettingsExtensions_SetRelative_TisObject_t_m352(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweener_t99_m351(__this /* static, unused */, p0, method) (( Tweener_t99 * (*) (Object_t * /* static, unused */, Tweener_t99 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared)(__this /* static, unused */, p0, method)
struct TweenSettingsExtensions_t100;
struct Tweener_t99;
struct TweenSettingsExtensions_t100;
struct Object_t;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C" Object_t * TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared (Object_t * __this /* static, unused */, Object_t * p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define TweenSettingsExtensions_SetLoops_TisObject_t_m354(__this /* static, unused */, p0, p1, p2, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared)(__this /* static, unused */, p0, p1, p2, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweener_t99_m353(__this /* static, unused */, p0, p1, p2, method) (( Tweener_t99 * (*) (Object_t * /* static, unused */, Tweener_t99 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct TweenSettingsExtensions_t100;
struct TweenerCore_3_t116;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t116_m355(__this /* static, unused */, p0, method) (( TweenerCore_3_t116 * (*) (Object_t * /* static, unused */, TweenerCore_3_t116 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m352_gshared)(__this /* static, unused */, p0, method)
struct TweenSettingsExtensions_t100;
struct TweenerCore_3_t116;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t116_m356(__this /* static, unused */, p0, p1, p2, method) (( TweenerCore_3_t116 * (*) (Object_t * /* static, unused */, TweenerCore_3_t116 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void Basics::.ctor()
extern "C" void Basics__ctor_m106 (Basics_t27 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Basics::Start()
extern TypeInfo* DOTween_t119_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t120_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t121_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m357_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m358_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweener_t99_m353_MethodInfo_var;
extern const MethodInfo* Basics_U3CStartU3Em__0_m108_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m361_MethodInfo_var;
extern const MethodInfo* Basics_U3CStartU3Em__1_m109_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m362_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t116_m355_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t116_m356_MethodInfo_var;
extern "C" void Basics_Start_m107 (Basics_t27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		DOGetter_1_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		DOSetter_1_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		Nullable_1__ctor_m357_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		Nullable_1__ctor_m358_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		TweenSettingsExtensions_SetLoops_TisTweener_t99_m353_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		Basics_U3CStartU3Em__0_m108_MethodInfo_var = il2cpp_codegen_method_info_from_index(12);
		DOGetter_1__ctor_m361_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		Basics_U3CStartU3Em__1_m109_MethodInfo_var = il2cpp_codegen_method_info_from_index(14);
		DOSetter_1__ctor_m362_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t116_m355_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t116_m356_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	{
		Nullable_1_t117  L_0 = {0};
		Nullable_1__ctor_m357(&L_0, 0, /*hidden argument*/Nullable_1__ctor_m357_MethodInfo_var);
		Nullable_1_t117  L_1 = {0};
		Nullable_1__ctor_m357(&L_1, 1, /*hidden argument*/Nullable_1__ctor_m357_MethodInfo_var);
		Nullable_1_t118  L_2 = {0};
		Nullable_1__ctor_m358(&L_2, 2, /*hidden argument*/Nullable_1__ctor_m358_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t119_il2cpp_TypeInfo_var);
		DOTween_Init_m359(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Transform_t11 * L_3 = (__this->___cubeA_2);
		Vector3_t15  L_4 = {0};
		Vector3__ctor_m249(&L_4, (-2.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		Tweener_t99 * L_5 = ShortcutExtensions_DOMove_m360(NULL /*static, unused*/, L_3, L_4, (1.0f), 0, /*hidden argument*/NULL);
		Tweener_t99 * L_6 = TweenSettingsExtensions_SetRelative_TisTweener_t99_m351(NULL /*static, unused*/, L_5, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var);
		TweenSettingsExtensions_SetLoops_TisTweener_t99_m353(NULL /*static, unused*/, L_6, (-1), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweener_t99_m353_MethodInfo_var);
		IntPtr_t L_7 = { (void*)Basics_U3CStartU3Em__0_m108_MethodInfo_var };
		DOGetter_1_t120 * L_8 = (DOGetter_1_t120 *)il2cpp_codegen_object_new (DOGetter_1_t120_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m361(L_8, __this, L_7, /*hidden argument*/DOGetter_1__ctor_m361_MethodInfo_var);
		IntPtr_t L_9 = { (void*)Basics_U3CStartU3Em__1_m109_MethodInfo_var };
		DOSetter_1_t121 * L_10 = (DOSetter_1_t121 *)il2cpp_codegen_object_new (DOSetter_1_t121_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m362(L_10, __this, L_9, /*hidden argument*/DOSetter_1__ctor_m362_MethodInfo_var);
		Vector3_t15  L_11 = {0};
		Vector3__ctor_m249(&L_11, (-2.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		TweenerCore_3_t116 * L_12 = DOTween_To_m363(NULL /*static, unused*/, L_8, L_10, L_11, (1.0f), /*hidden argument*/NULL);
		TweenerCore_3_t116 * L_13 = TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t116_m355(NULL /*static, unused*/, L_12, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t116_m355_MethodInfo_var);
		TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t116_m356(NULL /*static, unused*/, L_13, (-1), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t116_m356_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector3 Basics::<Start>m__0()
extern "C" Vector3_t15  Basics_U3CStartU3Em__0_m108 (Basics_t27 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___cubeB_3);
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_position_m247(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern "C" void Basics_U3CStartU3Em__1_m109 (Basics_t27 * __this, Vector3_t15  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___cubeB_3);
		Vector3_t15  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_position_m276(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
#ifndef _MSC_VER
#else
#endif
// Sequences
#include "AssemblyU2DCSharp_SequencesMethodDeclarations.h"

// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_Sequence.h"
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensionsMethodDeclarations.h"
struct TweenSettingsExtensions_t100;
struct Sequence_t122;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Sequence>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Sequence>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisSequence_t122_m364(__this /* static, unused */, p0, p1, p2, method) (( Sequence_t122 * (*) (Object_t * /* static, unused */, Sequence_t122 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m354_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void Sequences::.ctor()
extern "C" void Sequences__ctor_m110 (Sequences_t28 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sequences::Start()
extern TypeInfo* DOTween_t119_il2cpp_TypeInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisSequence_t122_m364_MethodInfo_var;
extern "C" void Sequences_Start_m111 (Sequences_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		TweenSettingsExtensions_SetLoops_TisSequence_t122_m364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483666);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t122 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t119_il2cpp_TypeInfo_var);
		Sequence_t122 * L_0 = DOTween_Sequence_m365(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Sequence_t122 * L_1 = V_0;
		Transform_t11 * L_2 = (__this->___target_2);
		Tweener_t99 * L_3 = ShortcutExtensions_DOMoveY_m366(NULL /*static, unused*/, L_2, (2.0f), (1.0f), 0, /*hidden argument*/NULL);
		TweenSettingsExtensions_Append_m367(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Sequence_t122 * L_4 = V_0;
		Transform_t11 * L_5 = (__this->___target_2);
		Vector3_t15  L_6 = {0};
		Vector3__ctor_m249(&L_6, (0.0f), (135.0f), (0.0f), /*hidden argument*/NULL);
		Tweener_t99 * L_7 = ShortcutExtensions_DORotate_m250(NULL /*static, unused*/, L_5, L_6, (1.0f), 0, /*hidden argument*/NULL);
		TweenSettingsExtensions_Join_m368(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		Sequence_t122 * L_8 = V_0;
		Transform_t11 * L_9 = (__this->___target_2);
		Tweener_t99 * L_10 = ShortcutExtensions_DOScaleY_m369(NULL /*static, unused*/, L_9, (0.2f), (1.0f), /*hidden argument*/NULL);
		TweenSettingsExtensions_Append_m367(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		Sequence_t122 * L_11 = V_0;
		Transform_t11 * L_12 = (__this->___target_2);
		Sequence_t122 * L_13 = V_0;
		float L_14 = TweenExtensions_Duration_m370(NULL /*static, unused*/, L_13, 1, /*hidden argument*/NULL);
		Tweener_t99 * L_15 = ShortcutExtensions_DOMoveX_m371(NULL /*static, unused*/, L_12, (4.0f), L_14, 0, /*hidden argument*/NULL);
		Tweener_t99 * L_16 = TweenSettingsExtensions_SetRelative_TisTweener_t99_m351(NULL /*static, unused*/, L_15, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t99_m351_MethodInfo_var);
		TweenSettingsExtensions_Insert_m372(NULL /*static, unused*/, L_11, (0.0f), L_16, /*hidden argument*/NULL);
		Sequence_t122 * L_17 = V_0;
		TweenSettingsExtensions_SetLoops_TisSequence_t122_m364(NULL /*static, unused*/, L_17, 4, 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisSequence_t122_m364_MethodInfo_var);
		return;
	}
}
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
#ifndef _MSC_VER
#else
#endif
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"


// System.Void OrbitCamera::.ctor()
extern "C" void OrbitCamera__ctor_m112 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	{
		__this->___Distance_3 = (10.0f);
		__this->___DistanceMin_4 = (5.0f);
		__this->___DistanceMax_5 = (15.0f);
		__this->___X_MouseSensitivity_10 = (5.0f);
		__this->___Y_MouseSensitivity_11 = (5.0f);
		__this->___MouseWheelSensitivity_12 = (5.0f);
		__this->___Y_MinLimit_13 = (15.0f);
		__this->___Y_MaxLimit_14 = (70.0f);
		__this->___DistanceSmooth_15 = (0.025f);
		Vector3_t15  L_0 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___desiredPosition_17 = L_0;
		__this->___X_Smooth_18 = (0.05f);
		__this->___Y_Smooth_19 = (0.1f);
		Vector3_t15  L_1 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___position_23 = L_1;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Start()
extern "C" void OrbitCamera_Start_m113 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___TargetLookAt_2);
		NullCheck(L_0);
		Transform_t11 * L_1 = Component_get_transform_m243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t15  L_2 = Transform_get_position_m247(L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = GameObject_get_transform_m273(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t15  L_5 = Transform_get_position_m247(L_4, /*hidden argument*/NULL);
		float L_6 = Vector3_Distance_m374(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		__this->___Distance_3 = L_6;
		float L_7 = (__this->___Distance_3);
		float L_8 = (__this->___DistanceMax_5);
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0048;
		}
	}
	{
		float L_9 = (__this->___Distance_3);
		__this->___DistanceMax_5 = L_9;
	}

IL_0048:
	{
		float L_10 = (__this->___Distance_3);
		__this->___startingDistance_6 = L_10;
		OrbitCamera_Reset_m120(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Update()
extern "C" void OrbitCamera_Update_m114 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OrbitCamera::LateUpdate()
extern "C" void OrbitCamera_LateUpdate_m115 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___TargetLookAt_2);
		bool L_1 = Object_op_Equality_m375(NULL /*static, unused*/, L_0, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		OrbitCamera_HandlePlayerInput_m116(__this, /*hidden argument*/NULL);
		OrbitCamera_CalculateDesiredPosition_m117(__this, /*hidden argument*/NULL);
		OrbitCamera_UpdatePosition_m119(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::HandlePlayerInput()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_HandlePlayerInput_m116 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.01f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m306(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		float L_1 = (__this->___mouseX_8);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m376(NULL /*static, unused*/, (String_t*) &_stringLiteral18, /*hidden argument*/NULL);
		float L_3 = (__this->___X_MouseSensitivity_10);
		__this->___mouseX_8 = ((float)((float)L_1+(float)((float)((float)L_2*(float)L_3))));
		float L_4 = (__this->___mouseY_9);
		float L_5 = Input_GetAxis_m376(NULL /*static, unused*/, (String_t*) &_stringLiteral19, /*hidden argument*/NULL);
		float L_6 = (__this->___Y_MouseSensitivity_11);
		__this->___mouseY_9 = ((float)((float)L_4-(float)((float)((float)L_5*(float)L_6))));
	}

IL_004d:
	{
		float L_7 = (__this->___mouseY_9);
		float L_8 = (__this->___Y_MinLimit_13);
		float L_9 = (__this->___Y_MaxLimit_14);
		float L_10 = OrbitCamera_ClampAngle_m121(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->___mouseY_9 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m376(NULL /*static, unused*/, (String_t*) &_stringLiteral20, /*hidden argument*/NULL);
		float L_12 = V_0;
		if ((((float)L_11) < ((float)((-L_12)))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxis_m376(NULL /*static, unused*/, (String_t*) &_stringLiteral20, /*hidden argument*/NULL);
		float L_14 = V_0;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_00bb;
		}
	}

IL_008c:
	{
		float L_15 = (__this->___Distance_3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		float L_16 = Input_GetAxis_m376(NULL /*static, unused*/, (String_t*) &_stringLiteral20, /*hidden argument*/NULL);
		float L_17 = (__this->___MouseWheelSensitivity_12);
		float L_18 = (__this->___DistanceMin_4);
		float L_19 = (__this->___DistanceMax_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_20 = Mathf_Clamp_m274(NULL /*static, unused*/, ((float)((float)L_15-(float)((float)((float)L_16*(float)L_17)))), L_18, L_19, /*hidden argument*/NULL);
		__this->___desiredDistance_7 = L_20;
	}

IL_00bb:
	{
		return;
	}
}
// System.Void OrbitCamera::CalculateDesiredPosition()
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_CalculateDesiredPosition_m117 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distance_3);
		float L_1 = (__this->___desiredDistance_7);
		float* L_2 = &(__this->___velocityDistance_16);
		float L_3 = (__this->___DistanceSmooth_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_4 = Mathf_SmoothDamp_m377(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->___Distance_3 = L_4;
		float L_5 = (__this->___mouseY_9);
		float L_6 = (__this->___mouseX_8);
		float L_7 = (__this->___Distance_3);
		Vector3_t15  L_8 = OrbitCamera_CalculatePosition_m118(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->___desiredPosition_17 = L_8;
		return;
	}
}
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern "C" Vector3_t15  OrbitCamera_CalculatePosition_m118 (OrbitCamera_t29 * __this, float ___rotationX, float ___rotationY, float ___distance, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	Quaternion_t13  V_1 = {0};
	{
		float L_0 = ___distance;
		Vector3__ctor_m249((&V_0), (0.0f), (0.0f), ((-L_0)), /*hidden argument*/NULL);
		float L_1 = ___rotationX;
		float L_2 = ___rotationY;
		Quaternion_t13  L_3 = Quaternion_Euler_m278(NULL /*static, unused*/, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t11 * L_4 = (__this->___TargetLookAt_2);
		NullCheck(L_4);
		Vector3_t15  L_5 = Transform_get_position_m247(L_4, /*hidden argument*/NULL);
		Quaternion_t13  L_6 = V_1;
		Vector3_t15  L_7 = V_0;
		Vector3_t15  L_8 = Quaternion_op_Multiply_m378(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t15  L_9 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void OrbitCamera::UpdatePosition()
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_UpdatePosition_m119 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t15 * L_0 = &(__this->___position_23);
		float L_1 = (L_0->___x_1);
		Vector3_t15 * L_2 = &(__this->___desiredPosition_17);
		float L_3 = (L_2->___x_1);
		float* L_4 = &(__this->___velX_20);
		float L_5 = (__this->___X_Smooth_18);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_6 = Mathf_SmoothDamp_m377(NULL /*static, unused*/, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t15 * L_7 = &(__this->___position_23);
		float L_8 = (L_7->___y_2);
		Vector3_t15 * L_9 = &(__this->___desiredPosition_17);
		float L_10 = (L_9->___y_2);
		float* L_11 = &(__this->___velY_21);
		float L_12 = (__this->___Y_Smooth_19);
		float L_13 = Mathf_SmoothDamp_m377(NULL /*static, unused*/, L_8, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Vector3_t15 * L_14 = &(__this->___position_23);
		float L_15 = (L_14->___z_3);
		Vector3_t15 * L_16 = &(__this->___desiredPosition_17);
		float L_17 = (L_16->___z_3);
		float* L_18 = &(__this->___velZ_22);
		float L_19 = (__this->___X_Smooth_18);
		float L_20 = Mathf_SmoothDamp_m377(NULL /*static, unused*/, L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = V_0;
		float L_22 = V_1;
		float L_23 = V_2;
		Vector3_t15  L_24 = {0};
		Vector3__ctor_m249(&L_24, L_21, L_22, L_23, /*hidden argument*/NULL);
		__this->___position_23 = L_24;
		Transform_t11 * L_25 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		Vector3_t15  L_26 = (__this->___position_23);
		NullCheck(L_25);
		Transform_set_position_m276(L_25, L_26, /*hidden argument*/NULL);
		Transform_t11 * L_27 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		Transform_t11 * L_28 = (__this->___TargetLookAt_2);
		NullCheck(L_27);
		Transform_LookAt_m379(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Reset()
extern "C" void OrbitCamera_Reset_m120 (OrbitCamera_t29 * __this, const MethodInfo* method)
{
	{
		__this->___mouseX_8 = (0.0f);
		__this->___mouseY_9 = (0.0f);
		float L_0 = (__this->___startingDistance_6);
		__this->___Distance_3 = L_0;
		float L_1 = (__this->___Distance_3);
		__this->___desiredDistance_7 = L_1;
		return;
	}
}
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" float OrbitCamera_ClampAngle_m121 (OrbitCamera_t29 * __this, float ___angle, float ___min, float ___max, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_002d;
	}

IL_0005:
	{
		float L_0 = ___angle;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		float L_1 = ___angle;
		___angle = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0019:
	{
		float L_2 = ___angle;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ___angle;
		___angle = ((float)((float)L_3-(float)(360.0f)));
	}

IL_002d:
	{
		float L_4 = ___angle;
		if ((((float)L_4) < ((float)(-360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_5 = ___angle;
		if ((((float)L_5) > ((float)(360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_6 = ___angle;
		float L_7 = ___min;
		float L_8 = ___max;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp_m274(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
#ifndef _MSC_VER
#else
#endif
// TestParticles
#include "AssemblyU2DCSharp_TestParticlesMethodDeclarations.h"

// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
struct Object_t123;
struct GameObject_t2;
struct Object_t123;
struct Object_t;
// Declaration !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m381_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m381(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m381_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t2_m380(__this /* static, unused */, p0, method) (( GameObject_t2 * (*) (Object_t * /* static, unused */, GameObject_t2 *, const MethodInfo*))Object_Instantiate_TisObject_t_m381_gshared)(__this /* static, unused */, p0, method)


// System.Void TestParticles::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TestParticles__ctor_m122 (TestParticles_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CurrentElementIndex_10 = (-1);
		__this->___m_CurrentParticleIndex_11 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_ElementName_12 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_ParticleName_13 = L_1;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::Start()
extern "C" void TestParticles_Start_m123 (TestParticles_t30 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___m_PrefabListFire_2);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_1 = (__this->___m_PrefabListWind_3);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_2 = (__this->___m_PrefabListWater_4);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_3 = (__this->___m_PrefabListEarth_5);
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_4 = (__this->___m_PrefabListIce_6);
		NullCheck(L_4);
		if ((((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_5 = (__this->___m_PrefabListThunder_7);
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_6 = (__this->___m_PrefabListLight_8);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_7 = (__this->___m_PrefabListDarkness_9);
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}

IL_0070:
	{
		__this->___m_CurrentElementIndex_10 = 0;
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m126(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void TestParticles::Update()
extern TypeInfo* Input_t102_il2cpp_TypeInfo_var;
extern "C" void TestParticles_Update_m124 (TestParticles_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_1 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyUp_m382(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)((int32_t)L_3+(int32_t)1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m126(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyUp_m382(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)((int32_t)L_5-(int32_t)1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m126(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKeyUp_m382(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_7 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)L_7-(int32_t)1));
		TestParticles_ShowParticle_m126(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t102_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetKeyUp_m382(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_9 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)L_9+(int32_t)1));
		TestParticles_ShowParticle_m126(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void TestParticles::OnGUI()
extern TypeInfo* WindowFunction_t125_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern const MethodInfo* TestParticles_InfoWindow_m128_MethodInfo_var;
extern const MethodInfo* TestParticles_ParticleInformationWindow_m127_MethodInfo_var;
extern "C" void TestParticles_OnGUI_m125 (TestParticles_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		TestParticles_InfoWindow_m128_MethodInfo_var = il2cpp_codegen_method_info_from_index(19);
		TestParticles_ParticleInformationWindow_m127_MethodInfo_var = il2cpp_codegen_method_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_1 = {0};
		Rect__ctor_m383(&L_1, (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)260))))), (5.0f), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)TestParticles_InfoWindow_m128_MethodInfo_var };
		WindowFunction_t125 * L_3 = (WindowFunction_t125 *)il2cpp_codegen_object_new (WindowFunction_t125_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m384(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Window_m385(NULL /*static, unused*/, 1, L_1, L_3, (String_t*) &_stringLiteral21, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_6 = {0};
		Rect__ctor_m383(&L_6, (((float)((int32_t)((int32_t)L_4-(int32_t)((int32_t)260))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)85))))), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_7 = { (void*)TestParticles_ParticleInformationWindow_m127_MethodInfo_var };
		WindowFunction_t125 * L_8 = (WindowFunction_t125 *)il2cpp_codegen_object_new (WindowFunction_t125_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m384(L_8, __this, L_7, /*hidden argument*/NULL);
		GUI_Window_m385(NULL /*static, unused*/, 2, L_6, L_8, (String_t*) &_stringLiteral22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::ShowParticle()
extern const MethodInfo* Object_Instantiate_TisGameObject_t2_m380_MethodInfo_var;
extern "C" void TestParticles_ShowParticle_m126 (TestParticles_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGameObject_t2_m380_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_0018;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		int32_t L_1 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 7;
	}

IL_002b:
	{
		int32_t L_2 = (__this->___m_CurrentElementIndex_10);
		if (L_2)
		{
			goto IL_0052;
		}
	}
	{
		GameObjectU5BU5D_t5* L_3 = (__this->___m_PrefabListFire_2);
		__this->___m_CurrentElementList_14 = L_3;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral23;
		goto IL_0165;
	}

IL_0052:
	{
		int32_t L_4 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		GameObjectU5BU5D_t5* L_5 = (__this->___m_PrefabListWater_4);
		__this->___m_CurrentElementList_14 = L_5;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral24;
		goto IL_0165;
	}

IL_007a:
	{
		int32_t L_6 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00a2;
		}
	}
	{
		GameObjectU5BU5D_t5* L_7 = (__this->___m_PrefabListWind_3);
		__this->___m_CurrentElementList_14 = L_7;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral25;
		goto IL_0165;
	}

IL_00a2:
	{
		int32_t L_8 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_00ca;
		}
	}
	{
		GameObjectU5BU5D_t5* L_9 = (__this->___m_PrefabListEarth_5);
		__this->___m_CurrentElementList_14 = L_9;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral26;
		goto IL_0165;
	}

IL_00ca:
	{
		int32_t L_10 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_00f2;
		}
	}
	{
		GameObjectU5BU5D_t5* L_11 = (__this->___m_PrefabListThunder_7);
		__this->___m_CurrentElementList_14 = L_11;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral27;
		goto IL_0165;
	}

IL_00f2:
	{
		int32_t L_12 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_12) == ((uint32_t)5))))
		{
			goto IL_011a;
		}
	}
	{
		GameObjectU5BU5D_t5* L_13 = (__this->___m_PrefabListIce_6);
		__this->___m_CurrentElementList_14 = L_13;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral28;
		goto IL_0165;
	}

IL_011a:
	{
		int32_t L_14 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_14) == ((uint32_t)6))))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t5* L_15 = (__this->___m_PrefabListLight_8);
		__this->___m_CurrentElementList_14 = L_15;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral29;
		goto IL_0165;
	}

IL_0142:
	{
		int32_t L_16 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_16) == ((uint32_t)7))))
		{
			goto IL_0165;
		}
	}
	{
		GameObjectU5BU5D_t5* L_17 = (__this->___m_PrefabListDarkness_9);
		__this->___m_CurrentElementList_14 = L_17;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral30;
	}

IL_0165:
	{
		int32_t L_18 = (__this->___m_CurrentParticleIndex_11);
		GameObjectU5BU5D_t5* L_19 = (__this->___m_CurrentElementList_14);
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_0184;
		}
	}
	{
		__this->___m_CurrentParticleIndex_11 = 0;
		goto IL_01a0;
	}

IL_0184:
	{
		int32_t L_20 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_01a0;
		}
	}
	{
		GameObjectU5BU5D_t5* L_21 = (__this->___m_CurrentElementList_14);
		NullCheck(L_21);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_21)->max_length)))-(int32_t)1));
	}

IL_01a0:
	{
		GameObjectU5BU5D_t5* L_22 = (__this->___m_CurrentElementList_14);
		int32_t L_23 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)));
		String_t* L_25 = Object_get_name_m259((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)), /*hidden argument*/NULL);
		__this->___m_ParticleName_13 = L_25;
		GameObject_t2 * L_26 = (__this->___m_CurrentParticle_15);
		bool L_27 = Object_op_Inequality_m386(NULL /*static, unused*/, L_26, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01d4;
		}
	}
	{
		GameObject_t2 * L_28 = (__this->___m_CurrentParticle_15);
		Object_DestroyObject_m387(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_01d4:
	{
		GameObjectU5BU5D_t5* L_29 = (__this->___m_CurrentElementList_14);
		int32_t L_30 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		GameObject_t2 * L_32 = Object_Instantiate_TisGameObject_t2_m380(NULL /*static, unused*/, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_29, L_31)), /*hidden argument*/Object_Instantiate_TisGameObject_t2_m380_MethodInfo_var);
		__this->___m_CurrentParticle_15 = L_32;
		return;
	}
}
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern TypeInfo* ObjectU5BU5D_t115_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern "C" void TestParticles_ParticleInformationWindow_m127 (TestParticles_t30 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t124  L_0 = {0};
		Rect__ctor_m383(&L_0, (12.0f), (25.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t115* L_1 = ((ObjectU5BU5D_t115*)SZArrayNew(ObjectU5BU5D_t115_il2cpp_TypeInfo_var, 7));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, (String_t*) &_stringLiteral31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)(String_t*) &_stringLiteral31;
		ObjectU5BU5D_t115* L_2 = L_1;
		String_t* L_3 = (__this->___m_ElementName_12);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t115* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)(String_t*) &_stringLiteral32;
		ObjectU5BU5D_t115* L_5 = L_4;
		int32_t L_6 = (__this->___m_CurrentParticleIndex_11);
		int32_t L_7 = ((int32_t)((int32_t)L_6+(int32_t)1));
		Object_t * L_8 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t115* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, (String_t*) &_stringLiteral33);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)(String_t*) &_stringLiteral33;
		ObjectU5BU5D_t115* L_10 = L_9;
		GameObjectU5BU5D_t5* L_11 = (__this->___m_CurrentElementList_14);
		NullCheck(L_11);
		int32_t L_12 = (((int32_t)(((Array_t *)L_11)->max_length)));
		Object_t * L_13 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5)) = (Object_t *)L_13;
		ObjectU5BU5D_t115* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		ArrayElementTypeCheck (L_14, (String_t*) &_stringLiteral10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 6)) = (Object_t *)(String_t*) &_stringLiteral10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m388(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Label_m389(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/NULL);
		Rect_t124  L_16 = {0};
		Rect__ctor_m383(&L_16, (12.0f), (50.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		String_t* L_17 = (__this->___m_ParticleName_13);
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m390(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral34, L_18, /*hidden argument*/NULL);
		GUI_Label_m389(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::InfoWindow(System.Int32)
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern "C" void TestParticles_InfoWindow_m128 (TestParticles_t30 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t124  L_0 = {0};
		Rect__ctor_m383(&L_0, (15.0f), (25.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Label_m389(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral35, /*hidden argument*/NULL);
		Rect_t124  L_1 = {0};
		Rect__ctor_m383(&L_1, (15.0f), (50.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		GUI_Label_m389(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral36, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"


// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneBehaviour__ctor_m129 (BackgroundPlaneBehaviour_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m391(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"

// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"


// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C" void CloudRecoBehaviour__ctor_m130 (CloudRecoBehaviour_t33 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m392(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"

// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"


// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m131 (CylinderTargetBehaviour_t35 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m393(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviourMethodDeclarations.h"

// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstractMethodDeclarations.h"


// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern "C" void DataSetLoadBehaviour__ctor_m132 (DataSetLoadBehaviour_t37 * __this, const MethodInfo* method)
{
	{
		DataSetLoadAbstractBehaviour__ctor_m394(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133 (DataSetLoadBehaviour_t37 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"

// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_gen.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"


// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler__ctor_m134 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mErrorText_3 = L_0;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m398_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_Awake_m135 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_1_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var = il2cpp_codegen_method_info_from_index(22);
		Action_1__ctor_m398_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var };
		Action_1_t128 * L_6 = (Action_1_t128 *)il2cpp_codegen_object_new (Action_1_t128_il2cpp_TypeInfo_var);
		Action_1__ctor_m398(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m398_MethodInfo_var);
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m399(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern TypeInfo* WindowFunction_t125_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m138_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnGUI_m136 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DefaultInitializationErrorHandler_DrawWindowContent_m138_MethodInfo_var = il2cpp_codegen_method_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mErrorOccurred_4);
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_3 = {0};
		Rect__ctor_m383(&L_3, (0.0f), (0.0f), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)DefaultInitializationErrorHandler_DrawWindowContent_m138_MethodInfo_var };
		WindowFunction_t125 * L_5 = (WindowFunction_t125 *)il2cpp_codegen_object_new (WindowFunction_t125_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m384(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Window_m385(NULL /*static, unused*/, 0, L_3, L_5, (String_t*) &_stringLiteral37, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m398_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m137 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_1_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var = il2cpp_codegen_method_info_from_index(22);
		Action_1__ctor_m398_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo_var };
		Action_1_t128 * L_6 = (Action_1_t128 *)il2cpp_codegen_object_new (Action_1_t128_il2cpp_TypeInfo_var);
		Action_1__ctor_m398(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m398_MethodInfo_var);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m400(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m138 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_2 = {0};
		Rect__ctor_m383(&L_2, (10.0f), (25.0f), (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20))))), (((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95))))), /*hidden argument*/NULL);
		String_t* L_3 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Label_m389(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_6 = {0};
		Rect__ctor_m383(&L_6, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m401(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral38, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m402(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m139 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral39, L_0, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral40;
		goto IL_00ed;
	}

IL_005d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral41;
		goto IL_00ed;
	}

IL_006d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral42;
		goto IL_00ed;
	}

IL_007d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral43;
		goto IL_00ed;
	}

IL_008d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral44;
		goto IL_00ed;
	}

IL_009d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral45;
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral46;
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral47;
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral48;
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral49;
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m140 (DefaultInitializationErrorHandler_t39 * __this, bool ___errorOccurred, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred;
		__this->___mErrorOccurred_4 = L_0;
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnQCARInitializationError_m141 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___initError, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError;
		DefaultInitializationErrorHandler_SetErrorCode_m139(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m140(__this, 1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"

// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1.h"
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
struct Component_t103;
struct ReconstructionBehaviour_t40;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t40_m404(__this, method) (( ReconstructionBehaviour_t40 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m142 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern TypeInfo* Action_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t130_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t40_m404_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m405_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m407_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_Start_m143 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Action_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Component_GetComponent_TisReconstructionBehaviour_t40_m404_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var = il2cpp_codegen_method_info_from_index(26);
		Action_1__ctor_m405_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var = il2cpp_codegen_method_info_from_index(28);
		Action_1__ctor_m407_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t40 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t40_m404(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t40_m404_MethodInfo_var);
		__this->___mReconstructionBehaviour_2 = L_0;
		ReconstructionBehaviour_t40 * L_1 = (__this->___mReconstructionBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t40 * L_3 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_4 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var };
		Action_1_t129 * L_5 = (Action_1_t129 *)il2cpp_codegen_object_new (Action_1_t129_il2cpp_TypeInfo_var);
		Action_1__ctor_m405(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m405_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m406(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t40 * L_6 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_7 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var };
		Action_1_t130 * L_8 = (Action_1_t130 *)il2cpp_codegen_object_new (Action_1_t130_il2cpp_TypeInfo_var);
		Action_1__ctor_m407(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m407_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m408(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern TypeInfo* Action_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t130_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m405_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m407_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m144 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Action_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var = il2cpp_codegen_method_info_from_index(26);
		Action_1__ctor_m405_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var = il2cpp_codegen_method_info_from_index(28);
		Action_1__ctor_m407_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t40 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t40 * L_2 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_3 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo_var };
		Action_1_t129 * L_4 = (Action_1_t129 *)il2cpp_codegen_object_new (Action_1_t129_il2cpp_TypeInfo_var);
		Action_1__ctor_m405(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m405_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m409(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t40 * L_5 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_6 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo_var };
		Action_1_t130 * L_7 = (Action_1_t130 *)il2cpp_codegen_object_new (Action_1_t130_il2cpp_TypeInfo_var);
		Action_1__ctor_m407(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m407_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m410(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m145 (DefaultSmartTerrainEventHandler_t43 * __this, Object_t * ___prop, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t40 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t40 * L_2 = (__this->___mReconstructionBehaviour_2);
		PropBehaviour_t41 * L_3 = (__this->___PropTemplate_3);
		Object_t * L_4 = ___prop;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m411(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146 (DefaultSmartTerrainEventHandler_t43 * __this, Object_t * ___surface, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t40 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t40 * L_2 = (__this->___mReconstructionBehaviour_2);
		SurfaceBehaviour_t42 * L_3 = (__this->___SurfaceTemplate_4);
		Object_t * L_4 = ___surface;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m412(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"

// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
struct Component_t103;
struct TrackableBehaviour_t44;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, method) (( TrackableBehaviour_t44 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C" void DefaultTrackableEventHandler__ctor_m147 (DefaultTrackableEventHandler_t45 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var;
extern "C" void DefaultTrackableEventHandler_Start_m148 (DefaultTrackableEventHandler_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t44 * L_0 = Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t44 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t44 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m414(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void DefaultTrackableEventHandler_OnTrackableStateChanged_m149 (DefaultTrackableEventHandler_t45 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m150(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m151(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultTrackableEventHandler_OnTrackingFound_m150 (DefaultTrackableEventHandler_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	{
		TrackableBehaviour_t44 * L_0 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_0);
		V_0 = L_1;
		TrackableBehaviour_t44 * L_2 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_2);
		int32_t L_3 = Object_GetInstanceID_m415(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m260(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral50, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		ARVRModes_t6 * L_6 = (__this->___arvrmodes_3);
		NullCheck(L_6);
		ARVRModes_SeeEntire_m4(L_6, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0038:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m260(NULL /*static, unused*/, L_7, (String_t*) &_stringLiteral51, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		ARVRModes_t6 * L_9 = (__this->___arvrmodes_3);
		NullCheck(L_9);
		ARVRModes_SeeDoor_m5(L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		TrackableBehaviour_t44 * L_10 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral52, L_11, (String_t*) &_stringLiteral53, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		ARVRModes_t6 * L_13 = (__this->___arvrmodes_3);
		NullCheck(L_13);
		ARVRModes_SwitchVRToAR_m7(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultTrackableEventHandler_OnTrackingLost_m151 (DefaultTrackableEventHandler_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t44 * L_0 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral52, L_1, (String_t*) &_stringLiteral54, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		ARVRModes_t6 * L_3 = (__this->___arvrmodes_3);
		NullCheck(L_3);
		ARVRModes_SwitchARToVR_m6(L_3, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"



// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C" void GLErrorHandler__ctor_m152 (GLErrorHandler_t46 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GLErrorHandler_t46_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler__cctor_m153 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		GLErrorHandler_t46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((GLErrorHandler_t46_StaticFields*)GLErrorHandler_t46_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern TypeInfo* GLErrorHandler_t46_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_SetError_m154 (Object_t * __this /* static, unused */, String_t* ___errorText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorText;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t46_il2cpp_TypeInfo_var);
		((GLErrorHandler_t46_StaticFields*)GLErrorHandler_t46_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		((GLErrorHandler_t46_StaticFields*)GLErrorHandler_t46_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4 = 1;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern TypeInfo* GLErrorHandler_t46_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t125_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m156_MethodInfo_var;
extern "C" void GLErrorHandler_OnGUI_m155 (GLErrorHandler_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		WindowFunction_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		GLErrorHandler_DrawWindowContent_m156_MethodInfo_var = il2cpp_codegen_method_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t46_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t46_StaticFields*)GLErrorHandler_t46_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_3 = {0};
		Rect__ctor_m383(&L_3, (0.0f), (0.0f), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)GLErrorHandler_DrawWindowContent_m156_MethodInfo_var };
		WindowFunction_t125 * L_5 = (WindowFunction_t125 *)il2cpp_codegen_object_new (WindowFunction_t125_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m384(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Window_m385(NULL /*static, unused*/, 0, L_3, L_5, (String_t*) &_stringLiteral55, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GLErrorHandler_t46_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t126_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_DrawWindowContent_m156 (GLErrorHandler_t46 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_2 = {0};
		Rect__ctor_m383(&L_2, (10.0f), (25.0f), (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20))))), (((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t46_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t46_StaticFields*)GLErrorHandler_t46_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t126_il2cpp_TypeInfo_var);
		GUI_Label_m389(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m317(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t124  L_6 = {0};
		Rect__ctor_m383(&L_6, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m401(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral38, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m402(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"

// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"


// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m157 (HideExcessAreaBehaviour_t47 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m418(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"

// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"


// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m158 (ImageTargetBehaviour_t49 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m419(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"

// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// Vuforia.QCARUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityMethodDeclarations.h"


// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C" void AndroidUnityPlayer__ctor_m159 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C" void AndroidUnityPlayer_LoadNativeLibraries_m160 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m168(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C" void AndroidUnityPlayer_InitializePlatform_m161 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m169(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern "C" int32_t AndroidUnityPlayer_Start_m162 (AndroidUnityPlayer_t51 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey;
		int32_t L_1 = AndroidUnityPlayer_InitQCAR_m170(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m171(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_Update_m163 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m420(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m171(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m172(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m173(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = (__this->___mFramesSinceLastOrientationReset_4);
		__this->___mFramesSinceLastOrientationReset_4 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C" void AndroidUnityPlayer_OnPause_m164 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnPause_m422(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C" void AndroidUnityPlayer_OnResume_m165 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnResume_m423(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C" void AndroidUnityPlayer_OnDestroy_m166 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m424(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C" void AndroidUnityPlayer_Dispose_m167 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C" void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m168 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C" void AndroidUnityPlayer_InitAndroidPlatform_m169 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
extern "C" int32_t AndroidUnityPlayer_InitQCAR_m170 (AndroidUnityPlayer_t51 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_InitializeSurface_m171 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m425(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m172(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m173(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C" void AndroidUnityPlayer_ResetUnityScreenOrientation_m172 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_2 = L_0;
		__this->___mFramesSinceLastOrientationReset_4 = 0;
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_CheckOrientation_m173 (AndroidUnityPlayer_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mFramesSinceLastOrientationReset_4);
		V_0 = ((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		V_0 = ((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = (__this->___mScreenOrientation_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = (__this->___mJavaScreenOrientation_3);
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->___mJavaScreenOrientation_3 = L_8;
		int32_t L_9 = (__this->___mJavaScreenOrientation_3);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m426(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->___mFramesSinceLastJavaOrientationCheck_5 = 0;
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		__this->___mFramesSinceLastJavaOrientationCheck_5 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0063:
	{
		return;
	}
}
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"
struct Enumerable_t132;
struct List_1_t133;
struct IEnumerable_1_t134;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t132;
struct List_1_t135;
struct IEnumerable_1_t136;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t135 * Enumerable_ToList_TisObject_t_m428_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m428(__this /* static, unused */, p0, method) (( List_1_t135 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m428_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m427(__this /* static, unused */, p0, method) (( List_1_t133 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m428_gshared)(__this /* static, unused */, p0, method)


// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m174 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t139_0_0_0_var;
extern TypeInfo* Attribute_t138_il2cpp_TypeInfo_var;
extern TypeInfo* FactorySetter_t142_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t137_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m427_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m430_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m431_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m432_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m434_MethodInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_Awake_m175 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t139_0_0_0_var = il2cpp_codegen_type_from_index(51);
		Attribute_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		FactorySetter_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		Enumerator_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Enumerable_ToList_TisMethodInfo_t_m427_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		List_1_AddRange_m430_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483681);
		List_1_GetEnumerator_m431_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		Enumerator_get_Current_m432_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		Enumerator_MoveNext_m434_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t133 * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	Enumerator_t137  V_2 = {0};
	Attribute_t138 * V_3 = {0};
	ObjectU5BU5D_t115* V_4 = {0};
	int32_t V_5 = 0;
	Action_t139 * V_6 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m429(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t141* L_1 = (MethodInfoU5BU5D_t141*)VirtFuncInvoker1< MethodInfoU5BU5D_t141*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t133 * L_2 = Enumerable_ToList_TisMethodInfo_t_m427(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m427_MethodInfo_var);
		V_0 = L_2;
		List_1_t133 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m429(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t141* L_5 = (MethodInfoU5BU5D_t141*)VirtFuncInvoker1< MethodInfoU5BU5D_t141*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m430(L_3, (Object_t*)(Object_t*)L_5, /*hidden argument*/List_1_AddRange_m430_MethodInfo_var);
		List_1_t133 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t137  L_7 = List_1_GetEnumerator_m431(L_6, /*hidden argument*/List_1_GetEnumerator_m431_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m432((&V_2), /*hidden argument*/Enumerator_get_Current_m432_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t115* L_10 = (ObjectU5BU5D_t115*)VirtFuncInvoker1< ObjectU5BU5D_t115*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, 1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t115* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			int32_t L_13 = L_12;
			V_3 = ((Attribute_t138 *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(L_11, L_13)), Attribute_t138_il2cpp_TypeInfo_var));
			Attribute_t138 * L_14 = V_3;
			if (!((FactorySetter_t142 *)IsInst(L_14, FactorySetter_t142_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_15 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Action_t139_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_16 = V_1;
			Delegate_t143 * L_17 = Delegate_CreateDelegate_m433(NULL /*static, unused*/, L_15, __this, L_16, /*hidden argument*/NULL);
			V_6 = ((Action_t139 *)IsInst(L_17, Action_t139_il2cpp_TypeInfo_var));
			Action_t139 * L_18 = V_6;
			if (!L_18)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t139 * L_19 = V_6;
			NullCheck(L_19);
			VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_19);
		}

IL_0087:
		{
			int32_t L_20 = V_5;
			V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_21 = V_5;
			ObjectU5BU5D_t115* L_22 = V_4;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_23 = Enumerator_MoveNext_m434((&V_2), /*hidden argument*/Enumerator_MoveNext_m434_MethodInfo_var);
			if (L_23)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t137  L_24 = V_2;
		Enumerator_t137  L_25 = L_24;
		Object_t * L_26 = Box(Enumerator_t137_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_26);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern TypeInfo* VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral56, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t54 * L_0 = (VuforiaBehaviourComponentFactory_t54 *)il2cpp_codegen_object_new (VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m191(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m435(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"



// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C" void IOSUnityPlayer__ctor_m177 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C" void IOSUnityPlayer_LoadNativeLibraries_m178 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C" void IOSUnityPlayer_InitializePlatform_m179 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m188(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern "C" int32_t IOSUnityPlayer_Start_m180 (IOSUnityPlayer_t53 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		int32_t L_2 = IOSUnityPlayer_initQCARiOS_m189(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m186(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return (int32_t)(L_4);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_Update_m181 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m420(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m186(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m187(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C" void IOSUnityPlayer_Dispose_m182 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C" void IOSUnityPlayer_OnPause_m183 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnPause_m422(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C" void IOSUnityPlayer_OnResume_m184 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnResume_m423(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C" void IOSUnityPlayer_OnDestroy_m185 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m424(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_InitializeSurface_m186 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m425(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m187(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_SetUnityScreenOrientation_m187 (IOSUnityPlayer_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_0 = L_0;
		int32_t L_1 = (__this->___mScreenOrientation_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m426(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		IOSUnityPlayer_setSurfaceOrientationiOS_m190(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C" {void DEFAULT_CALL setPlatFormNative();}
extern "C" void IOSUnityPlayer_setPlatFormNative_m188 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPlatFormNative;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);}
extern "C" int32_t IOSUnityPlayer_initQCARiOS_m189 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initQCARiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initQCARiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Marshaling of parameter '___licenseKey' to native representation
	char* ____licenseKey_marshaled = { 0 };
	____licenseKey_marshaled = il2cpp_codegen_marshal_string(___licenseKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___screenOrientation, ____licenseKey_marshaled);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

	// Marshaling cleanup of parameter '___licenseKey' native representation
	il2cpp_codegen_marshal_free(____licenseKey_marshaled);
	____licenseKey_marshaled = NULL;

	return _return_value;
}
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C" {void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);}
extern "C" void IOSUnityPlayer_setSurfaceOrientationiOS_m190 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSurfaceOrientationiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___screenOrientation);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

}
#ifndef _MSC_VER
#else
#endif

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
struct GameObject_t2;
struct MaskOutBehaviour_t59;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m437_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m437(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t59_m436(__this, method) (( MaskOutBehaviour_t59 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct VirtualButtonBehaviour_t85;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t85_m438(__this, method) (( VirtualButtonBehaviour_t85 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct TurnOffBehaviour_t76;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t76_m439(__this, method) (( TurnOffBehaviour_t76 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct ImageTargetBehaviour_t49;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t49_m440(__this, method) (( ImageTargetBehaviour_t49 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct MarkerBehaviour_t57;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
#define GameObject_AddComponent_TisMarkerBehaviour_t57_m441(__this, method) (( MarkerBehaviour_t57 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct MultiTargetBehaviour_t61;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t61_m442(__this, method) (( MultiTargetBehaviour_t61 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct CylinderTargetBehaviour_t35;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t35_m443(__this, method) (( CylinderTargetBehaviour_t35 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct WordBehaviour_t92;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t92_m444(__this, method) (( WordBehaviour_t92 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct TextRecoBehaviour_t74;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t74_m445(__this, method) (( TextRecoBehaviour_t74 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
struct GameObject_t2;
struct ObjectTargetBehaviour_t63;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t63_m446(__this, method) (( ObjectTargetBehaviour_t63 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)


// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m191 (VuforiaBehaviourComponentFactory_t54 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t59_m436_MethodInfo_var;
extern "C" MaskOutAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMaskOutBehaviour_t59_m436_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483685);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MaskOutBehaviour_t59 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t59_m436(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t59_m436_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t85_m438_MethodInfo_var;
extern "C" VirtualButtonAbstractBehaviour_t86 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisVirtualButtonBehaviour_t85_m438_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483686);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		VirtualButtonBehaviour_t85 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t85_m438(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t85_m438_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t76_m439_MethodInfo_var;
extern "C" TurnOffAbstractBehaviour_t77 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTurnOffBehaviour_t76_m439_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		TurnOffBehaviour_t76 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t76_m439(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t76_m439_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t49_m440_MethodInfo_var;
extern "C" ImageTargetAbstractBehaviour_t50 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisImageTargetBehaviour_t49_m440_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		ImageTargetBehaviour_t49 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t49_m440(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t49_m440_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMarkerBehaviour_t57_m441_MethodInfo_var;
extern "C" MarkerAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMarkerBehaviour_t57_m441_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MarkerBehaviour_t57 * L_1 = GameObject_AddComponent_TisMarkerBehaviour_t57_m441(L_0, /*hidden argument*/GameObject_AddComponent_TisMarkerBehaviour_t57_m441_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t61_m442_MethodInfo_var;
extern "C" MultiTargetAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMultiTargetBehaviour_t61_m442_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MultiTargetBehaviour_t61 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t61_m442(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t61_m442_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t35_m443_MethodInfo_var;
extern "C" CylinderTargetAbstractBehaviour_t36 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisCylinderTargetBehaviour_t35_m443_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		CylinderTargetBehaviour_t35 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t35_m443(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t35_m443_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t92_m444_MethodInfo_var;
extern "C" WordAbstractBehaviour_t93 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m199 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisWordBehaviour_t92_m444_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		WordBehaviour_t92 * L_1 = GameObject_AddComponent_TisWordBehaviour_t92_m444(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t92_m444_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t74_m445_MethodInfo_var;
extern "C" TextRecoAbstractBehaviour_t75 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTextRecoBehaviour_t74_m445_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		TextRecoBehaviour_t74 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t74_m445(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t74_m445_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t63_m446_MethodInfo_var;
extern "C" ObjectTargetAbstractBehaviour_t64 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisObjectTargetBehaviour_t63_m446_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		ObjectTargetBehaviour_t63 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t63_m446(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t63_m446_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"

// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"


// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C" void KeepAliveBehaviour__ctor_m202 (KeepAliveBehaviour_t55 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour__ctor_m447(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"

// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehavMethodDeclarations.h"


// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m203 (MarkerBehaviour_t57 * __this, const MethodInfo* method)
{
	{
		MarkerAbstractBehaviour__ctor_m448(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBehaMethodDeclarations.h"
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"


// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m204 (MaskOutBehaviour_t59 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m449(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* MaterialU5BU5D_t113_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var;
extern "C" void MaskOutBehaviour_Start_m205 (MaskOutBehaviour_t59 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		MaterialU5BU5D_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(68);
		Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t8 * V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t113* V_2 = {0};
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t8 * L_1 = Component_GetComponent_TisRenderer_t8_m261(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var);
		V_0 = L_1;
		Renderer_t8 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t113* L_3 = Renderer_get_materials_m340(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)(((Array_t *)L_3)->max_length)));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t8 * L_5 = V_0;
		Material_t4 * L_6 = (((MaskOutAbstractBehaviour_t60 *)__this)->___maskMaterial_2);
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m451(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t113*)SZArrayNew(MaterialU5BU5D_t113_il2cpp_TypeInfo_var, L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t113* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t4 * L_10 = (((MaskOutAbstractBehaviour_t60 *)__this)->___maskMaterial_2);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		*((Material_t4 **)(Material_t4 **)SZArrayLdElema(L_8, L_9)) = (Material_t4 *)L_10;
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t8 * L_14 = V_0;
		MaterialU5BU5D_t113* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m452(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"

// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstractMethodDeclarations.h"


// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m206 (MultiTargetBehaviour_t61 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m453(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"

// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"


// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m207 (ObjectTargetBehaviour_t63 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m454(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"

// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavioMethodDeclarations.h"


// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m208 (PropBehaviour_t41 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m455(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviourMethodDeclarations.h"

// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"
struct GameObject_t2;
struct ComponentFactoryStarterBehaviour_t52;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m456(__this, method) (( ComponentFactoryStarterBehaviour_t52 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)


// System.Void Vuforia.QCARBehaviour::.ctor()
extern "C" void QCARBehaviour__ctor_m209 (QCARBehaviour_t66 * __this, const MethodInfo* method)
{
	{
		QCARAbstractBehaviour__ctor_m457(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARBehaviour::Awake()
extern TypeInfo* NullUnityPlayer_t146_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidUnityPlayer_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IOSUnityPlayer_t53_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* PlayModeUnityPlayer_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m456_MethodInfo_var;
extern "C" void QCARBehaviour_Awake_m210 (QCARBehaviour_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullUnityPlayer_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		AndroidUnityPlayer_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(72);
		IOSUnityPlayer_t53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		PlayModeUnityPlayer_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(74);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m456_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		NullUnityPlayer_t146 * L_0 = (NullUnityPlayer_t146 *)il2cpp_codegen_object_new (NullUnityPlayer_t146_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m458(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t51 * L_2 = (AndroidUnityPlayer_t51 *)il2cpp_codegen_object_new (AndroidUnityPlayer_t51_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m159(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t53 * L_4 = (IOSUnityPlayer_t53 *)il2cpp_codegen_object_new (IOSUnityPlayer_t53_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m177(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_5 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t147 * L_6 = (PlayModeUnityPlayer_t147 *)il2cpp_codegen_object_new (PlayModeUnityPlayer_t147_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m461(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0043:
	{
		Object_t * L_7 = V_0;
		QCARAbstractBehaviour_SetUnityPlayerImplementation_m462(__this, L_7, /*hidden argument*/NULL);
		GameObject_t2 * L_8 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m456(L_8, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m456_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"



// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m211 (ReconstructionBehaviour_t40 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m463(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"


// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C" void ReconstructionFromTargetBehaviour__ctor_m212 (ReconstructionFromTargetBehaviour_t69 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m464(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"


// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C" void SmartTerrainTrackerBehaviour__ctor_m213 (SmartTerrainTrackerBehaviour_t71 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m465(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"

// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"


// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m214 (SurfaceBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m466(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"

// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"


// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m215 (TextRecoBehaviour_t74 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m467(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
struct Component_t103;
struct MeshRenderer_t148;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t148_m468(__this, method) (( MeshRenderer_t148 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
struct Component_t103;
struct MeshFilter_t149;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t149_m469(__this, method) (( MeshFilter_t149 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m216 (TurnOffBehaviour_t76 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m470(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var;
extern "C" void TurnOffBehaviour_Awake_m217 (TurnOffBehaviour_t76 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t148 * V_0 = {0};
	MeshFilter_t149 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t148 * L_1 = Component_GetComponent_TisMeshRenderer_t148_m468(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t148 * L_2 = V_0;
		Object_Destroy_m471(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t149 * L_3 = Component_GetComponent_TisMeshFilter_t149_m469(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t149 * L_4 = V_1;
		Object_Destroy_m471(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"



// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m218 (TurnOffWordBehaviour_t78 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var;
extern "C" void TurnOffWordBehaviour_Awake_m219 (TurnOffWordBehaviour_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t148 * V_0 = {0};
	Transform_t11 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t148 * L_1 = Component_GetComponent_TisMeshRenderer_t148_m468(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t148_m468_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t148 * L_2 = V_0;
		Object_Destroy_m471(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t11 * L_3 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = Transform_FindChild_m472(L_3, (String_t*) &_stringLiteral57, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t11 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m386(NULL /*static, unused*/, L_5, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t11 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t2 * L_8 = Component_get_gameObject_m272(L_7, /*hidden argument*/NULL);
		Object_Destroy_m471(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"


// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m220 (UserDefinedTargetBuildingBehaviour_t79 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m473(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"

// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"


// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C" void VideoBackgroundBehaviour__ctor_m221 (VideoBackgroundBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		VideoBackgroundAbstractBehaviour__ctor_m474(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"


// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C" void VideoTextureRenderer__ctor_m222 (VideoTextureRenderer_t83 * __this, const MethodInfo* method)
{
	{
		VideoTextureRendererAbstractBehaviour__ctor_m475(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"

// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"


// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m223 (VirtualButtonBehaviour_t85 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m476(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"

// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"


// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m224 (WebCamBehaviour_t87 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m477(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
struct GameObject_t2;
struct CameraU5BU5D_t150;
struct GameObject_t2;
struct ObjectU5BU5D_t115;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t115* GameObject_GetComponentsInChildren_TisObject_t_m479_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m479(__this, method) (( ObjectU5BU5D_t115* (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m479_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t3_m478(__this, method) (( CameraU5BU5D_t150* (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m479_gshared)(__this, method)


// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m225 (WireframeBehaviour_t89 * __this, const MethodInfo* method)
{
	{
		__this->___ShowLines_3 = 1;
		Color_t90  L_0 = Color_get_green_m480(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___LineColor_4 = L_0;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern TypeInfo* ObjectU5BU5D_t115_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t151_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t4_il2cpp_TypeInfo_var;
extern "C" void WireframeBehaviour_CreateLineMaterial_m226 (WireframeBehaviour_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		Single_t151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Material_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t115* L_0 = ((ObjectU5BU5D_t115*)SZArrayNew(ObjectU5BU5D_t115_il2cpp_TypeInfo_var, ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral58;
		ObjectU5BU5D_t115* L_1 = L_0;
		Color_t90 * L_2 = &(__this->___LineColor_4);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t151_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t115* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral11;
		ObjectU5BU5D_t115* L_7 = L_6;
		Color_t90 * L_8 = &(__this->___LineColor_4);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t151_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t115* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4)) = (Object_t *)(String_t*) &_stringLiteral11;
		ObjectU5BU5D_t115* L_13 = L_12;
		Color_t90 * L_14 = &(__this->___LineColor_4);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t151_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5)) = (Object_t *)L_17;
		ObjectU5BU5D_t115* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, (String_t*) &_stringLiteral11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6)) = (Object_t *)(String_t*) &_stringLiteral11;
		ObjectU5BU5D_t115* L_19 = L_18;
		Color_t90 * L_20 = &(__this->___LineColor_4);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t151_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7)) = (Object_t *)L_23;
		ObjectU5BU5D_t115* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, (String_t*) &_stringLiteral59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8)) = (Object_t *)(String_t*) &_stringLiteral59;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m388(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t4 * L_26 = (Material_t4 *)il2cpp_codegen_object_new (Material_t4_il2cpp_TypeInfo_var);
		Material__ctor_m481(L_26, L_25, /*hidden argument*/NULL);
		__this->___mLineMaterial_2 = L_26;
		Material_t4 * L_27 = (__this->___mLineMaterial_2);
		NullCheck(L_27);
		Object_set_hideFlags_m482(L_27, ((int32_t)61), /*hidden argument*/NULL);
		Material_t4 * L_28 = (__this->___mLineMaterial_2);
		NullCheck(L_28);
		Shader_t152 * L_29 = Material_get_shader_m483(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m482(L_29, ((int32_t)61), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t3_m478_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var;
extern "C" void WireframeBehaviour_OnRenderObject_m227 (WireframeBehaviour_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		GameObject_GetComponentsInChildren_TisCamera_t3_m478_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	CameraU5BU5D_t150* V_1 = {0};
	bool V_2 = false;
	Camera_t3 * V_3 = {0};
	CameraU5BU5D_t150* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t149 * V_6 = {0};
	Mesh_t153 * V_7 = {0};
	Vector3U5BU5D_t154* V_8 = {0};
	Int32U5BU5D_t19* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t15  V_11 = {0};
	Vector3_t15  V_12 = {0};
	Vector3_t15  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_0 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = (Transform_t11 *)VirtFuncInvoker0< Transform_t11 * >::Invoke(8 /* UnityEngine.Transform Vuforia.QCARManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t2 * L_2 = Component_get_gameObject_m272(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t2 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t150* L_4 = GameObject_GetComponentsInChildren_TisCamera_t3_m478(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t3_m478_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		CameraU5BU5D_t150* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t150* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(Camera_t3 **)(Camera_t3 **)SZArrayLdElema(L_6, L_8));
		Camera_t3 * L_9 = Camera_get_current_m485(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3 * L_10 = V_3;
		bool L_11 = Object_op_Equality_m375(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_5;
		CameraU5BU5D_t150* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_16 = (__this->___ShowLines_3);
		if (L_16)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t149 * L_17 = Component_GetComponent_TisMeshFilter_t149_m469(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var);
		V_6 = L_17;
		MeshFilter_t149 * L_18 = V_6;
		bool L_19 = Object_op_Implicit_m397(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t4 * L_20 = (__this->___mLineMaterial_2);
		bool L_21 = Object_op_Equality_m375(NULL /*static, unused*/, L_20, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m226(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t149 * L_22 = V_6;
		NullCheck(L_22);
		Mesh_t153 * L_23 = MeshFilter_get_sharedMesh_m486(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		Mesh_t153 * L_24 = V_7;
		NullCheck(L_24);
		Vector3U5BU5D_t154* L_25 = Mesh_get_vertices_m487(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Mesh_t153 * L_26 = V_7;
		NullCheck(L_26);
		Int32U5BU5D_t19* L_27 = Mesh_get_triangles_m488(L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		GL_PushMatrix_m489(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t11 * L_28 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Matrix4x4_t156  L_29 = Transform_get_localToWorldMatrix_m490(L_28, /*hidden argument*/NULL);
		GL_MultMatrix_m491(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t4 * L_30 = (__this->___mLineMaterial_2);
		NullCheck(L_30);
		Material_SetPass_m492(L_30, 0, /*hidden argument*/NULL);
		GL_Begin_m493(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t154* L_31 = V_8;
		Int32U5BU5D_t19* L_32 = V_9;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34)));
		V_11 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34)))));
		Vector3U5BU5D_t154* L_35 = V_8;
		Int32U5BU5D_t19* L_36 = V_9;
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38)));
		V_12 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38)))));
		Vector3U5BU5D_t154* L_39 = V_8;
		Int32U5BU5D_t19* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)((int32_t)L_41+(int32_t)2)));
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)2));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42)));
		V_13 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42)))));
		Vector3_t15  L_43 = V_11;
		GL_Vertex_m494(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Vector3_t15  L_44 = V_12;
		GL_Vertex_m494(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t15  L_45 = V_12;
		GL_Vertex_m494(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t15  L_46 = V_13;
		GL_Vertex_m494(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector3_t15  L_47 = V_13;
		GL_Vertex_m494(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t15  L_48 = V_11;
		GL_Vertex_m494(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_50 = V_10;
		Int32U5BU5D_t19* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)(((Array_t *)L_51)->max_length))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m495(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m496(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var;
extern "C" void WireframeBehaviour_OnDrawGizmos_m228 (WireframeBehaviour_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t149 * V_0 = {0};
	Mesh_t153 * V_1 = {0};
	Vector3U5BU5D_t154* V_2 = {0};
	Int32U5BU5D_t19* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t15  V_5 = {0};
	Vector3_t15  V_6 = {0};
	Vector3_t15  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_3);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t149 * L_2 = Component_GetComponent_TisMeshFilter_t149_m469(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t149 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m397(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t2 * L_5 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m273(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t15  L_7 = Transform_get_position_m247(L_6, /*hidden argument*/NULL);
		GameObject_t2 * L_8 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = GameObject_get_transform_m273(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t13  L_10 = Transform_get_rotation_m245(L_9, /*hidden argument*/NULL);
		GameObject_t2 * L_11 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t11 * L_12 = GameObject_get_transform_m273(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t15  L_13 = Transform_get_lossyScale_m498(L_12, /*hidden argument*/NULL);
		Matrix4x4_t156  L_14 = Matrix4x4_TRS_m499(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m500(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t90  L_15 = (__this->___LineColor_4);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t149 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t153 * L_17 = MeshFilter_get_sharedMesh_m486(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t153 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t154* L_19 = Mesh_get_vertices_m487(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t153 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t19* L_21 = Mesh_get_triangles_m488(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t154* L_22 = V_2;
		Int32U5BU5D_t19* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25)));
		V_5 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25)))));
		Vector3U5BU5D_t154* L_26 = V_2;
		Int32U5BU5D_t19* L_27 = V_3;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28+(int32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29)));
		V_6 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29)))));
		Vector3U5BU5D_t154* L_30 = V_2;
		Int32U5BU5D_t19* L_31 = V_3;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)2)));
		int32_t L_33 = ((int32_t)((int32_t)L_32+(int32_t)2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33)));
		V_7 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33)))));
		Vector3_t15  L_34 = V_5;
		Vector3_t15  L_35 = V_6;
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t15  L_36 = V_6;
		Vector3_t15  L_37 = V_7;
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t15  L_38 = V_7;
		Vector3_t15  L_39 = V_5;
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_41 = V_4;
		Int32U5BU5D_t19* L_42 = V_3;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
#include "Assembly-CSharp_ArrayTypes.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
struct Component_t103;
struct ColliderU5BU5D_t158;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t157_m503(__this, p0, method) (( ColliderU5BU5D_t158* (*) (Component_t103 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m343_gshared)(__this, p0, method)
struct Component_t103;
struct WireframeBehaviourU5BU5D_t159;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504(__this, p0, method) (( WireframeBehaviourU5BU5D_t159* (*) (Component_t103 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m343_gshared)(__this, p0, method)


// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C" void WireframeTrackableEventHandler__ctor_m229 (WireframeTrackableEventHandler_t91 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_Start_m230 (WireframeTrackableEventHandler_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t44 * L_0 = Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t44 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t44 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m414(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void WireframeTrackableEventHandler_OnTrackableStateChanged_m231 (WireframeTrackableEventHandler_t91 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m232(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m233(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_OnTrackingFound_m232 (WireframeTrackableEventHandler_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t114* V_0 = {0};
	ColliderU5BU5D_t158* V_1 = {0};
	WireframeBehaviourU5BU5D_t159* V_2 = {0};
	Renderer_t8 * V_3 = {0};
	RendererU5BU5D_t114* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t157 * V_6 = {0};
	ColliderU5BU5D_t158* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t89 * V_9 = {0};
	WireframeBehaviourU5BU5D_t159* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t114* L_0 = Component_GetComponentsInChildren_TisRenderer_t8_m342(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t158* L_1 = Component_GetComponentsInChildren_TisCollider_t157_m503(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t159* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t114* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t114* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t8 **)(Renderer_t8 **)SZArrayLdElema(L_4, L_6));
		Renderer_t8 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m505(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t114* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t158* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t158* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t157 **)(Collider_t157 **)SZArrayLdElema(L_12, L_14));
		Collider_t157 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m506(L_15, 1, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t158* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t159* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t159* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t89 **)(WireframeBehaviour_t89 **)SZArrayLdElema(L_20, L_22));
		WireframeBehaviour_t89 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m240(L_23, 1, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t159* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t44 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral52, L_28, (String_t*) &_stringLiteral53, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_OnTrackingLost_m233 (WireframeTrackableEventHandler_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t114* V_0 = {0};
	ColliderU5BU5D_t158* V_1 = {0};
	WireframeBehaviourU5BU5D_t159* V_2 = {0};
	Renderer_t8 * V_3 = {0};
	RendererU5BU5D_t114* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t157 * V_6 = {0};
	ColliderU5BU5D_t158* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t89 * V_9 = {0};
	WireframeBehaviourU5BU5D_t159* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t114* L_0 = Component_GetComponentsInChildren_TisRenderer_t8_m342(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t8_m342_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t158* L_1 = Component_GetComponentsInChildren_TisCollider_t157_m503(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t157_m503_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t159* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t89_m504_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t114* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t114* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t8 **)(Renderer_t8 **)SZArrayLdElema(L_4, L_6));
		Renderer_t8 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m505(L_7, 0, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t114* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t158* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t158* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t157 **)(Collider_t157 **)SZArrayLdElema(L_12, L_14));
		Collider_t157 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m506(L_15, 0, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t158* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t159* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t159* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t89 **)(WireframeBehaviour_t89 **)SZArrayLdElema(L_20, L_22));
		WireframeBehaviour_t89 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m240(L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t159* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t44 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral52, L_28, (String_t*) &_stringLiteral54, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"

// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"


// System.Void Vuforia.WordBehaviour::.ctor()
extern "C" void WordBehaviour__ctor_m234 (WordBehaviour_t92 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m507(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
