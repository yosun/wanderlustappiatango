﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct Dictionary_2_t3839;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4138;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t4365;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t1286;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyCollection_t4368;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ValueCollection_t4369;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t3458;
// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t1281;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t4366;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t4367;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__43.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_34MethodDeclarations.h"
#define Dictionary_2__ctor_m26010(__this, method) (( void (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2__ctor_m16760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26011(__this, ___comparer, method) (( void (*) (Dictionary_2_t3839 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16762_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m26012(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3839 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16764_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m26013(__this, ___capacity, method) (( void (*) (Dictionary_2_t3839 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16766_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26014(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3839 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16768_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m26015(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3839 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m16770_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26016(__this, method) (( Object_t* (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16772_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26017(__this, method) (( Object_t* (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16774_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26018(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16776_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26019(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3839 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16778_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m26020(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3839 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16780_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m26021(__this, ___key, method) (( bool (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16782_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m26022(__this, ___key, method) (( void (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16784_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26023(__this, method) (( bool (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16786_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26024(__this, method) (( Object_t * (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16788_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26025(__this, method) (( bool (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16790_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26026(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3839 *, KeyValuePair_2_t3843 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16792_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26027(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3839 *, KeyValuePair_2_t3843 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16794_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26028(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3839 *, KeyValuePair_2U5BU5D_t4366*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26029(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3839 *, KeyValuePair_2_t3843 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16798_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26030(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3839 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16800_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26031(__this, method) (( Object_t * (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16802_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26032(__this, method) (( Object_t* (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16804_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26033(__this, method) (( Object_t * (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16806_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define Dictionary_2_get_Count_m26034(__this, method) (( int32_t (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_get_Count_m16808_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m26035(__this, ___key, method) (( ConstructorDelegate_t1286 * (*) (Dictionary_2_t3839 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m16810_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m26036(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3839 *, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_set_Item_m16812_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m26037(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3839 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16814_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m26038(__this, ___size, method) (( void (*) (Dictionary_2_t3839 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16816_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m26039(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3839 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16818_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m26040(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3843  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_make_pair_m16820_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m26041(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_pick_key_m16822_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m26042(__this /* static, unused */, ___key, ___value, method) (( ConstructorDelegate_t1286 * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_pick_value_m16824_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m26043(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3839 *, KeyValuePair_2U5BU5D_t4366*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16826_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Resize()
#define Dictionary_2_Resize_m26044(__this, method) (( void (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_Resize_m16828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m26045(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3839 *, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_Add_m16830_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define Dictionary_2_Clear_m26046(__this, method) (( void (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_Clear_m16832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m26047(__this, ___key, method) (( bool (*) (Dictionary_2_t3839 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m16834_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m26048(__this, ___value, method) (( bool (*) (Dictionary_2_t3839 *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_ContainsValue_m16836_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m26049(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3839 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m16838_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m26050(__this, ___sender, method) (( void (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16840_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m26051(__this, ___key, method) (( bool (*) (Dictionary_2_t3839 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m16842_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m26052(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3839 *, Type_t *, ConstructorDelegate_t1286 **, const MethodInfo*))Dictionary_2_TryGetValue_m16844_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m26053(__this, method) (( KeyCollection_t4368 * (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_get_Keys_m16846_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define Dictionary_2_get_Values_m26054(__this, method) (( ValueCollection_t4369 * (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_get_Values_m16848_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m26055(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16850_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m26056(__this, ___value, method) (( ConstructorDelegate_t1286 * (*) (Dictionary_2_t3839 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16852_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m26057(__this, ___pair, method) (( bool (*) (Dictionary_2_t3839 *, KeyValuePair_2_t3843 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16854_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m26058(__this, method) (( Enumerator_t4370  (*) (Dictionary_2_t3839 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16856_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m26059(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16858_gshared)(__this /* static, unused */, ___key, ___value, method)
