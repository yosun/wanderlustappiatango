﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct  DOGetter_1_t1032  : public MulticastDelegate_t307
{
};
