﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2621;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14008_gshared (GenericEqualityComparer_1_t2621 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14008(__this, method) (( void (*) (GenericEqualityComparer_1_t2621 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14008_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m27688_gshared (GenericEqualityComparer_1_t2621 * __this, DateTimeOffset_t1420  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m27688(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2621 *, DateTimeOffset_t1420 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m27688_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m27689_gshared (GenericEqualityComparer_1_t2621 * __this, DateTimeOffset_t1420  ___x, DateTimeOffset_t1420  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m27689(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2621 *, DateTimeOffset_t1420 , DateTimeOffset_t1420 , const MethodInfo*))GenericEqualityComparer_1_Equals_m27689_gshared)(__this, ___x, ___y, method)
