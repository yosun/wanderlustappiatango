﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
struct Enumerator_t3427;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t594;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t625;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19558(__this, ___l, method) (( void (*) (Enumerator_t3427 *, List_1_t625 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19559(__this, method) (( Object_t * (*) (Enumerator_t3427 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::Dispose()
#define Enumerator_Dispose_m19560(__this, method) (( void (*) (Enumerator_t3427 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::VerifyState()
#define Enumerator_VerifyState_m19561(__this, method) (( void (*) (Enumerator_t3427 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::MoveNext()
#define Enumerator_MoveNext_m19562(__this, method) (( bool (*) (Enumerator_t3427 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::get_Current()
#define Enumerator_get_Current_m19563(__this, method) (( DataSet_t594 * (*) (Enumerator_t3427 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
