﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t497;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C" void DebuggerHiddenAttribute__ctor_m2479 (DebuggerHiddenAttribute_t497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
