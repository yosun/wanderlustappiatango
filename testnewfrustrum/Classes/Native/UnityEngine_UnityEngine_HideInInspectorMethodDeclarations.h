﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.HideInInspector
struct HideInInspector_t900;

// System.Void UnityEngine.HideInInspector::.ctor()
extern "C" void HideInInspector__ctor_m4695 (HideInInspector_t900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
