﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>
struct InternalEnumerator_1_t3542;
// System.Object
struct Object_t;
// UnityEngine.MeshFilter
struct MeshFilter_t149;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m21681(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3542 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21682(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3542 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::Dispose()
#define InternalEnumerator_1_Dispose_m21683(__this, method) (( void (*) (InternalEnumerator_1_t3542 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21684(__this, method) (( bool (*) (InternalEnumerator_1_t3542 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::get_Current()
#define InternalEnumerator_1_get_Current_m21685(__this, method) (( MeshFilter_t149 * (*) (InternalEnumerator_1_t3542 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
