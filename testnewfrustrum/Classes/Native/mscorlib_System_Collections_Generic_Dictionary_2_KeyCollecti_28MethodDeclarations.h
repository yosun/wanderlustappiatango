﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyCollection_t3552;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4058;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Int32[]
struct Int32U5BU5D_t19;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m21857_gshared (KeyCollection_t3552 * __this, Dictionary_2_t869 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m21857(__this, ___dictionary, method) (( void (*) (KeyCollection_t3552 *, Dictionary_2_t869 *, const MethodInfo*))KeyCollection__ctor_m21857_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21858_gshared (KeyCollection_t3552 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21858(__this, ___item, method) (( void (*) (KeyCollection_t3552 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21858_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21859_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21859(__this, method) (( void (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21859_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21860_gshared (KeyCollection_t3552 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21860(__this, ___item, method) (( bool (*) (KeyCollection_t3552 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21860_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21861_gshared (KeyCollection_t3552 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21861(__this, ___item, method) (( bool (*) (KeyCollection_t3552 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21861_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21862_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21862(__this, method) (( Object_t* (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m21863_gshared (KeyCollection_t3552 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m21863(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3552 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m21863_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21864_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21864(__this, method) (( Object_t * (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21864_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21865_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21865(__this, method) (( bool (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21865_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21866_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21866(__this, method) (( bool (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m21867_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m21867(__this, method) (( Object_t * (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m21867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m21868_gshared (KeyCollection_t3552 * __this, Int32U5BU5D_t19* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m21868(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3552 *, Int32U5BU5D_t19*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m21868_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t3553  KeyCollection_GetEnumerator_m21869_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m21869(__this, method) (( Enumerator_t3553  (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_GetEnumerator_m21869_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m21870_gshared (KeyCollection_t3552 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m21870(__this, method) (( int32_t (*) (KeyCollection_t3552 *, const MethodInfo*))KeyCollection_get_Count_m21870_gshared)(__this, method)
