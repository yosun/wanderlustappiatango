﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t321;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t1348  : public MonoBehaviour_t7
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t124  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t321 * ___warningIcon_5;
};
