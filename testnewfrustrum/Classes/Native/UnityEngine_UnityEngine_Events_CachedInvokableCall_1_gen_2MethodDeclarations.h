﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t1438;
// UnityEngine.Object
struct Object_t123;
struct Object_t123_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m7035(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1438 *, Object_t123 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m26742_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m26743(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1438 *, ObjectU5BU5D_t115*, const MethodInfo*))CachedInvokableCall_1_Invoke_m26744_gshared)(__this, ___args, method)
