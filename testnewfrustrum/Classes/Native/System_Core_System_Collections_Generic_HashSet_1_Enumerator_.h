﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t755;
// UnityEngine.MeshRenderer
struct MeshRenderer_t148;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
struct  Enumerator_t894 
{
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::hashset
	HashSet_1_t755 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::current
	MeshRenderer_t148 * ___current_3;
};
