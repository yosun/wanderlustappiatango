﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparer_1_t3592;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct  Comparer_1_t3592  : public Object_t
{
};
struct Comparer_1_t3592_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::_default
	Comparer_1_t3592 * ____default_0;
};
