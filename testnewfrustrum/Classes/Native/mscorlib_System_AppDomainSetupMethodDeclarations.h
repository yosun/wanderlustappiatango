﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomainSetup
struct AppDomainSetup_t2493;

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m13189 (AppDomainSetup_t2493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
