﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t1022;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23317_gshared (DOSetter_1_t1022 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23317(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1022 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23317_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23318_gshared (DOSetter_1_t1022 * __this, Vector2_t10  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23318(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1022 *, Vector2_t10 , const MethodInfo*))DOSetter_1_Invoke_m23318_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23319_gshared (DOSetter_1_t1022 * __this, Vector2_t10  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23319(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1022 *, Vector2_t10 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23319_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23320_gshared (DOSetter_1_t1022 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23320(__this, ___result, method) (( void (*) (DOSetter_1_t1022 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23320_gshared)(__this, ___result, method)
