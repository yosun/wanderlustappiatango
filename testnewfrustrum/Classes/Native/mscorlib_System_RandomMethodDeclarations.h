﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Random
struct Random_t1270;

// System.Void System.Random::.ctor()
extern "C" void Random__ctor_m13839 (Random_t1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor(System.Int32)
extern "C" void Random__ctor_m6962 (Random_t1270 * __this, int32_t ___Seed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
