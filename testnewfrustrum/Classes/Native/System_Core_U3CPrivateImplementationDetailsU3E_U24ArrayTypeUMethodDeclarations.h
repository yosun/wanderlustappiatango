﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct U24ArrayTypeU24136_t1580;
struct U24ArrayTypeU24136_t1580_marshaled;

void U24ArrayTypeU24136_t1580_marshal(const U24ArrayTypeU24136_t1580& unmarshaled, U24ArrayTypeU24136_t1580_marshaled& marshaled);
void U24ArrayTypeU24136_t1580_marshal_back(const U24ArrayTypeU24136_t1580_marshaled& marshaled, U24ArrayTypeU24136_t1580& unmarshaled);
void U24ArrayTypeU24136_t1580_marshal_cleanup(U24ArrayTypeU24136_t1580_marshaled& marshaled);
