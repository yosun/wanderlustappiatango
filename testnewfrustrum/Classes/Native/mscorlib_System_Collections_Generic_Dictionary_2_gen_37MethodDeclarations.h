﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3776;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t4333;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t3780;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t3784;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int64>
struct IDictionary_2_t4337;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct KeyValuePair_2U5BU5D_t4338;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct IEnumerator_1_t4339;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_34.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor()
extern "C" void Dictionary_2__ctor_m25147_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m25147(__this, method) (( void (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2__ctor_m25147_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25149_gshared (Dictionary_2_t3776 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m25149(__this, ___comparer, method) (( void (*) (Dictionary_2_t3776 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25149_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m25151_gshared (Dictionary_2_t3776 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m25151(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3776 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25151_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m25153_gshared (Dictionary_2_t3776 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m25153(__this, ___capacity, method) (( void (*) (Dictionary_2_t3776 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m25153_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25155_gshared (Dictionary_2_t3776 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m25155(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3776 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25155_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m25157_gshared (Dictionary_2_t3776 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m25157(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3776 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m25157_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25159_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25159(__this, method) (( Object_t* (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25159_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25161_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25161(__this, method) (( Object_t* (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25161_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m25163_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25163(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25163_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25165_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25165(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25165_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25167_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m25167(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25167_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m25169_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m25169(__this, ___key, method) (( bool (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m25169_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25171_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m25171(__this, ___key, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25171_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25173_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25173(__this, method) (( bool (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25173_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25175_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25175(__this, method) (( Object_t * (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25177_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25177(__this, method) (( bool (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25179_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2_t3777  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25179(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3776 *, KeyValuePair_2_t3777 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25179_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25181_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2_t3777  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25181(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3776 *, KeyValuePair_2_t3777 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25181_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25183_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2U5BU5D_t4338* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25183(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3776 *, KeyValuePair_2U5BU5D_t4338*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25183_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25185_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2_t3777  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25185(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3776 *, KeyValuePair_2_t3777 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25185_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25187_gshared (Dictionary_2_t3776 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25187(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3776 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25187_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25189_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25189(__this, method) (( Object_t * (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25189_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25191_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25191(__this, method) (( Object_t* (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25191_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25193_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25193(__this, method) (( Object_t * (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25193_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m25195_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m25195(__this, method) (( int32_t (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_get_Count_m25195_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Item(TKey)
extern "C" int64_t Dictionary_2_get_Item_m25197_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m25197(__this, ___key, method) (( int64_t (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m25197_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m25199_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m25199(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_set_Item_m25199_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m25201_gshared (Dictionary_2_t3776 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m25201(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3776 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m25201_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m25203_gshared (Dictionary_2_t3776 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m25203(__this, ___size, method) (( void (*) (Dictionary_2_t3776 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m25203_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m25205_gshared (Dictionary_2_t3776 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m25205(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3776 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m25205_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3777  Dictionary_2_make_pair_m25207_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m25207(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3777  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_make_pair_m25207_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m25209_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m25209(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_key_m25209_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_value(TKey,TValue)
extern "C" int64_t Dictionary_2_pick_value_m25211_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m25211(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_value_m25211_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m25213_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2U5BU5D_t4338* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m25213(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3776 *, KeyValuePair_2U5BU5D_t4338*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m25213_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Resize()
extern "C" void Dictionary_2_Resize_m25215_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m25215(__this, method) (( void (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_Resize_m25215_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m25217_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m25217(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_Add_m25217_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Clear()
extern "C" void Dictionary_2_Clear_m25219_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m25219(__this, method) (( void (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_Clear_m25219_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m25221_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m25221(__this, ___key, method) (( bool (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m25221_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m25223_gshared (Dictionary_2_t3776 * __this, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m25223(__this, ___value, method) (( bool (*) (Dictionary_2_t3776 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m25223_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m25225_gshared (Dictionary_2_t3776 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m25225(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3776 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m25225_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m25227_gshared (Dictionary_2_t3776 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m25227(__this, ___sender, method) (( void (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m25227_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m25229_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m25229(__this, ___key, method) (( bool (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m25229_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m25231_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, int64_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m25231(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3776 *, Object_t *, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m25231_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Keys()
extern "C" KeyCollection_t3780 * Dictionary_2_get_Keys_m25233_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m25233(__this, method) (( KeyCollection_t3780 * (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_get_Keys_m25233_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Values()
extern "C" ValueCollection_t3784 * Dictionary_2_get_Values_m25235_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m25235(__this, method) (( ValueCollection_t3784 * (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_get_Values_m25235_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m25237_gshared (Dictionary_2_t3776 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m25237(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m25237_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTValue(System.Object)
extern "C" int64_t Dictionary_2_ToTValue_m25239_gshared (Dictionary_2_t3776 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m25239(__this, ___value, method) (( int64_t (*) (Dictionary_2_t3776 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m25239_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m25241_gshared (Dictionary_2_t3776 * __this, KeyValuePair_2_t3777  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m25241(__this, ___pair, method) (( bool (*) (Dictionary_2_t3776 *, KeyValuePair_2_t3777 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25241_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t3782  Dictionary_2_GetEnumerator_m25243_gshared (Dictionary_2_t3776 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m25243(__this, method) (( Enumerator_t3782  (*) (Dictionary_2_t3776 *, const MethodInfo*))Dictionary_2_GetEnumerator_m25243_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m25245_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m25245(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25245_gshared)(__this /* static, unused */, ___key, ___value, method)
