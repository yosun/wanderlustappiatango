﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextureRenderer
struct TextureRenderer_t727;
// UnityEngine.Texture
struct Texture_t321;
// UnityEngine.RenderTexture
struct RenderTexture_t786;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Int32 Vuforia.TextureRenderer::get_Width()
extern "C" int32_t TextureRenderer_get_Width_m4059 (TextureRenderer_t727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TextureRenderer::get_Height()
extern "C" int32_t TextureRenderer_get_Height_m4060 (TextureRenderer_t727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::.ctor(UnityEngine.Texture,System.Int32,Vuforia.QCARRenderer/Vec2I)
extern "C" void TextureRenderer__ctor_m4061 (TextureRenderer_t727 * __this, Texture_t321 * ___textureToRender, int32_t ___renderTextureLayer, Vec2I_t663  ___requestedTextureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Vuforia.TextureRenderer::Render()
extern "C" RenderTexture_t786 * TextureRenderer_Render_m4062 (TextureRenderer_t727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::Destroy()
extern "C" void TextureRenderer_Destroy_m4063 (TextureRenderer_t727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
