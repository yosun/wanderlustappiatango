﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t671;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t68;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
struct  Enumerator_t825 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::l
	List_1_t671 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::current
	ReconstructionAbstractBehaviour_t68 * ___current_3;
};
