﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int16>
struct InternalEnumerator_1_t3967;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27403_gshared (InternalEnumerator_1_t3967 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27403(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3967 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27403_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27404_gshared (InternalEnumerator_1_t3967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27404(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3967 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27404_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27405_gshared (InternalEnumerator_1_t3967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27405(__this, method) (( void (*) (InternalEnumerator_1_t3967 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27405_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27406_gshared (InternalEnumerator_1_t3967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27406(__this, method) (( bool (*) (InternalEnumerator_1_t3967 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27406_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m27407_gshared (InternalEnumerator_1_t3967 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27407(__this, method) (( int16_t (*) (InternalEnumerator_1_t3967 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27407_gshared)(__this, method)
