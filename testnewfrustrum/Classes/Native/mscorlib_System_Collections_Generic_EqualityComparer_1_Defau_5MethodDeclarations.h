﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>
struct DefaultComparer_t3578;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
extern "C" void DefaultComparer__ctor_m22079_gshared (DefaultComparer_t3578 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22079(__this, method) (( void (*) (DefaultComparer_t3578 *, const MethodInfo*))DefaultComparer__ctor_m22079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22080_gshared (DefaultComparer_t3578 * __this, VirtualButtonData_t643  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22080(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3578 *, VirtualButtonData_t643 , const MethodInfo*))DefaultComparer_GetHashCode_m22080_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22081_gshared (DefaultComparer_t3578 * __this, VirtualButtonData_t643  ___x, VirtualButtonData_t643  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22081(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3578 *, VirtualButtonData_t643 , VirtualButtonData_t643 , const MethodInfo*))DefaultComparer_Equals_m22081_gshared)(__this, ___x, ___y, method)
