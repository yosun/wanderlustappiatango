﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>
struct Enumerator_t3512;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t716;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21001(__this, ___l, method) (( void (*) (Enumerator_t3512 *, List_1_t716 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21002(__this, method) (( Object_t * (*) (Enumerator_t3512 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m21003(__this, method) (( void (*) (Enumerator_t3512 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::VerifyState()
#define Enumerator_VerifyState_m21004(__this, method) (( void (*) (Enumerator_t3512 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m21005(__this, method) (( bool (*) (Enumerator_t3512 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m21006(__this, method) (( TrackableBehaviour_t44 * (*) (Enumerator_t3512 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
