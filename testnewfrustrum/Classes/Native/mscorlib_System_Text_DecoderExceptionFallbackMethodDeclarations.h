﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderExceptionFallback
struct DecoderExceptionFallback_t2448;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2447;
// System.Object
struct Object_t;

// System.Void System.Text.DecoderExceptionFallback::.ctor()
extern "C" void DecoderExceptionFallback__ctor_m12886 (DecoderExceptionFallback_t2448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderExceptionFallback::CreateFallbackBuffer()
extern "C" DecoderFallbackBuffer_t2447 * DecoderExceptionFallback_CreateFallbackBuffer_m12887 (DecoderExceptionFallback_t2448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderExceptionFallback::Equals(System.Object)
extern "C" bool DecoderExceptionFallback_Equals_m12888 (DecoderExceptionFallback_t2448 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderExceptionFallback::GetHashCode()
extern "C" int32_t DecoderExceptionFallback_GetHashCode_m12889 (DecoderExceptionFallback_t2448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
