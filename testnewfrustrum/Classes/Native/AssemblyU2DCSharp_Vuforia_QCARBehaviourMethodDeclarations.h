﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARBehaviour
struct QCARBehaviour_t66;

// System.Void Vuforia.QCARBehaviour::.ctor()
extern "C" void QCARBehaviour__ctor_m209 (QCARBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARBehaviour::Awake()
extern "C" void QCARBehaviour_Awake_m210 (QCARBehaviour_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
