﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt64>
struct InternalEnumerator_1_t3817;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25693_gshared (InternalEnumerator_1_t3817 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25693(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3817 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25693_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25694_gshared (InternalEnumerator_1_t3817 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25694(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3817 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25694_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25695_gshared (InternalEnumerator_1_t3817 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25695(__this, method) (( void (*) (InternalEnumerator_1_t3817 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25695_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25696_gshared (InternalEnumerator_1_t3817 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25696(__this, method) (( bool (*) (InternalEnumerator_1_t3817 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25696_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C" uint64_t InternalEnumerator_1_get_Current_m25697_gshared (InternalEnumerator_1_t3817 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25697(__this, method) (( uint64_t (*) (InternalEnumerator_1_t3817 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25697_gshared)(__this, method)
