﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t54;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t60;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t86;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t77;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t62;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t36;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t93;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t75;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t64;

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m191 (VuforiaBehaviourComponentFactory_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t86 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t77 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t50 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t36 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t93 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m199 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t75 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t64 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
