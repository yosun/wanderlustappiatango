﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t42;

// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m214 (SurfaceBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
