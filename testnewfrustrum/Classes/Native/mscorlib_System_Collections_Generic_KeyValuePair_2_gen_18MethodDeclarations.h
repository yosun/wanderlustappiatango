﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
struct KeyValuePair_2_t3462;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m20125_gshared (KeyValuePair_2_t3462 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m20125(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3462 *, Object_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m20125_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m20126_gshared (KeyValuePair_2_t3462 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m20126(__this, method) (( Object_t * (*) (KeyValuePair_2_t3462 *, const MethodInfo*))KeyValuePair_2_get_Key_m20126_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m20127_gshared (KeyValuePair_2_t3462 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m20127(__this, ___value, method) (( void (*) (KeyValuePair_2_t3462 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m20127_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C" uint16_t KeyValuePair_2_get_Value_m20128_gshared (KeyValuePair_2_t3462 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m20128(__this, method) (( uint16_t (*) (KeyValuePair_2_t3462 *, const MethodInfo*))KeyValuePair_2_get_Value_m20128_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m20129_gshared (KeyValuePair_2_t3462 * __this, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m20129(__this, ___value, method) (( void (*) (KeyValuePair_2_t3462 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m20129_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m20130_gshared (KeyValuePair_2_t3462 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m20130(__this, method) (( String_t* (*) (KeyValuePair_2_t3462 *, const MethodInfo*))KeyValuePair_2_ToString_m20130_gshared)(__this, method)
