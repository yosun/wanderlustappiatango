﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t2296;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2292;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m12095 (ContextLevelActivator_t2296 * __this, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
