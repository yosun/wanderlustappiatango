﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct  GetDelegate_t1284  : public MulticastDelegate_t307
{
};
