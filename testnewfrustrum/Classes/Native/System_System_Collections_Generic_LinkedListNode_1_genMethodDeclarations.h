﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t817;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t659;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m19799_gshared (LinkedListNode_1_t817 * __this, LinkedList_1_t659 * ___list, int32_t ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m19799(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t817 *, LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedListNode_1__ctor_m19799_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m19800_gshared (LinkedListNode_1_t817 * __this, LinkedList_1_t659 * ___list, int32_t ___value, LinkedListNode_1_t817 * ___previousNode, LinkedListNode_1_t817 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m19800(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t817 *, LinkedList_1_t659 *, int32_t, LinkedListNode_1_t817 *, LinkedListNode_1_t817 *, const MethodInfo*))LinkedListNode_1__ctor_m19800_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C" void LinkedListNode_1_Detach_m19801_gshared (LinkedListNode_1_t817 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m19801(__this, method) (( void (*) (LinkedListNode_1_t817 *, const MethodInfo*))LinkedListNode_1_Detach_m19801_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C" LinkedList_1_t659 * LinkedListNode_1_get_List_m19802_gshared (LinkedListNode_1_t817 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m19802(__this, method) (( LinkedList_1_t659 * (*) (LinkedListNode_1_t817 *, const MethodInfo*))LinkedListNode_1_get_List_m19802_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C" LinkedListNode_1_t817 * LinkedListNode_1_get_Next_m4559_gshared (LinkedListNode_1_t817 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m4559(__this, method) (( LinkedListNode_1_t817 * (*) (LinkedListNode_1_t817 *, const MethodInfo*))LinkedListNode_1_get_Next_m4559_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C" int32_t LinkedListNode_1_get_Value_m4424_gshared (LinkedListNode_1_t817 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m4424(__this, method) (( int32_t (*) (LinkedListNode_1_t817 *, const MethodInfo*))LinkedListNode_1_get_Value_m4424_gshared)(__this, method)
