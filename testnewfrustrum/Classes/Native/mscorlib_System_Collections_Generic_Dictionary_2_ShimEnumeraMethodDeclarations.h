﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t3209;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3196;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16311_gshared (ShimEnumerator_t3209 * __this, Dictionary_2_t3196 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16311(__this, ___host, method) (( void (*) (ShimEnumerator_t3209 *, Dictionary_2_t3196 *, const MethodInfo*))ShimEnumerator__ctor_m16311_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16312_gshared (ShimEnumerator_t3209 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16312(__this, method) (( bool (*) (ShimEnumerator_t3209 *, const MethodInfo*))ShimEnumerator_MoveNext_m16312_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m16313_gshared (ShimEnumerator_t3209 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16313(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3209 *, const MethodInfo*))ShimEnumerator_get_Entry_m16313_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16314_gshared (ShimEnumerator_t3209 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16314(__this, method) (( Object_t * (*) (ShimEnumerator_t3209 *, const MethodInfo*))ShimEnumerator_get_Key_m16314_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16315_gshared (ShimEnumerator_t3209 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16315(__this, method) (( Object_t * (*) (ShimEnumerator_t3209 *, const MethodInfo*))ShimEnumerator_get_Value_m16315_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16316_gshared (ShimEnumerator_t3209 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16316(__this, method) (( Object_t * (*) (ShimEnumerator_t3209 *, const MethodInfo*))ShimEnumerator_get_Current_m16316_gshared)(__this, method)
