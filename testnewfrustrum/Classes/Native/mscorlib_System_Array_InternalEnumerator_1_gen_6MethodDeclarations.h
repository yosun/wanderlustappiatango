﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Renderer>
struct InternalEnumerator_1_t3107;
// System.Object
struct Object_t;
// UnityEngine.Renderer
struct Renderer_t8;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14892(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3107 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Renderer>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14893(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3107 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::Dispose()
#define InternalEnumerator_1_Dispose_m14894(__this, method) (( void (*) (InternalEnumerator_1_t3107 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Renderer>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14895(__this, method) (( bool (*) (InternalEnumerator_1_t3107 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Renderer>::get_Current()
#define InternalEnumerator_1_get_Current_m14896(__this, method) (( Renderer_t8 * (*) (InternalEnumerator_1_t3107 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
