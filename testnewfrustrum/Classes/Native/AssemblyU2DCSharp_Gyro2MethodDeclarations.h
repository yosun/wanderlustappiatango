﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Gyro2
struct Gyro2_t12;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Gyro2::.ctor()
extern "C" void Gyro2__ctor_m13 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::.cctor()
extern "C" void Gyro2__cctor_m14 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Awake()
extern "C" void Gyro2_Awake_m15 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Start()
extern "C" void Gyro2_Start_m16 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern "C" Vector3_t15  Gyro2_ClampPosInRoom_m17 (Object_t * __this /* static, unused */, Vector3_t15  ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void Gyro2_StationParent_m18 (Object_t * __this /* static, unused */, Vector3_t15  ___pos, Quaternion_t13  ___rot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern "C" void Gyro2_StationParentShiftAngle_m19 (Gyro2_t12 * __this, float ___angle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Init(UnityEngine.Transform)
extern "C" void Gyro2_Init_m20 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::ResetStatic()
extern "C" void Gyro2_ResetStatic_m21 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::ResetStaticSmoothly()
extern "C" void Gyro2_ResetStaticSmoothly_m22 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Reset()
extern "C" void Gyro2_Reset_m23 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern "C" void Gyro2_DoSwipeRotation_m24 (Gyro2_t12 * __this, float ___degree, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::DisableSettingsButotn()
extern "C" void Gyro2_DisableSettingsButotn_m25 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::Update()
extern "C" void Gyro2_Update_m26 (Gyro2_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::AssignCalibration()
extern "C" void Gyro2_AssignCalibration_m27 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gyro2::AssignCamBaseRotation()
extern "C" void Gyro2_AssignCamBaseRotation_m28 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Gyro2::G()
extern "C" Quaternion_t13  Gyro2_G_m29 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t13  Gyro2_ConvertRotation_m30 (Object_t * __this /* static, unused */, Quaternion_t13  ___q, const MethodInfo* method) IL2CPP_METHOD_ATTR;
