﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyCollection_t3854;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t1415;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t4139;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.String[]
struct StringU5BU5D_t109;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_83.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_10MethodDeclarations.h"
#define KeyCollection__ctor_m26277(__this, ___dictionary, method) (( void (*) (KeyCollection_t3854 *, Dictionary_2_t1415 *, const MethodInfo*))KeyCollection__ctor_m16914_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m26278(__this, ___item, method) (( void (*) (KeyCollection_t3854 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16915_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m26279(__this, method) (( void (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m26280(__this, ___item, method) (( bool (*) (KeyCollection_t3854 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16917_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m26281(__this, ___item, method) (( bool (*) (KeyCollection_t3854 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16918_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m26282(__this, method) (( Object_t* (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16919_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m26283(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3854 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m16920_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m26284(__this, method) (( Object_t * (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m26285(__this, method) (( bool (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16922_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m26286(__this, method) (( bool (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16923_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m26287(__this, method) (( Object_t * (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m16924_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m26288(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3854 *, StringU5BU5D_t109*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m16925_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetEnumerator()
#define KeyCollection_GetEnumerator_m26289(__this, method) (( Enumerator_t4385  (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_GetEnumerator_m16926_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Count()
#define KeyCollection_get_Count_m26290(__this, method) (( int32_t (*) (KeyCollection_t3854 *, const MethodInfo*))KeyCollection_get_Count_m16927_gshared)(__this, method)
