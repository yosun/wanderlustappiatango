﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Collections.Generic.ICollection`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct ICollection_1_t4239;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct KeyCollection_t3568;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ValueCollection_t3572;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3193;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct IDictionary_2_t4240;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t4241;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>
struct IEnumerator_1_t4242;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__24.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
extern "C" void Dictionary_2__ctor_m4562_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4562(__this, method) (( void (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2__ctor_m4562_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21937_gshared (Dictionary_2_t870 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21937(__this, ___comparer, method) (( void (*) (Dictionary_2_t870 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21937_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21938_gshared (Dictionary_2_t870 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21938(__this, ___dictionary, method) (( void (*) (Dictionary_2_t870 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21938_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21939_gshared (Dictionary_2_t870 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21939(__this, ___capacity, method) (( void (*) (Dictionary_2_t870 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21939_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21940_gshared (Dictionary_2_t870 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21940(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t870 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21940_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21941_gshared (Dictionary_2_t870 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21941(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t870 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m21941_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21942_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21942(__this, method) (( Object_t* (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21942_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21943_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21943(__this, method) (( Object_t* (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21943_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21944_gshared (Dictionary_2_t870 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21944(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21944_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21945_gshared (Dictionary_2_t870 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21945(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t870 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21945_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21946_gshared (Dictionary_2_t870 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21946(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t870 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21946_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21947_gshared (Dictionary_2_t870 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21947(__this, ___key, method) (( bool (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21947_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21948_gshared (Dictionary_2_t870 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21948(__this, ___key, method) (( void (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21948_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21949_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21949(__this, method) (( bool (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21949_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21950_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21950(__this, method) (( Object_t * (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21951_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21951(__this, method) (( bool (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21951_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21952_gshared (Dictionary_2_t870 * __this, KeyValuePair_2_t3565  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21952(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t870 *, KeyValuePair_2_t3565 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21952_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21953_gshared (Dictionary_2_t870 * __this, KeyValuePair_2_t3565  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21953(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t870 *, KeyValuePair_2_t3565 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21953_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21954_gshared (Dictionary_2_t870 * __this, KeyValuePair_2U5BU5D_t4241* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21954(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t870 *, KeyValuePair_2U5BU5D_t4241*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21954_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21955_gshared (Dictionary_2_t870 * __this, KeyValuePair_2_t3565  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21955(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t870 *, KeyValuePair_2_t3565 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21955_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21956_gshared (Dictionary_2_t870 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21956(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t870 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21956_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21957_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21957(__this, method) (( Object_t * (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21957_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21958_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21958(__this, method) (( Object_t* (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21958_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21959_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21959(__this, method) (( Object_t * (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21959_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21960_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21960(__this, method) (( int32_t (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_get_Count_m21960_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Item(TKey)
extern "C" VirtualButtonData_t643  Dictionary_2_get_Item_m21961_gshared (Dictionary_2_t870 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21961(__this, ___key, method) (( VirtualButtonData_t643  (*) (Dictionary_2_t870 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m21961_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21962_gshared (Dictionary_2_t870 * __this, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21962(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t870 *, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_set_Item_m21962_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21963_gshared (Dictionary_2_t870 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21963(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t870 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21963_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21964_gshared (Dictionary_2_t870 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21964(__this, ___size, method) (( void (*) (Dictionary_2_t870 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21964_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21965_gshared (Dictionary_2_t870 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21965(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t870 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21965_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3565  Dictionary_2_make_pair_m21966_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21966(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3565  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_make_pair_m21966_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m21967_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21967(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_pick_key_m21967_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::pick_value(TKey,TValue)
extern "C" VirtualButtonData_t643  Dictionary_2_pick_value_m21968_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21968(__this /* static, unused */, ___key, ___value, method) (( VirtualButtonData_t643  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_pick_value_m21968_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21969_gshared (Dictionary_2_t870 * __this, KeyValuePair_2U5BU5D_t4241* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21969(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t870 *, KeyValuePair_2U5BU5D_t4241*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21969_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Resize()
extern "C" void Dictionary_2_Resize_m21970_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21970(__this, method) (( void (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_Resize_m21970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21971_gshared (Dictionary_2_t870 * __this, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21971(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t870 *, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_Add_m21971_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Clear()
extern "C" void Dictionary_2_Clear_m21972_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21972(__this, method) (( void (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_Clear_m21972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21973_gshared (Dictionary_2_t870 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21973(__this, ___key, method) (( bool (*) (Dictionary_2_t870 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m21973_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21974_gshared (Dictionary_2_t870 * __this, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21974(__this, ___value, method) (( bool (*) (Dictionary_2_t870 *, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_ContainsValue_m21974_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21975_gshared (Dictionary_2_t870 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21975(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t870 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m21975_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21976_gshared (Dictionary_2_t870 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21976(__this, ___sender, method) (( void (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21976_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21977_gshared (Dictionary_2_t870 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21977(__this, ___key, method) (( bool (*) (Dictionary_2_t870 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m21977_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21978_gshared (Dictionary_2_t870 * __this, int32_t ___key, VirtualButtonData_t643 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21978(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t870 *, int32_t, VirtualButtonData_t643 *, const MethodInfo*))Dictionary_2_TryGetValue_m21978_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Keys()
extern "C" KeyCollection_t3568 * Dictionary_2_get_Keys_m21979_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21979(__this, method) (( KeyCollection_t3568 * (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_get_Keys_m21979_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Values()
extern "C" ValueCollection_t3572 * Dictionary_2_get_Values_m21980_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21980(__this, method) (( ValueCollection_t3572 * (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_get_Values_m21980_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m21981_gshared (Dictionary_2_t870 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21981(__this, ___key, method) (( int32_t (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21981_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToTValue(System.Object)
extern "C" VirtualButtonData_t643  Dictionary_2_ToTValue_m21982_gshared (Dictionary_2_t870 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21982(__this, ___value, method) (( VirtualButtonData_t643  (*) (Dictionary_2_t870 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21982_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21983_gshared (Dictionary_2_t870 * __this, KeyValuePair_2_t3565  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21983(__this, ___pair, method) (( bool (*) (Dictionary_2_t870 *, KeyValuePair_2_t3565 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21983_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3570  Dictionary_2_GetEnumerator_m21984_gshared (Dictionary_2_t870 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21984(__this, method) (( Enumerator_t3570  (*) (Dictionary_2_t870 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21984_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m21985_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21985(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t643 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21985_gshared)(__this /* static, unused */, ___key, ___value, method)
