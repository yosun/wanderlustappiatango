﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>
struct IList_1_t3509;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TrackableBehaviour>
struct  ReadOnlyCollection_1_t3510  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TrackableBehaviour>::list
	Object_t* ___list_0;
};
