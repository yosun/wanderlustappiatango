﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>
struct Enumerator_t3678;
// System.Object
struct Object_t;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t947;

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23590(__this, ___l, method) (( void (*) (Enumerator_t3678 *, List_1_t947 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23591(__this, method) (( Object_t * (*) (Enumerator_t3678 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::Dispose()
#define Enumerator_Dispose_m23592(__this, method) (( void (*) (Enumerator_t3678 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::VerifyState()
#define Enumerator_VerifyState_m23593(__this, method) (( void (*) (Enumerator_t3678 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::MoveNext()
#define Enumerator_MoveNext_m23594(__this, method) (( bool (*) (Enumerator_t3678 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::get_Current()
#define Enumerator_get_Current_m23595(__this, method) (( ABSSequentiable_t943 * (*) (Enumerator_t3678 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
