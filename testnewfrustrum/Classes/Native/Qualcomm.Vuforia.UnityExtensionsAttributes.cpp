﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t897_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1269);
		DebuggableAttribute_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1270);
		CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1271);
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 10;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Eyewear"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t897 * tmp;
		tmp = (DebuggableAttribute_t897 *)il2cpp_codegen_object_new (DebuggableAttribute_t897_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4679(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t898 * tmp;
		tmp = (CompilationRelaxationsAttribute_t898 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m4680(tmp, 8, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.NUnitTests"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Editor"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium.Editor"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.UnitTests"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void BackgroundPlaneAbstractBehaviour_t32_CustomAttributesCacheGenerator_mNumDivisions(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelHeightInt_m2652(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelWidthInt_m2653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMaxDepthForVideoBackground_m2654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMinDepthForVideoBackground_m2655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void TrackableBehaviour_t44_CustomAttributesCacheGenerator_mTrackableName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void TrackableBehaviour_t44_CustomAttributesCacheGenerator_mPreviousScale(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void TrackableBehaviour_t44_CustomAttributesCacheGenerator_mPreserveChildSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TrackableBehaviour_t44_CustomAttributesCacheGenerator_mInitializedInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mDataSetPath(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mExtendedTracking(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mInitializeSmartTerrain(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mReconstructionToInitialize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMax(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mIsSmartTerrainOccluderOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderRotation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderLockedInPlace(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mAutoSetOccluderFromTargetSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mAspectRatioXY(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mAspectRatioXZ(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mShowBoundingBox(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mBBoxMin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mBBoxMax(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mPreviewImage(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_get_Name_m2713(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_set_Name_m2714(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_get_ID_m2715(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_set_ID_m2716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
extern const Il2CppType* ReconstructionAbstractBehaviour_t68_0_0_0_var;
extern TypeInfo* RequireComponent_t163_il2cpp_TypeInfo_var;
void ReconstructionFromTargetAbstractBehaviour_t70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionAbstractBehaviour_t68_0_0_0_var = il2cpp_codegen_type_from_index(231);
		RequireComponent_t163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t163 * tmp;
		tmp = (RequireComponent_t163 *)il2cpp_codegen_object_new (RequireComponent_t163_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m513(tmp, il2cpp_codegen_type_get_object(ReconstructionAbstractBehaviour_t68_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void SmartTerrainTrackableBehaviour_t591_CustomAttributesCacheGenerator_mMeshFilterToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void SmartTerrainTrackableBehaviour_t591_CustomAttributesCacheGenerator_mMeshColliderToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoInitTracker(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoStartTracker(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoInitBuilder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mSceneUnitsToMillimeter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void CylinderTargetAbstractBehaviour_t36_CustomAttributesCacheGenerator_mTopDiameterRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void CylinderTargetAbstractBehaviour_t36_CustomAttributesCacheGenerator_mBottomDiameterRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void DataSet_t594_CustomAttributesCacheGenerator_DataSet_Exists_m2807(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is deprecated, please use the QCARUnity.StorageType version of the Exists method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void DataSet_t594_CustomAttributesCacheGenerator_DataSet_Load_m4825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is deprecated, please use the QCARUnity.StorageType version of the Load method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mDataSetsToLoad(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mDataSetsToActivate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mExternalDatasetRoots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Center_m2815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Center_m2816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_HalfExtents_m2817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_HalfExtents_m2818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Rotation_m2819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Rotation_m2820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CRotationYU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_Center_m2822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_Center_m2823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_HalfExtents_m2824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_HalfExtents_m2825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_RotationY_m2826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_RotationY_m2827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void DataSetImpl_t578_CustomAttributesCacheGenerator_DataSetImpl_Load_m2891(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("The DataSet.StorageType enumeration is deprecated, please use QCARUnity.StorageType instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Tracker_t623_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Tracker_t623_CustomAttributesCacheGenerator_Tracker_get_IsActive_m2944(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Tracker_t623_CustomAttributesCacheGenerator_Tracker_set_IsActive_m2945(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MarkerImpl_t628_CustomAttributesCacheGenerator_U3CMarkerIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MarkerImpl_t628_CustomAttributesCacheGenerator_MarkerImpl_get_MarkerID_m2962(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MarkerImpl_t628_CustomAttributesCacheGenerator_MarkerImpl_set_MarkerID_m2963(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void QCARManagerImpl_t660_CustomAttributesCacheGenerator_U3CVideoBackgroundTextureSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void QCARManagerImpl_t660_CustomAttributesCacheGenerator_QCARManagerImpl_set_VideoBackgroundTextureSet_m3013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void QCARManagerImpl_t660_CustomAttributesCacheGenerator_QCARManagerImpl_get_VideoBackgroundTextureSet_m3014(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass3_t655_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_U3CParentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_get_Parent_m3059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_set_Parent_m3060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordList_t678_CustomAttributesCacheGenerator_WordList_LoadWordListFile_m5037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordList_t678_CustomAttributesCacheGenerator_WordList_AddWordsFromFile_m5040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordList_t678_CustomAttributesCacheGenerator_WordList_LoadFilterListFile_m5049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_LoadWordListFile_m3154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_AddWordsFromFile_m3157(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_LoadFilterListFile_m3166(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void ISmartTerrainEventHandler_t777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("This interface will be removed with the next Vuforia release. Please use ReconstructionBehaviour.Register...Callback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* RequireComponent_t163_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		RequireComponent_t163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t163 * tmp;
		tmp = (RequireComponent_t163 *)il2cpp_codegen_object_new (RequireComponent_t163_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m513(tmp, il2cpp_codegen_type_get_object(QCARAbstractBehaviour_t67_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepARCameraAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepTrackableBehavioursAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepTextRecoBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepUDTBuildingBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepCloudRecoBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepSmartTerrainAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mInitializedInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mMaximumExtentEnabled(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mMaximumExtent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mAutomaticStart(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mNavMeshUpdates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mNavMeshPadding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3970(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("This ISmartTerrainEventHandler interface will be removed with the next Vuforia release. Please use ReconstructionBehaviour.Register...Callback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void PropAbstractBehaviour_t65_CustomAttributesCacheGenerator_mBoxColliderToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t726_CustomAttributesCacheGenerator_U3CTrackableSourcePtrU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t726_CustomAttributesCacheGenerator_TrackableSourceImpl_get_TrackableSourcePtr_m4056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t726_CustomAttributesCacheGenerator_TrackableSourceImpl_set_TrackableSourcePtr_m4057(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void WebCamImpl_t610_CustomAttributesCacheGenerator_U3CIsTextureSizeAvailableU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void WebCamImpl_t610_CustomAttributesCacheGenerator_WebCamImpl_get_IsTextureSizeAvailable_m4082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void WebCamImpl_t610_CustomAttributesCacheGenerator_WebCamImpl_set_IsTextureSizeAvailable_m4083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void ITrackerEventHandler_t788_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("This interface will be removed with the next Vuforia release. Please use QCARBehaviour.RegisterQCARStartedCallback and RegisterTrackablesUpdatedCallback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void ImageTargetAbstractBehaviour_t50_CustomAttributesCacheGenerator_mAspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void ImageTargetAbstractBehaviour_t50_CustomAttributesCacheGenerator_mImageTargetType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void MarkerAbstractBehaviour_t58_CustomAttributesCacheGenerator_mMarkerID(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_VuforiaLicenseKey(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mCameraOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mAutoAdjustStereoCameraSkewing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mSceneScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_CameraDeviceModeSetting(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MaxSimultaneousImageTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MaxSimultaneousObjectTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_UseDelayedLoadingObjectTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_CameraDirection(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MirrorVideoBackground(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mWorldCenterMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mWorldCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mPrimaryCamera(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mSecondaryCamera(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_QCARAbstractBehaviour_RegisterTrackerEventHandler_m4145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("The ITrackerEventHandler interface will be removed with the next Vuforia release. Please use QCARBehaviour.RegisterQCARStartedCallback and RegisterTrackablesUpdatedCallback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWordListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mCustomWordListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAdditionalCustomWords(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mFilterMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mFilterListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAdditionalFilterWords(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWordPrefabCreationMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mMaximumWordInstances(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t3_0_0_0_var;
extern TypeInfo* RequireComponent_t163_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_0_0_0_var = il2cpp_codegen_type_from_index(24);
		RequireComponent_t163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t163 * tmp;
		tmp = (RequireComponent_t163 *)il2cpp_codegen_object_new (RequireComponent_t163_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m513(tmp, il2cpp_codegen_type_get_object(Camera_t3_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mHasUpdatedPose(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mPrevTransform(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mPrevParent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mPlayModeRenderVideo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mDeviceNameSetInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mFlipHorizontally(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mTurnOffWebCam(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void WordAbstractBehaviour_t93_CustomAttributesCacheGenerator_mMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t900_il2cpp_TypeInfo_var;
void WordAbstractBehaviour_t93_CustomAttributesCacheGenerator_mSpecificWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		HideInInspector_t900_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t900 * tmp;
		tmp = (HideInInspector_t900 *)il2cpp_codegen_object_new (HideInInspector_t900_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4695(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t759_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_Qualcomm_Vuforia_UnityExtensions_Assembly_AttributeGenerators[152] = 
{
	NULL,
	g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator,
	BackgroundPlaneAbstractBehaviour_t32_CustomAttributesCacheGenerator_mNumDivisions,
	UnityCameraExtensions_t564_CustomAttributesCacheGenerator,
	UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelHeightInt_m2652,
	UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelWidthInt_m2653,
	UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMaxDepthForVideoBackground_m2654,
	UnityCameraExtensions_t564_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMinDepthForVideoBackground_m2655,
	TrackableBehaviour_t44_CustomAttributesCacheGenerator_mTrackableName,
	TrackableBehaviour_t44_CustomAttributesCacheGenerator_mPreviousScale,
	TrackableBehaviour_t44_CustomAttributesCacheGenerator_mPreserveChildSize,
	TrackableBehaviour_t44_CustomAttributesCacheGenerator_mInitializedInEditor,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mDataSetPath,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mExtendedTracking,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mInitializeSmartTerrain,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mReconstructionToInitialize,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMin,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMax,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mIsSmartTerrainOccluderOffset,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderOffset,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderRotation,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mSmartTerrainOccluderLockedInPlace,
	DataSetTrackableBehaviour_t567_CustomAttributesCacheGenerator_mAutoSetOccluderFromTargetSize,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mAspectRatioXY,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mAspectRatioXZ,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mShowBoundingBox,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mBBoxMin,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mBBoxMax,
	ObjectTargetAbstractBehaviour_t64_CustomAttributesCacheGenerator_mPreviewImage,
	TrackableImpl_t577_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	TrackableImpl_t577_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_get_Name_m2713,
	TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_set_Name_m2714,
	TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_get_ID_m2715,
	TrackableImpl_t577_CustomAttributesCacheGenerator_TrackableImpl_set_ID_m2716,
	ReconstructionFromTargetAbstractBehaviour_t70_CustomAttributesCacheGenerator,
	SmartTerrainTrackableBehaviour_t591_CustomAttributesCacheGenerator_mMeshFilterToUpdate,
	SmartTerrainTrackableBehaviour_t591_CustomAttributesCacheGenerator_mMeshColliderToUpdate,
	SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoInitTracker,
	SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoStartTracker,
	SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAutoInitBuilder,
	SmartTerrainTrackerAbstractBehaviour_t72_CustomAttributesCacheGenerator_mSceneUnitsToMillimeter,
	CylinderTargetAbstractBehaviour_t36_CustomAttributesCacheGenerator_mTopDiameterRatio,
	CylinderTargetAbstractBehaviour_t36_CustomAttributesCacheGenerator_mBottomDiameterRatio,
	DataSet_t594_CustomAttributesCacheGenerator_DataSet_Exists_m2807,
	DataSet_t594_CustomAttributesCacheGenerator_DataSet_Load_m4825,
	DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mDataSetsToLoad,
	DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mDataSetsToActivate,
	DataSetLoadAbstractBehaviour_t38_CustomAttributesCacheGenerator_mExternalDatasetRoots,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Center_m2815,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Center_m2816,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_HalfExtents_m2817,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_HalfExtents_m2818,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Rotation_m2819,
	OrientedBoundingBox_t598_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Rotation_m2820,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_U3CRotationYU3Ek__BackingField,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_Center_m2822,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_Center_m2823,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_HalfExtents_m2824,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_HalfExtents_m2825,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_RotationY_m2826,
	OrientedBoundingBox3D_t599_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_RotationY_m2827,
	DataSetImpl_t578_CustomAttributesCacheGenerator_DataSetImpl_Load_m2891,
	Tracker_t623_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField,
	Tracker_t623_CustomAttributesCacheGenerator_Tracker_get_IsActive_m2944,
	Tracker_t623_CustomAttributesCacheGenerator_Tracker_set_IsActive_m2945,
	MarkerImpl_t628_CustomAttributesCacheGenerator_U3CMarkerIDU3Ek__BackingField,
	MarkerImpl_t628_CustomAttributesCacheGenerator_MarkerImpl_get_MarkerID_m2962,
	MarkerImpl_t628_CustomAttributesCacheGenerator_MarkerImpl_set_MarkerID_m2963,
	QCARManagerImpl_t660_CustomAttributesCacheGenerator_U3CVideoBackgroundTextureSetU3Ek__BackingField,
	QCARManagerImpl_t660_CustomAttributesCacheGenerator_QCARManagerImpl_set_VideoBackgroundTextureSet_m3013,
	QCARManagerImpl_t660_CustomAttributesCacheGenerator_QCARManagerImpl_get_VideoBackgroundTextureSet_m3014,
	U3CU3Ec__DisplayClass3_t655_CustomAttributesCacheGenerator,
	SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_U3CParentU3Ek__BackingField,
	SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_get_Parent_m3059,
	SmartTerrainTrackableImpl_t669_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_set_Parent_m3060,
	WordList_t678_CustomAttributesCacheGenerator_WordList_LoadWordListFile_m5037,
	WordList_t678_CustomAttributesCacheGenerator_WordList_AddWordsFromFile_m5040,
	WordList_t678_CustomAttributesCacheGenerator_WordList_LoadFilterListFile_m5049,
	WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_LoadWordListFile_m3154,
	WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_AddWordsFromFile_m3157,
	WordListImpl_t698_CustomAttributesCacheGenerator_WordListImpl_LoadFilterListFile_m3166,
	ISmartTerrainEventHandler_t777_CustomAttributesCacheGenerator,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepARCameraAlive,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepTrackableBehavioursAlive,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepTextRecoBehaviourAlive,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepUDTBuildingBehaviourAlive,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepCloudRecoBehaviourAlive,
	KeepAliveAbstractBehaviour_t56_CustomAttributesCacheGenerator_mKeepSmartTerrainAlive,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mInitializedInEditor,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mMaximumExtentEnabled,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mMaximumExtent,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mAutomaticStart,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mNavMeshUpdates,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_mNavMeshPadding,
	ReconstructionAbstractBehaviour_t68_CustomAttributesCacheGenerator_ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3970,
	PropAbstractBehaviour_t65_CustomAttributesCacheGenerator_mBoxColliderToUpdate,
	TrackableSourceImpl_t726_CustomAttributesCacheGenerator_U3CTrackableSourcePtrU3Ek__BackingField,
	TrackableSourceImpl_t726_CustomAttributesCacheGenerator_TrackableSourceImpl_get_TrackableSourcePtr_m4056,
	TrackableSourceImpl_t726_CustomAttributesCacheGenerator_TrackableSourceImpl_set_TrackableSourcePtr_m4057,
	WebCamImpl_t610_CustomAttributesCacheGenerator_U3CIsTextureSizeAvailableU3Ek__BackingField,
	WebCamImpl_t610_CustomAttributesCacheGenerator_WebCamImpl_get_IsTextureSizeAvailable_m4082,
	WebCamImpl_t610_CustomAttributesCacheGenerator_WebCamImpl_set_IsTextureSizeAvailable_m4083,
	ITrackerEventHandler_t788_CustomAttributesCacheGenerator,
	ImageTargetAbstractBehaviour_t50_CustomAttributesCacheGenerator_mAspectRatio,
	ImageTargetAbstractBehaviour_t50_CustomAttributesCacheGenerator_mImageTargetType,
	MarkerAbstractBehaviour_t58_CustomAttributesCacheGenerator_mMarkerID,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_VuforiaLicenseKey,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mCameraOffset,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mAutoAdjustStereoCameraSkewing,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mSceneScaleFactor,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_CameraDeviceModeSetting,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MaxSimultaneousImageTargets,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MaxSimultaneousObjectTargets,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_UseDelayedLoadingObjectTargets,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_CameraDirection,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_MirrorVideoBackground,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mWorldCenterMode,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mWorldCenter,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mPrimaryCamera,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_mSecondaryCamera,
	QCARAbstractBehaviour_t67_CustomAttributesCacheGenerator_QCARAbstractBehaviour_RegisterTrackerEventHandler_m4145,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWordListFile,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mCustomWordListFile,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAdditionalCustomWords,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mFilterMode,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mFilterListFile,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAdditionalFilterWords,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWordPrefabCreationMode,
	TextRecoAbstractBehaviour_t75_CustomAttributesCacheGenerator_mMaximumWordInstances,
	VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator,
	VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField,
	VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4225,
	VideoBackgroundAbstractBehaviour_t82_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4226,
	VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mName,
	VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mSensitivity,
	VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mHasUpdatedPose,
	VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mPrevTransform,
	VirtualButtonAbstractBehaviour_t86_CustomAttributesCacheGenerator_mPrevParent,
	WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mPlayModeRenderVideo,
	WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mDeviceNameSetInEditor,
	WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mFlipHorizontally,
	WebCamAbstractBehaviour_t88_CustomAttributesCacheGenerator_mTurnOffWebCam,
	WordAbstractBehaviour_t93_CustomAttributesCacheGenerator_mMode,
	WordAbstractBehaviour_t93_CustomAttributesCacheGenerator_mSpecificWord,
	U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t759_CustomAttributesCacheGenerator,
};
