﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
struct KeyValuePair_2_t3792;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_34MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25349(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3792 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m25251_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m25350(__this, method) (( String_t* (*) (KeyValuePair_2_t3792 *, const MethodInfo*))KeyValuePair_2_get_Key_m25252_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25351(__this, ___value, method) (( void (*) (KeyValuePair_2_t3792 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m25253_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m25352(__this, method) (( int64_t (*) (KeyValuePair_2_t3792 *, const MethodInfo*))KeyValuePair_2_get_Value_m25254_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25353(__this, ___value, method) (( void (*) (KeyValuePair_2_t3792 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m25255_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m25354(__this, method) (( String_t* (*) (KeyValuePair_2_t3792 *, const MethodInfo*))KeyValuePair_2_ToString_m25256_gshared)(__this, method)
