﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t624;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t578;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSetImpl>
struct IEnumerable_1_t4160;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSetImpl>
struct IEnumerator_1_t4161;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.DataSetImpl>
struct ICollection_1_t4162;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSetImpl>
struct ReadOnlyCollection_1_t3420;
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t3418;
// System.Predicate`1<Vuforia.DataSetImpl>
struct Predicate_1_t3421;
// System.Comparison`1<Vuforia.DataSetImpl>
struct Comparison_1_t3422;
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4384(__this, method) (( void (*) (List_1_t624 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4389(__this, ___collection, method) (( void (*) (List_1_t624 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor(System.Int32)
#define List_1__ctor_m19386(__this, ___capacity, method) (( void (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.cctor()
#define List_1__cctor_m19387(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19388(__this, method) (( Object_t* (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19389(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t624 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19390(__this, method) (( Object_t * (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19391(__this, ___item, method) (( int32_t (*) (List_1_t624 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19392(__this, ___item, method) (( bool (*) (List_1_t624 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19393(__this, ___item, method) (( int32_t (*) (List_1_t624 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19394(__this, ___index, ___item, method) (( void (*) (List_1_t624 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19395(__this, ___item, method) (( void (*) (List_1_t624 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19396(__this, method) (( bool (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19397(__this, method) (( bool (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19398(__this, method) (( Object_t * (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19399(__this, method) (( bool (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19400(__this, method) (( bool (*) (List_1_t624 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19401(__this, ___index, method) (( Object_t * (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19402(__this, ___index, ___value, method) (( void (*) (List_1_t624 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Add(T)
#define List_1_Add_m19403(__this, ___item, method) (( void (*) (List_1_t624 *, DataSetImpl_t578 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19404(__this, ___newCount, method) (( void (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19405(__this, ___collection, method) (( void (*) (List_1_t624 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19406(__this, ___enumerable, method) (( void (*) (List_1_t624 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19407(__this, ___collection, method) (( void (*) (List_1_t624 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AsReadOnly()
#define List_1_AsReadOnly_m19408(__this, method) (( ReadOnlyCollection_1_t3420 * (*) (List_1_t624 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Clear()
#define List_1_Clear_m19409(__this, method) (( void (*) (List_1_t624 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Contains(T)
#define List_1_Contains_m19410(__this, ___item, method) (( bool (*) (List_1_t624 *, DataSetImpl_t578 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19411(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t624 *, DataSetImplU5BU5D_t3418*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Find(System.Predicate`1<T>)
#define List_1_Find_m19412(__this, ___match, method) (( DataSetImpl_t578 * (*) (List_1_t624 *, Predicate_1_t3421 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19413(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3421 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19414(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t624 *, int32_t, int32_t, Predicate_1_t3421 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GetEnumerator()
#define List_1_GetEnumerator_m4386(__this, method) (( Enumerator_t809  (*) (List_1_t624 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::IndexOf(T)
#define List_1_IndexOf_m19415(__this, ___item, method) (( int32_t (*) (List_1_t624 *, DataSetImpl_t578 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19416(__this, ___start, ___delta, method) (( void (*) (List_1_t624 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19417(__this, ___index, method) (( void (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Insert(System.Int32,T)
#define List_1_Insert_m19418(__this, ___index, ___item, method) (( void (*) (List_1_t624 *, int32_t, DataSetImpl_t578 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19419(__this, ___collection, method) (( void (*) (List_1_t624 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Remove(T)
#define List_1_Remove_m19420(__this, ___item, method) (( bool (*) (List_1_t624 *, DataSetImpl_t578 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19421(__this, ___match, method) (( int32_t (*) (List_1_t624 *, Predicate_1_t3421 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19422(__this, ___index, method) (( void (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Reverse()
#define List_1_Reverse_m19423(__this, method) (( void (*) (List_1_t624 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Sort()
#define List_1_Sort_m19424(__this, method) (( void (*) (List_1_t624 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19425(__this, ___comparison, method) (( void (*) (List_1_t624 *, Comparison_1_t3422 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.DataSetImpl>::ToArray()
#define List_1_ToArray_m19426(__this, method) (( DataSetImplU5BU5D_t3418* (*) (List_1_t624 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::TrimExcess()
#define List_1_TrimExcess_m19427(__this, method) (( void (*) (List_1_t624 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Capacity()
#define List_1_get_Capacity_m19428(__this, method) (( int32_t (*) (List_1_t624 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19429(__this, ___value, method) (( void (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Count()
#define List_1_get_Count_m19430(__this, method) (( int32_t (*) (List_1_t624 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Item(System.Int32)
#define List_1_get_Item_m19431(__this, ___index, method) (( DataSetImpl_t578 * (*) (List_1_t624 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::set_Item(System.Int32,T)
#define List_1_set_Item_m19432(__this, ___index, ___value, method) (( void (*) (List_1_t624 *, int32_t, DataSetImpl_t578 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
