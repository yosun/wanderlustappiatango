﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t3767;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1365;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4332;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t459;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m25088_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25088(__this, method) (( void (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1__ctor_m25088_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25089_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25089(__this, method) (( bool (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25089_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25090_gshared (Collection_1_t3767 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25090(__this, ___array, ___index, method) (( void (*) (Collection_1_t3767 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25090_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25091_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25091(__this, method) (( Object_t * (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25091_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25092_gshared (Collection_1_t3767 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25092(__this, ___value, method) (( int32_t (*) (Collection_1_t3767 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25092_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25093_gshared (Collection_1_t3767 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25093(__this, ___value, method) (( bool (*) (Collection_1_t3767 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25093_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25094_gshared (Collection_1_t3767 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25094(__this, ___value, method) (( int32_t (*) (Collection_1_t3767 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25094_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25095_gshared (Collection_1_t3767 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25095(__this, ___index, ___value, method) (( void (*) (Collection_1_t3767 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25095_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25096_gshared (Collection_1_t3767 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25096(__this, ___value, method) (( void (*) (Collection_1_t3767 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25096_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25097_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25097(__this, method) (( bool (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25097_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25098_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25098(__this, method) (( Object_t * (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25098_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25099_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25099(__this, method) (( bool (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25100_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25100(__this, method) (( bool (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25100_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25101_gshared (Collection_1_t3767 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25101(__this, ___index, method) (( Object_t * (*) (Collection_1_t3767 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25101_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25102_gshared (Collection_1_t3767 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25102(__this, ___index, ___value, method) (( void (*) (Collection_1_t3767 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25102_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m25103_gshared (Collection_1_t3767 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_Add_m25103(__this, ___item, method) (( void (*) (Collection_1_t3767 *, UILineInfo_t458 , const MethodInfo*))Collection_1_Add_m25103_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m25104_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25104(__this, method) (( void (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_Clear_m25104_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m25105_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25105(__this, method) (( void (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_ClearItems_m25105_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m25106_gshared (Collection_1_t3767 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_Contains_m25106(__this, ___item, method) (( bool (*) (Collection_1_t3767 *, UILineInfo_t458 , const MethodInfo*))Collection_1_Contains_m25106_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25107_gshared (Collection_1_t3767 * __this, UILineInfoU5BU5D_t1365* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25107(__this, ___array, ___index, method) (( void (*) (Collection_1_t3767 *, UILineInfoU5BU5D_t1365*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25107_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25108_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25108(__this, method) (( Object_t* (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_GetEnumerator_m25108_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25109_gshared (Collection_1_t3767 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25109(__this, ___item, method) (( int32_t (*) (Collection_1_t3767 *, UILineInfo_t458 , const MethodInfo*))Collection_1_IndexOf_m25109_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25110_gshared (Collection_1_t3767 * __this, int32_t ___index, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_Insert_m25110(__this, ___index, ___item, method) (( void (*) (Collection_1_t3767 *, int32_t, UILineInfo_t458 , const MethodInfo*))Collection_1_Insert_m25110_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25111_gshared (Collection_1_t3767 * __this, int32_t ___index, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25111(__this, ___index, ___item, method) (( void (*) (Collection_1_t3767 *, int32_t, UILineInfo_t458 , const MethodInfo*))Collection_1_InsertItem_m25111_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m25112_gshared (Collection_1_t3767 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_Remove_m25112(__this, ___item, method) (( bool (*) (Collection_1_t3767 *, UILineInfo_t458 , const MethodInfo*))Collection_1_Remove_m25112_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25113_gshared (Collection_1_t3767 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25113(__this, ___index, method) (( void (*) (Collection_1_t3767 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25113_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25114_gshared (Collection_1_t3767 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25114(__this, ___index, method) (( void (*) (Collection_1_t3767 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25114_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25115_gshared (Collection_1_t3767 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25115(__this, method) (( int32_t (*) (Collection_1_t3767 *, const MethodInfo*))Collection_1_get_Count_m25115_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t458  Collection_1_get_Item_m25116_gshared (Collection_1_t3767 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25116(__this, ___index, method) (( UILineInfo_t458  (*) (Collection_1_t3767 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25116_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25117_gshared (Collection_1_t3767 * __this, int32_t ___index, UILineInfo_t458  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25117(__this, ___index, ___value, method) (( void (*) (Collection_1_t3767 *, int32_t, UILineInfo_t458 , const MethodInfo*))Collection_1_set_Item_m25117_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25118_gshared (Collection_1_t3767 * __this, int32_t ___index, UILineInfo_t458  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25118(__this, ___index, ___item, method) (( void (*) (Collection_1_t3767 *, int32_t, UILineInfo_t458 , const MethodInfo*))Collection_1_SetItem_m25118_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25119_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25119(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25119_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t458  Collection_1_ConvertItem_m25120_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25120(__this /* static, unused */, ___item, method) (( UILineInfo_t458  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25120_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25121_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25121(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25121_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25122_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25122(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25122_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25123_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25123(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25123_gshared)(__this /* static, unused */, ___list, method)
