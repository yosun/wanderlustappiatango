﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>
struct Transform_1_t3559;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m21919_gshared (Transform_1_t3559 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m21919(__this, ___object, ___method, method) (( void (*) (Transform_1_t3559 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m21919_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3550  Transform_1_Invoke_m21920_gshared (Transform_1_t3559 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m21920(__this, ___key, ___value, method) (( KeyValuePair_2_t3550  (*) (Transform_1_t3559 *, int32_t, TrackableResultData_t642 , const MethodInfo*))Transform_1_Invoke_m21920_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m21921_gshared (Transform_1_t3559 * __this, int32_t ___key, TrackableResultData_t642  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m21921(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3559 *, int32_t, TrackableResultData_t642 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m21921_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3550  Transform_1_EndInvoke_m21922_gshared (Transform_1_t3559 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m21922(__this, ___result, method) (( KeyValuePair_2_t3550  (*) (Transform_1_t3559 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m21922_gshared)(__this, ___result, method)
