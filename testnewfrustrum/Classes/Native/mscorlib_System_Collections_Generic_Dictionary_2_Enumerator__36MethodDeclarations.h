﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Enumerator_t3856;
// System.Object
struct Object_t;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t1284;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t1415;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m26305(__this, ___dictionary, method) (( void (*) (Enumerator_t3856 *, Dictionary_2_t1415 *, const MethodInfo*))Enumerator__ctor_m16933_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26306(__this, method) (( Object_t * (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26307(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26308(__this, method) (( Object_t * (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26309(__this, method) (( Object_t * (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::MoveNext()
#define Enumerator_MoveNext_m26310(__this, method) (( bool (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_MoveNext_m16938_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Current()
#define Enumerator_get_Current_m26311(__this, method) (( KeyValuePair_2_t1421  (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_get_Current_m16939_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m26312(__this, method) (( String_t* (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_get_CurrentKey_m16940_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m26313(__this, method) (( GetDelegate_t1284 * (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_get_CurrentValue_m16941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyState()
#define Enumerator_VerifyState_m26314(__this, method) (( void (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_VerifyState_m16942_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m26315(__this, method) (( void (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_VerifyCurrent_m16943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Dispose()
#define Enumerator_Dispose_m26316(__this, method) (( void (*) (Enumerator_t3856 *, const MethodInfo*))Enumerator_Dispose_m16944_gshared)(__this, method)
