﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1392;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1172;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1184;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"
#define Enumerator__ctor_m24461(__this, ___host, method) (( void (*) (Enumerator_t1392 *, Dictionary_2_t1184 *, const MethodInfo*))Enumerator__ctor_m16963_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24462(__this, method) (( Object_t * (*) (Enumerator_t1392 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m24463(__this, method) (( void (*) (Enumerator_t1392 *, const MethodInfo*))Enumerator_Dispose_m16965_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m24464(__this, method) (( bool (*) (Enumerator_t1392 *, const MethodInfo*))Enumerator_MoveNext_m16966_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m24465(__this, method) (( GUIStyle_t1172 * (*) (Enumerator_t1392 *, const MethodInfo*))Enumerator_get_Current_m16967_gshared)(__this, method)
