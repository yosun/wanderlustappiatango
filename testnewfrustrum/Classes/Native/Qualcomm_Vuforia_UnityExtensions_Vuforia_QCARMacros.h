﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARMacros
struct  QCARMacros_t750 
{
	union
	{
		struct
		{
		};
		uint8_t QCARMacros_t750__padding[1];
	};
};
