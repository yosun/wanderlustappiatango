﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>
struct Enumerator_t843;
// System.Object
struct Object_t;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t776;
// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct List_1_t704;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20910(__this, ___l, method) (( void (*) (Enumerator_t843 *, List_1_t704 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20911(__this, method) (( Object_t * (*) (Enumerator_t843 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::Dispose()
#define Enumerator_Dispose_m20912(__this, method) (( void (*) (Enumerator_t843 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::VerifyState()
#define Enumerator_VerifyState_m20913(__this, method) (( void (*) (Enumerator_t843 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4505(__this, method) (( bool (*) (Enumerator_t843 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::get_Current()
#define Enumerator_get_Current_m4504(__this, method) (( Object_t * (*) (Enumerator_t843 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
