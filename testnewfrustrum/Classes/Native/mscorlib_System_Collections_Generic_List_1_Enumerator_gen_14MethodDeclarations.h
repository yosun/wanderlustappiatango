﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
struct Enumerator_t862;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t98;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t781;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21663(__this, ___l, method) (( void (*) (Enumerator_t862 *, List_1_t781 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21664(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m21665(__this, method) (( void (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::VerifyState()
#define Enumerator_VerifyState_m21666(__this, method) (( void (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m4541(__this, method) (( bool (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m4540(__this, method) (( Object_t * (*) (Enumerator_t862 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
