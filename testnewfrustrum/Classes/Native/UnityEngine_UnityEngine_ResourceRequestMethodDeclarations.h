﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t1196;
// UnityEngine.Object
struct Object_t123;
struct Object_t123_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m6129 (ResourceRequest_t1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t123 * ResourceRequest_get_asset_m6130 (ResourceRequest_t1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
