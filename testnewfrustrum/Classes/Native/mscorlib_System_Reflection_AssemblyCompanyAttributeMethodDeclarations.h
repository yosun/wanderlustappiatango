﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t482;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
extern "C" void AssemblyCompanyAttribute__ctor_m2430 (AssemblyCompanyAttribute_t482 * __this, String_t* ___company, const MethodInfo* method) IL2CPP_METHOD_ATTR;
