﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t1061;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t1991  : public MulticastDelegate_t307
{
};
