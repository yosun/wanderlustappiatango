﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.Decoder
struct Decoder_t2181;
// System.Text.DecoderFallback
struct DecoderFallback_t2446;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2447;
// System.Byte[]
struct ByteU5BU5D_t616;
// System.Char[]
struct CharU5BU5D_t110;

// System.Void System.Text.Decoder::.ctor()
extern "C" void Decoder__ctor_m12883 (Decoder_t2181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
extern "C" void Decoder_set_Fallback_m12884 (Decoder_t2181 * __this, DecoderFallback_t2446 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
extern "C" DecoderFallbackBuffer_t2447 * Decoder_get_FallbackBuffer_m12885 (Decoder_t2181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
