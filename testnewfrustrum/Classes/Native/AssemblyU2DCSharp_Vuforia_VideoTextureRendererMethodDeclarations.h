﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t83;

// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C" void VideoTextureRenderer__ctor_m222 (VideoTextureRenderer_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
