﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3276;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m17232_gshared (DefaultComparer_t3276 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17232(__this, method) (( void (*) (DefaultComparer_t3276 *, const MethodInfo*))DefaultComparer__ctor_m17232_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17233_gshared (DefaultComparer_t3276 * __this, UIVertex_t313  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17233(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3276 *, UIVertex_t313 , const MethodInfo*))DefaultComparer_GetHashCode_m17233_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17234_gshared (DefaultComparer_t3276 * __this, UIVertex_t313  ___x, UIVertex_t313  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17234(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3276 *, UIVertex_t313 , UIVertex_t313 , const MethodInfo*))DefaultComparer_Equals_m17234_gshared)(__this, ___x, ___y, method)
