﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t1050;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23973_gshared (DOGetter_1_t1050 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23973(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1050 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23973_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C" Color_t90  DOGetter_1_Invoke_m23974_gshared (DOGetter_1_t1050 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23974(__this, method) (( Color_t90  (*) (DOGetter_1_t1050 *, const MethodInfo*))DOGetter_1_Invoke_m23974_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23975_gshared (DOGetter_1_t1050 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23975(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1050 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23975_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" Color_t90  DOGetter_1_EndInvoke_m23976_gshared (DOGetter_1_t1050 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23976(__this, ___result, method) (( Color_t90  (*) (DOGetter_1_t1050 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23976_gshared)(__this, ___result, method)
