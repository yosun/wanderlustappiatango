﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Object>
struct  Predicate_1_t3121  : public MulticastDelegate_t307
{
};
