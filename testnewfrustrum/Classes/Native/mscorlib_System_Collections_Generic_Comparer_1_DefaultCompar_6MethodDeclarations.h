﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t3772;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25140_gshared (DefaultComparer_t3772 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25140(__this, method) (( void (*) (DefaultComparer_t3772 *, const MethodInfo*))DefaultComparer__ctor_m25140_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m25141_gshared (DefaultComparer_t3772 * __this, UILineInfo_t458  ___x, UILineInfo_t458  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m25141(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3772 *, UILineInfo_t458 , UILineInfo_t458 , const MethodInfo*))DefaultComparer_Compare_m25141_gshared)(__this, ___x, ___y, method)
