﻿#pragma once
#include <stdint.h>
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t636;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.PlayModeEditorUtility
struct  PlayModeEditorUtility_t637  : public Object_t
{
};
struct PlayModeEditorUtility_t637_StaticFields{
	// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::sInstance
	Object_t * ___sInstance_0;
};
