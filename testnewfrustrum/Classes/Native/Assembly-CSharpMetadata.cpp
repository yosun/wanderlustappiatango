﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
extern TypeInfo ARVRModes_t6_il2cpp_TypeInfo;
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
extern TypeInfo AnimateTexture_t9_il2cpp_TypeInfo;
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
extern TypeInfo Gyro2_t12_il2cpp_TypeInfo;
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
extern TypeInfo GyroCameraYo_t14_il2cpp_TypeInfo;
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
extern TypeInfo Input2_t16_il2cpp_TypeInfo;
// Input2
#include "AssemblyU2DCSharp_Input2.h"
extern TypeInfo Mathf2_t17_il2cpp_TypeInfo;
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
extern TypeInfo Raycaster_t18_il2cpp_TypeInfo;
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
extern TypeInfo SetRenderQueue_t20_il2cpp_TypeInfo;
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
extern TypeInfo SetRenderQueueChildren_t21_il2cpp_TypeInfo;
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
extern TypeInfo SetRotationManually_t24_il2cpp_TypeInfo;
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
extern TypeInfo WorldLoop_t25_il2cpp_TypeInfo;
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
extern TypeInfo WorldNav_t26_il2cpp_TypeInfo;
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
extern TypeInfo Basics_t27_il2cpp_TypeInfo;
// Basics
#include "AssemblyU2DCSharp_Basics.h"
extern TypeInfo Sequences_t28_il2cpp_TypeInfo;
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
extern TypeInfo OrbitCamera_t29_il2cpp_TypeInfo;
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
extern TypeInfo TestParticles_t30_il2cpp_TypeInfo;
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
extern TypeInfo BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo;
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
extern TypeInfo CloudRecoBehaviour_t33_il2cpp_TypeInfo;
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
extern TypeInfo CylinderTargetBehaviour_t35_il2cpp_TypeInfo;
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
extern TypeInfo DataSetLoadBehaviour_t37_il2cpp_TypeInfo;
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
extern TypeInfo DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo;
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
extern TypeInfo DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo;
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
extern TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo;
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
extern TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo;
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
extern TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo;
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
extern TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo;
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
extern TypeInfo KeepAliveBehaviour_t55_il2cpp_TypeInfo;
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
extern TypeInfo MarkerBehaviour_t57_il2cpp_TypeInfo;
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
extern TypeInfo MaskOutBehaviour_t59_il2cpp_TypeInfo;
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
extern TypeInfo MultiTargetBehaviour_t61_il2cpp_TypeInfo;
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
extern TypeInfo ObjectTargetBehaviour_t63_il2cpp_TypeInfo;
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
extern TypeInfo PropBehaviour_t41_il2cpp_TypeInfo;
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
extern TypeInfo QCARBehaviour_t66_il2cpp_TypeInfo;
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
extern TypeInfo ReconstructionBehaviour_t40_il2cpp_TypeInfo;
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
extern TypeInfo ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo;
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
extern TypeInfo SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo;
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
extern TypeInfo SurfaceBehaviour_t42_il2cpp_TypeInfo;
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
extern TypeInfo TextRecoBehaviour_t74_il2cpp_TypeInfo;
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
extern TypeInfo TurnOffBehaviour_t76_il2cpp_TypeInfo;
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
extern TypeInfo TurnOffWordBehaviour_t78_il2cpp_TypeInfo;
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
extern TypeInfo UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
extern TypeInfo VideoBackgroundBehaviour_t81_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
extern TypeInfo VideoTextureRenderer_t83_il2cpp_TypeInfo;
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
extern TypeInfo VirtualButtonBehaviour_t85_il2cpp_TypeInfo;
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
extern TypeInfo WebCamBehaviour_t87_il2cpp_TypeInfo;
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
extern TypeInfo WireframeBehaviour_t89_il2cpp_TypeInfo;
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
extern TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo;
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
extern TypeInfo WordBehaviour_t92_il2cpp_TypeInfo;
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[55] = 
{
	&U3CModuleU3E_t0_il2cpp_TypeInfo,
	&TheCurrentMode_t1_il2cpp_TypeInfo,
	&ARVRModes_t6_il2cpp_TypeInfo,
	&AnimateTexture_t9_il2cpp_TypeInfo,
	&Gyro2_t12_il2cpp_TypeInfo,
	&GyroCameraYo_t14_il2cpp_TypeInfo,
	&Input2_t16_il2cpp_TypeInfo,
	&Mathf2_t17_il2cpp_TypeInfo,
	&Raycaster_t18_il2cpp_TypeInfo,
	&SetRenderQueue_t20_il2cpp_TypeInfo,
	&SetRenderQueueChildren_t21_il2cpp_TypeInfo,
	&SetRotationManually_t24_il2cpp_TypeInfo,
	&WorldLoop_t25_il2cpp_TypeInfo,
	&WorldNav_t26_il2cpp_TypeInfo,
	&Basics_t27_il2cpp_TypeInfo,
	&Sequences_t28_il2cpp_TypeInfo,
	&OrbitCamera_t29_il2cpp_TypeInfo,
	&TestParticles_t30_il2cpp_TypeInfo,
	&BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo,
	&CloudRecoBehaviour_t33_il2cpp_TypeInfo,
	&CylinderTargetBehaviour_t35_il2cpp_TypeInfo,
	&DataSetLoadBehaviour_t37_il2cpp_TypeInfo,
	&DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo,
	&DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo,
	&GLErrorHandler_t46_il2cpp_TypeInfo,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo,
	&ImageTargetBehaviour_t49_il2cpp_TypeInfo,
	&AndroidUnityPlayer_t51_il2cpp_TypeInfo,
	&ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo,
	&IOSUnityPlayer_t53_il2cpp_TypeInfo,
	&VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo,
	&KeepAliveBehaviour_t55_il2cpp_TypeInfo,
	&MarkerBehaviour_t57_il2cpp_TypeInfo,
	&MaskOutBehaviour_t59_il2cpp_TypeInfo,
	&MultiTargetBehaviour_t61_il2cpp_TypeInfo,
	&ObjectTargetBehaviour_t63_il2cpp_TypeInfo,
	&PropBehaviour_t41_il2cpp_TypeInfo,
	&QCARBehaviour_t66_il2cpp_TypeInfo,
	&ReconstructionBehaviour_t40_il2cpp_TypeInfo,
	&ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo,
	&SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo,
	&SurfaceBehaviour_t42_il2cpp_TypeInfo,
	&TextRecoBehaviour_t74_il2cpp_TypeInfo,
	&TurnOffBehaviour_t76_il2cpp_TypeInfo,
	&TurnOffWordBehaviour_t78_il2cpp_TypeInfo,
	&UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo,
	&VideoBackgroundBehaviour_t81_il2cpp_TypeInfo,
	&VideoTextureRenderer_t83_il2cpp_TypeInfo,
	&VirtualButtonBehaviour_t85_il2cpp_TypeInfo,
	&WebCamBehaviour_t87_il2cpp_TypeInfo,
	&WireframeBehaviour_t89_il2cpp_TypeInfo,
	&WireframeTrackableEventHandler_t91_il2cpp_TypeInfo,
	&WordBehaviour_t92_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", NULL, NULL, NULL, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[10];
static const char* s_StringTable[109] = 
{
	"value__",
	"AR",
	"VR",
	"goMaskThese",
	"goARModeOnlyStuff",
	"camAR",
	"camVR",
	"goVRSet",
	"matTouchMeLipstick",
	"goUI_PanelNav",
	"goDisableForEntire",
	"goEnableForEntire",
	"goDisableForDoor",
	"goEnableForDoor",
	"tcm",
	"uvOffset",
	"uvAnimationRate",
	"renderer",
	"transform",
	"gyroEnabled",
	"camBase",
	"calibration",
	"refRot",
	"baseOrientation",
	"goUI_Directions_FiveFingers",
	"goUI_Directions_Swipe",
	"goUI_SettingsButtonToggle",
	"countdown",
	"parentTransform",
	"rotFix",
	"rotateAround",
	"offScreen",
	"oldAngle",
	"lastPos",
	"useMouse",
	"swipeThreshhold",
	"swipeThreshX",
	"swipeThreshY",
	"FarFarAway",
	"m_queues",
	"sliderX",
	"sliderY",
	"sliderZ",
	"rotationvalue",
	"cam",
	"raycaster",
	"coeff",
	"mainCharacter",
	"goMainCharacter",
	"camCharacter",
	"walking",
	"dirwalking",
	"goUI_Directions_TextWalkButton",
	"cubeA",
	"cubeB",
	"target",
	"TargetLookAt",
	"Distance",
	"DistanceMin",
	"DistanceMax",
	"startingDistance",
	"desiredDistance",
	"mouseX",
	"mouseY",
	"X_MouseSensitivity",
	"Y_MouseSensitivity",
	"MouseWheelSensitivity",
	"Y_MinLimit",
	"Y_MaxLimit",
	"DistanceSmooth",
	"velocityDistance",
	"desiredPosition",
	"X_Smooth",
	"Y_Smooth",
	"velX",
	"velY",
	"velZ",
	"position",
	"m_PrefabListFire",
	"m_PrefabListWind",
	"m_PrefabListWater",
	"m_PrefabListEarth",
	"m_PrefabListIce",
	"m_PrefabListThunder",
	"m_PrefabListLight",
	"m_PrefabListDarkness",
	"m_CurrentElementIndex",
	"m_CurrentParticleIndex",
	"m_ElementName",
	"m_ParticleName",
	"m_CurrentElementList",
	"m_CurrentParticle",
	"WINDOW_TITLE",
	"mErrorText",
	"mErrorOccurred",
	"mReconstructionBehaviour",
	"PropTemplate",
	"SurfaceTemplate",
	"mTrackableBehaviour",
	"arvrmodes",
	"NUM_FRAMES_TO_QUERY_ORIENTATION",
	"JAVA_ORIENTATION_CHECK_FRM_INTERVAL",
	"mScreenOrientation",
	"mJavaScreenOrientation",
	"mFramesSinceLastOrientationReset",
	"mFramesSinceLastJavaOrientationCheck",
	"mLineMaterial",
	"ShowLines",
	"LineColor",
};
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
static const Il2CppFieldDefinition s_FieldTable[116] = 
{
	{ 0, 88, offsetof(TheCurrentMode_t1, ___value___1) + sizeof(Object_t), 0 } ,
	{ 1, 89, 0, 0 } ,
	{ 2, 89, 0, 0 } ,
	{ 3, 97, offsetof(ARVRModes_t6, ___goMaskThese_2), 0 } ,
	{ 4, 97, offsetof(ARVRModes_t6, ___goARModeOnlyStuff_3), 0 } ,
	{ 5, 98, offsetof(ARVRModes_t6, ___camAR_4), 0 } ,
	{ 6, 98, offsetof(ARVRModes_t6, ___camVR_5), 0 } ,
	{ 7, 97, offsetof(ARVRModes_t6, ___goVRSet_6), 0 } ,
	{ 8, 99, offsetof(ARVRModes_t6, ___matTouchMeLipstick_7), 0 } ,
	{ 9, 97, offsetof(ARVRModes_t6, ___goUI_PanelNav_8), 0 } ,
	{ 10, 100, offsetof(ARVRModes_t6, ___goDisableForEntire_9), 0 } ,
	{ 11, 100, offsetof(ARVRModes_t6, ___goEnableForEntire_10), 0 } ,
	{ 12, 100, offsetof(ARVRModes_t6, ___goDisableForDoor_11), 0 } ,
	{ 13, 100, offsetof(ARVRModes_t6, ___goEnableForDoor_12), 0 } ,
	{ 14, 101, offsetof(ARVRModes_t6_StaticFields, ___tcm_13), 0 } ,
	{ 15, 104, offsetof(AnimateTexture_t9, ___uvOffset_2), 0 } ,
	{ 16, 104, offsetof(AnimateTexture_t9, ___uvAnimationRate_3), 0 } ,
	{ 17, 105, offsetof(AnimateTexture_t9, ___renderer_4), 0 } ,
	{ 18, 109, offsetof(Gyro2_t12_StaticFields, ___transform_2), 0 } ,
	{ 19, 110, offsetof(Gyro2_t12_StaticFields, ___gyroEnabled_3), 0 } ,
	{ 20, 111, offsetof(Gyro2_t12_StaticFields, ___camBase_4), 0 } ,
	{ 21, 111, offsetof(Gyro2_t12_StaticFields, ___calibration_5), 0 } ,
	{ 22, 111, offsetof(Gyro2_t12_StaticFields, ___refRot_6), 0 } ,
	{ 23, 111, offsetof(Gyro2_t12_StaticFields, ___baseOrientation_7), 0 } ,
	{ 24, 97, offsetof(Gyro2_t12, ___goUI_Directions_FiveFingers_8), 0 } ,
	{ 25, 97, offsetof(Gyro2_t12, ___goUI_Directions_Swipe_9), 0 } ,
	{ 26, 97, offsetof(Gyro2_t12, ___goUI_SettingsButtonToggle_10), 0 } ,
	{ 27, 112, offsetof(Gyro2_t12_StaticFields, ___countdown_11), 0 } ,
	{ 28, 114, offsetof(GyroCameraYo_t14, ___parentTransform_2), 2 } ,
	{ 18, 114, offsetof(GyroCameraYo_t14, ___transform_3), 0 } ,
	{ 29, 115, offsetof(GyroCameraYo_t14, ___rotFix_4), 0 } ,
	{ 30, 116, offsetof(GyroCameraYo_t14, ___rotateAround_5), 0 } ,
	{ 31, 120, offsetof(Input2_t16_StaticFields, ___offScreen_2), 0 } ,
	{ 32, 112, offsetof(Input2_t16_StaticFields, ___oldAngle_3), 0 } ,
	{ 33, 121, offsetof(Input2_t16_StaticFields, ___lastPos_4), 0 } ,
	{ 34, 110, offsetof(Input2_t16_StaticFields, ___useMouse_5), 0 } ,
	{ 35, 112, offsetof(Input2_t16_StaticFields, ___swipeThreshhold_6), 0 } ,
	{ 36, 112, offsetof(Input2_t16_StaticFields, ___swipeThreshX_7), 0 } ,
	{ 37, 112, offsetof(Input2_t16_StaticFields, ___swipeThreshY_8), 0 } ,
	{ 38, 126, offsetof(Mathf2_t17_StaticFields, ___FarFarAway_0), 0 } ,
	{ 39, 129, offsetof(SetRenderQueue_t20, ___m_queues_2), 3 } ,
	{ 39, 129, offsetof(SetRenderQueueChildren_t21, ___m_queues_2), 4 } ,
	{ 40, 134, offsetof(SetRotationManually_t24, ___sliderX_2), 0 } ,
	{ 41, 134, offsetof(SetRotationManually_t24, ___sliderY_3), 0 } ,
	{ 42, 134, offsetof(SetRotationManually_t24, ___sliderZ_4), 0 } ,
	{ 43, 135, offsetof(SetRotationManually_t24, ___rotationvalue_5), 0 } ,
	{ 44, 98, offsetof(WorldLoop_t25, ___cam_2), 0 } ,
	{ 45, 138, offsetof(WorldLoop_t25, ___raycaster_3), 0 } ,
	{ 46, 112, offsetof(WorldNav_t26_StaticFields, ___coeff_2), 0 } ,
	{ 47, 97, offsetof(WorldNav_t26, ___mainCharacter_3), 0 } ,
	{ 48, 141, offsetof(WorldNav_t26_StaticFields, ___goMainCharacter_4), 0 } ,
	{ 49, 142, offsetof(WorldNav_t26_StaticFields, ___camCharacter_5), 0 } ,
	{ 50, 143, offsetof(WorldNav_t26_StaticFields, ___walking_6), 0 } ,
	{ 51, 144, offsetof(WorldNav_t26, ___dirwalking_7), 0 } ,
	{ 52, 97, offsetof(WorldNav_t26, ___goUI_Directions_TextWalkButton_8), 0 } ,
	{ 53, 146, offsetof(Basics_t27, ___cubeA_2), 0 } ,
	{ 54, 146, offsetof(Basics_t27, ___cubeB_3), 0 } ,
	{ 55, 146, offsetof(Sequences_t28, ___target_2), 0 } ,
	{ 56, 146, offsetof(OrbitCamera_t29, ___TargetLookAt_2), 0 } ,
	{ 57, 151, offsetof(OrbitCamera_t29, ___Distance_3), 0 } ,
	{ 58, 151, offsetof(OrbitCamera_t29, ___DistanceMin_4), 0 } ,
	{ 59, 151, offsetof(OrbitCamera_t29, ___DistanceMax_5), 0 } ,
	{ 60, 152, offsetof(OrbitCamera_t29, ___startingDistance_6), 0 } ,
	{ 61, 152, offsetof(OrbitCamera_t29, ___desiredDistance_7), 0 } ,
	{ 62, 152, offsetof(OrbitCamera_t29, ___mouseX_8), 0 } ,
	{ 63, 152, offsetof(OrbitCamera_t29, ___mouseY_9), 0 } ,
	{ 64, 151, offsetof(OrbitCamera_t29, ___X_MouseSensitivity_10), 0 } ,
	{ 65, 151, offsetof(OrbitCamera_t29, ___Y_MouseSensitivity_11), 0 } ,
	{ 66, 151, offsetof(OrbitCamera_t29, ___MouseWheelSensitivity_12), 0 } ,
	{ 67, 151, offsetof(OrbitCamera_t29, ___Y_MinLimit_13), 0 } ,
	{ 68, 151, offsetof(OrbitCamera_t29, ___Y_MaxLimit_14), 0 } ,
	{ 69, 151, offsetof(OrbitCamera_t29, ___DistanceSmooth_15), 0 } ,
	{ 70, 152, offsetof(OrbitCamera_t29, ___velocityDistance_16), 0 } ,
	{ 71, 116, offsetof(OrbitCamera_t29, ___desiredPosition_17), 0 } ,
	{ 72, 151, offsetof(OrbitCamera_t29, ___X_Smooth_18), 0 } ,
	{ 73, 151, offsetof(OrbitCamera_t29, ___Y_Smooth_19), 0 } ,
	{ 74, 152, offsetof(OrbitCamera_t29, ___velX_20), 0 } ,
	{ 75, 152, offsetof(OrbitCamera_t29, ___velY_21), 0 } ,
	{ 76, 152, offsetof(OrbitCamera_t29, ___velZ_22), 0 } ,
	{ 77, 116, offsetof(OrbitCamera_t29, ___position_23), 0 } ,
	{ 78, 100, offsetof(TestParticles_t30, ___m_PrefabListFire_2), 0 } ,
	{ 79, 100, offsetof(TestParticles_t30, ___m_PrefabListWind_3), 0 } ,
	{ 80, 100, offsetof(TestParticles_t30, ___m_PrefabListWater_4), 0 } ,
	{ 81, 100, offsetof(TestParticles_t30, ___m_PrefabListEarth_5), 0 } ,
	{ 82, 100, offsetof(TestParticles_t30, ___m_PrefabListIce_6), 0 } ,
	{ 83, 100, offsetof(TestParticles_t30, ___m_PrefabListThunder_7), 0 } ,
	{ 84, 100, offsetof(TestParticles_t30, ___m_PrefabListLight_8), 0 } ,
	{ 85, 100, offsetof(TestParticles_t30, ___m_PrefabListDarkness_9), 0 } ,
	{ 86, 144, offsetof(TestParticles_t30, ___m_CurrentElementIndex_10), 0 } ,
	{ 87, 144, offsetof(TestParticles_t30, ___m_CurrentParticleIndex_11), 0 } ,
	{ 88, 155, offsetof(TestParticles_t30, ___m_ElementName_12), 0 } ,
	{ 89, 155, offsetof(TestParticles_t30, ___m_ParticleName_13), 0 } ,
	{ 90, 156, offsetof(TestParticles_t30, ___m_CurrentElementList_14), 0 } ,
	{ 91, 157, offsetof(TestParticles_t30, ___m_CurrentParticle_15), 0 } ,
	{ 92, 175, 0, 0 } ,
	{ 93, 155, offsetof(DefaultInitializationErrorHandler_t39, ___mErrorText_3), 0 } ,
	{ 94, 176, offsetof(DefaultInitializationErrorHandler_t39, ___mErrorOccurred_4), 0 } ,
	{ 95, 179, offsetof(DefaultSmartTerrainEventHandler_t43, ___mReconstructionBehaviour_2), 0 } ,
	{ 96, 180, offsetof(DefaultSmartTerrainEventHandler_t43, ___PropTemplate_3), 0 } ,
	{ 97, 181, offsetof(DefaultSmartTerrainEventHandler_t43, ___SurfaceTemplate_4), 0 } ,
	{ 98, 185, offsetof(DefaultTrackableEventHandler_t45, ___mTrackableBehaviour_2), 0 } ,
	{ 99, 186, offsetof(DefaultTrackableEventHandler_t45, ___arvrmodes_3), 0 } ,
	{ 92, 175, 0, 0 } ,
	{ 93, 190, offsetof(GLErrorHandler_t46_StaticFields, ___mErrorText_3), 0 } ,
	{ 94, 143, offsetof(GLErrorHandler_t46_StaticFields, ___mErrorOccurred_4), 0 } ,
	{ 100, 198, 0, 0 } ,
	{ 101, 198, 0, 0 } ,
	{ 102, 199, offsetof(AndroidUnityPlayer_t51, ___mScreenOrientation_2), 0 } ,
	{ 103, 199, offsetof(AndroidUnityPlayer_t51, ___mJavaScreenOrientation_3), 0 } ,
	{ 104, 144, offsetof(AndroidUnityPlayer_t51, ___mFramesSinceLastOrientationReset_4), 0 } ,
	{ 105, 144, offsetof(AndroidUnityPlayer_t51, ___mFramesSinceLastJavaOrientationCheck_5), 0 } ,
	{ 102, 199, offsetof(IOSUnityPlayer_t53, ___mScreenOrientation_0), 0 } ,
	{ 106, 263, offsetof(WireframeBehaviour_t89, ___mLineMaterial_2), 0 } ,
	{ 107, 264, offsetof(WireframeBehaviour_t89, ___ShowLines_3), 0 } ,
	{ 108, 265, offsetof(WireframeBehaviour_t89, ___LineColor_4), 0 } ,
	{ 98, 185, offsetof(WireframeTrackableEventHandler_t91, ___mTrackableBehaviour_2), 0 } ,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
static const Il2CppFieldDefaultValue s_DefaultValues[6] = 
{
	{ 1, 21, 0 },
	{ 2, 21, 4 },
	{ 94, 9, 8 },
	{ 102, 9, 37 },
	{ 105, 21, 53 },
	{ 106, 21, 57 },
};
static const uint8_t s_DefaultValueDataTable[61] = 
{
	0x00,
	0x00,
	0x00,
	0x00,
	0x01,
	0x00,
	0x00,
	0x00,
	0x19,
	0x00,
	0x00,
	0x00,
	0x51,
	0x43,
	0x41,
	0x52,
	0x20,
	0x49,
	0x6E,
	0x69,
	0x74,
	0x69,
	0x61,
	0x6C,
	0x69,
	0x7A,
	0x61,
	0x74,
	0x69,
	0x6F,
	0x6E,
	0x20,
	0x45,
	0x72,
	0x72,
	0x6F,
	0x72,
	0x0C,
	0x00,
	0x00,
	0x00,
	0x53,
	0x61,
	0x6D,
	0x70,
	0x6C,
	0x65,
	0x20,
	0x45,
	0x72,
	0x72,
	0x6F,
	0x72,
	0x19,
	0x00,
	0x00,
	0x00,
	0x3C,
	0x00,
	0x00,
	0x00,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	54,
	NULL,
	s_StringTable,
	109,
	s_FieldTable,
	116,
	s_DefaultValues,
	6,
	s_DefaultValueDataTable,
	61,
	10,
	NULL,
	g_AssemblyU2DCSharp_Assembly_AttributeGenerators,
};
