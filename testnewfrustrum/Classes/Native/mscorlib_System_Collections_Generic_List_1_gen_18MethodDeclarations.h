﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t566;
// System.Object
struct Object_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t178;
// System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>
struct IEnumerable_1_t4130;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>
struct IEnumerator_1_t4131;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>
struct ICollection_1_t4132;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackableEventHandler>
struct ReadOnlyCollection_1_t3359;
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3357;
// System.Predicate`1<Vuforia.ITrackableEventHandler>
struct Predicate_1_t3360;
// System.Comparison`1<Vuforia.ITrackableEventHandler>
struct Comparison_1_t3361;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4300(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18303(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m18304(__this, ___capacity, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::.cctor()
#define List_1__cctor_m18305(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18306(__this, method) (( Object_t* (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18307(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t566 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18308(__this, method) (( Object_t * (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18309(__this, ___item, method) (( int32_t (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18310(__this, ___item, method) (( bool (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18311(__this, ___item, method) (( int32_t (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18312(__this, ___index, ___item, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18313(__this, ___item, method) (( void (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18314(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18315(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18316(__this, method) (( Object_t * (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18317(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18318(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18319(__this, ___index, method) (( Object_t * (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18320(__this, ___index, ___value, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Add(T)
#define List_1_Add_m18321(__this, ___item, method) (( void (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18322(__this, ___newCount, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18323(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18324(__this, ___enumerable, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18325(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m18326(__this, method) (( ReadOnlyCollection_1_t3359 * (*) (List_1_t566 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Clear()
#define List_1_Clear_m18327(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Contains(T)
#define List_1_Contains_m18328(__this, ___item, method) (( bool (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18329(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t566 *, ITrackableEventHandlerU5BU5D_t3357*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m18330(__this, ___match, method) (( Object_t * (*) (List_1_t566 *, Predicate_1_t3360 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18331(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3360 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18332(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t566 *, int32_t, int32_t, Predicate_1_t3360 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4296(__this, method) (( Enumerator_t793  (*) (List_1_t566 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::IndexOf(T)
#define List_1_IndexOf_m18333(__this, ___item, method) (( int32_t (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18334(__this, ___start, ___delta, method) (( void (*) (List_1_t566 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18335(__this, ___index, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m18336(__this, ___index, ___item, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18337(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Remove(T)
#define List_1_Remove_m18338(__this, ___item, method) (( bool (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18339(__this, ___match, method) (( int32_t (*) (List_1_t566 *, Predicate_1_t3360 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18340(__this, ___index, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Reverse()
#define List_1_Reverse_m18341(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Sort()
#define List_1_Sort_m18342(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18343(__this, ___comparison, method) (( void (*) (List_1_t566 *, Comparison_1_t3361 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::ToArray()
#define List_1_ToArray_m18344(__this, method) (( ITrackableEventHandlerU5BU5D_t3357* (*) (List_1_t566 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::TrimExcess()
#define List_1_TrimExcess_m18345(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::get_Capacity()
#define List_1_get_Capacity_m18346(__this, method) (( int32_t (*) (List_1_t566 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18347(__this, ___value, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::get_Count()
#define List_1_get_Count_m18348(__this, method) (( int32_t (*) (List_1_t566 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m18349(__this, ___index, method) (( Object_t * (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m18350(__this, ___index, ___value, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
