﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t448;
// UnityEngine.UI.Graphic
struct Graphic_t278;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t4097;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3286;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3288;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t281;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2136(__this, method) (( void (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1__ctor_m16490_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17536(__this, method) (( Object_t * (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m17537(__this, ___item, method) (( void (*) (IndexedSet_1_t448 *, Graphic_t278 *, const MethodInfo*))IndexedSet_1_Add_m16494_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m17538(__this, ___item, method) (( bool (*) (IndexedSet_1_t448 *, Graphic_t278 *, const MethodInfo*))IndexedSet_1_Remove_m16496_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17539(__this, method) (( Object_t* (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m16498_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m17540(__this, method) (( void (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1_Clear_m16500_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m17541(__this, ___item, method) (( bool (*) (IndexedSet_1_t448 *, Graphic_t278 *, const MethodInfo*))IndexedSet_1_Contains_m16502_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17542(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t448 *, GraphicU5BU5D_t3286*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m16504_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m17543(__this, method) (( int32_t (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1_get_Count_m16506_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17544(__this, method) (( bool (*) (IndexedSet_1_t448 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m16508_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17545(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t448 *, Graphic_t278 *, const MethodInfo*))IndexedSet_1_IndexOf_m16510_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17546(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t448 *, int32_t, Graphic_t278 *, const MethodInfo*))IndexedSet_1_Insert_m16512_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17547(__this, ___index, method) (( void (*) (IndexedSet_1_t448 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m16514_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m17548(__this, ___index, method) (( Graphic_t278 * (*) (IndexedSet_1_t448 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m16516_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17549(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t448 *, int32_t, Graphic_t278 *, const MethodInfo*))IndexedSet_1_set_Item_m16518_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m17550(__this, ___match, method) (( void (*) (IndexedSet_1_t448 *, Predicate_1_t3288 *, const MethodInfo*))IndexedSet_1_RemoveAll_m16519_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m17551(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t448 *, Comparison_1_t281 *, const MethodInfo*))IndexedSet_1_Sort_m16520_gshared)(__this, ___sortLayoutFunction, method)
