﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Font>
struct Action_1_t438;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t266;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10MethodDeclarations.h"
#define Action_1__ctor_m2078(__this, ___object, ___method, method) (( void (*) (Action_1_t438 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14968_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m17081(__this, ___obj, method) (( void (*) (Action_1_t438 *, Font_t266 *, const MethodInfo*))Action_1_Invoke_m14970_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m17082(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t438 *, Font_t266 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14972_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m17083(__this, ___result, method) (( void (*) (Action_1_t438 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14974_gshared)(__this, ___result, method)
