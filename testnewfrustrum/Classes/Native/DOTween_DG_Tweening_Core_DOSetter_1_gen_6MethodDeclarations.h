﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t1039;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23683_gshared (DOSetter_1_t1039 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23683(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1039 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23683_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23684_gshared (DOSetter_1_t1039 * __this, uint64_t ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23684(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1039 *, uint64_t, const MethodInfo*))DOSetter_1_Invoke_m23684_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23685_gshared (DOSetter_1_t1039 * __this, uint64_t ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23685(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1039 *, uint64_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23685_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23686_gshared (DOSetter_1_t1039 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23686(__this, ___result, method) (( void (*) (DOSetter_1_t1039 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23686_gshared)(__this, ___result, method)
