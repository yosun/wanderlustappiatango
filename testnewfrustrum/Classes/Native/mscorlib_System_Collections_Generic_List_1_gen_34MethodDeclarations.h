﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t716;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t782;
// System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
struct IEnumerator_1_t888;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>
struct ICollection_1_t4204;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TrackableBehaviour>
struct ReadOnlyCollection_1_t3510;
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t818;
// System.Predicate`1<Vuforia.TrackableBehaviour>
struct Predicate_1_t3511;
// System.Comparison`1<Vuforia.TrackableBehaviour>
struct Comparison_1_t3513;
// System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_48.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4502(__this, method) (( void (*) (List_1_t716 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20918(__this, ___collection, method) (( void (*) (List_1_t716 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m20919(__this, ___capacity, method) (( void (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.cctor()
#define List_1__cctor_m20920(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20921(__this, method) (( Object_t* (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20922(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t716 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20923(__this, method) (( Object_t * (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20924(__this, ___item, method) (( int32_t (*) (List_1_t716 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20925(__this, ___item, method) (( bool (*) (List_1_t716 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20926(__this, ___item, method) (( int32_t (*) (List_1_t716 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20927(__this, ___index, ___item, method) (( void (*) (List_1_t716 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20928(__this, ___item, method) (( void (*) (List_1_t716 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20929(__this, method) (( bool (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20930(__this, method) (( bool (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20931(__this, method) (( Object_t * (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20932(__this, method) (( bool (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20933(__this, method) (( bool (*) (List_1_t716 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20934(__this, ___index, method) (( Object_t * (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20935(__this, ___index, ___value, method) (( void (*) (List_1_t716 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Add(T)
#define List_1_Add_m20936(__this, ___item, method) (( void (*) (List_1_t716 *, TrackableBehaviour_t44 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20937(__this, ___newCount, method) (( void (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20938(__this, ___collection, method) (( void (*) (List_1_t716 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20939(__this, ___enumerable, method) (( void (*) (List_1_t716 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20940(__this, ___collection, method) (( void (*) (List_1_t716 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m20941(__this, method) (( ReadOnlyCollection_1_t3510 * (*) (List_1_t716 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Clear()
#define List_1_Clear_m20942(__this, method) (( void (*) (List_1_t716 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Contains(T)
#define List_1_Contains_m20943(__this, ___item, method) (( bool (*) (List_1_t716 *, TrackableBehaviour_t44 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20944(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t716 *, TrackableBehaviourU5BU5D_t818*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m20945(__this, ___match, method) (( TrackableBehaviour_t44 * (*) (List_1_t716 *, Predicate_1_t3511 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20946(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3511 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20947(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t716 *, int32_t, int32_t, Predicate_1_t3511 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m20948(__this, method) (( Enumerator_t3512  (*) (List_1_t716 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::IndexOf(T)
#define List_1_IndexOf_m20949(__this, ___item, method) (( int32_t (*) (List_1_t716 *, TrackableBehaviour_t44 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20950(__this, ___start, ___delta, method) (( void (*) (List_1_t716 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20951(__this, ___index, method) (( void (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m20952(__this, ___index, ___item, method) (( void (*) (List_1_t716 *, int32_t, TrackableBehaviour_t44 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20953(__this, ___collection, method) (( void (*) (List_1_t716 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Remove(T)
#define List_1_Remove_m20954(__this, ___item, method) (( bool (*) (List_1_t716 *, TrackableBehaviour_t44 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20955(__this, ___match, method) (( int32_t (*) (List_1_t716 *, Predicate_1_t3511 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20956(__this, ___index, method) (( void (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Reverse()
#define List_1_Reverse_m20957(__this, method) (( void (*) (List_1_t716 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Sort()
#define List_1_Sort_m20958(__this, method) (( void (*) (List_1_t716 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20959(__this, ___comparison, method) (( void (*) (List_1_t716 *, Comparison_1_t3513 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::ToArray()
#define List_1_ToArray_m20960(__this, method) (( TrackableBehaviourU5BU5D_t818* (*) (List_1_t716 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::TrimExcess()
#define List_1_TrimExcess_m20961(__this, method) (( void (*) (List_1_t716 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Capacity()
#define List_1_get_Capacity_m20962(__this, method) (( int32_t (*) (List_1_t716 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20963(__this, ___value, method) (( void (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Count()
#define List_1_get_Count_m20964(__this, method) (( int32_t (*) (List_1_t716 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m20965(__this, ___index, method) (( TrackableBehaviour_t44 * (*) (List_1_t716 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m20966(__this, ___index, ___value, method) (( void (*) (List_1_t716 *, int32_t, TrackableBehaviour_t44 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
