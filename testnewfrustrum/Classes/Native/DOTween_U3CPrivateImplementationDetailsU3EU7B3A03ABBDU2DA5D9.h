﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#pragma pack(push, tp, 1)
struct  __StaticArrayInitTypeSizeU3D120_t1013 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t1013__padding[120];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D120_t1013_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t1013__padding[120];
	};
};
#pragma pack(pop, tp)
