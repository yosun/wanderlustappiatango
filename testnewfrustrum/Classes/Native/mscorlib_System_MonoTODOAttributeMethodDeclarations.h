﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t2064;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m10319 (MonoTODOAttribute_t2064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C" void MonoTODOAttribute__ctor_m10320 (MonoTODOAttribute_t2064 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
