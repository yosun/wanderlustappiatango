﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Slider
struct Slider_t22;
// UnityEngine.UI.Text
struct Text_t23;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SetRotationManually
struct  SetRotationManually_t24  : public MonoBehaviour_t7
{
	// UnityEngine.UI.Slider SetRotationManually::sliderX
	Slider_t22 * ___sliderX_2;
	// UnityEngine.UI.Slider SetRotationManually::sliderY
	Slider_t22 * ___sliderY_3;
	// UnityEngine.UI.Slider SetRotationManually::sliderZ
	Slider_t22 * ___sliderZ_4;
	// UnityEngine.UI.Text SetRotationManually::rotationvalue
	Text_t23 * ___rotationvalue_5;
};
