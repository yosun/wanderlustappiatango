﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.UInt16>
struct Collection_1_t3691;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.UInt16[]
struct UInt16U5BU5D_t1060;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4180;
// System.Collections.Generic.IList`1<System.UInt16>
struct IList_1_t3689;

// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::.ctor()
extern "C" void Collection_1__ctor_m23857_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23857(__this, method) (( void (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1__ctor_m23857_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23858_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23858(__this, method) (( bool (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23858_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23859_gshared (Collection_1_t3691 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23859(__this, ___array, ___index, method) (( void (*) (Collection_1_t3691 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23859_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23860_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23860(__this, method) (( Object_t * (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23860_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23861_gshared (Collection_1_t3691 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23861(__this, ___value, method) (( int32_t (*) (Collection_1_t3691 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23861_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23862_gshared (Collection_1_t3691 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23862(__this, ___value, method) (( bool (*) (Collection_1_t3691 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23862_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23863_gshared (Collection_1_t3691 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23863(__this, ___value, method) (( int32_t (*) (Collection_1_t3691 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23863_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23864_gshared (Collection_1_t3691 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23864(__this, ___index, ___value, method) (( void (*) (Collection_1_t3691 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23864_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23865_gshared (Collection_1_t3691 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23865(__this, ___value, method) (( void (*) (Collection_1_t3691 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23865_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23866_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23866(__this, method) (( bool (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23866_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23867_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23867(__this, method) (( Object_t * (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23867_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23868_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23868(__this, method) (( bool (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23868_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23869_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23869(__this, method) (( bool (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23869_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23870_gshared (Collection_1_t3691 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23870(__this, ___index, method) (( Object_t * (*) (Collection_1_t3691 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23870_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23871_gshared (Collection_1_t3691 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23871(__this, ___index, ___value, method) (( void (*) (Collection_1_t3691 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23871_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Add(T)
extern "C" void Collection_1_Add_m23872_gshared (Collection_1_t3691 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Add_m23872(__this, ___item, method) (( void (*) (Collection_1_t3691 *, uint16_t, const MethodInfo*))Collection_1_Add_m23872_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Clear()
extern "C" void Collection_1_Clear_m23873_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23873(__this, method) (( void (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_Clear_m23873_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::ClearItems()
extern "C" void Collection_1_ClearItems_m23874_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23874(__this, method) (( void (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_ClearItems_m23874_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Contains(T)
extern "C" bool Collection_1_Contains_m23875_gshared (Collection_1_t3691 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m23875(__this, ___item, method) (( bool (*) (Collection_1_t3691 *, uint16_t, const MethodInfo*))Collection_1_Contains_m23875_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23876_gshared (Collection_1_t3691 * __this, UInt16U5BU5D_t1060* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23876(__this, ___array, ___index, method) (( void (*) (Collection_1_t3691 *, UInt16U5BU5D_t1060*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23876_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23877_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23877(__this, method) (( Object_t* (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_GetEnumerator_m23877_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23878_gshared (Collection_1_t3691 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23878(__this, ___item, method) (( int32_t (*) (Collection_1_t3691 *, uint16_t, const MethodInfo*))Collection_1_IndexOf_m23878_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23879_gshared (Collection_1_t3691 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m23879(__this, ___index, ___item, method) (( void (*) (Collection_1_t3691 *, int32_t, uint16_t, const MethodInfo*))Collection_1_Insert_m23879_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23880_gshared (Collection_1_t3691 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23880(__this, ___index, ___item, method) (( void (*) (Collection_1_t3691 *, int32_t, uint16_t, const MethodInfo*))Collection_1_InsertItem_m23880_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Remove(T)
extern "C" bool Collection_1_Remove_m23881_gshared (Collection_1_t3691 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m23881(__this, ___item, method) (( bool (*) (Collection_1_t3691 *, uint16_t, const MethodInfo*))Collection_1_Remove_m23881_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23882_gshared (Collection_1_t3691 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23882(__this, ___index, method) (( void (*) (Collection_1_t3691 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23882_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23883_gshared (Collection_1_t3691 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23883(__this, ___index, method) (( void (*) (Collection_1_t3691 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23883_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23884_gshared (Collection_1_t3691 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23884(__this, method) (( int32_t (*) (Collection_1_t3691 *, const MethodInfo*))Collection_1_get_Count_m23884_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t Collection_1_get_Item_m23885_gshared (Collection_1_t3691 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23885(__this, ___index, method) (( uint16_t (*) (Collection_1_t3691 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23885_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23886_gshared (Collection_1_t3691 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23886(__this, ___index, ___value, method) (( void (*) (Collection_1_t3691 *, int32_t, uint16_t, const MethodInfo*))Collection_1_set_Item_m23886_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23887_gshared (Collection_1_t3691 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23887(__this, ___index, ___item, method) (( void (*) (Collection_1_t3691 *, int32_t, uint16_t, const MethodInfo*))Collection_1_SetItem_m23887_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23888_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23888(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23888_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::ConvertItem(System.Object)
extern "C" uint16_t Collection_1_ConvertItem_m23889_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23889(__this /* static, unused */, ___item, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23889_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23890_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23890(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23890_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23891_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23891(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23891_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23892_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23892(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23892_gshared)(__this /* static, unused */, ___list, method)
