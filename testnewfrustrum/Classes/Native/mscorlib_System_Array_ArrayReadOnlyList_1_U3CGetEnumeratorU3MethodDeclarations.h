﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3971;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m27433_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3971 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m27433(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3971 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m27433_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3971 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27434(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3971 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27434_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27435_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3971 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27435(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3971 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27435_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27436_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3971 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27436(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3971 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27436_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27437_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3971 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27437(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3971 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27437_gshared)(__this, method)
