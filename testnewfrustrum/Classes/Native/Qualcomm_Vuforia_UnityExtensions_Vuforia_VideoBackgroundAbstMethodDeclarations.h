﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t82;

// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
extern "C" bool VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4225 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
extern "C" void VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4226 (VideoBackgroundAbstractBehaviour_t82 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern "C" void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4227 (VideoBackgroundAbstractBehaviour_t82 * __this, bool ___disable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void VideoBackgroundAbstractBehaviour_SetStereoDepth_m4228 (VideoBackgroundAbstractBehaviour_t82 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
extern "C" void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4229 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
extern "C" void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4230 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern "C" void VideoBackgroundAbstractBehaviour_Awake_m4231 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPreRender_m4232 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPostRender_m4233 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern "C" void VideoBackgroundAbstractBehaviour_OnDestroy_m4234 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern "C" void VideoBackgroundAbstractBehaviour__ctor_m474 (VideoBackgroundAbstractBehaviour_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
