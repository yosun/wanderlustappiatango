﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.UlongPlugin
struct UlongPlugin_t984;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1037;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t1038;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t1039;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.UlongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_Reset_m5435 (UlongPlugin_t984 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 DG.Tweening.Plugins.UlongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.UInt64)
extern "C" uint64_t UlongPlugin_ConvertToStartValue_m5436 (UlongPlugin_t984 * __this, TweenerCore_3_t1037 * ___t, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UlongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_SetRelativeEndValue_m5437 (UlongPlugin_t984 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UlongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_SetChangeValue_m5438 (UlongPlugin_t984 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.UlongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt64)
extern "C" float UlongPlugin_GetSpeedBasedDuration_m5439 (UlongPlugin_t984 * __this, NoOptions_t933  ___options, float ___unitsXSecond, uint64_t ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UlongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.Single,System.UInt64,System.UInt64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void UlongPlugin_EvaluateAndApply_m5440 (UlongPlugin_t984 * __this, NoOptions_t933  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1038 * ___getter, DOSetter_1_t1039 * ___setter, float ___elapsed, uint64_t ___startValue, uint64_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UlongPlugin::.ctor()
extern "C" void UlongPlugin__ctor_m5441 (UlongPlugin_t984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
