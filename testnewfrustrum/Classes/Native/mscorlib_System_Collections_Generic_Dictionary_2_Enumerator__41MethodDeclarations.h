﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t3949;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1881;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__40MethodDeclarations.h"
#define Enumerator__ctor_m27232(__this, ___dictionary, method) (( void (*) (Enumerator_t3949 *, Dictionary_2_t1881 *, const MethodInfo*))Enumerator__ctor_m27130_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27233(__this, method) (( Object_t * (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27131_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27234(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27235(__this, method) (( Object_t * (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27236(__this, method) (( Object_t * (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m27237(__this, method) (( bool (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_MoveNext_m27135_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m27238(__this, method) (( KeyValuePair_2_t3946  (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_get_Current_m27136_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27239(__this, method) (( String_t* (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_get_CurrentKey_m27137_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27240(__this, method) (( bool (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_get_CurrentValue_m27138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m27241(__this, method) (( void (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_VerifyState_m27139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27242(__this, method) (( void (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_VerifyCurrent_m27140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m27243(__this, method) (( void (*) (Enumerator_t3949 *, const MethodInfo*))Enumerator_Dispose_m27141_gshared)(__this, method)
