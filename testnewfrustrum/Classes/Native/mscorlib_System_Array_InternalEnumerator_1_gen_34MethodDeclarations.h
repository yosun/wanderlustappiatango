﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>
struct InternalEnumerator_1_t3444;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19816_gshared (InternalEnumerator_1_t3444 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19816(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3444 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19816_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19817_gshared (InternalEnumerator_1_t3444 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19817(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19817_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19818_gshared (InternalEnumerator_1_t3444 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19818(__this, method) (( void (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19818_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19819_gshared (InternalEnumerator_1_t3444 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19819(__this, method) (( bool (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19819_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::get_Current()
extern "C" SurfaceData_t651  InternalEnumerator_1_get_Current_m19820_gshared (InternalEnumerator_1_t3444 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19820(__this, method) (( SurfaceData_t651  (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19820_gshared)(__this, method)
