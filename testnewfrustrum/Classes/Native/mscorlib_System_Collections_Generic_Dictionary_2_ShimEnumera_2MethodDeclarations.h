﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>
struct ShimEnumerator_t3472;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3461;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20197_gshared (ShimEnumerator_t3472 * __this, Dictionary_2_t3461 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20197(__this, ___host, method) (( void (*) (ShimEnumerator_t3472 *, Dictionary_2_t3461 *, const MethodInfo*))ShimEnumerator__ctor_m20197_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20198_gshared (ShimEnumerator_t3472 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20198(__this, method) (( bool (*) (ShimEnumerator_t3472 *, const MethodInfo*))ShimEnumerator_MoveNext_m20198_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m20199_gshared (ShimEnumerator_t3472 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20199(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3472 *, const MethodInfo*))ShimEnumerator_get_Entry_m20199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20200_gshared (ShimEnumerator_t3472 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20200(__this, method) (( Object_t * (*) (ShimEnumerator_t3472 *, const MethodInfo*))ShimEnumerator_get_Key_m20200_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20201_gshared (ShimEnumerator_t3472 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20201(__this, method) (( Object_t * (*) (ShimEnumerator_t3472 *, const MethodInfo*))ShimEnumerator_get_Value_m20201_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20202_gshared (ShimEnumerator_t3472 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20202(__this, method) (( Object_t * (*) (ShimEnumerator_t3472 *, const MethodInfo*))ShimEnumerator_get_Current_m20202_gshared)(__this, method)
