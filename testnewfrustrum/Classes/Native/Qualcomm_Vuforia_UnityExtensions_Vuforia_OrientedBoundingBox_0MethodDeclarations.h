﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox3D
struct OrientedBoundingBox3D_t599;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void OrientedBoundingBox3D__ctor_m2821 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___center, Vector3_t15  ___halfExtents, float ___rotationY, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t15  OrientedBoundingBox3D_get_Center_m2822 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m2823 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t15  OrientedBoundingBox3D_get_HalfExtents_m2824 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m2825 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m2826 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m2827 (OrientedBoundingBox3D_t599 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
