﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
struct InternalEnumerator_1_t3755;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24855_gshared (InternalEnumerator_1_t3755 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24855(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3755 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24855_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24856_gshared (InternalEnumerator_1_t3755 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24856(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3755 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24856_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24857_gshared (InternalEnumerator_1_t3755 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24857(__this, method) (( void (*) (InternalEnumerator_1_t3755 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24857_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24858_gshared (InternalEnumerator_1_t3755 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24858(__this, method) (( bool (*) (InternalEnumerator_1_t3755 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24858_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C" Keyframe_t1236  InternalEnumerator_1_get_Current_m24859_gshared (InternalEnumerator_1_t3755 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24859(__this, method) (( Keyframe_t1236  (*) (InternalEnumerator_1_t3755 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24859_gshared)(__this, method)
