﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct InternalEnumerator_1_t4000;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m27561(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4000 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19271_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27562(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4000 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
#define InternalEnumerator_1_Dispose_m27563(__this, method) (( void (*) (InternalEnumerator_1_t4000 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19273_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
#define InternalEnumerator_1_MoveNext_m27564(__this, method) (( bool (*) (InternalEnumerator_1_t4000 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19274_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
#define InternalEnumerator_1_get_Current_m27565(__this, method) (( uint8_t (*) (InternalEnumerator_1_t4000 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19275_gshared)(__this, method)
