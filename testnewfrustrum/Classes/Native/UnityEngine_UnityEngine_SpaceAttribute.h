﻿#pragma once
#include <stdint.h>
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t503  : public PropertyAttribute_t1329
{
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;
};
