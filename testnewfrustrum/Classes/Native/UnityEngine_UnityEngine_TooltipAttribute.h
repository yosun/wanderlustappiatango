﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t505  : public PropertyAttribute_t1329
{
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;
};
