﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t1045;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.String>::.ctor(System.Object,System.IntPtr)
// DG.Tweening.Core.DOSetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_13MethodDeclarations.h"
#define DOSetter_1__ctor_m23718(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1045 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23607_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T)
#define DOSetter_1_Invoke_m23719(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1045 *, String_t*, const MethodInfo*))DOSetter_1_Invoke_m23608_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define DOSetter_1_BeginInvoke_m23720(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1045 *, String_t*, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23609_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.String>::EndInvoke(System.IAsyncResult)
#define DOSetter_1_EndInvoke_m23721(__this, ___result, method) (( void (*) (DOSetter_1_t1045 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23610_gshared)(__this, ___result, method)
