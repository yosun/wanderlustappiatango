﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t252  : public UnityEventBase_t1347
{
	// System.Object[] UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::m_InvokeArray
	ObjectU5BU5D_t115* ___m_InvokeArray_4;
};
