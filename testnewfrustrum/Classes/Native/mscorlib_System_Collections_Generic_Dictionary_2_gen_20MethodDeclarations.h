﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t735;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1371;
// System.Collections.Generic.ICollection`1<Vuforia.WebCamProfile/ProfileData>
struct ICollection_1_t4252;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3619;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3620;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3087;
// System.Collections.Generic.IDictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct IDictionary_2_t4253;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4254;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t4255;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_36MethodDeclarations.h"
#define Dictionary_2__ctor_m22427(__this, method) (( void (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2__ctor_m22428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m22429(__this, ___comparer, method) (( void (*) (Dictionary_2_t735 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22430_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m22431(__this, ___dictionary, method) (( void (*) (Dictionary_2_t735 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22432_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
#define Dictionary_2__ctor_m22433(__this, ___capacity, method) (( void (*) (Dictionary_2_t735 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22434_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m22435(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t735 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22436_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m22437(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t735 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m22438_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22439(__this, method) (( Object_t* (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22441(__this, method) (( Object_t* (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22443(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22444_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22445(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t735 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22446_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m22447(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t735 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22448_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m22449(__this, ___key, method) (( bool (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22450_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m22451(__this, ___key, method) (( void (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22452_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22453(__this, method) (( bool (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22455(__this, method) (( Object_t * (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22457(__this, method) (( bool (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22459(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t735 *, KeyValuePair_2_t3618 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22461(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t735 *, KeyValuePair_2_t3618 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22463(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t735 *, KeyValuePair_2U5BU5D_t4254*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22465(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t735 *, KeyValuePair_2_t3618 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22467(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t735 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22468_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22469(__this, method) (( Object_t * (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22471(__this, method) (( Object_t* (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22473(__this, method) (( Object_t * (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Count()
#define Dictionary_2_get_Count_m22475(__this, method) (( int32_t (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_get_Count_m22476_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
#define Dictionary_2_get_Item_m22477(__this, ___key, method) (( ProfileData_t734  (*) (Dictionary_2_t735 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m22478_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m22479(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t735 *, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_set_Item_m22480_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m22481(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t735 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22482_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m22483(__this, ___size, method) (( void (*) (Dictionary_2_t735 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22484_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m22485(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t735 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22486_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m22487(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3618  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_make_pair_m22488_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m22489(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_pick_key_m22490_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m22491(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t734  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_pick_value_m22492_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m22493(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t735 *, KeyValuePair_2U5BU5D_t4254*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22494_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Resize()
#define Dictionary_2_Resize_m22495(__this, method) (( void (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_Resize_m22496_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
#define Dictionary_2_Add_m22497(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t735 *, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_Add_m22498_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Clear()
#define Dictionary_2_Clear_m22499(__this, method) (( void (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_Clear_m22500_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m22501(__this, ___key, method) (( bool (*) (Dictionary_2_t735 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m22502_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m22503(__this, ___value, method) (( bool (*) (Dictionary_2_t735 *, ProfileData_t734 , const MethodInfo*))Dictionary_2_ContainsValue_m22504_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m22505(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t735 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m22506_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m22507(__this, ___sender, method) (( void (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22508_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
#define Dictionary_2_Remove_m22509(__this, ___key, method) (( bool (*) (Dictionary_2_t735 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m22510_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m22511(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t735 *, String_t*, ProfileData_t734 *, const MethodInfo*))Dictionary_2_TryGetValue_m22512_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Keys()
#define Dictionary_2_get_Keys_m22513(__this, method) (( KeyCollection_t3619 * (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_get_Keys_m22514_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Values()
#define Dictionary_2_get_Values_m22515(__this, method) (( ValueCollection_t3620 * (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_get_Values_m22516_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m22517(__this, ___key, method) (( String_t* (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22518_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m22519(__this, ___value, method) (( ProfileData_t734  (*) (Dictionary_2_t735 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22520_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m22521(__this, ___pair, method) (( bool (*) (Dictionary_2_t735 *, KeyValuePair_2_t3618 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22522_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m22523(__this, method) (( Enumerator_t3621  (*) (Dictionary_2_t735 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22524_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m22525(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t734 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22526_gshared)(__this /* static, unused */, ___key, ___value, method)
