﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
struct Enumerator_t3400;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t609;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"
#define Enumerator__ctor_m19074(__this, ___l, method) (( void (*) (Enumerator_t3400 *, List_1_t609 *, const MethodInfo*))Enumerator__ctor_m18954_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19075(__this, method) (( Object_t * (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18955_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
#define Enumerator_Dispose_m19076(__this, method) (( void (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_Dispose_m18956_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
#define Enumerator_VerifyState_m19077(__this, method) (( void (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_VerifyState_m18957_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
#define Enumerator_MoveNext_m19078(__this, method) (( bool (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_MoveNext_m4422_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
#define Enumerator_get_Current_m19079(__this, method) (( int32_t (*) (Enumerator_t3400 *, const MethodInfo*))Enumerator_get_Current_m4420_gshared)(__this, method)
