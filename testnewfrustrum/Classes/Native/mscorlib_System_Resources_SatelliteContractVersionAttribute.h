﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1875;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1583  : public Attribute_t138
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1875 * ___ver_0;
};
