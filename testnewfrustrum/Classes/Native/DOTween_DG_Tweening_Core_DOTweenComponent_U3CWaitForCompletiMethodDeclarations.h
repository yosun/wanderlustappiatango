﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
struct U3CWaitForCompletionU3Ed__0_t936;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::MoveNext()
extern "C" bool U3CWaitForCompletionU3Ed__0_MoveNext_m5277 (U3CWaitForCompletionU3Ed__0_t936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278 (U3CWaitForCompletionU3Ed__0_t936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.IDisposable.Dispose()
extern "C" void U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279 (U3CWaitForCompletionU3Ed__0_t936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280 (U3CWaitForCompletionU3Ed__0_t936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::.ctor(System.Int32)
extern "C" void U3CWaitForCompletionU3Ed__0__ctor_m5281 (U3CWaitForCompletionU3Ed__0_t936 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
