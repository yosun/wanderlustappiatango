﻿#pragma once
#include <stdint.h>
// Vuforia.InternalEyewear
struct InternalEyewear_t587;
// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t560;
// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t562;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.InternalEyewear
struct  InternalEyewear_t587  : public Object_t
{
	// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::mProfileManager
	InternalEyewearCalibrationProfileManager_t560 * ___mProfileManager_1;
	// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::mCalibrator
	InternalEyewearUserCalibrator_t562 * ___mCalibrator_2;
};
struct InternalEyewear_t587_StaticFields{
	// Vuforia.InternalEyewear Vuforia.InternalEyewear::mInstance
	InternalEyewear_t587 * ___mInstance_0;
};
