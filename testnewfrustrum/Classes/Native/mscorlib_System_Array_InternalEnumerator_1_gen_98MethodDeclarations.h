﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct InternalEnumerator_1_t3972;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27438_gshared (InternalEnumerator_1_t3972 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27438(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3972 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27438_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27439_gshared (InternalEnumerator_1_t3972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27439(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3972 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27439_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27440_gshared (InternalEnumerator_1_t3972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27440(__this, method) (( void (*) (InternalEnumerator_1_t3972 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27440_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27441_gshared (InternalEnumerator_1_t3972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27441(__this, method) (( bool (*) (InternalEnumerator_1_t3972 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27441_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t2069  InternalEnumerator_1_get_Current_m27442_gshared (InternalEnumerator_1_t3972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27442(__this, method) (( TableRange_t2069  (*) (InternalEnumerator_1_t3972 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27442_gshared)(__this, method)
