﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t68;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ReconstructionAbstractBehaviour>
struct  Predicate_1_t3455  : public MulticastDelegate_t307
{
};
