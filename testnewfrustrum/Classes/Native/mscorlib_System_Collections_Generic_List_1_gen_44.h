﻿#pragma once
#include <stdint.h>
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t3643;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct  List_1_t754  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_items
	IUserDefinedTargetEventHandlerU5BU5D_t3643* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t754_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::EmptyArray
	IUserDefinedTargetEventHandlerU5BU5D_t3643* ___EmptyArray_4;
};
