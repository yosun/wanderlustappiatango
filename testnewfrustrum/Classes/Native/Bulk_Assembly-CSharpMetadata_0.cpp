﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t0_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t0_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
// Metadata Definition ARVRModes/TheCurrentMode
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentModeMethodDeclarations.h"
static const MethodInfo* TheCurrentMode_t1_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference TheCurrentMode_t1_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TheCurrentMode_t1_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair TheCurrentMode_t1_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TheCurrentMode_t1_0_0_0;
extern const Il2CppType TheCurrentMode_t1_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
extern TypeInfo ARVRModes_t6_il2cpp_TypeInfo;
extern const Il2CppType ARVRModes_t6_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata TheCurrentMode_t1_DefinitionMetadata = 
{
	&ARVRModes_t6_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TheCurrentMode_t1_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TheCurrentMode_t1_VTable/* vtableMethods */
	, TheCurrentMode_t1_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TheCurrentMode"/* name */
	, ""/* namespaze */
	, TheCurrentMode_t1_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TheCurrentMode_t1_0_0_0/* byval_arg */
	, &TheCurrentMode_t1_1_0_0/* this_arg */
	, &TheCurrentMode_t1_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TheCurrentMode_t1)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TheCurrentMode_t1)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
// Metadata Definition ARVRModes
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModesMethodDeclarations.h"
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::.ctor()
extern const MethodInfo ARVRModes__ctor_m0_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ARVRModes__ctor_m0/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::.cctor()
extern const MethodInfo ARVRModes__cctor_m1_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ARVRModes__cctor_m1/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::Start()
extern const MethodInfo ARVRModes_Start_m2_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ARVRModes_Start_m2/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppType GameObjectU5BU5D_t5_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ARVRModes_t6_ARVRModes_FlipGameObjectArray_m3_ParameterInfos[] = 
{
	{"g", 0, 134217729, 0, &GameObjectU5BU5D_t5_0_0_0},
	{"flip", 1, 134217730, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::FlipGameObjectArray(UnityEngine.GameObject[],System.Boolean)
extern const MethodInfo ARVRModes_FlipGameObjectArray_m3_MethodInfo = 
{
	"FlipGameObjectArray"/* name */
	, (methodPointerType)&ARVRModes_FlipGameObjectArray_m3/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, ARVRModes_t6_ARVRModes_FlipGameObjectArray_m3_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SeeEntire()
extern const MethodInfo ARVRModes_SeeEntire_m4_MethodInfo = 
{
	"SeeEntire"/* name */
	, (methodPointerType)&ARVRModes_SeeEntire_m4/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SeeDoor()
extern const MethodInfo ARVRModes_SeeDoor_m5_MethodInfo = 
{
	"SeeDoor"/* name */
	, (methodPointerType)&ARVRModes_SeeDoor_m5/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchARToVR()
extern const MethodInfo ARVRModes_SwitchARToVR_m6_MethodInfo = 
{
	"SwitchARToVR"/* name */
	, (methodPointerType)&ARVRModes_SwitchARToVR_m6/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchVRToAR()
extern const MethodInfo ARVRModes_SwitchVRToAR_m7_MethodInfo = 
{
	"SwitchVRToAR"/* name */
	, (methodPointerType)&ARVRModes_SwitchVRToAR_m7/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchModes()
extern const MethodInfo ARVRModes_SwitchModes_m8_MethodInfo = 
{
	"SwitchModes"/* name */
	, (methodPointerType)&ARVRModes_SwitchModes_m8/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::Update()
extern const MethodInfo ARVRModes_Update_m9_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ARVRModes_Update_m9/* method */
	, &ARVRModes_t6_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ARVRModes_t6_MethodInfos[] =
{
	&ARVRModes__ctor_m0_MethodInfo,
	&ARVRModes__cctor_m1_MethodInfo,
	&ARVRModes_Start_m2_MethodInfo,
	&ARVRModes_FlipGameObjectArray_m3_MethodInfo,
	&ARVRModes_SeeEntire_m4_MethodInfo,
	&ARVRModes_SeeDoor_m5_MethodInfo,
	&ARVRModes_SwitchARToVR_m6_MethodInfo,
	&ARVRModes_SwitchVRToAR_m7_MethodInfo,
	&ARVRModes_SwitchModes_m8_MethodInfo,
	&ARVRModes_Update_m9_MethodInfo,
	NULL
};
static const Il2CppType* ARVRModes_t6_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TheCurrentMode_t1_0_0_0,
};
extern const MethodInfo Object_Equals_m537_MethodInfo;
extern const MethodInfo Object_GetHashCode_m538_MethodInfo;
extern const MethodInfo Object_ToString_m539_MethodInfo;
static const Il2CppMethodReference ARVRModes_t6_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool ARVRModes_t6_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ARVRModes_t6_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct ARVRModes_t6;
const Il2CppTypeDefinitionMetadata ARVRModes_t6_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ARVRModes_t6_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, ARVRModes_t6_VTable/* vtableMethods */
	, ARVRModes_t6_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3/* fieldStart */

};
TypeInfo ARVRModes_t6_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ARVRModes"/* name */
	, ""/* namespaze */
	, ARVRModes_t6_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ARVRModes_t6_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ARVRModes_t6_0_0_0/* byval_arg */
	, &ARVRModes_t6_1_0_0/* this_arg */
	, &ARVRModes_t6_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ARVRModes_t6)/* instance_size */
	, sizeof (ARVRModes_t6)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ARVRModes_t6_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
// Metadata Definition AnimateTexture
extern TypeInfo AnimateTexture_t9_il2cpp_TypeInfo;
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTextureMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::.ctor()
extern const MethodInfo AnimateTexture__ctor_m10_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnimateTexture__ctor_m10/* method */
	, &AnimateTexture_t9_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Awake()
extern const MethodInfo AnimateTexture_Awake_m11_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&AnimateTexture_Awake_m11/* method */
	, &AnimateTexture_t9_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Update()
extern const MethodInfo AnimateTexture_Update_m12_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AnimateTexture_Update_m12/* method */
	, &AnimateTexture_t9_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AnimateTexture_t9_MethodInfos[] =
{
	&AnimateTexture__ctor_m10_MethodInfo,
	&AnimateTexture_Awake_m11_MethodInfo,
	&AnimateTexture_Update_m12_MethodInfo,
	NULL
};
static const Il2CppMethodReference AnimateTexture_t9_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool AnimateTexture_t9_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AnimateTexture_t9_0_0_0;
extern const Il2CppType AnimateTexture_t9_1_0_0;
struct AnimateTexture_t9;
const Il2CppTypeDefinitionMetadata AnimateTexture_t9_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, AnimateTexture_t9_VTable/* vtableMethods */
	, AnimateTexture_t9_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 15/* fieldStart */

};
TypeInfo AnimateTexture_t9_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimateTexture"/* name */
	, ""/* namespaze */
	, AnimateTexture_t9_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AnimateTexture_t9_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimateTexture_t9_0_0_0/* byval_arg */
	, &AnimateTexture_t9_1_0_0/* this_arg */
	, &AnimateTexture_t9_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimateTexture_t9)/* instance_size */
	, sizeof (AnimateTexture_t9)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
// Metadata Definition Gyro2
extern TypeInfo Gyro2_t12_il2cpp_TypeInfo;
// Gyro2
#include "AssemblyU2DCSharp_Gyro2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::.ctor()
extern const MethodInfo Gyro2__ctor_m13_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Gyro2__ctor_m13/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::.cctor()
extern const MethodInfo Gyro2__cctor_m14_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Gyro2__cctor_m14/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Awake()
extern const MethodInfo Gyro2_Awake_m15_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Gyro2_Awake_m15/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Start()
extern const MethodInfo Gyro2_Start_m16_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Gyro2_Start_m16/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_ClampPosInRoom_m17_ParameterInfos[] = 
{
	{"p", 0, 134217731, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern const MethodInfo Gyro2_ClampPosInRoom_m17_MethodInfo = 
{
	"ClampPosInRoom"/* name */
	, (methodPointerType)&Gyro2_ClampPosInRoom_m17/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Vector3_t15/* invoker_method */
	, Gyro2_t12_Gyro2_ClampPosInRoom_m17_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Quaternion_t13_0_0_0;
extern const Il2CppType Quaternion_t13_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_StationParent_m18_ParameterInfos[] = 
{
	{"pos", 0, 134217732, 0, &Vector3_t15_0_0_0},
	{"rot", 1, 134217733, 0, &Quaternion_t13_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern const MethodInfo Gyro2_StationParent_m18_MethodInfo = 
{
	"StationParent"/* name */
	, (methodPointerType)&Gyro2_StationParent_m18/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15_Quaternion_t13/* invoker_method */
	, Gyro2_t12_Gyro2_StationParent_m18_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_StationParentShiftAngle_m19_ParameterInfos[] = 
{
	{"angle", 0, 134217734, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern const MethodInfo Gyro2_StationParentShiftAngle_m19_MethodInfo = 
{
	"StationParentShiftAngle"/* name */
	, (methodPointerType)&Gyro2_StationParentShiftAngle_m19/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Gyro2_t12_Gyro2_StationParentShiftAngle_m19_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_Init_m20_ParameterInfos[] = 
{
	{"t", 0, 134217735, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Init(UnityEngine.Transform)
extern const MethodInfo Gyro2_Init_m20_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&Gyro2_Init_m20/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Gyro2_t12_Gyro2_Init_m20_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::ResetStatic()
extern const MethodInfo Gyro2_ResetStatic_m21_MethodInfo = 
{
	"ResetStatic"/* name */
	, (methodPointerType)&Gyro2_ResetStatic_m21/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::ResetStaticSmoothly()
extern const MethodInfo Gyro2_ResetStaticSmoothly_m22_MethodInfo = 
{
	"ResetStaticSmoothly"/* name */
	, (methodPointerType)&Gyro2_ResetStaticSmoothly_m22/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Reset()
extern const MethodInfo Gyro2_Reset_m23_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Gyro2_Reset_m23/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_DoSwipeRotation_m24_ParameterInfos[] = 
{
	{"degree", 0, 134217736, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern const MethodInfo Gyro2_DoSwipeRotation_m24_MethodInfo = 
{
	"DoSwipeRotation"/* name */
	, (methodPointerType)&Gyro2_DoSwipeRotation_m24/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Gyro2_t12_Gyro2_DoSwipeRotation_m24_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::DisableSettingsButotn()
extern const MethodInfo Gyro2_DisableSettingsButotn_m25_MethodInfo = 
{
	"DisableSettingsButotn"/* name */
	, (methodPointerType)&Gyro2_DisableSettingsButotn_m25/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::Update()
extern const MethodInfo Gyro2_Update_m26_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Gyro2_Update_m26/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::AssignCalibration()
extern const MethodInfo Gyro2_AssignCalibration_m27_MethodInfo = 
{
	"AssignCalibration"/* name */
	, (methodPointerType)&Gyro2_AssignCalibration_m27/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Gyro2::AssignCamBaseRotation()
extern const MethodInfo Gyro2_AssignCamBaseRotation_m28_MethodInfo = 
{
	"AssignCamBaseRotation"/* name */
	, (methodPointerType)&Gyro2_AssignCamBaseRotation_m28/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Gyro2::G()
extern const MethodInfo Gyro2_G_m29_MethodInfo = 
{
	"G"/* name */
	, (methodPointerType)&Gyro2_G_m29/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t13_0_0_0;
static const ParameterInfo Gyro2_t12_Gyro2_ConvertRotation_m30_ParameterInfos[] = 
{
	{"q", 0, 134217737, 0, &Quaternion_t13_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t13_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern const MethodInfo Gyro2_ConvertRotation_m30_MethodInfo = 
{
	"ConvertRotation"/* name */
	, (methodPointerType)&Gyro2_ConvertRotation_m30/* method */
	, &Gyro2_t12_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13_Quaternion_t13/* invoker_method */
	, Gyro2_t12_Gyro2_ConvertRotation_m30_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Gyro2_t12_MethodInfos[] =
{
	&Gyro2__ctor_m13_MethodInfo,
	&Gyro2__cctor_m14_MethodInfo,
	&Gyro2_Awake_m15_MethodInfo,
	&Gyro2_Start_m16_MethodInfo,
	&Gyro2_ClampPosInRoom_m17_MethodInfo,
	&Gyro2_StationParent_m18_MethodInfo,
	&Gyro2_StationParentShiftAngle_m19_MethodInfo,
	&Gyro2_Init_m20_MethodInfo,
	&Gyro2_ResetStatic_m21_MethodInfo,
	&Gyro2_ResetStaticSmoothly_m22_MethodInfo,
	&Gyro2_Reset_m23_MethodInfo,
	&Gyro2_DoSwipeRotation_m24_MethodInfo,
	&Gyro2_DisableSettingsButotn_m25_MethodInfo,
	&Gyro2_Update_m26_MethodInfo,
	&Gyro2_AssignCalibration_m27_MethodInfo,
	&Gyro2_AssignCamBaseRotation_m28_MethodInfo,
	&Gyro2_G_m29_MethodInfo,
	&Gyro2_ConvertRotation_m30_MethodInfo,
	NULL
};
static const Il2CppMethodReference Gyro2_t12_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool Gyro2_t12_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Gyro2_t12_0_0_0;
extern const Il2CppType Gyro2_t12_1_0_0;
struct Gyro2_t12;
const Il2CppTypeDefinitionMetadata Gyro2_t12_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Gyro2_t12_VTable/* vtableMethods */
	, Gyro2_t12_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 18/* fieldStart */

};
TypeInfo Gyro2_t12_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Gyro2"/* name */
	, ""/* namespaze */
	, Gyro2_t12_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Gyro2_t12_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Gyro2_t12_0_0_0/* byval_arg */
	, &Gyro2_t12_1_0_0/* this_arg */
	, &Gyro2_t12_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Gyro2_t12)/* instance_size */
	, sizeof (Gyro2_t12)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Gyro2_t12_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
// Metadata Definition GyroCameraYo
extern TypeInfo GyroCameraYo_t14_il2cpp_TypeInfo;
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::.ctor()
extern const MethodInfo GyroCameraYo__ctor_m31_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GyroCameraYo__ctor_m31/* method */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Start()
extern const MethodInfo GyroCameraYo_Start_m32_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&GyroCameraYo_Start_m32/* method */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Update()
extern const MethodInfo GyroCameraYo_Update_m33_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&GyroCameraYo_Update_m33/* method */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t13_0_0_0;
static const ParameterInfo GyroCameraYo_t14_GyroCameraYo_ConvertRotation_m34_ParameterInfos[] = 
{
	{"q", 0, 134217738, 0, &Quaternion_t13_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t13_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern const MethodInfo GyroCameraYo_ConvertRotation_m34_MethodInfo = 
{
	"ConvertRotation"/* name */
	, (methodPointerType)&GyroCameraYo_ConvertRotation_m34/* method */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13_Quaternion_t13/* invoker_method */
	, GyroCameraYo_t14_GyroCameraYo_ConvertRotation_m34_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::GyroCam()
extern const MethodInfo GyroCameraYo_GyroCam_m35_MethodInfo = 
{
	"GyroCam"/* name */
	, (methodPointerType)&GyroCameraYo_GyroCam_m35/* method */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GyroCameraYo_t14_MethodInfos[] =
{
	&GyroCameraYo__ctor_m31_MethodInfo,
	&GyroCameraYo_Start_m32_MethodInfo,
	&GyroCameraYo_Update_m33_MethodInfo,
	&GyroCameraYo_ConvertRotation_m34_MethodInfo,
	&GyroCameraYo_GyroCam_m35_MethodInfo,
	NULL
};
static const Il2CppMethodReference GyroCameraYo_t14_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool GyroCameraYo_t14_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GyroCameraYo_t14_0_0_0;
extern const Il2CppType GyroCameraYo_t14_1_0_0;
struct GyroCameraYo_t14;
const Il2CppTypeDefinitionMetadata GyroCameraYo_t14_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, GyroCameraYo_t14_VTable/* vtableMethods */
	, GyroCameraYo_t14_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 28/* fieldStart */

};
TypeInfo GyroCameraYo_t14_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GyroCameraYo"/* name */
	, ""/* namespaze */
	, GyroCameraYo_t14_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GyroCameraYo_t14_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GyroCameraYo_t14_0_0_0/* byval_arg */
	, &GyroCameraYo_t14_1_0_0/* this_arg */
	, &GyroCameraYo_t14_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GyroCameraYo_t14)/* instance_size */
	, sizeof (GyroCameraYo_t14)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Input2
#include "AssemblyU2DCSharp_Input2.h"
// Metadata Definition Input2
extern TypeInfo Input2_t16_il2cpp_TypeInfo;
// Input2
#include "AssemblyU2DCSharp_Input2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Input2::.ctor()
extern const MethodInfo Input2__ctor_m36_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Input2__ctor_m36/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Input2::.cctor()
extern const MethodInfo Input2__cctor_m37_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Input2__cctor_m37/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::IsPointerOverUI()
extern const MethodInfo Input2_IsPointerOverUI_m38_MethodInfo = 
{
	"IsPointerOverUI"/* name */
	, (methodPointerType)&Input2_IsPointerOverUI_m38/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::TouchBegin()
extern const MethodInfo Input2_TouchBegin_m39_MethodInfo = 
{
	"TouchBegin"/* name */
	, (methodPointerType)&Input2_TouchBegin_m39/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetFirstPoint()
extern const MethodInfo Input2_GetFirstPoint_m40_MethodInfo = 
{
	"GetFirstPoint"/* name */
	, (methodPointerType)&Input2_GetFirstPoint_m40/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern const MethodInfo Input2_GetSecondPoint_m41_MethodInfo = 
{
	"GetSecondPoint"/* name */
	, (methodPointerType)&Input2_GetSecondPoint_m41/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::GetPointerCount()
extern const MethodInfo Input2_GetPointerCount_m42_MethodInfo = 
{
	"GetPointerCount"/* name */
	, (methodPointerType)&Input2_GetPointerCount_m42/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Input2_t16_Input2_HasPointStarted_m43_ParameterInfos[] = 
{
	{"num", 0, 134217739, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointStarted(System.Int32)
extern const MethodInfo Input2_HasPointStarted_m43_MethodInfo = 
{
	"HasPointStarted"/* name */
	, (methodPointerType)&Input2_HasPointStarted_m43/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Input2_t16_Input2_HasPointStarted_m43_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Input2_t16_Input2_HasPointEnded_m44_ParameterInfos[] = 
{
	{"num", 0, 134217740, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointEnded(System.Int32)
extern const MethodInfo Input2_HasPointEnded_m44_MethodInfo = 
{
	"HasPointEnded"/* name */
	, (methodPointerType)&Input2_HasPointEnded_m44/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Input2_t16_Input2_HasPointEnded_m44_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Input2_t16_Input2_HasPointMoved_m45_ParameterInfos[] = 
{
	{"num", 0, 134217741, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointMoved(System.Int32)
extern const MethodInfo Input2_HasPointMoved_m45_MethodInfo = 
{
	"HasPointMoved"/* name */
	, (methodPointerType)&Input2_HasPointMoved_m45/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Input2_t16_Input2_HasPointMoved_m45_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single Input2::Pinch()
extern const MethodInfo Input2_Pinch_m46_MethodInfo = 
{
	"Pinch"/* name */
	, (methodPointerType)&Input2_Pinch_m46/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single Input2::Twist()
extern const MethodInfo Input2_Twist_m47_MethodInfo = 
{
	"Twist"/* name */
	, (methodPointerType)&Input2_Twist_m47/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::TwistInt()
extern const MethodInfo Input2_TwistInt_m48_MethodInfo = 
{
	"TwistInt"/* name */
	, (methodPointerType)&Input2_TwistInt_m48/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Input2_t16_Input2_SwipeLeft_m49_ParameterInfos[] = 
{
	{"swipedir", 0, 134217742, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeLeft(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeLeft_m49_MethodInfo = 
{
	"SwipeLeft"/* name */
	, (methodPointerType)&Input2_SwipeLeft_m49/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10/* invoker_method */
	, Input2_t16_Input2_SwipeLeft_m49_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Input2_t16_Input2_SwipeRight_m50_ParameterInfos[] = 
{
	{"swipedir", 0, 134217743, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeRight(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeRight_m50_MethodInfo = 
{
	"SwipeRight"/* name */
	, (methodPointerType)&Input2_SwipeRight_m50/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10/* invoker_method */
	, Input2_t16_Input2_SwipeRight_m50_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Input2_t16_Input2_SwipeDown_m51_ParameterInfos[] = 
{
	{"swipedir", 0, 134217744, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeDown(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeDown_m51_MethodInfo = 
{
	"SwipeDown"/* name */
	, (methodPointerType)&Input2_SwipeDown_m51/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10/* invoker_method */
	, Input2_t16_Input2_SwipeDown_m51_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Input2_t16_Input2_SwipeUp_m52_ParameterInfos[] = 
{
	{"swipedir", 0, 134217745, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::SwipeUp(UnityEngine.Vector2)
extern const MethodInfo Input2_SwipeUp_m52_MethodInfo = 
{
	"SwipeUp"/* name */
	, (methodPointerType)&Input2_SwipeUp_m52/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10/* invoker_method */
	, Input2_t16_Input2_SwipeUp_m52_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::ContinuousSwipe()
extern const MethodInfo Input2_ContinuousSwipe_m53_MethodInfo = 
{
	"ContinuousSwipe"/* name */
	, (methodPointerType)&Input2_ContinuousSwipe_m53/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::Swipe()
extern const MethodInfo Input2_Swipe_m54_MethodInfo = 
{
	"Swipe"/* name */
	, (methodPointerType)&Input2_Swipe_m54/* method */
	, &Input2_t16_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Input2_t16_MethodInfos[] =
{
	&Input2__ctor_m36_MethodInfo,
	&Input2__cctor_m37_MethodInfo,
	&Input2_IsPointerOverUI_m38_MethodInfo,
	&Input2_TouchBegin_m39_MethodInfo,
	&Input2_GetFirstPoint_m40_MethodInfo,
	&Input2_GetSecondPoint_m41_MethodInfo,
	&Input2_GetPointerCount_m42_MethodInfo,
	&Input2_HasPointStarted_m43_MethodInfo,
	&Input2_HasPointEnded_m44_MethodInfo,
	&Input2_HasPointMoved_m45_MethodInfo,
	&Input2_Pinch_m46_MethodInfo,
	&Input2_Twist_m47_MethodInfo,
	&Input2_TwistInt_m48_MethodInfo,
	&Input2_SwipeLeft_m49_MethodInfo,
	&Input2_SwipeRight_m50_MethodInfo,
	&Input2_SwipeDown_m51_MethodInfo,
	&Input2_SwipeUp_m52_MethodInfo,
	&Input2_ContinuousSwipe_m53_MethodInfo,
	&Input2_Swipe_m54_MethodInfo,
	NULL
};
static const Il2CppMethodReference Input2_t16_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool Input2_t16_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Input2_t16_0_0_0;
extern const Il2CppType Input2_t16_1_0_0;
struct Input2_t16;
const Il2CppTypeDefinitionMetadata Input2_t16_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Input2_t16_VTable/* vtableMethods */
	, Input2_t16_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 32/* fieldStart */

};
TypeInfo Input2_t16_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Input2"/* name */
	, ""/* namespaze */
	, Input2_t16_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Input2_t16_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Input2_t16_0_0_0/* byval_arg */
	, &Input2_t16_1_0_0/* this_arg */
	, &Input2_t16_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Input2_t16)/* instance_size */
	, sizeof (Input2_t16)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Input2_t16_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
// Metadata Definition Mathf2
extern TypeInfo Mathf2_t17_il2cpp_TypeInfo;
// Mathf2
#include "AssemblyU2DCSharp_Mathf2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.ctor()
extern const MethodInfo Mathf2__ctor_m55_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mathf2__ctor_m55/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.cctor()
extern const MethodInfo Mathf2__cctor_m56_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Mathf2__cctor_m56/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RaycastHit_t94_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreenIgnore0_m57_MethodInfo = 
{
	"WhatDidWeHitCenterScreenIgnore0"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreenIgnore0_m57/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayerMask_t95_0_0_0;
extern const Il2CppType LayerMask_t95_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_WhatDidWeHitCenterScreen_m58_ParameterInfos[] = 
{
	{"lm", 0, 134217746, 0, &LayerMask_t95_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t94_LayerMask_t95 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreen_m58_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m58/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94_LayerMask_t95/* invoker_method */
	, Mathf2_t17_Mathf2_WhatDidWeHitCenterScreen_m58_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RaycastHit_t94 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern const MethodInfo Mathf2_WhatDidWeHitCenterScreen_m59_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m59/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_WhatDidWeHit_m60_ParameterInfos[] = 
{
	{"v", 0, 134217747, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t94_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern const MethodInfo Mathf2_WhatDidWeHit_m60_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m60/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94_Vector2_t10/* invoker_method */
	, Mathf2_t17_Mathf2_WhatDidWeHit_m60_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_WhatDidWeHitCam_m61_ParameterInfos[] = 
{
	{"v", 0, 134217748, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t94_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCam(UnityEngine.Vector2)
extern const MethodInfo Mathf2_WhatDidWeHitCam_m61_MethodInfo = 
{
	"WhatDidWeHitCam"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCam_m61/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94_Vector2_t10/* invoker_method */
	, Mathf2_t17_Mathf2_WhatDidWeHitCam_m61_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType LayerMask_t95_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_WhatDidWeHit_m62_ParameterInfos[] = 
{
	{"v", 0, 134217749, 0, &Vector2_t10_0_0_0},
	{"lm", 1, 134217750, 0, &LayerMask_t95_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t94_Vector2_t10_LayerMask_t95 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHit_m62_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m62/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94_Vector2_t10_LayerMask_t95/* invoker_method */
	, Mathf2_t17_Mathf2_WhatDidWeHit_m62_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Ray_t96_0_0_0;
extern const Il2CppType Ray_t96_0_0_0;
extern const Il2CppType LayerMask_t95_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_WhatDidWeHit_m63_ParameterInfos[] = 
{
	{"ray", 0, 134217751, 0, &Ray_t96_0_0_0},
	{"lm", 1, 134217752, 0, &LayerMask_t95_0_0_0},
};
extern void* RuntimeInvoker_RaycastHit_t94_Ray_t96_LayerMask_t95 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
extern const MethodInfo Mathf2_WhatDidWeHit_m63_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m63/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t94_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t94_Ray_t96_LayerMask_t95/* invoker_method */
	, Mathf2_t17_Mathf2_WhatDidWeHit_m63_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_Pad0s_m64_ParameterInfos[] = 
{
	{"n", 0, 134217753, 0, &Int32_t127_0_0_0},
};
extern const Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::Pad0s(System.Int32)
extern const MethodInfo Mathf2_Pad0s_m64_MethodInfo = 
{
	"Pad0s"/* name */
	, (methodPointerType)&Mathf2_Pad0s_m64/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, Mathf2_t17_Mathf2_Pad0s_m64_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_Round10th_m65_ParameterInfos[] = 
{
	{"n", 0, 134217754, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::Round10th(System.Single)
extern const MethodInfo Mathf2_Round10th_m65_MethodInfo = 
{
	"Round10th"/* name */
	, (methodPointerType)&Mathf2_Round10th_m65/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151/* invoker_method */
	, Mathf2_t17_Mathf2_Round10th_m65_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GenUUID()
extern const MethodInfo Mathf2_GenUUID_m66_MethodInfo = 
{
	"GenUUID"/* name */
	, (methodPointerType)&Mathf2_GenUUID_m66/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_RoundVector3_m67_ParameterInfos[] = 
{
	{"v", 0, 134217755, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern const MethodInfo Mathf2_RoundVector3_m67_MethodInfo = 
{
	"RoundVector3"/* name */
	, (methodPointerType)&Mathf2_RoundVector3_m67/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Vector3_t15/* invoker_method */
	, Mathf2_t17_Mathf2_RoundVector3_m67_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::RandRot()
extern const MethodInfo Mathf2_RandRot_m68_MethodInfo = 
{
	"RandRot"/* name */
	, (methodPointerType)&Mathf2_RandRot_m68/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_round_m69_ParameterInfos[] = 
{
	{"f", 0, 134217756, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::round(System.Single)
extern const MethodInfo Mathf2_round_m69_MethodInfo = 
{
	"round"/* name */
	, (methodPointerType)&Mathf2_round_m69/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151/* invoker_method */
	, Mathf2_t17_Mathf2_round_m69_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Float_m70_ParameterInfos[] = 
{
	{"str", 0, 134217757, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::String2Float(System.String)
extern const MethodInfo Mathf2_String2Float_m70_MethodInfo = 
{
	"String2Float"/* name */
	, (methodPointerType)&Mathf2_String2Float_m70/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Float_m70_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_isNumeric_m71_ParameterInfos[] = 
{
	{"str", 0, 134217758, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mathf2::isNumeric(System.String)
extern const MethodInfo Mathf2_isNumeric_m71_MethodInfo = 
{
	"isNumeric"/* name */
	, (methodPointerType)&Mathf2_isNumeric_m71/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_isNumeric_m71_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Int_m72_ParameterInfos[] = 
{
	{"str", 0, 134217759, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Mathf2::String2Int(System.String)
extern const MethodInfo Mathf2_String2Int_m72_MethodInfo = 
{
	"String2Int"/* name */
	, (methodPointerType)&Mathf2_String2Int_m72/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Int_m72_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Color_m73_ParameterInfos[] = 
{
	{"str", 0, 134217760, 0, &String_t_0_0_0},
};
extern const Il2CppType Color_t90_0_0_0;
extern void* RuntimeInvoker_Color_t90_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color Mathf2::String2Color(System.String)
extern const MethodInfo Mathf2_String2Color_m73_MethodInfo = 
{
	"String2Color"/* name */
	, (methodPointerType)&Mathf2_String2Color_m73/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Color_t90_0_0_0/* return_type */
	, RuntimeInvoker_Color_t90_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Color_m73_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Vector3_m74_ParameterInfos[] = 
{
	{"str", 0, 134217761, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern const MethodInfo Mathf2_String2Vector3_m74_MethodInfo = 
{
	"String2Vector3"/* name */
	, (methodPointerType)&Mathf2_String2Vector3_m74/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Vector3_m74_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Vector2_m75_ParameterInfos[] = 
{
	{"str", 0, 134217762, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t10_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern const MethodInfo Mathf2_String2Vector2_m75_MethodInfo = 
{
	"String2Vector2"/* name */
	, (methodPointerType)&Mathf2_String2Vector2_m75/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Vector2_m75_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_RoundFraction_m76_ParameterInfos[] = 
{
	{"val", 0, 134217763, 0, &Single_t151_0_0_0},
	{"denominator", 1, 134217764, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern const MethodInfo Mathf2_RoundFraction_m76_MethodInfo = 
{
	"RoundFraction"/* name */
	, (methodPointerType)&Mathf2_RoundFraction_m76/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151/* invoker_method */
	, Mathf2_t17_Mathf2_RoundFraction_m76_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_String2Quat_m77_ParameterInfos[] = 
{
	{"str", 0, 134217765, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Quaternion_t13_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern const MethodInfo Mathf2_String2Quat_m77_MethodInfo = 
{
	"String2Quat"/* name */
	, (methodPointerType)&Mathf2_String2Quat_m77/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_String2Quat_m77_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_UnsignedVector3_m78_ParameterInfos[] = 
{
	{"v", 0, 134217766, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern const MethodInfo Mathf2_UnsignedVector3_m78_MethodInfo = 
{
	"UnsignedVector3"/* name */
	, (methodPointerType)&Mathf2_UnsignedVector3_m78/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Vector3_t15/* invoker_method */
	, Mathf2_t17_Mathf2_UnsignedVector3_m78_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetUnixTime()
extern const MethodInfo Mathf2_GetUnixTime_m79_MethodInfo = 
{
	"GetUnixTime"/* name */
	, (methodPointerType)&Mathf2_GetUnixTime_m79/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_GetPositionString_m80_ParameterInfos[] = 
{
	{"t", 0, 134217767, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetPositionString_m80_MethodInfo = 
{
	"GetPositionString"/* name */
	, (methodPointerType)&Mathf2_GetPositionString_m80/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_GetPositionString_m80_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_GetRotationString_m81_ParameterInfos[] = 
{
	{"t", 0, 134217768, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetRotationString_m81_MethodInfo = 
{
	"GetRotationString"/* name */
	, (methodPointerType)&Mathf2_GetRotationString_m81/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_GetRotationString_m81_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
static const ParameterInfo Mathf2_t17_Mathf2_GetScaleString_m82_ParameterInfos[] = 
{
	{"t", 0, 134217769, 0, &Transform_t11_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern const MethodInfo Mathf2_GetScaleString_m82_MethodInfo = 
{
	"GetScaleString"/* name */
	, (methodPointerType)&Mathf2_GetScaleString_m82/* method */
	, &Mathf2_t17_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t17_Mathf2_GetScaleString_m82_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mathf2_t17_MethodInfos[] =
{
	&Mathf2__ctor_m55_MethodInfo,
	&Mathf2__cctor_m56_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreenIgnore0_m57_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m58_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m59_MethodInfo,
	&Mathf2_WhatDidWeHit_m60_MethodInfo,
	&Mathf2_WhatDidWeHitCam_m61_MethodInfo,
	&Mathf2_WhatDidWeHit_m62_MethodInfo,
	&Mathf2_WhatDidWeHit_m63_MethodInfo,
	&Mathf2_Pad0s_m64_MethodInfo,
	&Mathf2_Round10th_m65_MethodInfo,
	&Mathf2_GenUUID_m66_MethodInfo,
	&Mathf2_RoundVector3_m67_MethodInfo,
	&Mathf2_RandRot_m68_MethodInfo,
	&Mathf2_round_m69_MethodInfo,
	&Mathf2_String2Float_m70_MethodInfo,
	&Mathf2_isNumeric_m71_MethodInfo,
	&Mathf2_String2Int_m72_MethodInfo,
	&Mathf2_String2Color_m73_MethodInfo,
	&Mathf2_String2Vector3_m74_MethodInfo,
	&Mathf2_String2Vector2_m75_MethodInfo,
	&Mathf2_RoundFraction_m76_MethodInfo,
	&Mathf2_String2Quat_m77_MethodInfo,
	&Mathf2_UnsignedVector3_m78_MethodInfo,
	&Mathf2_GetUnixTime_m79_MethodInfo,
	&Mathf2_GetPositionString_m80_MethodInfo,
	&Mathf2_GetRotationString_m81_MethodInfo,
	&Mathf2_GetScaleString_m82_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference Mathf2_t17_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Mathf2_t17_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Mathf2_t17_0_0_0;
extern const Il2CppType Mathf2_t17_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Mathf2_t17;
const Il2CppTypeDefinitionMetadata Mathf2_t17_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Mathf2_t17_VTable/* vtableMethods */
	, Mathf2_t17_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 39/* fieldStart */

};
TypeInfo Mathf2_t17_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mathf2"/* name */
	, ""/* namespaze */
	, Mathf2_t17_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Mathf2_t17_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mathf2_t17_0_0_0/* byval_arg */
	, &Mathf2_t17_1_0_0/* this_arg */
	, &Mathf2_t17_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mathf2_t17)/* instance_size */
	, sizeof (Mathf2_t17)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Mathf2_t17_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
// Metadata Definition Raycaster
extern TypeInfo Raycaster_t18_il2cpp_TypeInfo;
// Raycaster
#include "AssemblyU2DCSharp_RaycasterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::.ctor()
extern const MethodInfo Raycaster__ctor_m83_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Raycaster__ctor_m83/* method */
	, &Raycaster_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Raycaster_t18_Raycaster_RaycastIt_m84_ParameterInfos[] = 
{
	{"cam", 0, 134217770, 0, &Camera_t3_0_0_0},
	{"pos", 1, 134217771, 0, &Vector2_t10_0_0_0},
	{"distance", 2, 134217772, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Vector2_t10_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern const MethodInfo Raycaster_RaycastIt_m84_MethodInfo = 
{
	"RaycastIt"/* name */
	, (methodPointerType)&Raycaster_RaycastIt_m84/* method */
	, &Raycaster_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Vector2_t10_Single_t151/* invoker_method */
	, Raycaster_t18_Raycaster_RaycastIt_m84_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Raycaster_t18_MethodInfos[] =
{
	&Raycaster__ctor_m83_MethodInfo,
	&Raycaster_RaycastIt_m84_MethodInfo,
	NULL
};
static const Il2CppMethodReference Raycaster_t18_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool Raycaster_t18_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Raycaster_t18_0_0_0;
extern const Il2CppType Raycaster_t18_1_0_0;
struct Raycaster_t18;
const Il2CppTypeDefinitionMetadata Raycaster_t18_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Raycaster_t18_VTable/* vtableMethods */
	, Raycaster_t18_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Raycaster_t18_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Raycaster"/* name */
	, ""/* namespaze */
	, Raycaster_t18_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Raycaster_t18_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Raycaster_t18_0_0_0/* byval_arg */
	, &Raycaster_t18_1_0_0/* this_arg */
	, &Raycaster_t18_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Raycaster_t18)/* instance_size */
	, sizeof (Raycaster_t18)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
// Metadata Definition SetRenderQueue
extern TypeInfo SetRenderQueue_t20_il2cpp_TypeInfo;
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueueMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::.ctor()
extern const MethodInfo SetRenderQueue__ctor_m85_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueue__ctor_m85/* method */
	, &SetRenderQueue_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::Awake()
extern const MethodInfo SetRenderQueue_Awake_m86_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueue_Awake_m86/* method */
	, &SetRenderQueue_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRenderQueue_t20_MethodInfos[] =
{
	&SetRenderQueue__ctor_m85_MethodInfo,
	&SetRenderQueue_Awake_m86_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRenderQueue_t20_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool SetRenderQueue_t20_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRenderQueue_t20_0_0_0;
extern const Il2CppType SetRenderQueue_t20_1_0_0;
struct SetRenderQueue_t20;
const Il2CppTypeDefinitionMetadata SetRenderQueue_t20_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRenderQueue_t20_VTable/* vtableMethods */
	, SetRenderQueue_t20_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 40/* fieldStart */

};
TypeInfo SetRenderQueue_t20_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueue"/* name */
	, ""/* namespaze */
	, SetRenderQueue_t20_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRenderQueue_t20_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRenderQueue_t20_0_0_0/* byval_arg */
	, &SetRenderQueue_t20_1_0_0/* this_arg */
	, &SetRenderQueue_t20_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueue_t20)/* instance_size */
	, sizeof (SetRenderQueue_t20)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
// Metadata Definition SetRenderQueueChildren
extern TypeInfo SetRenderQueueChildren_t21_il2cpp_TypeInfo;
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildrenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::.ctor()
extern const MethodInfo SetRenderQueueChildren__ctor_m87_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueueChildren__ctor_m87/* method */
	, &SetRenderQueueChildren_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Start()
extern const MethodInfo SetRenderQueueChildren_Start_m88_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Start_m88/* method */
	, &SetRenderQueueChildren_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Awake()
extern const MethodInfo SetRenderQueueChildren_Awake_m89_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Awake_m89/* method */
	, &SetRenderQueueChildren_t21_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRenderQueueChildren_t21_MethodInfos[] =
{
	&SetRenderQueueChildren__ctor_m87_MethodInfo,
	&SetRenderQueueChildren_Start_m88_MethodInfo,
	&SetRenderQueueChildren_Awake_m89_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRenderQueueChildren_t21_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool SetRenderQueueChildren_t21_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRenderQueueChildren_t21_0_0_0;
extern const Il2CppType SetRenderQueueChildren_t21_1_0_0;
struct SetRenderQueueChildren_t21;
const Il2CppTypeDefinitionMetadata SetRenderQueueChildren_t21_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRenderQueueChildren_t21_VTable/* vtableMethods */
	, SetRenderQueueChildren_t21_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 41/* fieldStart */

};
TypeInfo SetRenderQueueChildren_t21_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueueChildren"/* name */
	, ""/* namespaze */
	, SetRenderQueueChildren_t21_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRenderQueueChildren_t21_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRenderQueueChildren_t21_0_0_0/* byval_arg */
	, &SetRenderQueueChildren_t21_1_0_0/* this_arg */
	, &SetRenderQueueChildren_t21_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueueChildren_t21)/* instance_size */
	, sizeof (SetRenderQueueChildren_t21)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
// Metadata Definition SetRotationManually
extern TypeInfo SetRotationManually_t24_il2cpp_TypeInfo;
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManuallyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::.ctor()
extern const MethodInfo SetRotationManually__ctor_m90_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRotationManually__ctor_m90/* method */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderX()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderX_m91_MethodInfo = 
{
	"UpdateRotationFromSliderX"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderX_m91/* method */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderY()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderY_m92_MethodInfo = 
{
	"UpdateRotationFromSliderY"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderY_m92/* method */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::UpdateRotationFromSliderZ()
extern const MethodInfo SetRotationManually_UpdateRotationFromSliderZ_m93_MethodInfo = 
{
	"UpdateRotationFromSliderZ"/* name */
	, (methodPointerType)&SetRotationManually_UpdateRotationFromSliderZ_m93/* method */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SetRotationManually::Update()
extern const MethodInfo SetRotationManually_Update_m94_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SetRotationManually_Update_m94/* method */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetRotationManually_t24_MethodInfos[] =
{
	&SetRotationManually__ctor_m90_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderX_m91_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderY_m92_MethodInfo,
	&SetRotationManually_UpdateRotationFromSliderZ_m93_MethodInfo,
	&SetRotationManually_Update_m94_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetRotationManually_t24_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool SetRotationManually_t24_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SetRotationManually_t24_0_0_0;
extern const Il2CppType SetRotationManually_t24_1_0_0;
struct SetRotationManually_t24;
const Il2CppTypeDefinitionMetadata SetRotationManually_t24_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, SetRotationManually_t24_VTable/* vtableMethods */
	, SetRotationManually_t24_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 42/* fieldStart */

};
TypeInfo SetRotationManually_t24_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRotationManually"/* name */
	, ""/* namespaze */
	, SetRotationManually_t24_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetRotationManually_t24_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetRotationManually_t24_0_0_0/* byval_arg */
	, &SetRotationManually_t24_1_0_0/* this_arg */
	, &SetRotationManually_t24_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRotationManually_t24)/* instance_size */
	, sizeof (SetRotationManually_t24)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
// Metadata Definition WorldLoop
extern TypeInfo WorldLoop_t25_il2cpp_TypeInfo;
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoopMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::.ctor()
extern const MethodInfo WorldLoop__ctor_m95_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldLoop__ctor_m95/* method */
	, &WorldLoop_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Awake()
extern const MethodInfo WorldLoop_Awake_m96_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldLoop_Awake_m96/* method */
	, &WorldLoop_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Start()
extern const MethodInfo WorldLoop_Start_m97_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WorldLoop_Start_m97/* method */
	, &WorldLoop_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Update()
extern const MethodInfo WorldLoop_Update_m98_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WorldLoop_Update_m98/* method */
	, &WorldLoop_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WorldLoop_t25_MethodInfos[] =
{
	&WorldLoop__ctor_m95_MethodInfo,
	&WorldLoop_Awake_m96_MethodInfo,
	&WorldLoop_Start_m97_MethodInfo,
	&WorldLoop_Update_m98_MethodInfo,
	NULL
};
static const Il2CppMethodReference WorldLoop_t25_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool WorldLoop_t25_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WorldLoop_t25_0_0_0;
extern const Il2CppType WorldLoop_t25_1_0_0;
struct WorldLoop_t25;
const Il2CppTypeDefinitionMetadata WorldLoop_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WorldLoop_t25_VTable/* vtableMethods */
	, WorldLoop_t25_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 46/* fieldStart */

};
TypeInfo WorldLoop_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldLoop"/* name */
	, ""/* namespaze */
	, WorldLoop_t25_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WorldLoop_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WorldLoop_t25_0_0_0/* byval_arg */
	, &WorldLoop_t25_1_0_0/* this_arg */
	, &WorldLoop_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldLoop_t25)/* instance_size */
	, sizeof (WorldLoop_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
// Metadata Definition WorldNav
extern TypeInfo WorldNav_t26_il2cpp_TypeInfo;
// WorldNav
#include "AssemblyU2DCSharp_WorldNavMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.ctor()
extern const MethodInfo WorldNav__ctor_m99_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldNav__ctor_m99/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.cctor()
extern const MethodInfo WorldNav__cctor_m100_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&WorldNav__cctor_m100/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Awake()
extern const MethodInfo WorldNav_Awake_m101_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldNav_Awake_m101/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Update()
extern const MethodInfo WorldNav_Update_m102_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WorldNav_Update_m102/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo WorldNav_t26_WorldNav_Walk_m103_ParameterInfos[] = 
{
	{"dir", 0, 134217773, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Walk(System.Int32)
extern const MethodInfo WorldNav_Walk_m103_MethodInfo = 
{
	"Walk"/* name */
	, (methodPointerType)&WorldNav_Walk_m103/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, WorldNav_t26_WorldNav_Walk_m103_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo WorldNav_t26_WorldNav_WalkDirTrue_m104_ParameterInfos[] = 
{
	{"dir", 0, 134217774, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern const MethodInfo WorldNav_WalkDirTrue_m104_MethodInfo = 
{
	"WalkDirTrue"/* name */
	, (methodPointerType)&WorldNav_WalkDirTrue_m104/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, WorldNav_t26_WorldNav_WalkDirTrue_m104_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo WorldNav_t26_WorldNav_WalkDirFalse_m105_ParameterInfos[] = 
{
	{"dir", 0, 134217775, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern const MethodInfo WorldNav_WalkDirFalse_m105_MethodInfo = 
{
	"WalkDirFalse"/* name */
	, (methodPointerType)&WorldNav_WalkDirFalse_m105/* method */
	, &WorldNav_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, WorldNav_t26_WorldNav_WalkDirFalse_m105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WorldNav_t26_MethodInfos[] =
{
	&WorldNav__ctor_m99_MethodInfo,
	&WorldNav__cctor_m100_MethodInfo,
	&WorldNav_Awake_m101_MethodInfo,
	&WorldNav_Update_m102_MethodInfo,
	&WorldNav_Walk_m103_MethodInfo,
	&WorldNav_WalkDirTrue_m104_MethodInfo,
	&WorldNav_WalkDirFalse_m105_MethodInfo,
	NULL
};
static const Il2CppMethodReference WorldNav_t26_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool WorldNav_t26_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WorldNav_t26_0_0_0;
extern const Il2CppType WorldNav_t26_1_0_0;
struct WorldNav_t26;
const Il2CppTypeDefinitionMetadata WorldNav_t26_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WorldNav_t26_VTable/* vtableMethods */
	, WorldNav_t26_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 48/* fieldStart */

};
TypeInfo WorldNav_t26_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldNav"/* name */
	, ""/* namespaze */
	, WorldNav_t26_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WorldNav_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WorldNav_t26_0_0_0/* byval_arg */
	, &WorldNav_t26_1_0_0/* this_arg */
	, &WorldNav_t26_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldNav_t26)/* instance_size */
	, sizeof (WorldNav_t26)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WorldNav_t26_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Basics
#include "AssemblyU2DCSharp_Basics.h"
// Metadata Definition Basics
extern TypeInfo Basics_t27_il2cpp_TypeInfo;
// Basics
#include "AssemblyU2DCSharp_BasicsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::.ctor()
extern const MethodInfo Basics__ctor_m106_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Basics__ctor_m106/* method */
	, &Basics_t27_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::Start()
extern const MethodInfo Basics_Start_m107_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Basics_Start_m107/* method */
	, &Basics_t27_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Basics::<Start>m__0()
extern const MethodInfo Basics_U3CStartU3Em__0_m108_MethodInfo = 
{
	"<Start>m__0"/* name */
	, (methodPointerType)&Basics_U3CStartU3Em__0_m108/* method */
	, &Basics_t27_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15/* invoker_method */
	, NULL/* parameters */
	, 5/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Basics_t27_Basics_U3CStartU3Em__1_m109_ParameterInfos[] = 
{
	{"x", 0, 134217776, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern const MethodInfo Basics_U3CStartU3Em__1_m109_MethodInfo = 
{
	"<Start>m__1"/* name */
	, (methodPointerType)&Basics_U3CStartU3Em__1_m109/* method */
	, &Basics_t27_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15/* invoker_method */
	, Basics_t27_Basics_U3CStartU3Em__1_m109_ParameterInfos/* parameters */
	, 6/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Basics_t27_MethodInfos[] =
{
	&Basics__ctor_m106_MethodInfo,
	&Basics_Start_m107_MethodInfo,
	&Basics_U3CStartU3Em__0_m108_MethodInfo,
	&Basics_U3CStartU3Em__1_m109_MethodInfo,
	NULL
};
static const Il2CppMethodReference Basics_t27_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool Basics_t27_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Basics_t27_0_0_0;
extern const Il2CppType Basics_t27_1_0_0;
struct Basics_t27;
const Il2CppTypeDefinitionMetadata Basics_t27_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Basics_t27_VTable/* vtableMethods */
	, Basics_t27_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 55/* fieldStart */

};
TypeInfo Basics_t27_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Basics"/* name */
	, ""/* namespaze */
	, Basics_t27_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Basics_t27_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Basics_t27_0_0_0/* byval_arg */
	, &Basics_t27_1_0_0/* this_arg */
	, &Basics_t27_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Basics_t27)/* instance_size */
	, sizeof (Basics_t27)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
// Metadata Definition Sequences
extern TypeInfo Sequences_t28_il2cpp_TypeInfo;
// Sequences
#include "AssemblyU2DCSharp_SequencesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Sequences::.ctor()
extern const MethodInfo Sequences__ctor_m110_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Sequences__ctor_m110/* method */
	, &Sequences_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Sequences::Start()
extern const MethodInfo Sequences_Start_m111_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Sequences_Start_m111/* method */
	, &Sequences_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Sequences_t28_MethodInfos[] =
{
	&Sequences__ctor_m110_MethodInfo,
	&Sequences_Start_m111_MethodInfo,
	NULL
};
static const Il2CppMethodReference Sequences_t28_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool Sequences_t28_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Sequences_t28_0_0_0;
extern const Il2CppType Sequences_t28_1_0_0;
struct Sequences_t28;
const Il2CppTypeDefinitionMetadata Sequences_t28_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, Sequences_t28_VTable/* vtableMethods */
	, Sequences_t28_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 57/* fieldStart */

};
TypeInfo Sequences_t28_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sequences"/* name */
	, ""/* namespaze */
	, Sequences_t28_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Sequences_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sequences_t28_0_0_0/* byval_arg */
	, &Sequences_t28_1_0_0/* this_arg */
	, &Sequences_t28_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sequences_t28)/* instance_size */
	, sizeof (Sequences_t28)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
// Metadata Definition OrbitCamera
extern TypeInfo OrbitCamera_t29_il2cpp_TypeInfo;
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::.ctor()
extern const MethodInfo OrbitCamera__ctor_m112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrbitCamera__ctor_m112/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Start()
extern const MethodInfo OrbitCamera_Start_m113_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&OrbitCamera_Start_m113/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Update()
extern const MethodInfo OrbitCamera_Update_m114_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&OrbitCamera_Update_m114/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::LateUpdate()
extern const MethodInfo OrbitCamera_LateUpdate_m115_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&OrbitCamera_LateUpdate_m115/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::HandlePlayerInput()
extern const MethodInfo OrbitCamera_HandlePlayerInput_m116_MethodInfo = 
{
	"HandlePlayerInput"/* name */
	, (methodPointerType)&OrbitCamera_HandlePlayerInput_m116/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::CalculateDesiredPosition()
extern const MethodInfo OrbitCamera_CalculateDesiredPosition_m117_MethodInfo = 
{
	"CalculateDesiredPosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculateDesiredPosition_m117/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo OrbitCamera_t29_OrbitCamera_CalculatePosition_m118_ParameterInfos[] = 
{
	{"rotationX", 0, 134217777, 0, &Single_t151_0_0_0},
	{"rotationY", 1, 134217778, 0, &Single_t151_0_0_0},
	{"distance", 2, 134217779, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern const MethodInfo OrbitCamera_CalculatePosition_m118_MethodInfo = 
{
	"CalculatePosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculatePosition_m118/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Single_t151_Single_t151_Single_t151/* invoker_method */
	, OrbitCamera_t29_OrbitCamera_CalculatePosition_m118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::UpdatePosition()
extern const MethodInfo OrbitCamera_UpdatePosition_m119_MethodInfo = 
{
	"UpdatePosition"/* name */
	, (methodPointerType)&OrbitCamera_UpdatePosition_m119/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Reset()
extern const MethodInfo OrbitCamera_Reset_m120_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&OrbitCamera_Reset_m120/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo OrbitCamera_t29_OrbitCamera_ClampAngle_m121_ParameterInfos[] = 
{
	{"angle", 0, 134217780, 0, &Single_t151_0_0_0},
	{"min", 1, 134217781, 0, &Single_t151_0_0_0},
	{"max", 2, 134217782, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern const MethodInfo OrbitCamera_ClampAngle_m121_MethodInfo = 
{
	"ClampAngle"/* name */
	, (methodPointerType)&OrbitCamera_ClampAngle_m121/* method */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, OrbitCamera_t29_OrbitCamera_ClampAngle_m121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrbitCamera_t29_MethodInfos[] =
{
	&OrbitCamera__ctor_m112_MethodInfo,
	&OrbitCamera_Start_m113_MethodInfo,
	&OrbitCamera_Update_m114_MethodInfo,
	&OrbitCamera_LateUpdate_m115_MethodInfo,
	&OrbitCamera_HandlePlayerInput_m116_MethodInfo,
	&OrbitCamera_CalculateDesiredPosition_m117_MethodInfo,
	&OrbitCamera_CalculatePosition_m118_MethodInfo,
	&OrbitCamera_UpdatePosition_m119_MethodInfo,
	&OrbitCamera_Reset_m120_MethodInfo,
	&OrbitCamera_ClampAngle_m121_MethodInfo,
	NULL
};
static const Il2CppMethodReference OrbitCamera_t29_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool OrbitCamera_t29_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OrbitCamera_t29_0_0_0;
extern const Il2CppType OrbitCamera_t29_1_0_0;
struct OrbitCamera_t29;
const Il2CppTypeDefinitionMetadata OrbitCamera_t29_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, OrbitCamera_t29_VTable/* vtableMethods */
	, OrbitCamera_t29_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 58/* fieldStart */

};
TypeInfo OrbitCamera_t29_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrbitCamera"/* name */
	, ""/* namespaze */
	, OrbitCamera_t29_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrbitCamera_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrbitCamera_t29_0_0_0/* byval_arg */
	, &OrbitCamera_t29_1_0_0/* this_arg */
	, &OrbitCamera_t29_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrbitCamera_t29)/* instance_size */
	, sizeof (OrbitCamera_t29)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
// Metadata Definition TestParticles
extern TypeInfo TestParticles_t30_il2cpp_TypeInfo;
// TestParticles
#include "AssemblyU2DCSharp_TestParticlesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::.ctor()
extern const MethodInfo TestParticles__ctor_m122_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TestParticles__ctor_m122/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Start()
extern const MethodInfo TestParticles_Start_m123_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TestParticles_Start_m123/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Update()
extern const MethodInfo TestParticles_Update_m124_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TestParticles_Update_m124/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::OnGUI()
extern const MethodInfo TestParticles_OnGUI_m125_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&TestParticles_OnGUI_m125/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ShowParticle()
extern const MethodInfo TestParticles_ShowParticle_m126_MethodInfo = 
{
	"ShowParticle"/* name */
	, (methodPointerType)&TestParticles_ShowParticle_m126/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TestParticles_t30_TestParticles_ParticleInformationWindow_m127_ParameterInfos[] = 
{
	{"id", 0, 134217783, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern const MethodInfo TestParticles_ParticleInformationWindow_m127_MethodInfo = 
{
	"ParticleInformationWindow"/* name */
	, (methodPointerType)&TestParticles_ParticleInformationWindow_m127/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, TestParticles_t30_TestParticles_ParticleInformationWindow_m127_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TestParticles_t30_TestParticles_InfoWindow_m128_ParameterInfos[] = 
{
	{"id", 0, 134217784, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::InfoWindow(System.Int32)
extern const MethodInfo TestParticles_InfoWindow_m128_MethodInfo = 
{
	"InfoWindow"/* name */
	, (methodPointerType)&TestParticles_InfoWindow_m128/* method */
	, &TestParticles_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, TestParticles_t30_TestParticles_InfoWindow_m128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TestParticles_t30_MethodInfos[] =
{
	&TestParticles__ctor_m122_MethodInfo,
	&TestParticles_Start_m123_MethodInfo,
	&TestParticles_Update_m124_MethodInfo,
	&TestParticles_OnGUI_m125_MethodInfo,
	&TestParticles_ShowParticle_m126_MethodInfo,
	&TestParticles_ParticleInformationWindow_m127_MethodInfo,
	&TestParticles_InfoWindow_m128_MethodInfo,
	NULL
};
static const Il2CppMethodReference TestParticles_t30_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool TestParticles_t30_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TestParticles_t30_0_0_0;
extern const Il2CppType TestParticles_t30_1_0_0;
struct TestParticles_t30;
const Il2CppTypeDefinitionMetadata TestParticles_t30_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TestParticles_t30_VTable/* vtableMethods */
	, TestParticles_t30_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 80/* fieldStart */

};
TypeInfo TestParticles_t30_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TestParticles"/* name */
	, ""/* namespaze */
	, TestParticles_t30_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TestParticles_t30_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TestParticles_t30_0_0_0/* byval_arg */
	, &TestParticles_t30_1_0_0/* this_arg */
	, &TestParticles_t30_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TestParticles_t30)/* instance_size */
	, sizeof (TestParticles_t30)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
// Metadata Definition Vuforia.BackgroundPlaneBehaviour
extern TypeInfo BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo;
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern const MethodInfo BackgroundPlaneBehaviour__ctor_m129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackgroundPlaneBehaviour__ctor_m129/* method */
	, &BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BackgroundPlaneBehaviour_t31_MethodInfos[] =
{
	&BackgroundPlaneBehaviour__ctor_m129_MethodInfo,
	NULL
};
extern const MethodInfo BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m543_MethodInfo;
static const Il2CppMethodReference BackgroundPlaneBehaviour_t31_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m543_MethodInfo,
};
static bool BackgroundPlaneBehaviour_t31_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVideoBackgroundEventHandler_t171_0_0_0;
static Il2CppInterfaceOffsetPair BackgroundPlaneBehaviour_t31_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t171_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BackgroundPlaneBehaviour_t31_0_0_0;
extern const Il2CppType BackgroundPlaneBehaviour_t31_1_0_0;
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t32_0_0_0;
struct BackgroundPlaneBehaviour_t31;
const Il2CppTypeDefinitionMetadata BackgroundPlaneBehaviour_t31_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BackgroundPlaneBehaviour_t31_InterfacesOffsets/* interfaceOffsets */
	, &BackgroundPlaneAbstractBehaviour_t32_0_0_0/* parent */
	, BackgroundPlaneBehaviour_t31_VTable/* vtableMethods */
	, BackgroundPlaneBehaviour_t31_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackgroundPlaneBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, BackgroundPlaneBehaviour_t31_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BackgroundPlaneBehaviour_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackgroundPlaneBehaviour_t31_0_0_0/* byval_arg */
	, &BackgroundPlaneBehaviour_t31_1_0_0/* this_arg */
	, &BackgroundPlaneBehaviour_t31_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackgroundPlaneBehaviour_t31)/* instance_size */
	, sizeof (BackgroundPlaneBehaviour_t31)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
// Metadata Definition Vuforia.CloudRecoBehaviour
extern TypeInfo CloudRecoBehaviour_t33_il2cpp_TypeInfo;
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern const MethodInfo CloudRecoBehaviour__ctor_m130_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CloudRecoBehaviour__ctor_m130/* method */
	, &CloudRecoBehaviour_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CloudRecoBehaviour_t33_MethodInfos[] =
{
	&CloudRecoBehaviour__ctor_m130_MethodInfo,
	NULL
};
static const Il2CppMethodReference CloudRecoBehaviour_t33_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool CloudRecoBehaviour_t33_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CloudRecoBehaviour_t33_0_0_0;
extern const Il2CppType CloudRecoBehaviour_t33_1_0_0;
extern const Il2CppType CloudRecoAbstractBehaviour_t34_0_0_0;
struct CloudRecoBehaviour_t33;
const Il2CppTypeDefinitionMetadata CloudRecoBehaviour_t33_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CloudRecoAbstractBehaviour_t34_0_0_0/* parent */
	, CloudRecoBehaviour_t33_VTable/* vtableMethods */
	, CloudRecoBehaviour_t33_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CloudRecoBehaviour_t33_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CloudRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CloudRecoBehaviour_t33_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CloudRecoBehaviour_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CloudRecoBehaviour_t33_0_0_0/* byval_arg */
	, &CloudRecoBehaviour_t33_1_0_0/* this_arg */
	, &CloudRecoBehaviour_t33_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CloudRecoBehaviour_t33)/* instance_size */
	, sizeof (CloudRecoBehaviour_t33)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
// Metadata Definition Vuforia.CylinderTargetBehaviour
extern TypeInfo CylinderTargetBehaviour_t35_il2cpp_TypeInfo;
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern const MethodInfo CylinderTargetBehaviour__ctor_m131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CylinderTargetBehaviour__ctor_m131/* method */
	, &CylinderTargetBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CylinderTargetBehaviour_t35_MethodInfos[] =
{
	&CylinderTargetBehaviour__ctor_m131_MethodInfo,
	NULL
};
extern const MethodInfo TrackableBehaviour_get_TrackableName_m544_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo;
extern const MethodInfo TrackableBehaviour_get_Trackable_m547_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m555_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m556_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m557_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m558_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_OnTrackerUpdate_m560_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m561_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m562_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_CorrectScaleImpl_m563_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587_MethodInfo;
extern const MethodInfo DataSetTrackableBehaviour_OnDrawGizmos_m588_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m589_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m590_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m591_MethodInfo;
extern const MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m592_MethodInfo;
static const Il2CppMethodReference CylinderTargetBehaviour_t35_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m555_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m556_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m557_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m558_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m560_MethodInfo,
	&CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m561_MethodInfo,
	&CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m562_MethodInfo,
	&CylinderTargetAbstractBehaviour_CorrectScaleImpl_m563_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m588_MethodInfo,
	&CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m589_MethodInfo,
	&CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m590_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m591_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m592_MethodInfo,
};
static bool CylinderTargetBehaviour_t35_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorCylinderTargetBehaviour_t172_0_0_0;
extern const Il2CppType IEditorDataSetTrackableBehaviour_t173_0_0_0;
extern const Il2CppType IEditorTrackableBehaviour_t174_0_0_0;
extern const Il2CppType WorldCenterTrackableBehaviour_t175_0_0_0;
static Il2CppInterfaceOffsetPair CylinderTargetBehaviour_t35_InterfacesOffsets[] = 
{
	{ &IEditorCylinderTargetBehaviour_t172_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t173_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CylinderTargetBehaviour_t35_0_0_0;
extern const Il2CppType CylinderTargetBehaviour_t35_1_0_0;
extern const Il2CppType CylinderTargetAbstractBehaviour_t36_0_0_0;
struct CylinderTargetBehaviour_t35;
const Il2CppTypeDefinitionMetadata CylinderTargetBehaviour_t35_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CylinderTargetBehaviour_t35_InterfacesOffsets/* interfaceOffsets */
	, &CylinderTargetAbstractBehaviour_t36_0_0_0/* parent */
	, CylinderTargetBehaviour_t35_VTable/* vtableMethods */
	, CylinderTargetBehaviour_t35_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CylinderTargetBehaviour_t35_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CylinderTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CylinderTargetBehaviour_t35_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CylinderTargetBehaviour_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CylinderTargetBehaviour_t35_0_0_0/* byval_arg */
	, &CylinderTargetBehaviour_t35_1_0_0/* this_arg */
	, &CylinderTargetBehaviour_t35_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CylinderTargetBehaviour_t35)/* instance_size */
	, sizeof (CylinderTargetBehaviour_t35)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 55/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
// Metadata Definition Vuforia.DataSetLoadBehaviour
extern TypeInfo DataSetLoadBehaviour_t37_il2cpp_TypeInfo;
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern const MethodInfo DataSetLoadBehaviour__ctor_m132_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DataSetLoadBehaviour__ctor_m132/* method */
	, &DataSetLoadBehaviour_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern const MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133_MethodInfo = 
{
	"AddOSSpecificExternalDatasetSearchDirs"/* name */
	, (methodPointerType)&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133/* method */
	, &DataSetLoadBehaviour_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DataSetLoadBehaviour_t37_MethodInfos[] =
{
	&DataSetLoadBehaviour__ctor_m132_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133_MethodInfo,
	NULL
};
extern const MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133_MethodInfo;
static const Il2CppMethodReference DataSetLoadBehaviour_t37_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m133_MethodInfo,
};
static bool DataSetLoadBehaviour_t37_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DataSetLoadBehaviour_t37_0_0_0;
extern const Il2CppType DataSetLoadBehaviour_t37_1_0_0;
extern const Il2CppType DataSetLoadAbstractBehaviour_t38_0_0_0;
struct DataSetLoadBehaviour_t37;
const Il2CppTypeDefinitionMetadata DataSetLoadBehaviour_t37_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DataSetLoadAbstractBehaviour_t38_0_0_0/* parent */
	, DataSetLoadBehaviour_t37_VTable/* vtableMethods */
	, DataSetLoadBehaviour_t37_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DataSetLoadBehaviour_t37_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataSetLoadBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, DataSetLoadBehaviour_t37_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DataSetLoadBehaviour_t37_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DataSetLoadBehaviour_t37_0_0_0/* byval_arg */
	, &DataSetLoadBehaviour_t37_1_0_0/* this_arg */
	, &DataSetLoadBehaviour_t37_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DataSetLoadBehaviour_t37)/* instance_size */
	, sizeof (DataSetLoadBehaviour_t37)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
// Metadata Definition Vuforia.DefaultInitializationErrorHandler
extern TypeInfo DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo;
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern const MethodInfo DefaultInitializationErrorHandler__ctor_m134_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler__ctor_m134/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const MethodInfo DefaultInitializationErrorHandler_Awake_m135_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_Awake_m135/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern const MethodInfo DefaultInitializationErrorHandler_OnGUI_m136_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnGUI_m136/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const MethodInfo DefaultInitializationErrorHandler_OnDestroy_m137_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnDestroy_m137/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_DrawWindowContent_m138_ParameterInfos[] = 
{
	{"id", 0, 134217785, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern const MethodInfo DefaultInitializationErrorHandler_DrawWindowContent_m138_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_DrawWindowContent_m138/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_DrawWindowContent_m138_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InitError_t176_0_0_0;
extern const Il2CppType InitError_t176_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_SetErrorCode_m139_ParameterInfos[] = 
{
	{"errorCode", 0, 134217786, 0, &InitError_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern const MethodInfo DefaultInitializationErrorHandler_SetErrorCode_m139_MethodInfo = 
{
	"SetErrorCode"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorCode_m139/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_SetErrorCode_m139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_SetErrorOccurred_m140_ParameterInfos[] = 
{
	{"errorOccurred", 0, 134217787, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern const MethodInfo DefaultInitializationErrorHandler_SetErrorOccurred_m140_MethodInfo = 
{
	"SetErrorOccurred"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorOccurred_m140/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_SetErrorOccurred_m140_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InitError_t176_0_0_0;
static const ParameterInfo DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_OnQCARInitializationError_m141_ParameterInfos[] = 
{
	{"initError", 0, 134217788, 0, &InitError_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern const MethodInfo DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo = 
{
	"OnQCARInitializationError"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnQCARInitializationError_m141/* method */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, DefaultInitializationErrorHandler_t39_DefaultInitializationErrorHandler_OnQCARInitializationError_m141_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultInitializationErrorHandler_t39_MethodInfos[] =
{
	&DefaultInitializationErrorHandler__ctor_m134_MethodInfo,
	&DefaultInitializationErrorHandler_Awake_m135_MethodInfo,
	&DefaultInitializationErrorHandler_OnGUI_m136_MethodInfo,
	&DefaultInitializationErrorHandler_OnDestroy_m137_MethodInfo,
	&DefaultInitializationErrorHandler_DrawWindowContent_m138_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorCode_m139_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorOccurred_m140_MethodInfo,
	&DefaultInitializationErrorHandler_OnQCARInitializationError_m141_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultInitializationErrorHandler_t39_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool DefaultInitializationErrorHandler_t39_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultInitializationErrorHandler_t39_0_0_0;
extern const Il2CppType DefaultInitializationErrorHandler_t39_1_0_0;
struct DefaultInitializationErrorHandler_t39;
const Il2CppTypeDefinitionMetadata DefaultInitializationErrorHandler_t39_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultInitializationErrorHandler_t39_VTable/* vtableMethods */
	, DefaultInitializationErrorHandler_t39_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 94/* fieldStart */

};
TypeInfo DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultInitializationErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultInitializationErrorHandler_t39_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultInitializationErrorHandler_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultInitializationErrorHandler_t39_0_0_0/* byval_arg */
	, &DefaultInitializationErrorHandler_t39_1_0_0/* this_arg */
	, &DefaultInitializationErrorHandler_t39_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultInitializationErrorHandler_t39)/* instance_size */
	, sizeof (DefaultInitializationErrorHandler_t39)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
// Metadata Definition Vuforia.DefaultSmartTerrainEventHandler
extern TypeInfo DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo;
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern const MethodInfo DefaultSmartTerrainEventHandler__ctor_m142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler__ctor_m142/* method */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern const MethodInfo DefaultSmartTerrainEventHandler_Start_m143_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_Start_m143/* method */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern const MethodInfo DefaultSmartTerrainEventHandler_OnDestroy_m144_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnDestroy_m144/* method */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Prop_t97_0_0_0;
extern const Il2CppType Prop_t97_0_0_0;
static const ParameterInfo DefaultSmartTerrainEventHandler_t43_DefaultSmartTerrainEventHandler_OnPropCreated_m145_ParameterInfos[] = 
{
	{"prop", 0, 134217789, 0, &Prop_t97_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern const MethodInfo DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo = 
{
	"OnPropCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnPropCreated_m145/* method */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t43_DefaultSmartTerrainEventHandler_OnPropCreated_m145_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Surface_t98_0_0_0;
extern const Il2CppType Surface_t98_0_0_0;
static const ParameterInfo DefaultSmartTerrainEventHandler_t43_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_ParameterInfos[] = 
{
	{"surface", 0, 134217790, 0, &Surface_t98_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern const MethodInfo DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146/* method */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t43_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultSmartTerrainEventHandler_t43_MethodInfos[] =
{
	&DefaultSmartTerrainEventHandler__ctor_m142_MethodInfo,
	&DefaultSmartTerrainEventHandler_Start_m143_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnDestroy_m144_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnPropCreated_m145_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultSmartTerrainEventHandler_t43_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool DefaultSmartTerrainEventHandler_t43_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultSmartTerrainEventHandler_t43_0_0_0;
extern const Il2CppType DefaultSmartTerrainEventHandler_t43_1_0_0;
struct DefaultSmartTerrainEventHandler_t43;
const Il2CppTypeDefinitionMetadata DefaultSmartTerrainEventHandler_t43_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultSmartTerrainEventHandler_t43_VTable/* vtableMethods */
	, DefaultSmartTerrainEventHandler_t43_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 97/* fieldStart */

};
TypeInfo DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultSmartTerrainEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultSmartTerrainEventHandler_t43_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultSmartTerrainEventHandler_t43_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultSmartTerrainEventHandler_t43_0_0_0/* byval_arg */
	, &DefaultSmartTerrainEventHandler_t43_1_0_0/* this_arg */
	, &DefaultSmartTerrainEventHandler_t43_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultSmartTerrainEventHandler_t43)/* instance_size */
	, sizeof (DefaultSmartTerrainEventHandler_t43)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
// Metadata Definition Vuforia.DefaultTrackableEventHandler
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern const MethodInfo DefaultTrackableEventHandler__ctor_m147_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler__ctor_m147/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo DefaultTrackableEventHandler_Start_m148_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_Start_m148/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Status_t177_0_0_0;
extern const Il2CppType Status_t177_0_0_0;
extern const Il2CppType Status_t177_0_0_0;
static const ParameterInfo DefaultTrackableEventHandler_t45_DefaultTrackableEventHandler_OnTrackableStateChanged_m149_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217791, 0, &Status_t177_0_0_0},
	{"newStatus", 1, 134217792, 0, &Status_t177_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern const MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m149_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackableStateChanged_m149/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, DefaultTrackableEventHandler_t45_DefaultTrackableEventHandler_OnTrackableStateChanged_m149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern const MethodInfo DefaultTrackableEventHandler_OnTrackingFound_m150_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingFound_m150/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern const MethodInfo DefaultTrackableEventHandler_OnTrackingLost_m151_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingLost_m151/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultTrackableEventHandler_t45_MethodInfos[] =
{
	&DefaultTrackableEventHandler__ctor_m147_MethodInfo,
	&DefaultTrackableEventHandler_Start_m148_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m149_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingFound_m150_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingLost_m151_MethodInfo,
	NULL
};
extern const MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m149_MethodInfo;
static const Il2CppMethodReference DefaultTrackableEventHandler_t45_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m149_MethodInfo,
};
static bool DefaultTrackableEventHandler_t45_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ITrackableEventHandler_t178_0_0_0;
static const Il2CppType* DefaultTrackableEventHandler_t45_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t178_0_0_0,
};
static Il2CppInterfaceOffsetPair DefaultTrackableEventHandler_t45_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t178_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern const Il2CppType DefaultTrackableEventHandler_t45_1_0_0;
struct DefaultTrackableEventHandler_t45;
const Il2CppTypeDefinitionMetadata DefaultTrackableEventHandler_t45_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DefaultTrackableEventHandler_t45_InterfacesTypeInfos/* implementedInterfaces */
	, DefaultTrackableEventHandler_t45_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DefaultTrackableEventHandler_t45_VTable/* vtableMethods */
	, DefaultTrackableEventHandler_t45_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 100/* fieldStart */

};
TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultTrackableEventHandler_t45_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultTrackableEventHandler_t45_0_0_0/* byval_arg */
	, &DefaultTrackableEventHandler_t45_1_0_0/* this_arg */
	, &DefaultTrackableEventHandler_t45_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultTrackableEventHandler_t45)/* instance_size */
	, sizeof (DefaultTrackableEventHandler_t45)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
// Metadata Definition Vuforia.GLErrorHandler
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.ctor()
extern const MethodInfo GLErrorHandler__ctor_m152_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GLErrorHandler__ctor_m152/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.cctor()
extern const MethodInfo GLErrorHandler__cctor_m153_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&GLErrorHandler__cctor_m153/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo GLErrorHandler_t46_GLErrorHandler_SetError_m154_ParameterInfos[] = 
{
	{"errorText", 0, 134217793, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern const MethodInfo GLErrorHandler_SetError_m154_MethodInfo = 
{
	"SetError"/* name */
	, (methodPointerType)&GLErrorHandler_SetError_m154/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, GLErrorHandler_t46_GLErrorHandler_SetError_m154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern const MethodInfo GLErrorHandler_OnGUI_m155_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&GLErrorHandler_OnGUI_m155/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo GLErrorHandler_t46_GLErrorHandler_DrawWindowContent_m156_ParameterInfos[] = 
{
	{"id", 0, 134217794, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern const MethodInfo GLErrorHandler_DrawWindowContent_m156_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&GLErrorHandler_DrawWindowContent_m156/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GLErrorHandler_t46_GLErrorHandler_DrawWindowContent_m156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GLErrorHandler_t46_MethodInfos[] =
{
	&GLErrorHandler__ctor_m152_MethodInfo,
	&GLErrorHandler__cctor_m153_MethodInfo,
	&GLErrorHandler_SetError_m154_MethodInfo,
	&GLErrorHandler_OnGUI_m155_MethodInfo,
	&GLErrorHandler_DrawWindowContent_m156_MethodInfo,
	NULL
};
static const Il2CppMethodReference GLErrorHandler_t46_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool GLErrorHandler_t46_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GLErrorHandler_t46_0_0_0;
extern const Il2CppType GLErrorHandler_t46_1_0_0;
struct GLErrorHandler_t46;
const Il2CppTypeDefinitionMetadata GLErrorHandler_t46_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, GLErrorHandler_t46_VTable/* vtableMethods */
	, GLErrorHandler_t46_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 102/* fieldStart */

};
TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GLErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, GLErrorHandler_t46_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GLErrorHandler_t46_0_0_0/* byval_arg */
	, &GLErrorHandler_t46_1_0_0/* this_arg */
	, &GLErrorHandler_t46_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GLErrorHandler_t46)/* instance_size */
	, sizeof (GLErrorHandler_t46)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GLErrorHandler_t46_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
// Metadata Definition Vuforia.HideExcessAreaBehaviour
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern const MethodInfo HideExcessAreaBehaviour__ctor_m157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideExcessAreaBehaviour__ctor_m157/* method */
	, &HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HideExcessAreaBehaviour_t47_MethodInfos[] =
{
	&HideExcessAreaBehaviour__ctor_m157_MethodInfo,
	NULL
};
static const Il2CppMethodReference HideExcessAreaBehaviour_t47_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool HideExcessAreaBehaviour_t47_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern const Il2CppType HideExcessAreaBehaviour_t47_1_0_0;
extern const Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
struct HideExcessAreaBehaviour_t47;
const Il2CppTypeDefinitionMetadata HideExcessAreaBehaviour_t47_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &HideExcessAreaAbstractBehaviour_t48_0_0_0/* parent */
	, HideExcessAreaBehaviour_t47_VTable/* vtableMethods */
	, HideExcessAreaBehaviour_t47_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideExcessAreaBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, HideExcessAreaBehaviour_t47_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideExcessAreaBehaviour_t47_0_0_0/* byval_arg */
	, &HideExcessAreaBehaviour_t47_1_0_0/* this_arg */
	, &HideExcessAreaBehaviour_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideExcessAreaBehaviour_t47)/* instance_size */
	, sizeof (HideExcessAreaBehaviour_t47)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
// Metadata Definition Vuforia.ImageTargetBehaviour
extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern const MethodInfo ImageTargetBehaviour__ctor_m158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ImageTargetBehaviour__ctor_m158/* method */
	, &ImageTargetBehaviour_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ImageTargetBehaviour_t49_MethodInfos[] =
{
	&ImageTargetBehaviour__ctor_m158_MethodInfo,
	NULL
};
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m593_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m594_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m595_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m596_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m598_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_CorrectScaleImpl_m599_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m600_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m601_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m602_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m603_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m604_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m605_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_GetSize_m606_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_SetWidth_m607_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_SetHeight_m608_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m609_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m610_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m611_MethodInfo;
extern const MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m612_MethodInfo;
static const Il2CppMethodReference ImageTargetBehaviour_t49_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m593_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m594_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m595_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m596_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m560_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m598_MethodInfo,
	&ImageTargetAbstractBehaviour_CorrectScaleImpl_m599_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m588_MethodInfo,
	&ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m600_MethodInfo,
	&ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m601_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m602_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m603_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m604_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m605_MethodInfo,
	&ImageTargetAbstractBehaviour_GetSize_m606_MethodInfo,
	&ImageTargetAbstractBehaviour_SetWidth_m607_MethodInfo,
	&ImageTargetAbstractBehaviour_SetHeight_m608_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m609_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m610_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m611_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m612_MethodInfo,
};
static bool ImageTargetBehaviour_t49_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorImageTargetBehaviour_t179_0_0_0;
static Il2CppInterfaceOffsetPair ImageTargetBehaviour_t49_InterfacesOffsets[] = 
{
	{ &IEditorImageTargetBehaviour_t179_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t173_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern const Il2CppType ImageTargetBehaviour_t49_1_0_0;
extern const Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
struct ImageTargetBehaviour_t49;
const Il2CppTypeDefinitionMetadata ImageTargetBehaviour_t49_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ImageTargetBehaviour_t49_InterfacesOffsets/* interfaceOffsets */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* parent */
	, ImageTargetBehaviour_t49_VTable/* vtableMethods */
	, ImageTargetBehaviour_t49_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ImageTargetBehaviour_t49_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ImageTargetBehaviour_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ImageTargetBehaviour_t49_0_0_0/* byval_arg */
	, &ImageTargetBehaviour_t49_1_0_0/* this_arg */
	, &ImageTargetBehaviour_t49_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImageTargetBehaviour_t49)/* instance_size */
	, sizeof (ImageTargetBehaviour_t49)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 64/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
// Metadata Definition Vuforia.AndroidUnityPlayer
extern TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo;
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern const MethodInfo AndroidUnityPlayer__ctor_m159_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AndroidUnityPlayer__ctor_m159/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m160_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibraries_m160/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern const MethodInfo AndroidUnityPlayer_InitializePlatform_m161_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializePlatform_m161/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AndroidUnityPlayer_t51_AndroidUnityPlayer_Start_m162_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217795, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_InitError_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern const MethodInfo AndroidUnityPlayer_Start_m162_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Start_m162/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t176_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t176_Object_t/* invoker_method */
	, AndroidUnityPlayer_t51_AndroidUnityPlayer_Start_m162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern const MethodInfo AndroidUnityPlayer_Update_m163_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Update_m163/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern const MethodInfo AndroidUnityPlayer_OnPause_m164_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnPause_m164/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern const MethodInfo AndroidUnityPlayer_OnResume_m165_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnResume_m165/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern const MethodInfo AndroidUnityPlayer_OnDestroy_m166_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnDestroy_m166/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern const MethodInfo AndroidUnityPlayer_Dispose_m167_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Dispose_m167/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibrariesFromJava_m168_MethodInfo = 
{
	"LoadNativeLibrariesFromJava"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m168/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern const MethodInfo AndroidUnityPlayer_InitAndroidPlatform_m169_MethodInfo = 
{
	"InitAndroidPlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitAndroidPlatform_m169/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AndroidUnityPlayer_t51_AndroidUnityPlayer_InitQCAR_m170_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217796, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
extern const MethodInfo AndroidUnityPlayer_InitQCAR_m170_MethodInfo = 
{
	"InitQCAR"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitQCAR_m170/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, AndroidUnityPlayer_t51_AndroidUnityPlayer_InitQCAR_m170_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern const MethodInfo AndroidUnityPlayer_InitializeSurface_m171_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializeSurface_m171/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern const MethodInfo AndroidUnityPlayer_ResetUnityScreenOrientation_m172_MethodInfo = 
{
	"ResetUnityScreenOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_ResetUnityScreenOrientation_m172/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern const MethodInfo AndroidUnityPlayer_CheckOrientation_m173_MethodInfo = 
{
	"CheckOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_CheckOrientation_m173/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AndroidUnityPlayer_t51_MethodInfos[] =
{
	&AndroidUnityPlayer__ctor_m159_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m160_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m161_MethodInfo,
	&AndroidUnityPlayer_Start_m162_MethodInfo,
	&AndroidUnityPlayer_Update_m163_MethodInfo,
	&AndroidUnityPlayer_OnPause_m164_MethodInfo,
	&AndroidUnityPlayer_OnResume_m165_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m166_MethodInfo,
	&AndroidUnityPlayer_Dispose_m167_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m168_MethodInfo,
	&AndroidUnityPlayer_InitAndroidPlatform_m169_MethodInfo,
	&AndroidUnityPlayer_InitQCAR_m170_MethodInfo,
	&AndroidUnityPlayer_InitializeSurface_m171_MethodInfo,
	&AndroidUnityPlayer_ResetUnityScreenOrientation_m172_MethodInfo,
	&AndroidUnityPlayer_CheckOrientation_m173_MethodInfo,
	NULL
};
extern const MethodInfo AndroidUnityPlayer_Dispose_m167_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m160_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_InitializePlatform_m161_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_Start_m162_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_Update_m163_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnPause_m164_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnResume_m165_MethodInfo;
extern const MethodInfo AndroidUnityPlayer_OnDestroy_m166_MethodInfo;
static const Il2CppMethodReference AndroidUnityPlayer_t51_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&AndroidUnityPlayer_Dispose_m167_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m160_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m161_MethodInfo,
	&AndroidUnityPlayer_Start_m162_MethodInfo,
	&AndroidUnityPlayer_Update_m163_MethodInfo,
	&AndroidUnityPlayer_OnPause_m164_MethodInfo,
	&AndroidUnityPlayer_OnResume_m165_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m166_MethodInfo,
};
static bool AndroidUnityPlayer_t51_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t144_0_0_0;
extern const Il2CppType IUnityPlayer_t180_0_0_0;
static const Il2CppType* AndroidUnityPlayer_t51_InterfacesTypeInfos[] = 
{
	&IDisposable_t144_0_0_0,
	&IUnityPlayer_t180_0_0_0,
};
static Il2CppInterfaceOffsetPair AndroidUnityPlayer_t51_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
	{ &IUnityPlayer_t180_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AndroidUnityPlayer_t51_0_0_0;
extern const Il2CppType AndroidUnityPlayer_t51_1_0_0;
struct AndroidUnityPlayer_t51;
const Il2CppTypeDefinitionMetadata AndroidUnityPlayer_t51_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AndroidUnityPlayer_t51_InterfacesTypeInfos/* implementedInterfaces */
	, AndroidUnityPlayer_t51_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AndroidUnityPlayer_t51_VTable/* vtableMethods */
	, AndroidUnityPlayer_t51_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 105/* fieldStart */

};
TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AndroidUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, AndroidUnityPlayer_t51_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AndroidUnityPlayer_t51_0_0_0/* byval_arg */
	, &AndroidUnityPlayer_t51_1_0_0/* this_arg */
	, &AndroidUnityPlayer_t51_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AndroidUnityPlayer_t51)/* instance_size */
	, sizeof (AndroidUnityPlayer_t51)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
// Metadata Definition Vuforia.ComponentFactoryStarterBehaviour
extern TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo;
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern const MethodInfo ComponentFactoryStarterBehaviour__ctor_m174_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour__ctor_m174/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const MethodInfo ComponentFactoryStarterBehaviour_Awake_m175_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_Awake_m175/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern const MethodInfo ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176_MethodInfo = 
{
	"SetBehaviourComponentFactory"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 7/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentFactoryStarterBehaviour_t52_MethodInfos[] =
{
	&ComponentFactoryStarterBehaviour__ctor_m174_MethodInfo,
	&ComponentFactoryStarterBehaviour_Awake_m175_MethodInfo,
	&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentFactoryStarterBehaviour_t52_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool ComponentFactoryStarterBehaviour_t52_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ComponentFactoryStarterBehaviour_t52_0_0_0;
extern const Il2CppType ComponentFactoryStarterBehaviour_t52_1_0_0;
struct ComponentFactoryStarterBehaviour_t52;
const Il2CppTypeDefinitionMetadata ComponentFactoryStarterBehaviour_t52_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, ComponentFactoryStarterBehaviour_t52_VTable/* vtableMethods */
	, ComponentFactoryStarterBehaviour_t52_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentFactoryStarterBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ComponentFactoryStarterBehaviour_t52_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentFactoryStarterBehaviour_t52_0_0_0/* byval_arg */
	, &ComponentFactoryStarterBehaviour_t52_1_0_0/* this_arg */
	, &ComponentFactoryStarterBehaviour_t52_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentFactoryStarterBehaviour_t52)/* instance_size */
	, sizeof (ComponentFactoryStarterBehaviour_t52)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
// Metadata Definition Vuforia.IOSUnityPlayer
extern TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo;
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern const MethodInfo IOSUnityPlayer__ctor_m177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IOSUnityPlayer__ctor_m177/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern const MethodInfo IOSUnityPlayer_LoadNativeLibraries_m178_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&IOSUnityPlayer_LoadNativeLibraries_m178/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern const MethodInfo IOSUnityPlayer_InitializePlatform_m179_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializePlatform_m179/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_Start_m180_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217797, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_InitError_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern const MethodInfo IOSUnityPlayer_Start_m180_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&IOSUnityPlayer_Start_m180/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t176_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t176_Object_t/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_Start_m180_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Update()
extern const MethodInfo IOSUnityPlayer_Update_m181_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&IOSUnityPlayer_Update_m181/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern const MethodInfo IOSUnityPlayer_Dispose_m182_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&IOSUnityPlayer_Dispose_m182/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern const MethodInfo IOSUnityPlayer_OnPause_m183_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnPause_m183/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern const MethodInfo IOSUnityPlayer_OnResume_m184_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnResume_m184/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern const MethodInfo IOSUnityPlayer_OnDestroy_m185_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnDestroy_m185/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern const MethodInfo IOSUnityPlayer_InitializeSurface_m186_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializeSurface_m186/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern const MethodInfo IOSUnityPlayer_SetUnityScreenOrientation_m187_MethodInfo = 
{
	"SetUnityScreenOrientation"/* name */
	, (methodPointerType)&IOSUnityPlayer_SetUnityScreenOrientation_m187/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern const MethodInfo IOSUnityPlayer_setPlatFormNative_m188_MethodInfo = 
{
	"setPlatFormNative"/* name */
	, (methodPointerType)&IOSUnityPlayer_setPlatFormNative_m188/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_initQCARiOS_m189_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217798, 0, &Int32_t127_0_0_0},
	{"licenseKey", 1, 134217799, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern const MethodInfo IOSUnityPlayer_initQCARiOS_m189_MethodInfo = 
{
	"initQCARiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_initQCARiOS_m189/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127_Object_t/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_initQCARiOS_m189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_setSurfaceOrientationiOS_m190_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217800, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern const MethodInfo IOSUnityPlayer_setSurfaceOrientationiOS_m190_MethodInfo = 
{
	"setSurfaceOrientationiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_setSurfaceOrientationiOS_m190/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_setSurfaceOrientationiOS_m190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IOSUnityPlayer_t53_MethodInfos[] =
{
	&IOSUnityPlayer__ctor_m177_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m178_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m179_MethodInfo,
	&IOSUnityPlayer_Start_m180_MethodInfo,
	&IOSUnityPlayer_Update_m181_MethodInfo,
	&IOSUnityPlayer_Dispose_m182_MethodInfo,
	&IOSUnityPlayer_OnPause_m183_MethodInfo,
	&IOSUnityPlayer_OnResume_m184_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m185_MethodInfo,
	&IOSUnityPlayer_InitializeSurface_m186_MethodInfo,
	&IOSUnityPlayer_SetUnityScreenOrientation_m187_MethodInfo,
	&IOSUnityPlayer_setPlatFormNative_m188_MethodInfo,
	&IOSUnityPlayer_initQCARiOS_m189_MethodInfo,
	&IOSUnityPlayer_setSurfaceOrientationiOS_m190_MethodInfo,
	NULL
};
extern const MethodInfo IOSUnityPlayer_Dispose_m182_MethodInfo;
extern const MethodInfo IOSUnityPlayer_LoadNativeLibraries_m178_MethodInfo;
extern const MethodInfo IOSUnityPlayer_InitializePlatform_m179_MethodInfo;
extern const MethodInfo IOSUnityPlayer_Start_m180_MethodInfo;
extern const MethodInfo IOSUnityPlayer_Update_m181_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnPause_m183_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnResume_m184_MethodInfo;
extern const MethodInfo IOSUnityPlayer_OnDestroy_m185_MethodInfo;
static const Il2CppMethodReference IOSUnityPlayer_t53_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&IOSUnityPlayer_Dispose_m182_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m178_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m179_MethodInfo,
	&IOSUnityPlayer_Start_m180_MethodInfo,
	&IOSUnityPlayer_Update_m181_MethodInfo,
	&IOSUnityPlayer_OnPause_m183_MethodInfo,
	&IOSUnityPlayer_OnResume_m184_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m185_MethodInfo,
};
static bool IOSUnityPlayer_t53_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* IOSUnityPlayer_t53_InterfacesTypeInfos[] = 
{
	&IDisposable_t144_0_0_0,
	&IUnityPlayer_t180_0_0_0,
};
static Il2CppInterfaceOffsetPair IOSUnityPlayer_t53_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
	{ &IUnityPlayer_t180_0_0_0, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType IOSUnityPlayer_t53_0_0_0;
extern const Il2CppType IOSUnityPlayer_t53_1_0_0;
struct IOSUnityPlayer_t53;
const Il2CppTypeDefinitionMetadata IOSUnityPlayer_t53_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IOSUnityPlayer_t53_InterfacesTypeInfos/* implementedInterfaces */
	, IOSUnityPlayer_t53_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IOSUnityPlayer_t53_VTable/* vtableMethods */
	, IOSUnityPlayer_t53_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 111/* fieldStart */

};
TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "IOSUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, IOSUnityPlayer_t53_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IOSUnityPlayer_t53_0_0_0/* byval_arg */
	, &IOSUnityPlayer_t53_1_0_0/* this_arg */
	, &IOSUnityPlayer_t53_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IOSUnityPlayer_t53)/* instance_size */
	, sizeof (IOSUnityPlayer_t53)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
// Metadata Definition Vuforia.VuforiaBehaviourComponentFactory
extern TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo;
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern const MethodInfo VuforiaBehaviourComponentFactory__ctor_m191_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory__ctor_m191/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_ParameterInfos[] = 
{
	{"gameObject", 0, 134217801, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MaskOutAbstractBehaviour_t60_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_MethodInfo = 
{
	"AddMaskOutBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t60_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_ParameterInfos[] = 
{
	{"gameObject", 0, 134217802, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType VirtualButtonAbstractBehaviour_t86_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_MethodInfo = 
{
	"AddVirtualButtonBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_ParameterInfos[] = 
{
	{"gameObject", 0, 134217803, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType TurnOffAbstractBehaviour_t77_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_MethodInfo = 
{
	"AddTurnOffBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_ParameterInfos[] = 
{
	{"gameObject", 0, 134217804, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_MethodInfo = 
{
	"AddImageTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_ParameterInfos[] = 
{
	{"gameObject", 0, 134217805, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MarkerAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_MethodInfo = 
{
	"AddMarkerBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_ParameterInfos[] = 
{
	{"gameObject", 0, 134217806, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType MultiTargetAbstractBehaviour_t62_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_MethodInfo = 
{
	"AddMultiTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t62_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_ParameterInfos[] = 
{
	{"gameObject", 0, 134217807, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_MethodInfo = 
{
	"AddCylinderTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_ParameterInfos[] = 
{
	{"gameObject", 0, 134217808, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType WordAbstractBehaviour_t93_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_MethodInfo = 
{
	"AddWordBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddWordBehaviour_m199/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t93_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_ParameterInfos[] = 
{
	{"gameObject", 0, 134217809, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType TextRecoAbstractBehaviour_t75_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_MethodInfo = 
{
	"AddTextRecoBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_ParameterInfos[] = 
{
	{"gameObject", 0, 134217810, 0, &GameObject_t2_0_0_0},
};
extern const Il2CppType ObjectTargetAbstractBehaviour_t64_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_MethodInfo = 
{
	"AddObjectTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t64_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VuforiaBehaviourComponentFactory_t54_MethodInfos[] =
{
	&VuforiaBehaviourComponentFactory__ctor_m191_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_MethodInfo,
	NULL
};
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_MethodInfo;
extern const MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_MethodInfo;
static const Il2CppMethodReference VuforiaBehaviourComponentFactory_t54_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m192_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m193_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m194_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m195_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m196_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m197_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m198_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m199_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m200_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m201_MethodInfo,
};
static bool VuforiaBehaviourComponentFactory_t54_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IBehaviourComponentFactory_t181_0_0_0;
static const Il2CppType* VuforiaBehaviourComponentFactory_t54_InterfacesTypeInfos[] = 
{
	&IBehaviourComponentFactory_t181_0_0_0,
};
static Il2CppInterfaceOffsetPair VuforiaBehaviourComponentFactory_t54_InterfacesOffsets[] = 
{
	{ &IBehaviourComponentFactory_t181_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VuforiaBehaviourComponentFactory_t54_0_0_0;
extern const Il2CppType VuforiaBehaviourComponentFactory_t54_1_0_0;
struct VuforiaBehaviourComponentFactory_t54;
const Il2CppTypeDefinitionMetadata VuforiaBehaviourComponentFactory_t54_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, VuforiaBehaviourComponentFactory_t54_InterfacesTypeInfos/* implementedInterfaces */
	, VuforiaBehaviourComponentFactory_t54_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, VuforiaBehaviourComponentFactory_t54_VTable/* vtableMethods */
	, VuforiaBehaviourComponentFactory_t54_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VuforiaBehaviourComponentFactory"/* name */
	, "Vuforia"/* namespaze */
	, VuforiaBehaviourComponentFactory_t54_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VuforiaBehaviourComponentFactory_t54_0_0_0/* byval_arg */
	, &VuforiaBehaviourComponentFactory_t54_1_0_0/* this_arg */
	, &VuforiaBehaviourComponentFactory_t54_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VuforiaBehaviourComponentFactory_t54)/* instance_size */
	, sizeof (VuforiaBehaviourComponentFactory_t54)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
// Metadata Definition Vuforia.KeepAliveBehaviour
extern TypeInfo KeepAliveBehaviour_t55_il2cpp_TypeInfo;
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern const MethodInfo KeepAliveBehaviour__ctor_m202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&KeepAliveBehaviour__ctor_m202/* method */
	, &KeepAliveBehaviour_t55_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* KeepAliveBehaviour_t55_MethodInfos[] =
{
	&KeepAliveBehaviour__ctor_m202_MethodInfo,
	NULL
};
static const Il2CppMethodReference KeepAliveBehaviour_t55_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool KeepAliveBehaviour_t55_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType KeepAliveBehaviour_t55_0_0_0;
extern const Il2CppType KeepAliveBehaviour_t55_1_0_0;
extern const Il2CppType KeepAliveAbstractBehaviour_t56_0_0_0;
struct KeepAliveBehaviour_t55;
const Il2CppTypeDefinitionMetadata KeepAliveBehaviour_t55_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &KeepAliveAbstractBehaviour_t56_0_0_0/* parent */
	, KeepAliveBehaviour_t55_VTable/* vtableMethods */
	, KeepAliveBehaviour_t55_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo KeepAliveBehaviour_t55_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeepAliveBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, KeepAliveBehaviour_t55_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &KeepAliveBehaviour_t55_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 8/* custom_attributes_cache */
	, &KeepAliveBehaviour_t55_0_0_0/* byval_arg */
	, &KeepAliveBehaviour_t55_1_0_0/* this_arg */
	, &KeepAliveBehaviour_t55_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeepAliveBehaviour_t55)/* instance_size */
	, sizeof (KeepAliveBehaviour_t55)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Metadata Definition Vuforia.MarkerBehaviour
extern TypeInfo MarkerBehaviour_t57_il2cpp_TypeInfo;
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MarkerBehaviour::.ctor()
extern const MethodInfo MarkerBehaviour__ctor_m203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarkerBehaviour__ctor_m203/* method */
	, &MarkerBehaviour_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MarkerBehaviour_t57_MethodInfos[] =
{
	&MarkerBehaviour__ctor_m203_MethodInfo,
	NULL
};
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m613_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m614_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m615_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m616_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnTrackerUpdate_m617_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_InternalUnregisterTrackable_m618_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_CorrectScaleImpl_m619_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m620_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m621_MethodInfo;
extern const MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m622_MethodInfo;
static const Il2CppMethodReference MarkerBehaviour_t57_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m613_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m614_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m615_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m616_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m617_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&MarkerAbstractBehaviour_InternalUnregisterTrackable_m618_MethodInfo,
	&MarkerAbstractBehaviour_CorrectScaleImpl_m619_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m620_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m621_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m622_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
};
static bool MarkerBehaviour_t57_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorMarkerBehaviour_t182_0_0_0;
static Il2CppInterfaceOffsetPair MarkerBehaviour_t57_InterfacesOffsets[] = 
{
	{ &IEditorMarkerBehaviour_t182_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 28},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MarkerBehaviour_t57_0_0_0;
extern const Il2CppType MarkerBehaviour_t57_1_0_0;
struct MarkerBehaviour_t57;
const Il2CppTypeDefinitionMetadata MarkerBehaviour_t57_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarkerBehaviour_t57_InterfacesOffsets/* interfaceOffsets */
	, &MarkerAbstractBehaviour_t58_0_0_0/* parent */
	, MarkerBehaviour_t57_VTable/* vtableMethods */
	, MarkerBehaviour_t57_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MarkerBehaviour_t57_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarkerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MarkerBehaviour_t57_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MarkerBehaviour_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MarkerBehaviour_t57_0_0_0/* byval_arg */
	, &MarkerBehaviour_t57_1_0_0/* this_arg */
	, &MarkerBehaviour_t57_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarkerBehaviour_t57)/* instance_size */
	, sizeof (MarkerBehaviour_t57)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Metadata Definition Vuforia.MaskOutBehaviour
extern TypeInfo MaskOutBehaviour_t59_il2cpp_TypeInfo;
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern const MethodInfo MaskOutBehaviour__ctor_m204_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MaskOutBehaviour__ctor_m204/* method */
	, &MaskOutBehaviour_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::Start()
extern const MethodInfo MaskOutBehaviour_Start_m205_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&MaskOutBehaviour_Start_m205/* method */
	, &MaskOutBehaviour_t59_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MaskOutBehaviour_t59_MethodInfos[] =
{
	&MaskOutBehaviour__ctor_m204_MethodInfo,
	&MaskOutBehaviour_Start_m205_MethodInfo,
	NULL
};
static const Il2CppMethodReference MaskOutBehaviour_t59_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool MaskOutBehaviour_t59_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MaskOutBehaviour_t59_0_0_0;
extern const Il2CppType MaskOutBehaviour_t59_1_0_0;
struct MaskOutBehaviour_t59;
const Il2CppTypeDefinitionMetadata MaskOutBehaviour_t59_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MaskOutAbstractBehaviour_t60_0_0_0/* parent */
	, MaskOutBehaviour_t59_VTable/* vtableMethods */
	, MaskOutBehaviour_t59_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MaskOutBehaviour_t59_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MaskOutBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MaskOutBehaviour_t59_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MaskOutBehaviour_t59_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MaskOutBehaviour_t59_0_0_0/* byval_arg */
	, &MaskOutBehaviour_t59_1_0_0/* this_arg */
	, &MaskOutBehaviour_t59_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MaskOutBehaviour_t59)/* instance_size */
	, sizeof (MaskOutBehaviour_t59)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Metadata Definition Vuforia.MultiTargetBehaviour
extern TypeInfo MultiTargetBehaviour_t61_il2cpp_TypeInfo;
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern const MethodInfo MultiTargetBehaviour__ctor_m206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MultiTargetBehaviour__ctor_m206/* method */
	, &MultiTargetBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MultiTargetBehaviour_t61_MethodInfos[] =
{
	&MultiTargetBehaviour__ctor_m206_MethodInfo,
	NULL
};
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m623_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m624_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m625_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m626_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m627_MethodInfo;
extern const MethodInfo TrackableBehaviour_CorrectScaleImpl_m628_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m629_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m630_MethodInfo;
extern const MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m631_MethodInfo;
static const Il2CppMethodReference MultiTargetBehaviour_t61_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m623_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m624_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m625_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m626_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m560_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m627_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m628_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m588_MethodInfo,
	&MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m629_MethodInfo,
	&MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m630_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m631_MethodInfo,
};
static bool MultiTargetBehaviour_t61_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorMultiTargetBehaviour_t183_0_0_0;
static Il2CppInterfaceOffsetPair MultiTargetBehaviour_t61_InterfacesOffsets[] = 
{
	{ &IEditorMultiTargetBehaviour_t183_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t173_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MultiTargetBehaviour_t61_0_0_0;
extern const Il2CppType MultiTargetBehaviour_t61_1_0_0;
struct MultiTargetBehaviour_t61;
const Il2CppTypeDefinitionMetadata MultiTargetBehaviour_t61_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MultiTargetBehaviour_t61_InterfacesOffsets/* interfaceOffsets */
	, &MultiTargetAbstractBehaviour_t62_0_0_0/* parent */
	, MultiTargetBehaviour_t61_VTable/* vtableMethods */
	, MultiTargetBehaviour_t61_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MultiTargetBehaviour_t61_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MultiTargetBehaviour_t61_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MultiTargetBehaviour_t61_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MultiTargetBehaviour_t61_0_0_0/* byval_arg */
	, &MultiTargetBehaviour_t61_1_0_0/* this_arg */
	, &MultiTargetBehaviour_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiTargetBehaviour_t61)/* instance_size */
	, sizeof (MultiTargetBehaviour_t61)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 54/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
// Metadata Definition Vuforia.ObjectTargetBehaviour
extern TypeInfo ObjectTargetBehaviour_t63_il2cpp_TypeInfo;
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern const MethodInfo ObjectTargetBehaviour__ctor_m207_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectTargetBehaviour__ctor_m207/* method */
	, &ObjectTargetBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectTargetBehaviour_t63_MethodInfos[] =
{
	&ObjectTargetBehaviour__ctor_m207_MethodInfo,
	NULL
};
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m632_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m633_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m634_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m635_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m636_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_CorrectScaleImpl_m637_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m638_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m639_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m640_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m641_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m642_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m643_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m644_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_GetSize_m645_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetLength_m646_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetWidth_m647_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetHeight_m648_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m649_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m650_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_SetBoundingBox_m651_MethodInfo;
extern const MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m652_MethodInfo;
static const Il2CppMethodReference ObjectTargetBehaviour_t63_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m632_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m633_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m634_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m635_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m560_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m636_MethodInfo,
	&ObjectTargetAbstractBehaviour_CorrectScaleImpl_m637_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m588_MethodInfo,
	&ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m638_MethodInfo,
	&ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m639_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m640_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m641_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m642_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m643_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m644_MethodInfo,
	&ObjectTargetAbstractBehaviour_GetSize_m645_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetLength_m646_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetWidth_m647_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetHeight_m648_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m649_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m650_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetBoundingBox_m651_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m652_MethodInfo,
};
static bool ObjectTargetBehaviour_t63_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorObjectTargetBehaviour_t184_0_0_0;
static Il2CppInterfaceOffsetPair ObjectTargetBehaviour_t63_InterfacesOffsets[] = 
{
	{ &IEditorObjectTargetBehaviour_t184_0_0_0, 53},
	{ &IEditorDataSetTrackableBehaviour_t173_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ObjectTargetBehaviour_t63_0_0_0;
extern const Il2CppType ObjectTargetBehaviour_t63_1_0_0;
struct ObjectTargetBehaviour_t63;
const Il2CppTypeDefinitionMetadata ObjectTargetBehaviour_t63_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectTargetBehaviour_t63_InterfacesOffsets/* interfaceOffsets */
	, &ObjectTargetAbstractBehaviour_t64_0_0_0/* parent */
	, ObjectTargetBehaviour_t63_VTable/* vtableMethods */
	, ObjectTargetBehaviour_t63_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ObjectTargetBehaviour_t63_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ObjectTargetBehaviour_t63_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjectTargetBehaviour_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectTargetBehaviour_t63_0_0_0/* byval_arg */
	, &ObjectTargetBehaviour_t63_1_0_0/* this_arg */
	, &ObjectTargetBehaviour_t63_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectTargetBehaviour_t63)/* instance_size */
	, sizeof (ObjectTargetBehaviour_t63)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 66/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Metadata Definition Vuforia.PropBehaviour
extern TypeInfo PropBehaviour_t41_il2cpp_TypeInfo;
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.PropBehaviour::.ctor()
extern const MethodInfo PropBehaviour__ctor_m208_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropBehaviour__ctor_m208/* method */
	, &PropBehaviour_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropBehaviour_t41_MethodInfos[] =
{
	&PropBehaviour__ctor_m208_MethodInfo,
	NULL
};
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m653_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m654_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m655_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m656_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_InternalUnregisterTrackable_m657_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_UpdateMeshAndColliders_m658_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Start_m659_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m660_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m661_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m662_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m663_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m664_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m665_MethodInfo;
extern const MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m666_MethodInfo;
static const Il2CppMethodReference PropBehaviour_t41_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m653_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m654_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m655_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m656_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m617_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&PropAbstractBehaviour_InternalUnregisterTrackable_m657_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m628_MethodInfo,
	&PropAbstractBehaviour_UpdateMeshAndColliders_m658_MethodInfo,
	&PropAbstractBehaviour_Start_m659_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m660_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m661_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m662_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m663_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m664_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m665_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m666_MethodInfo,
};
static bool PropBehaviour_t41_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorPropBehaviour_t185_0_0_0;
static Il2CppInterfaceOffsetPair PropBehaviour_t41_InterfacesOffsets[] = 
{
	{ &IEditorPropBehaviour_t185_0_0_0, 27},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PropBehaviour_t41_0_0_0;
extern const Il2CppType PropBehaviour_t41_1_0_0;
extern const Il2CppType PropAbstractBehaviour_t65_0_0_0;
struct PropBehaviour_t41;
const Il2CppTypeDefinitionMetadata PropBehaviour_t41_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropBehaviour_t41_InterfacesOffsets/* interfaceOffsets */
	, &PropAbstractBehaviour_t65_0_0_0/* parent */
	, PropBehaviour_t41_VTable/* vtableMethods */
	, PropBehaviour_t41_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropBehaviour_t41_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, PropBehaviour_t41_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropBehaviour_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PropBehaviour_t41_0_0_0/* byval_arg */
	, &PropBehaviour_t41_1_0_0/* this_arg */
	, &PropBehaviour_t41_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropBehaviour_t41)/* instance_size */
	, sizeof (PropBehaviour_t41)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
// Metadata Definition Vuforia.QCARBehaviour
extern TypeInfo QCARBehaviour_t66_il2cpp_TypeInfo;
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::.ctor()
extern const MethodInfo QCARBehaviour__ctor_m209_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARBehaviour__ctor_m209/* method */
	, &QCARBehaviour_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::Awake()
extern const MethodInfo QCARBehaviour_Awake_m210_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&QCARBehaviour_Awake_m210/* method */
	, &QCARBehaviour_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QCARBehaviour_t66_MethodInfos[] =
{
	&QCARBehaviour__ctor_m209_MethodInfo,
	&QCARBehaviour_Awake_m210_MethodInfo,
	NULL
};
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m667_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m668_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m669_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m670_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m671_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m672_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m673_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m674_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m675_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m676_MethodInfo;
extern const MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m677_MethodInfo;
static const Il2CppMethodReference QCARBehaviour_t66_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m667_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m668_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m669_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m670_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m671_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m672_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m673_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m674_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m675_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m676_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m677_MethodInfo,
};
static bool QCARBehaviour_t66_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorQCARBehaviour_t186_0_0_0;
static Il2CppInterfaceOffsetPair QCARBehaviour_t66_InterfacesOffsets[] = 
{
	{ &IEditorQCARBehaviour_t186_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType QCARBehaviour_t66_0_0_0;
extern const Il2CppType QCARBehaviour_t66_1_0_0;
extern const Il2CppType QCARAbstractBehaviour_t67_0_0_0;
struct QCARBehaviour_t66;
const Il2CppTypeDefinitionMetadata QCARBehaviour_t66_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, QCARBehaviour_t66_InterfacesOffsets/* interfaceOffsets */
	, &QCARAbstractBehaviour_t67_0_0_0/* parent */
	, QCARBehaviour_t66_VTable/* vtableMethods */
	, QCARBehaviour_t66_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo QCARBehaviour_t66_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, QCARBehaviour_t66_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QCARBehaviour_t66_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QCARBehaviour_t66_0_0_0/* byval_arg */
	, &QCARBehaviour_t66_1_0_0/* this_arg */
	, &QCARBehaviour_t66_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARBehaviour_t66)/* instance_size */
	, sizeof (QCARBehaviour_t66)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// Metadata Definition Vuforia.ReconstructionBehaviour
extern TypeInfo ReconstructionBehaviour_t40_il2cpp_TypeInfo;
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern const MethodInfo ReconstructionBehaviour__ctor_m211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionBehaviour__ctor_m211/* method */
	, &ReconstructionBehaviour_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReconstructionBehaviour_t40_MethodInfos[] =
{
	&ReconstructionBehaviour__ctor_m211_MethodInfo,
	NULL
};
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m678_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m679_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m680_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m681_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m682_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m683_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m684_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m685_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m686_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m687_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m688_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m689_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m690_MethodInfo;
extern const MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m691_MethodInfo;
static const Il2CppMethodReference ReconstructionBehaviour_t40_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m678_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m679_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m680_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m681_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m682_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m683_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m684_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m685_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m686_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m687_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m688_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m689_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m690_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m691_MethodInfo,
};
static bool ReconstructionBehaviour_t40_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorReconstructionBehaviour_t187_0_0_0;
static Il2CppInterfaceOffsetPair ReconstructionBehaviour_t40_InterfacesOffsets[] = 
{
	{ &IEditorReconstructionBehaviour_t187_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReconstructionBehaviour_t40_0_0_0;
extern const Il2CppType ReconstructionBehaviour_t40_1_0_0;
extern const Il2CppType ReconstructionAbstractBehaviour_t68_0_0_0;
struct ReconstructionBehaviour_t40;
const Il2CppTypeDefinitionMetadata ReconstructionBehaviour_t40_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReconstructionBehaviour_t40_InterfacesOffsets/* interfaceOffsets */
	, &ReconstructionAbstractBehaviour_t68_0_0_0/* parent */
	, ReconstructionBehaviour_t40_VTable/* vtableMethods */
	, ReconstructionBehaviour_t40_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ReconstructionBehaviour_t40_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionBehaviour_t40_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReconstructionBehaviour_t40_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReconstructionBehaviour_t40_0_0_0/* byval_arg */
	, &ReconstructionBehaviour_t40_1_0_0/* this_arg */
	, &ReconstructionBehaviour_t40_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionBehaviour_t40)/* instance_size */
	, sizeof (ReconstructionBehaviour_t40)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
// Metadata Definition Vuforia.ReconstructionFromTargetBehaviour
extern TypeInfo ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo;
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern const MethodInfo ReconstructionFromTargetBehaviour__ctor_m212_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionFromTargetBehaviour__ctor_m212/* method */
	, &ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReconstructionFromTargetBehaviour_t69_MethodInfos[] =
{
	&ReconstructionFromTargetBehaviour__ctor_m212_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReconstructionFromTargetBehaviour_t69_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool ReconstructionFromTargetBehaviour_t69_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ReconstructionFromTargetBehaviour_t69_0_0_0;
extern const Il2CppType ReconstructionFromTargetBehaviour_t69_1_0_0;
extern const Il2CppType ReconstructionFromTargetAbstractBehaviour_t70_0_0_0;
struct ReconstructionFromTargetBehaviour_t69;
const Il2CppTypeDefinitionMetadata ReconstructionFromTargetBehaviour_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ReconstructionFromTargetAbstractBehaviour_t70_0_0_0/* parent */
	, ReconstructionFromTargetBehaviour_t69_VTable/* vtableMethods */
	, ReconstructionFromTargetBehaviour_t69_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionFromTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionFromTargetBehaviour_t69_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReconstructionFromTargetBehaviour_t69_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReconstructionFromTargetBehaviour_t69_0_0_0/* byval_arg */
	, &ReconstructionFromTargetBehaviour_t69_1_0_0/* this_arg */
	, &ReconstructionFromTargetBehaviour_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionFromTargetBehaviour_t69)/* instance_size */
	, sizeof (ReconstructionFromTargetBehaviour_t69)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
// Metadata Definition Vuforia.SmartTerrainTrackerBehaviour
extern TypeInfo SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo;
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern const MethodInfo SmartTerrainTrackerBehaviour__ctor_m213_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmartTerrainTrackerBehaviour__ctor_m213/* method */
	, &SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SmartTerrainTrackerBehaviour_t71_MethodInfos[] =
{
	&SmartTerrainTrackerBehaviour__ctor_m213_MethodInfo,
	NULL
};
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m692_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m693_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m694_MethodInfo;
extern const MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m695_MethodInfo;
static const Il2CppMethodReference SmartTerrainTrackerBehaviour_t71_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m692_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m693_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m694_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m695_MethodInfo,
};
static bool SmartTerrainTrackerBehaviour_t71_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorSmartTerrainTrackerBehaviour_t188_0_0_0;
static Il2CppInterfaceOffsetPair SmartTerrainTrackerBehaviour_t71_InterfacesOffsets[] = 
{
	{ &IEditorSmartTerrainTrackerBehaviour_t188_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SmartTerrainTrackerBehaviour_t71_0_0_0;
extern const Il2CppType SmartTerrainTrackerBehaviour_t71_1_0_0;
extern const Il2CppType SmartTerrainTrackerAbstractBehaviour_t72_0_0_0;
struct SmartTerrainTrackerBehaviour_t71;
const Il2CppTypeDefinitionMetadata SmartTerrainTrackerBehaviour_t71_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SmartTerrainTrackerBehaviour_t71_InterfacesOffsets/* interfaceOffsets */
	, &SmartTerrainTrackerAbstractBehaviour_t72_0_0_0/* parent */
	, SmartTerrainTrackerBehaviour_t71_VTable/* vtableMethods */
	, SmartTerrainTrackerBehaviour_t71_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmartTerrainTrackerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SmartTerrainTrackerBehaviour_t71_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SmartTerrainTrackerBehaviour_t71_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmartTerrainTrackerBehaviour_t71_0_0_0/* byval_arg */
	, &SmartTerrainTrackerBehaviour_t71_1_0_0/* this_arg */
	, &SmartTerrainTrackerBehaviour_t71_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmartTerrainTrackerBehaviour_t71)/* instance_size */
	, sizeof (SmartTerrainTrackerBehaviour_t71)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Metadata Definition Vuforia.SurfaceBehaviour
extern TypeInfo SurfaceBehaviour_t42_il2cpp_TypeInfo;
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern const MethodInfo SurfaceBehaviour__ctor_m214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SurfaceBehaviour__ctor_m214/* method */
	, &SurfaceBehaviour_t42_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SurfaceBehaviour_t42_MethodInfos[] =
{
	&SurfaceBehaviour__ctor_m214_MethodInfo,
	NULL
};
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m696_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m697_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m698_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m699_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_InternalUnregisterTrackable_m700_MethodInfo;
extern const MethodInfo SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m701_MethodInfo;
extern const MethodInfo SmartTerrainTrackableBehaviour_Start_m702_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m703_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m704_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m705_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m706_MethodInfo;
extern const MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m707_MethodInfo;
static const Il2CppMethodReference SurfaceBehaviour_t42_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m696_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m697_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m698_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m699_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m617_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&SurfaceAbstractBehaviour_InternalUnregisterTrackable_m700_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m628_MethodInfo,
	&SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m701_MethodInfo,
	&SmartTerrainTrackableBehaviour_Start_m702_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m703_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m704_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m705_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m706_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m707_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
};
static bool SurfaceBehaviour_t42_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorSurfaceBehaviour_t189_0_0_0;
static Il2CppInterfaceOffsetPair SurfaceBehaviour_t42_InterfacesOffsets[] = 
{
	{ &IEditorSurfaceBehaviour_t189_0_0_0, 27},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
	{ &WorldCenterTrackableBehaviour_t175_0_0_0, 32},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SurfaceBehaviour_t42_0_0_0;
extern const Il2CppType SurfaceBehaviour_t42_1_0_0;
extern const Il2CppType SurfaceAbstractBehaviour_t73_0_0_0;
struct SurfaceBehaviour_t42;
const Il2CppTypeDefinitionMetadata SurfaceBehaviour_t42_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SurfaceBehaviour_t42_InterfacesOffsets/* interfaceOffsets */
	, &SurfaceAbstractBehaviour_t73_0_0_0/* parent */
	, SurfaceBehaviour_t42_VTable/* vtableMethods */
	, SurfaceBehaviour_t42_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SurfaceBehaviour_t42_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceBehaviour_t42_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SurfaceBehaviour_t42_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SurfaceBehaviour_t42_0_0_0/* byval_arg */
	, &SurfaceBehaviour_t42_1_0_0/* this_arg */
	, &SurfaceBehaviour_t42_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceBehaviour_t42)/* instance_size */
	, sizeof (SurfaceBehaviour_t42)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Metadata Definition Vuforia.TextRecoBehaviour
extern TypeInfo TextRecoBehaviour_t74_il2cpp_TypeInfo;
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern const MethodInfo TextRecoBehaviour__ctor_m215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoBehaviour__ctor_m215/* method */
	, &TextRecoBehaviour_t74_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextRecoBehaviour_t74_MethodInfos[] =
{
	&TextRecoBehaviour__ctor_m215_MethodInfo,
	NULL
};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m708_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m709_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m710_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m711_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m712_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m713_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m714_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m715_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m716_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m717_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m718_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m719_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m720_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m721_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m722_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m723_MethodInfo;
static const Il2CppMethodReference TextRecoBehaviour_t74_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m708_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m709_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m710_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m711_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m712_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m713_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m714_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m715_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m716_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m717_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m718_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m719_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m720_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m721_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m722_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m723_MethodInfo,
};
static bool TextRecoBehaviour_t74_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorTextRecoBehaviour_t190_0_0_0;
static Il2CppInterfaceOffsetPair TextRecoBehaviour_t74_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t190_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TextRecoBehaviour_t74_0_0_0;
extern const Il2CppType TextRecoBehaviour_t74_1_0_0;
struct TextRecoBehaviour_t74;
const Il2CppTypeDefinitionMetadata TextRecoBehaviour_t74_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextRecoBehaviour_t74_InterfacesOffsets/* interfaceOffsets */
	, &TextRecoAbstractBehaviour_t75_0_0_0/* parent */
	, TextRecoBehaviour_t74_VTable/* vtableMethods */
	, TextRecoBehaviour_t74_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TextRecoBehaviour_t74_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoBehaviour_t74_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextRecoBehaviour_t74_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextRecoBehaviour_t74_0_0_0/* byval_arg */
	, &TextRecoBehaviour_t74_1_0_0/* this_arg */
	, &TextRecoBehaviour_t74_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoBehaviour_t74)/* instance_size */
	, sizeof (TextRecoBehaviour_t74)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Metadata Definition Vuforia.TurnOffBehaviour
extern TypeInfo TurnOffBehaviour_t76_il2cpp_TypeInfo;
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern const MethodInfo TurnOffBehaviour__ctor_m216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffBehaviour__ctor_m216/* method */
	, &TurnOffBehaviour_t76_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern const MethodInfo TurnOffBehaviour_Awake_m217_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffBehaviour_Awake_m217/* method */
	, &TurnOffBehaviour_t76_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TurnOffBehaviour_t76_MethodInfos[] =
{
	&TurnOffBehaviour__ctor_m216_MethodInfo,
	&TurnOffBehaviour_Awake_m217_MethodInfo,
	NULL
};
static const Il2CppMethodReference TurnOffBehaviour_t76_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool TurnOffBehaviour_t76_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TurnOffBehaviour_t76_0_0_0;
extern const Il2CppType TurnOffBehaviour_t76_1_0_0;
struct TurnOffBehaviour_t76;
const Il2CppTypeDefinitionMetadata TurnOffBehaviour_t76_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TurnOffAbstractBehaviour_t77_0_0_0/* parent */
	, TurnOffBehaviour_t76_VTable/* vtableMethods */
	, TurnOffBehaviour_t76_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TurnOffBehaviour_t76_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffBehaviour_t76_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TurnOffBehaviour_t76_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TurnOffBehaviour_t76_0_0_0/* byval_arg */
	, &TurnOffBehaviour_t76_1_0_0/* this_arg */
	, &TurnOffBehaviour_t76_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffBehaviour_t76)/* instance_size */
	, sizeof (TurnOffBehaviour_t76)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
// Metadata Definition Vuforia.TurnOffWordBehaviour
extern TypeInfo TurnOffWordBehaviour_t78_il2cpp_TypeInfo;
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern const MethodInfo TurnOffWordBehaviour__ctor_m218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffWordBehaviour__ctor_m218/* method */
	, &TurnOffWordBehaviour_t78_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern const MethodInfo TurnOffWordBehaviour_Awake_m219_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffWordBehaviour_Awake_m219/* method */
	, &TurnOffWordBehaviour_t78_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TurnOffWordBehaviour_t78_MethodInfos[] =
{
	&TurnOffWordBehaviour__ctor_m218_MethodInfo,
	&TurnOffWordBehaviour_Awake_m219_MethodInfo,
	NULL
};
static const Il2CppMethodReference TurnOffWordBehaviour_t78_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool TurnOffWordBehaviour_t78_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TurnOffWordBehaviour_t78_0_0_0;
extern const Il2CppType TurnOffWordBehaviour_t78_1_0_0;
struct TurnOffWordBehaviour_t78;
const Il2CppTypeDefinitionMetadata TurnOffWordBehaviour_t78_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TurnOffWordBehaviour_t78_VTable/* vtableMethods */
	, TurnOffWordBehaviour_t78_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TurnOffWordBehaviour_t78_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffWordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffWordBehaviour_t78_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TurnOffWordBehaviour_t78_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TurnOffWordBehaviour_t78_0_0_0/* byval_arg */
	, &TurnOffWordBehaviour_t78_1_0_0/* this_arg */
	, &TurnOffWordBehaviour_t78_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffWordBehaviour_t78)/* instance_size */
	, sizeof (TurnOffWordBehaviour_t78)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
// Metadata Definition Vuforia.UserDefinedTargetBuildingBehaviour
extern TypeInfo UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern const MethodInfo UserDefinedTargetBuildingBehaviour__ctor_m220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingBehaviour__ctor_m220/* method */
	, &UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserDefinedTargetBuildingBehaviour_t79_MethodInfos[] =
{
	&UserDefinedTargetBuildingBehaviour__ctor_m220_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserDefinedTargetBuildingBehaviour_t79_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool UserDefinedTargetBuildingBehaviour_t79_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType UserDefinedTargetBuildingBehaviour_t79_0_0_0;
extern const Il2CppType UserDefinedTargetBuildingBehaviour_t79_1_0_0;
extern const Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t80_0_0_0;
struct UserDefinedTargetBuildingBehaviour_t79;
const Il2CppTypeDefinitionMetadata UserDefinedTargetBuildingBehaviour_t79_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UserDefinedTargetBuildingAbstractBehaviour_t80_0_0_0/* parent */
	, UserDefinedTargetBuildingBehaviour_t79_VTable/* vtableMethods */
	, UserDefinedTargetBuildingBehaviour_t79_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingBehaviour_t79_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserDefinedTargetBuildingBehaviour_t79_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserDefinedTargetBuildingBehaviour_t79_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingBehaviour_t79_1_0_0/* this_arg */
	, &UserDefinedTargetBuildingBehaviour_t79_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingBehaviour_t79)/* instance_size */
	, sizeof (UserDefinedTargetBuildingBehaviour_t79)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
// Metadata Definition Vuforia.VideoBackgroundBehaviour
extern TypeInfo VideoBackgroundBehaviour_t81_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern const MethodInfo VideoBackgroundBehaviour__ctor_m221_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundBehaviour__ctor_m221/* method */
	, &VideoBackgroundBehaviour_t81_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoBackgroundBehaviour_t81_MethodInfos[] =
{
	&VideoBackgroundBehaviour__ctor_m221_MethodInfo,
	NULL
};
static const Il2CppMethodReference VideoBackgroundBehaviour_t81_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool VideoBackgroundBehaviour_t81_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VideoBackgroundBehaviour_t81_0_0_0;
extern const Il2CppType VideoBackgroundBehaviour_t81_1_0_0;
extern const Il2CppType VideoBackgroundAbstractBehaviour_t82_0_0_0;
struct VideoBackgroundBehaviour_t81;
const Il2CppTypeDefinitionMetadata VideoBackgroundBehaviour_t81_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &VideoBackgroundAbstractBehaviour_t82_0_0_0/* parent */
	, VideoBackgroundBehaviour_t81_VTable/* vtableMethods */
	, VideoBackgroundBehaviour_t81_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VideoBackgroundBehaviour_t81_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundBehaviour_t81_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VideoBackgroundBehaviour_t81_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 9/* custom_attributes_cache */
	, &VideoBackgroundBehaviour_t81_0_0_0/* byval_arg */
	, &VideoBackgroundBehaviour_t81_1_0_0/* this_arg */
	, &VideoBackgroundBehaviour_t81_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundBehaviour_t81)/* instance_size */
	, sizeof (VideoBackgroundBehaviour_t81)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
// Metadata Definition Vuforia.VideoTextureRenderer
extern TypeInfo VideoTextureRenderer_t83_il2cpp_TypeInfo;
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern const MethodInfo VideoTextureRenderer__ctor_m222_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRenderer__ctor_m222/* method */
	, &VideoTextureRenderer_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoTextureRenderer_t83_MethodInfos[] =
{
	&VideoTextureRenderer__ctor_m222_MethodInfo,
	NULL
};
extern const MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m724_MethodInfo;
static const Il2CppMethodReference VideoTextureRenderer_t83_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m724_MethodInfo,
};
static bool VideoTextureRenderer_t83_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VideoTextureRenderer_t83_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t171_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VideoTextureRenderer_t83_0_0_0;
extern const Il2CppType VideoTextureRenderer_t83_1_0_0;
extern const Il2CppType VideoTextureRendererAbstractBehaviour_t84_0_0_0;
struct VideoTextureRenderer_t83;
const Il2CppTypeDefinitionMetadata VideoTextureRenderer_t83_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VideoTextureRenderer_t83_InterfacesOffsets/* interfaceOffsets */
	, &VideoTextureRendererAbstractBehaviour_t84_0_0_0/* parent */
	, VideoTextureRenderer_t83_VTable/* vtableMethods */
	, VideoTextureRenderer_t83_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VideoTextureRenderer_t83_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRenderer"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRenderer_t83_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VideoTextureRenderer_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VideoTextureRenderer_t83_0_0_0/* byval_arg */
	, &VideoTextureRenderer_t83_1_0_0/* this_arg */
	, &VideoTextureRenderer_t83_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRenderer_t83)/* instance_size */
	, sizeof (VideoTextureRenderer_t83)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Metadata Definition Vuforia.VirtualButtonBehaviour
extern TypeInfo VirtualButtonBehaviour_t85_il2cpp_TypeInfo;
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern const MethodInfo VirtualButtonBehaviour__ctor_m223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonBehaviour__ctor_m223/* method */
	, &VirtualButtonBehaviour_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualButtonBehaviour_t85_MethodInfos[] =
{
	&VirtualButtonBehaviour__ctor_m223_MethodInfo,
	NULL
};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m725_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m726_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m727_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m728_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m729_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m730_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m731_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m732_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m733_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m734_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m735_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m736_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m737_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m738_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m739_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m740_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m741_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m742_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m743_MethodInfo;
static const Il2CppMethodReference VirtualButtonBehaviour_t85_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m725_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m726_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m727_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m728_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m729_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m730_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m731_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m732_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m733_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m734_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m735_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m736_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m737_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m738_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m739_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m740_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m741_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m742_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m743_MethodInfo,
};
static bool VirtualButtonBehaviour_t85_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorVirtualButtonBehaviour_t191_0_0_0;
static Il2CppInterfaceOffsetPair VirtualButtonBehaviour_t85_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t191_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VirtualButtonBehaviour_t85_0_0_0;
extern const Il2CppType VirtualButtonBehaviour_t85_1_0_0;
struct VirtualButtonBehaviour_t85;
const Il2CppTypeDefinitionMetadata VirtualButtonBehaviour_t85_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VirtualButtonBehaviour_t85_InterfacesOffsets/* interfaceOffsets */
	, &VirtualButtonAbstractBehaviour_t86_0_0_0/* parent */
	, VirtualButtonBehaviour_t85_VTable/* vtableMethods */
	, VirtualButtonBehaviour_t85_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VirtualButtonBehaviour_t85_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonBehaviour_t85_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VirtualButtonBehaviour_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualButtonBehaviour_t85_0_0_0/* byval_arg */
	, &VirtualButtonBehaviour_t85_1_0_0/* this_arg */
	, &VirtualButtonBehaviour_t85_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonBehaviour_t85)/* instance_size */
	, sizeof (VirtualButtonBehaviour_t85)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
// Metadata Definition Vuforia.WebCamBehaviour
extern TypeInfo WebCamBehaviour_t87_il2cpp_TypeInfo;
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern const MethodInfo WebCamBehaviour__ctor_m224_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamBehaviour__ctor_m224/* method */
	, &WebCamBehaviour_t87_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WebCamBehaviour_t87_MethodInfos[] =
{
	&WebCamBehaviour__ctor_m224_MethodInfo,
	NULL
};
static const Il2CppMethodReference WebCamBehaviour_t87_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool WebCamBehaviour_t87_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WebCamBehaviour_t87_0_0_0;
extern const Il2CppType WebCamBehaviour_t87_1_0_0;
extern const Il2CppType WebCamAbstractBehaviour_t88_0_0_0;
struct WebCamBehaviour_t87;
const Il2CppTypeDefinitionMetadata WebCamBehaviour_t87_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &WebCamAbstractBehaviour_t88_0_0_0/* parent */
	, WebCamBehaviour_t87_VTable/* vtableMethods */
	, WebCamBehaviour_t87_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WebCamBehaviour_t87_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamBehaviour_t87_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WebCamBehaviour_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamBehaviour_t87_0_0_0/* byval_arg */
	, &WebCamBehaviour_t87_1_0_0/* this_arg */
	, &WebCamBehaviour_t87_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamBehaviour_t87)/* instance_size */
	, sizeof (WebCamBehaviour_t87)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
// Metadata Definition Vuforia.WireframeBehaviour
extern TypeInfo WireframeBehaviour_t89_il2cpp_TypeInfo;
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern const MethodInfo WireframeBehaviour__ctor_m225_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeBehaviour__ctor_m225/* method */
	, &WireframeBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern const MethodInfo WireframeBehaviour_CreateLineMaterial_m226_MethodInfo = 
{
	"CreateLineMaterial"/* name */
	, (methodPointerType)&WireframeBehaviour_CreateLineMaterial_m226/* method */
	, &WireframeBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern const MethodInfo WireframeBehaviour_OnRenderObject_m227_MethodInfo = 
{
	"OnRenderObject"/* name */
	, (methodPointerType)&WireframeBehaviour_OnRenderObject_m227/* method */
	, &WireframeBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo WireframeBehaviour_OnDrawGizmos_m228_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&WireframeBehaviour_OnDrawGizmos_m228/* method */
	, &WireframeBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WireframeBehaviour_t89_MethodInfos[] =
{
	&WireframeBehaviour__ctor_m225_MethodInfo,
	&WireframeBehaviour_CreateLineMaterial_m226_MethodInfo,
	&WireframeBehaviour_OnRenderObject_m227_MethodInfo,
	&WireframeBehaviour_OnDrawGizmos_m228_MethodInfo,
	NULL
};
static const Il2CppMethodReference WireframeBehaviour_t89_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool WireframeBehaviour_t89_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WireframeBehaviour_t89_0_0_0;
extern const Il2CppType WireframeBehaviour_t89_1_0_0;
struct WireframeBehaviour_t89;
const Il2CppTypeDefinitionMetadata WireframeBehaviour_t89_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WireframeBehaviour_t89_VTable/* vtableMethods */
	, WireframeBehaviour_t89_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 112/* fieldStart */

};
TypeInfo WireframeBehaviour_t89_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WireframeBehaviour_t89_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WireframeBehaviour_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WireframeBehaviour_t89_0_0_0/* byval_arg */
	, &WireframeBehaviour_t89_1_0_0/* this_arg */
	, &WireframeBehaviour_t89_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeBehaviour_t89)/* instance_size */
	, sizeof (WireframeBehaviour_t89)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
// Metadata Definition Vuforia.WireframeTrackableEventHandler
extern TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo;
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern const MethodInfo WireframeTrackableEventHandler__ctor_m229_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler__ctor_m229/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo WireframeTrackableEventHandler_Start_m230_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_Start_m230/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Status_t177_0_0_0;
extern const Il2CppType Status_t177_0_0_0;
static const ParameterInfo WireframeTrackableEventHandler_t91_WireframeTrackableEventHandler_OnTrackableStateChanged_m231_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217811, 0, &Status_t177_0_0_0},
	{"newStatus", 1, 134217812, 0, &Status_t177_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern const MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m231_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackableStateChanged_m231/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, WireframeTrackableEventHandler_t91_WireframeTrackableEventHandler_OnTrackableStateChanged_m231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern const MethodInfo WireframeTrackableEventHandler_OnTrackingFound_m232_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingFound_m232/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern const MethodInfo WireframeTrackableEventHandler_OnTrackingLost_m233_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingLost_m233/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WireframeTrackableEventHandler_t91_MethodInfos[] =
{
	&WireframeTrackableEventHandler__ctor_m229_MethodInfo,
	&WireframeTrackableEventHandler_Start_m230_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m231_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingFound_m232_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingLost_m233_MethodInfo,
	NULL
};
extern const MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m231_MethodInfo;
static const Il2CppMethodReference WireframeTrackableEventHandler_t91_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m231_MethodInfo,
};
static bool WireframeTrackableEventHandler_t91_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WireframeTrackableEventHandler_t91_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t178_0_0_0,
};
static Il2CppInterfaceOffsetPair WireframeTrackableEventHandler_t91_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t178_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WireframeTrackableEventHandler_t91_0_0_0;
extern const Il2CppType WireframeTrackableEventHandler_t91_1_0_0;
struct WireframeTrackableEventHandler_t91;
const Il2CppTypeDefinitionMetadata WireframeTrackableEventHandler_t91_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WireframeTrackableEventHandler_t91_InterfacesTypeInfos/* implementedInterfaces */
	, WireframeTrackableEventHandler_t91_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WireframeTrackableEventHandler_t91_VTable/* vtableMethods */
	, WireframeTrackableEventHandler_t91_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 115/* fieldStart */

};
TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, WireframeTrackableEventHandler_t91_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WireframeTrackableEventHandler_t91_0_0_0/* byval_arg */
	, &WireframeTrackableEventHandler_t91_1_0_0/* this_arg */
	, &WireframeTrackableEventHandler_t91_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeTrackableEventHandler_t91)/* instance_size */
	, sizeof (WireframeTrackableEventHandler_t91)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Metadata Definition Vuforia.WordBehaviour
extern TypeInfo WordBehaviour_t92_il2cpp_TypeInfo;
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordBehaviour::.ctor()
extern const MethodInfo WordBehaviour__ctor_m234_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordBehaviour__ctor_m234/* method */
	, &WordBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WordBehaviour_t92_MethodInfos[] =
{
	&WordBehaviour__ctor_m234_MethodInfo,
	NULL
};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m744_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m745_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m746_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m747_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m748_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m749_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m750_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m751_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m752_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m753_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m754_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m755_MethodInfo;
static const Il2CppMethodReference WordBehaviour_t92_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m544_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m744_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m745_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m746_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m747_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559_MethodInfo,
	&TrackableBehaviour_get_Trackable_m547_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m617_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m597_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m748_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m628_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m749_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m750_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m751_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m752_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m753_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m754_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m755_MethodInfo,
};
static bool WordBehaviour_t92_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorWordBehaviour_t192_0_0_0;
static Il2CppInterfaceOffsetPair WordBehaviour_t92_InterfacesOffsets[] = 
{
	{ &IEditorWordBehaviour_t192_0_0_0, 25},
	{ &IEditorTrackableBehaviour_t174_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WordBehaviour_t92_0_0_0;
extern const Il2CppType WordBehaviour_t92_1_0_0;
struct WordBehaviour_t92;
const Il2CppTypeDefinitionMetadata WordBehaviour_t92_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WordBehaviour_t92_InterfacesOffsets/* interfaceOffsets */
	, &WordAbstractBehaviour_t93_0_0_0/* parent */
	, WordBehaviour_t92_VTable/* vtableMethods */
	, WordBehaviour_t92_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WordBehaviour_t92_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordBehaviour_t92_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WordBehaviour_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WordBehaviour_t92_0_0_0/* byval_arg */
	, &WordBehaviour_t92_1_0_0/* this_arg */
	, &WordBehaviour_t92_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordBehaviour_t92)/* instance_size */
	, sizeof (WordBehaviour_t92)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
