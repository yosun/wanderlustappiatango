﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestParticles
struct TestParticles_t30;

// System.Void TestParticles::.ctor()
extern "C" void TestParticles__ctor_m122 (TestParticles_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Start()
extern "C" void TestParticles_Start_m123 (TestParticles_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Update()
extern "C" void TestParticles_Update_m124 (TestParticles_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::OnGUI()
extern "C" void TestParticles_OnGUI_m125 (TestParticles_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ShowParticle()
extern "C" void TestParticles_ShowParticle_m126 (TestParticles_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern "C" void TestParticles_ParticleInformationWindow_m127 (TestParticles_t30 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::InfoWindow(System.Int32)
extern "C" void TestParticles_InfoWindow_m128 (TestParticles_t30 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
