﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3608;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3603;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22557_gshared (Enumerator_t3608 * __this, Dictionary_2_t3603 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22557(__this, ___host, method) (( void (*) (Enumerator_t3608 *, Dictionary_2_t3603 *, const MethodInfo*))Enumerator__ctor_m22557_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22558_gshared (Enumerator_t3608 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22558(__this, method) (( Object_t * (*) (Enumerator_t3608 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22558_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22559_gshared (Enumerator_t3608 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22559(__this, method) (( void (*) (Enumerator_t3608 *, const MethodInfo*))Enumerator_Dispose_m22559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22560_gshared (Enumerator_t3608 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22560(__this, method) (( bool (*) (Enumerator_t3608 *, const MethodInfo*))Enumerator_MoveNext_m22560_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m22561_gshared (Enumerator_t3608 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22561(__this, method) (( Object_t * (*) (Enumerator_t3608 *, const MethodInfo*))Enumerator_get_Current_m22561_gshared)(__this, method)
