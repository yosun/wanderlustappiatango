﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
struct Enumerator_t3936;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3931;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_44.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m27130_gshared (Enumerator_t3936 * __this, Dictionary_2_t3931 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m27130(__this, ___dictionary, method) (( void (*) (Enumerator_t3936 *, Dictionary_2_t3931 *, const MethodInfo*))Enumerator__ctor_m27130_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27131_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27131(__this, method) (( Object_t * (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27131_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27132_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133(__this, method) (( Object_t * (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27133_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134(__this, method) (( Object_t * (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27134_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27135_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27135(__this, method) (( bool (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_MoveNext_m27135_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" KeyValuePair_2_t3932  Enumerator_get_Current_m27136_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27136(__this, method) (( KeyValuePair_2_t3932  (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_get_Current_m27136_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m27137_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m27137(__this, method) (( Object_t * (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_get_CurrentKey_m27137_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentValue()
extern "C" uint8_t Enumerator_get_CurrentValue_m27138_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m27138(__this, method) (( uint8_t (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_get_CurrentValue_m27138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m27139_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m27139(__this, method) (( void (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_VerifyState_m27139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m27140_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m27140(__this, method) (( void (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_VerifyCurrent_m27140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m27141_gshared (Enumerator_t3936 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27141(__this, method) (( void (*) (Enumerator_t3936 *, const MethodInfo*))Enumerator_Dispose_m27141_gshared)(__this, method)
