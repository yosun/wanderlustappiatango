﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t638;

// System.Void Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::.ctor()
extern "C" void NullPremiumObjectFactory__ctor_m2997 (NullPremiumObjectFactory_t638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
