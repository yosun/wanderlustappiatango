﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t202;

// System.Void UnityEngine.EventSystems.EventTrigger/Entry::.ctor()
extern "C" void Entry__ctor_m783 (Entry_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
