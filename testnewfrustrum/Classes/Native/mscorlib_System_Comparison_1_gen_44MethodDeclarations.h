﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3594;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m22318_gshared (Comparison_1_t3594 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m22318(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3594 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m22318_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m22319_gshared (Comparison_1_t3594 * __this, TargetSearchResult_t720  ___x, TargetSearchResult_t720  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m22319(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3594 *, TargetSearchResult_t720 , TargetSearchResult_t720 , const MethodInfo*))Comparison_1_Invoke_m22319_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m22320_gshared (Comparison_1_t3594 * __this, TargetSearchResult_t720  ___x, TargetSearchResult_t720  ___y, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m22320(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3594 *, TargetSearchResult_t720 , TargetSearchResult_t720 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m22320_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m22321_gshared (Comparison_1_t3594 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m22321(__this, ___result, method) (( int32_t (*) (Comparison_1_t3594 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m22321_gshared)(__this, ___result, method)
