﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t2260;
struct ParameterModifier_t2260_marshaled;

void ParameterModifier_t2260_marshal(const ParameterModifier_t2260& unmarshaled, ParameterModifier_t2260_marshaled& marshaled);
void ParameterModifier_t2260_marshal_back(const ParameterModifier_t2260_marshaled& marshaled, ParameterModifier_t2260& unmarshaled);
void ParameterModifier_t2260_marshal_cleanup(ParameterModifier_t2260_marshaled& marshaled);
