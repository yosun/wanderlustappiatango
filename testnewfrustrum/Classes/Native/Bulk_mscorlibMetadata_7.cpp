﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// Metadata Definition System.ExecutionEngineException
extern TypeInfo ExecutionEngineException_t2520_il2cpp_TypeInfo;
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineExceptionMethodDeclarations.h"
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor()
extern const MethodInfo ExecutionEngineException__ctor_m13583_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m13583/* method */
	, &ExecutionEngineException_t2520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ExecutionEngineException_t2520_ExecutionEngineException__ctor_m13584_ParameterInfos[] = 
{
	{"info", 0, 134224164, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224165, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ExecutionEngineException__ctor_m13584_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m13584/* method */
	, &ExecutionEngineException_t2520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ExecutionEngineException_t2520_ExecutionEngineException__ctor_m13584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecutionEngineException_t2520_MethodInfos[] =
{
	&ExecutionEngineException__ctor_m13583_MethodInfo,
	&ExecutionEngineException__ctor_m13584_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Exception_ToString_m7170_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7171_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7172_MethodInfo;
extern const MethodInfo Exception_get_Message_m7173_MethodInfo;
extern const MethodInfo Exception_get_Source_m7174_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7175_MethodInfo;
extern const MethodInfo Exception_GetType_m7176_MethodInfo;
static const Il2CppMethodReference ExecutionEngineException_t2520_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool ExecutionEngineException_t2520_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t513_0_0_0;
extern const Il2CppType _Exception_t1473_0_0_0;
static Il2CppInterfaceOffsetPair ExecutionEngineException_t2520_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionEngineException_t2520_0_0_0;
extern const Il2CppType ExecutionEngineException_t2520_1_0_0;
extern const Il2CppType SystemException_t2004_0_0_0;
struct ExecutionEngineException_t2520;
const Il2CppTypeDefinitionMetadata ExecutionEngineException_t2520_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecutionEngineException_t2520_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, ExecutionEngineException_t2520_VTable/* vtableMethods */
	, ExecutionEngineException_t2520_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecutionEngineException_t2520_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionEngineException"/* name */
	, "System"/* namespaze */
	, ExecutionEngineException_t2520_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecutionEngineException_t2520_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 887/* custom_attributes_cache */
	, &ExecutionEngineException_t2520_0_0_0/* byval_arg */
	, &ExecutionEngineException_t2520_1_0_0/* this_arg */
	, &ExecutionEngineException_t2520_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionEngineException_t2520)/* instance_size */
	, sizeof (ExecutionEngineException_t2520)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// Metadata Definition System.FieldAccessException
extern TypeInfo FieldAccessException_t2521_il2cpp_TypeInfo;
// System.FieldAccessException
#include "mscorlib_System_FieldAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor()
extern const MethodInfo FieldAccessException__ctor_m13585_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13585/* method */
	, &FieldAccessException_t2521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FieldAccessException_t2521_FieldAccessException__ctor_m13586_ParameterInfos[] = 
{
	{"message", 0, 134224166, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.String)
extern const MethodInfo FieldAccessException__ctor_m13586_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13586/* method */
	, &FieldAccessException_t2521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FieldAccessException_t2521_FieldAccessException__ctor_m13586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo FieldAccessException_t2521_FieldAccessException__ctor_m13587_ParameterInfos[] = 
{
	{"info", 0, 134224167, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224168, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FieldAccessException__ctor_m13587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13587/* method */
	, &FieldAccessException_t2521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, FieldAccessException_t2521_FieldAccessException__ctor_m13587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FieldAccessException_t2521_MethodInfos[] =
{
	&FieldAccessException__ctor_m13585_MethodInfo,
	&FieldAccessException__ctor_m13586_MethodInfo,
	&FieldAccessException__ctor_m13587_MethodInfo,
	NULL
};
static const Il2CppMethodReference FieldAccessException_t2521_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool FieldAccessException_t2521_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FieldAccessException_t2521_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAccessException_t2521_0_0_0;
extern const Il2CppType FieldAccessException_t2521_1_0_0;
extern const Il2CppType MemberAccessException_t2522_0_0_0;
struct FieldAccessException_t2521;
const Il2CppTypeDefinitionMetadata FieldAccessException_t2521_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAccessException_t2521_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2522_0_0_0/* parent */
	, FieldAccessException_t2521_VTable/* vtableMethods */
	, FieldAccessException_t2521_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FieldAccessException_t2521_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAccessException"/* name */
	, "System"/* namespaze */
	, FieldAccessException_t2521_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FieldAccessException_t2521_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 888/* custom_attributes_cache */
	, &FieldAccessException_t2521_0_0_0/* byval_arg */
	, &FieldAccessException_t2521_1_0_0/* this_arg */
	, &FieldAccessException_t2521_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAccessException_t2521)/* instance_size */
	, sizeof (FieldAccessException_t2521)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// Metadata Definition System.FlagsAttribute
extern TypeInfo FlagsAttribute_t489_il2cpp_TypeInfo;
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FlagsAttribute::.ctor()
extern const MethodInfo FlagsAttribute__ctor_m2437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FlagsAttribute__ctor_m2437/* method */
	, &FlagsAttribute_t489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FlagsAttribute_t489_MethodInfos[] =
{
	&FlagsAttribute__ctor_m2437_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5260_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5261_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference FlagsAttribute_t489_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool FlagsAttribute_t489_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t901_0_0_0;
static Il2CppInterfaceOffsetPair FlagsAttribute_t489_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FlagsAttribute_t489_0_0_0;
extern const Il2CppType FlagsAttribute_t489_1_0_0;
extern const Il2CppType Attribute_t138_0_0_0;
struct FlagsAttribute_t489;
const Il2CppTypeDefinitionMetadata FlagsAttribute_t489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FlagsAttribute_t489_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, FlagsAttribute_t489_VTable/* vtableMethods */
	, FlagsAttribute_t489_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FlagsAttribute_t489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FlagsAttribute"/* name */
	, "System"/* namespaze */
	, FlagsAttribute_t489_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FlagsAttribute_t489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 889/* custom_attributes_cache */
	, &FlagsAttribute_t489_0_0_0/* byval_arg */
	, &FlagsAttribute_t489_1_0_0/* this_arg */
	, &FlagsAttribute_t489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FlagsAttribute_t489)/* instance_size */
	, sizeof (FlagsAttribute_t489)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.FormatException
#include "mscorlib_System_FormatException.h"
// Metadata Definition System.FormatException
extern TypeInfo FormatException_t1399_il2cpp_TypeInfo;
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor()
extern const MethodInfo FormatException__ctor_m13588_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m13588/* method */
	, &FormatException_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormatException_t1399_FormatException__ctor_m6952_ParameterInfos[] = 
{
	{"message", 0, 134224169, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.String)
extern const MethodInfo FormatException__ctor_m6952_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m6952/* method */
	, &FormatException_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FormatException_t1399_FormatException__ctor_m6952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo FormatException_t1399_FormatException__ctor_m9394_ParameterInfos[] = 
{
	{"info", 0, 134224170, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224171, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FormatException__ctor_m9394_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m9394/* method */
	, &FormatException_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, FormatException_t1399_FormatException__ctor_m9394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatException_t1399_MethodInfos[] =
{
	&FormatException__ctor_m13588_MethodInfo,
	&FormatException__ctor_m6952_MethodInfo,
	&FormatException__ctor_m9394_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatException_t1399_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool FormatException_t1399_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatException_t1399_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatException_t1399_0_0_0;
extern const Il2CppType FormatException_t1399_1_0_0;
struct FormatException_t1399;
const Il2CppTypeDefinitionMetadata FormatException_t1399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatException_t1399_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, FormatException_t1399_VTable/* vtableMethods */
	, FormatException_t1399_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2374/* fieldStart */

};
TypeInfo FormatException_t1399_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatException"/* name */
	, "System"/* namespaze */
	, FormatException_t1399_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatException_t1399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 890/* custom_attributes_cache */
	, &FormatException_t1399_0_0_0/* byval_arg */
	, &FormatException_t1399_1_0_0/* this_arg */
	, &FormatException_t1399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatException_t1399)/* instance_size */
	, sizeof (FormatException_t1399)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.GC
#include "mscorlib_System_GC.h"
// Metadata Definition System.GC
extern TypeInfo GC_t2523_il2cpp_TypeInfo;
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GC_t2523_GC_SuppressFinalize_m8251_ParameterInfos[] = 
{
	{"obj", 0, 134224172, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.GC::SuppressFinalize(System.Object)
extern const MethodInfo GC_SuppressFinalize_m8251_MethodInfo = 
{
	"SuppressFinalize"/* name */
	, (methodPointerType)&GC_SuppressFinalize_m8251/* method */
	, &GC_t2523_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, GC_t2523_GC_SuppressFinalize_m8251_ParameterInfos/* parameters */
	, 891/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GC_t2523_MethodInfos[] =
{
	&GC_SuppressFinalize_m8251_MethodInfo,
	NULL
};
static const Il2CppMethodReference GC_t2523_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool GC_t2523_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GC_t2523_0_0_0;
extern const Il2CppType GC_t2523_1_0_0;
struct GC_t2523;
const Il2CppTypeDefinitionMetadata GC_t2523_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GC_t2523_VTable/* vtableMethods */
	, GC_t2523_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GC_t2523_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GC"/* name */
	, "System"/* namespaze */
	, GC_t2523_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GC_t2523_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GC_t2523_0_0_0/* byval_arg */
	, &GC_t2523_1_0_0/* this_arg */
	, &GC_t2523_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GC_t2523)/* instance_size */
	, sizeof (GC_t2523)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Guid
#include "mscorlib_System_Guid.h"
// Metadata Definition System.Guid
extern TypeInfo Guid_t108_il2cpp_TypeInfo;
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo Guid_t108_Guid__ctor_m13589_ParameterInfos[] = 
{
	{"b", 0, 134224173, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Byte[])
extern const MethodInfo Guid__ctor_m13589_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m13589/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Guid_t108_Guid__ctor_m13589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
static const ParameterInfo Guid_t108_Guid__ctor_m13590_ParameterInfos[] = 
{
	{"a", 0, 134224174, 0, &Int32_t127_0_0_0},
	{"b", 1, 134224175, 0, &Int16_t534_0_0_0},
	{"c", 2, 134224176, 0, &Int16_t534_0_0_0},
	{"d", 3, 134224177, 0, &Byte_t449_0_0_0},
	{"e", 4, 134224178, 0, &Byte_t449_0_0_0},
	{"f", 5, 134224179, 0, &Byte_t449_0_0_0},
	{"g", 6, 134224180, 0, &Byte_t449_0_0_0},
	{"h", 7, 134224181, 0, &Byte_t449_0_0_0},
	{"i", 8, 134224182, 0, &Byte_t449_0_0_0},
	{"j", 9, 134224183, 0, &Byte_t449_0_0_0},
	{"k", 10, 134224184, 0, &Byte_t449_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Int32,System.Int16,System.Int16,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
extern const MethodInfo Guid__ctor_m13590_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m13590/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, Guid_t108_Guid__ctor_m13590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.cctor()
extern const MethodInfo Guid__cctor_m13591_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Guid__cctor_m13591/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t108_Guid_CheckNull_m13592_ParameterInfos[] = 
{
	{"o", 0, 134224185, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckNull(System.Object)
extern const MethodInfo Guid_CheckNull_m13592_MethodInfo = 
{
	"CheckNull"/* name */
	, (methodPointerType)&Guid_CheckNull_m13592/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Guid_t108_Guid_CheckNull_m13592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Guid_t108_Guid_CheckLength_m13593_ParameterInfos[] = 
{
	{"o", 0, 134224186, 0, &ByteU5BU5D_t616_0_0_0},
	{"l", 1, 134224187, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckLength(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckLength_m13593_MethodInfo = 
{
	"CheckLength"/* name */
	, (methodPointerType)&Guid_CheckLength_m13593/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, Guid_t108_Guid_CheckLength_m13593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Guid_t108_Guid_CheckArray_m13594_ParameterInfos[] = 
{
	{"o", 0, 134224188, 0, &ByteU5BU5D_t616_0_0_0},
	{"l", 1, 134224189, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckArray(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckArray_m13594_MethodInfo = 
{
	"CheckArray"/* name */
	, (methodPointerType)&Guid_CheckArray_m13594/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, Guid_t108_Guid_CheckArray_m13594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Guid_t108_Guid_Compare_m13595_ParameterInfos[] = 
{
	{"x", 0, 134224190, 0, &Int32_t127_0_0_0},
	{"y", 1, 134224191, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::Compare(System.Int32,System.Int32)
extern const MethodInfo Guid_Compare_m13595_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&Guid_Compare_m13595/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Guid_t108_Guid_Compare_m13595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t108_Guid_CompareTo_m13596_ParameterInfos[] = 
{
	{"value", 0, 134224192, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Object)
extern const MethodInfo Guid_CompareTo_m13596_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m13596/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Guid_t108_Guid_CompareTo_m13596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t108_Guid_Equals_m13597_ParameterInfos[] = 
{
	{"o", 0, 134224193, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t169_0_0_0;
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Object)
extern const MethodInfo Guid_Equals_m13597_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m13597/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Guid_t108_Guid_Equals_m13597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t108_0_0_0;
extern const Il2CppType Guid_t108_0_0_0;
static const ParameterInfo Guid_t108_Guid_CompareTo_m13598_ParameterInfos[] = 
{
	{"value", 0, 134224194, 0, &Guid_t108_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Guid_t108 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Guid)
extern const MethodInfo Guid_CompareTo_m13598_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m13598/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Guid_t108/* invoker_method */
	, Guid_t108_Guid_CompareTo_m13598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t108_0_0_0;
static const ParameterInfo Guid_t108_Guid_Equals_m13599_ParameterInfos[] = 
{
	{"g", 0, 134224195, 0, &Guid_t108_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Guid_t108 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Guid)
extern const MethodInfo Guid_Equals_m13599_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m13599/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Guid_t108/* invoker_method */
	, Guid_t108_Guid_Equals_m13599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::GetHashCode()
extern const MethodInfo Guid_GetHashCode_m13600_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Guid_GetHashCode_m13600/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Guid_t108_Guid_ToHex_m13601_ParameterInfos[] = 
{
	{"b", 0, 134224196, 0, &Int32_t127_0_0_0},
};
extern const Il2CppType Char_t451_0_0_0;
extern void* RuntimeInvoker_Char_t451_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Guid::ToHex(System.Int32)
extern const MethodInfo Guid_ToHex_m13601_MethodInfo = 
{
	"ToHex"/* name */
	, (methodPointerType)&Guid_ToHex_m13601/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Char_t451_0_0_0/* return_type */
	, RuntimeInvoker_Char_t451_Int32_t127/* invoker_method */
	, Guid_t108_Guid_ToHex_m13601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Guid_t108 (const MethodInfo* method, void* obj, void** args);
// System.Guid System.Guid::NewGuid()
extern const MethodInfo Guid_NewGuid_m324_MethodInfo = 
{
	"NewGuid"/* name */
	, (methodPointerType)&Guid_NewGuid_m324/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Guid_t108_0_0_0/* return_type */
	, RuntimeInvoker_Guid_t108/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Guid_t108_Guid_AppendInt_m13602_ParameterInfos[] = 
{
	{"builder", 0, 134224197, 0, &StringBuilder_t423_0_0_0},
	{"value", 1, 134224198, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendInt(System.Text.StringBuilder,System.Int32)
extern const MethodInfo Guid_AppendInt_m13602_MethodInfo = 
{
	"AppendInt"/* name */
	, (methodPointerType)&Guid_AppendInt_m13602/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, Guid_t108_Guid_AppendInt_m13602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
static const ParameterInfo Guid_t108_Guid_AppendShort_m13603_ParameterInfos[] = 
{
	{"builder", 0, 134224199, 0, &StringBuilder_t423_0_0_0},
	{"value", 1, 134224200, 0, &Int16_t534_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendShort(System.Text.StringBuilder,System.Int16)
extern const MethodInfo Guid_AppendShort_m13603_MethodInfo = 
{
	"AppendShort"/* name */
	, (methodPointerType)&Guid_AppendShort_m13603/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int16_t534/* invoker_method */
	, Guid_t108_Guid_AppendShort_m13603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
static const ParameterInfo Guid_t108_Guid_AppendByte_m13604_ParameterInfos[] = 
{
	{"builder", 0, 134224201, 0, &StringBuilder_t423_0_0_0},
	{"value", 1, 134224202, 0, &Byte_t449_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendByte(System.Text.StringBuilder,System.Byte)
extern const MethodInfo Guid_AppendByte_m13604_MethodInfo = 
{
	"AppendByte"/* name */
	, (methodPointerType)&Guid_AppendByte_m13604/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Guid_t108_Guid_AppendByte_m13604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Guid_t108_Guid_BaseToString_m13605_ParameterInfos[] = 
{
	{"h", 0, 134224203, 0, &Boolean_t169_0_0_0},
	{"p", 1, 134224204, 0, &Boolean_t169_0_0_0},
	{"b", 2, 134224205, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::BaseToString(System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Guid_BaseToString_m13605_MethodInfo = 
{
	"BaseToString"/* name */
	, (methodPointerType)&Guid_BaseToString_m13605/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, Guid_t108_Guid_BaseToString_m13605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString()
extern const MethodInfo Guid_ToString_m13606_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m13606/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Guid_t108_Guid_ToString_m325_ParameterInfos[] = 
{
	{"format", 0, 134224206, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String)
extern const MethodInfo Guid_ToString_m325_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m325/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Guid_t108_Guid_ToString_m325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo Guid_t108_Guid_ToString_m13607_ParameterInfos[] = 
{
	{"format", 0, 134224207, 0, &String_t_0_0_0},
	{"provider", 1, 134224208, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String,System.IFormatProvider)
extern const MethodInfo Guid_ToString_m13607_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m13607/* method */
	, &Guid_t108_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Guid_t108_Guid_ToString_m13607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Guid_t108_MethodInfos[] =
{
	&Guid__ctor_m13589_MethodInfo,
	&Guid__ctor_m13590_MethodInfo,
	&Guid__cctor_m13591_MethodInfo,
	&Guid_CheckNull_m13592_MethodInfo,
	&Guid_CheckLength_m13593_MethodInfo,
	&Guid_CheckArray_m13594_MethodInfo,
	&Guid_Compare_m13595_MethodInfo,
	&Guid_CompareTo_m13596_MethodInfo,
	&Guid_Equals_m13597_MethodInfo,
	&Guid_CompareTo_m13598_MethodInfo,
	&Guid_Equals_m13599_MethodInfo,
	&Guid_GetHashCode_m13600_MethodInfo,
	&Guid_ToHex_m13601_MethodInfo,
	&Guid_NewGuid_m324_MethodInfo,
	&Guid_AppendInt_m13602_MethodInfo,
	&Guid_AppendShort_m13603_MethodInfo,
	&Guid_AppendByte_m13604_MethodInfo,
	&Guid_BaseToString_m13605_MethodInfo,
	&Guid_ToString_m13606_MethodInfo,
	&Guid_ToString_m325_MethodInfo,
	&Guid_ToString_m13607_MethodInfo,
	NULL
};
extern const MethodInfo Guid_Equals_m13597_MethodInfo;
extern const MethodInfo Guid_GetHashCode_m13600_MethodInfo;
extern const MethodInfo Guid_ToString_m13606_MethodInfo;
extern const MethodInfo Guid_ToString_m13607_MethodInfo;
extern const MethodInfo Guid_CompareTo_m13596_MethodInfo;
extern const MethodInfo Guid_CompareTo_m13598_MethodInfo;
extern const MethodInfo Guid_Equals_m13599_MethodInfo;
static const Il2CppMethodReference Guid_t108_VTable[] =
{
	&Guid_Equals_m13597_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Guid_GetHashCode_m13600_MethodInfo,
	&Guid_ToString_m13606_MethodInfo,
	&Guid_ToString_m13607_MethodInfo,
	&Guid_CompareTo_m13596_MethodInfo,
	&Guid_CompareTo_m13598_MethodInfo,
	&Guid_Equals_m13599_MethodInfo,
};
static bool Guid_t108_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
extern const Il2CppType IComparable_1_t3077_0_0_0;
extern const Il2CppType IEquatable_1_t3078_0_0_0;
static const Il2CppType* Guid_t108_InterfacesTypeInfos[] = 
{
	&IFormattable_t164_0_0_0,
	&IComparable_t166_0_0_0,
	&IComparable_1_t3077_0_0_0,
	&IEquatable_1_t3078_0_0_0,
};
static Il2CppInterfaceOffsetPair Guid_t108_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IComparable_t166_0_0_0, 5},
	{ &IComparable_1_t3077_0_0_0, 6},
	{ &IEquatable_1_t3078_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Guid_t108_1_0_0;
extern const Il2CppType ValueType_t524_0_0_0;
const Il2CppTypeDefinitionMetadata Guid_t108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Guid_t108_InterfacesTypeInfos/* implementedInterfaces */
	, Guid_t108_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Guid_t108_VTable/* vtableMethods */
	, Guid_t108_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2375/* fieldStart */

};
TypeInfo Guid_t108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Guid"/* name */
	, "System"/* namespaze */
	, Guid_t108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Guid_t108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 892/* custom_attributes_cache */
	, &Guid_t108_0_0_0/* byval_arg */
	, &Guid_t108_1_0_0/* this_arg */
	, &Guid_t108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Guid_t108)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Guid_t108)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Guid_t108 )/* native_size */
	, sizeof(Guid_t108_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.ICustomFormatter
extern TypeInfo ICustomFormatter_t2600_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo ICustomFormatter_t2600_ICustomFormatter_Format_m14638_ParameterInfos[] = 
{
	{"format", 0, 134224209, 0, &String_t_0_0_0},
	{"arg", 1, 134224210, 0, &Object_t_0_0_0},
	{"formatProvider", 2, 134224211, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ICustomFormatter::Format(System.String,System.Object,System.IFormatProvider)
extern const MethodInfo ICustomFormatter_Format_m14638_MethodInfo = 
{
	"Format"/* name */
	, NULL/* method */
	, &ICustomFormatter_t2600_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ICustomFormatter_t2600_ICustomFormatter_Format_m14638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICustomFormatter_t2600_MethodInfos[] =
{
	&ICustomFormatter_Format_m14638_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomFormatter_t2600_0_0_0;
extern const Il2CppType ICustomFormatter_t2600_1_0_0;
struct ICustomFormatter_t2600;
const Il2CppTypeDefinitionMetadata ICustomFormatter_t2600_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICustomFormatter_t2600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomFormatter"/* name */
	, "System"/* namespaze */
	, ICustomFormatter_t2600_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICustomFormatter_t2600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 893/* custom_attributes_cache */
	, &ICustomFormatter_t2600_0_0_0/* byval_arg */
	, &ICustomFormatter_t2600_1_0_0/* this_arg */
	, &ICustomFormatter_t2600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IFormatProvider
extern TypeInfo IFormatProvider_t2583_il2cpp_TypeInfo;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatProvider_t2583_IFormatProvider_GetFormat_m14639_ParameterInfos[] = 
{
	{"formatType", 0, 134224212, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.IFormatProvider::GetFormat(System.Type)
extern const MethodInfo IFormatProvider_GetFormat_m14639_MethodInfo = 
{
	"GetFormat"/* name */
	, NULL/* method */
	, &IFormatProvider_t2583_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatProvider_t2583_IFormatProvider_GetFormat_m14639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatProvider_t2583_MethodInfos[] =
{
	&IFormatProvider_GetFormat_m14639_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatProvider_t2583_1_0_0;
struct IFormatProvider_t2583;
const Il2CppTypeDefinitionMetadata IFormatProvider_t2583_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatProvider_t2583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatProvider"/* name */
	, "System"/* namespaze */
	, IFormatProvider_t2583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatProvider_t2583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 894/* custom_attributes_cache */
	, &IFormatProvider_t2583_0_0_0/* byval_arg */
	, &IFormatProvider_t2583_1_0_0/* this_arg */
	, &IFormatProvider_t2583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// Metadata Definition System.IndexOutOfRangeException
extern TypeInfo IndexOutOfRangeException_t1394_il2cpp_TypeInfo;
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor()
extern const MethodInfo IndexOutOfRangeException__ctor_m13608_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m13608/* method */
	, &IndexOutOfRangeException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t1394_IndexOutOfRangeException__ctor_m6925_ParameterInfos[] = 
{
	{"message", 0, 134224213, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern const MethodInfo IndexOutOfRangeException__ctor_m6925_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m6925/* method */
	, &IndexOutOfRangeException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IndexOutOfRangeException_t1394_IndexOutOfRangeException__ctor_m6925_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t1394_IndexOutOfRangeException__ctor_m13609_ParameterInfos[] = 
{
	{"info", 0, 134224214, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224215, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IndexOutOfRangeException__ctor_m13609_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m13609/* method */
	, &IndexOutOfRangeException_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, IndexOutOfRangeException_t1394_IndexOutOfRangeException__ctor_m13609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexOutOfRangeException_t1394_MethodInfos[] =
{
	&IndexOutOfRangeException__ctor_m13608_MethodInfo,
	&IndexOutOfRangeException__ctor_m6925_MethodInfo,
	&IndexOutOfRangeException__ctor_m13609_MethodInfo,
	NULL
};
static const Il2CppMethodReference IndexOutOfRangeException_t1394_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool IndexOutOfRangeException_t1394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IndexOutOfRangeException_t1394_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IndexOutOfRangeException_t1394_0_0_0;
extern const Il2CppType IndexOutOfRangeException_t1394_1_0_0;
struct IndexOutOfRangeException_t1394;
const Il2CppTypeDefinitionMetadata IndexOutOfRangeException_t1394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IndexOutOfRangeException_t1394_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, IndexOutOfRangeException_t1394_VTable/* vtableMethods */
	, IndexOutOfRangeException_t1394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IndexOutOfRangeException_t1394_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexOutOfRangeException"/* name */
	, "System"/* namespaze */
	, IndexOutOfRangeException_t1394_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IndexOutOfRangeException_t1394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 895/* custom_attributes_cache */
	, &IndexOutOfRangeException_t1394_0_0_0/* byval_arg */
	, &IndexOutOfRangeException_t1394_1_0_0/* this_arg */
	, &IndexOutOfRangeException_t1394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IndexOutOfRangeException_t1394)/* instance_size */
	, sizeof (IndexOutOfRangeException_t1394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// Metadata Definition System.InvalidCastException
extern TypeInfo InvalidCastException_t2524_il2cpp_TypeInfo;
// System.InvalidCastException
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor()
extern const MethodInfo InvalidCastException__ctor_m13610_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13610/* method */
	, &InvalidCastException_t2524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidCastException_t2524_InvalidCastException__ctor_m13611_ParameterInfos[] = 
{
	{"message", 0, 134224216, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.String)
extern const MethodInfo InvalidCastException__ctor_m13611_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13611/* method */
	, &InvalidCastException_t2524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvalidCastException_t2524_InvalidCastException__ctor_m13611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo InvalidCastException_t2524_InvalidCastException__ctor_m13612_ParameterInfos[] = 
{
	{"info", 0, 134224217, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224218, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidCastException__ctor_m13612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13612/* method */
	, &InvalidCastException_t2524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, InvalidCastException_t2524_InvalidCastException__ctor_m13612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidCastException_t2524_MethodInfos[] =
{
	&InvalidCastException__ctor_m13610_MethodInfo,
	&InvalidCastException__ctor_m13611_MethodInfo,
	&InvalidCastException__ctor_m13612_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidCastException_t2524_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool InvalidCastException_t2524_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidCastException_t2524_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidCastException_t2524_0_0_0;
extern const Il2CppType InvalidCastException_t2524_1_0_0;
struct InvalidCastException_t2524;
const Il2CppTypeDefinitionMetadata InvalidCastException_t2524_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidCastException_t2524_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, InvalidCastException_t2524_VTable/* vtableMethods */
	, InvalidCastException_t2524_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2389/* fieldStart */

};
TypeInfo InvalidCastException_t2524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidCastException"/* name */
	, "System"/* namespaze */
	, InvalidCastException_t2524_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidCastException_t2524_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 896/* custom_attributes_cache */
	, &InvalidCastException_t2524_0_0_0/* byval_arg */
	, &InvalidCastException_t2524_1_0_0/* this_arg */
	, &InvalidCastException_t2524_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidCastException_t2524)/* instance_size */
	, sizeof (InvalidCastException_t2524)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// Metadata Definition System.InvalidOperationException
extern TypeInfo InvalidOperationException_t1828_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor()
extern const MethodInfo InvalidOperationException__ctor_m9319_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m9319/* method */
	, &InvalidOperationException_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidOperationException_t1828_InvalidOperationException__ctor_m8313_ParameterInfos[] = 
{
	{"message", 0, 134224219, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern const MethodInfo InvalidOperationException__ctor_m8313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m8313/* method */
	, &InvalidOperationException_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvalidOperationException_t1828_InvalidOperationException__ctor_m8313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t140_0_0_0;
extern const Il2CppType Exception_t140_0_0_0;
static const ParameterInfo InvalidOperationException_t1828_InvalidOperationException__ctor_m13613_ParameterInfos[] = 
{
	{"message", 0, 134224220, 0, &String_t_0_0_0},
	{"innerException", 1, 134224221, 0, &Exception_t140_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
extern const MethodInfo InvalidOperationException__ctor_m13613_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m13613/* method */
	, &InvalidOperationException_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, InvalidOperationException_t1828_InvalidOperationException__ctor_m13613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo InvalidOperationException_t1828_InvalidOperationException__ctor_m13614_ParameterInfos[] = 
{
	{"info", 0, 134224222, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224223, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidOperationException__ctor_m13614_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m13614/* method */
	, &InvalidOperationException_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, InvalidOperationException_t1828_InvalidOperationException__ctor_m13614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidOperationException_t1828_MethodInfos[] =
{
	&InvalidOperationException__ctor_m9319_MethodInfo,
	&InvalidOperationException__ctor_m8313_MethodInfo,
	&InvalidOperationException__ctor_m13613_MethodInfo,
	&InvalidOperationException__ctor_m13614_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidOperationException_t1828_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool InvalidOperationException_t1828_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidOperationException_t1828_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidOperationException_t1828_0_0_0;
extern const Il2CppType InvalidOperationException_t1828_1_0_0;
struct InvalidOperationException_t1828;
const Il2CppTypeDefinitionMetadata InvalidOperationException_t1828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidOperationException_t1828_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, InvalidOperationException_t1828_VTable/* vtableMethods */
	, InvalidOperationException_t1828_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2390/* fieldStart */

};
TypeInfo InvalidOperationException_t1828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidOperationException"/* name */
	, "System"/* namespaze */
	, InvalidOperationException_t1828_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidOperationException_t1828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 897/* custom_attributes_cache */
	, &InvalidOperationException_t1828_0_0_0/* byval_arg */
	, &InvalidOperationException_t1828_1_0_0/* this_arg */
	, &InvalidOperationException_t1828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidOperationException_t1828)/* instance_size */
	, sizeof (InvalidOperationException_t1828)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// Metadata Definition System.LoaderOptimization
extern TypeInfo LoaderOptimization_t2525_il2cpp_TypeInfo;
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimizationMethodDeclarations.h"
static const MethodInfo* LoaderOptimization_t2525_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference LoaderOptimization_t2525_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool LoaderOptimization_t2525_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IConvertible_t165_0_0_0;
static Il2CppInterfaceOffsetPair LoaderOptimization_t2525_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoaderOptimization_t2525_0_0_0;
extern const Il2CppType LoaderOptimization_t2525_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata LoaderOptimization_t2525_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoaderOptimization_t2525_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, LoaderOptimization_t2525_VTable/* vtableMethods */
	, LoaderOptimization_t2525_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2391/* fieldStart */

};
TypeInfo LoaderOptimization_t2525_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoaderOptimization"/* name */
	, "System"/* namespaze */
	, LoaderOptimization_t2525_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 898/* custom_attributes_cache */
	, &LoaderOptimization_t2525_0_0_0/* byval_arg */
	, &LoaderOptimization_t2525_1_0_0/* this_arg */
	, &LoaderOptimization_t2525_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoaderOptimization_t2525)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoaderOptimization_t2525)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Math
#include "mscorlib_System_Math.h"
// Metadata Definition System.Math
extern TypeInfo Math_t2526_il2cpp_TypeInfo;
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Math_t2526_Math_Abs_m13615_ParameterInfos[] = 
{
	{"value", 0, 134224224, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single System.Math::Abs(System.Single)
extern const MethodInfo Math_Abs_m13615_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13615/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151/* invoker_method */
	, Math_t2526_Math_Abs_m13615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Math_t2526_Math_Abs_m13616_ParameterInfos[] = 
{
	{"value", 0, 134224225, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Abs(System.Int32)
extern const MethodInfo Math_Abs_m13616_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13616/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, Math_t2526_Math_Abs_m13616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo Math_t2526_Math_Abs_m13617_ParameterInfos[] = 
{
	{"value", 0, 134224226, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Math::Abs(System.Int64)
extern const MethodInfo Math_Abs_m13617_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13617/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Int64_t1092/* invoker_method */
	, Math_t2526_Math_Abs_m13617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Ceiling_m13618_ParameterInfos[] = 
{
	{"a", 0, 134224227, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Ceiling(System.Double)
extern const MethodInfo Math_Ceiling_m13618_MethodInfo = 
{
	"Ceiling"/* name */
	, (methodPointerType)&Math_Ceiling_m13618/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Ceiling_m13618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Floor_m13619_ParameterInfos[] = 
{
	{"d", 0, 134224228, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Floor(System.Double)
extern const MethodInfo Math_Floor_m13619_MethodInfo = 
{
	"Floor"/* name */
	, (methodPointerType)&Math_Floor_m13619/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Floor_m13619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Log_m6928_ParameterInfos[] = 
{
	{"a", 0, 134224229, 0, &Double_t1407_0_0_0},
	{"newBase", 1, 134224230, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double,System.Double)
extern const MethodInfo Math_Log_m6928_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m6928/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Log_m6928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Math_t2526_Math_Max_m4344_ParameterInfos[] = 
{
	{"val1", 0, 134224231, 0, &Single_t151_0_0_0},
	{"val2", 1, 134224232, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single System.Math::Max(System.Single,System.Single)
extern const MethodInfo Math_Max_m4344_MethodInfo = 
{
	"Max"/* name */
	, (methodPointerType)&Math_Max_m4344/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151/* invoker_method */
	, Math_t2526_Math_Max_m4344_ParameterInfos/* parameters */
	, 901/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Math_t2526_Math_Max_m8257_ParameterInfos[] = 
{
	{"val1", 0, 134224233, 0, &Int32_t127_0_0_0},
	{"val2", 1, 134224234, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern const MethodInfo Math_Max_m8257_MethodInfo = 
{
	"Max"/* name */
	, (methodPointerType)&Math_Max_m8257/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Math_t2526_Math_Max_m8257_ParameterInfos/* parameters */
	, 902/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Math_t2526_Math_Min_m13620_ParameterInfos[] = 
{
	{"val1", 0, 134224235, 0, &Int32_t127_0_0_0},
	{"val2", 1, 134224236, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern const MethodInfo Math_Min_m13620_MethodInfo = 
{
	"Min"/* name */
	, (methodPointerType)&Math_Min_m13620/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Math_t2526_Math_Min_m13620_ParameterInfos/* parameters */
	, 903/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Decimal_t1059_0_0_0;
extern const Il2CppType Decimal_t1059_0_0_0;
static const ParameterInfo Math_t2526_Math_Round_m13621_ParameterInfos[] = 
{
	{"d", 0, 134224237, 0, &Decimal_t1059_0_0_0},
};
extern void* RuntimeInvoker_Decimal_t1059_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
// System.Decimal System.Math::Round(System.Decimal)
extern const MethodInfo Math_Round_m13621_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m13621/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Decimal_t1059_0_0_0/* return_type */
	, RuntimeInvoker_Decimal_t1059_Decimal_t1059/* invoker_method */
	, Math_t2526_Math_Round_m13621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Round_m13622_ParameterInfos[] = 
{
	{"a", 0, 134224238, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Round(System.Double)
extern const MethodInfo Math_Round_m13622_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m13622/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Round_m13622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Sin_m13623_ParameterInfos[] = 
{
	{"a", 0, 134224239, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sin(System.Double)
extern const MethodInfo Math_Sin_m13623_MethodInfo = 
{
	"Sin"/* name */
	, (methodPointerType)&Math_Sin_m13623/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Sin_m13623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Cos_m13624_ParameterInfos[] = 
{
	{"d", 0, 134224240, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Cos(System.Double)
extern const MethodInfo Math_Cos_m13624_MethodInfo = 
{
	"Cos"/* name */
	, (methodPointerType)&Math_Cos_m13624/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Cos_m13624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Asin_m13625_ParameterInfos[] = 
{
	{"d", 0, 134224241, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Asin(System.Double)
extern const MethodInfo Math_Asin_m13625_MethodInfo = 
{
	"Asin"/* name */
	, (methodPointerType)&Math_Asin_m13625/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Asin_m13625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Atan2_m13626_ParameterInfos[] = 
{
	{"y", 0, 134224242, 0, &Double_t1407_0_0_0},
	{"x", 1, 134224243, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Atan2(System.Double,System.Double)
extern const MethodInfo Math_Atan2_m13626_MethodInfo = 
{
	"Atan2"/* name */
	, (methodPointerType)&Math_Atan2_m13626/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Atan2_m13626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Log_m13627_ParameterInfos[] = 
{
	{"d", 0, 134224244, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double)
extern const MethodInfo Math_Log_m13627_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m13627/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Log_m13627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Pow_m13628_ParameterInfos[] = 
{
	{"x", 0, 134224245, 0, &Double_t1407_0_0_0},
	{"y", 1, 134224246, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Pow(System.Double,System.Double)
extern const MethodInfo Math_Pow_m13628_MethodInfo = 
{
	"Pow"/* name */
	, (methodPointerType)&Math_Pow_m13628/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Pow_m13628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Math_t2526_Math_Sqrt_m13629_ParameterInfos[] = 
{
	{"d", 0, 134224247, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sqrt(System.Double)
extern const MethodInfo Math_Sqrt_m13629_MethodInfo = 
{
	"Sqrt"/* name */
	, (methodPointerType)&Math_Sqrt_m13629/* method */
	, &Math_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Double_t1407/* invoker_method */
	, Math_t2526_Math_Sqrt_m13629_ParameterInfos/* parameters */
	, 904/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Math_t2526_MethodInfos[] =
{
	&Math_Abs_m13615_MethodInfo,
	&Math_Abs_m13616_MethodInfo,
	&Math_Abs_m13617_MethodInfo,
	&Math_Ceiling_m13618_MethodInfo,
	&Math_Floor_m13619_MethodInfo,
	&Math_Log_m6928_MethodInfo,
	&Math_Max_m4344_MethodInfo,
	&Math_Max_m8257_MethodInfo,
	&Math_Min_m13620_MethodInfo,
	&Math_Round_m13621_MethodInfo,
	&Math_Round_m13622_MethodInfo,
	&Math_Sin_m13623_MethodInfo,
	&Math_Cos_m13624_MethodInfo,
	&Math_Asin_m13625_MethodInfo,
	&Math_Atan2_m13626_MethodInfo,
	&Math_Log_m13627_MethodInfo,
	&Math_Pow_m13628_MethodInfo,
	&Math_Sqrt_m13629_MethodInfo,
	NULL
};
static const Il2CppMethodReference Math_t2526_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Math_t2526_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Math_t2526_0_0_0;
extern const Il2CppType Math_t2526_1_0_0;
struct Math_t2526;
const Il2CppTypeDefinitionMetadata Math_t2526_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Math_t2526_VTable/* vtableMethods */
	, Math_t2526_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Math_t2526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Math"/* name */
	, "System"/* namespaze */
	, Math_t2526_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Math_t2526_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Math_t2526_0_0_0/* byval_arg */
	, &Math_t2526_1_0_0/* this_arg */
	, &Math_t2526_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Math_t2526)/* instance_size */
	, sizeof (Math_t2526)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// Metadata Definition System.MemberAccessException
extern TypeInfo MemberAccessException_t2522_il2cpp_TypeInfo;
// System.MemberAccessException
#include "mscorlib_System_MemberAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor()
extern const MethodInfo MemberAccessException__ctor_m13630_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13630/* method */
	, &MemberAccessException_t2522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MemberAccessException_t2522_MemberAccessException__ctor_m13631_ParameterInfos[] = 
{
	{"message", 0, 134224248, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.String)
extern const MethodInfo MemberAccessException__ctor_m13631_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13631/* method */
	, &MemberAccessException_t2522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MemberAccessException_t2522_MemberAccessException__ctor_m13631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MemberAccessException_t2522_MemberAccessException__ctor_m13632_ParameterInfos[] = 
{
	{"info", 0, 134224249, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224250, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberAccessException__ctor_m13632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13632/* method */
	, &MemberAccessException_t2522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MemberAccessException_t2522_MemberAccessException__ctor_m13632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberAccessException_t2522_MethodInfos[] =
{
	&MemberAccessException__ctor_m13630_MethodInfo,
	&MemberAccessException__ctor_m13631_MethodInfo,
	&MemberAccessException__ctor_m13632_MethodInfo,
	NULL
};
static const Il2CppMethodReference MemberAccessException_t2522_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MemberAccessException_t2522_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberAccessException_t2522_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberAccessException_t2522_1_0_0;
struct MemberAccessException_t2522;
const Il2CppTypeDefinitionMetadata MemberAccessException_t2522_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberAccessException_t2522_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, MemberAccessException_t2522_VTable/* vtableMethods */
	, MemberAccessException_t2522_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberAccessException_t2522_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberAccessException"/* name */
	, "System"/* namespaze */
	, MemberAccessException_t2522_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberAccessException_t2522_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 905/* custom_attributes_cache */
	, &MemberAccessException_t2522_0_0_0/* byval_arg */
	, &MemberAccessException_t2522_1_0_0/* this_arg */
	, &MemberAccessException_t2522_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberAccessException_t2522)/* instance_size */
	, sizeof (MemberAccessException_t2522)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// Metadata Definition System.MethodAccessException
extern TypeInfo MethodAccessException_t2527_il2cpp_TypeInfo;
// System.MethodAccessException
#include "mscorlib_System_MethodAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor()
extern const MethodInfo MethodAccessException__ctor_m13633_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m13633/* method */
	, &MethodAccessException_t2527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MethodAccessException_t2527_MethodAccessException__ctor_m13634_ParameterInfos[] = 
{
	{"info", 0, 134224251, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224252, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodAccessException__ctor_m13634_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m13634/* method */
	, &MethodAccessException_t2527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MethodAccessException_t2527_MethodAccessException__ctor_m13634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodAccessException_t2527_MethodInfos[] =
{
	&MethodAccessException__ctor_m13633_MethodInfo,
	&MethodAccessException__ctor_m13634_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodAccessException_t2527_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MethodAccessException_t2527_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodAccessException_t2527_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAccessException_t2527_0_0_0;
extern const Il2CppType MethodAccessException_t2527_1_0_0;
struct MethodAccessException_t2527;
const Il2CppTypeDefinitionMetadata MethodAccessException_t2527_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAccessException_t2527_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2522_0_0_0/* parent */
	, MethodAccessException_t2527_VTable/* vtableMethods */
	, MethodAccessException_t2527_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodAccessException_t2527_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAccessException"/* name */
	, "System"/* namespaze */
	, MethodAccessException_t2527_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodAccessException_t2527_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 906/* custom_attributes_cache */
	, &MethodAccessException_t2527_0_0_0/* byval_arg */
	, &MethodAccessException_t2527_1_0_0/* this_arg */
	, &MethodAccessException_t2527_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAccessException_t2527)/* instance_size */
	, sizeof (MethodAccessException_t2527)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// Metadata Definition System.MissingFieldException
extern TypeInfo MissingFieldException_t2528_il2cpp_TypeInfo;
// System.MissingFieldException
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor()
extern const MethodInfo MissingFieldException__ctor_m13635_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13635/* method */
	, &MissingFieldException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingFieldException_t2528_MissingFieldException__ctor_m13636_ParameterInfos[] = 
{
	{"message", 0, 134224253, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.String)
extern const MethodInfo MissingFieldException__ctor_m13636_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13636/* method */
	, &MissingFieldException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MissingFieldException_t2528_MissingFieldException__ctor_m13636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MissingFieldException_t2528_MissingFieldException__ctor_m13637_ParameterInfos[] = 
{
	{"info", 0, 134224254, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224255, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingFieldException__ctor_m13637_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13637/* method */
	, &MissingFieldException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MissingFieldException_t2528_MissingFieldException__ctor_m13637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingFieldException::get_Message()
extern const MethodInfo MissingFieldException_get_Message_m13638_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingFieldException_get_Message_m13638/* method */
	, &MissingFieldException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingFieldException_t2528_MethodInfos[] =
{
	&MissingFieldException__ctor_m13635_MethodInfo,
	&MissingFieldException__ctor_m13636_MethodInfo,
	&MissingFieldException__ctor_m13637_MethodInfo,
	&MissingFieldException_get_Message_m13638_MethodInfo,
	NULL
};
extern const MethodInfo MissingFieldException_get_Message_m13638_MethodInfo;
static const PropertyInfo MissingFieldException_t2528____Message_PropertyInfo = 
{
	&MissingFieldException_t2528_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingFieldException_get_Message_m13638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingFieldException_t2528_PropertyInfos[] =
{
	&MissingFieldException_t2528____Message_PropertyInfo,
	NULL
};
extern const MethodInfo MissingMemberException_GetObjectData_m13643_MethodInfo;
static const Il2CppMethodReference MissingFieldException_t2528_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&MissingFieldException_get_Message_m13638_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MissingFieldException_t2528_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingFieldException_t2528_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingFieldException_t2528_0_0_0;
extern const Il2CppType MissingFieldException_t2528_1_0_0;
extern const Il2CppType MissingMemberException_t2529_0_0_0;
struct MissingFieldException_t2528;
const Il2CppTypeDefinitionMetadata MissingFieldException_t2528_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingFieldException_t2528_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2529_0_0_0/* parent */
	, MissingFieldException_t2528_VTable/* vtableMethods */
	, MissingFieldException_t2528_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MissingFieldException_t2528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingFieldException"/* name */
	, "System"/* namespaze */
	, MissingFieldException_t2528_MethodInfos/* methods */
	, MissingFieldException_t2528_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingFieldException_t2528_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 907/* custom_attributes_cache */
	, &MissingFieldException_t2528_0_0_0/* byval_arg */
	, &MissingFieldException_t2528_1_0_0/* this_arg */
	, &MissingFieldException_t2528_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingFieldException_t2528)/* instance_size */
	, sizeof (MissingFieldException_t2528)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// Metadata Definition System.MissingMemberException
extern TypeInfo MissingMemberException_t2529_il2cpp_TypeInfo;
// System.MissingMemberException
#include "mscorlib_System_MissingMemberExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor()
extern const MethodInfo MissingMemberException__ctor_m13639_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13639/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t2529_MissingMemberException__ctor_m13640_ParameterInfos[] = 
{
	{"message", 0, 134224256, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String)
extern const MethodInfo MissingMemberException__ctor_m13640_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13640/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MissingMemberException_t2529_MissingMemberException__ctor_m13640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MissingMemberException_t2529_MissingMemberException__ctor_m13641_ParameterInfos[] = 
{
	{"info", 0, 134224257, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224258, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException__ctor_m13641_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13641/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MissingMemberException_t2529_MissingMemberException__ctor_m13641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t2529_MissingMemberException__ctor_m13642_ParameterInfos[] = 
{
	{"className", 0, 134224259, 0, &String_t_0_0_0},
	{"memberName", 1, 134224260, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String,System.String)
extern const MethodInfo MissingMemberException__ctor_m13642_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13642/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MissingMemberException_t2529_MissingMemberException__ctor_m13642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MissingMemberException_t2529_MissingMemberException_GetObjectData_m13643_ParameterInfos[] = 
{
	{"info", 0, 134224261, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224262, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException_GetObjectData_m13643_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MissingMemberException_GetObjectData_m13643/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MissingMemberException_t2529_MissingMemberException_GetObjectData_m13643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMemberException::get_Message()
extern const MethodInfo MissingMemberException_get_Message_m13644_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMemberException_get_Message_m13644/* method */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMemberException_t2529_MethodInfos[] =
{
	&MissingMemberException__ctor_m13639_MethodInfo,
	&MissingMemberException__ctor_m13640_MethodInfo,
	&MissingMemberException__ctor_m13641_MethodInfo,
	&MissingMemberException__ctor_m13642_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&MissingMemberException_get_Message_m13644_MethodInfo,
	NULL
};
extern const MethodInfo MissingMemberException_get_Message_m13644_MethodInfo;
static const PropertyInfo MissingMemberException_t2529____Message_PropertyInfo = 
{
	&MissingMemberException_t2529_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMemberException_get_Message_m13644_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMemberException_t2529_PropertyInfos[] =
{
	&MissingMemberException_t2529____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMemberException_t2529_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&MissingMemberException_get_Message_m13644_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MissingMemberException_t2529_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMemberException_t2529_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMemberException_t2529_1_0_0;
struct MissingMemberException_t2529;
const Il2CppTypeDefinitionMetadata MissingMemberException_t2529_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMemberException_t2529_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2522_0_0_0/* parent */
	, MissingMemberException_t2529_VTable/* vtableMethods */
	, MissingMemberException_t2529_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2398/* fieldStart */

};
TypeInfo MissingMemberException_t2529_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMemberException"/* name */
	, "System"/* namespaze */
	, MissingMemberException_t2529_MethodInfos/* methods */
	, MissingMemberException_t2529_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMemberException_t2529_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 908/* custom_attributes_cache */
	, &MissingMemberException_t2529_0_0_0/* byval_arg */
	, &MissingMemberException_t2529_1_0_0/* this_arg */
	, &MissingMemberException_t2529_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMemberException_t2529)/* instance_size */
	, sizeof (MissingMemberException_t2529)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// Metadata Definition System.MissingMethodException
extern TypeInfo MissingMethodException_t2530_il2cpp_TypeInfo;
// System.MissingMethodException
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor()
extern const MethodInfo MissingMethodException__ctor_m13645_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13645/* method */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t2530_MissingMethodException__ctor_m13646_ParameterInfos[] = 
{
	{"message", 0, 134224263, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String)
extern const MethodInfo MissingMethodException__ctor_m13646_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13646/* method */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MissingMethodException_t2530_MissingMethodException__ctor_m13646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MissingMethodException_t2530_MissingMethodException__ctor_m13647_ParameterInfos[] = 
{
	{"info", 0, 134224264, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224265, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMethodException__ctor_m13647_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13647/* method */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MissingMethodException_t2530_MissingMethodException__ctor_m13647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t2530_MissingMethodException__ctor_m13648_ParameterInfos[] = 
{
	{"className", 0, 134224266, 0, &String_t_0_0_0},
	{"methodName", 1, 134224267, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String,System.String)
extern const MethodInfo MissingMethodException__ctor_m13648_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13648/* method */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, MissingMethodException_t2530_MissingMethodException__ctor_m13648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMethodException::get_Message()
extern const MethodInfo MissingMethodException_get_Message_m13649_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMethodException_get_Message_m13649/* method */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMethodException_t2530_MethodInfos[] =
{
	&MissingMethodException__ctor_m13645_MethodInfo,
	&MissingMethodException__ctor_m13646_MethodInfo,
	&MissingMethodException__ctor_m13647_MethodInfo,
	&MissingMethodException__ctor_m13648_MethodInfo,
	&MissingMethodException_get_Message_m13649_MethodInfo,
	NULL
};
extern const MethodInfo MissingMethodException_get_Message_m13649_MethodInfo;
static const PropertyInfo MissingMethodException_t2530____Message_PropertyInfo = 
{
	&MissingMethodException_t2530_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMethodException_get_Message_m13649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMethodException_t2530_PropertyInfos[] =
{
	&MissingMethodException_t2530____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMethodException_t2530_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&MissingMethodException_get_Message_m13649_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&MissingMemberException_GetObjectData_m13643_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MissingMethodException_t2530_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMethodException_t2530_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMethodException_t2530_0_0_0;
extern const Il2CppType MissingMethodException_t2530_1_0_0;
struct MissingMethodException_t2530;
const Il2CppTypeDefinitionMetadata MissingMethodException_t2530_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMethodException_t2530_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2529_0_0_0/* parent */
	, MissingMethodException_t2530_VTable/* vtableMethods */
	, MissingMethodException_t2530_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2401/* fieldStart */

};
TypeInfo MissingMethodException_t2530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMethodException"/* name */
	, "System"/* namespaze */
	, MissingMethodException_t2530_MethodInfos/* methods */
	, MissingMethodException_t2530_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMethodException_t2530_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 909/* custom_attributes_cache */
	, &MissingMethodException_t2530_0_0_0/* byval_arg */
	, &MissingMethodException_t2530_1_0_0/* this_arg */
	, &MissingMethodException_t2530_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMethodException_t2530)/* instance_size */
	, sizeof (MissingMethodException_t2530)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// Metadata Definition System.MonoAsyncCall
extern TypeInfo MonoAsyncCall_t2531_il2cpp_TypeInfo;
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoAsyncCall::.ctor()
extern const MethodInfo MonoAsyncCall__ctor_m13650_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoAsyncCall__ctor_m13650/* method */
	, &MonoAsyncCall_t2531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoAsyncCall_t2531_MethodInfos[] =
{
	&MonoAsyncCall__ctor_m13650_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoAsyncCall_t2531_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MonoAsyncCall_t2531_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoAsyncCall_t2531_0_0_0;
extern const Il2CppType MonoAsyncCall_t2531_1_0_0;
struct MonoAsyncCall_t2531;
const Il2CppTypeDefinitionMetadata MonoAsyncCall_t2531_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoAsyncCall_t2531_VTable/* vtableMethods */
	, MonoAsyncCall_t2531_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2402/* fieldStart */

};
TypeInfo MonoAsyncCall_t2531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoAsyncCall"/* name */
	, "System"/* namespaze */
	, MonoAsyncCall_t2531_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoAsyncCall_t2531_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoAsyncCall_t2531_0_0_0/* byval_arg */
	, &MonoAsyncCall_t2531_1_0_0/* this_arg */
	, &MonoAsyncCall_t2531_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoAsyncCall_t2531)/* instance_size */
	, sizeof (MonoAsyncCall_t2531)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern TypeInfo AttributeInfo_t2532_il2cpp_TypeInfo;
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfoMethodDeclarations.h"
extern const Il2CppType AttributeUsageAttribute_t1452_0_0_0;
extern const Il2CppType AttributeUsageAttribute_t1452_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AttributeInfo_t2532_AttributeInfo__ctor_m13651_ParameterInfos[] = 
{
	{"usage", 0, 134224292, 0, &AttributeUsageAttribute_t1452_0_0_0},
	{"inheritanceLevel", 1, 134224293, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs/AttributeInfo::.ctor(System.AttributeUsageAttribute,System.Int32)
extern const MethodInfo AttributeInfo__ctor_m13651_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AttributeInfo__ctor_m13651/* method */
	, &AttributeInfo_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, AttributeInfo_t2532_AttributeInfo__ctor_m13651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs/AttributeInfo::get_Usage()
extern const MethodInfo AttributeInfo_get_Usage_m13652_MethodInfo = 
{
	"get_Usage"/* name */
	, (methodPointerType)&AttributeInfo_get_Usage_m13652/* method */
	, &AttributeInfo_t2532_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1452_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.MonoCustomAttrs/AttributeInfo::get_InheritanceLevel()
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m13653_MethodInfo = 
{
	"get_InheritanceLevel"/* name */
	, (methodPointerType)&AttributeInfo_get_InheritanceLevel_m13653/* method */
	, &AttributeInfo_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeInfo_t2532_MethodInfos[] =
{
	&AttributeInfo__ctor_m13651_MethodInfo,
	&AttributeInfo_get_Usage_m13652_MethodInfo,
	&AttributeInfo_get_InheritanceLevel_m13653_MethodInfo,
	NULL
};
extern const MethodInfo AttributeInfo_get_Usage_m13652_MethodInfo;
static const PropertyInfo AttributeInfo_t2532____Usage_PropertyInfo = 
{
	&AttributeInfo_t2532_il2cpp_TypeInfo/* parent */
	, "Usage"/* name */
	, &AttributeInfo_get_Usage_m13652_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m13653_MethodInfo;
static const PropertyInfo AttributeInfo_t2532____InheritanceLevel_PropertyInfo = 
{
	&AttributeInfo_t2532_il2cpp_TypeInfo/* parent */
	, "InheritanceLevel"/* name */
	, &AttributeInfo_get_InheritanceLevel_m13653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AttributeInfo_t2532_PropertyInfos[] =
{
	&AttributeInfo_t2532____Usage_PropertyInfo,
	&AttributeInfo_t2532____InheritanceLevel_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AttributeInfo_t2532_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AttributeInfo_t2532_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeInfo_t2532_0_0_0;
extern const Il2CppType AttributeInfo_t2532_1_0_0;
extern TypeInfo MonoCustomAttrs_t2533_il2cpp_TypeInfo;
extern const Il2CppType MonoCustomAttrs_t2533_0_0_0;
struct AttributeInfo_t2532;
const Il2CppTypeDefinitionMetadata AttributeInfo_t2532_DefinitionMetadata = 
{
	&MonoCustomAttrs_t2533_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeInfo_t2532_VTable/* vtableMethods */
	, AttributeInfo_t2532_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2409/* fieldStart */

};
TypeInfo AttributeInfo_t2532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeInfo"/* name */
	, ""/* namespaze */
	, AttributeInfo_t2532_MethodInfos/* methods */
	, AttributeInfo_t2532_PropertyInfos/* properties */
	, NULL/* events */
	, &AttributeInfo_t2532_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeInfo_t2532_0_0_0/* byval_arg */
	, &AttributeInfo_t2532_1_0_0/* this_arg */
	, &AttributeInfo_t2532_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeInfo_t2532)/* instance_size */
	, sizeof (AttributeInfo_t2532)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// Metadata Definition System.MonoCustomAttrs
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs::.cctor()
extern const MethodInfo MonoCustomAttrs__cctor_m13654_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoCustomAttrs__cctor_m13654/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_IsUserCattrProvider_m13655_ParameterInfos[] = 
{
	{"obj", 0, 134224268, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsUserCattrProvider(System.Object)
extern const MethodInfo MonoCustomAttrs_IsUserCattrProvider_m13655_MethodInfo = 
{
	"IsUserCattrProvider"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsUserCattrProvider_m13655/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_IsUserCattrProvider_m13655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributesInternal_m13656_ParameterInfos[] = 
{
	{"obj", 0, 134224269, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224270, 0, &Type_t_0_0_0},
	{"pseudoAttrs", 2, 134224271, 0, &Boolean_t169_0_0_0},
};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesInternal(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesInternal_m13656_MethodInfo = 
{
	"GetCustomAttributesInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesInternal_m13656/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributesInternal_m13656_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetPseudoCustomAttributes_m13657_ParameterInfos[] = 
{
	{"obj", 0, 134224272, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224273, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetPseudoCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetPseudoCustomAttributes_m13657_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetPseudoCustomAttributes_m13657/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetPseudoCustomAttributes_m13657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributesBase_m13658_ParameterInfos[] = 
{
	{"obj", 0, 134224274, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224275, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesBase(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesBase_m13658_MethodInfo = 
{
	"GetCustomAttributesBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesBase_m13658/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributesBase_m13658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttribute_m13659_ParameterInfos[] = 
{
	{"obj", 0, 134224276, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224277, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224278, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Attribute System.MonoCustomAttrs::GetCustomAttribute(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttribute_m13659_MethodInfo = 
{
	"GetCustomAttribute"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttribute_m13659/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Attribute_t138_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttribute_m13659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributes_m13660_ParameterInfos[] = 
{
	{"obj", 0, 134224279, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224280, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224281, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m13660_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m13660/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributes_m13660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributes_m13661_ParameterInfos[] = 
{
	{"obj", 0, 134224282, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"inherit", 1, 134224283, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m13661_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m13661/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetCustomAttributes_m13661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_IsDefined_m13662_ParameterInfos[] = 
{
	{"obj", 0, 134224284, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"attributeType", 1, 134224285, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224286, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefined(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_IsDefined_m13662_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefined_m13662/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_IsDefined_m13662_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_IsDefinedInternal_m13663_ParameterInfos[] = 
{
	{"obj", 0, 134224287, 0, &ICustomAttributeProvider_t2599_0_0_0},
	{"AttributeType", 1, 134224288, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefinedInternal(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_IsDefinedInternal_m13663_MethodInfo = 
{
	"IsDefinedInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefinedInternal_m13663/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_IsDefinedInternal_m13663_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetBasePropertyDefinition_m13664_ParameterInfos[] = 
{
	{"property", 0, 134224289, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoCustomAttrs::GetBasePropertyDefinition(System.Reflection.PropertyInfo)
extern const MethodInfo MonoCustomAttrs_GetBasePropertyDefinition_m13664_MethodInfo = 
{
	"GetBasePropertyDefinition"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBasePropertyDefinition_m13664/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetBasePropertyDefinition_m13664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2599_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_GetBase_m13665_ParameterInfos[] = 
{
	{"obj", 0, 134224290, 0, &ICustomAttributeProvider_t2599_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ICustomAttributeProvider System.MonoCustomAttrs::GetBase(System.Reflection.ICustomAttributeProvider)
extern const MethodInfo MonoCustomAttrs_GetBase_m13665_MethodInfo = 
{
	"GetBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBase_m13665/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &ICustomAttributeProvider_t2599_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_GetBase_m13665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2533_MonoCustomAttrs_RetrieveAttributeUsage_m13666_ParameterInfos[] = 
{
	{"attributeType", 0, 134224291, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs::RetrieveAttributeUsage(System.Type)
extern const MethodInfo MonoCustomAttrs_RetrieveAttributeUsage_m13666_MethodInfo = 
{
	"RetrieveAttributeUsage"/* name */
	, (methodPointerType)&MonoCustomAttrs_RetrieveAttributeUsage_m13666/* method */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1452_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2533_MonoCustomAttrs_RetrieveAttributeUsage_m13666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoCustomAttrs_t2533_MethodInfos[] =
{
	&MonoCustomAttrs__cctor_m13654_MethodInfo,
	&MonoCustomAttrs_IsUserCattrProvider_m13655_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesInternal_m13656_MethodInfo,
	&MonoCustomAttrs_GetPseudoCustomAttributes_m13657_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesBase_m13658_MethodInfo,
	&MonoCustomAttrs_GetCustomAttribute_m13659_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m13660_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m13661_MethodInfo,
	&MonoCustomAttrs_IsDefined_m13662_MethodInfo,
	&MonoCustomAttrs_IsDefinedInternal_m13663_MethodInfo,
	&MonoCustomAttrs_GetBasePropertyDefinition_m13664_MethodInfo,
	&MonoCustomAttrs_GetBase_m13665_MethodInfo,
	&MonoCustomAttrs_RetrieveAttributeUsage_m13666_MethodInfo,
	NULL
};
static const Il2CppType* MonoCustomAttrs_t2533_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AttributeInfo_t2532_0_0_0,
};
static const Il2CppMethodReference MonoCustomAttrs_t2533_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MonoCustomAttrs_t2533_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCustomAttrs_t2533_1_0_0;
struct MonoCustomAttrs_t2533;
const Il2CppTypeDefinitionMetadata MonoCustomAttrs_t2533_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoCustomAttrs_t2533_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoCustomAttrs_t2533_VTable/* vtableMethods */
	, MonoCustomAttrs_t2533_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2411/* fieldStart */

};
TypeInfo MonoCustomAttrs_t2533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCustomAttrs"/* name */
	, "System"/* namespaze */
	, MonoCustomAttrs_t2533_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoCustomAttrs_t2533_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCustomAttrs_t2533_0_0_0/* byval_arg */
	, &MonoCustomAttrs_t2533_1_0_0/* this_arg */
	, &MonoCustomAttrs_t2533_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCustomAttrs_t2533)/* instance_size */
	, sizeof (MonoCustomAttrs_t2533)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoCustomAttrs_t2533_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// Metadata Definition System.MonoTouchAOTHelper
extern TypeInfo MonoTouchAOTHelper_t2534_il2cpp_TypeInfo;
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTouchAOTHelper::.cctor()
extern const MethodInfo MonoTouchAOTHelper__cctor_m13667_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoTouchAOTHelper__cctor_m13667/* method */
	, &MonoTouchAOTHelper_t2534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTouchAOTHelper_t2534_MethodInfos[] =
{
	&MonoTouchAOTHelper__cctor_m13667_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTouchAOTHelper_t2534_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MonoTouchAOTHelper_t2534_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTouchAOTHelper_t2534_0_0_0;
extern const Il2CppType MonoTouchAOTHelper_t2534_1_0_0;
struct MonoTouchAOTHelper_t2534;
const Il2CppTypeDefinitionMetadata MonoTouchAOTHelper_t2534_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTouchAOTHelper_t2534_VTable/* vtableMethods */
	, MonoTouchAOTHelper_t2534_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2414/* fieldStart */

};
TypeInfo MonoTouchAOTHelper_t2534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTouchAOTHelper"/* name */
	, "System"/* namespaze */
	, MonoTouchAOTHelper_t2534_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTouchAOTHelper_t2534_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTouchAOTHelper_t2534_0_0_0/* byval_arg */
	, &MonoTouchAOTHelper_t2534_1_0_0/* this_arg */
	, &MonoTouchAOTHelper_t2534_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTouchAOTHelper_t2534)/* instance_size */
	, sizeof (MonoTouchAOTHelper_t2534)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoTouchAOTHelper_t2534_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// Metadata Definition System.MonoTypeInfo
extern TypeInfo MonoTypeInfo_t2535_il2cpp_TypeInfo;
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTypeInfo::.ctor()
extern const MethodInfo MonoTypeInfo__ctor_m13668_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTypeInfo__ctor_m13668/* method */
	, &MonoTypeInfo_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTypeInfo_t2535_MethodInfos[] =
{
	&MonoTypeInfo__ctor_m13668_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTypeInfo_t2535_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MonoTypeInfo_t2535_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTypeInfo_t2535_0_0_0;
extern const Il2CppType MonoTypeInfo_t2535_1_0_0;
struct MonoTypeInfo_t2535;
const Il2CppTypeDefinitionMetadata MonoTypeInfo_t2535_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTypeInfo_t2535_VTable/* vtableMethods */
	, MonoTypeInfo_t2535_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2415/* fieldStart */

};
TypeInfo MonoTypeInfo_t2535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTypeInfo"/* name */
	, "System"/* namespaze */
	, MonoTypeInfo_t2535_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTypeInfo_t2535_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTypeInfo_t2535_0_0_0/* byval_arg */
	, &MonoTypeInfo_t2535_1_0_0/* this_arg */
	, &MonoTypeInfo_t2535_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTypeInfo_t2535)/* instance_size */
	, sizeof (MonoTypeInfo_t2535)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoType
#include "mscorlib_System_MonoType.h"
// Metadata Definition System.MonoType
extern TypeInfo MonoType_t_il2cpp_TypeInfo;
// System.MonoType
#include "mscorlib_System_MonoTypeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_get_attributes_m13669_ParameterInfos[] = 
{
	{"type", 0, 134224294, 0, &Type_t_0_0_0},
};
extern const Il2CppType TypeAttributes_t2267_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t2267_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::get_attributes(System.Type)
extern const MethodInfo MonoType_get_attributes_m13669_MethodInfo = 
{
	"get_attributes"/* name */
	, (methodPointerType)&MonoType_get_attributes_m13669/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2267_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2267_Object_t/* invoker_method */
	, MonoType_t_MonoType_get_attributes_m13669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t1287_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetDefaultConstructor()
extern const MethodInfo MonoType_GetDefaultConstructor_m13670_MethodInfo = 
{
	"GetDefaultConstructor"/* name */
	, (methodPointerType)&MonoType_GetDefaultConstructor_m13670/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TypeAttributes_t2267 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::GetAttributeFlagsImpl()
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m13671_MethodInfo = 
{
	"GetAttributeFlagsImpl"/* name */
	, (methodPointerType)&MonoType_GetAttributeFlagsImpl_m13671/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2267_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2267/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 59/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Binder_t1431_0_0_0;
extern const Il2CppType Binder_t1431_0_0_0;
extern const Il2CppType CallingConventions_t2242_0_0_0;
extern const Il2CppType CallingConventions_t2242_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1432_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1432_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructorImpl_m13672_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224295, 0, &BindingFlags_t2241_0_0_0},
	{"binder", 1, 134224296, 0, &Binder_t1431_0_0_0},
	{"callConvention", 2, 134224297, 0, &CallingConventions_t2242_0_0_0},
	{"types", 3, 134224298, 0, &TypeU5BU5D_t878_0_0_0},
	{"modifiers", 4, 134224299, 0, &ParameterModifierU5BU5D_t1432_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetConstructorImpl_m13672_MethodInfo = 
{
	"GetConstructorImpl"/* name */
	, (methodPointerType)&MonoType_GetConstructorImpl_m13672/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructorImpl_m13672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 58/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_internal_m13673_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224300, 0, &BindingFlags_t2241_0_0_0},
	{"reflected_type", 1, 134224301, 0, &Type_t_0_0_0},
};
extern const Il2CppType ConstructorInfoU5BU5D_t1424_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetConstructors_internal_m13673_MethodInfo = 
{
	"GetConstructors_internal"/* name */
	, (methodPointerType)&MonoType_GetConstructors_internal_m13673/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t1424_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_internal_m13673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_m13674_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224302, 0, &BindingFlags_t2241_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetConstructors_m13674_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&MonoType_GetConstructors_m13674/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t1424_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_m13674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 72/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InternalGetEvent_m13675_ParameterInfos[] = 
{
	{"name", 0, 134224303, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224304, 0, &BindingFlags_t2241_0_0_0},
};
extern const Il2CppType EventInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::InternalGetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_InternalGetEvent_m13675_MethodInfo = 
{
	"InternalGetEvent"/* name */
	, (methodPointerType)&MonoType_InternalGetEvent_m13675/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_InternalGetEvent_m13675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetEvent_m13676_ParameterInfos[] = 
{
	{"name", 0, 134224305, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224306, 0, &BindingFlags_t2241_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::GetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetEvent_m13676_MethodInfo = 
{
	"GetEvent"/* name */
	, (methodPointerType)&MonoType_GetEvent_m13676/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetEvent_m13676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetField_m13677_ParameterInfos[] = 
{
	{"name", 0, 134224307, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224308, 0, &BindingFlags_t2241_0_0_0},
};
extern const Il2CppType FieldInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.MonoType::GetField(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetField_m13677_MethodInfo = 
{
	"GetField"/* name */
	, (methodPointerType)&MonoType_GetField_m13677/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetField_m13677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 44/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_internal_m13678_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224309, 0, &BindingFlags_t2241_0_0_0},
	{"reflected_type", 1, 134224310, 0, &Type_t_0_0_0},
};
extern const Il2CppType FieldInfoU5BU5D_t1429_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetFields_internal_m13678_MethodInfo = 
{
	"GetFields_internal"/* name */
	, (methodPointerType)&MonoType_GetFields_internal_m13678/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t1429_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetFields_internal_m13678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_m13679_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224311, 0, &BindingFlags_t2241_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetFields_m13679_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&MonoType_GetFields_m13679/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t1429_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetFields_m13679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetInterfaces()
extern const MethodInfo MonoType_GetInterfaces_m13680_MethodInfo = 
{
	"GetInterfaces"/* name */
	, (methodPointerType)&MonoType_GetInterfaces_m13680/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t878_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 39/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodsByName_m13681_ParameterInfos[] = 
{
	{"name", 0, 134224312, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224313, 0, &BindingFlags_t2241_0_0_0},
	{"ignoreCase", 2, 134224314, 0, &Boolean_t169_0_0_0},
	{"reflected_type", 3, 134224315, 0, &Type_t_0_0_0},
};
extern const Il2CppType MethodInfoU5BU5D_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetMethodsByName_m13681_MethodInfo = 
{
	"GetMethodsByName"/* name */
	, (methodPointerType)&MonoType_GetMethodsByName_m13681/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodsByName_m13681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethods_m13682_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224316, 0, &BindingFlags_t2241_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethods(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetMethods_m13682_MethodInfo = 
{
	"GetMethods"/* name */
	, (methodPointerType)&MonoType_GetMethods_m13682/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetMethods_m13682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Binder_t1431_0_0_0;
extern const Il2CppType CallingConventions_t2242_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1432_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodImpl_m13683_ParameterInfos[] = 
{
	{"name", 0, 134224317, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224318, 0, &BindingFlags_t2241_0_0_0},
	{"binder", 2, 134224319, 0, &Binder_t1431_0_0_0},
	{"callConvention", 3, 134224320, 0, &CallingConventions_t2242_0_0_0},
	{"types", 4, 134224321, 0, &TypeU5BU5D_t878_0_0_0},
	{"modifiers", 5, 134224322, 0, &ParameterModifierU5BU5D_t1432_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.MonoType::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetMethodImpl_m13683_MethodInfo = 
{
	"GetMethodImpl"/* name */
	, (methodPointerType)&MonoType_GetMethodImpl_m13683/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodImpl_m13683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertiesByName_m13684_ParameterInfos[] = 
{
	{"name", 0, 134224323, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224324, 0, &BindingFlags_t2241_0_0_0},
	{"icase", 2, 134224325, 0, &Boolean_t169_0_0_0},
	{"reflected_type", 3, 134224326, 0, &Type_t_0_0_0},
};
extern const Il2CppType PropertyInfoU5BU5D_t1428_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetPropertiesByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetPropertiesByName_m13684_MethodInfo = 
{
	"GetPropertiesByName"/* name */
	, (methodPointerType)&MonoType_GetPropertiesByName_m13684/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t1428_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertiesByName_m13684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2241_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetProperties_m13685_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224327, 0, &BindingFlags_t2241_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetProperties(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetProperties_m13685_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&MonoType_GetProperties_m13685/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t1428_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, MonoType_t_MonoType_GetProperties_m13685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Binder_t1431_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1432_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertyImpl_m13686_ParameterInfos[] = 
{
	{"name", 0, 134224328, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224329, 0, &BindingFlags_t2241_0_0_0},
	{"binder", 2, 134224330, 0, &Binder_t1431_0_0_0},
	{"returnType", 3, 134224331, 0, &Type_t_0_0_0},
	{"types", 4, 134224332, 0, &TypeU5BU5D_t878_0_0_0},
	{"modifiers", 5, 134224333, 0, &ParameterModifierU5BU5D_t1432_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoType::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetPropertyImpl_m13686_MethodInfo = 
{
	"GetPropertyImpl"/* name */
	, (methodPointerType)&MonoType_GetPropertyImpl_m13686/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertyImpl_m13686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::HasElementTypeImpl()
extern const MethodInfo MonoType_HasElementTypeImpl_m13687_MethodInfo = 
{
	"HasElementTypeImpl"/* name */
	, (methodPointerType)&MonoType_HasElementTypeImpl_m13687/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 60/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsArrayImpl()
extern const MethodInfo MonoType_IsArrayImpl_m13688_MethodInfo = 
{
	"IsArrayImpl"/* name */
	, (methodPointerType)&MonoType_IsArrayImpl_m13688/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 61/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsByRefImpl()
extern const MethodInfo MonoType_IsByRefImpl_m13689_MethodInfo = 
{
	"IsByRefImpl"/* name */
	, (methodPointerType)&MonoType_IsByRefImpl_m13689/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 62/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPointerImpl()
extern const MethodInfo MonoType_IsPointerImpl_m13690_MethodInfo = 
{
	"IsPointerImpl"/* name */
	, (methodPointerType)&MonoType_IsPointerImpl_m13690/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 63/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPrimitiveImpl()
extern const MethodInfo MonoType_IsPrimitiveImpl_m13691_MethodInfo = 
{
	"IsPrimitiveImpl"/* name */
	, (methodPointerType)&MonoType_IsPrimitiveImpl_m13691/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 64/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsSubclassOf_m13692_ParameterInfos[] = 
{
	{"type", 0, 134224334, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsSubclassOf(System.Type)
extern const MethodInfo MonoType_IsSubclassOf_m13692_MethodInfo = 
{
	"IsSubclassOf"/* name */
	, (methodPointerType)&MonoType_IsSubclassOf_m13692/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, MonoType_t_MonoType_IsSubclassOf_m13692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2241_0_0_0;
extern const Il2CppType Binder_t1431_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1432_0_0_0;
extern const Il2CppType CultureInfo_t1405_0_0_0;
extern const Il2CppType CultureInfo_t1405_0_0_0;
extern const Il2CppType StringU5BU5D_t109_0_0_0;
extern const Il2CppType StringU5BU5D_t109_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InvokeMember_m13693_ParameterInfos[] = 
{
	{"name", 0, 134224335, 0, &String_t_0_0_0},
	{"invokeAttr", 1, 134224336, 0, &BindingFlags_t2241_0_0_0},
	{"binder", 2, 134224337, 0, &Binder_t1431_0_0_0},
	{"target", 3, 134224338, 0, &Object_t_0_0_0},
	{"args", 4, 134224339, 0, &ObjectU5BU5D_t115_0_0_0},
	{"modifiers", 5, 134224340, 0, &ParameterModifierU5BU5D_t1432_0_0_0},
	{"culture", 6, 134224341, 0, &CultureInfo_t1405_0_0_0},
	{"namedParameters", 7, 134224342, 0, &StringU5BU5D_t109_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.MonoType::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern const MethodInfo MonoType_InvokeMember_m13693_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&MonoType_InvokeMember_m13693/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_InvokeMember_m13693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 73/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetElementType()
extern const MethodInfo MonoType_GetElementType_m13694_MethodInfo = 
{
	"GetElementType"/* name */
	, (methodPointerType)&MonoType_GetElementType_m13694/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_UnderlyingSystemType()
extern const MethodInfo MonoType_get_UnderlyingSystemType_m13695_MethodInfo = 
{
	"get_UnderlyingSystemType"/* name */
	, (methodPointerType)&MonoType_get_UnderlyingSystemType_m13695/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t2003_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.MonoType::get_Assembly()
extern const MethodInfo MonoType_get_Assembly_m13696_MethodInfo = 
{
	"get_Assembly"/* name */
	, (methodPointerType)&MonoType_get_Assembly_m13696/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2003_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_AssemblyQualifiedName()
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m13697_MethodInfo = 
{
	"get_AssemblyQualifiedName"/* name */
	, (methodPointerType)&MonoType_get_AssemblyQualifiedName_m13697/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoType_t_MonoType_getFullName_m13698_ParameterInfos[] = 
{
	{"full_name", 0, 134224343, 0, &Boolean_t169_0_0_0},
	{"assembly_qualified", 1, 134224344, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::getFullName(System.Boolean,System.Boolean)
extern const MethodInfo MonoType_getFullName_m13698_MethodInfo = 
{
	"getFullName"/* name */
	, (methodPointerType)&MonoType_getFullName_m13698/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, MonoType_t_MonoType_getFullName_m13698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_BaseType()
extern const MethodInfo MonoType_get_BaseType_m13699_MethodInfo = 
{
	"get_BaseType"/* name */
	, (methodPointerType)&MonoType_get_BaseType_m13699/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_FullName()
extern const MethodInfo MonoType_get_FullName_m13700_MethodInfo = 
{
	"get_FullName"/* name */
	, (methodPointerType)&MonoType_get_FullName_m13700/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsDefined_m13701_ParameterInfos[] = 
{
	{"attributeType", 0, 134224345, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224346, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoType_IsDefined_m13701_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoType_IsDefined_m13701/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_SByte_t170/* invoker_method */
	, MonoType_t_MonoType_IsDefined_m13701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m13702_ParameterInfos[] = 
{
	{"inherit", 0, 134224347, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m13702_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m13702/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m13702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m13703_ParameterInfos[] = 
{
	{"attributeType", 0, 134224348, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224349, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m13703_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m13703/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m13703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberTypes_t2247_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t2247 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.MonoType::get_MemberType()
extern const MethodInfo MonoType_get_MemberType_m13704_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&MonoType_get_MemberType_m13704/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2247_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2247/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Name()
extern const MethodInfo MonoType_get_Name_m13705_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoType_get_Name_m13705/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Namespace()
extern const MethodInfo MonoType_get_Namespace_m13706_MethodInfo = 
{
	"get_Namespace"/* name */
	, (methodPointerType)&MonoType_get_Namespace_m13706/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t2226_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Module System.MonoType::get_Module()
extern const MethodInfo MonoType_get_Module_m13707_MethodInfo = 
{
	"get_Module"/* name */
	, (methodPointerType)&MonoType_get_Module_m13707/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Module_t2226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_DeclaringType()
extern const MethodInfo MonoType_get_DeclaringType_m13708_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoType_get_DeclaringType_m13708/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_ReflectedType()
extern const MethodInfo MonoType_get_ReflectedType_m13709_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoType_get_ReflectedType_m13709/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeTypeHandle_t2047_0_0_0;
extern void* RuntimeInvoker_RuntimeTypeHandle_t2047 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeTypeHandle System.MonoType::get_TypeHandle()
extern const MethodInfo MonoType_get_TypeHandle_m13710_MethodInfo = 
{
	"get_TypeHandle"/* name */
	, (methodPointerType)&MonoType_get_TypeHandle_m13710/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeTypeHandle_t2047_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeTypeHandle_t2047/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetObjectData_m13711_ParameterInfos[] = 
{
	{"info", 0, 134224350, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224351, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoType_GetObjectData_m13711_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoType_GetObjectData_m13711/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MonoType_t_MonoType_GetObjectData_m13711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 81/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::ToString()
extern const MethodInfo MonoType_ToString_m13712_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoType_ToString_m13712/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetGenericArguments()
extern const MethodInfo MonoType_GetGenericArguments_m13713_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MonoType_GetGenericArguments_m13713/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t878_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 74/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_ContainsGenericParameters()
extern const MethodInfo MonoType_get_ContainsGenericParameters_m13714_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MonoType_get_ContainsGenericParameters_m13714/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 75/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_IsGenericParameter()
extern const MethodInfo MonoType_get_IsGenericParameter_m13715_MethodInfo = 
{
	"get_IsGenericParameter"/* name */
	, (methodPointerType)&MonoType_get_IsGenericParameter_m13715/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 80/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetGenericTypeDefinition()
extern const MethodInfo MonoType_GetGenericTypeDefinition_m13716_MethodInfo = 
{
	"GetGenericTypeDefinition"/* name */
	, (methodPointerType)&MonoType_GetGenericTypeDefinition_m13716/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 77/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t1434_0_0_0;
extern const Il2CppType MethodBase_t1434_0_0_0;
static const ParameterInfo MonoType_t_MonoType_CheckMethodSecurity_m13717_ParameterInfos[] = 
{
	{"mb", 0, 134224352, 0, &MethodBase_t1434_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.MonoType::CheckMethodSecurity(System.Reflection.MethodBase)
extern const MethodInfo MonoType_CheckMethodSecurity_m13717_MethodInfo = 
{
	"CheckMethodSecurity"/* name */
	, (methodPointerType)&MonoType_CheckMethodSecurity_m13717/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_CheckMethodSecurity_m13717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_1_0_0;
extern const Il2CppType ObjectU5BU5D_t115_1_0_0;
extern const Il2CppType MethodBase_t1434_0_0_0;
static const ParameterInfo MonoType_t_MonoType_ReorderParamArrayArguments_m13718_ParameterInfos[] = 
{
	{"args", 0, 134224353, 0, &ObjectU5BU5D_t115_1_0_0},
	{"method", 1, 134224354, 0, &MethodBase_t1434_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::ReorderParamArrayArguments(System.Object[]&,System.Reflection.MethodBase)
extern const MethodInfo MonoType_ReorderParamArrayArguments_m13718_MethodInfo = 
{
	"ReorderParamArrayArguments"/* name */
	, (methodPointerType)&MonoType_ReorderParamArrayArguments_m13718/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_ObjectU5BU5DU26_t2697_Object_t/* invoker_method */
	, MonoType_t_MonoType_ReorderParamArrayArguments_m13718_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoType_t_MethodInfos[] =
{
	&MonoType_get_attributes_m13669_MethodInfo,
	&MonoType_GetDefaultConstructor_m13670_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m13671_MethodInfo,
	&MonoType_GetConstructorImpl_m13672_MethodInfo,
	&MonoType_GetConstructors_internal_m13673_MethodInfo,
	&MonoType_GetConstructors_m13674_MethodInfo,
	&MonoType_InternalGetEvent_m13675_MethodInfo,
	&MonoType_GetEvent_m13676_MethodInfo,
	&MonoType_GetField_m13677_MethodInfo,
	&MonoType_GetFields_internal_m13678_MethodInfo,
	&MonoType_GetFields_m13679_MethodInfo,
	&MonoType_GetInterfaces_m13680_MethodInfo,
	&MonoType_GetMethodsByName_m13681_MethodInfo,
	&MonoType_GetMethods_m13682_MethodInfo,
	&MonoType_GetMethodImpl_m13683_MethodInfo,
	&MonoType_GetPropertiesByName_m13684_MethodInfo,
	&MonoType_GetProperties_m13685_MethodInfo,
	&MonoType_GetPropertyImpl_m13686_MethodInfo,
	&MonoType_HasElementTypeImpl_m13687_MethodInfo,
	&MonoType_IsArrayImpl_m13688_MethodInfo,
	&MonoType_IsByRefImpl_m13689_MethodInfo,
	&MonoType_IsPointerImpl_m13690_MethodInfo,
	&MonoType_IsPrimitiveImpl_m13691_MethodInfo,
	&MonoType_IsSubclassOf_m13692_MethodInfo,
	&MonoType_InvokeMember_m13693_MethodInfo,
	&MonoType_GetElementType_m13694_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m13695_MethodInfo,
	&MonoType_get_Assembly_m13696_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m13697_MethodInfo,
	&MonoType_getFullName_m13698_MethodInfo,
	&MonoType_get_BaseType_m13699_MethodInfo,
	&MonoType_get_FullName_m13700_MethodInfo,
	&MonoType_IsDefined_m13701_MethodInfo,
	&MonoType_GetCustomAttributes_m13702_MethodInfo,
	&MonoType_GetCustomAttributes_m13703_MethodInfo,
	&MonoType_get_MemberType_m13704_MethodInfo,
	&MonoType_get_Name_m13705_MethodInfo,
	&MonoType_get_Namespace_m13706_MethodInfo,
	&MonoType_get_Module_m13707_MethodInfo,
	&MonoType_get_DeclaringType_m13708_MethodInfo,
	&MonoType_get_ReflectedType_m13709_MethodInfo,
	&MonoType_get_TypeHandle_m13710_MethodInfo,
	&MonoType_GetObjectData_m13711_MethodInfo,
	&MonoType_ToString_m13712_MethodInfo,
	&MonoType_GetGenericArguments_m13713_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m13714_MethodInfo,
	&MonoType_get_IsGenericParameter_m13715_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m13716_MethodInfo,
	&MonoType_CheckMethodSecurity_m13717_MethodInfo,
	&MonoType_ReorderParamArrayArguments_m13718_MethodInfo,
	NULL
};
extern const MethodInfo MonoType_get_UnderlyingSystemType_m13695_MethodInfo;
static const PropertyInfo MonoType_t____UnderlyingSystemType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "UnderlyingSystemType"/* name */
	, &MonoType_get_UnderlyingSystemType_m13695_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Assembly_m13696_MethodInfo;
static const PropertyInfo MonoType_t____Assembly_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Assembly"/* name */
	, &MonoType_get_Assembly_m13696_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m13697_MethodInfo;
static const PropertyInfo MonoType_t____AssemblyQualifiedName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "AssemblyQualifiedName"/* name */
	, &MonoType_get_AssemblyQualifiedName_m13697_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_BaseType_m13699_MethodInfo;
static const PropertyInfo MonoType_t____BaseType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "BaseType"/* name */
	, &MonoType_get_BaseType_m13699_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_FullName_m13700_MethodInfo;
static const PropertyInfo MonoType_t____FullName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "FullName"/* name */
	, &MonoType_get_FullName_m13700_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_MemberType_m13704_MethodInfo;
static const PropertyInfo MonoType_t____MemberType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &MonoType_get_MemberType_m13704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Name_m13705_MethodInfo;
static const PropertyInfo MonoType_t____Name_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoType_get_Name_m13705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Namespace_m13706_MethodInfo;
static const PropertyInfo MonoType_t____Namespace_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Namespace"/* name */
	, &MonoType_get_Namespace_m13706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Module_m13707_MethodInfo;
static const PropertyInfo MonoType_t____Module_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Module"/* name */
	, &MonoType_get_Module_m13707_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_DeclaringType_m13708_MethodInfo;
static const PropertyInfo MonoType_t____DeclaringType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoType_get_DeclaringType_m13708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ReflectedType_m13709_MethodInfo;
static const PropertyInfo MonoType_t____ReflectedType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoType_get_ReflectedType_m13709_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_TypeHandle_m13710_MethodInfo;
static const PropertyInfo MonoType_t____TypeHandle_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "TypeHandle"/* name */
	, &MonoType_get_TypeHandle_m13710_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ContainsGenericParameters_m13714_MethodInfo;
static const PropertyInfo MonoType_t____ContainsGenericParameters_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MonoType_get_ContainsGenericParameters_m13714_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_IsGenericParameter_m13715_MethodInfo;
static const PropertyInfo MonoType_t____IsGenericParameter_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "IsGenericParameter"/* name */
	, &MonoType_get_IsGenericParameter_m13715_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoType_t_PropertyInfos[] =
{
	&MonoType_t____UnderlyingSystemType_PropertyInfo,
	&MonoType_t____Assembly_PropertyInfo,
	&MonoType_t____AssemblyQualifiedName_PropertyInfo,
	&MonoType_t____BaseType_PropertyInfo,
	&MonoType_t____FullName_PropertyInfo,
	&MonoType_t____MemberType_PropertyInfo,
	&MonoType_t____Name_PropertyInfo,
	&MonoType_t____Namespace_PropertyInfo,
	&MonoType_t____Module_PropertyInfo,
	&MonoType_t____DeclaringType_PropertyInfo,
	&MonoType_t____ReflectedType_PropertyInfo,
	&MonoType_t____TypeHandle_PropertyInfo,
	&MonoType_t____ContainsGenericParameters_PropertyInfo,
	&MonoType_t____IsGenericParameter_PropertyInfo,
	NULL
};
extern const MethodInfo Type_Equals_m10239_MethodInfo;
extern const MethodInfo Type_GetHashCode_m10253_MethodInfo;
extern const MethodInfo MonoType_ToString_m13712_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m13703_MethodInfo;
extern const MethodInfo MonoType_IsDefined_m13701_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m13702_MethodInfo;
extern const MethodInfo Type_get_Attributes_m10219_MethodInfo;
extern const MethodInfo Type_get_HasElementType_m10221_MethodInfo;
extern const MethodInfo Type_get_IsAbstract_m10222_MethodInfo;
extern const MethodInfo Type_get_IsArray_m10223_MethodInfo;
extern const MethodInfo Type_get_IsByRef_m10224_MethodInfo;
extern const MethodInfo Type_get_IsClass_m10225_MethodInfo;
extern const MethodInfo Type_get_IsContextful_m10226_MethodInfo;
extern const MethodInfo Type_get_IsEnum_m10227_MethodInfo;
extern const MethodInfo Type_get_IsExplicitLayout_m10228_MethodInfo;
extern const MethodInfo Type_get_IsInterface_m10229_MethodInfo;
extern const MethodInfo Type_get_IsMarshalByRef_m10230_MethodInfo;
extern const MethodInfo Type_get_IsPointer_m10231_MethodInfo;
extern const MethodInfo Type_get_IsPrimitive_m10232_MethodInfo;
extern const MethodInfo Type_get_IsSealed_m10233_MethodInfo;
extern const MethodInfo Type_get_IsSerializable_m10234_MethodInfo;
extern const MethodInfo Type_get_IsValueType_m10235_MethodInfo;
extern const MethodInfo Type_Equals_m10240_MethodInfo;
extern const MethodInfo MonoType_IsSubclassOf_m13692_MethodInfo;
extern const MethodInfo MonoType_GetInterfaces_m13680_MethodInfo;
extern const MethodInfo Type_IsAssignableFrom_m10251_MethodInfo;
extern const MethodInfo Type_IsInstanceOfType_m10252_MethodInfo;
extern const MethodInfo MonoType_GetElementType_m13694_MethodInfo;
extern const MethodInfo MonoType_GetEvent_m13676_MethodInfo;
extern const MethodInfo MonoType_GetField_m13677_MethodInfo;
extern const MethodInfo MonoType_GetFields_m13679_MethodInfo;
extern const MethodInfo Type_GetMethod_m10254_MethodInfo;
extern const MethodInfo Type_GetMethod_m10255_MethodInfo;
extern const MethodInfo Type_GetMethod_m10256_MethodInfo;
extern const MethodInfo Type_GetMethod_m10257_MethodInfo;
extern const MethodInfo MonoType_GetMethodImpl_m13683_MethodInfo;
extern const MethodInfo MonoType_GetMethods_m13682_MethodInfo;
extern const MethodInfo MonoType_GetProperties_m13685_MethodInfo;
extern const MethodInfo Type_GetProperty_m10258_MethodInfo;
extern const MethodInfo Type_GetProperty_m10259_MethodInfo;
extern const MethodInfo Type_GetProperty_m10260_MethodInfo;
extern const MethodInfo Type_GetProperty_m10261_MethodInfo;
extern const MethodInfo MonoType_GetPropertyImpl_m13686_MethodInfo;
extern const MethodInfo MonoType_GetConstructorImpl_m13672_MethodInfo;
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m13671_MethodInfo;
extern const MethodInfo MonoType_HasElementTypeImpl_m13687_MethodInfo;
extern const MethodInfo MonoType_IsArrayImpl_m13688_MethodInfo;
extern const MethodInfo MonoType_IsByRefImpl_m13689_MethodInfo;
extern const MethodInfo MonoType_IsPointerImpl_m13690_MethodInfo;
extern const MethodInfo MonoType_IsPrimitiveImpl_m13691_MethodInfo;
extern const MethodInfo Type_IsValueTypeImpl_m10263_MethodInfo;
extern const MethodInfo Type_IsContextfulImpl_m10264_MethodInfo;
extern const MethodInfo Type_IsMarshalByRefImpl_m10265_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10266_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10267_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10268_MethodInfo;
extern const MethodInfo Type_GetConstructors_m10269_MethodInfo;
extern const MethodInfo MonoType_GetConstructors_m13674_MethodInfo;
extern const MethodInfo MonoType_InvokeMember_m13693_MethodInfo;
extern const MethodInfo MonoType_GetGenericArguments_m13713_MethodInfo;
extern const MethodInfo Type_get_IsGenericTypeDefinition_m10274_MethodInfo;
extern const MethodInfo MonoType_GetGenericTypeDefinition_m13716_MethodInfo;
extern const MethodInfo Type_get_IsGenericType_m10277_MethodInfo;
extern const MethodInfo Type_MakeGenericType_m10279_MethodInfo;
extern const MethodInfo MonoType_GetObjectData_m13711_MethodInfo;
static const Il2CppMethodReference MonoType_t_VTable[] =
{
	&Type_Equals_m10239_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Type_GetHashCode_m10253_MethodInfo,
	&MonoType_ToString_m13712_MethodInfo,
	&MonoType_GetCustomAttributes_m13703_MethodInfo,
	&MonoType_IsDefined_m13701_MethodInfo,
	&MonoType_get_DeclaringType_m13708_MethodInfo,
	&MonoType_get_MemberType_m13704_MethodInfo,
	&MonoType_get_Name_m13705_MethodInfo,
	&MonoType_get_ReflectedType_m13709_MethodInfo,
	&MonoType_get_Module_m13707_MethodInfo,
	&MonoType_IsDefined_m13701_MethodInfo,
	&MonoType_GetCustomAttributes_m13702_MethodInfo,
	&MonoType_GetCustomAttributes_m13703_MethodInfo,
	&MonoType_get_Assembly_m13696_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m13697_MethodInfo,
	&Type_get_Attributes_m10219_MethodInfo,
	&MonoType_get_BaseType_m13699_MethodInfo,
	&MonoType_get_FullName_m13700_MethodInfo,
	&Type_get_HasElementType_m10221_MethodInfo,
	&Type_get_IsAbstract_m10222_MethodInfo,
	&Type_get_IsArray_m10223_MethodInfo,
	&Type_get_IsByRef_m10224_MethodInfo,
	&Type_get_IsClass_m10225_MethodInfo,
	&Type_get_IsContextful_m10226_MethodInfo,
	&Type_get_IsEnum_m10227_MethodInfo,
	&Type_get_IsExplicitLayout_m10228_MethodInfo,
	&Type_get_IsInterface_m10229_MethodInfo,
	&Type_get_IsMarshalByRef_m10230_MethodInfo,
	&Type_get_IsPointer_m10231_MethodInfo,
	&Type_get_IsPrimitive_m10232_MethodInfo,
	&Type_get_IsSealed_m10233_MethodInfo,
	&Type_get_IsSerializable_m10234_MethodInfo,
	&Type_get_IsValueType_m10235_MethodInfo,
	&MonoType_get_Namespace_m13706_MethodInfo,
	&MonoType_get_TypeHandle_m13710_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m13695_MethodInfo,
	&Type_Equals_m10240_MethodInfo,
	&MonoType_IsSubclassOf_m13692_MethodInfo,
	&MonoType_GetInterfaces_m13680_MethodInfo,
	&Type_IsAssignableFrom_m10251_MethodInfo,
	&Type_IsInstanceOfType_m10252_MethodInfo,
	&MonoType_GetElementType_m13694_MethodInfo,
	&MonoType_GetEvent_m13676_MethodInfo,
	&MonoType_GetField_m13677_MethodInfo,
	&MonoType_GetFields_m13679_MethodInfo,
	&Type_GetMethod_m10254_MethodInfo,
	&Type_GetMethod_m10255_MethodInfo,
	&Type_GetMethod_m10256_MethodInfo,
	&Type_GetMethod_m10257_MethodInfo,
	&MonoType_GetMethodImpl_m13683_MethodInfo,
	&MonoType_GetMethods_m13682_MethodInfo,
	&MonoType_GetProperties_m13685_MethodInfo,
	&Type_GetProperty_m10258_MethodInfo,
	&Type_GetProperty_m10259_MethodInfo,
	&Type_GetProperty_m10260_MethodInfo,
	&Type_GetProperty_m10261_MethodInfo,
	&MonoType_GetPropertyImpl_m13686_MethodInfo,
	&MonoType_GetConstructorImpl_m13672_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m13671_MethodInfo,
	&MonoType_HasElementTypeImpl_m13687_MethodInfo,
	&MonoType_IsArrayImpl_m13688_MethodInfo,
	&MonoType_IsByRefImpl_m13689_MethodInfo,
	&MonoType_IsPointerImpl_m13690_MethodInfo,
	&MonoType_IsPrimitiveImpl_m13691_MethodInfo,
	&Type_IsValueTypeImpl_m10263_MethodInfo,
	&Type_IsContextfulImpl_m10264_MethodInfo,
	&Type_IsMarshalByRefImpl_m10265_MethodInfo,
	&Type_GetConstructor_m10266_MethodInfo,
	&Type_GetConstructor_m10267_MethodInfo,
	&Type_GetConstructor_m10268_MethodInfo,
	&Type_GetConstructors_m10269_MethodInfo,
	&MonoType_GetConstructors_m13674_MethodInfo,
	&MonoType_InvokeMember_m13693_MethodInfo,
	&MonoType_GetGenericArguments_m13713_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m13714_MethodInfo,
	&Type_get_IsGenericTypeDefinition_m10274_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m13716_MethodInfo,
	&Type_get_IsGenericType_m10277_MethodInfo,
	&Type_MakeGenericType_m10279_MethodInfo,
	&MonoType_get_IsGenericParameter_m13715_MethodInfo,
	&MonoType_GetObjectData_m13711_MethodInfo,
};
static bool MonoType_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoType_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
};
extern const Il2CppType IReflect_t2637_0_0_0;
extern const Il2CppType _Type_t2635_0_0_0;
extern const Il2CppType _MemberInfo_t2636_0_0_0;
static Il2CppInterfaceOffsetPair MonoType_t_InterfacesOffsets[] = 
{
	{ &IReflect_t2637_0_0_0, 14},
	{ &_Type_t2635_0_0_0, 14},
	{ &ICustomAttributeProvider_t2599_0_0_0, 4},
	{ &_MemberInfo_t2636_0_0_0, 6},
	{ &ISerializable_t513_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType MonoType_t_1_0_0;
struct MonoType_t;
const Il2CppTypeDefinitionMetadata MonoType_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoType_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoType_t_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, MonoType_t_VTable/* vtableMethods */
	, MonoType_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2417/* fieldStart */

};
TypeInfo MonoType_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoType"/* name */
	, "System"/* namespaze */
	, MonoType_t_MethodInfos/* methods */
	, MonoType_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoType_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoType_t_0_0_0/* byval_arg */
	, &MonoType_t_1_0_0/* this_arg */
	, &MonoType_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoType_t)/* instance_size */
	, sizeof (MonoType_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 14/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 82/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// Metadata Definition System.MulticastNotSupportedException
extern TypeInfo MulticastNotSupportedException_t2536_il2cpp_TypeInfo;
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor()
extern const MethodInfo MulticastNotSupportedException__ctor_m13719_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13719/* method */
	, &MulticastNotSupportedException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t2536_MulticastNotSupportedException__ctor_m13720_ParameterInfos[] = 
{
	{"message", 0, 134224355, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.String)
extern const MethodInfo MulticastNotSupportedException__ctor_m13720_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13720/* method */
	, &MulticastNotSupportedException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MulticastNotSupportedException_t2536_MulticastNotSupportedException__ctor_m13720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t2536_MulticastNotSupportedException__ctor_m13721_ParameterInfos[] = 
{
	{"info", 0, 134224356, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224357, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MulticastNotSupportedException__ctor_m13721_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13721/* method */
	, &MulticastNotSupportedException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, MulticastNotSupportedException_t2536_MulticastNotSupportedException__ctor_m13721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MulticastNotSupportedException_t2536_MethodInfos[] =
{
	&MulticastNotSupportedException__ctor_m13719_MethodInfo,
	&MulticastNotSupportedException__ctor_m13720_MethodInfo,
	&MulticastNotSupportedException__ctor_m13721_MethodInfo,
	NULL
};
static const Il2CppMethodReference MulticastNotSupportedException_t2536_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool MulticastNotSupportedException_t2536_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MulticastNotSupportedException_t2536_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastNotSupportedException_t2536_0_0_0;
extern const Il2CppType MulticastNotSupportedException_t2536_1_0_0;
struct MulticastNotSupportedException_t2536;
const Il2CppTypeDefinitionMetadata MulticastNotSupportedException_t2536_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastNotSupportedException_t2536_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, MulticastNotSupportedException_t2536_VTable/* vtableMethods */
	, MulticastNotSupportedException_t2536_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MulticastNotSupportedException_t2536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastNotSupportedException"/* name */
	, "System"/* namespaze */
	, MulticastNotSupportedException_t2536_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MulticastNotSupportedException_t2536_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 910/* custom_attributes_cache */
	, &MulticastNotSupportedException_t2536_0_0_0/* byval_arg */
	, &MulticastNotSupportedException_t2536_1_0_0/* this_arg */
	, &MulticastNotSupportedException_t2536_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastNotSupportedException_t2536)/* instance_size */
	, sizeof (MulticastNotSupportedException_t2536)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// Metadata Definition System.NonSerializedAttribute
extern TypeInfo NonSerializedAttribute_t2537_il2cpp_TypeInfo;
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NonSerializedAttribute::.ctor()
extern const MethodInfo NonSerializedAttribute__ctor_m13722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonSerializedAttribute__ctor_m13722/* method */
	, &NonSerializedAttribute_t2537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonSerializedAttribute_t2537_MethodInfos[] =
{
	&NonSerializedAttribute__ctor_m13722_MethodInfo,
	NULL
};
static const Il2CppMethodReference NonSerializedAttribute_t2537_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool NonSerializedAttribute_t2537_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NonSerializedAttribute_t2537_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NonSerializedAttribute_t2537_0_0_0;
extern const Il2CppType NonSerializedAttribute_t2537_1_0_0;
struct NonSerializedAttribute_t2537;
const Il2CppTypeDefinitionMetadata NonSerializedAttribute_t2537_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NonSerializedAttribute_t2537_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, NonSerializedAttribute_t2537_VTable/* vtableMethods */
	, NonSerializedAttribute_t2537_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonSerializedAttribute_t2537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonSerializedAttribute"/* name */
	, "System"/* namespaze */
	, NonSerializedAttribute_t2537_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonSerializedAttribute_t2537_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 911/* custom_attributes_cache */
	, &NonSerializedAttribute_t2537_0_0_0/* byval_arg */
	, &NonSerializedAttribute_t2537_1_0_0/* this_arg */
	, &NonSerializedAttribute_t2537_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonSerializedAttribute_t2537)/* instance_size */
	, sizeof (NonSerializedAttribute_t2537)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// Metadata Definition System.NotImplementedException
extern TypeInfo NotImplementedException_t1810_il2cpp_TypeInfo;
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor()
extern const MethodInfo NotImplementedException__ctor_m13723_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m13723/* method */
	, &NotImplementedException_t1810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotImplementedException_t1810_NotImplementedException__ctor_m8272_ParameterInfos[] = 
{
	{"message", 0, 134224358, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.String)
extern const MethodInfo NotImplementedException__ctor_m8272_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m8272/* method */
	, &NotImplementedException_t1810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NotImplementedException_t1810_NotImplementedException__ctor_m8272_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo NotImplementedException_t1810_NotImplementedException__ctor_m13724_ParameterInfos[] = 
{
	{"info", 0, 134224359, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224360, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotImplementedException__ctor_m13724_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m13724/* method */
	, &NotImplementedException_t1810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, NotImplementedException_t1810_NotImplementedException__ctor_m13724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotImplementedException_t1810_MethodInfos[] =
{
	&NotImplementedException__ctor_m13723_MethodInfo,
	&NotImplementedException__ctor_m8272_MethodInfo,
	&NotImplementedException__ctor_m13724_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotImplementedException_t1810_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool NotImplementedException_t1810_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotImplementedException_t1810_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotImplementedException_t1810_0_0_0;
extern const Il2CppType NotImplementedException_t1810_1_0_0;
struct NotImplementedException_t1810;
const Il2CppTypeDefinitionMetadata NotImplementedException_t1810_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotImplementedException_t1810_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, NotImplementedException_t1810_VTable/* vtableMethods */
	, NotImplementedException_t1810_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NotImplementedException_t1810_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotImplementedException"/* name */
	, "System"/* namespaze */
	, NotImplementedException_t1810_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotImplementedException_t1810_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 912/* custom_attributes_cache */
	, &NotImplementedException_t1810_0_0_0/* byval_arg */
	, &NotImplementedException_t1810_1_0_0/* this_arg */
	, &NotImplementedException_t1810_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotImplementedException_t1810)/* instance_size */
	, sizeof (NotImplementedException_t1810)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// Metadata Definition System.NotSupportedException
extern TypeInfo NotSupportedException_t435_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor()
extern const MethodInfo NotSupportedException__ctor_m2061_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m2061/* method */
	, &NotSupportedException_t435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotSupportedException_t435_NotSupportedException__ctor_m5645_ParameterInfos[] = 
{
	{"message", 0, 134224361, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.String)
extern const MethodInfo NotSupportedException__ctor_m5645_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m5645/* method */
	, &NotSupportedException_t435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NotSupportedException_t435_NotSupportedException__ctor_m5645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo NotSupportedException_t435_NotSupportedException__ctor_m13725_ParameterInfos[] = 
{
	{"info", 0, 134224362, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224363, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotSupportedException__ctor_m13725_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m13725/* method */
	, &NotSupportedException_t435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, NotSupportedException_t435_NotSupportedException__ctor_m13725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotSupportedException_t435_MethodInfos[] =
{
	&NotSupportedException__ctor_m2061_MethodInfo,
	&NotSupportedException__ctor_m5645_MethodInfo,
	&NotSupportedException__ctor_m13725_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotSupportedException_t435_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool NotSupportedException_t435_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotSupportedException_t435_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotSupportedException_t435_0_0_0;
extern const Il2CppType NotSupportedException_t435_1_0_0;
struct NotSupportedException_t435;
const Il2CppTypeDefinitionMetadata NotSupportedException_t435_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotSupportedException_t435_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, NotSupportedException_t435_VTable/* vtableMethods */
	, NotSupportedException_t435_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2418/* fieldStart */

};
TypeInfo NotSupportedException_t435_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotSupportedException"/* name */
	, "System"/* namespaze */
	, NotSupportedException_t435_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotSupportedException_t435_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 913/* custom_attributes_cache */
	, &NotSupportedException_t435_0_0_0/* byval_arg */
	, &NotSupportedException_t435_1_0_0/* this_arg */
	, &NotSupportedException_t435_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotSupportedException_t435)/* instance_size */
	, sizeof (NotSupportedException_t435)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// Metadata Definition System.NullReferenceException
extern TypeInfo NullReferenceException_t800_il2cpp_TypeInfo;
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor()
extern const MethodInfo NullReferenceException__ctor_m13726_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m13726/* method */
	, &NullReferenceException_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NullReferenceException_t800_NullReferenceException__ctor_m6905_ParameterInfos[] = 
{
	{"message", 0, 134224364, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.String)
extern const MethodInfo NullReferenceException__ctor_m6905_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m6905/* method */
	, &NullReferenceException_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NullReferenceException_t800_NullReferenceException__ctor_m6905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo NullReferenceException_t800_NullReferenceException__ctor_m13727_ParameterInfos[] = 
{
	{"info", 0, 134224365, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224366, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NullReferenceException__ctor_m13727_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m13727/* method */
	, &NullReferenceException_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, NullReferenceException_t800_NullReferenceException__ctor_m13727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NullReferenceException_t800_MethodInfos[] =
{
	&NullReferenceException__ctor_m13726_MethodInfo,
	&NullReferenceException__ctor_m6905_MethodInfo,
	&NullReferenceException__ctor_m13727_MethodInfo,
	NULL
};
static const Il2CppMethodReference NullReferenceException_t800_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool NullReferenceException_t800_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NullReferenceException_t800_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullReferenceException_t800_0_0_0;
extern const Il2CppType NullReferenceException_t800_1_0_0;
struct NullReferenceException_t800;
const Il2CppTypeDefinitionMetadata NullReferenceException_t800_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullReferenceException_t800_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, NullReferenceException_t800_VTable/* vtableMethods */
	, NullReferenceException_t800_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2419/* fieldStart */

};
TypeInfo NullReferenceException_t800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullReferenceException"/* name */
	, "System"/* namespaze */
	, NullReferenceException_t800_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NullReferenceException_t800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 914/* custom_attributes_cache */
	, &NullReferenceException_t800_0_0_0/* byval_arg */
	, &NullReferenceException_t800_1_0_0/* this_arg */
	, &NullReferenceException_t800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullReferenceException_t800)/* instance_size */
	, sizeof (NullReferenceException_t800)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t2538_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::.ctor()
extern const MethodInfo CustomInfo__ctor_m13728_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CustomInfo__ctor_m13728/* method */
	, &CustomInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_1_0_0;
extern const Il2CppType Boolean_t169_1_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_1_0_0;
static const ParameterInfo CustomInfo_t2538_CustomInfo_GetActiveSection_m13729_ParameterInfos[] = 
{
	{"format", 0, 134224517, 0, &String_t_0_0_0},
	{"positive", 1, 134224518, 0, &Boolean_t169_1_0_0},
	{"zero", 2, 134224519, 0, &Boolean_t169_0_0_0},
	{"offset", 3, 134224520, 0, &Int32_t127_1_0_0},
	{"length", 4, 134224521, 0, &Int32_t127_1_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_BooleanU26_t526_SByte_t170_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::GetActiveSection(System.String,System.Boolean&,System.Boolean,System.Int32&,System.Int32&)
extern const MethodInfo CustomInfo_GetActiveSection_m13729_MethodInfo = 
{
	"GetActiveSection"/* name */
	, (methodPointerType)&CustomInfo_GetActiveSection_m13729/* method */
	, &CustomInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_BooleanU26_t526_SByte_t170_Int32U26_t535_Int32U26_t535/* invoker_method */
	, CustomInfo_t2538_CustomInfo_GetActiveSection_m13729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo CustomInfo_t2538_CustomInfo_Parse_m13730_ParameterInfos[] = 
{
	{"format", 0, 134224522, 0, &String_t_0_0_0},
	{"offset", 1, 134224523, 0, &Int32_t127_0_0_0},
	{"length", 2, 134224524, 0, &Int32_t127_0_0_0},
	{"nfi", 3, 134224525, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern const Il2CppType CustomInfo_t2538_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter/CustomInfo System.NumberFormatter/CustomInfo::Parse(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo CustomInfo_Parse_m13730_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&CustomInfo_Parse_m13730/* method */
	, &CustomInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &CustomInfo_t2538_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_Object_t/* invoker_method */
	, CustomInfo_t2538_CustomInfo_Parse_m13730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo CustomInfo_t2538_CustomInfo_Format_m13731_ParameterInfos[] = 
{
	{"format", 0, 134224526, 0, &String_t_0_0_0},
	{"offset", 1, 134224527, 0, &Int32_t127_0_0_0},
	{"length", 2, 134224528, 0, &Int32_t127_0_0_0},
	{"nfi", 3, 134224529, 0, &NumberFormatInfo_t2170_0_0_0},
	{"positive", 4, 134224530, 0, &Boolean_t169_0_0_0},
	{"sb_int", 5, 134224531, 0, &StringBuilder_t423_0_0_0},
	{"sb_dec", 6, 134224532, 0, &StringBuilder_t423_0_0_0},
	{"sb_exp", 7, 134224533, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_Object_t_SByte_t170_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter/CustomInfo::Format(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo,System.Boolean,System.Text.StringBuilder,System.Text.StringBuilder,System.Text.StringBuilder)
extern const MethodInfo CustomInfo_Format_m13731_MethodInfo = 
{
	"Format"/* name */
	, (methodPointerType)&CustomInfo_Format_m13731/* method */
	, &CustomInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_Object_t_SByte_t170_Object_t_Object_t_Object_t/* invoker_method */
	, CustomInfo_t2538_CustomInfo_Format_m13731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CustomInfo_t2538_MethodInfos[] =
{
	&CustomInfo__ctor_m13728_MethodInfo,
	&CustomInfo_GetActiveSection_m13729_MethodInfo,
	&CustomInfo_Parse_m13730_MethodInfo,
	&CustomInfo_Format_m13731_MethodInfo,
	NULL
};
static const Il2CppMethodReference CustomInfo_t2538_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CustomInfo_t2538_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t2538_1_0_0;
extern TypeInfo NumberFormatter_t2539_il2cpp_TypeInfo;
extern const Il2CppType NumberFormatter_t2539_0_0_0;
struct CustomInfo_t2538;
const Il2CppTypeDefinitionMetadata CustomInfo_t2538_DefinitionMetadata = 
{
	&NumberFormatter_t2539_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t2538_VTable/* vtableMethods */
	, CustomInfo_t2538_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2420/* fieldStart */

};
TypeInfo CustomInfo_t2538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, CustomInfo_t2538_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CustomInfo_t2538_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t2538_0_0_0/* byval_arg */
	, &CustomInfo_t2538_1_0_0/* this_arg */
	, &CustomInfo_t2538_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t2538)/* instance_size */
	, sizeof (CustomInfo_t2538)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType Thread_t2308_0_0_0;
extern const Il2CppType Thread_t2308_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter__ctor_m13732_ParameterInfos[] = 
{
	{"current", 0, 134224367, 0, &Thread_t2308_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.ctor(System.Threading.Thread)
extern const MethodInfo NumberFormatter__ctor_m13732_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NumberFormatter__ctor_m13732/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter__ctor_m13732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.cctor()
extern const MethodInfo NumberFormatter__cctor_m13733_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&NumberFormatter__cctor_m13733/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64U2A_t3079_1_0_2;
extern const Il2CppType UInt64U2A_t3079_1_0_0;
extern const Il2CppType Int32U2A_t3080_1_0_2;
extern const Il2CppType Int32U2A_t3080_1_0_0;
extern const Il2CppType CharU2A_t2731_1_0_2;
extern const Il2CppType CharU2A_t2731_1_0_0;
extern const Il2CppType CharU2A_t2731_1_0_2;
extern const Il2CppType Int64U2A_t3081_1_0_2;
extern const Il2CppType Int64U2A_t3081_1_0_0;
extern const Il2CppType Int32U2A_t3080_1_0_2;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_GetFormatterTables_m13734_ParameterInfos[] = 
{
	{"MantissaBitsTable", 0, 134224368, 0, &UInt64U2A_t3079_1_0_2},
	{"TensExponentTable", 1, 134224369, 0, &Int32U2A_t3080_1_0_2},
	{"DigitLowerTable", 2, 134224370, 0, &CharU2A_t2731_1_0_2},
	{"DigitUpperTable", 3, 134224371, 0, &CharU2A_t2731_1_0_2},
	{"TenPowersList", 4, 134224372, 0, &Int64U2A_t3081_1_0_2},
	{"DecHexDigits", 5, 134224373, 0, &Int32U2A_t3080_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_UInt64U2AU26_t3082_Int32U2AU26_t3083_CharU2AU26_t3084_CharU2AU26_t3084_Int64U2AU26_t3085_Int32U2AU26_t3083 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::GetFormatterTables(System.UInt64*&,System.Int32*&,System.Char*&,System.Char*&,System.Int64*&,System.Int32*&)
extern const MethodInfo NumberFormatter_GetFormatterTables_m13734_MethodInfo = 
{
	"GetFormatterTables"/* name */
	, (methodPointerType)&NumberFormatter_GetFormatterTables_m13734/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt64U2AU26_t3082_Int32U2AU26_t3083_CharU2AU26_t3084_CharU2AU26_t3084_Int64U2AU26_t3085_Int32U2AU26_t3083/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_GetFormatterTables_m13734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_GetTenPowerOf_m13735_ParameterInfos[] = 
{
	{"i", 0, 134224374, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.NumberFormatter::GetTenPowerOf(System.Int32)
extern const MethodInfo NumberFormatter_GetTenPowerOf_m13735_MethodInfo = 
{
	"GetTenPowerOf"/* name */
	, (methodPointerType)&NumberFormatter_GetTenPowerOf_m13735/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_GetTenPowerOf_m13735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13736_ParameterInfos[] = 
{
	{"value", 0, 134224375, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13736_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13736/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13737_ParameterInfos[] = 
{
	{"value", 0, 134224376, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13737_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13737/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13738_ParameterInfos[] = 
{
	{"hi", 0, 134224377, 0, &UInt32_t1075_0_0_0},
	{"lo", 1, 134224378, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32,System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13738_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13738/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_InitDecHexDigits_m13738_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FastToDecHex_m13739_ParameterInfos[] = 
{
	{"val", 0, 134224379, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1075_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::FastToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_FastToDecHex_m13739_MethodInfo = 
{
	"FastToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_FastToDecHex_m13739/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FastToDecHex_m13739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_ToDecHex_m13740_ParameterInfos[] = 
{
	{"val", 0, 134224380, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1075_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::ToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_ToDecHex_m13740_MethodInfo = 
{
	"ToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_ToDecHex_m13740/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_ToDecHex_m13740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FastDecHexLen_m13741_ParameterInfos[] = 
{
	{"val", 0, 134224381, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::FastDecHexLen(System.Int32)
extern const MethodInfo NumberFormatter_FastDecHexLen_m13741_MethodInfo = 
{
	"FastDecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_FastDecHexLen_m13741/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FastDecHexLen_m13741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_DecHexLen_m13742_ParameterInfos[] = 
{
	{"val", 0, 134224382, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen(System.UInt32)
extern const MethodInfo NumberFormatter_DecHexLen_m13742_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m13742/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_DecHexLen_m13742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen()
extern const MethodInfo NumberFormatter_DecHexLen_m13743_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m13743/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_ScaleOrder_m13744_ParameterInfos[] = 
{
	{"hi", 0, 134224383, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ScaleOrder(System.Int64)
extern const MethodInfo NumberFormatter_ScaleOrder_m13744_MethodInfo = 
{
	"ScaleOrder"/* name */
	, (methodPointerType)&NumberFormatter_ScaleOrder_m13744/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_ScaleOrder_m13744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::InitialFloatingPrecision()
extern const MethodInfo NumberFormatter_InitialFloatingPrecision_m13745_MethodInfo = 
{
	"InitialFloatingPrecision"/* name */
	, (methodPointerType)&NumberFormatter_InitialFloatingPrecision_m13745/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_ParsePrecision_m13746_ParameterInfos[] = 
{
	{"format", 0, 134224384, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ParsePrecision(System.String)
extern const MethodInfo NumberFormatter_ParsePrecision_m13746_MethodInfo = 
{
	"ParsePrecision"/* name */
	, (methodPointerType)&NumberFormatter_ParsePrecision_m13746/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_ParsePrecision_m13746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13747_ParameterInfos[] = 
{
	{"format", 0, 134224385, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String)
extern const MethodInfo NumberFormatter_Init_m13747_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13747/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_InitHex_m13748_ParameterInfos[] = 
{
	{"value", 0, 134224386, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitHex(System.UInt64)
extern const MethodInfo NumberFormatter_InitHex_m13748_MethodInfo = 
{
	"InitHex"/* name */
	, (methodPointerType)&NumberFormatter_InitHex_m13748/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_InitHex_m13748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13749_ParameterInfos[] = 
{
	{"format", 0, 134224387, 0, &String_t_0_0_0},
	{"value", 1, 134224388, 0, &Int32_t127_0_0_0},
	{"defPrecision", 2, 134224389, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13749_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13749/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13750_ParameterInfos[] = 
{
	{"format", 0, 134224390, 0, &String_t_0_0_0},
	{"value", 1, 134224391, 0, &UInt32_t1075_0_0_0},
	{"defPrecision", 2, 134224392, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13750_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13750/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13751_ParameterInfos[] = 
{
	{"format", 0, 134224393, 0, &String_t_0_0_0},
	{"value", 1, 134224394, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int64)
extern const MethodInfo NumberFormatter_Init_m13751_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13751/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13752_ParameterInfos[] = 
{
	{"format", 0, 134224395, 0, &String_t_0_0_0},
	{"value", 1, 134224396, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt64)
extern const MethodInfo NumberFormatter_Init_m13752_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13752/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64_t1092/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13753_ParameterInfos[] = 
{
	{"format", 0, 134224397, 0, &String_t_0_0_0},
	{"value", 1, 134224398, 0, &Double_t1407_0_0_0},
	{"defPrecision", 2, 134224399, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Double_t1407_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Double,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13753_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13753/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Double_t1407_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1059_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Init_m13754_ParameterInfos[] = 
{
	{"format", 0, 134224400, 0, &String_t_0_0_0},
	{"value", 1, 134224401, 0, &Decimal_t1059_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Decimal_t1059 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Decimal)
extern const MethodInfo NumberFormatter_Init_m13754_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13754/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Decimal_t1059/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Init_m13754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_ResetCharBuf_m13755_ParameterInfos[] = 
{
	{"size", 0, 134224402, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ResetCharBuf(System.Int32)
extern const MethodInfo NumberFormatter_ResetCharBuf_m13755_MethodInfo = 
{
	"ResetCharBuf"/* name */
	, (methodPointerType)&NumberFormatter_ResetCharBuf_m13755/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_ResetCharBuf_m13755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Resize_m13756_ParameterInfos[] = 
{
	{"len", 0, 134224403, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Resize(System.Int32)
extern const MethodInfo NumberFormatter_Resize_m13756_MethodInfo = 
{
	"Resize"/* name */
	, (methodPointerType)&NumberFormatter_Resize_m13756/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Resize_m13756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Append_m13757_ParameterInfos[] = 
{
	{"c", 0, 134224404, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char)
extern const MethodInfo NumberFormatter_Append_m13757_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13757/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Append_m13757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Append_m13758_ParameterInfos[] = 
{
	{"c", 0, 134224405, 0, &Char_t451_0_0_0},
	{"cnt", 1, 134224406, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char,System.Int32)
extern const MethodInfo NumberFormatter_Append_m13758_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13758/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Append_m13758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Append_m13759_ParameterInfos[] = 
{
	{"s", 0, 134224407, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.String)
extern const MethodInfo NumberFormatter_Append_m13759_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13759/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Append_m13759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_GetNumberFormatInstance_m13760_ParameterInfos[] = 
{
	{"fp", 0, 134224408, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.NumberFormatInfo System.NumberFormatter::GetNumberFormatInstance(System.IFormatProvider)
extern const MethodInfo NumberFormatter_GetNumberFormatInstance_m13760_MethodInfo = 
{
	"GetNumberFormatInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetNumberFormatInstance_m13760/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatInfo_t2170_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_GetNumberFormatInstance_m13760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1405_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_set_CurrentCulture_m13761_ParameterInfos[] = 
{
	{"value", 0, 134224409, 0, &CultureInfo_t1405_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::set_CurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_set_CurrentCulture_m13761_MethodInfo = 
{
	"set_CurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_set_CurrentCulture_m13761/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_set_CurrentCulture_m13761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_IntegerDigits()
extern const MethodInfo NumberFormatter_get_IntegerDigits_m13762_MethodInfo = 
{
	"get_IntegerDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_IntegerDigits_m13762/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_DecimalDigits()
extern const MethodInfo NumberFormatter_get_DecimalDigits_m13763_MethodInfo = 
{
	"get_DecimalDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_DecimalDigits_m13763/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsFloatingSource()
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m13764_MethodInfo = 
{
	"get_IsFloatingSource"/* name */
	, (methodPointerType)&NumberFormatter_get_IsFloatingSource_m13764/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZero()
extern const MethodInfo NumberFormatter_get_IsZero_m13765_MethodInfo = 
{
	"get_IsZero"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZero_m13765/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZeroInteger()
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m13766_MethodInfo = 
{
	"get_IsZeroInteger"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZeroInteger_m13766/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_RoundPos_m13767_ParameterInfos[] = 
{
	{"pos", 0, 134224410, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RoundPos(System.Int32)
extern const MethodInfo NumberFormatter_RoundPos_m13767_MethodInfo = 
{
	"RoundPos"/* name */
	, (methodPointerType)&NumberFormatter_RoundPos_m13767/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_RoundPos_m13767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_RoundDecimal_m13768_ParameterInfos[] = 
{
	{"decimals", 0, 134224411, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundDecimal(System.Int32)
extern const MethodInfo NumberFormatter_RoundDecimal_m13768_MethodInfo = 
{
	"RoundDecimal"/* name */
	, (methodPointerType)&NumberFormatter_RoundDecimal_m13768/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_RoundDecimal_m13768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_RoundBits_m13769_ParameterInfos[] = 
{
	{"shift", 0, 134224412, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundBits(System.Int32)
extern const MethodInfo NumberFormatter_RoundBits_m13769_MethodInfo = 
{
	"RoundBits"/* name */
	, (methodPointerType)&NumberFormatter_RoundBits_m13769/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_RoundBits_m13769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RemoveTrailingZeros()
extern const MethodInfo NumberFormatter_RemoveTrailingZeros_m13770_MethodInfo = 
{
	"RemoveTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_RemoveTrailingZeros_m13770/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AddOneToDecHex()
extern const MethodInfo NumberFormatter_AddOneToDecHex_m13771_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m13771/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AddOneToDecHex_m13772_ParameterInfos[] = 
{
	{"val", 0, 134224413, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1075_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::AddOneToDecHex(System.UInt32)
extern const MethodInfo NumberFormatter_AddOneToDecHex_m13772_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m13772/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AddOneToDecHex_m13772_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros()
extern const MethodInfo NumberFormatter_CountTrailingZeros_m13773_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m13773/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_CountTrailingZeros_m13774_ParameterInfos[] = 
{
	{"val", 0, 134224414, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros(System.UInt32)
extern const MethodInfo NumberFormatter_CountTrailingZeros_m13774_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m13774/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_CountTrailingZeros_m13774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetInstance()
extern const MethodInfo NumberFormatter_GetInstance_m13775_MethodInfo = 
{
	"GetInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetInstance_m13775/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2539_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Release()
extern const MethodInfo NumberFormatter_Release_m13776_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&NumberFormatter_Release_m13776/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1405_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_SetThreadCurrentCulture_m13777_ParameterInfos[] = 
{
	{"culture", 0, 134224415, 0, &CultureInfo_t1405_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::SetThreadCurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_SetThreadCurrentCulture_m13777_MethodInfo = 
{
	"SetThreadCurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_SetThreadCurrentCulture_m13777/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_SetThreadCurrentCulture_m13777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType SByte_t170_0_0_0;
extern const Il2CppType SByte_t170_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13778_ParameterInfos[] = 
{
	{"format", 0, 134224416, 0, &String_t_0_0_0},
	{"value", 1, 134224417, 0, &SByte_t170_0_0_0},
	{"fp", 2, 134224418, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.SByte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13778_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13778/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Byte_t449_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13779_ParameterInfos[] = 
{
	{"format", 0, 134224419, 0, &String_t_0_0_0},
	{"value", 1, 134224420, 0, &Byte_t449_0_0_0},
	{"fp", 2, 134224421, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Byte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13779_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13779/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt16_t454_0_0_0;
extern const Il2CppType UInt16_t454_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13780_ParameterInfos[] = 
{
	{"format", 0, 134224422, 0, &String_t_0_0_0},
	{"value", 1, 134224423, 0, &UInt16_t454_0_0_0},
	{"fp", 2, 134224424, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13780_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13780/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13781_ParameterInfos[] = 
{
	{"format", 0, 134224425, 0, &String_t_0_0_0},
	{"value", 1, 134224426, 0, &Int16_t534_0_0_0},
	{"fp", 2, 134224427, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13781_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13781/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t534_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13782_ParameterInfos[] = 
{
	{"format", 0, 134224428, 0, &String_t_0_0_0},
	{"value", 1, 134224429, 0, &UInt32_t1075_0_0_0},
	{"fp", 2, 134224430, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13782_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13782/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13783_ParameterInfos[] = 
{
	{"format", 0, 134224431, 0, &String_t_0_0_0},
	{"value", 1, 134224432, 0, &Int32_t127_0_0_0},
	{"fp", 2, 134224433, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13783_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13783/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13784_ParameterInfos[] = 
{
	{"format", 0, 134224434, 0, &String_t_0_0_0},
	{"value", 1, 134224435, 0, &UInt64_t1091_0_0_0},
	{"fp", 2, 134224436, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13784_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13784/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13785_ParameterInfos[] = 
{
	{"format", 0, 134224437, 0, &String_t_0_0_0},
	{"value", 1, 134224438, 0, &Int64_t1092_0_0_0},
	{"fp", 2, 134224439, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13785_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13785/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1092_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13786_ParameterInfos[] = 
{
	{"format", 0, 134224440, 0, &String_t_0_0_0},
	{"value", 1, 134224441, 0, &Single_t151_0_0_0},
	{"fp", 2, 134224442, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13786_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13786/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13787_ParameterInfos[] = 
{
	{"format", 0, 134224443, 0, &String_t_0_0_0},
	{"value", 1, 134224444, 0, &Double_t1407_0_0_0},
	{"fp", 2, 134224445, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Double_t1407_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13787_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13787/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Double_t1407_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1059_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13788_ParameterInfos[] = 
{
	{"format", 0, 134224446, 0, &String_t_0_0_0},
	{"value", 1, 134224447, 0, &Decimal_t1059_0_0_0},
	{"fp", 2, 134224448, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Decimal_t1059_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Decimal,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13788_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13788/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Decimal_t1059_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13789_ParameterInfos[] = 
{
	{"value", 0, 134224449, 0, &UInt32_t1075_0_0_0},
	{"fp", 1, 134224450, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13789_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13789/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13790_ParameterInfos[] = 
{
	{"value", 0, 134224451, 0, &Int32_t127_0_0_0},
	{"fp", 1, 134224452, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13790_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13790/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13791_ParameterInfos[] = 
{
	{"value", 0, 134224453, 0, &UInt64_t1091_0_0_0},
	{"fp", 1, 134224454, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13791_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13791/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1092_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13792_ParameterInfos[] = 
{
	{"value", 0, 134224455, 0, &Int64_t1092_0_0_0},
	{"fp", 1, 134224456, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1092_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13792_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13792/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1092_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13793_ParameterInfos[] = 
{
	{"value", 0, 134224457, 0, &Single_t151_0_0_0},
	{"fp", 1, 134224458, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13793_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13793/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t151_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13794_ParameterInfos[] = 
{
	{"value", 0, 134224459, 0, &Double_t1407_0_0_0},
	{"fp", 1, 134224460, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1407_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13794_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13794/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1407_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FastIntegerToString_m13795_ParameterInfos[] = 
{
	{"value", 0, 134224461, 0, &Int32_t127_0_0_0},
	{"fp", 1, 134224462, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FastIntegerToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_FastIntegerToString_m13795_MethodInfo = 
{
	"FastIntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_FastIntegerToString_m13795/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FastIntegerToString_m13795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t2583_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_IntegerToString_m13796_ParameterInfos[] = 
{
	{"format", 0, 134224463, 0, &String_t_0_0_0},
	{"fp", 1, 134224464, 0, &IFormatProvider_t2583_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::IntegerToString(System.String,System.IFormatProvider)
extern const MethodInfo NumberFormatter_IntegerToString_m13796_MethodInfo = 
{
	"IntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_IntegerToString_m13796/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_IntegerToString_m13796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_NumberToString_m13797_ParameterInfos[] = 
{
	{"format", 0, 134224465, 0, &String_t_0_0_0},
	{"nfi", 1, 134224466, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_NumberToString_m13797_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13797/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_NumberToString_m13797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatCurrency_m13798_ParameterInfos[] = 
{
	{"precision", 0, 134224467, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224468, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCurrency(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCurrency_m13798_MethodInfo = 
{
	"FormatCurrency"/* name */
	, (methodPointerType)&NumberFormatter_FormatCurrency_m13798/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatCurrency_m13798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatDecimal_m13799_ParameterInfos[] = 
{
	{"precision", 0, 134224469, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224470, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatDecimal(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatDecimal_m13799_MethodInfo = 
{
	"FormatDecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatDecimal_m13799/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatDecimal_m13799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatHexadecimal_m13800_ParameterInfos[] = 
{
	{"precision", 0, 134224471, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatHexadecimal(System.Int32)
extern const MethodInfo NumberFormatter_FormatHexadecimal_m13800_MethodInfo = 
{
	"FormatHexadecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatHexadecimal_m13800/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatHexadecimal_m13800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatFixedPoint_m13801_ParameterInfos[] = 
{
	{"precision", 0, 134224472, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224473, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatFixedPoint(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatFixedPoint_m13801_MethodInfo = 
{
	"FormatFixedPoint"/* name */
	, (methodPointerType)&NumberFormatter_FormatFixedPoint_m13801/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatFixedPoint_m13801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatRoundtrip_m13802_ParameterInfos[] = 
{
	{"origval", 0, 134224474, 0, &Double_t1407_0_0_0},
	{"nfi", 1, 134224475, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1407_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Double,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m13802_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m13802/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1407_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatRoundtrip_m13802_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatRoundtrip_m13803_ParameterInfos[] = 
{
	{"origval", 0, 134224476, 0, &Single_t151_0_0_0},
	{"nfi", 1, 134224477, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Single,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m13803_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m13803/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t151_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatRoundtrip_m13803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatGeneral_m13804_ParameterInfos[] = 
{
	{"precision", 0, 134224478, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224479, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatGeneral(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatGeneral_m13804_MethodInfo = 
{
	"FormatGeneral"/* name */
	, (methodPointerType)&NumberFormatter_FormatGeneral_m13804/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatGeneral_m13804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatNumber_m13805_ParameterInfos[] = 
{
	{"precision", 0, 134224480, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224481, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatNumber(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatNumber_m13805_MethodInfo = 
{
	"FormatNumber"/* name */
	, (methodPointerType)&NumberFormatter_FormatNumber_m13805/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatNumber_m13805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatPercent_m13806_ParameterInfos[] = 
{
	{"precision", 0, 134224482, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224483, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatPercent(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatPercent_m13806_MethodInfo = 
{
	"FormatPercent"/* name */
	, (methodPointerType)&NumberFormatter_FormatPercent_m13806/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatPercent_m13806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatExponential_m13807_ParameterInfos[] = 
{
	{"precision", 0, 134224484, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224485, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatExponential_m13807_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m13807/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatExponential_m13807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatExponential_m13808_ParameterInfos[] = 
{
	{"precision", 0, 134224486, 0, &Int32_t127_0_0_0},
	{"nfi", 1, 134224487, 0, &NumberFormatInfo_t2170_0_0_0},
	{"expDigits", 2, 134224488, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo,System.Int32)
extern const MethodInfo NumberFormatter_FormatExponential_m13808_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m13808/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatExponential_m13808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FormatCustom_m13809_ParameterInfos[] = 
{
	{"format", 0, 134224489, 0, &String_t_0_0_0},
	{"nfi", 1, 134224490, 0, &NumberFormatInfo_t2170_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCustom(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCustom_m13809_MethodInfo = 
{
	"FormatCustom"/* name */
	, (methodPointerType)&NumberFormatter_FormatCustom_m13809/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FormatCustom_m13809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_ZeroTrimEnd_m13810_ParameterInfos[] = 
{
	{"sb", 0, 134224491, 0, &StringBuilder_t423_0_0_0},
	{"canEmpty", 1, 134224492, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ZeroTrimEnd(System.Text.StringBuilder,System.Boolean)
extern const MethodInfo NumberFormatter_ZeroTrimEnd_m13810_MethodInfo = 
{
	"ZeroTrimEnd"/* name */
	, (methodPointerType)&NumberFormatter_ZeroTrimEnd_m13810/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_ZeroTrimEnd_m13810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_IsZeroOnly_m13811_ParameterInfos[] = 
{
	{"sb", 0, 134224493, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::IsZeroOnly(System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_IsZeroOnly_m13811_MethodInfo = 
{
	"IsZeroOnly"/* name */
	, (methodPointerType)&NumberFormatter_IsZeroOnly_m13811/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_IsZeroOnly_m13811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendNonNegativeNumber_m13812_ParameterInfos[] = 
{
	{"sb", 0, 134224494, 0, &StringBuilder_t423_0_0_0},
	{"v", 1, 134224495, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendNonNegativeNumber(System.Text.StringBuilder,System.Int32)
extern const MethodInfo NumberFormatter_AppendNonNegativeNumber_m13812_MethodInfo = 
{
	"AppendNonNegativeNumber"/* name */
	, (methodPointerType)&NumberFormatter_AppendNonNegativeNumber_m13812/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendNonNegativeNumber_m13812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendIntegerString_m13813_ParameterInfos[] = 
{
	{"minLength", 0, 134224496, 0, &Int32_t127_0_0_0},
	{"sb", 1, 134224497, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendIntegerString_m13813_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m13813/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendIntegerString_m13813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendIntegerString_m13814_ParameterInfos[] = 
{
	{"minLength", 0, 134224498, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32)
extern const MethodInfo NumberFormatter_AppendIntegerString_m13814_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m13814/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendIntegerString_m13814_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendDecimalString_m13815_ParameterInfos[] = 
{
	{"precision", 0, 134224499, 0, &Int32_t127_0_0_0},
	{"sb", 1, 134224500, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDecimalString_m13815_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m13815/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendDecimalString_m13815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendDecimalString_m13816_ParameterInfos[] = 
{
	{"precision", 0, 134224501, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32)
extern const MethodInfo NumberFormatter_AppendDecimalString_m13816_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m13816/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendDecimalString_m13816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendIntegerStringWithGroupSeparator_m13817_ParameterInfos[] = 
{
	{"groups", 0, 134224502, 0, &Int32U5BU5D_t19_0_0_0},
	{"groupSeparator", 1, 134224503, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerStringWithGroupSeparator(System.Int32[],System.String)
extern const MethodInfo NumberFormatter_AppendIntegerStringWithGroupSeparator_m13817_MethodInfo = 
{
	"AppendIntegerStringWithGroupSeparator"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerStringWithGroupSeparator_m13817/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendIntegerStringWithGroupSeparator_m13817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NumberFormatInfo_t2170_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendExponent_m13818_ParameterInfos[] = 
{
	{"nfi", 0, 134224504, 0, &NumberFormatInfo_t2170_0_0_0},
	{"exponent", 1, 134224505, 0, &Int32_t127_0_0_0},
	{"minDigits", 2, 134224506, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendExponent(System.Globalization.NumberFormatInfo,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendExponent_m13818_MethodInfo = 
{
	"AppendExponent"/* name */
	, (methodPointerType)&NumberFormatter_AppendExponent_m13818/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendExponent_m13818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendOneDigit_m13819_ParameterInfos[] = 
{
	{"start", 0, 134224507, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendOneDigit(System.Int32)
extern const MethodInfo NumberFormatter_AppendOneDigit_m13819_MethodInfo = 
{
	"AppendOneDigit"/* name */
	, (methodPointerType)&NumberFormatter_AppendOneDigit_m13819/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendOneDigit_m13819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_FastAppendDigits_m13820_ParameterInfos[] = 
{
	{"val", 0, 134224508, 0, &Int32_t127_0_0_0},
	{"force", 1, 134224509, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::FastAppendDigits(System.Int32,System.Boolean)
extern const MethodInfo NumberFormatter_FastAppendDigits_m13820_MethodInfo = 
{
	"FastAppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_FastAppendDigits_m13820/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_FastAppendDigits_m13820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendDigits_m13821_ParameterInfos[] = 
{
	{"start", 0, 134224510, 0, &Int32_t127_0_0_0},
	{"end", 1, 134224511, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendDigits_m13821_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m13821/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendDigits_m13821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_AppendDigits_m13822_ParameterInfos[] = 
{
	{"start", 0, 134224512, 0, &Int32_t127_0_0_0},
	{"end", 1, 134224513, 0, &Int32_t127_0_0_0},
	{"sb", 2, 134224514, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDigits_m13822_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m13822/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Object_t/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_AppendDigits_m13822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Multiply10_m13823_ParameterInfos[] = 
{
	{"count", 0, 134224515, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Multiply10(System.Int32)
extern const MethodInfo NumberFormatter_Multiply10_m13823_MethodInfo = 
{
	"Multiply10"/* name */
	, (methodPointerType)&NumberFormatter_Multiply10_m13823/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Multiply10_m13823_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo NumberFormatter_t2539_NumberFormatter_Divide10_m13824_ParameterInfos[] = 
{
	{"count", 0, 134224516, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Divide10(System.Int32)
extern const MethodInfo NumberFormatter_Divide10_m13824_MethodInfo = 
{
	"Divide10"/* name */
	, (methodPointerType)&NumberFormatter_Divide10_m13824/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, NumberFormatter_t2539_NumberFormatter_Divide10_m13824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetClone()
extern const MethodInfo NumberFormatter_GetClone_m13825_MethodInfo = 
{
	"GetClone"/* name */
	, (methodPointerType)&NumberFormatter_GetClone_m13825/* method */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2539_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NumberFormatter_t2539_MethodInfos[] =
{
	&NumberFormatter__ctor_m13732_MethodInfo,
	&NumberFormatter__cctor_m13733_MethodInfo,
	&NumberFormatter_GetFormatterTables_m13734_MethodInfo,
	&NumberFormatter_GetTenPowerOf_m13735_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13736_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13737_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13738_MethodInfo,
	&NumberFormatter_FastToDecHex_m13739_MethodInfo,
	&NumberFormatter_ToDecHex_m13740_MethodInfo,
	&NumberFormatter_FastDecHexLen_m13741_MethodInfo,
	&NumberFormatter_DecHexLen_m13742_MethodInfo,
	&NumberFormatter_DecHexLen_m13743_MethodInfo,
	&NumberFormatter_ScaleOrder_m13744_MethodInfo,
	&NumberFormatter_InitialFloatingPrecision_m13745_MethodInfo,
	&NumberFormatter_ParsePrecision_m13746_MethodInfo,
	&NumberFormatter_Init_m13747_MethodInfo,
	&NumberFormatter_InitHex_m13748_MethodInfo,
	&NumberFormatter_Init_m13749_MethodInfo,
	&NumberFormatter_Init_m13750_MethodInfo,
	&NumberFormatter_Init_m13751_MethodInfo,
	&NumberFormatter_Init_m13752_MethodInfo,
	&NumberFormatter_Init_m13753_MethodInfo,
	&NumberFormatter_Init_m13754_MethodInfo,
	&NumberFormatter_ResetCharBuf_m13755_MethodInfo,
	&NumberFormatter_Resize_m13756_MethodInfo,
	&NumberFormatter_Append_m13757_MethodInfo,
	&NumberFormatter_Append_m13758_MethodInfo,
	&NumberFormatter_Append_m13759_MethodInfo,
	&NumberFormatter_GetNumberFormatInstance_m13760_MethodInfo,
	&NumberFormatter_set_CurrentCulture_m13761_MethodInfo,
	&NumberFormatter_get_IntegerDigits_m13762_MethodInfo,
	&NumberFormatter_get_DecimalDigits_m13763_MethodInfo,
	&NumberFormatter_get_IsFloatingSource_m13764_MethodInfo,
	&NumberFormatter_get_IsZero_m13765_MethodInfo,
	&NumberFormatter_get_IsZeroInteger_m13766_MethodInfo,
	&NumberFormatter_RoundPos_m13767_MethodInfo,
	&NumberFormatter_RoundDecimal_m13768_MethodInfo,
	&NumberFormatter_RoundBits_m13769_MethodInfo,
	&NumberFormatter_RemoveTrailingZeros_m13770_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m13771_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m13772_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m13773_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m13774_MethodInfo,
	&NumberFormatter_GetInstance_m13775_MethodInfo,
	&NumberFormatter_Release_m13776_MethodInfo,
	&NumberFormatter_SetThreadCurrentCulture_m13777_MethodInfo,
	&NumberFormatter_NumberToString_m13778_MethodInfo,
	&NumberFormatter_NumberToString_m13779_MethodInfo,
	&NumberFormatter_NumberToString_m13780_MethodInfo,
	&NumberFormatter_NumberToString_m13781_MethodInfo,
	&NumberFormatter_NumberToString_m13782_MethodInfo,
	&NumberFormatter_NumberToString_m13783_MethodInfo,
	&NumberFormatter_NumberToString_m13784_MethodInfo,
	&NumberFormatter_NumberToString_m13785_MethodInfo,
	&NumberFormatter_NumberToString_m13786_MethodInfo,
	&NumberFormatter_NumberToString_m13787_MethodInfo,
	&NumberFormatter_NumberToString_m13788_MethodInfo,
	&NumberFormatter_NumberToString_m13789_MethodInfo,
	&NumberFormatter_NumberToString_m13790_MethodInfo,
	&NumberFormatter_NumberToString_m13791_MethodInfo,
	&NumberFormatter_NumberToString_m13792_MethodInfo,
	&NumberFormatter_NumberToString_m13793_MethodInfo,
	&NumberFormatter_NumberToString_m13794_MethodInfo,
	&NumberFormatter_FastIntegerToString_m13795_MethodInfo,
	&NumberFormatter_IntegerToString_m13796_MethodInfo,
	&NumberFormatter_NumberToString_m13797_MethodInfo,
	&NumberFormatter_FormatCurrency_m13798_MethodInfo,
	&NumberFormatter_FormatDecimal_m13799_MethodInfo,
	&NumberFormatter_FormatHexadecimal_m13800_MethodInfo,
	&NumberFormatter_FormatFixedPoint_m13801_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m13802_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m13803_MethodInfo,
	&NumberFormatter_FormatGeneral_m13804_MethodInfo,
	&NumberFormatter_FormatNumber_m13805_MethodInfo,
	&NumberFormatter_FormatPercent_m13806_MethodInfo,
	&NumberFormatter_FormatExponential_m13807_MethodInfo,
	&NumberFormatter_FormatExponential_m13808_MethodInfo,
	&NumberFormatter_FormatCustom_m13809_MethodInfo,
	&NumberFormatter_ZeroTrimEnd_m13810_MethodInfo,
	&NumberFormatter_IsZeroOnly_m13811_MethodInfo,
	&NumberFormatter_AppendNonNegativeNumber_m13812_MethodInfo,
	&NumberFormatter_AppendIntegerString_m13813_MethodInfo,
	&NumberFormatter_AppendIntegerString_m13814_MethodInfo,
	&NumberFormatter_AppendDecimalString_m13815_MethodInfo,
	&NumberFormatter_AppendDecimalString_m13816_MethodInfo,
	&NumberFormatter_AppendIntegerStringWithGroupSeparator_m13817_MethodInfo,
	&NumberFormatter_AppendExponent_m13818_MethodInfo,
	&NumberFormatter_AppendOneDigit_m13819_MethodInfo,
	&NumberFormatter_FastAppendDigits_m13820_MethodInfo,
	&NumberFormatter_AppendDigits_m13821_MethodInfo,
	&NumberFormatter_AppendDigits_m13822_MethodInfo,
	&NumberFormatter_Multiply10_m13823_MethodInfo,
	&NumberFormatter_Divide10_m13824_MethodInfo,
	&NumberFormatter_GetClone_m13825_MethodInfo,
	NULL
};
extern const MethodInfo NumberFormatter_set_CurrentCulture_m13761_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____CurrentCulture_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "CurrentCulture"/* name */
	, NULL/* get */
	, &NumberFormatter_set_CurrentCulture_m13761_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IntegerDigits_m13762_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____IntegerDigits_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "IntegerDigits"/* name */
	, &NumberFormatter_get_IntegerDigits_m13762_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_DecimalDigits_m13763_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____DecimalDigits_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "DecimalDigits"/* name */
	, &NumberFormatter_get_DecimalDigits_m13763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m13764_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____IsFloatingSource_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "IsFloatingSource"/* name */
	, &NumberFormatter_get_IsFloatingSource_m13764_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZero_m13765_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____IsZero_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "IsZero"/* name */
	, &NumberFormatter_get_IsZero_m13765_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m13766_MethodInfo;
static const PropertyInfo NumberFormatter_t2539____IsZeroInteger_PropertyInfo = 
{
	&NumberFormatter_t2539_il2cpp_TypeInfo/* parent */
	, "IsZeroInteger"/* name */
	, &NumberFormatter_get_IsZeroInteger_m13766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NumberFormatter_t2539_PropertyInfos[] =
{
	&NumberFormatter_t2539____CurrentCulture_PropertyInfo,
	&NumberFormatter_t2539____IntegerDigits_PropertyInfo,
	&NumberFormatter_t2539____DecimalDigits_PropertyInfo,
	&NumberFormatter_t2539____IsFloatingSource_PropertyInfo,
	&NumberFormatter_t2539____IsZero_PropertyInfo,
	&NumberFormatter_t2539____IsZeroInteger_PropertyInfo,
	NULL
};
static const Il2CppType* NumberFormatter_t2539_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t2538_0_0_0,
};
static const Il2CppMethodReference NumberFormatter_t2539_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool NumberFormatter_t2539_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t2539_1_0_0;
struct NumberFormatter_t2539;
const Il2CppTypeDefinitionMetadata NumberFormatter_t2539_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t2539_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t2539_VTable/* vtableMethods */
	, NumberFormatter_t2539_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2434/* fieldStart */

};
TypeInfo NumberFormatter_t2539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NumberFormatter_t2539_MethodInfos/* methods */
	, NumberFormatter_t2539_PropertyInfos/* properties */
	, NULL/* events */
	, &NumberFormatter_t2539_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t2539_0_0_0/* byval_arg */
	, &NumberFormatter_t2539_1_0_0/* this_arg */
	, &NumberFormatter_t2539_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t2539)/* instance_size */
	, sizeof (NumberFormatter_t2539)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t2539_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t2539_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t1809_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1809_ObjectDisposedException__ctor_m8268_ParameterInfos[] = 
{
	{"objectName", 0, 134224534, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern const MethodInfo ObjectDisposedException__ctor_m8268_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m8268/* method */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ObjectDisposedException_t1809_ObjectDisposedException__ctor_m8268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1809_ObjectDisposedException__ctor_m13826_ParameterInfos[] = 
{
	{"objectName", 0, 134224535, 0, &String_t_0_0_0},
	{"message", 1, 134224536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String,System.String)
extern const MethodInfo ObjectDisposedException__ctor_m13826_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m13826/* method */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ObjectDisposedException_t1809_ObjectDisposedException__ctor_m13826_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ObjectDisposedException_t1809_ObjectDisposedException__ctor_m13827_ParameterInfos[] = 
{
	{"info", 0, 134224537, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224538, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException__ctor_m13827_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m13827/* method */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ObjectDisposedException_t1809_ObjectDisposedException__ctor_m13827_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ObjectDisposedException::get_Message()
extern const MethodInfo ObjectDisposedException_get_Message_m13828_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&ObjectDisposedException_get_Message_m13828/* method */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo ObjectDisposedException_t1809_ObjectDisposedException_GetObjectData_m13829_ParameterInfos[] = 
{
	{"info", 0, 134224539, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224540, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException_GetObjectData_m13829_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjectDisposedException_GetObjectData_m13829/* method */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, ObjectDisposedException_t1809_ObjectDisposedException_GetObjectData_m13829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectDisposedException_t1809_MethodInfos[] =
{
	&ObjectDisposedException__ctor_m8268_MethodInfo,
	&ObjectDisposedException__ctor_m13826_MethodInfo,
	&ObjectDisposedException__ctor_m13827_MethodInfo,
	&ObjectDisposedException_get_Message_m13828_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13829_MethodInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_get_Message_m13828_MethodInfo;
static const PropertyInfo ObjectDisposedException_t1809____Message_PropertyInfo = 
{
	&ObjectDisposedException_t1809_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &ObjectDisposedException_get_Message_m13828_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectDisposedException_t1809_PropertyInfos[] =
{
	&ObjectDisposedException_t1809____Message_PropertyInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_GetObjectData_m13829_MethodInfo;
static const Il2CppMethodReference ObjectDisposedException_t1809_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13829_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&ObjectDisposedException_get_Message_m13828_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13829_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool ObjectDisposedException_t1809_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ObjectDisposedException_t1809_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t1809_0_0_0;
extern const Il2CppType ObjectDisposedException_t1809_1_0_0;
struct ObjectDisposedException_t1809;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t1809_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t1809_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t1828_0_0_0/* parent */
	, ObjectDisposedException_t1809_VTable/* vtableMethods */
	, ObjectDisposedException_t1809_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2460/* fieldStart */

};
TypeInfo ObjectDisposedException_t1809_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, ObjectDisposedException_t1809_MethodInfos/* methods */
	, ObjectDisposedException_t1809_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectDisposedException_t1809_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 916/* custom_attributes_cache */
	, &ObjectDisposedException_t1809_0_0_0/* byval_arg */
	, &ObjectDisposedException_t1809_1_0_0/* this_arg */
	, &ObjectDisposedException_t1809_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t1809)/* instance_size */
	, sizeof (ObjectDisposedException_t1809)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t2518_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
extern const Il2CppType PlatformID_t2542_0_0_0;
extern const Il2CppType PlatformID_t2542_0_0_0;
extern const Il2CppType Version_t1875_0_0_0;
extern const Il2CppType Version_t1875_0_0_0;
static const ParameterInfo OperatingSystem_t2518_OperatingSystem__ctor_m13830_ParameterInfos[] = 
{
	{"platform", 0, 134224541, 0, &PlatformID_t2542_0_0_0},
	{"version", 1, 134224542, 0, &Version_t1875_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::.ctor(System.PlatformID,System.Version)
extern const MethodInfo OperatingSystem__ctor_m13830_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OperatingSystem__ctor_m13830/* method */
	, &OperatingSystem_t2518_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, OperatingSystem_t2518_OperatingSystem__ctor_m13830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PlatformID_t2542 (const MethodInfo* method, void* obj, void** args);
// System.PlatformID System.OperatingSystem::get_Platform()
extern const MethodInfo OperatingSystem_get_Platform_m13831_MethodInfo = 
{
	"get_Platform"/* name */
	, (methodPointerType)&OperatingSystem_get_Platform_m13831/* method */
	, &OperatingSystem_t2518_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2542_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2542/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo OperatingSystem_t2518_OperatingSystem_GetObjectData_m13832_ParameterInfos[] = 
{
	{"info", 0, 134224543, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224544, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OperatingSystem_GetObjectData_m13832_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&OperatingSystem_GetObjectData_m13832/* method */
	, &OperatingSystem_t2518_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, OperatingSystem_t2518_OperatingSystem_GetObjectData_m13832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.OperatingSystem::ToString()
extern const MethodInfo OperatingSystem_ToString_m13833_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&OperatingSystem_ToString_m13833/* method */
	, &OperatingSystem_t2518_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OperatingSystem_t2518_MethodInfos[] =
{
	&OperatingSystem__ctor_m13830_MethodInfo,
	&OperatingSystem_get_Platform_m13831_MethodInfo,
	&OperatingSystem_GetObjectData_m13832_MethodInfo,
	&OperatingSystem_ToString_m13833_MethodInfo,
	NULL
};
extern const MethodInfo OperatingSystem_get_Platform_m13831_MethodInfo;
static const PropertyInfo OperatingSystem_t2518____Platform_PropertyInfo = 
{
	&OperatingSystem_t2518_il2cpp_TypeInfo/* parent */
	, "Platform"/* name */
	, &OperatingSystem_get_Platform_m13831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* OperatingSystem_t2518_PropertyInfos[] =
{
	&OperatingSystem_t2518____Platform_PropertyInfo,
	NULL
};
extern const MethodInfo OperatingSystem_ToString_m13833_MethodInfo;
extern const MethodInfo OperatingSystem_GetObjectData_m13832_MethodInfo;
static const Il2CppMethodReference OperatingSystem_t2518_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&OperatingSystem_ToString_m13833_MethodInfo,
	&OperatingSystem_GetObjectData_m13832_MethodInfo,
};
static bool OperatingSystem_t2518_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
static const Il2CppType* OperatingSystem_t2518_InterfacesTypeInfos[] = 
{
	&ICloneable_t512_0_0_0,
	&ISerializable_t513_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t2518_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t2518_0_0_0;
extern const Il2CppType OperatingSystem_t2518_1_0_0;
struct OperatingSystem_t2518;
const Il2CppTypeDefinitionMetadata OperatingSystem_t2518_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t2518_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t2518_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t2518_VTable/* vtableMethods */
	, OperatingSystem_t2518_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2462/* fieldStart */

};
TypeInfo OperatingSystem_t2518_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, OperatingSystem_t2518_MethodInfos/* methods */
	, OperatingSystem_t2518_PropertyInfos/* properties */
	, NULL/* events */
	, &OperatingSystem_t2518_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 917/* custom_attributes_cache */
	, &OperatingSystem_t2518_0_0_0/* byval_arg */
	, &OperatingSystem_t2518_1_0_0/* this_arg */
	, &OperatingSystem_t2518_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t2518)/* instance_size */
	, sizeof (OperatingSystem_t2518)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t2540_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor()
extern const MethodInfo OutOfMemoryException__ctor_m13834_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m13834/* method */
	, &OutOfMemoryException_t2540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo OutOfMemoryException_t2540_OutOfMemoryException__ctor_m13835_ParameterInfos[] = 
{
	{"info", 0, 134224545, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224546, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OutOfMemoryException__ctor_m13835_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m13835/* method */
	, &OutOfMemoryException_t2540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, OutOfMemoryException_t2540_OutOfMemoryException__ctor_m13835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OutOfMemoryException_t2540_MethodInfos[] =
{
	&OutOfMemoryException__ctor_m13834_MethodInfo,
	&OutOfMemoryException__ctor_m13835_MethodInfo,
	NULL
};
static const Il2CppMethodReference OutOfMemoryException_t2540_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool OutOfMemoryException_t2540_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t2540_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t2540_0_0_0;
extern const Il2CppType OutOfMemoryException_t2540_1_0_0;
struct OutOfMemoryException_t2540;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t2540_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t2540_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, OutOfMemoryException_t2540_VTable/* vtableMethods */
	, OutOfMemoryException_t2540_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2465/* fieldStart */

};
TypeInfo OutOfMemoryException_t2540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, OutOfMemoryException_t2540_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OutOfMemoryException_t2540_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 918/* custom_attributes_cache */
	, &OutOfMemoryException_t2540_0_0_0/* byval_arg */
	, &OutOfMemoryException_t2540_1_0_0/* this_arg */
	, &OutOfMemoryException_t2540_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t2540)/* instance_size */
	, sizeof (OutOfMemoryException_t2540)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t2541_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor()
extern const MethodInfo OverflowException__ctor_m13836_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13836/* method */
	, &OverflowException_t2541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OverflowException_t2541_OverflowException__ctor_m13837_ParameterInfos[] = 
{
	{"message", 0, 134224547, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.String)
extern const MethodInfo OverflowException__ctor_m13837_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13837/* method */
	, &OverflowException_t2541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, OverflowException_t2541_OverflowException__ctor_m13837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo OverflowException_t2541_OverflowException__ctor_m13838_ParameterInfos[] = 
{
	{"info", 0, 134224548, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224549, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OverflowException__ctor_m13838_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13838/* method */
	, &OverflowException_t2541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, OverflowException_t2541_OverflowException__ctor_m13838_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OverflowException_t2541_MethodInfos[] =
{
	&OverflowException__ctor_m13836_MethodInfo,
	&OverflowException__ctor_m13837_MethodInfo,
	&OverflowException__ctor_m13838_MethodInfo,
	NULL
};
static const Il2CppMethodReference OverflowException_t2541_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool OverflowException_t2541_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OverflowException_t2541_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t2541_0_0_0;
extern const Il2CppType OverflowException_t2541_1_0_0;
extern const Il2CppType ArithmeticException_t1803_0_0_0;
struct OverflowException_t2541;
const Il2CppTypeDefinitionMetadata OverflowException_t2541_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t2541_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1803_0_0_0/* parent */
	, OverflowException_t2541_VTable/* vtableMethods */
	, OverflowException_t2541_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2466/* fieldStart */

};
TypeInfo OverflowException_t2541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, OverflowException_t2541_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OverflowException_t2541_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 919/* custom_attributes_cache */
	, &OverflowException_t2541_0_0_0/* byval_arg */
	, &OverflowException_t2541_1_0_0/* this_arg */
	, &OverflowException_t2541_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t2541)/* instance_size */
	, sizeof (OverflowException_t2541)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t2542_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const MethodInfo* PlatformID_t2542_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PlatformID_t2542_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool PlatformID_t2542_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PlatformID_t2542_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t2542_1_0_0;
const Il2CppTypeDefinitionMetadata PlatformID_t2542_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t2542_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, PlatformID_t2542_VTable/* vtableMethods */
	, PlatformID_t2542_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2467/* fieldStart */

};
TypeInfo PlatformID_t2542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, PlatformID_t2542_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 920/* custom_attributes_cache */
	, &PlatformID_t2542_0_0_0/* byval_arg */
	, &PlatformID_t2542_1_0_0/* this_arg */
	, &PlatformID_t2542_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t2542)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t2542)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t1270_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor()
extern const MethodInfo Random__ctor_m13839_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m13839/* method */
	, &Random_t1270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Random_t1270_Random__ctor_m6962_ParameterInfos[] = 
{
	{"Seed", 0, 134224550, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor(System.Int32)
extern const MethodInfo Random__ctor_m6962_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m6962/* method */
	, &Random_t1270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Random_t1270_Random__ctor_m6962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Random_t1270_MethodInfos[] =
{
	&Random__ctor_m13839_MethodInfo,
	&Random__ctor_m6962_MethodInfo,
	NULL
};
static const Il2CppMethodReference Random_t1270_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Random_t1270_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t1270_0_0_0;
extern const Il2CppType Random_t1270_1_0_0;
struct Random_t1270;
const Il2CppTypeDefinitionMetadata Random_t1270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t1270_VTable/* vtableMethods */
	, Random_t1270_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2475/* fieldStart */

};
TypeInfo Random_t1270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, Random_t1270_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Random_t1270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 921/* custom_attributes_cache */
	, &Random_t1270_0_0_0/* byval_arg */
	, &Random_t1270_1_0_0/* this_arg */
	, &Random_t1270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t1270)/* instance_size */
	, sizeof (Random_t1270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t2543_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor()
extern const MethodInfo RankException__ctor_m13840_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13840/* method */
	, &RankException_t2543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RankException_t2543_RankException__ctor_m13841_ParameterInfos[] = 
{
	{"message", 0, 134224551, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.String)
extern const MethodInfo RankException__ctor_m13841_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13841/* method */
	, &RankException_t2543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RankException_t2543_RankException__ctor_m13841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo RankException_t2543_RankException__ctor_m13842_ParameterInfos[] = 
{
	{"info", 0, 134224552, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224553, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RankException__ctor_m13842_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13842/* method */
	, &RankException_t2543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, RankException_t2543_RankException__ctor_m13842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RankException_t2543_MethodInfos[] =
{
	&RankException__ctor_m13840_MethodInfo,
	&RankException__ctor_m13841_MethodInfo,
	&RankException__ctor_m13842_MethodInfo,
	NULL
};
static const Il2CppMethodReference RankException_t2543_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool RankException_t2543_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RankException_t2543_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t2543_0_0_0;
extern const Il2CppType RankException_t2543_1_0_0;
struct RankException_t2543;
const Il2CppTypeDefinitionMetadata RankException_t2543_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t2543_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, RankException_t2543_VTable/* vtableMethods */
	, RankException_t2543_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RankException_t2543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, RankException_t2543_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RankException_t2543_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 922/* custom_attributes_cache */
	, &RankException_t2543_0_0_0/* byval_arg */
	, &RankException_t2543_1_0_0/* this_arg */
	, &RankException_t2543_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t2543)/* instance_size */
	, sizeof (RankException_t2543)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t2544_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const MethodInfo* ResolveEventArgs_t2544_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ResolveEventArgs_t2544_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ResolveEventArgs_t2544_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t2544_0_0_0;
extern const Il2CppType ResolveEventArgs_t2544_1_0_0;
extern const Il2CppType EventArgs_t1688_0_0_0;
struct ResolveEventArgs_t2544;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t2544_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1688_0_0_0/* parent */
	, ResolveEventArgs_t2544_VTable/* vtableMethods */
	, ResolveEventArgs_t2544_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventArgs_t2544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, ResolveEventArgs_t2544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventArgs_t2544_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 923/* custom_attributes_cache */
	, &ResolveEventArgs_t2544_0_0_0/* byval_arg */
	, &ResolveEventArgs_t2544_1_0_0/* this_arg */
	, &ResolveEventArgs_t2544_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t2544)/* instance_size */
	, sizeof (ResolveEventArgs_t2544)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t2545_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2545_RuntimeMethodHandle__ctor_m13843_ParameterInfos[] = 
{
	{"v", 0, 134224554, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.IntPtr)
extern const MethodInfo RuntimeMethodHandle__ctor_m13843_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m13843/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_IntPtr_t/* invoker_method */
	, RuntimeMethodHandle_t2545_RuntimeMethodHandle__ctor_m13843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2545_RuntimeMethodHandle__ctor_m13844_ParameterInfos[] = 
{
	{"info", 0, 134224555, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224556, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle__ctor_m13844_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m13844/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, RuntimeMethodHandle_t2545_RuntimeMethodHandle__ctor_m13844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.RuntimeMethodHandle::get_Value()
extern const MethodInfo RuntimeMethodHandle_get_Value_m13845_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&RuntimeMethodHandle_get_Value_m13845/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2545_RuntimeMethodHandle_GetObjectData_m13846_ParameterInfos[] = 
{
	{"info", 0, 134224557, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224558, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m13846_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetObjectData_m13846/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, RuntimeMethodHandle_t2545_RuntimeMethodHandle_GetObjectData_m13846_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2545_RuntimeMethodHandle_Equals_m13847_ParameterInfos[] = 
{
	{"obj", 0, 134224559, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.RuntimeMethodHandle::Equals(System.Object)
extern const MethodInfo RuntimeMethodHandle_Equals_m13847_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&RuntimeMethodHandle_Equals_m13847/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, RuntimeMethodHandle_t2545_RuntimeMethodHandle_Equals_m13847_ParameterInfos/* parameters */
	, 925/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.RuntimeMethodHandle::GetHashCode()
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m13848_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetHashCode_m13848/* method */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RuntimeMethodHandle_t2545_MethodInfos[] =
{
	&RuntimeMethodHandle__ctor_m13843_MethodInfo,
	&RuntimeMethodHandle__ctor_m13844_MethodInfo,
	&RuntimeMethodHandle_get_Value_m13845_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m13846_MethodInfo,
	&RuntimeMethodHandle_Equals_m13847_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m13848_MethodInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_get_Value_m13845_MethodInfo;
static const PropertyInfo RuntimeMethodHandle_t2545____Value_PropertyInfo = 
{
	&RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &RuntimeMethodHandle_get_Value_m13845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RuntimeMethodHandle_t2545_PropertyInfos[] =
{
	&RuntimeMethodHandle_t2545____Value_PropertyInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_Equals_m13847_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m13848_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m13846_MethodInfo;
static const Il2CppMethodReference RuntimeMethodHandle_t2545_VTable[] =
{
	&RuntimeMethodHandle_Equals_m13847_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m13848_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m13846_MethodInfo,
};
static bool RuntimeMethodHandle_t2545_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RuntimeMethodHandle_t2545_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t2545_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t2545_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t2545_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t2545_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t2545_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t2545_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, RuntimeMethodHandle_t2545_VTable/* vtableMethods */
	, RuntimeMethodHandle_t2545_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2478/* fieldStart */

};
TypeInfo RuntimeMethodHandle_t2545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, RuntimeMethodHandle_t2545_MethodInfos/* methods */
	, RuntimeMethodHandle_t2545_PropertyInfos/* properties */
	, NULL/* events */
	, &RuntimeMethodHandle_t2545_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 924/* custom_attributes_cache */
	, &RuntimeMethodHandle_t2545_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t2545_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t2545_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t2545)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t2545)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t2545 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t1390_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.ctor()
extern const MethodInfo StringComparer__ctor_m13849_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringComparer__ctor_m13849/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.cctor()
extern const MethodInfo StringComparer__cctor_m13850_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringComparer__cctor_m13850/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringComparer_t1390_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_InvariantCultureIgnoreCase()
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m9342_MethodInfo = 
{
	"get_InvariantCultureIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_InvariantCultureIgnoreCase_m9342/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m6918_MethodInfo = 
{
	"get_OrdinalIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_OrdinalIgnoreCase_m6918/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_Compare_m13851_ParameterInfos[] = 
{
	{"x", 0, 134224560, 0, &Object_t_0_0_0},
	{"y", 1, 134224561, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.Object,System.Object)
extern const MethodInfo StringComparer_Compare_m13851_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&StringComparer_Compare_m13851/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_Compare_m13851_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_Equals_m13852_ParameterInfos[] = 
{
	{"x", 0, 134224562, 0, &Object_t_0_0_0},
	{"y", 1, 134224563, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.Object,System.Object)
extern const MethodInfo StringComparer_Equals_m13852_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&StringComparer_Equals_m13852/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_Equals_m13852_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_GetHashCode_m13853_ParameterInfos[] = 
{
	{"obj", 0, 134224564, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.Object)
extern const MethodInfo StringComparer_GetHashCode_m13853_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&StringComparer_GetHashCode_m13853/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_GetHashCode_m13853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_Compare_m14640_ParameterInfos[] = 
{
	{"x", 0, 134224565, 0, &String_t_0_0_0},
	{"y", 1, 134224566, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.String,System.String)
extern const MethodInfo StringComparer_Compare_m14640_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_Compare_m14640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_Equals_m14641_ParameterInfos[] = 
{
	{"x", 0, 134224567, 0, &String_t_0_0_0},
	{"y", 1, 134224568, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.String,System.String)
extern const MethodInfo StringComparer_Equals_m14641_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_Equals_m14641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1390_StringComparer_GetHashCode_m14642_ParameterInfos[] = 
{
	{"obj", 0, 134224569, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.String)
extern const MethodInfo StringComparer_GetHashCode_m14642_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &StringComparer_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, StringComparer_t1390_StringComparer_GetHashCode_m14642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringComparer_t1390_MethodInfos[] =
{
	&StringComparer__ctor_m13849_MethodInfo,
	&StringComparer__cctor_m13850_MethodInfo,
	&StringComparer_get_InvariantCultureIgnoreCase_m9342_MethodInfo,
	&StringComparer_get_OrdinalIgnoreCase_m6918_MethodInfo,
	&StringComparer_Compare_m13851_MethodInfo,
	&StringComparer_Equals_m13852_MethodInfo,
	&StringComparer_GetHashCode_m13853_MethodInfo,
	&StringComparer_Compare_m14640_MethodInfo,
	&StringComparer_Equals_m14641_MethodInfo,
	&StringComparer_GetHashCode_m14642_MethodInfo,
	NULL
};
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m9342_MethodInfo;
static const PropertyInfo StringComparer_t1390____InvariantCultureIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1390_il2cpp_TypeInfo/* parent */
	, "InvariantCultureIgnoreCase"/* name */
	, &StringComparer_get_InvariantCultureIgnoreCase_m9342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m6918_MethodInfo;
static const PropertyInfo StringComparer_t1390____OrdinalIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1390_il2cpp_TypeInfo/* parent */
	, "OrdinalIgnoreCase"/* name */
	, &StringComparer_get_OrdinalIgnoreCase_m6918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* StringComparer_t1390_PropertyInfos[] =
{
	&StringComparer_t1390____InvariantCultureIgnoreCase_PropertyInfo,
	&StringComparer_t1390____OrdinalIgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo StringComparer_Compare_m14640_MethodInfo;
extern const MethodInfo StringComparer_Equals_m14641_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m14642_MethodInfo;
extern const MethodInfo StringComparer_Compare_m13851_MethodInfo;
extern const MethodInfo StringComparer_Equals_m13852_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m13853_MethodInfo;
static const Il2CppMethodReference StringComparer_t1390_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&StringComparer_Compare_m14640_MethodInfo,
	&StringComparer_Equals_m14641_MethodInfo,
	&StringComparer_GetHashCode_m14642_MethodInfo,
	&StringComparer_Compare_m13851_MethodInfo,
	&StringComparer_Equals_m13852_MethodInfo,
	&StringComparer_GetHashCode_m13853_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool StringComparer_t1390_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_1_t3086_0_0_0;
extern const Il2CppType IEqualityComparer_1_t3087_0_0_0;
extern const Il2CppType IComparer_t1852_0_0_0;
extern const Il2CppType IEqualityComparer_t1858_0_0_0;
static const Il2CppType* StringComparer_t1390_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3086_0_0_0,
	&IEqualityComparer_1_t3087_0_0_0,
	&IComparer_t1852_0_0_0,
	&IEqualityComparer_t1858_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t1390_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3086_0_0_0, 4},
	{ &IEqualityComparer_1_t3087_0_0_0, 5},
	{ &IComparer_t1852_0_0_0, 7},
	{ &IEqualityComparer_t1858_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t1390_1_0_0;
struct StringComparer_t1390;
const Il2CppTypeDefinitionMetadata StringComparer_t1390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t1390_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t1390_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t1390_VTable/* vtableMethods */
	, StringComparer_t1390_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2479/* fieldStart */

};
TypeInfo StringComparer_t1390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, StringComparer_t1390_MethodInfos/* methods */
	, StringComparer_t1390_PropertyInfos/* properties */
	, NULL/* events */
	, &StringComparer_t1390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 926/* custom_attributes_cache */
	, &StringComparer_t1390_0_0_0/* byval_arg */
	, &StringComparer_t1390_1_0_0/* this_arg */
	, &StringComparer_t1390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t1390)/* instance_size */
	, sizeof (StringComparer_t1390)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t1390_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t2546_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
extern const Il2CppType CultureInfo_t1405_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CultureAwareComparer_t2546_CultureAwareComparer__ctor_m13854_ParameterInfos[] = 
{
	{"ci", 0, 134224570, 0, &CultureInfo_t1405_0_0_0},
	{"ignore_case", 1, 134224571, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CultureAwareComparer::.ctor(System.Globalization.CultureInfo,System.Boolean)
extern const MethodInfo CultureAwareComparer__ctor_m13854_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CultureAwareComparer__ctor_m13854/* method */
	, &CultureAwareComparer_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, CultureAwareComparer_t2546_CultureAwareComparer__ctor_m13854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2546_CultureAwareComparer_Compare_m13855_ParameterInfos[] = 
{
	{"x", 0, 134224572, 0, &String_t_0_0_0},
	{"y", 1, 134224573, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::Compare(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Compare_m13855_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&CultureAwareComparer_Compare_m13855/* method */
	, &CultureAwareComparer_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2546_CultureAwareComparer_Compare_m13855_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2546_CultureAwareComparer_Equals_m13856_ParameterInfos[] = 
{
	{"x", 0, 134224574, 0, &String_t_0_0_0},
	{"y", 1, 134224575, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CultureAwareComparer::Equals(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Equals_m13856_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&CultureAwareComparer_Equals_m13856/* method */
	, &CultureAwareComparer_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2546_CultureAwareComparer_Equals_m13856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2546_CultureAwareComparer_GetHashCode_m13857_ParameterInfos[] = 
{
	{"s", 0, 134224576, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::GetHashCode(System.String)
extern const MethodInfo CultureAwareComparer_GetHashCode_m13857_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&CultureAwareComparer_GetHashCode_m13857/* method */
	, &CultureAwareComparer_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, CultureAwareComparer_t2546_CultureAwareComparer_GetHashCode_m13857_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CultureAwareComparer_t2546_MethodInfos[] =
{
	&CultureAwareComparer__ctor_m13854_MethodInfo,
	&CultureAwareComparer_Compare_m13855_MethodInfo,
	&CultureAwareComparer_Equals_m13856_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13857_MethodInfo,
	NULL
};
extern const MethodInfo CultureAwareComparer_Compare_m13855_MethodInfo;
extern const MethodInfo CultureAwareComparer_Equals_m13856_MethodInfo;
extern const MethodInfo CultureAwareComparer_GetHashCode_m13857_MethodInfo;
static const Il2CppMethodReference CultureAwareComparer_t2546_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CultureAwareComparer_Compare_m13855_MethodInfo,
	&CultureAwareComparer_Equals_m13856_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13857_MethodInfo,
	&StringComparer_Compare_m13851_MethodInfo,
	&StringComparer_Equals_m13852_MethodInfo,
	&StringComparer_GetHashCode_m13853_MethodInfo,
	&CultureAwareComparer_Compare_m13855_MethodInfo,
	&CultureAwareComparer_Equals_m13856_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13857_MethodInfo,
};
static bool CultureAwareComparer_t2546_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t2546_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3086_0_0_0, 4},
	{ &IEqualityComparer_1_t3087_0_0_0, 5},
	{ &IComparer_t1852_0_0_0, 7},
	{ &IEqualityComparer_t1858_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t2546_0_0_0;
extern const Il2CppType CultureAwareComparer_t2546_1_0_0;
struct CultureAwareComparer_t2546;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t2546_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t2546_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1390_0_0_0/* parent */
	, CultureAwareComparer_t2546_VTable/* vtableMethods */
	, CultureAwareComparer_t2546_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2483/* fieldStart */

};
TypeInfo CultureAwareComparer_t2546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, CultureAwareComparer_t2546_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CultureAwareComparer_t2546_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t2546_0_0_0/* byval_arg */
	, &CultureAwareComparer_t2546_1_0_0/* this_arg */
	, &CultureAwareComparer_t2546_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t2546)/* instance_size */
	, sizeof (CultureAwareComparer_t2546)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t2547_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo OrdinalComparer_t2547_OrdinalComparer__ctor_m13858_ParameterInfos[] = 
{
	{"ignoreCase", 0, 134224577, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OrdinalComparer::.ctor(System.Boolean)
extern const MethodInfo OrdinalComparer__ctor_m13858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrdinalComparer__ctor_m13858/* method */
	, &OrdinalComparer_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, OrdinalComparer_t2547_OrdinalComparer__ctor_m13858_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2547_OrdinalComparer_Compare_m13859_ParameterInfos[] = 
{
	{"x", 0, 134224578, 0, &String_t_0_0_0},
	{"y", 1, 134224579, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::Compare(System.String,System.String)
extern const MethodInfo OrdinalComparer_Compare_m13859_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&OrdinalComparer_Compare_m13859/* method */
	, &OrdinalComparer_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2547_OrdinalComparer_Compare_m13859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2547_OrdinalComparer_Equals_m13860_ParameterInfos[] = 
{
	{"x", 0, 134224580, 0, &String_t_0_0_0},
	{"y", 1, 134224581, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.OrdinalComparer::Equals(System.String,System.String)
extern const MethodInfo OrdinalComparer_Equals_m13860_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&OrdinalComparer_Equals_m13860/* method */
	, &OrdinalComparer_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2547_OrdinalComparer_Equals_m13860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2547_OrdinalComparer_GetHashCode_m13861_ParameterInfos[] = 
{
	{"s", 0, 134224582, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::GetHashCode(System.String)
extern const MethodInfo OrdinalComparer_GetHashCode_m13861_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&OrdinalComparer_GetHashCode_m13861/* method */
	, &OrdinalComparer_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, OrdinalComparer_t2547_OrdinalComparer_GetHashCode_m13861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrdinalComparer_t2547_MethodInfos[] =
{
	&OrdinalComparer__ctor_m13858_MethodInfo,
	&OrdinalComparer_Compare_m13859_MethodInfo,
	&OrdinalComparer_Equals_m13860_MethodInfo,
	&OrdinalComparer_GetHashCode_m13861_MethodInfo,
	NULL
};
extern const MethodInfo OrdinalComparer_Compare_m13859_MethodInfo;
extern const MethodInfo OrdinalComparer_Equals_m13860_MethodInfo;
extern const MethodInfo OrdinalComparer_GetHashCode_m13861_MethodInfo;
static const Il2CppMethodReference OrdinalComparer_t2547_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&OrdinalComparer_Compare_m13859_MethodInfo,
	&OrdinalComparer_Equals_m13860_MethodInfo,
	&OrdinalComparer_GetHashCode_m13861_MethodInfo,
	&StringComparer_Compare_m13851_MethodInfo,
	&StringComparer_Equals_m13852_MethodInfo,
	&StringComparer_GetHashCode_m13853_MethodInfo,
	&OrdinalComparer_Compare_m13859_MethodInfo,
	&OrdinalComparer_Equals_m13860_MethodInfo,
	&OrdinalComparer_GetHashCode_m13861_MethodInfo,
};
static bool OrdinalComparer_t2547_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t2547_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3086_0_0_0, 4},
	{ &IEqualityComparer_1_t3087_0_0_0, 5},
	{ &IComparer_t1852_0_0_0, 7},
	{ &IEqualityComparer_t1858_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t2547_0_0_0;
extern const Il2CppType OrdinalComparer_t2547_1_0_0;
struct OrdinalComparer_t2547;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t2547_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t2547_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1390_0_0_0/* parent */
	, OrdinalComparer_t2547_VTable/* vtableMethods */
	, OrdinalComparer_t2547_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2485/* fieldStart */

};
TypeInfo OrdinalComparer_t2547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, OrdinalComparer_t2547_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrdinalComparer_t2547_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t2547_0_0_0/* byval_arg */
	, &OrdinalComparer_t2547_1_0_0/* this_arg */
	, &OrdinalComparer_t2547_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t2547)/* instance_size */
	, sizeof (OrdinalComparer_t2547)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t2548_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const MethodInfo* StringComparison_t2548_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringComparison_t2548_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool StringComparison_t2548_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringComparison_t2548_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t2548_0_0_0;
extern const Il2CppType StringComparison_t2548_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t2548_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t2548_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, StringComparison_t2548_VTable/* vtableMethods */
	, StringComparison_t2548_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2486/* fieldStart */

};
TypeInfo StringComparison_t2548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, StringComparison_t2548_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 927/* custom_attributes_cache */
	, &StringComparison_t2548_0_0_0/* byval_arg */
	, &StringComparison_t2548_1_0_0/* this_arg */
	, &StringComparison_t2548_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t2548)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t2548)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t2549_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const MethodInfo* StringSplitOptions_t2549_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringSplitOptions_t2549_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool StringSplitOptions_t2549_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t2549_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t2549_0_0_0;
extern const Il2CppType StringSplitOptions_t2549_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t2549_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t2549_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, StringSplitOptions_t2549_VTable/* vtableMethods */
	, StringSplitOptions_t2549_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2493/* fieldStart */

};
TypeInfo StringSplitOptions_t2549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, StringSplitOptions_t2549_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 928/* custom_attributes_cache */
	, &StringSplitOptions_t2549_0_0_0/* byval_arg */
	, &StringSplitOptions_t2549_1_0_0/* this_arg */
	, &StringSplitOptions_t2549_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t2549)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t2549)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t2004_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor()
extern const MethodInfo SystemException__ctor_m13862_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13862/* method */
	, &SystemException_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SystemException_t2004_SystemException__ctor_m9369_ParameterInfos[] = 
{
	{"message", 0, 134224583, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String)
extern const MethodInfo SystemException__ctor_m9369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m9369/* method */
	, &SystemException_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SystemException_t2004_SystemException__ctor_m9369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo SystemException_t2004_SystemException__ctor_m13863_ParameterInfos[] = 
{
	{"info", 0, 134224584, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224585, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SystemException__ctor_m13863_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13863/* method */
	, &SystemException_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, SystemException_t2004_SystemException__ctor_m13863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t140_0_0_0;
static const ParameterInfo SystemException_t2004_SystemException__ctor_m13864_ParameterInfos[] = 
{
	{"message", 0, 134224586, 0, &String_t_0_0_0},
	{"innerException", 1, 134224587, 0, &Exception_t140_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern const MethodInfo SystemException__ctor_m13864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13864/* method */
	, &SystemException_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, SystemException_t2004_SystemException__ctor_m13864_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SystemException_t2004_MethodInfos[] =
{
	&SystemException__ctor_m13862_MethodInfo,
	&SystemException__ctor_m9369_MethodInfo,
	&SystemException__ctor_m13863_MethodInfo,
	&SystemException__ctor_m13864_MethodInfo,
	NULL
};
static const Il2CppMethodReference SystemException_t2004_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool SystemException_t2004_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SystemException_t2004_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t2004_1_0_0;
struct SystemException_t2004;
const Il2CppTypeDefinitionMetadata SystemException_t2004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t2004_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t140_0_0_0/* parent */
	, SystemException_t2004_VTable/* vtableMethods */
	, SystemException_t2004_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SystemException_t2004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, SystemException_t2004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SystemException_t2004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 929/* custom_attributes_cache */
	, &SystemException_t2004_0_0_0/* byval_arg */
	, &SystemException_t2004_1_0_0/* this_arg */
	, &SystemException_t2004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t2004)/* instance_size */
	, sizeof (SystemException_t2004)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t2550_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ThreadStaticAttribute::.ctor()
extern const MethodInfo ThreadStaticAttribute__ctor_m13865_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ThreadStaticAttribute__ctor_m13865/* method */
	, &ThreadStaticAttribute_t2550_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadStaticAttribute_t2550_MethodInfos[] =
{
	&ThreadStaticAttribute__ctor_m13865_MethodInfo,
	NULL
};
static const Il2CppMethodReference ThreadStaticAttribute_t2550_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ThreadStaticAttribute_t2550_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t2550_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t2550_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t2550_1_0_0;
struct ThreadStaticAttribute_t2550;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t2550_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t2550_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ThreadStaticAttribute_t2550_VTable/* vtableMethods */
	, ThreadStaticAttribute_t2550_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadStaticAttribute_t2550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, ThreadStaticAttribute_t2550_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadStaticAttribute_t2550_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 930/* custom_attributes_cache */
	, &ThreadStaticAttribute_t2550_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t2550_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t2550_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t2550)/* instance_size */
	, sizeof (ThreadStaticAttribute_t2550)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t112_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan__ctor_m13866_ParameterInfos[] = 
{
	{"ticks", 0, 134224588, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int64)
extern const MethodInfo TimeSpan__ctor_m13866_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13866/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092/* invoker_method */
	, TimeSpan_t112_TimeSpan__ctor_m13866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan__ctor_m13867_ParameterInfos[] = 
{
	{"hours", 0, 134224589, 0, &Int32_t127_0_0_0},
	{"minutes", 1, 134224590, 0, &Int32_t127_0_0_0},
	{"seconds", 2, 134224591, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m13867_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13867/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, TimeSpan_t112_TimeSpan__ctor_m13867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan__ctor_m13868_ParameterInfos[] = 
{
	{"days", 0, 134224592, 0, &Int32_t127_0_0_0},
	{"hours", 1, 134224593, 0, &Int32_t127_0_0_0},
	{"minutes", 2, 134224594, 0, &Int32_t127_0_0_0},
	{"seconds", 3, 134224595, 0, &Int32_t127_0_0_0},
	{"milliseconds", 4, 134224596, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m13868_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13868/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, TimeSpan_t112_TimeSpan__ctor_m13868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.cctor()
extern const MethodInfo TimeSpan__cctor_m13869_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeSpan__cctor_m13869/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_CalculateTicks_m13870_ParameterInfos[] = 
{
	{"days", 0, 134224597, 0, &Int32_t127_0_0_0},
	{"hours", 1, 134224598, 0, &Int32_t127_0_0_0},
	{"minutes", 2, 134224599, 0, &Int32_t127_0_0_0},
	{"seconds", 3, 134224600, 0, &Int32_t127_0_0_0},
	{"milliseconds", 4, 134224601, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::CalculateTicks(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan_CalculateTicks_m13870_MethodInfo = 
{
	"CalculateTicks"/* name */
	, (methodPointerType)&TimeSpan_CalculateTicks_m13870/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, TimeSpan_t112_TimeSpan_CalculateTicks_m13870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Days()
extern const MethodInfo TimeSpan_get_Days_m13871_MethodInfo = 
{
	"get_Days"/* name */
	, (methodPointerType)&TimeSpan_get_Days_m13871/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Hours()
extern const MethodInfo TimeSpan_get_Hours_m13872_MethodInfo = 
{
	"get_Hours"/* name */
	, (methodPointerType)&TimeSpan_get_Hours_m13872/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Milliseconds()
extern const MethodInfo TimeSpan_get_Milliseconds_m13873_MethodInfo = 
{
	"get_Milliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_Milliseconds_m13873/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Minutes()
extern const MethodInfo TimeSpan_get_Minutes_m13874_MethodInfo = 
{
	"get_Minutes"/* name */
	, (methodPointerType)&TimeSpan_get_Minutes_m13874/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Seconds()
extern const MethodInfo TimeSpan_get_Seconds_m13875_MethodInfo = 
{
	"get_Seconds"/* name */
	, (methodPointerType)&TimeSpan_get_Seconds_m13875/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::get_Ticks()
extern const MethodInfo TimeSpan_get_Ticks_m13876_MethodInfo = 
{
	"get_Ticks"/* name */
	, (methodPointerType)&TimeSpan_get_Ticks_m13876/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalDays()
extern const MethodInfo TimeSpan_get_TotalDays_m13877_MethodInfo = 
{
	"get_TotalDays"/* name */
	, (methodPointerType)&TimeSpan_get_TotalDays_m13877/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalHours()
extern const MethodInfo TimeSpan_get_TotalHours_m13878_MethodInfo = 
{
	"get_TotalHours"/* name */
	, (methodPointerType)&TimeSpan_get_TotalHours_m13878/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m4401_MethodInfo = 
{
	"get_TotalMilliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMilliseconds_m4401/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMinutes()
extern const MethodInfo TimeSpan_get_TotalMinutes_m13879_MethodInfo = 
{
	"get_TotalMinutes"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMinutes_m13879/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalSeconds()
extern const MethodInfo TimeSpan_get_TotalSeconds_m335_MethodInfo = 
{
	"get_TotalSeconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalSeconds_m335/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_Add_m13880_ParameterInfos[] = 
{
	{"ts", 0, 134224602, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Add(System.TimeSpan)
extern const MethodInfo TimeSpan_Add_m13880_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&TimeSpan_Add_m13880/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_Add_m13880_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_Compare_m13881_ParameterInfos[] = 
{
	{"t1", 0, 134224603, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224604, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::Compare(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_Compare_m13881_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&TimeSpan_Compare_m13881/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_Compare_m13881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_CompareTo_m13882_ParameterInfos[] = 
{
	{"value", 0, 134224605, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.Object)
extern const MethodInfo TimeSpan_CompareTo_m13882_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m13882/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, TimeSpan_t112_TimeSpan_CompareTo_m13882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_CompareTo_m13883_ParameterInfos[] = 
{
	{"value", 0, 134224606, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern const MethodInfo TimeSpan_CompareTo_m13883_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m13883/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_CompareTo_m13883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_Equals_m13884_ParameterInfos[] = 
{
	{"obj", 0, 134224607, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern const MethodInfo TimeSpan_Equals_m13884_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m13884/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_Equals_m13884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Duration()
extern const MethodInfo TimeSpan_Duration_m13885_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TimeSpan_Duration_m13885/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_Equals_m13886_ParameterInfos[] = 
{
	{"value", 0, 134224608, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.Object)
extern const MethodInfo TimeSpan_Equals_m13886_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m13886/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TimeSpan_t112_TimeSpan_Equals_m13886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_FromDays_m4403_ParameterInfos[] = 
{
	{"value", 0, 134224609, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromDays(System.Double)
extern const MethodInfo TimeSpan_FromDays_m4403_MethodInfo = 
{
	"FromDays"/* name */
	, (methodPointerType)&TimeSpan_FromDays_m4403/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_Double_t1407/* invoker_method */
	, TimeSpan_t112_TimeSpan_FromDays_m4403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_FromMinutes_m13887_ParameterInfos[] = 
{
	{"value", 0, 134224610, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromMinutes(System.Double)
extern const MethodInfo TimeSpan_FromMinutes_m13887_MethodInfo = 
{
	"FromMinutes"/* name */
	, (methodPointerType)&TimeSpan_FromMinutes_m13887/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_Double_t1407/* invoker_method */
	, TimeSpan_t112_TimeSpan_FromMinutes_m13887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_From_m13888_ParameterInfos[] = 
{
	{"value", 0, 134224611, 0, &Double_t1407_0_0_0},
	{"tickMultiplicator", 1, 134224612, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_Double_t1407_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::From(System.Double,System.Int64)
extern const MethodInfo TimeSpan_From_m13888_MethodInfo = 
{
	"From"/* name */
	, (methodPointerType)&TimeSpan_From_m13888/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_Double_t1407_Int64_t1092/* invoker_method */
	, TimeSpan_t112_TimeSpan_From_m13888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::GetHashCode()
extern const MethodInfo TimeSpan_GetHashCode_m13889_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TimeSpan_GetHashCode_m13889/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Negate()
extern const MethodInfo TimeSpan_Negate_m13890_MethodInfo = 
{
	"Negate"/* name */
	, (methodPointerType)&TimeSpan_Negate_m13890/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_Subtract_m13891_ParameterInfos[] = 
{
	{"ts", 0, 134224613, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Subtract(System.TimeSpan)
extern const MethodInfo TimeSpan_Subtract_m13891_MethodInfo = 
{
	"Subtract"/* name */
	, (methodPointerType)&TimeSpan_Subtract_m13891/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_Subtract_m13891_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TimeSpan::ToString()
extern const MethodInfo TimeSpan_ToString_m13892_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TimeSpan_ToString_m13892/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_Addition_m13893_ParameterInfos[] = 
{
	{"t1", 0, 134224614, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224615, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Addition(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Addition_m13893_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&TimeSpan_op_Addition_m13893/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_Addition_m13893_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_Equality_m13894_ParameterInfos[] = 
{
	{"t1", 0, 134224616, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224617, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Equality_m13894_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TimeSpan_op_Equality_m13894/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_Equality_m13894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_GreaterThan_m13895_ParameterInfos[] = 
{
	{"t1", 0, 134224618, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224619, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThan_m13895_MethodInfo = 
{
	"op_GreaterThan"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThan_m13895/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_GreaterThan_m13895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_GreaterThanOrEqual_m13896_ParameterInfos[] = 
{
	{"t1", 0, 134224620, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224621, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThanOrEqual_m13896_MethodInfo = 
{
	"op_GreaterThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThanOrEqual_m13896/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_GreaterThanOrEqual_m13896_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_Inequality_m13897_ParameterInfos[] = 
{
	{"t1", 0, 134224622, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224623, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Inequality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Inequality_m13897_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&TimeSpan_op_Inequality_m13897/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_Inequality_m13897_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_LessThan_m13898_ParameterInfos[] = 
{
	{"t1", 0, 134224624, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224625, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThan_m13898_MethodInfo = 
{
	"op_LessThan"/* name */
	, (methodPointerType)&TimeSpan_op_LessThan_m13898/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_LessThan_m13898_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_LessThanOrEqual_m13899_ParameterInfos[] = 
{
	{"t1", 0, 134224626, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224627, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThanOrEqual_m13899_MethodInfo = 
{
	"op_LessThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_LessThanOrEqual_m13899/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_LessThanOrEqual_m13899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t112_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeSpan_t112_TimeSpan_op_Subtraction_m13900_ParameterInfos[] = 
{
	{"t1", 0, 134224628, 0, &TimeSpan_t112_0_0_0},
	{"t2", 1, 134224629, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_TimeSpan_t112_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Subtraction(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Subtraction_m13900_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&TimeSpan_op_Subtraction_m13900/* method */
	, &TimeSpan_t112_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_TimeSpan_t112_TimeSpan_t112/* invoker_method */
	, TimeSpan_t112_TimeSpan_op_Subtraction_m13900_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeSpan_t112_MethodInfos[] =
{
	&TimeSpan__ctor_m13866_MethodInfo,
	&TimeSpan__ctor_m13867_MethodInfo,
	&TimeSpan__ctor_m13868_MethodInfo,
	&TimeSpan__cctor_m13869_MethodInfo,
	&TimeSpan_CalculateTicks_m13870_MethodInfo,
	&TimeSpan_get_Days_m13871_MethodInfo,
	&TimeSpan_get_Hours_m13872_MethodInfo,
	&TimeSpan_get_Milliseconds_m13873_MethodInfo,
	&TimeSpan_get_Minutes_m13874_MethodInfo,
	&TimeSpan_get_Seconds_m13875_MethodInfo,
	&TimeSpan_get_Ticks_m13876_MethodInfo,
	&TimeSpan_get_TotalDays_m13877_MethodInfo,
	&TimeSpan_get_TotalHours_m13878_MethodInfo,
	&TimeSpan_get_TotalMilliseconds_m4401_MethodInfo,
	&TimeSpan_get_TotalMinutes_m13879_MethodInfo,
	&TimeSpan_get_TotalSeconds_m335_MethodInfo,
	&TimeSpan_Add_m13880_MethodInfo,
	&TimeSpan_Compare_m13881_MethodInfo,
	&TimeSpan_CompareTo_m13882_MethodInfo,
	&TimeSpan_CompareTo_m13883_MethodInfo,
	&TimeSpan_Equals_m13884_MethodInfo,
	&TimeSpan_Duration_m13885_MethodInfo,
	&TimeSpan_Equals_m13886_MethodInfo,
	&TimeSpan_FromDays_m4403_MethodInfo,
	&TimeSpan_FromMinutes_m13887_MethodInfo,
	&TimeSpan_From_m13888_MethodInfo,
	&TimeSpan_GetHashCode_m13889_MethodInfo,
	&TimeSpan_Negate_m13890_MethodInfo,
	&TimeSpan_Subtract_m13891_MethodInfo,
	&TimeSpan_ToString_m13892_MethodInfo,
	&TimeSpan_op_Addition_m13893_MethodInfo,
	&TimeSpan_op_Equality_m13894_MethodInfo,
	&TimeSpan_op_GreaterThan_m13895_MethodInfo,
	&TimeSpan_op_GreaterThanOrEqual_m13896_MethodInfo,
	&TimeSpan_op_Inequality_m13897_MethodInfo,
	&TimeSpan_op_LessThan_m13898_MethodInfo,
	&TimeSpan_op_LessThanOrEqual_m13899_MethodInfo,
	&TimeSpan_op_Subtraction_m13900_MethodInfo,
	NULL
};
extern const MethodInfo TimeSpan_get_Days_m13871_MethodInfo;
static const PropertyInfo TimeSpan_t112____Days_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Days"/* name */
	, &TimeSpan_get_Days_m13871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Hours_m13872_MethodInfo;
static const PropertyInfo TimeSpan_t112____Hours_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Hours"/* name */
	, &TimeSpan_get_Hours_m13872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Milliseconds_m13873_MethodInfo;
static const PropertyInfo TimeSpan_t112____Milliseconds_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Milliseconds"/* name */
	, &TimeSpan_get_Milliseconds_m13873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Minutes_m13874_MethodInfo;
static const PropertyInfo TimeSpan_t112____Minutes_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Minutes"/* name */
	, &TimeSpan_get_Minutes_m13874_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Seconds_m13875_MethodInfo;
static const PropertyInfo TimeSpan_t112____Seconds_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Seconds"/* name */
	, &TimeSpan_get_Seconds_m13875_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Ticks_m13876_MethodInfo;
static const PropertyInfo TimeSpan_t112____Ticks_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "Ticks"/* name */
	, &TimeSpan_get_Ticks_m13876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalDays_m13877_MethodInfo;
static const PropertyInfo TimeSpan_t112____TotalDays_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "TotalDays"/* name */
	, &TimeSpan_get_TotalDays_m13877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalHours_m13878_MethodInfo;
static const PropertyInfo TimeSpan_t112____TotalHours_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "TotalHours"/* name */
	, &TimeSpan_get_TotalHours_m13878_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m4401_MethodInfo;
static const PropertyInfo TimeSpan_t112____TotalMilliseconds_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "TotalMilliseconds"/* name */
	, &TimeSpan_get_TotalMilliseconds_m4401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMinutes_m13879_MethodInfo;
static const PropertyInfo TimeSpan_t112____TotalMinutes_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "TotalMinutes"/* name */
	, &TimeSpan_get_TotalMinutes_m13879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalSeconds_m335_MethodInfo;
static const PropertyInfo TimeSpan_t112____TotalSeconds_PropertyInfo = 
{
	&TimeSpan_t112_il2cpp_TypeInfo/* parent */
	, "TotalSeconds"/* name */
	, &TimeSpan_get_TotalSeconds_m335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeSpan_t112_PropertyInfos[] =
{
	&TimeSpan_t112____Days_PropertyInfo,
	&TimeSpan_t112____Hours_PropertyInfo,
	&TimeSpan_t112____Milliseconds_PropertyInfo,
	&TimeSpan_t112____Minutes_PropertyInfo,
	&TimeSpan_t112____Seconds_PropertyInfo,
	&TimeSpan_t112____Ticks_PropertyInfo,
	&TimeSpan_t112____TotalDays_PropertyInfo,
	&TimeSpan_t112____TotalHours_PropertyInfo,
	&TimeSpan_t112____TotalMilliseconds_PropertyInfo,
	&TimeSpan_t112____TotalMinutes_PropertyInfo,
	&TimeSpan_t112____TotalSeconds_PropertyInfo,
	NULL
};
extern const MethodInfo TimeSpan_Equals_m13886_MethodInfo;
extern const MethodInfo TimeSpan_GetHashCode_m13889_MethodInfo;
extern const MethodInfo TimeSpan_ToString_m13892_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m13882_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m13883_MethodInfo;
extern const MethodInfo TimeSpan_Equals_m13884_MethodInfo;
static const Il2CppMethodReference TimeSpan_t112_VTable[] =
{
	&TimeSpan_Equals_m13886_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&TimeSpan_GetHashCode_m13889_MethodInfo,
	&TimeSpan_ToString_m13892_MethodInfo,
	&TimeSpan_CompareTo_m13882_MethodInfo,
	&TimeSpan_CompareTo_m13883_MethodInfo,
	&TimeSpan_Equals_m13884_MethodInfo,
};
static bool TimeSpan_t112_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t3088_0_0_0;
extern const Il2CppType IEquatable_1_t3089_0_0_0;
static const Il2CppType* TimeSpan_t112_InterfacesTypeInfos[] = 
{
	&IComparable_t166_0_0_0,
	&IComparable_1_t3088_0_0_0,
	&IEquatable_1_t3089_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t112_InterfacesOffsets[] = 
{
	{ &IComparable_t166_0_0_0, 4},
	{ &IComparable_1_t3088_0_0_0, 5},
	{ &IEquatable_1_t3089_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t112_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t112_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t112_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, TimeSpan_t112_VTable/* vtableMethods */
	, TimeSpan_t112_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2496/* fieldStart */

};
TypeInfo TimeSpan_t112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, TimeSpan_t112_MethodInfos/* methods */
	, TimeSpan_t112_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeSpan_t112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 931/* custom_attributes_cache */
	, &TimeSpan_t112_0_0_0/* byval_arg */
	, &TimeSpan_t112_1_0_0/* this_arg */
	, &TimeSpan_t112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t112)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t112)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t112 )/* native_size */
	, sizeof(TimeSpan_t112_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 38/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t2551_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.ctor()
extern const MethodInfo TimeZone__ctor_m13901_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeZone__ctor_m13901/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.cctor()
extern const MethodInfo TimeZone__cctor_m13902_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeZone__cctor_m13902/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeZone_t2551_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeZone System.TimeZone::get_CurrentTimeZone()
extern const MethodInfo TimeZone_get_CurrentTimeZone_m13903_MethodInfo = 
{
	"get_CurrentTimeZone"/* name */
	, (methodPointerType)&TimeZone_get_CurrentTimeZone_m13903/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &TimeZone_t2551_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_GetDaylightChanges_m14643_ParameterInfos[] = 
{
	{"year", 0, 134224630, 0, &Int32_t127_0_0_0},
};
extern const Il2CppType DaylightTime_t2175_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo TimeZone_GetDaylightChanges_m14643_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, NULL/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2175_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, TimeZone_t2551_TimeZone_GetDaylightChanges_m14643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_GetUtcOffset_m14644_ParameterInfos[] = 
{
	{"time", 0, 134224631, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo TimeZone_GetUtcOffset_m14644_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, NULL/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_DateTime_t111/* invoker_method */
	, TimeZone_t2551_TimeZone_GetUtcOffset_m14644_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_IsDaylightSavingTime_m13904_ParameterInfos[] = 
{
	{"time", 0, 134224632, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13904_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m13904/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_DateTime_t111/* invoker_method */
	, TimeZone_t2551_TimeZone_IsDaylightSavingTime_m13904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
extern const Il2CppType DaylightTime_t2175_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_IsDaylightSavingTime_m13905_ParameterInfos[] = 
{
	{"time", 0, 134224633, 0, &DateTime_t111_0_0_0},
	{"daylightTimes", 1, 134224634, 0, &DaylightTime_t2175_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_DateTime_t111_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime,System.Globalization.DaylightTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13905_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m13905/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_DateTime_t111_Object_t/* invoker_method */
	, TimeZone_t2551_TimeZone_IsDaylightSavingTime_m13905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_ToLocalTime_m13906_ParameterInfos[] = 
{
	{"time", 0, 134224635, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t111_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToLocalTime(System.DateTime)
extern const MethodInfo TimeZone_ToLocalTime_m13906_MethodInfo = 
{
	"ToLocalTime"/* name */
	, (methodPointerType)&TimeZone_ToLocalTime_m13906/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t111_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t111_DateTime_t111/* invoker_method */
	, TimeZone_t2551_TimeZone_ToLocalTime_m13906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_ToUniversalTime_m13907_ParameterInfos[] = 
{
	{"time", 0, 134224636, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t111_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToUniversalTime(System.DateTime)
extern const MethodInfo TimeZone_ToUniversalTime_m13907_MethodInfo = 
{
	"ToUniversalTime"/* name */
	, (methodPointerType)&TimeZone_ToUniversalTime_m13907/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t111_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t111_DateTime_t111/* invoker_method */
	, TimeZone_t2551_TimeZone_ToUniversalTime_m13907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_GetLocalTimeDiff_m13908_ParameterInfos[] = 
{
	{"time", 0, 134224637, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m13908_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m13908/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_DateTime_t111/* invoker_method */
	, TimeZone_t2551_TimeZone_GetLocalTimeDiff_m13908_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
extern const Il2CppType TimeSpan_t112_0_0_0;
static const ParameterInfo TimeZone_t2551_TimeZone_GetLocalTimeDiff_m13909_ParameterInfos[] = 
{
	{"time", 0, 134224638, 0, &DateTime_t111_0_0_0},
	{"utc_offset", 1, 134224639, 0, &TimeSpan_t112_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_DateTime_t111_TimeSpan_t112 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime,System.TimeSpan)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m13909_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m13909/* method */
	, &TimeZone_t2551_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_DateTime_t111_TimeSpan_t112/* invoker_method */
	, TimeZone_t2551_TimeZone_GetLocalTimeDiff_m13909_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeZone_t2551_MethodInfos[] =
{
	&TimeZone__ctor_m13901_MethodInfo,
	&TimeZone__cctor_m13902_MethodInfo,
	&TimeZone_get_CurrentTimeZone_m13903_MethodInfo,
	&TimeZone_GetDaylightChanges_m14643_MethodInfo,
	&TimeZone_GetUtcOffset_m14644_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13904_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13905_MethodInfo,
	&TimeZone_ToLocalTime_m13906_MethodInfo,
	&TimeZone_ToUniversalTime_m13907_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m13908_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m13909_MethodInfo,
	NULL
};
extern const MethodInfo TimeZone_get_CurrentTimeZone_m13903_MethodInfo;
static const PropertyInfo TimeZone_t2551____CurrentTimeZone_PropertyInfo = 
{
	&TimeZone_t2551_il2cpp_TypeInfo/* parent */
	, "CurrentTimeZone"/* name */
	, &TimeZone_get_CurrentTimeZone_m13903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeZone_t2551_PropertyInfos[] =
{
	&TimeZone_t2551____CurrentTimeZone_PropertyInfo,
	NULL
};
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13904_MethodInfo;
extern const MethodInfo TimeZone_ToLocalTime_m13906_MethodInfo;
extern const MethodInfo TimeZone_ToUniversalTime_m13907_MethodInfo;
static const Il2CppMethodReference TimeZone_t2551_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
	&TimeZone_IsDaylightSavingTime_m13904_MethodInfo,
	&TimeZone_ToLocalTime_m13906_MethodInfo,
	&TimeZone_ToUniversalTime_m13907_MethodInfo,
};
static bool TimeZone_t2551_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t2551_1_0_0;
struct TimeZone_t2551;
const Il2CppTypeDefinitionMetadata TimeZone_t2551_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t2551_VTable/* vtableMethods */
	, TimeZone_t2551_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2500/* fieldStart */

};
TypeInfo TimeZone_t2551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, TimeZone_t2551_MethodInfos/* methods */
	, TimeZone_t2551_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeZone_t2551_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 932/* custom_attributes_cache */
	, &TimeZone_t2551_0_0_0/* byval_arg */
	, &TimeZone_t2551_1_0_0/* this_arg */
	, &TimeZone_t2551_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t2551)/* instance_size */
	, sizeof (TimeZone_t2551)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t2551_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t2552_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor()
extern const MethodInfo CurrentSystemTimeZone__ctor_m13910_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m13910/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone__ctor_m13911_ParameterInfos[] = 
{
	{"lnow", 0, 134224640, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor(System.Int64)
extern const MethodInfo CurrentSystemTimeZone__ctor_m13911_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m13911/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone__ctor_m13911_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_ParameterInfos[] = 
{
	{"sender", 0, 134224641, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int64U5BU5D_t2586_1_0_2;
extern const Il2CppType Int64U5BU5D_t2586_1_0_0;
extern const Il2CppType StringU5BU5D_t109_1_0_2;
extern const Il2CppType StringU5BU5D_t109_1_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetTimeZoneData_m13913_ParameterInfos[] = 
{
	{"year", 0, 134224642, 0, &Int32_t127_0_0_0},
	{"data", 1, 134224643, 0, &Int64U5BU5D_t2586_1_0_2},
	{"names", 2, 134224644, 0, &StringU5BU5D_t109_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127_Int64U5BU5DU26_t3090_StringU5BU5DU26_t3091 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)
extern const MethodInfo CurrentSystemTimeZone_GetTimeZoneData_m13913_MethodInfo = 
{
	"GetTimeZoneData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetTimeZoneData_m13913/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127_Int64U5BU5DU26_t3090_StringU5BU5DU26_t3091/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetTimeZoneData_m13913_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetDaylightChanges_m13914_ParameterInfos[] = 
{
	{"year", 0, 134224645, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m13914_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightChanges_m13914/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2175_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetDaylightChanges_m13914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetUtcOffset_m13915_ParameterInfos[] = 
{
	{"time", 0, 134224646, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t112_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.CurrentSystemTimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m13915_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetUtcOffset_m13915/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t112_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t112_DateTime_t111/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetUtcOffset_m13915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DaylightTime_t2175_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_OnDeserialization_m13916_ParameterInfos[] = 
{
	{"dlt", 0, 134224647, 0, &DaylightTime_t2175_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::OnDeserialization(System.Globalization.DaylightTime)
extern const MethodInfo CurrentSystemTimeZone_OnDeserialization_m13916_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_OnDeserialization_m13916/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_OnDeserialization_m13916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64U5BU5D_t2586_0_0_0;
extern const Il2CppType Int64U5BU5D_t2586_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetDaylightTimeFromData_m13917_ParameterInfos[] = 
{
	{"data", 0, 134224648, 0, &Int64U5BU5D_t2586_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightTimeFromData(System.Int64[])
extern const MethodInfo CurrentSystemTimeZone_GetDaylightTimeFromData_m13917_MethodInfo = 
{
	"GetDaylightTimeFromData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightTimeFromData_m13917/* method */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2175_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2552_CurrentSystemTimeZone_GetDaylightTimeFromData_m13917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CurrentSystemTimeZone_t2552_MethodInfos[] =
{
	&CurrentSystemTimeZone__ctor_m13910_MethodInfo,
	&CurrentSystemTimeZone__ctor_m13911_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_MethodInfo,
	&CurrentSystemTimeZone_GetTimeZoneData_m13913_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m13914_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m13915_MethodInfo,
	&CurrentSystemTimeZone_OnDeserialization_m13916_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightTimeFromData_m13917_MethodInfo,
	NULL
};
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m13914_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m13915_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_MethodInfo;
static const Il2CppMethodReference CurrentSystemTimeZone_t2552_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m13914_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m13915_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13904_MethodInfo,
	&TimeZone_ToLocalTime_m13906_MethodInfo,
	&TimeZone_ToUniversalTime_m13907_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13912_MethodInfo,
};
static bool CurrentSystemTimeZone_t2552_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDeserializationCallback_t1609_0_0_0;
static const Il2CppType* CurrentSystemTimeZone_t2552_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t1609_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t2552_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t1609_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t2552_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t2552_1_0_0;
struct CurrentSystemTimeZone_t2552;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t2552_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t2552_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t2552_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t2551_0_0_0/* parent */
	, CurrentSystemTimeZone_t2552_VTable/* vtableMethods */
	, CurrentSystemTimeZone_t2552_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2501/* fieldStart */

};
TypeInfo CurrentSystemTimeZone_t2552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, CurrentSystemTimeZone_t2552_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CurrentSystemTimeZone_t2552_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t2552_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t2552_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t2552_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t2552)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t2552)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t2552_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// Metadata Definition System.TypeCode
extern TypeInfo TypeCode_t2553_il2cpp_TypeInfo;
// System.TypeCode
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
static const MethodInfo* TypeCode_t2553_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeCode_t2553_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TypeCode_t2553_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeCode_t2553_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeCode_t2553_0_0_0;
extern const Il2CppType TypeCode_t2553_1_0_0;
const Il2CppTypeDefinitionMetadata TypeCode_t2553_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeCode_t2553_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TypeCode_t2553_VTable/* vtableMethods */
	, TypeCode_t2553_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2509/* fieldStart */

};
TypeInfo TypeCode_t2553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeCode"/* name */
	, "System"/* namespaze */
	, TypeCode_t2553_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 933/* custom_attributes_cache */
	, &TypeCode_t2553_0_0_0/* byval_arg */
	, &TypeCode_t2553_1_0_0/* this_arg */
	, &TypeCode_t2553_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeCode_t2553)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeCode_t2553)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// Metadata Definition System.TypeInitializationException
extern TypeInfo TypeInitializationException_t2554_il2cpp_TypeInfo;
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo TypeInitializationException_t2554_TypeInitializationException__ctor_m13918_ParameterInfos[] = 
{
	{"info", 0, 134224649, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224650, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException__ctor_m13918_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInitializationException__ctor_m13918/* method */
	, &TypeInitializationException_t2554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, TypeInitializationException_t2554_TypeInitializationException__ctor_m13918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo TypeInitializationException_t2554_TypeInitializationException_GetObjectData_m13919_ParameterInfos[] = 
{
	{"info", 0, 134224651, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224652, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException_GetObjectData_m13919_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeInitializationException_GetObjectData_m13919/* method */
	, &TypeInitializationException_t2554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, TypeInitializationException_t2554_TypeInitializationException_GetObjectData_m13919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInitializationException_t2554_MethodInfos[] =
{
	&TypeInitializationException__ctor_m13918_MethodInfo,
	&TypeInitializationException_GetObjectData_m13919_MethodInfo,
	NULL
};
extern const MethodInfo TypeInitializationException_GetObjectData_m13919_MethodInfo;
static const Il2CppMethodReference TypeInitializationException_t2554_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&TypeInitializationException_GetObjectData_m13919_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&TypeInitializationException_GetObjectData_m13919_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool TypeInitializationException_t2554_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInitializationException_t2554_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInitializationException_t2554_0_0_0;
extern const Il2CppType TypeInitializationException_t2554_1_0_0;
struct TypeInitializationException_t2554;
const Il2CppTypeDefinitionMetadata TypeInitializationException_t2554_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInitializationException_t2554_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, TypeInitializationException_t2554_VTable/* vtableMethods */
	, TypeInitializationException_t2554_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2528/* fieldStart */

};
TypeInfo TypeInitializationException_t2554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInitializationException"/* name */
	, "System"/* namespaze */
	, TypeInitializationException_t2554_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInitializationException_t2554_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 934/* custom_attributes_cache */
	, &TypeInitializationException_t2554_0_0_0/* byval_arg */
	, &TypeInitializationException_t2554_1_0_0/* this_arg */
	, &TypeInitializationException_t2554_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInitializationException_t2554)/* instance_size */
	, sizeof (TypeInitializationException_t2554)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// Metadata Definition System.TypeLoadException
extern TypeInfo TypeLoadException_t2510_il2cpp_TypeInfo;
// System.TypeLoadException
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor()
extern const MethodInfo TypeLoadException__ctor_m13920_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13920/* method */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeLoadException_t2510_TypeLoadException__ctor_m13921_ParameterInfos[] = 
{
	{"message", 0, 134224653, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.String)
extern const MethodInfo TypeLoadException__ctor_m13921_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13921/* method */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TypeLoadException_t2510_TypeLoadException__ctor_m13921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo TypeLoadException_t2510_TypeLoadException__ctor_m13922_ParameterInfos[] = 
{
	{"info", 0, 134224654, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224655, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException__ctor_m13922_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13922/* method */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, TypeLoadException_t2510_TypeLoadException__ctor_m13922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TypeLoadException::get_Message()
extern const MethodInfo TypeLoadException_get_Message_m13923_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&TypeLoadException_get_Message_m13923/* method */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo TypeLoadException_t2510_TypeLoadException_GetObjectData_m13924_ParameterInfos[] = 
{
	{"info", 0, 134224656, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224657, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException_GetObjectData_m13924_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeLoadException_GetObjectData_m13924/* method */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, TypeLoadException_t2510_TypeLoadException_GetObjectData_m13924_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLoadException_t2510_MethodInfos[] =
{
	&TypeLoadException__ctor_m13920_MethodInfo,
	&TypeLoadException__ctor_m13921_MethodInfo,
	&TypeLoadException__ctor_m13922_MethodInfo,
	&TypeLoadException_get_Message_m13923_MethodInfo,
	&TypeLoadException_GetObjectData_m13924_MethodInfo,
	NULL
};
extern const MethodInfo TypeLoadException_get_Message_m13923_MethodInfo;
static const PropertyInfo TypeLoadException_t2510____Message_PropertyInfo = 
{
	&TypeLoadException_t2510_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &TypeLoadException_get_Message_m13923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeLoadException_t2510_PropertyInfos[] =
{
	&TypeLoadException_t2510____Message_PropertyInfo,
	NULL
};
extern const MethodInfo TypeLoadException_GetObjectData_m13924_MethodInfo;
static const Il2CppMethodReference TypeLoadException_t2510_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&TypeLoadException_GetObjectData_m13924_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&TypeLoadException_get_Message_m13923_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&TypeLoadException_GetObjectData_m13924_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool TypeLoadException_t2510_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLoadException_t2510_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLoadException_t2510_0_0_0;
extern const Il2CppType TypeLoadException_t2510_1_0_0;
struct TypeLoadException_t2510;
const Il2CppTypeDefinitionMetadata TypeLoadException_t2510_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLoadException_t2510_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, TypeLoadException_t2510_VTable/* vtableMethods */
	, TypeLoadException_t2510_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2529/* fieldStart */

};
TypeInfo TypeLoadException_t2510_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLoadException"/* name */
	, "System"/* namespaze */
	, TypeLoadException_t2510_MethodInfos/* methods */
	, TypeLoadException_t2510_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeLoadException_t2510_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 935/* custom_attributes_cache */
	, &TypeLoadException_t2510_0_0_0/* byval_arg */
	, &TypeLoadException_t2510_1_0_0/* this_arg */
	, &TypeLoadException_t2510_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLoadException_t2510)/* instance_size */
	, sizeof (TypeLoadException_t2510)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Metadata Definition System.UnauthorizedAccessException
extern TypeInfo UnauthorizedAccessException_t2555_il2cpp_TypeInfo;
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor()
extern const MethodInfo UnauthorizedAccessException__ctor_m13925_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13925/* method */
	, &UnauthorizedAccessException_t2555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2555_UnauthorizedAccessException__ctor_m13926_ParameterInfos[] = 
{
	{"message", 0, 134224658, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.String)
extern const MethodInfo UnauthorizedAccessException__ctor_m13926_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13926/* method */
	, &UnauthorizedAccessException_t2555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnauthorizedAccessException_t2555_UnauthorizedAccessException__ctor_m13926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2555_UnauthorizedAccessException__ctor_m13927_ParameterInfos[] = 
{
	{"info", 0, 134224659, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224660, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnauthorizedAccessException__ctor_m13927_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13927/* method */
	, &UnauthorizedAccessException_t2555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UnauthorizedAccessException_t2555_UnauthorizedAccessException__ctor_m13927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnauthorizedAccessException_t2555_MethodInfos[] =
{
	&UnauthorizedAccessException__ctor_m13925_MethodInfo,
	&UnauthorizedAccessException__ctor_m13926_MethodInfo,
	&UnauthorizedAccessException__ctor_m13927_MethodInfo,
	NULL
};
static const Il2CppMethodReference UnauthorizedAccessException_t2555_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool UnauthorizedAccessException_t2555_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnauthorizedAccessException_t2555_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnauthorizedAccessException_t2555_0_0_0;
extern const Il2CppType UnauthorizedAccessException_t2555_1_0_0;
struct UnauthorizedAccessException_t2555;
const Il2CppTypeDefinitionMetadata UnauthorizedAccessException_t2555_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnauthorizedAccessException_t2555_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2004_0_0_0/* parent */
	, UnauthorizedAccessException_t2555_VTable/* vtableMethods */
	, UnauthorizedAccessException_t2555_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnauthorizedAccessException_t2555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnauthorizedAccessException"/* name */
	, "System"/* namespaze */
	, UnauthorizedAccessException_t2555_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnauthorizedAccessException_t2555_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 936/* custom_attributes_cache */
	, &UnauthorizedAccessException_t2555_0_0_0/* byval_arg */
	, &UnauthorizedAccessException_t2555_1_0_0/* this_arg */
	, &UnauthorizedAccessException_t2555_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnauthorizedAccessException_t2555)/* instance_size */
	, sizeof (UnauthorizedAccessException_t2555)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// Metadata Definition System.UnhandledExceptionEventArgs
extern TypeInfo UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo;
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo UnhandledExceptionEventArgs_t2556_UnhandledExceptionEventArgs__ctor_m13928_ParameterInfos[] = 
{
	{"exception", 0, 134224661, 0, &Object_t_0_0_0},
	{"isTerminating", 1, 134224662, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventArgs::.ctor(System.Object,System.Boolean)
extern const MethodInfo UnhandledExceptionEventArgs__ctor_m13928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs__ctor_m13928/* method */
	, &UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, UnhandledExceptionEventArgs_t2556_UnhandledExceptionEventArgs__ctor_m13928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m13929_MethodInfo = 
{
	"get_ExceptionObject"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_ExceptionObject_m13929/* method */
	, &UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 938/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.UnhandledExceptionEventArgs::get_IsTerminating()
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m13930_MethodInfo = 
{
	"get_IsTerminating"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_IsTerminating_m13930/* method */
	, &UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 939/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventArgs_t2556_MethodInfos[] =
{
	&UnhandledExceptionEventArgs__ctor_m13928_MethodInfo,
	&UnhandledExceptionEventArgs_get_ExceptionObject_m13929_MethodInfo,
	&UnhandledExceptionEventArgs_get_IsTerminating_m13930_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m13929_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2556____ExceptionObject_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* parent */
	, "ExceptionObject"/* name */
	, &UnhandledExceptionEventArgs_get_ExceptionObject_m13929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m13930_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2556____IsTerminating_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* parent */
	, "IsTerminating"/* name */
	, &UnhandledExceptionEventArgs_get_IsTerminating_m13930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UnhandledExceptionEventArgs_t2556_PropertyInfos[] =
{
	&UnhandledExceptionEventArgs_t2556____ExceptionObject_PropertyInfo,
	&UnhandledExceptionEventArgs_t2556____IsTerminating_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UnhandledExceptionEventArgs_t2556_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool UnhandledExceptionEventArgs_t2556_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventArgs_t2556_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2556_1_0_0;
struct UnhandledExceptionEventArgs_t2556;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventArgs_t2556_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1688_0_0_0/* parent */
	, UnhandledExceptionEventArgs_t2556_VTable/* vtableMethods */
	, UnhandledExceptionEventArgs_t2556_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2532/* fieldStart */

};
TypeInfo UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventArgs"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventArgs_t2556_MethodInfos/* methods */
	, UnhandledExceptionEventArgs_t2556_PropertyInfos/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventArgs_t2556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 937/* custom_attributes_cache */
	, &UnhandledExceptionEventArgs_t2556_0_0_0/* byval_arg */
	, &UnhandledExceptionEventArgs_t2556_1_0_0/* this_arg */
	, &UnhandledExceptionEventArgs_t2556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventArgs_t2556)/* instance_size */
	, sizeof (UnhandledExceptionEventArgs_t2556)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// Metadata Definition System.UnitySerializationHolder/UnityType
extern TypeInfo UnityType_t2557_il2cpp_TypeInfo;
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
static const MethodInfo* UnityType_t2557_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityType_t2557_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UnityType_t2557_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityType_t2557_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnityType_t2557_0_0_0;
extern const Il2CppType UnityType_t2557_1_0_0;
extern TypeInfo UnitySerializationHolder_t2558_il2cpp_TypeInfo;
extern const Il2CppType UnitySerializationHolder_t2558_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t449_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityType_t2557_DefinitionMetadata = 
{
	&UnitySerializationHolder_t2558_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityType_t2557_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UnityType_t2557_VTable/* vtableMethods */
	, UnityType_t2557_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2534/* fieldStart */

};
TypeInfo UnityType_t2557_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityType"/* name */
	, ""/* namespaze */
	, UnityType_t2557_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityType_t2557_0_0_0/* byval_arg */
	, &UnityType_t2557_1_0_0/* this_arg */
	, &UnityType_t2557_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityType_t2557)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityType_t2557)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// Metadata Definition System.UnitySerializationHolder
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder__ctor_m13931_ParameterInfos[] = 
{
	{"info", 0, 134224663, 0, &SerializationInfo_t1382_0_0_0},
	{"ctx", 1, 134224664, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder__ctor_m13931_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnitySerializationHolder__ctor_m13931/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder__ctor_m13931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder_GetTypeData_m13932_ParameterInfos[] = 
{
	{"instance", 0, 134224665, 0, &Type_t_0_0_0},
	{"info", 1, 134224666, 0, &SerializationInfo_t1382_0_0_0},
	{"ctx", 2, 134224667, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetTypeData_m13932_MethodInfo = 
{
	"GetTypeData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetTypeData_m13932/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder_GetTypeData_m13932_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DBNull_t2501_0_0_0;
extern const Il2CppType DBNull_t2501_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder_GetDBNullData_m13933_ParameterInfos[] = 
{
	{"instance", 0, 134224668, 0, &DBNull_t2501_0_0_0},
	{"info", 1, 134224669, 0, &SerializationInfo_t1382_0_0_0},
	{"ctx", 2, 134224670, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetDBNullData_m13933_MethodInfo = 
{
	"GetDBNullData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetDBNullData_m13933/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder_GetDBNullData_m13933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t2226_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder_GetModuleData_m13934_ParameterInfos[] = 
{
	{"instance", 0, 134224671, 0, &Module_t2226_0_0_0},
	{"info", 1, 134224672, 0, &SerializationInfo_t1382_0_0_0},
	{"ctx", 2, 134224673, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetModuleData_m13934_MethodInfo = 
{
	"GetModuleData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetModuleData_m13934/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder_GetModuleData_m13934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder_GetObjectData_m13935_ParameterInfos[] = 
{
	{"info", 0, 134224674, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224675, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetObjectData_m13935_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetObjectData_m13935/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder_GetObjectData_m13935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2558_UnitySerializationHolder_GetRealObject_m13936_ParameterInfos[] = 
{
	{"context", 0, 134224676, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetRealObject_m13936_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetRealObject_m13936/* method */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1383/* invoker_method */
	, UnitySerializationHolder_t2558_UnitySerializationHolder_GetRealObject_m13936_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnitySerializationHolder_t2558_MethodInfos[] =
{
	&UnitySerializationHolder__ctor_m13931_MethodInfo,
	&UnitySerializationHolder_GetTypeData_m13932_MethodInfo,
	&UnitySerializationHolder_GetDBNullData_m13933_MethodInfo,
	&UnitySerializationHolder_GetModuleData_m13934_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13935_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13936_MethodInfo,
	NULL
};
static const Il2CppType* UnitySerializationHolder_t2558_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnityType_t2557_0_0_0,
};
extern const MethodInfo UnitySerializationHolder_GetObjectData_m13935_MethodInfo;
extern const MethodInfo UnitySerializationHolder_GetRealObject_m13936_MethodInfo;
static const Il2CppMethodReference UnitySerializationHolder_t2558_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13935_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13936_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13935_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13936_MethodInfo,
};
static bool UnitySerializationHolder_t2558_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t2615_0_0_0;
static const Il2CppType* UnitySerializationHolder_t2558_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
	&IObjectReference_t2615_0_0_0,
};
static Il2CppInterfaceOffsetPair UnitySerializationHolder_t2558_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &IObjectReference_t2615_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnitySerializationHolder_t2558_1_0_0;
struct UnitySerializationHolder_t2558;
const Il2CppTypeDefinitionMetadata UnitySerializationHolder_t2558_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnitySerializationHolder_t2558_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, UnitySerializationHolder_t2558_InterfacesTypeInfos/* implementedInterfaces */
	, UnitySerializationHolder_t2558_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnitySerializationHolder_t2558_VTable/* vtableMethods */
	, UnitySerializationHolder_t2558_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2539/* fieldStart */

};
TypeInfo UnitySerializationHolder_t2558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnitySerializationHolder"/* name */
	, "System"/* namespaze */
	, UnitySerializationHolder_t2558_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnitySerializationHolder_t2558_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnitySerializationHolder_t2558_0_0_0/* byval_arg */
	, &UnitySerializationHolder_t2558_1_0_0/* this_arg */
	, &UnitySerializationHolder_t2558_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnitySerializationHolder_t2558)/* instance_size */
	, sizeof (UnitySerializationHolder_t2558)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Version
#include "mscorlib_System_Version.h"
// Metadata Definition System.Version
extern TypeInfo Version_t1875_il2cpp_TypeInfo;
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor()
extern const MethodInfo Version__ctor_m13937_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13937/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1875_Version__ctor_m13938_ParameterInfos[] = 
{
	{"version", 0, 134224677, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.String)
extern const MethodInfo Version__ctor_m13938_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13938/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Version_t1875_Version__ctor_m13938_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Version_t1875_Version__ctor_m9334_ParameterInfos[] = 
{
	{"major", 0, 134224678, 0, &Int32_t127_0_0_0},
	{"minor", 1, 134224679, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m9334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m9334/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, Version_t1875_Version__ctor_m9334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Version_t1875_Version__ctor_m13939_ParameterInfos[] = 
{
	{"major", 0, 134224680, 0, &Int32_t127_0_0_0},
	{"minor", 1, 134224681, 0, &Int32_t127_0_0_0},
	{"build", 2, 134224682, 0, &Int32_t127_0_0_0},
	{"revision", 3, 134224683, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m13939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13939/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Version_t1875_Version__ctor_m13939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Version_t1875_Version_CheckedSet_m13940_ParameterInfos[] = 
{
	{"defined", 0, 134224684, 0, &Int32_t127_0_0_0},
	{"major", 1, 134224685, 0, &Int32_t127_0_0_0},
	{"minor", 2, 134224686, 0, &Int32_t127_0_0_0},
	{"build", 3, 134224687, 0, &Int32_t127_0_0_0},
	{"revision", 4, 134224688, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version_CheckedSet_m13940_MethodInfo = 
{
	"CheckedSet"/* name */
	, (methodPointerType)&Version_CheckedSet_m13940/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Version_t1875_Version_CheckedSet_m13940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Build()
extern const MethodInfo Version_get_Build_m13941_MethodInfo = 
{
	"get_Build"/* name */
	, (methodPointerType)&Version_get_Build_m13941/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Major()
extern const MethodInfo Version_get_Major_m13942_MethodInfo = 
{
	"get_Major"/* name */
	, (methodPointerType)&Version_get_Major_m13942/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Minor()
extern const MethodInfo Version_get_Minor_m13943_MethodInfo = 
{
	"get_Minor"/* name */
	, (methodPointerType)&Version_get_Minor_m13943/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Revision()
extern const MethodInfo Version_get_Revision_m13944_MethodInfo = 
{
	"get_Revision"/* name */
	, (methodPointerType)&Version_get_Revision_m13944/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1875_Version_CompareTo_m13945_ParameterInfos[] = 
{
	{"version", 0, 134224689, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Object)
extern const MethodInfo Version_CompareTo_m13945_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m13945/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Version_t1875_Version_CompareTo_m13945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1875_Version_Equals_m13946_ParameterInfos[] = 
{
	{"obj", 0, 134224690, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Object)
extern const MethodInfo Version_Equals_m13946_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m13946/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Version_t1875_Version_Equals_m13946_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1875_0_0_0;
static const ParameterInfo Version_t1875_Version_CompareTo_m13947_ParameterInfos[] = 
{
	{"value", 0, 134224691, 0, &Version_t1875_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Version)
extern const MethodInfo Version_CompareTo_m13947_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m13947/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Version_t1875_Version_CompareTo_m13947_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1875_0_0_0;
static const ParameterInfo Version_t1875_Version_Equals_m13948_ParameterInfos[] = 
{
	{"obj", 0, 134224692, 0, &Version_t1875_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Version)
extern const MethodInfo Version_Equals_m13948_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m13948/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Version_t1875_Version_Equals_m13948_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::GetHashCode()
extern const MethodInfo Version_GetHashCode_m13949_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Version_GetHashCode_m13949/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Version::ToString()
extern const MethodInfo Version_ToString_m13950_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Version_ToString_m13950/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1875_Version_CreateFromString_m13951_ParameterInfos[] = 
{
	{"info", 0, 134224693, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Version::CreateFromString(System.String)
extern const MethodInfo Version_CreateFromString_m13951_MethodInfo = 
{
	"CreateFromString"/* name */
	, (methodPointerType)&Version_CreateFromString_m13951/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1875_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Version_t1875_Version_CreateFromString_m13951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1875_0_0_0;
extern const Il2CppType Version_t1875_0_0_0;
static const ParameterInfo Version_t1875_Version_op_Equality_m13952_ParameterInfos[] = 
{
	{"v1", 0, 134224694, 0, &Version_t1875_0_0_0},
	{"v2", 1, 134224695, 0, &Version_t1875_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern const MethodInfo Version_op_Equality_m13952_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Version_op_Equality_m13952/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, Version_t1875_Version_op_Equality_m13952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1875_0_0_0;
extern const Il2CppType Version_t1875_0_0_0;
static const ParameterInfo Version_t1875_Version_op_Inequality_m13953_ParameterInfos[] = 
{
	{"v1", 0, 134224696, 0, &Version_t1875_0_0_0},
	{"v2", 1, 134224697, 0, &Version_t1875_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern const MethodInfo Version_op_Inequality_m13953_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&Version_op_Inequality_m13953/* method */
	, &Version_t1875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, Version_t1875_Version_op_Inequality_m13953_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Version_t1875_MethodInfos[] =
{
	&Version__ctor_m13937_MethodInfo,
	&Version__ctor_m13938_MethodInfo,
	&Version__ctor_m9334_MethodInfo,
	&Version__ctor_m13939_MethodInfo,
	&Version_CheckedSet_m13940_MethodInfo,
	&Version_get_Build_m13941_MethodInfo,
	&Version_get_Major_m13942_MethodInfo,
	&Version_get_Minor_m13943_MethodInfo,
	&Version_get_Revision_m13944_MethodInfo,
	&Version_CompareTo_m13945_MethodInfo,
	&Version_Equals_m13946_MethodInfo,
	&Version_CompareTo_m13947_MethodInfo,
	&Version_Equals_m13948_MethodInfo,
	&Version_GetHashCode_m13949_MethodInfo,
	&Version_ToString_m13950_MethodInfo,
	&Version_CreateFromString_m13951_MethodInfo,
	&Version_op_Equality_m13952_MethodInfo,
	&Version_op_Inequality_m13953_MethodInfo,
	NULL
};
extern const MethodInfo Version_get_Build_m13941_MethodInfo;
static const PropertyInfo Version_t1875____Build_PropertyInfo = 
{
	&Version_t1875_il2cpp_TypeInfo/* parent */
	, "Build"/* name */
	, &Version_get_Build_m13941_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Major_m13942_MethodInfo;
static const PropertyInfo Version_t1875____Major_PropertyInfo = 
{
	&Version_t1875_il2cpp_TypeInfo/* parent */
	, "Major"/* name */
	, &Version_get_Major_m13942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Minor_m13943_MethodInfo;
static const PropertyInfo Version_t1875____Minor_PropertyInfo = 
{
	&Version_t1875_il2cpp_TypeInfo/* parent */
	, "Minor"/* name */
	, &Version_get_Minor_m13943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Revision_m13944_MethodInfo;
static const PropertyInfo Version_t1875____Revision_PropertyInfo = 
{
	&Version_t1875_il2cpp_TypeInfo/* parent */
	, "Revision"/* name */
	, &Version_get_Revision_m13944_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Version_t1875_PropertyInfos[] =
{
	&Version_t1875____Build_PropertyInfo,
	&Version_t1875____Major_PropertyInfo,
	&Version_t1875____Minor_PropertyInfo,
	&Version_t1875____Revision_PropertyInfo,
	NULL
};
extern const MethodInfo Version_Equals_m13946_MethodInfo;
extern const MethodInfo Version_GetHashCode_m13949_MethodInfo;
extern const MethodInfo Version_ToString_m13950_MethodInfo;
extern const MethodInfo Version_CompareTo_m13945_MethodInfo;
extern const MethodInfo Version_CompareTo_m13947_MethodInfo;
extern const MethodInfo Version_Equals_m13948_MethodInfo;
static const Il2CppMethodReference Version_t1875_VTable[] =
{
	&Version_Equals_m13946_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Version_GetHashCode_m13949_MethodInfo,
	&Version_ToString_m13950_MethodInfo,
	&Version_CompareTo_m13945_MethodInfo,
	&Version_CompareTo_m13947_MethodInfo,
	&Version_Equals_m13948_MethodInfo,
};
static bool Version_t1875_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t3092_0_0_0;
extern const Il2CppType IEquatable_1_t3093_0_0_0;
static const Il2CppType* Version_t1875_InterfacesTypeInfos[] = 
{
	&IComparable_t166_0_0_0,
	&ICloneable_t512_0_0_0,
	&IComparable_1_t3092_0_0_0,
	&IEquatable_1_t3093_0_0_0,
};
static Il2CppInterfaceOffsetPair Version_t1875_InterfacesOffsets[] = 
{
	{ &IComparable_t166_0_0_0, 4},
	{ &ICloneable_t512_0_0_0, 5},
	{ &IComparable_1_t3092_0_0_0, 5},
	{ &IEquatable_1_t3093_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Version_t1875_1_0_0;
struct Version_t1875;
const Il2CppTypeDefinitionMetadata Version_t1875_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Version_t1875_InterfacesTypeInfos/* implementedInterfaces */
	, Version_t1875_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Version_t1875_VTable/* vtableMethods */
	, Version_t1875_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2542/* fieldStart */

};
TypeInfo Version_t1875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Version"/* name */
	, "System"/* namespaze */
	, Version_t1875_MethodInfos/* methods */
	, Version_t1875_PropertyInfos/* properties */
	, NULL/* events */
	, &Version_t1875_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 940/* custom_attributes_cache */
	, &Version_t1875_0_0_0/* byval_arg */
	, &Version_t1875_1_0_0/* this_arg */
	, &Version_t1875_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Version_t1875)/* instance_size */
	, sizeof (Version_t1875)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// Metadata Definition System.WeakReference
extern TypeInfo WeakReference_t2344_il2cpp_TypeInfo;
// System.WeakReference
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor()
extern const MethodInfo WeakReference__ctor_m13954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13954/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2344_WeakReference__ctor_m13955_ParameterInfos[] = 
{
	{"target", 0, 134224698, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object)
extern const MethodInfo WeakReference__ctor_m13955_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13955/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, WeakReference_t2344_WeakReference__ctor_m13955_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo WeakReference_t2344_WeakReference__ctor_m13956_ParameterInfos[] = 
{
	{"target", 0, 134224699, 0, &Object_t_0_0_0},
	{"trackResurrection", 1, 134224700, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
extern const MethodInfo WeakReference__ctor_m13956_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13956/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, WeakReference_t2344_WeakReference__ctor_m13956_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo WeakReference_t2344_WeakReference__ctor_m13957_ParameterInfos[] = 
{
	{"info", 0, 134224701, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224702, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference__ctor_m13957_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13957/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, WeakReference_t2344_WeakReference__ctor_m13957_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2344_WeakReference_AllocateHandle_m13958_ParameterInfos[] = 
{
	{"target", 0, 134224703, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::AllocateHandle(System.Object)
extern const MethodInfo WeakReference_AllocateHandle_m13958_MethodInfo = 
{
	"AllocateHandle"/* name */
	, (methodPointerType)&WeakReference_AllocateHandle_m13958/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, WeakReference_t2344_WeakReference_AllocateHandle_m13958_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.WeakReference::get_Target()
extern const MethodInfo WeakReference_get_Target_m13959_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&WeakReference_get_Target_m13959/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.WeakReference::get_TrackResurrection()
extern const MethodInfo WeakReference_get_TrackResurrection_m13960_MethodInfo = 
{
	"get_TrackResurrection"/* name */
	, (methodPointerType)&WeakReference_get_TrackResurrection_m13960/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::Finalize()
extern const MethodInfo WeakReference_Finalize_m13961_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&WeakReference_Finalize_m13961/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo WeakReference_t2344_WeakReference_GetObjectData_m13962_ParameterInfos[] = 
{
	{"info", 0, 134224704, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134224705, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference_GetObjectData_m13962_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&WeakReference_GetObjectData_m13962/* method */
	, &WeakReference_t2344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, WeakReference_t2344_WeakReference_GetObjectData_m13962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WeakReference_t2344_MethodInfos[] =
{
	&WeakReference__ctor_m13954_MethodInfo,
	&WeakReference__ctor_m13955_MethodInfo,
	&WeakReference__ctor_m13956_MethodInfo,
	&WeakReference__ctor_m13957_MethodInfo,
	&WeakReference_AllocateHandle_m13958_MethodInfo,
	&WeakReference_get_Target_m13959_MethodInfo,
	&WeakReference_get_TrackResurrection_m13960_MethodInfo,
	&WeakReference_Finalize_m13961_MethodInfo,
	&WeakReference_GetObjectData_m13962_MethodInfo,
	NULL
};
extern const MethodInfo WeakReference_get_Target_m13959_MethodInfo;
static const PropertyInfo WeakReference_t2344____Target_PropertyInfo = 
{
	&WeakReference_t2344_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &WeakReference_get_Target_m13959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WeakReference_get_TrackResurrection_m13960_MethodInfo;
static const PropertyInfo WeakReference_t2344____TrackResurrection_PropertyInfo = 
{
	&WeakReference_t2344_il2cpp_TypeInfo/* parent */
	, "TrackResurrection"/* name */
	, &WeakReference_get_TrackResurrection_m13960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WeakReference_t2344_PropertyInfos[] =
{
	&WeakReference_t2344____Target_PropertyInfo,
	&WeakReference_t2344____TrackResurrection_PropertyInfo,
	NULL
};
extern const MethodInfo WeakReference_Finalize_m13961_MethodInfo;
extern const MethodInfo WeakReference_GetObjectData_m13962_MethodInfo;
static const Il2CppMethodReference WeakReference_t2344_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&WeakReference_Finalize_m13961_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&WeakReference_GetObjectData_m13962_MethodInfo,
	&WeakReference_get_Target_m13959_MethodInfo,
	&WeakReference_get_TrackResurrection_m13960_MethodInfo,
	&WeakReference_GetObjectData_m13962_MethodInfo,
};
static bool WeakReference_t2344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WeakReference_t2344_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
};
static Il2CppInterfaceOffsetPair WeakReference_t2344_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WeakReference_t2344_0_0_0;
extern const Il2CppType WeakReference_t2344_1_0_0;
struct WeakReference_t2344;
const Il2CppTypeDefinitionMetadata WeakReference_t2344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WeakReference_t2344_InterfacesTypeInfos/* implementedInterfaces */
	, WeakReference_t2344_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WeakReference_t2344_VTable/* vtableMethods */
	, WeakReference_t2344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2547/* fieldStart */

};
TypeInfo WeakReference_t2344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WeakReference"/* name */
	, "System"/* namespaze */
	, WeakReference_t2344_MethodInfos/* methods */
	, WeakReference_t2344_PropertyInfos/* properties */
	, NULL/* events */
	, &WeakReference_t2344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 941/* custom_attributes_cache */
	, &WeakReference_t2344_0_0_0/* byval_arg */
	, &WeakReference_t2344_1_0_0/* this_arg */
	, &WeakReference_t2344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WeakReference_t2344)/* instance_size */
	, sizeof (WeakReference_t2344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t2559_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t2559_PrimalityTest__ctor_m13963_ParameterInfos[] = 
{
	{"object", 0, 134224706, 0, &Object_t_0_0_0},
	{"method", 1, 134224707, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m13963_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m13963/* method */
	, &PrimalityTest_t2559_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t2559_PrimalityTest__ctor_m13963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t2095_0_0_0;
extern const Il2CppType BigInteger_t2095_0_0_0;
extern const Il2CppType ConfidenceFactor_t2092_0_0_0;
extern const Il2CppType ConfidenceFactor_t2092_0_0_0;
static const ParameterInfo PrimalityTest_t2559_PrimalityTest_Invoke_m13964_ParameterInfos[] = 
{
	{"bi", 0, 134224708, 0, &BigInteger_t2095_0_0_0},
	{"confidence", 1, 134224709, 0, &ConfidenceFactor_t2092_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m13964_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m13964/* method */
	, &PrimalityTest_t2559_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Int32_t127/* invoker_method */
	, PrimalityTest_t2559_PrimalityTest_Invoke_m13964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t2095_0_0_0;
extern const Il2CppType ConfidenceFactor_t2092_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t2559_PrimalityTest_BeginInvoke_m13965_ParameterInfos[] = 
{
	{"bi", 0, 134224710, 0, &BigInteger_t2095_0_0_0},
	{"confidence", 1, 134224711, 0, &ConfidenceFactor_t2092_0_0_0},
	{"callback", 2, 134224712, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224713, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m13965_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m13965/* method */
	, &PrimalityTest_t2559_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t2559_PrimalityTest_BeginInvoke_m13965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo PrimalityTest_t2559_PrimalityTest_EndInvoke_m13966_ParameterInfos[] = 
{
	{"result", 0, 134224714, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m13966_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m13966/* method */
	, &PrimalityTest_t2559_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, PrimalityTest_t2559_PrimalityTest_EndInvoke_m13966_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t2559_MethodInfos[] =
{
	&PrimalityTest__ctor_m13963_MethodInfo,
	&PrimalityTest_Invoke_m13964_MethodInfo,
	&PrimalityTest_BeginInvoke_m13965_MethodInfo,
	&PrimalityTest_EndInvoke_m13966_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m13964_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m13965_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m13966_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t2559_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&PrimalityTest_Invoke_m13964_MethodInfo,
	&PrimalityTest_BeginInvoke_m13965_MethodInfo,
	&PrimalityTest_EndInvoke_m13966_MethodInfo,
};
static bool PrimalityTest_t2559_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrimalityTest_t2559_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTest_t2559_0_0_0;
extern const Il2CppType PrimalityTest_t2559_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct PrimalityTest_t2559;
const Il2CppTypeDefinitionMetadata PrimalityTest_t2559_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t2559_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, PrimalityTest_t2559_VTable/* vtableMethods */
	, PrimalityTest_t2559_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t2559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t2559_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t2559_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t2559_0_0_0/* byval_arg */
	, &PrimalityTest_t2559_1_0_0/* this_arg */
	, &PrimalityTest_t2559_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t2559/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t2559)/* instance_size */
	, sizeof (PrimalityTest_t2559)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// Metadata Definition System.Reflection.MemberFilter
extern TypeInfo MemberFilter_t2046_il2cpp_TypeInfo;
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MemberFilter_t2046_MemberFilter__ctor_m13967_ParameterInfos[] = 
{
	{"object", 0, 134224715, 0, &Object_t_0_0_0},
	{"method", 1, 134224716, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MemberFilter__ctor_m13967_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberFilter__ctor_m13967/* method */
	, &MemberFilter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, MemberFilter_t2046_MemberFilter__ctor_m13967_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t2046_MemberFilter_Invoke_m13968_ParameterInfos[] = 
{
	{"m", 0, 134224717, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224718, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern const MethodInfo MemberFilter_Invoke_m13968_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MemberFilter_Invoke_m13968/* method */
	, &MemberFilter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, MemberFilter_t2046_MemberFilter_Invoke_m13968_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t2046_MemberFilter_BeginInvoke_m13969_ParameterInfos[] = 
{
	{"m", 0, 134224719, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224720, 0, &Object_t_0_0_0},
	{"callback", 2, 134224721, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224722, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo MemberFilter_BeginInvoke_m13969_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MemberFilter_BeginInvoke_m13969/* method */
	, &MemberFilter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MemberFilter_t2046_MemberFilter_BeginInvoke_m13969_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo MemberFilter_t2046_MemberFilter_EndInvoke_m13970_ParameterInfos[] = 
{
	{"result", 0, 134224723, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo MemberFilter_EndInvoke_m13970_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MemberFilter_EndInvoke_m13970/* method */
	, &MemberFilter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, MemberFilter_t2046_MemberFilter_EndInvoke_m13970_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberFilter_t2046_MethodInfos[] =
{
	&MemberFilter__ctor_m13967_MethodInfo,
	&MemberFilter_Invoke_m13968_MethodInfo,
	&MemberFilter_BeginInvoke_m13969_MethodInfo,
	&MemberFilter_EndInvoke_m13970_MethodInfo,
	NULL
};
extern const MethodInfo MemberFilter_Invoke_m13968_MethodInfo;
extern const MethodInfo MemberFilter_BeginInvoke_m13969_MethodInfo;
extern const MethodInfo MemberFilter_EndInvoke_m13970_MethodInfo;
static const Il2CppMethodReference MemberFilter_t2046_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&MemberFilter_Invoke_m13968_MethodInfo,
	&MemberFilter_BeginInvoke_m13969_MethodInfo,
	&MemberFilter_EndInvoke_m13970_MethodInfo,
};
static bool MemberFilter_t2046_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberFilter_t2046_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberFilter_t2046_0_0_0;
extern const Il2CppType MemberFilter_t2046_1_0_0;
struct MemberFilter_t2046;
const Il2CppTypeDefinitionMetadata MemberFilter_t2046_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberFilter_t2046_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, MemberFilter_t2046_VTable/* vtableMethods */
	, MemberFilter_t2046_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberFilter_t2046_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberFilter"/* name */
	, "System.Reflection"/* namespaze */
	, MemberFilter_t2046_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberFilter_t2046_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 942/* custom_attributes_cache */
	, &MemberFilter_t2046_0_0_0/* byval_arg */
	, &MemberFilter_t2046_1_0_0/* this_arg */
	, &MemberFilter_t2046_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MemberFilter_t2046/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberFilter_t2046)/* instance_size */
	, sizeof (MemberFilter_t2046)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// Metadata Definition System.Reflection.TypeFilter
extern TypeInfo TypeFilter_t2251_il2cpp_TypeInfo;
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TypeFilter_t2251_TypeFilter__ctor_m13971_ParameterInfos[] = 
{
	{"object", 0, 134224724, 0, &Object_t_0_0_0},
	{"method", 1, 134224725, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TypeFilter__ctor_m13971_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeFilter__ctor_m13971/* method */
	, &TypeFilter_t2251_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, TypeFilter_t2251_TypeFilter__ctor_m13971_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t2251_TypeFilter_Invoke_m13972_ParameterInfos[] = 
{
	{"m", 0, 134224726, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224727, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern const MethodInfo TypeFilter_Invoke_m13972_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TypeFilter_Invoke_m13972/* method */
	, &TypeFilter_t2251_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, TypeFilter_t2251_TypeFilter_Invoke_m13972_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t2251_TypeFilter_BeginInvoke_m13973_ParameterInfos[] = 
{
	{"m", 0, 134224728, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224729, 0, &Object_t_0_0_0},
	{"callback", 2, 134224730, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224731, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo TypeFilter_BeginInvoke_m13973_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TypeFilter_BeginInvoke_m13973/* method */
	, &TypeFilter_t2251_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, TypeFilter_t2251_TypeFilter_BeginInvoke_m13973_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo TypeFilter_t2251_TypeFilter_EndInvoke_m13974_ParameterInfos[] = 
{
	{"result", 0, 134224732, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo TypeFilter_EndInvoke_m13974_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TypeFilter_EndInvoke_m13974/* method */
	, &TypeFilter_t2251_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TypeFilter_t2251_TypeFilter_EndInvoke_m13974_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeFilter_t2251_MethodInfos[] =
{
	&TypeFilter__ctor_m13971_MethodInfo,
	&TypeFilter_Invoke_m13972_MethodInfo,
	&TypeFilter_BeginInvoke_m13973_MethodInfo,
	&TypeFilter_EndInvoke_m13974_MethodInfo,
	NULL
};
extern const MethodInfo TypeFilter_Invoke_m13972_MethodInfo;
extern const MethodInfo TypeFilter_BeginInvoke_m13973_MethodInfo;
extern const MethodInfo TypeFilter_EndInvoke_m13974_MethodInfo;
static const Il2CppMethodReference TypeFilter_t2251_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&TypeFilter_Invoke_m13972_MethodInfo,
	&TypeFilter_BeginInvoke_m13973_MethodInfo,
	&TypeFilter_EndInvoke_m13974_MethodInfo,
};
static bool TypeFilter_t2251_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilter_t2251_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilter_t2251_0_0_0;
extern const Il2CppType TypeFilter_t2251_1_0_0;
struct TypeFilter_t2251;
const Il2CppTypeDefinitionMetadata TypeFilter_t2251_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilter_t2251_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, TypeFilter_t2251_VTable/* vtableMethods */
	, TypeFilter_t2251_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TypeFilter_t2251_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilter"/* name */
	, "System.Reflection"/* namespaze */
	, TypeFilter_t2251_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeFilter_t2251_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 943/* custom_attributes_cache */
	, &TypeFilter_t2251_0_0_0/* byval_arg */
	, &TypeFilter_t2251_1_0_0/* this_arg */
	, &TypeFilter_t2251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TypeFilter_t2251/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilter_t2251)/* instance_size */
	, sizeof (TypeFilter_t2251)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern TypeInfo HeaderHandler_t2561_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo HeaderHandler_t2561_HeaderHandler__ctor_m13975_ParameterInfos[] = 
{
	{"object", 0, 134224733, 0, &Object_t_0_0_0},
	{"method", 1, 134224734, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.HeaderHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo HeaderHandler__ctor_m13975_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HeaderHandler__ctor_m13975/* method */
	, &HeaderHandler_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, HeaderHandler_t2561_HeaderHandler__ctor_m13975_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2560_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2560_0_0_0;
static const ParameterInfo HeaderHandler_t2561_HeaderHandler_Invoke_m13976_ParameterInfos[] = 
{
	{"headers", 0, 134224735, 0, &HeaderU5BU5D_t2560_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::Invoke(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo HeaderHandler_Invoke_m13976_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&HeaderHandler_Invoke_m13976/* method */
	, &HeaderHandler_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2561_HeaderHandler_Invoke_m13976_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2560_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HeaderHandler_t2561_HeaderHandler_BeginInvoke_m13977_ParameterInfos[] = 
{
	{"headers", 0, 134224736, 0, &HeaderU5BU5D_t2560_0_0_0},
	{"callback", 1, 134224737, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134224738, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Remoting.Messaging.HeaderHandler::BeginInvoke(System.Runtime.Remoting.Messaging.Header[],System.AsyncCallback,System.Object)
extern const MethodInfo HeaderHandler_BeginInvoke_m13977_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&HeaderHandler_BeginInvoke_m13977/* method */
	, &HeaderHandler_t2561_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2561_HeaderHandler_BeginInvoke_m13977_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo HeaderHandler_t2561_HeaderHandler_EndInvoke_m13978_ParameterInfos[] = 
{
	{"result", 0, 134224739, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo HeaderHandler_EndInvoke_m13978_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&HeaderHandler_EndInvoke_m13978/* method */
	, &HeaderHandler_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2561_HeaderHandler_EndInvoke_m13978_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HeaderHandler_t2561_MethodInfos[] =
{
	&HeaderHandler__ctor_m13975_MethodInfo,
	&HeaderHandler_Invoke_m13976_MethodInfo,
	&HeaderHandler_BeginInvoke_m13977_MethodInfo,
	&HeaderHandler_EndInvoke_m13978_MethodInfo,
	NULL
};
extern const MethodInfo HeaderHandler_Invoke_m13976_MethodInfo;
extern const MethodInfo HeaderHandler_BeginInvoke_m13977_MethodInfo;
extern const MethodInfo HeaderHandler_EndInvoke_m13978_MethodInfo;
static const Il2CppMethodReference HeaderHandler_t2561_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&HeaderHandler_Invoke_m13976_MethodInfo,
	&HeaderHandler_BeginInvoke_m13977_MethodInfo,
	&HeaderHandler_EndInvoke_m13978_MethodInfo,
};
static bool HeaderHandler_t2561_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HeaderHandler_t2561_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HeaderHandler_t2561_0_0_0;
extern const Il2CppType HeaderHandler_t2561_1_0_0;
struct HeaderHandler_t2561;
const Il2CppTypeDefinitionMetadata HeaderHandler_t2561_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HeaderHandler_t2561_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, HeaderHandler_t2561_VTable/* vtableMethods */
	, HeaderHandler_t2561_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HeaderHandler_t2561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeaderHandler"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, HeaderHandler_t2561_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HeaderHandler_t2561_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 944/* custom_attributes_cache */
	, &HeaderHandler_t2561_0_0_0/* byval_arg */
	, &HeaderHandler_t2561_1_0_0/* this_arg */
	, &HeaderHandler_t2561_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_HeaderHandler_t2561/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeaderHandler_t2561)/* instance_size */
	, sizeof (HeaderHandler_t2561)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`1
extern TypeInfo Action_1_t2693_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Action_1_t2693_Il2CppGenericContainer;
extern TypeInfo Action_1_t2693_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Action_1_t2693_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Action_1_t2693_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Action_1_t2693_Il2CppGenericParametersArray[1] = 
{
	&Action_1_t2693_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Action_1_t2693_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Action_1_t2693_il2cpp_TypeInfo, 1, 0, Action_1_t2693_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_1_t2693_Action_1__ctor_m14645_ParameterInfos[] = 
{
	{"object", 0, 134224740, 0, &Object_t_0_0_0},
	{"method", 1, 134224741, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Action`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action_1__ctor_m14645_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_1_t2693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2693_Action_1__ctor_m14645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2693_gp_0_0_0_0;
extern const Il2CppType Action_1_t2693_gp_0_0_0_0;
static const ParameterInfo Action_1_t2693_Action_1_Invoke_m14646_ParameterInfos[] = 
{
	{"obj", 0, 134224742, 0, &Action_1_t2693_gp_0_0_0_0},
};
// System.Void System.Action`1::Invoke(T)
extern const MethodInfo Action_1_Invoke_m14646_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_1_t2693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2693_Action_1_Invoke_m14646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2693_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_1_t2693_Action_1_BeginInvoke_m14647_ParameterInfos[] = 
{
	{"obj", 0, 134224743, 0, &Action_1_t2693_gp_0_0_0_0},
	{"callback", 1, 134224744, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134224745, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Action`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Action_1_BeginInvoke_m14647_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2693_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2693_Action_1_BeginInvoke_m14647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Action_1_t2693_Action_1_EndInvoke_m14648_ParameterInfos[] = 
{
	{"result", 0, 134224746, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void System.Action`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_1_EndInvoke_m14648_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2693_Action_1_EndInvoke_m14648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_1_t2693_MethodInfos[] =
{
	&Action_1__ctor_m14645_MethodInfo,
	&Action_1_Invoke_m14646_MethodInfo,
	&Action_1_BeginInvoke_m14647_MethodInfo,
	&Action_1_EndInvoke_m14648_MethodInfo,
	NULL
};
extern const MethodInfo Action_1_Invoke_m14646_MethodInfo;
extern const MethodInfo Action_1_BeginInvoke_m14647_MethodInfo;
extern const MethodInfo Action_1_EndInvoke_m14648_MethodInfo;
static const Il2CppMethodReference Action_1_t2693_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Action_1_Invoke_m14646_MethodInfo,
	&Action_1_BeginInvoke_m14647_MethodInfo,
	&Action_1_EndInvoke_m14648_MethodInfo,
};
static bool Action_1_t2693_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_1_t2693_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Action_1_t2693_0_0_0;
extern const Il2CppType Action_1_t2693_1_0_0;
struct Action_1_t2693;
const Il2CppTypeDefinitionMetadata Action_1_t2693_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_1_t2693_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Action_1_t2693_VTable/* vtableMethods */
	, Action_1_t2693_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_1_t2693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2693_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_1_t2693_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_1_t2693_0_0_0/* byval_arg */
	, &Action_1_t2693_1_0_0/* this_arg */
	, &Action_1_t2693_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_1_t2693_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// Metadata Definition System.AppDomainInitializer
extern TypeInfo AppDomainInitializer_t2492_il2cpp_TypeInfo;
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2492_AppDomainInitializer__ctor_m13979_ParameterInfos[] = 
{
	{"object", 0, 134224747, 0, &Object_t_0_0_0},
	{"method", 1, 134224748, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AppDomainInitializer__ctor_m13979_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainInitializer__ctor_m13979/* method */
	, &AppDomainInitializer_t2492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, AppDomainInitializer_t2492_AppDomainInitializer__ctor_m13979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
static const ParameterInfo AppDomainInitializer_t2492_AppDomainInitializer_Invoke_m13980_ParameterInfos[] = 
{
	{"args", 0, 134224749, 0, &StringU5BU5D_t109_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::Invoke(System.String[])
extern const MethodInfo AppDomainInitializer_Invoke_m13980_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AppDomainInitializer_Invoke_m13980/* method */
	, &AppDomainInitializer_t2492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AppDomainInitializer_t2492_AppDomainInitializer_Invoke_m13980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2492_AppDomainInitializer_BeginInvoke_m13981_ParameterInfos[] = 
{
	{"args", 0, 134224750, 0, &StringU5BU5D_t109_0_0_0},
	{"callback", 1, 134224751, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134224752, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AppDomainInitializer::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern const MethodInfo AppDomainInitializer_BeginInvoke_m13981_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_BeginInvoke_m13981/* method */
	, &AppDomainInitializer_t2492_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AppDomainInitializer_t2492_AppDomainInitializer_BeginInvoke_m13981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo AppDomainInitializer_t2492_AppDomainInitializer_EndInvoke_m13982_ParameterInfos[] = 
{
	{"result", 0, 134224753, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::EndInvoke(System.IAsyncResult)
extern const MethodInfo AppDomainInitializer_EndInvoke_m13982_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_EndInvoke_m13982/* method */
	, &AppDomainInitializer_t2492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AppDomainInitializer_t2492_AppDomainInitializer_EndInvoke_m13982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainInitializer_t2492_MethodInfos[] =
{
	&AppDomainInitializer__ctor_m13979_MethodInfo,
	&AppDomainInitializer_Invoke_m13980_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m13981_MethodInfo,
	&AppDomainInitializer_EndInvoke_m13982_MethodInfo,
	NULL
};
extern const MethodInfo AppDomainInitializer_Invoke_m13980_MethodInfo;
extern const MethodInfo AppDomainInitializer_BeginInvoke_m13981_MethodInfo;
extern const MethodInfo AppDomainInitializer_EndInvoke_m13982_MethodInfo;
static const Il2CppMethodReference AppDomainInitializer_t2492_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&AppDomainInitializer_Invoke_m13980_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m13981_MethodInfo,
	&AppDomainInitializer_EndInvoke_m13982_MethodInfo,
};
static bool AppDomainInitializer_t2492_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AppDomainInitializer_t2492_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainInitializer_t2492_0_0_0;
extern const Il2CppType AppDomainInitializer_t2492_1_0_0;
struct AppDomainInitializer_t2492;
const Il2CppTypeDefinitionMetadata AppDomainInitializer_t2492_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppDomainInitializer_t2492_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, AppDomainInitializer_t2492_VTable/* vtableMethods */
	, AppDomainInitializer_t2492_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AppDomainInitializer_t2492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainInitializer"/* name */
	, "System"/* namespaze */
	, AppDomainInitializer_t2492_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainInitializer_t2492_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 945/* custom_attributes_cache */
	, &AppDomainInitializer_t2492_0_0_0/* byval_arg */
	, &AppDomainInitializer_t2492_1_0_0/* this_arg */
	, &AppDomainInitializer_t2492_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AppDomainInitializer_t2492/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainInitializer_t2492)/* instance_size */
	, sizeof (AppDomainInitializer_t2492)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// Metadata Definition System.AssemblyLoadEventHandler
extern TypeInfo AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo;
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler__ctor_m13983_ParameterInfos[] = 
{
	{"object", 0, 134224754, 0, &Object_t_0_0_0},
	{"method", 1, 134224755, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AssemblyLoadEventHandler__ctor_m13983_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler__ctor_m13983/* method */
	, &AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler__ctor_m13983_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2496_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2496_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_Invoke_m13984_ParameterInfos[] = 
{
	{"sender", 0, 134224756, 0, &Object_t_0_0_0},
	{"args", 1, 134224757, 0, &AssemblyLoadEventArgs_t2496_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::Invoke(System.Object,System.AssemblyLoadEventArgs)
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m13984_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_Invoke_m13984/* method */
	, &AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_Invoke_m13984_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2496_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_BeginInvoke_m13985_ParameterInfos[] = 
{
	{"sender", 0, 134224758, 0, &Object_t_0_0_0},
	{"args", 1, 134224759, 0, &AssemblyLoadEventArgs_t2496_0_0_0},
	{"callback", 2, 134224760, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224761, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AssemblyLoadEventHandler::BeginInvoke(System.Object,System.AssemblyLoadEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m13985_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_BeginInvoke_m13985/* method */
	, &AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_BeginInvoke_m13985_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_EndInvoke_m13986_ParameterInfos[] = 
{
	{"result", 0, 134224762, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m13986_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_EndInvoke_m13986/* method */
	, &AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2488_AssemblyLoadEventHandler_EndInvoke_m13986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyLoadEventHandler_t2488_MethodInfos[] =
{
	&AssemblyLoadEventHandler__ctor_m13983_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m13984_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m13985_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m13986_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m13984_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m13985_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m13986_MethodInfo;
static const Il2CppMethodReference AssemblyLoadEventHandler_t2488_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m13984_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m13985_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m13986_MethodInfo,
};
static bool AssemblyLoadEventHandler_t2488_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyLoadEventHandler_t2488_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventHandler_t2488_0_0_0;
extern const Il2CppType AssemblyLoadEventHandler_t2488_1_0_0;
struct AssemblyLoadEventHandler_t2488;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventHandler_t2488_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyLoadEventHandler_t2488_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, AssemblyLoadEventHandler_t2488_VTable/* vtableMethods */
	, AssemblyLoadEventHandler_t2488_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventHandler"/* name */
	, "System"/* namespaze */
	, AssemblyLoadEventHandler_t2488_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyLoadEventHandler_t2488_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 946/* custom_attributes_cache */
	, &AssemblyLoadEventHandler_t2488_0_0_0/* byval_arg */
	, &AssemblyLoadEventHandler_t2488_1_0_0/* this_arg */
	, &AssemblyLoadEventHandler_t2488_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2488/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventHandler_t2488)/* instance_size */
	, sizeof (AssemblyLoadEventHandler_t2488)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Comparison`1
extern TypeInfo Comparison_1_t2694_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Comparison_1_t2694_Il2CppGenericContainer;
extern TypeInfo Comparison_1_t2694_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Comparison_1_t2694_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Comparison_1_t2694_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Comparison_1_t2694_Il2CppGenericParametersArray[1] = 
{
	&Comparison_1_t2694_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Comparison_1_t2694_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Comparison_1_t2694_il2cpp_TypeInfo, 1, 0, Comparison_1_t2694_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Comparison_1_t2694_Comparison_1__ctor_m14649_ParameterInfos[] = 
{
	{"object", 0, 134224763, 0, &Object_t_0_0_0},
	{"method", 1, 134224764, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Comparison`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Comparison_1__ctor_m14649_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Comparison_1_t2694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2694_Comparison_1__ctor_m14649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2694_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2694_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2694_gp_0_0_0_0;
static const ParameterInfo Comparison_1_t2694_Comparison_1_Invoke_m14650_ParameterInfos[] = 
{
	{"x", 0, 134224765, 0, &Comparison_1_t2694_gp_0_0_0_0},
	{"y", 1, 134224766, 0, &Comparison_1_t2694_gp_0_0_0_0},
};
// System.Int32 System.Comparison`1::Invoke(T,T)
extern const MethodInfo Comparison_1_Invoke_m14650_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2694_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2694_Comparison_1_Invoke_m14650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2694_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2694_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Comparison_1_t2694_Comparison_1_BeginInvoke_m14651_ParameterInfos[] = 
{
	{"x", 0, 134224767, 0, &Comparison_1_t2694_gp_0_0_0_0},
	{"y", 1, 134224768, 0, &Comparison_1_t2694_gp_0_0_0_0},
	{"callback", 2, 134224769, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224770, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Comparison`1::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern const MethodInfo Comparison_1_BeginInvoke_m14651_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2694_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2694_Comparison_1_BeginInvoke_m14651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Comparison_1_t2694_Comparison_1_EndInvoke_m14652_ParameterInfos[] = 
{
	{"result", 0, 134224771, 0, &IAsyncResult_t304_0_0_0},
};
// System.Int32 System.Comparison`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Comparison_1_EndInvoke_m14652_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2694_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2694_Comparison_1_EndInvoke_m14652_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Comparison_1_t2694_MethodInfos[] =
{
	&Comparison_1__ctor_m14649_MethodInfo,
	&Comparison_1_Invoke_m14650_MethodInfo,
	&Comparison_1_BeginInvoke_m14651_MethodInfo,
	&Comparison_1_EndInvoke_m14652_MethodInfo,
	NULL
};
extern const MethodInfo Comparison_1_Invoke_m14650_MethodInfo;
extern const MethodInfo Comparison_1_BeginInvoke_m14651_MethodInfo;
extern const MethodInfo Comparison_1_EndInvoke_m14652_MethodInfo;
static const Il2CppMethodReference Comparison_1_t2694_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Comparison_1_Invoke_m14650_MethodInfo,
	&Comparison_1_BeginInvoke_m14651_MethodInfo,
	&Comparison_1_EndInvoke_m14652_MethodInfo,
};
static bool Comparison_1_t2694_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Comparison_1_t2694_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparison_1_t2694_0_0_0;
extern const Il2CppType Comparison_1_t2694_1_0_0;
struct Comparison_1_t2694;
const Il2CppTypeDefinitionMetadata Comparison_1_t2694_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Comparison_1_t2694_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Comparison_1_t2694_VTable/* vtableMethods */
	, Comparison_1_t2694_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Comparison_1_t2694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2694_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Comparison_1_t2694_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparison_1_t2694_0_0_0/* byval_arg */
	, &Comparison_1_t2694_1_0_0/* this_arg */
	, &Comparison_1_t2694_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Comparison_1_t2694_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Converter`2
extern TypeInfo Converter_2_t2695_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Converter_2_t2695_Il2CppGenericContainer;
extern TypeInfo Converter_2_t2695_gp_TInput_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2695_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2695_Il2CppGenericContainer, NULL, "TInput", 0, 0 };
extern TypeInfo Converter_2_t2695_gp_TOutput_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2695_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2695_Il2CppGenericContainer, NULL, "TOutput", 1, 0 };
static const Il2CppGenericParameter* Converter_2_t2695_Il2CppGenericParametersArray[2] = 
{
	&Converter_2_t2695_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull,
	&Converter_2_t2695_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Converter_2_t2695_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Converter_2_t2695_il2cpp_TypeInfo, 2, 0, Converter_2_t2695_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Converter_2_t2695_Converter_2__ctor_m14653_ParameterInfos[] = 
{
	{"object", 0, 134224772, 0, &Object_t_0_0_0},
	{"method", 1, 134224773, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Converter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Converter_2__ctor_m14653_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Converter_2_t2695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2695_Converter_2__ctor_m14653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2695_gp_0_0_0_0;
extern const Il2CppType Converter_2_t2695_gp_0_0_0_0;
static const ParameterInfo Converter_2_t2695_Converter_2_Invoke_m14654_ParameterInfos[] = 
{
	{"input", 0, 134224774, 0, &Converter_2_t2695_gp_0_0_0_0},
};
extern const Il2CppType Converter_2_t2695_gp_1_0_0_0;
// TOutput System.Converter`2::Invoke(TInput)
extern const MethodInfo Converter_2_Invoke_m14654_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Converter_2_t2695_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2695_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2695_Converter_2_Invoke_m14654_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2695_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Converter_2_t2695_Converter_2_BeginInvoke_m14655_ParameterInfos[] = 
{
	{"input", 0, 134224775, 0, &Converter_2_t2695_gp_0_0_0_0},
	{"callback", 1, 134224776, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134224777, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Converter`2::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern const MethodInfo Converter_2_BeginInvoke_m14655_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2695_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2695_Converter_2_BeginInvoke_m14655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Converter_2_t2695_Converter_2_EndInvoke_m14656_ParameterInfos[] = 
{
	{"result", 0, 134224778, 0, &IAsyncResult_t304_0_0_0},
};
// TOutput System.Converter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Converter_2_EndInvoke_m14656_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2695_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2695_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2695_Converter_2_EndInvoke_m14656_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Converter_2_t2695_MethodInfos[] =
{
	&Converter_2__ctor_m14653_MethodInfo,
	&Converter_2_Invoke_m14654_MethodInfo,
	&Converter_2_BeginInvoke_m14655_MethodInfo,
	&Converter_2_EndInvoke_m14656_MethodInfo,
	NULL
};
extern const MethodInfo Converter_2_Invoke_m14654_MethodInfo;
extern const MethodInfo Converter_2_BeginInvoke_m14655_MethodInfo;
extern const MethodInfo Converter_2_EndInvoke_m14656_MethodInfo;
static const Il2CppMethodReference Converter_2_t2695_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Converter_2_Invoke_m14654_MethodInfo,
	&Converter_2_BeginInvoke_m14655_MethodInfo,
	&Converter_2_EndInvoke_m14656_MethodInfo,
};
static bool Converter_2_t2695_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Converter_2_t2695_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Converter_2_t2695_0_0_0;
extern const Il2CppType Converter_2_t2695_1_0_0;
struct Converter_2_t2695;
const Il2CppTypeDefinitionMetadata Converter_2_t2695_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Converter_2_t2695_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Converter_2_t2695_VTable/* vtableMethods */
	, Converter_2_t2695_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Converter_2_t2695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Converter`2"/* name */
	, "System"/* namespaze */
	, Converter_2_t2695_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Converter_2_t2695_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Converter_2_t2695_0_0_0/* byval_arg */
	, &Converter_2_t2695_1_0_0/* this_arg */
	, &Converter_2_t2695_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Converter_2_t2695_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// Metadata Definition System.EventHandler
extern TypeInfo EventHandler_t2490_il2cpp_TypeInfo;
// System.EventHandler
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EventHandler_t2490_EventHandler__ctor_m13987_ParameterInfos[] = 
{
	{"object", 0, 134224779, 0, &Object_t_0_0_0},
	{"method", 1, 134224780, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EventHandler__ctor_m13987_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventHandler__ctor_m13987/* method */
	, &EventHandler_t2490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, EventHandler_t2490_EventHandler__ctor_m13987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1688_0_0_0;
static const ParameterInfo EventHandler_t2490_EventHandler_Invoke_m13988_ParameterInfos[] = 
{
	{"sender", 0, 134224781, 0, &Object_t_0_0_0},
	{"e", 1, 134224782, 0, &EventArgs_t1688_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern const MethodInfo EventHandler_Invoke_m13988_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EventHandler_Invoke_m13988/* method */
	, &EventHandler_t2490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, EventHandler_t2490_EventHandler_Invoke_m13988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1688_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EventHandler_t2490_EventHandler_BeginInvoke_m13989_ParameterInfos[] = 
{
	{"sender", 0, 134224783, 0, &Object_t_0_0_0},
	{"e", 1, 134224784, 0, &EventArgs_t1688_0_0_0},
	{"callback", 2, 134224785, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224786, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.EventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo EventHandler_BeginInvoke_m13989_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EventHandler_BeginInvoke_m13989/* method */
	, &EventHandler_t2490_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, EventHandler_t2490_EventHandler_BeginInvoke_m13989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo EventHandler_t2490_EventHandler_EndInvoke_m13990_ParameterInfos[] = 
{
	{"result", 0, 134224787, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo EventHandler_EndInvoke_m13990_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EventHandler_EndInvoke_m13990/* method */
	, &EventHandler_t2490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, EventHandler_t2490_EventHandler_EndInvoke_m13990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventHandler_t2490_MethodInfos[] =
{
	&EventHandler__ctor_m13987_MethodInfo,
	&EventHandler_Invoke_m13988_MethodInfo,
	&EventHandler_BeginInvoke_m13989_MethodInfo,
	&EventHandler_EndInvoke_m13990_MethodInfo,
	NULL
};
extern const MethodInfo EventHandler_Invoke_m13988_MethodInfo;
extern const MethodInfo EventHandler_BeginInvoke_m13989_MethodInfo;
extern const MethodInfo EventHandler_EndInvoke_m13990_MethodInfo;
static const Il2CppMethodReference EventHandler_t2490_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&EventHandler_Invoke_m13988_MethodInfo,
	&EventHandler_BeginInvoke_m13989_MethodInfo,
	&EventHandler_EndInvoke_m13990_MethodInfo,
};
static bool EventHandler_t2490_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventHandler_t2490_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventHandler_t2490_0_0_0;
extern const Il2CppType EventHandler_t2490_1_0_0;
struct EventHandler_t2490;
const Il2CppTypeDefinitionMetadata EventHandler_t2490_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandler_t2490_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, EventHandler_t2490_VTable/* vtableMethods */
	, EventHandler_t2490_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EventHandler_t2490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandler"/* name */
	, "System"/* namespaze */
	, EventHandler_t2490_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EventHandler_t2490_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 947/* custom_attributes_cache */
	, &EventHandler_t2490_0_0_0/* byval_arg */
	, &EventHandler_t2490_1_0_0/* this_arg */
	, &EventHandler_t2490_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EventHandler_t2490/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandler_t2490)/* instance_size */
	, sizeof (EventHandler_t2490)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Predicate`1
extern TypeInfo Predicate_1_t2696_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Predicate_1_t2696_Il2CppGenericContainer;
extern TypeInfo Predicate_1_t2696_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Predicate_1_t2696_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Predicate_1_t2696_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Predicate_1_t2696_Il2CppGenericParametersArray[1] = 
{
	&Predicate_1_t2696_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Predicate_1_t2696_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Predicate_1_t2696_il2cpp_TypeInfo, 1, 0, Predicate_1_t2696_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Predicate_1_t2696_Predicate_1__ctor_m14657_ParameterInfos[] = 
{
	{"object", 0, 134224788, 0, &Object_t_0_0_0},
	{"method", 1, 134224789, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Predicate`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Predicate_1__ctor_m14657_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Predicate_1_t2696_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2696_Predicate_1__ctor_m14657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2696_gp_0_0_0_0;
extern const Il2CppType Predicate_1_t2696_gp_0_0_0_0;
static const ParameterInfo Predicate_1_t2696_Predicate_1_Invoke_m14658_ParameterInfos[] = 
{
	{"obj", 0, 134224790, 0, &Predicate_1_t2696_gp_0_0_0_0},
};
// System.Boolean System.Predicate`1::Invoke(T)
extern const MethodInfo Predicate_1_Invoke_m14658_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2696_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2696_Predicate_1_Invoke_m14658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2696_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Predicate_1_t2696_Predicate_1_BeginInvoke_m14659_ParameterInfos[] = 
{
	{"obj", 0, 134224791, 0, &Predicate_1_t2696_gp_0_0_0_0},
	{"callback", 1, 134224792, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134224793, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Predicate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Predicate_1_BeginInvoke_m14659_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2696_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2696_Predicate_1_BeginInvoke_m14659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Predicate_1_t2696_Predicate_1_EndInvoke_m14660_ParameterInfos[] = 
{
	{"result", 0, 134224794, 0, &IAsyncResult_t304_0_0_0},
};
// System.Boolean System.Predicate`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Predicate_1_EndInvoke_m14660_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2696_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2696_Predicate_1_EndInvoke_m14660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Predicate_1_t2696_MethodInfos[] =
{
	&Predicate_1__ctor_m14657_MethodInfo,
	&Predicate_1_Invoke_m14658_MethodInfo,
	&Predicate_1_BeginInvoke_m14659_MethodInfo,
	&Predicate_1_EndInvoke_m14660_MethodInfo,
	NULL
};
extern const MethodInfo Predicate_1_Invoke_m14658_MethodInfo;
extern const MethodInfo Predicate_1_BeginInvoke_m14659_MethodInfo;
extern const MethodInfo Predicate_1_EndInvoke_m14660_MethodInfo;
static const Il2CppMethodReference Predicate_1_t2696_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Predicate_1_Invoke_m14658_MethodInfo,
	&Predicate_1_BeginInvoke_m14659_MethodInfo,
	&Predicate_1_EndInvoke_m14660_MethodInfo,
};
static bool Predicate_1_t2696_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Predicate_1_t2696_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Predicate_1_t2696_0_0_0;
extern const Il2CppType Predicate_1_t2696_1_0_0;
struct Predicate_1_t2696;
const Il2CppTypeDefinitionMetadata Predicate_1_t2696_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Predicate_1_t2696_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Predicate_1_t2696_VTable/* vtableMethods */
	, Predicate_1_t2696_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Predicate_1_t2696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t2696_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Predicate_1_t2696_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Predicate_1_t2696_0_0_0/* byval_arg */
	, &Predicate_1_t2696_1_0_0/* this_arg */
	, &Predicate_1_t2696_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Predicate_1_t2696_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// Metadata Definition System.ResolveEventHandler
extern TypeInfo ResolveEventHandler_t2489_il2cpp_TypeInfo;
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2489_ResolveEventHandler__ctor_m13991_ParameterInfos[] = 
{
	{"object", 0, 134224795, 0, &Object_t_0_0_0},
	{"method", 1, 134224796, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ResolveEventHandler__ctor_m13991_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ResolveEventHandler__ctor_m13991/* method */
	, &ResolveEventHandler_t2489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, ResolveEventHandler_t2489_ResolveEventHandler__ctor_m13991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2544_0_0_0;
static const ParameterInfo ResolveEventHandler_t2489_ResolveEventHandler_Invoke_m13992_ParameterInfos[] = 
{
	{"sender", 0, 134224797, 0, &Object_t_0_0_0},
	{"args", 1, 134224798, 0, &ResolveEventArgs_t2544_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern const MethodInfo ResolveEventHandler_Invoke_m13992_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ResolveEventHandler_Invoke_m13992/* method */
	, &ResolveEventHandler_t2489_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2003_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2489_ResolveEventHandler_Invoke_m13992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2544_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2489_ResolveEventHandler_BeginInvoke_m13993_ParameterInfos[] = 
{
	{"sender", 0, 134224799, 0, &Object_t_0_0_0},
	{"args", 1, 134224800, 0, &ResolveEventArgs_t2544_0_0_0},
	{"callback", 2, 134224801, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224802, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo ResolveEventHandler_BeginInvoke_m13993_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_BeginInvoke_m13993/* method */
	, &ResolveEventHandler_t2489_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2489_ResolveEventHandler_BeginInvoke_m13993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo ResolveEventHandler_t2489_ResolveEventHandler_EndInvoke_m13994_ParameterInfos[] = 
{
	{"result", 0, 134224803, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo ResolveEventHandler_EndInvoke_m13994_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_EndInvoke_m13994/* method */
	, &ResolveEventHandler_t2489_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2003_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2489_ResolveEventHandler_EndInvoke_m13994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ResolveEventHandler_t2489_MethodInfos[] =
{
	&ResolveEventHandler__ctor_m13991_MethodInfo,
	&ResolveEventHandler_Invoke_m13992_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m13993_MethodInfo,
	&ResolveEventHandler_EndInvoke_m13994_MethodInfo,
	NULL
};
extern const MethodInfo ResolveEventHandler_Invoke_m13992_MethodInfo;
extern const MethodInfo ResolveEventHandler_BeginInvoke_m13993_MethodInfo;
extern const MethodInfo ResolveEventHandler_EndInvoke_m13994_MethodInfo;
static const Il2CppMethodReference ResolveEventHandler_t2489_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&ResolveEventHandler_Invoke_m13992_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m13993_MethodInfo,
	&ResolveEventHandler_EndInvoke_m13994_MethodInfo,
};
static bool ResolveEventHandler_t2489_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ResolveEventHandler_t2489_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHandler_t2489_0_0_0;
extern const Il2CppType ResolveEventHandler_t2489_1_0_0;
struct ResolveEventHandler_t2489;
const Il2CppTypeDefinitionMetadata ResolveEventHandler_t2489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResolveEventHandler_t2489_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, ResolveEventHandler_t2489_VTable/* vtableMethods */
	, ResolveEventHandler_t2489_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventHandler_t2489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHandler"/* name */
	, "System"/* namespaze */
	, ResolveEventHandler_t2489_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventHandler_t2489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 948/* custom_attributes_cache */
	, &ResolveEventHandler_t2489_0_0_0/* byval_arg */
	, &ResolveEventHandler_t2489_1_0_0/* this_arg */
	, &ResolveEventHandler_t2489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ResolveEventHandler_t2489/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHandler_t2489)/* instance_size */
	, sizeof (ResolveEventHandler_t2489)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// Metadata Definition System.UnhandledExceptionEventHandler
extern TypeInfo UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo;
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler__ctor_m13995_ParameterInfos[] = 
{
	{"object", 0, 134224804, 0, &Object_t_0_0_0},
	{"method", 1, 134224805, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnhandledExceptionEventHandler__ctor_m13995_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler__ctor_m13995/* method */
	, &UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler__ctor_m13995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2556_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_Invoke_m13996_ParameterInfos[] = 
{
	{"sender", 0, 134224806, 0, &Object_t_0_0_0},
	{"e", 1, 134224807, 0, &UnhandledExceptionEventArgs_t2556_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::Invoke(System.Object,System.UnhandledExceptionEventArgs)
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m13996_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_Invoke_m13996/* method */
	, &UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_Invoke_m13996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2556_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_BeginInvoke_m13997_ParameterInfos[] = 
{
	{"sender", 0, 134224808, 0, &Object_t_0_0_0},
	{"e", 1, 134224809, 0, &UnhandledExceptionEventArgs_t2556_0_0_0},
	{"callback", 2, 134224810, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134224811, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.UnhandledExceptionEventHandler::BeginInvoke(System.Object,System.UnhandledExceptionEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m13997_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_BeginInvoke_m13997/* method */
	, &UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_BeginInvoke_m13997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_EndInvoke_m13998_ParameterInfos[] = 
{
	{"result", 0, 134224812, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m13998_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_EndInvoke_m13998/* method */
	, &UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2491_UnhandledExceptionEventHandler_EndInvoke_m13998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventHandler_t2491_MethodInfos[] =
{
	&UnhandledExceptionEventHandler__ctor_m13995_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m13996_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m13997_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m13998_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m13996_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m13997_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m13998_MethodInfo;
static const Il2CppMethodReference UnhandledExceptionEventHandler_t2491_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m13996_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m13997_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m13998_MethodInfo,
};
static bool UnhandledExceptionEventHandler_t2491_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnhandledExceptionEventHandler_t2491_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventHandler_t2491_0_0_0;
extern const Il2CppType UnhandledExceptionEventHandler_t2491_1_0_0;
struct UnhandledExceptionEventHandler_t2491;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventHandler_t2491_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnhandledExceptionEventHandler_t2491_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnhandledExceptionEventHandler_t2491_VTable/* vtableMethods */
	, UnhandledExceptionEventHandler_t2491_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventHandler"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventHandler_t2491_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventHandler_t2491_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 949/* custom_attributes_cache */
	, &UnhandledExceptionEventHandler_t2491_0_0_0/* byval_arg */
	, &UnhandledExceptionEventHandler_t2491_1_0_0/* this_arg */
	, &UnhandledExceptionEventHandler_t2491_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t2491/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventHandler_t2491)/* instance_size */
	, sizeof (UnhandledExceptionEventHandler_t2491)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
extern TypeInfo U24ArrayTypeU2456_t2562_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2456_t2562_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU2456_t2562_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2456_t2562_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2456_t2562_0_0_0;
extern const Il2CppType U24ArrayTypeU2456_t2562_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2582_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2582_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2456_t2562_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2456_t2562_VTable/* vtableMethods */
	, U24ArrayTypeU2456_t2562_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2456_t2562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$56"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2456_t2562_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2456_t2562_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2456_t2562_0_0_0/* byval_arg */
	, &U24ArrayTypeU2456_t2562_1_0_0/* this_arg */
	, &U24ArrayTypeU2456_t2562_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2456_t2562_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2562_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2562_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2456_t2562)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2456_t2562)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2456_t2562_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
extern TypeInfo U24ArrayTypeU2424_t2563_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2424_t2563_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2424_t2563_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2424_t2563_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2424_t2563_0_0_0;
extern const Il2CppType U24ArrayTypeU2424_t2563_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2424_t2563_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2424_t2563_VTable/* vtableMethods */
	, U24ArrayTypeU2424_t2563_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2424_t2563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$24"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2424_t2563_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2424_t2563_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2424_t2563_0_0_0/* byval_arg */
	, &U24ArrayTypeU2424_t2563_1_0_0/* this_arg */
	, &U24ArrayTypeU2424_t2563_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2424_t2563_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2563_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2563_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2424_t2563)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2424_t2563)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2424_t2563_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t2564_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t2564_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t2564_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2416_t2564_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t2564_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t2564_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t2564_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2416_t2564_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t2564_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t2564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t2564_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t2564_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t2564_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t2564_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t2564_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t2564_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2564_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2564_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t2564)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t2564)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t2564_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t2565_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24120_t2565_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24120_t2565_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24120_t2565_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t2565_0_0_0;
extern const Il2CppType U24ArrayTypeU24120_t2565_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t2565_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24120_t2565_VTable/* vtableMethods */
	, U24ArrayTypeU24120_t2565_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24120_t2565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24120_t2565_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24120_t2565_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t2565_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t2565_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t2565_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t2565_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2565_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2565_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t2565)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t2565)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t2565_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t2566_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t2566_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU243132_t2566_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU243132_t2566_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t2566_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t2566_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t2566_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU243132_t2566_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t2566_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t2566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t2566_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t2566_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t2566_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t2566_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t2566_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t2566_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2566_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2566_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t2566)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t2566)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t2566_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t2567_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t2567_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t2567_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2420_t2567_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t2567_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t2567_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t2567_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2420_t2567_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t2567_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t2567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t2567_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t2567_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t2567_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t2567_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t2567_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t2567_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2567_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2567_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t2567)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t2567)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t2567_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t2568_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t2568_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t2568_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2432_t2568_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t2568_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t2568_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t2568_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2432_t2568_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t2568_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t2568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t2568_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t2568_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t2568_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t2568_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t2568_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t2568_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2568_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2568_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t2568)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t2568)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t2568_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t2569_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t2569_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t2569_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2448_t2569_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t2569_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t2569_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t2569_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2448_t2569_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t2569_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t2569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t2569_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t2569_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t2569_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t2569_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t2569_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t2569_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2569_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2569_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t2569)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t2569)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t2569_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t2570_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t2570_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t2570_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2464_t2570_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t2570_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t2570_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t2570_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2464_t2570_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t2570_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t2570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t2570_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t2570_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t2570_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t2570_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t2570_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t2570_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2570_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2570_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t2570)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t2570)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t2570_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2571_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t2571_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t2571_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2412_t2571_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t2571_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t2571_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2571_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2412_t2571_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t2571_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t2571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t2571_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t2571_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2571_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2571_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2571_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2571_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2571_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2571_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2571)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2571)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2571_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t2572_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t2572_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t2572_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24136_t2572_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t2572_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t2572_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t2572_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24136_t2572_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t2572_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t2572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t2572_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t2572_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t2572_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t2572_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t2572_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t2572_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2572_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2572_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t2572)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t2572)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t2572_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
extern TypeInfo U24ArrayTypeU2472_t2573_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2472_t2573_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2472_t2573_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2472_t2573_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2472_t2573_0_0_0;
extern const Il2CppType U24ArrayTypeU2472_t2573_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2472_t2573_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2472_t2573_VTable/* vtableMethods */
	, U24ArrayTypeU2472_t2573_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2472_t2573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$72"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2472_t2573_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2472_t2573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2472_t2573_0_0_0/* byval_arg */
	, &U24ArrayTypeU2472_t2573_1_0_0/* this_arg */
	, &U24ArrayTypeU2472_t2573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2472_t2573_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2573_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2573_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2472_t2573)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2472_t2573)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2472_t2573_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t2574_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24124_t2574_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24124_t2574_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24124_t2574_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t2574_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t2574_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t2574_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24124_t2574_VTable/* vtableMethods */
	, U24ArrayTypeU24124_t2574_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24124_t2574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24124_t2574_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24124_t2574_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t2574_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t2574_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t2574_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t2574_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2574_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2574_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t2574)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t2574)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t2574_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
extern TypeInfo U24ArrayTypeU2496_t2575_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2496_t2575_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2496_t2575_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2496_t2575_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2496_t2575_0_0_0;
extern const Il2CppType U24ArrayTypeU2496_t2575_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2496_t2575_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2496_t2575_VTable/* vtableMethods */
	, U24ArrayTypeU2496_t2575_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2496_t2575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$96"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2496_t2575_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2496_t2575_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2496_t2575_0_0_0/* byval_arg */
	, &U24ArrayTypeU2496_t2575_1_0_0/* this_arg */
	, &U24ArrayTypeU2496_t2575_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2496_t2575_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2575_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2575_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2496_t2575)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2496_t2575)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2496_t2575_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
extern TypeInfo U24ArrayTypeU242048_t2576_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU242048_t2576_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU242048_t2576_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU242048_t2576_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU242048_t2576_0_0_0;
extern const Il2CppType U24ArrayTypeU242048_t2576_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU242048_t2576_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU242048_t2576_VTable/* vtableMethods */
	, U24ArrayTypeU242048_t2576_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU242048_t2576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$2048"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU242048_t2576_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU242048_t2576_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU242048_t2576_0_0_0/* byval_arg */
	, &U24ArrayTypeU242048_t2576_1_0_0/* this_arg */
	, &U24ArrayTypeU242048_t2576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU242048_t2576_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2576_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2576_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU242048_t2576)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU242048_t2576)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU242048_t2576_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t2577_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t2577_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t2577_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24256_t2577_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t2577_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t2577_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t2577_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24256_t2577_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t2577_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t2577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t2577_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t2577_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t2577_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t2577_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t2577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t2577_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2577_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2577_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t2577)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t2577)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t2577_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t2578_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU241024_t2578_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU241024_t2578_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU241024_t2578_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t2578_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t2578_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t2578_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU241024_t2578_VTable/* vtableMethods */
	, U24ArrayTypeU241024_t2578_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU241024_t2578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU241024_t2578_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU241024_t2578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t2578_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t2578_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t2578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t2578_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2578_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2578_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t2578)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t2578)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t2578_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
extern TypeInfo U24ArrayTypeU24640_t2579_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24640_t2579_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24640_t2579_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24640_t2579_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24640_t2579_0_0_0;
extern const Il2CppType U24ArrayTypeU24640_t2579_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24640_t2579_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24640_t2579_VTable/* vtableMethods */
	, U24ArrayTypeU24640_t2579_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24640_t2579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$640"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24640_t2579_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24640_t2579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24640_t2579_0_0_0/* byval_arg */
	, &U24ArrayTypeU24640_t2579_1_0_0/* this_arg */
	, &U24ArrayTypeU24640_t2579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24640_t2579_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2579_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2579_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24640_t2579)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24640_t2579)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24640_t2579_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t2580_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t2580_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t2580_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24128_t2580_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t2580_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t2580_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t2580_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24128_t2580_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t2580_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t2580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t2580_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t2580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t2580_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t2580_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t2580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t2580_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2580_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2580_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t2580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t2580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t2580_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
extern TypeInfo U24ArrayTypeU2452_t2581_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2452_t2581_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2452_t2581_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2452_t2581_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2452_t2581_0_0_0;
extern const Il2CppType U24ArrayTypeU2452_t2581_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2452_t2581_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2452_t2581_VTable/* vtableMethods */
	, U24ArrayTypeU2452_t2581_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2452_t2581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$52"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2452_t2581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2452_t2581_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2452_t2581_0_0_0/* byval_arg */
	, &U24ArrayTypeU2452_t2581_1_0_0/* this_arg */
	, &U24ArrayTypeU2452_t2581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2452_t2581_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2581_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2581_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2452_t2581)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2452_t2581)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2452_t2581_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t2582_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2582_il2cpp_TypeInfo__nestedTypes[20] =
{
	&U24ArrayTypeU2456_t2562_0_0_0,
	&U24ArrayTypeU2424_t2563_0_0_0,
	&U24ArrayTypeU2416_t2564_0_0_0,
	&U24ArrayTypeU24120_t2565_0_0_0,
	&U24ArrayTypeU243132_t2566_0_0_0,
	&U24ArrayTypeU2420_t2567_0_0_0,
	&U24ArrayTypeU2432_t2568_0_0_0,
	&U24ArrayTypeU2448_t2569_0_0_0,
	&U24ArrayTypeU2464_t2570_0_0_0,
	&U24ArrayTypeU2412_t2571_0_0_0,
	&U24ArrayTypeU24136_t2572_0_0_0,
	&U24ArrayTypeU2472_t2573_0_0_0,
	&U24ArrayTypeU24124_t2574_0_0_0,
	&U24ArrayTypeU2496_t2575_0_0_0,
	&U24ArrayTypeU242048_t2576_0_0_0,
	&U24ArrayTypeU24256_t2577_0_0_0,
	&U24ArrayTypeU241024_t2578_0_0_0,
	&U24ArrayTypeU24640_t2579_0_0_0,
	&U24ArrayTypeU24128_t2580_0_0_0,
	&U24ArrayTypeU2452_t2581_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t2582_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t2582_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2582_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2582;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2582_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2582_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2582_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t2582_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2549/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t2582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t2582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 950/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2582_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2582_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2582)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2582)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2582_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 20/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
