﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
struct __StaticArrayInitTypeSizeU3D120_t1013;
struct __StaticArrayInitTypeSizeU3D120_t1013_marshaled;

void __StaticArrayInitTypeSizeU3D120_t1013_marshal(const __StaticArrayInitTypeSizeU3D120_t1013& unmarshaled, __StaticArrayInitTypeSizeU3D120_t1013_marshaled& marshaled);
void __StaticArrayInitTypeSizeU3D120_t1013_marshal_back(const __StaticArrayInitTypeSizeU3D120_t1013_marshaled& marshaled, __StaticArrayInitTypeSizeU3D120_t1013& unmarshaled);
void __StaticArrayInitTypeSizeU3D120_t1013_marshal_cleanup(__StaticArrayInitTypeSizeU3D120_t1013_marshaled& marshaled);
