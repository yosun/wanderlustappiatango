﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t3162;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m15679_gshared (DefaultComparer_t3162 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15679(__this, method) (( void (*) (DefaultComparer_t3162 *, const MethodInfo*))DefaultComparer__ctor_m15679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m15680_gshared (DefaultComparer_t3162 * __this, RaycastResult_t231  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m15680(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3162 *, RaycastResult_t231 , const MethodInfo*))DefaultComparer_GetHashCode_m15680_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m15681_gshared (DefaultComparer_t3162 * __this, RaycastResult_t231  ___x, RaycastResult_t231  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m15681(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3162 *, RaycastResult_t231 , RaycastResult_t231 , const MethodInfo*))DefaultComparer_Equals_m15681_gshared)(__this, ___x, ___y, method)
