﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
struct InternalEnumerator_1_t3457;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t591;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m20016(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3457 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20017(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m20018(__this, method) (( void (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m20019(__this, method) (( bool (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m20020(__this, method) (( SmartTerrainTrackableBehaviour_t591 * (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
