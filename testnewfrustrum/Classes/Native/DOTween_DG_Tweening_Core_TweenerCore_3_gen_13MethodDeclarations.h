﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1055;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23995_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23995(__this, method) (( void (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3__ctor_m23995_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23996_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23996(__this, method) (( void (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Reset_m23996_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23997_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23997(__this, method) (( bool (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Validate_m23997_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23998_gshared (TweenerCore_3_t1055 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23998(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1055 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23998_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23999_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23999(__this, method) (( bool (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Startup_m23999_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m24000_gshared (TweenerCore_3_t1055 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m24000(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1055 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m24000_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
