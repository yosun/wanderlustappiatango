﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t61;

// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m206 (MultiTargetBehaviour_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
