﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t705;
// System.Object
struct Object_t;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t777;
// System.Collections.Generic.IEnumerable`1<Vuforia.ISmartTerrainEventHandler>
struct IEnumerable_1_t4205;
// System.Collections.Generic.IEnumerator`1<Vuforia.ISmartTerrainEventHandler>
struct IEnumerator_1_t4206;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ISmartTerrainEventHandler>
struct ICollection_1_t4207;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ISmartTerrainEventHandler>
struct ReadOnlyCollection_1_t3516;
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t3514;
// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct Predicate_1_t3517;
// System.Comparison`1<Vuforia.ISmartTerrainEventHandler>
struct Comparison_1_t3518;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4547(__this, method) (( void (*) (List_1_t705 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21011(__this, ___collection, method) (( void (*) (List_1_t705 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m21012(__this, ___capacity, method) (( void (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.cctor()
#define List_1__cctor_m21013(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21014(__this, method) (( Object_t* (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21015(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t705 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21016(__this, method) (( Object_t * (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21017(__this, ___item, method) (( int32_t (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21018(__this, ___item, method) (( bool (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21019(__this, ___item, method) (( int32_t (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21020(__this, ___index, ___item, method) (( void (*) (List_1_t705 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21021(__this, ___item, method) (( void (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21022(__this, method) (( bool (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21023(__this, method) (( bool (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21024(__this, method) (( Object_t * (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21025(__this, method) (( bool (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21026(__this, method) (( bool (*) (List_1_t705 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21027(__this, ___index, method) (( Object_t * (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21028(__this, ___index, ___value, method) (( void (*) (List_1_t705 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Add(T)
#define List_1_Add_m21029(__this, ___item, method) (( void (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21030(__this, ___newCount, method) (( void (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21031(__this, ___collection, method) (( void (*) (List_1_t705 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21032(__this, ___enumerable, method) (( void (*) (List_1_t705 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21033(__this, ___collection, method) (( void (*) (List_1_t705 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m21034(__this, method) (( ReadOnlyCollection_1_t3516 * (*) (List_1_t705 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Clear()
#define List_1_Clear_m21035(__this, method) (( void (*) (List_1_t705 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Contains(T)
#define List_1_Contains_m21036(__this, ___item, method) (( bool (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21037(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t705 *, ISmartTerrainEventHandlerU5BU5D_t3514*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m21038(__this, ___match, method) (( Object_t * (*) (List_1_t705 *, Predicate_1_t3517 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21039(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3517 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21040(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t705 *, int32_t, int32_t, Predicate_1_t3517 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4515(__this, method) (( Enumerator_t849  (*) (List_1_t705 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::IndexOf(T)
#define List_1_IndexOf_m21041(__this, ___item, method) (( int32_t (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21042(__this, ___start, ___delta, method) (( void (*) (List_1_t705 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21043(__this, ___index, method) (( void (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m21044(__this, ___index, ___item, method) (( void (*) (List_1_t705 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21045(__this, ___collection, method) (( void (*) (List_1_t705 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Remove(T)
#define List_1_Remove_m21046(__this, ___item, method) (( bool (*) (List_1_t705 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21047(__this, ___match, method) (( int32_t (*) (List_1_t705 *, Predicate_1_t3517 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21048(__this, ___index, method) (( void (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Reverse()
#define List_1_Reverse_m21049(__this, method) (( void (*) (List_1_t705 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Sort()
#define List_1_Sort_m21050(__this, method) (( void (*) (List_1_t705 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21051(__this, ___comparison, method) (( void (*) (List_1_t705 *, Comparison_1_t3518 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::ToArray()
#define List_1_ToArray_m21052(__this, method) (( ISmartTerrainEventHandlerU5BU5D_t3514* (*) (List_1_t705 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::TrimExcess()
#define List_1_TrimExcess_m21053(__this, method) (( void (*) (List_1_t705 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Capacity()
#define List_1_get_Capacity_m21054(__this, method) (( int32_t (*) (List_1_t705 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21055(__this, ___value, method) (( void (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Count()
#define List_1_get_Count_m21056(__this, method) (( int32_t (*) (List_1_t705 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m21057(__this, ___index, method) (( Object_t * (*) (List_1_t705 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m21058(__this, ___index, ___value, method) (( void (*) (List_1_t705 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
