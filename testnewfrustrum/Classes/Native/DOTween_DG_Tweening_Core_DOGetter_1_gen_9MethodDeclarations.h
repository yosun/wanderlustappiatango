﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t1047;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23959_gshared (DOGetter_1_t1047 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23959(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1047 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23959_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C" Vector4_t413  DOGetter_1_Invoke_m23960_gshared (DOGetter_1_t1047 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23960(__this, method) (( Vector4_t413  (*) (DOGetter_1_t1047 *, const MethodInfo*))DOGetter_1_Invoke_m23960_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23961_gshared (DOGetter_1_t1047 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23961(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1047 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23961_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C" Vector4_t413  DOGetter_1_EndInvoke_m23962_gshared (DOGetter_1_t1047 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23962(__this, ___result, method) (( Vector4_t413  (*) (DOGetter_1_t1047 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23962_gshared)(__this, ___result, method)
