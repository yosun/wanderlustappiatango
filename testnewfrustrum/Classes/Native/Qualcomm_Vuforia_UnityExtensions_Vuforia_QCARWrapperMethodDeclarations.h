﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARWrapper
struct QCARWrapper_t703;
// Vuforia.IQCARWrapper
struct IQCARWrapper_t702;

// Vuforia.IQCARWrapper Vuforia.QCARWrapper::get_Instance()
extern "C" Object_t * QCARWrapper_get_Instance_m3947 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::Create()
extern "C" void QCARWrapper_Create_m3948 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::SetImplementation(Vuforia.IQCARWrapper)
extern "C" void QCARWrapper_SetImplementation_m3949 (Object_t * __this /* static, unused */, Object_t * ___implementation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::.cctor()
extern "C" void QCARWrapper__cctor_m3950 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
