﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.LayoutElement
struct LayoutElement_t370;

// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern "C" void LayoutElement__ctor_m1806 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern "C" bool LayoutElement_get_ignoreLayout_m1807 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern "C" void LayoutElement_set_ignoreLayout_m1808 (LayoutElement_t370 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m1809 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern "C" void LayoutElement_CalculateLayoutInputVertical_m1810 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern "C" float LayoutElement_get_minWidth_m1811 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern "C" void LayoutElement_set_minWidth_m1812 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern "C" float LayoutElement_get_minHeight_m1813 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern "C" void LayoutElement_set_minHeight_m1814 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern "C" float LayoutElement_get_preferredWidth_m1815 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern "C" void LayoutElement_set_preferredWidth_m1816 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern "C" float LayoutElement_get_preferredHeight_m1817 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern "C" void LayoutElement_set_preferredHeight_m1818 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern "C" float LayoutElement_get_flexibleWidth_m1819 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern "C" void LayoutElement_set_flexibleWidth_m1820 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern "C" float LayoutElement_get_flexibleHeight_m1821 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern "C" void LayoutElement_set_flexibleHeight_m1822 (LayoutElement_t370 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern "C" int32_t LayoutElement_get_layoutPriority_m1823 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern "C" void LayoutElement_OnEnable_m1824 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern "C" void LayoutElement_OnTransformParentChanged_m1825 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern "C" void LayoutElement_OnDisable_m1826 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m1827 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m1828 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern "C" void LayoutElement_SetDirty_m1829 (LayoutElement_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
