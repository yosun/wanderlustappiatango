﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewear/EyewearCalibrationReading
struct EyewearCalibrationReading_t586;
struct EyewearCalibrationReading_t586_marshaled;

void EyewearCalibrationReading_t586_marshal(const EyewearCalibrationReading_t586& unmarshaled, EyewearCalibrationReading_t586_marshaled& marshaled);
void EyewearCalibrationReading_t586_marshal_back(const EyewearCalibrationReading_t586_marshaled& marshaled, EyewearCalibrationReading_t586& unmarshaled);
void EyewearCalibrationReading_t586_marshal_cleanup(EyewearCalibrationReading_t586_marshaled& marshaled);
