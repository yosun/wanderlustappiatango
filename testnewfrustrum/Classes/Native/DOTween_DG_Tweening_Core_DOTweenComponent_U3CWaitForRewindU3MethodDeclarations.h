﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
struct U3CWaitForRewindU3Ed__2_t937;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::MoveNext()
extern "C" bool U3CWaitForRewindU3Ed__2_MoveNext_m5282 (U3CWaitForRewindU3Ed__2_t937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283 (U3CWaitForRewindU3Ed__2_t937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.IDisposable.Dispose()
extern "C" void U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284 (U3CWaitForRewindU3Ed__2_t937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285 (U3CWaitForRewindU3Ed__2_t937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::.ctor(System.Int32)
extern "C" void U3CWaitForRewindU3Ed__2__ctor_m5286 (U3CWaitForRewindU3Ed__2_t937 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
