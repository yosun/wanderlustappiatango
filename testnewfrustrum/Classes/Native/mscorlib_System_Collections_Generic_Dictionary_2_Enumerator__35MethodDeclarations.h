﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Enumerator_t3833;
// System.Object
struct Object_t;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t1269;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Dictionary_2_t1271;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_37.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__34MethodDeclarations.h"
#define Enumerator__ctor_m25819(__this, ___dictionary, method) (( void (*) (Enumerator_t3833 *, Dictionary_2_t1271 *, const MethodInfo*))Enumerator__ctor_m25717_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25820(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25718_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25821(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25822(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25823(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::MoveNext()
#define Enumerator_MoveNext_m25824(__this, method) (( bool (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_MoveNext_m25722_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Current()
#define Enumerator_get_Current_m25825(__this, method) (( KeyValuePair_2_t3830  (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_Current_m25723_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25826(__this, method) (( uint64_t (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_CurrentKey_m25724_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25827(__this, method) (( NetworkAccessToken_t1269 * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_CurrentValue_m25725_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyState()
#define Enumerator_VerifyState_m25828(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_VerifyState_m25726_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25829(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_VerifyCurrent_m25727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::Dispose()
#define Enumerator_Dispose_m25830(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_Dispose_m25728_gshared)(__this, method)
