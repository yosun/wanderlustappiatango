﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>
struct InternalEnumerator_1_t3443;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19811_gshared (InternalEnumerator_1_t3443 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19811(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3443 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19811_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19812_gshared (InternalEnumerator_1_t3443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19812(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3443 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19812_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19813_gshared (InternalEnumerator_1_t3443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19813(__this, method) (( void (*) (InternalEnumerator_1_t3443 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19813_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19814_gshared (InternalEnumerator_1_t3443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19814(__this, method) (( bool (*) (InternalEnumerator_1_t3443 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19814_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::get_Current()
extern "C" SmartTerrainRevisionData_t650  InternalEnumerator_1_get_Current_m19815_gshared (InternalEnumerator_1_t3443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19815(__this, method) (( SmartTerrainRevisionData_t650  (*) (InternalEnumerator_1_t3443 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19815_gshared)(__this, method)
