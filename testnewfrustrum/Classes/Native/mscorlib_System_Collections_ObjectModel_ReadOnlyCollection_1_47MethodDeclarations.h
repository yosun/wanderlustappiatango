﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>
struct ReadOnlyCollection_1_t3665;
// DG.Tweening.TweenCallback
struct TweenCallback_t101;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<DG.Tweening.TweenCallback>
struct IList_1_t3664;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t3663;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.TweenCallback>
struct IEnumerator_1_t4284;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m23370(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3665 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15055_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23371(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3665 *, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15056_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23372(__this, method) (( void (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15057_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23373(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15058_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23374(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15059_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23375(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15060_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23376(__this, ___index, method) (( TweenCallback_t101 * (*) (ReadOnlyCollection_1_t3665 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15061_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23377(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15062_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23378(__this, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15063_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23379(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3665 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15064_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23380(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15065_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23381(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3665 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15066_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23382(__this, method) (( void (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15067_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23383(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15068_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23384(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3665 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15069_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23385(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15070_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23386(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3665 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15071_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23387(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15072_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23388(__this, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15073_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23389(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15074_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23390(__this, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15075_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23391(__this, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15076_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23392(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3665 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15077_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23393(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3665 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15078_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::Contains(T)
#define ReadOnlyCollection_1_Contains_m23394(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3665 *, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15079_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m23395(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3665 *, TweenCallbackU5BU5D_t3663*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15080_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m23396(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15081_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m23397(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3665 *, TweenCallback_t101 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15082_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::get_Count()
#define ReadOnlyCollection_1_get_Count_m23398(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3665 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15083_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m23399(__this, ___index, method) (( TweenCallback_t101 * (*) (ReadOnlyCollection_1_t3665 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15084_gshared)(__this, ___index, method)
