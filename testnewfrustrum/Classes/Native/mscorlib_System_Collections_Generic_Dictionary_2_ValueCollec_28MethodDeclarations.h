﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t3205;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3196;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16289_gshared (Enumerator_t3205 * __this, Dictionary_2_t3196 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16289(__this, ___host, method) (( void (*) (Enumerator_t3205 *, Dictionary_2_t3196 *, const MethodInfo*))Enumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared (Enumerator_t3205 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16290(__this, method) (( Object_t * (*) (Enumerator_t3205 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16291_gshared (Enumerator_t3205 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16291(__this, method) (( void (*) (Enumerator_t3205 *, const MethodInfo*))Enumerator_Dispose_m16291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16292_gshared (Enumerator_t3205 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16292(__this, method) (( bool (*) (Enumerator_t3205 *, const MethodInfo*))Enumerator_MoveNext_m16292_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16293_gshared (Enumerator_t3205 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16293(__this, method) (( Object_t * (*) (Enumerator_t3205 *, const MethodInfo*))Enumerator_get_Current_m16293_gshared)(__this, method)
