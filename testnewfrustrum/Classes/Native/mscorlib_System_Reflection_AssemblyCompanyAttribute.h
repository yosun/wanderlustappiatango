﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t482  : public Attribute_t138
{
	// System.String System.Reflection.AssemblyCompanyAttribute::name
	String_t* ___name_0;
};
