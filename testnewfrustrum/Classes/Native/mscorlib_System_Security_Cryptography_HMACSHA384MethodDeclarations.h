﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t2403;
// System.Byte[]
struct ByteU5BU5D_t616;

// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
extern "C" void HMACSHA384__ctor_m12577 (HMACSHA384_t2403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
extern "C" void HMACSHA384__ctor_m12578 (HMACSHA384_t2403 * __this, ByteU5BU5D_t616* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
extern "C" void HMACSHA384__cctor_m12579 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m12580 (HMACSHA384_t2403 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
