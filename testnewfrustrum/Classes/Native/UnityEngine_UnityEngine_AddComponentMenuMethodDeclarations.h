﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t491;
// System.String
struct String_t;

// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m2455 (AddComponentMenu_t491 * __this, String_t* ___menuName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m2488 (AddComponentMenu_t491 * __this, String_t* ___menuName, int32_t ___order, const MethodInfo* method) IL2CPP_METHOD_ATTR;
