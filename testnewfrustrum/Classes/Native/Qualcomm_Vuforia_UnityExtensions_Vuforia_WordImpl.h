﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Vuforia.Image
struct Image_t615;
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t684;
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Ima.h"
// Vuforia.WordImpl
struct  WordImpl_t685  : public TrackableImpl_t577
{
	// System.String Vuforia.WordImpl::mText
	String_t* ___mText_2;
	// UnityEngine.Vector2 Vuforia.WordImpl::mSize
	Vector2_t10  ___mSize_3;
	// Vuforia.Image Vuforia.WordImpl::mLetterMask
	Image_t615 * ___mLetterMask_4;
	// Vuforia.QCARManagerImpl/ImageHeaderData Vuforia.WordImpl::mLetterImageHeader
	ImageHeaderData_t648  ___mLetterImageHeader_5;
	// Vuforia.RectangleData[] Vuforia.WordImpl::mLetterBoundingBoxes
	RectangleDataU5BU5D_t684* ___mLetterBoundingBoxes_6;
};
