﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t3122;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C" void Comparer_1__ctor_m15138_gshared (Comparer_1_t3122 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m15138(__this, method) (( void (*) (Comparer_1_t3122 *, const MethodInfo*))Comparer_1__ctor_m15138_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern "C" void Comparer_1__cctor_m15139_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m15139(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m15139_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m15140_gshared (Comparer_1_t3122 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m15140(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3122 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m15140_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C" Comparer_1_t3122 * Comparer_1_get_Default_m15141_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m15141(__this /* static, unused */, method) (( Comparer_1_t3122 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m15141_gshared)(__this /* static, unused */, method)
