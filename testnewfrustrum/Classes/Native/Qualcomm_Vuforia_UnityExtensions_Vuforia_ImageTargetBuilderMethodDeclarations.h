﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t607;
// System.String
struct String_t;
// Vuforia.TrackableSource
struct TrackableSource_t619;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Boolean Vuforia.ImageTargetBuilder::Build(System.String,System.Single)
// System.Void Vuforia.ImageTargetBuilder::StartScan()
// System.Void Vuforia.ImageTargetBuilder::StopScan()
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilder::GetFrameQuality()
// Vuforia.TrackableSource Vuforia.ImageTargetBuilder::GetTrackableSource()
// System.Void Vuforia.ImageTargetBuilder::.ctor()
extern "C" void ImageTargetBuilder__ctor_m2860 (ImageTargetBuilder_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
