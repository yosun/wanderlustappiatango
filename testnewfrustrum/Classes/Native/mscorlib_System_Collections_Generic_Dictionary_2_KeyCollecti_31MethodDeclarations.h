﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Enumerator_t3569;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22016_gshared (Enumerator_t3569 * __this, Dictionary_2_t870 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22016(__this, ___host, method) (( void (*) (Enumerator_t3569 *, Dictionary_2_t870 *, const MethodInfo*))Enumerator__ctor_m22016_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22017_gshared (Enumerator_t3569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22017(__this, method) (( Object_t * (*) (Enumerator_t3569 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22017_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m22018_gshared (Enumerator_t3569 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22018(__this, method) (( void (*) (Enumerator_t3569 *, const MethodInfo*))Enumerator_Dispose_m22018_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22019_gshared (Enumerator_t3569 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22019(__this, method) (( bool (*) (Enumerator_t3569 *, const MethodInfo*))Enumerator_MoveNext_m22019_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22020_gshared (Enumerator_t3569 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22020(__this, method) (( int32_t (*) (Enumerator_t3569 *, const MethodInfo*))Enumerator_get_Current_m22020_gshared)(__this, method)
