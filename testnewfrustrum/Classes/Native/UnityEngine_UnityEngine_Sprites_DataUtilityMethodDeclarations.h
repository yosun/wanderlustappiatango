﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Sprites.DataUtility
struct DataUtility_t1200;
// UnityEngine.Sprite
struct Sprite_t292;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C" Vector4_t413  DataUtility_GetInnerUV_m2163 (Object_t * __this /* static, unused */, Sprite_t292 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C" Vector4_t413  DataUtility_GetOuterUV_m2162 (Object_t * __this /* static, unused */, Sprite_t292 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C" Vector4_t413  DataUtility_GetPadding_m2151 (Object_t * __this /* static, unused */, Sprite_t292 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C" Vector2_t10  DataUtility_GetMinSize_m2172 (Object_t * __this /* static, unused */, Sprite_t292 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C" void DataUtility_Internal_GetMinSize_m6166 (Object_t * __this /* static, unused */, Sprite_t292 * ___sprite, Vector2_t10 * ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
