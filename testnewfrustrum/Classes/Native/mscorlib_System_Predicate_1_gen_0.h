﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t351;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct  Predicate_1_t353  : public MulticastDelegate_t307
{
};
