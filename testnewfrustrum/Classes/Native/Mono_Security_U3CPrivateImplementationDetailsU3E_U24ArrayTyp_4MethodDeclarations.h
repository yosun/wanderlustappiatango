﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct U24ArrayTypeU2464_t1795;
struct U24ArrayTypeU2464_t1795_marshaled;

void U24ArrayTypeU2464_t1795_marshal(const U24ArrayTypeU2464_t1795& unmarshaled, U24ArrayTypeU2464_t1795_marshaled& marshaled);
void U24ArrayTypeU2464_t1795_marshal_back(const U24ArrayTypeU2464_t1795_marshaled& marshaled, U24ArrayTypeU2464_t1795& unmarshaled);
void U24ArrayTypeU2464_t1795_marshal_cleanup(U24ArrayTypeU2464_t1795_marshaled& marshaled);
