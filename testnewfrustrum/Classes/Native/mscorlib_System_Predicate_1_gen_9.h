﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t230;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
struct  Predicate_1_t3169  : public MulticastDelegate_t307
{
};
