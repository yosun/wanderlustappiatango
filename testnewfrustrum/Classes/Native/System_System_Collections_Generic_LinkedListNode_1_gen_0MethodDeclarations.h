﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3926;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3927;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m26990_gshared (LinkedListNode_1_t3926 * __this, LinkedList_1_t3927 * ___list, Object_t * ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m26990(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t3926 *, LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedListNode_1__ctor_m26990_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m26991_gshared (LinkedListNode_1_t3926 * __this, LinkedList_1_t3927 * ___list, Object_t * ___value, LinkedListNode_1_t3926 * ___previousNode, LinkedListNode_1_t3926 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m26991(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t3926 *, LinkedList_1_t3927 *, Object_t *, LinkedListNode_1_t3926 *, LinkedListNode_1_t3926 *, const MethodInfo*))LinkedListNode_1__ctor_m26991_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C" void LinkedListNode_1_Detach_m26992_gshared (LinkedListNode_1_t3926 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m26992(__this, method) (( void (*) (LinkedListNode_1_t3926 *, const MethodInfo*))LinkedListNode_1_Detach_m26992_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C" LinkedList_1_t3927 * LinkedListNode_1_get_List_m26993_gshared (LinkedListNode_1_t3926 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m26993(__this, method) (( LinkedList_1_t3927 * (*) (LinkedListNode_1_t3926 *, const MethodInfo*))LinkedListNode_1_get_List_m26993_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C" LinkedListNode_1_t3926 * LinkedListNode_1_get_Next_m26994_gshared (LinkedListNode_1_t3926 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m26994(__this, method) (( LinkedListNode_1_t3926 * (*) (LinkedListNode_1_t3926 *, const MethodInfo*))LinkedListNode_1_get_Next_m26994_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C" Object_t * LinkedListNode_1_get_Value_m26995_gshared (LinkedListNode_1_t3926 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m26995(__this, method) (( Object_t * (*) (LinkedListNode_1_t3926 *, const MethodInfo*))LinkedListNode_1_get_Value_m26995_gshared)(__this, method)
