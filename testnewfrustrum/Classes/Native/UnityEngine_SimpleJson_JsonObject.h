﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t1276;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.JsonObject
struct  JsonObject_t1277  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> SimpleJson.JsonObject::_members
	Dictionary_2_t1276 * ____members_0;
};
