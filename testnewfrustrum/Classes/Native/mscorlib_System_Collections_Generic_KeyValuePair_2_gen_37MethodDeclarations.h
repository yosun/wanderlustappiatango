﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct KeyValuePair_2_t3830;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t1269;
// System.String
struct String_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_36MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25785(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3830 *, uint64_t, NetworkAccessToken_t1269 *, const MethodInfo*))KeyValuePair_2__ctor_m25687_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Key()
#define KeyValuePair_2_get_Key_m25786(__this, method) (( uint64_t (*) (KeyValuePair_2_t3830 *, const MethodInfo*))KeyValuePair_2_get_Key_m25688_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25787(__this, ___value, method) (( void (*) (KeyValuePair_2_t3830 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m25689_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Value()
#define KeyValuePair_2_get_Value_m25788(__this, method) (( NetworkAccessToken_t1269 * (*) (KeyValuePair_2_t3830 *, const MethodInfo*))KeyValuePair_2_get_Value_m25690_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25789(__this, ___value, method) (( void (*) (KeyValuePair_2_t3830 *, NetworkAccessToken_t1269 *, const MethodInfo*))KeyValuePair_2_set_Value_m25691_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::ToString()
#define KeyValuePair_2_ToString_m25790(__this, method) (( String_t* (*) (KeyValuePair_2_t3830 *, const MethodInfo*))KeyValuePair_2_ToString_m25692_gshared)(__this, method)
