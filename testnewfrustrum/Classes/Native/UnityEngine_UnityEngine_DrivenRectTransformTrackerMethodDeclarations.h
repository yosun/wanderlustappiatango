﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t329;
// UnityEngine.Object
struct Object_t123;
struct Object_t123_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t272;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m2296 (DrivenRectTransformTracker_t329 * __this, Object_t123 * ___driver, RectTransform_t272 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m2294 (DrivenRectTransformTracker_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
