﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t235;
// UnityEngine.GameObject
struct GameObject_t2;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_t3189 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::l
	List_1_t235 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::current
	GameObject_t2 * ___current_3;
};
