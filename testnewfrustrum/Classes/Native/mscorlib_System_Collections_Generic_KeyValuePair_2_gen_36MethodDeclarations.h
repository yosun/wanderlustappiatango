﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
struct KeyValuePair_2_t3815;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m25687_gshared (KeyValuePair_2_t3815 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m25687(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3815 *, uint64_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m25687_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Key()
extern "C" uint64_t KeyValuePair_2_get_Key_m25688_gshared (KeyValuePair_2_t3815 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m25688(__this, method) (( uint64_t (*) (KeyValuePair_2_t3815 *, const MethodInfo*))KeyValuePair_2_get_Key_m25688_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m25689_gshared (KeyValuePair_2_t3815 * __this, uint64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m25689(__this, ___value, method) (( void (*) (KeyValuePair_2_t3815 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m25689_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m25690_gshared (KeyValuePair_2_t3815 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m25690(__this, method) (( Object_t * (*) (KeyValuePair_2_t3815 *, const MethodInfo*))KeyValuePair_2_get_Value_m25690_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m25691_gshared (KeyValuePair_2_t3815 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m25691(__this, ___value, method) (( void (*) (KeyValuePair_2_t3815 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m25691_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m25692_gshared (KeyValuePair_2_t3815 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m25692(__this, method) (( String_t* (*) (KeyValuePair_2_t3815 *, const MethodInfo*))KeyValuePair_2_ToString_m25692_gshared)(__this, method)
