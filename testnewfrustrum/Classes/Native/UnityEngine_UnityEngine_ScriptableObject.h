﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct  ScriptableObject_t955  : public Object_t123
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t955_marshaled
{
};
