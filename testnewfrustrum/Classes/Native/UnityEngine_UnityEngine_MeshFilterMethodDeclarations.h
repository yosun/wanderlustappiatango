﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshFilter
struct MeshFilter_t149;
// UnityEngine.Mesh
struct Mesh_t153;

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t153 * MeshFilter_get_mesh_m4283 (MeshFilter_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m4596 (MeshFilter_t149 * __this, Mesh_t153 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t153 * MeshFilter_get_sharedMesh_m486 (MeshFilter_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m4337 (MeshFilter_t149 * __this, Mesh_t153 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
