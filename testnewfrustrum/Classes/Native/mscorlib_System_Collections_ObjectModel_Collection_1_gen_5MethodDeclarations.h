﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t3758;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1364;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t4329;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t461;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m24945_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1__ctor_m24945(__this, method) (( void (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1__ctor_m24945_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24946_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24946(__this, method) (( bool (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24946_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24947_gshared (Collection_1_t3758 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m24947(__this, ___array, ___index, method) (( void (*) (Collection_1_t3758 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m24947_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m24948_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m24948(__this, method) (( Object_t * (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m24948_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m24949_gshared (Collection_1_t3758 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m24949(__this, ___value, method) (( int32_t (*) (Collection_1_t3758 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m24949_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m24950_gshared (Collection_1_t3758 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m24950(__this, ___value, method) (( bool (*) (Collection_1_t3758 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m24950_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m24951_gshared (Collection_1_t3758 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m24951(__this, ___value, method) (( int32_t (*) (Collection_1_t3758 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m24951_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m24952_gshared (Collection_1_t3758 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m24952(__this, ___index, ___value, method) (( void (*) (Collection_1_t3758 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m24952_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m24953_gshared (Collection_1_t3758 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m24953(__this, ___value, method) (( void (*) (Collection_1_t3758 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m24953_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m24954_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m24954(__this, method) (( bool (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m24954_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m24955_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m24955(__this, method) (( Object_t * (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m24955_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m24956_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m24956(__this, method) (( bool (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m24956_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m24957_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m24957(__this, method) (( bool (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m24957_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m24958_gshared (Collection_1_t3758 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m24958(__this, ___index, method) (( Object_t * (*) (Collection_1_t3758 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m24958_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m24959_gshared (Collection_1_t3758 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m24959(__this, ___index, ___value, method) (( void (*) (Collection_1_t3758 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m24959_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m24960_gshared (Collection_1_t3758 * __this, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_Add_m24960(__this, ___item, method) (( void (*) (Collection_1_t3758 *, UICharInfo_t460 , const MethodInfo*))Collection_1_Add_m24960_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m24961_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_Clear_m24961(__this, method) (( void (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_Clear_m24961_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m24962_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m24962(__this, method) (( void (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_ClearItems_m24962_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m24963_gshared (Collection_1_t3758 * __this, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_Contains_m24963(__this, ___item, method) (( bool (*) (Collection_1_t3758 *, UICharInfo_t460 , const MethodInfo*))Collection_1_Contains_m24963_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m24964_gshared (Collection_1_t3758 * __this, UICharInfoU5BU5D_t1364* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m24964(__this, ___array, ___index, method) (( void (*) (Collection_1_t3758 *, UICharInfoU5BU5D_t1364*, int32_t, const MethodInfo*))Collection_1_CopyTo_m24964_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m24965_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m24965(__this, method) (( Object_t* (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_GetEnumerator_m24965_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m24966_gshared (Collection_1_t3758 * __this, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m24966(__this, ___item, method) (( int32_t (*) (Collection_1_t3758 *, UICharInfo_t460 , const MethodInfo*))Collection_1_IndexOf_m24966_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m24967_gshared (Collection_1_t3758 * __this, int32_t ___index, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_Insert_m24967(__this, ___index, ___item, method) (( void (*) (Collection_1_t3758 *, int32_t, UICharInfo_t460 , const MethodInfo*))Collection_1_Insert_m24967_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m24968_gshared (Collection_1_t3758 * __this, int32_t ___index, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m24968(__this, ___index, ___item, method) (( void (*) (Collection_1_t3758 *, int32_t, UICharInfo_t460 , const MethodInfo*))Collection_1_InsertItem_m24968_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m24969_gshared (Collection_1_t3758 * __this, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_Remove_m24969(__this, ___item, method) (( bool (*) (Collection_1_t3758 *, UICharInfo_t460 , const MethodInfo*))Collection_1_Remove_m24969_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m24970_gshared (Collection_1_t3758 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m24970(__this, ___index, method) (( void (*) (Collection_1_t3758 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m24970_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m24971_gshared (Collection_1_t3758 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m24971(__this, ___index, method) (( void (*) (Collection_1_t3758 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m24971_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m24972_gshared (Collection_1_t3758 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m24972(__this, method) (( int32_t (*) (Collection_1_t3758 *, const MethodInfo*))Collection_1_get_Count_m24972_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t460  Collection_1_get_Item_m24973_gshared (Collection_1_t3758 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m24973(__this, ___index, method) (( UICharInfo_t460  (*) (Collection_1_t3758 *, int32_t, const MethodInfo*))Collection_1_get_Item_m24973_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m24974_gshared (Collection_1_t3758 * __this, int32_t ___index, UICharInfo_t460  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m24974(__this, ___index, ___value, method) (( void (*) (Collection_1_t3758 *, int32_t, UICharInfo_t460 , const MethodInfo*))Collection_1_set_Item_m24974_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m24975_gshared (Collection_1_t3758 * __this, int32_t ___index, UICharInfo_t460  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m24975(__this, ___index, ___item, method) (( void (*) (Collection_1_t3758 *, int32_t, UICharInfo_t460 , const MethodInfo*))Collection_1_SetItem_m24975_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m24976_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m24976(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m24976_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t460  Collection_1_ConvertItem_m24977_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m24977(__this /* static, unused */, ___item, method) (( UICharInfo_t460  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m24977_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m24978_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m24978(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m24978_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m24979_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m24979(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m24979_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m24980_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m24980(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m24980_gshared)(__this /* static, unused */, ___list, method)
