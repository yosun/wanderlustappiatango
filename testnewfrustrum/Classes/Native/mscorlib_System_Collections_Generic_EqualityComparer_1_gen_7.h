﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
struct EqualityComparer_1_t3616;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
struct  EqualityComparer_1_t3616  : public Object_t
{
};
struct EqualityComparer_1_t3616_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::_default
	EqualityComparer_1_t3616 * ____default_0;
};
