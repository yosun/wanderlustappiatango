﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t625;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t594;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t765;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>
struct IEnumerator_1_t808;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.DataSet>
struct ICollection_1_t4163;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>
struct ReadOnlyCollection_1_t3425;
// Vuforia.DataSet[]
struct DataSetU5BU5D_t3423;
// System.Predicate`1<Vuforia.DataSet>
struct Predicate_1_t3426;
// System.Comparison`1<Vuforia.DataSet>
struct Comparison_1_t3428;
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_46.h"

// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4385(__this, method) (( void (*) (List_1_t625 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19475(__this, ___collection, method) (( void (*) (List_1_t625 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor(System.Int32)
#define List_1__ctor_m19476(__this, ___capacity, method) (( void (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.cctor()
#define List_1__cctor_m19477(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19478(__this, method) (( Object_t* (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19479(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t625 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19480(__this, method) (( Object_t * (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19481(__this, ___item, method) (( int32_t (*) (List_1_t625 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19482(__this, ___item, method) (( bool (*) (List_1_t625 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19483(__this, ___item, method) (( int32_t (*) (List_1_t625 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19484(__this, ___index, ___item, method) (( void (*) (List_1_t625 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19485(__this, ___item, method) (( void (*) (List_1_t625 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19486(__this, method) (( bool (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19487(__this, method) (( bool (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19488(__this, method) (( Object_t * (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19489(__this, method) (( bool (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19490(__this, method) (( bool (*) (List_1_t625 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19491(__this, ___index, method) (( Object_t * (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19492(__this, ___index, ___value, method) (( void (*) (List_1_t625 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Add(T)
#define List_1_Add_m19493(__this, ___item, method) (( void (*) (List_1_t625 *, DataSet_t594 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19494(__this, ___newCount, method) (( void (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19495(__this, ___collection, method) (( void (*) (List_1_t625 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19496(__this, ___enumerable, method) (( void (*) (List_1_t625 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19497(__this, ___collection, method) (( void (*) (List_1_t625 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.DataSet>::AsReadOnly()
#define List_1_AsReadOnly_m19498(__this, method) (( ReadOnlyCollection_1_t3425 * (*) (List_1_t625 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Clear()
#define List_1_Clear_m19499(__this, method) (( void (*) (List_1_t625 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Contains(T)
#define List_1_Contains_m19500(__this, ___item, method) (( bool (*) (List_1_t625 *, DataSet_t594 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19501(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t625 *, DataSetU5BU5D_t3423*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.DataSet>::Find(System.Predicate`1<T>)
#define List_1_Find_m19502(__this, ___match, method) (( DataSet_t594 * (*) (List_1_t625 *, Predicate_1_t3426 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19503(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3426 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19504(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t625 *, int32_t, int32_t, Predicate_1_t3426 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.DataSet>::GetEnumerator()
#define List_1_GetEnumerator_m19505(__this, method) (( Enumerator_t3427  (*) (List_1_t625 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::IndexOf(T)
#define List_1_IndexOf_m19506(__this, ___item, method) (( int32_t (*) (List_1_t625 *, DataSet_t594 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19507(__this, ___start, ___delta, method) (( void (*) (List_1_t625 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19508(__this, ___index, method) (( void (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Insert(System.Int32,T)
#define List_1_Insert_m19509(__this, ___index, ___item, method) (( void (*) (List_1_t625 *, int32_t, DataSet_t594 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19510(__this, ___collection, method) (( void (*) (List_1_t625 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Remove(T)
#define List_1_Remove_m19511(__this, ___item, method) (( bool (*) (List_1_t625 *, DataSet_t594 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19512(__this, ___match, method) (( int32_t (*) (List_1_t625 *, Predicate_1_t3426 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19513(__this, ___index, method) (( void (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Reverse()
#define List_1_Reverse_m19514(__this, method) (( void (*) (List_1_t625 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Sort()
#define List_1_Sort_m19515(__this, method) (( void (*) (List_1_t625 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19516(__this, ___comparison, method) (( void (*) (List_1_t625 *, Comparison_1_t3428 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.DataSet>::ToArray()
#define List_1_ToArray_m19517(__this, method) (( DataSetU5BU5D_t3423* (*) (List_1_t625 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::TrimExcess()
#define List_1_TrimExcess_m19518(__this, method) (( void (*) (List_1_t625 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Capacity()
#define List_1_get_Capacity_m19519(__this, method) (( int32_t (*) (List_1_t625 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19520(__this, ___value, method) (( void (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Count()
#define List_1_get_Count_m19521(__this, method) (( int32_t (*) (List_1_t625 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.DataSet>::get_Item(System.Int32)
#define List_1_get_Item_m19522(__this, ___index, method) (( DataSet_t594 * (*) (List_1_t625 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::set_Item(System.Int32,T)
#define List_1_set_Item_m19523(__this, ___index, ___value, method) (( void (*) (List_1_t625 *, int32_t, DataSet_t594 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
