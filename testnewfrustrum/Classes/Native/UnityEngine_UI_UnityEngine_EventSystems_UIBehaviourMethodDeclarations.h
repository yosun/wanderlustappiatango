﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t199;

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
extern "C" void UIBehaviour__ctor_m851 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
extern "C" void UIBehaviour_Awake_m852 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
extern "C" void UIBehaviour_OnEnable_m853 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
extern "C" void UIBehaviour_Start_m854 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
extern "C" void UIBehaviour_OnDisable_m855 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
extern "C" void UIBehaviour_OnDestroy_m856 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
extern "C" bool UIBehaviour_IsActive_m857 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m858 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m859 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
extern "C" void UIBehaviour_OnTransformParentChanged_m860 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m861 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
extern "C" void UIBehaviour_OnCanvasGroupChanged_m862 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasHierarchyChanged()
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m863 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
extern "C" bool UIBehaviour_IsDestroyed_m864 (UIBehaviour_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
