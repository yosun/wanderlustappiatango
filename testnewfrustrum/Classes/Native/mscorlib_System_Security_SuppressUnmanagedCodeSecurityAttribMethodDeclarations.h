﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t2443;

// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m12865 (SuppressUnmanagedCodeSecurityAttribute_t2443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
