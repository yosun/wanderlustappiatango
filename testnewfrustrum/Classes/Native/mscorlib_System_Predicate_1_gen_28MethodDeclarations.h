﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3399;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Int32>
#include "mscorlib_System_Predicate_1_gen_27MethodDeclarations.h"
#define Predicate_1__ctor_m19070(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3399 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m19024_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
#define Predicate_1_Invoke_m19071(__this, ___obj, method) (( bool (*) (Predicate_1_t3399 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m19025_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m19072(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3399 *, int32_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m19026_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m19073(__this, ___result, method) (( bool (*) (Predicate_1_t3399 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m19027_gshared)(__this, ___result, method)
