﻿#pragma once
#include <stdint.h>
// Vuforia.WordList
struct WordList_t678;
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TextTrackerImpl
struct  TextTrackerImpl_t679  : public TextTracker_t676
{
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t678 * ___mWordList_1;
};
