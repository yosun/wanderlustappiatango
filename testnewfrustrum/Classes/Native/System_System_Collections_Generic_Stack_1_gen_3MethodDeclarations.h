﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t3268;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t4090;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t315;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m17132(__this, method) (( void (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1__ctor_m15420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m17133(__this, method) (( bool (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15421_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m17134(__this, method) (( Object_t * (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15422_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m17135(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3268 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15423_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17136(__this, method) (( Object_t* (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15424_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m17137(__this, method) (( Object_t * (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15425_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m17138(__this, method) (( List_1_t315 * (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_Peek_m15426_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m17139(__this, method) (( List_1_t315 * (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_Pop_m15427_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m17140(__this, ___t, method) (( void (*) (Stack_1_t3268 *, List_1_t315 *, const MethodInfo*))Stack_1_Push_m15428_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m17141(__this, method) (( int32_t (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_get_Count_m15429_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m17142(__this, method) (( Enumerator_t4091  (*) (Stack_1_t3268 *, const MethodInfo*))Stack_1_GetEnumerator_m15430_gshared)(__this, method)
