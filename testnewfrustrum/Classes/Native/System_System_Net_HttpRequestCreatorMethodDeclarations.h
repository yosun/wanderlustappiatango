﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1874;
// System.Net.WebRequest
struct WebRequest_t1833;
// System.Uri
struct Uri_t1273;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m8535 (HttpRequestCreator_t1874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1833 * HttpRequestCreator_Create_m8536 (HttpRequestCreator_t1874 * __this, Uri_t1273 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
