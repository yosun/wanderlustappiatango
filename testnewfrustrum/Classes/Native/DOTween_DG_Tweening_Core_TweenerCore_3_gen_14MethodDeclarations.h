﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t3111;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m14944_gshared (TweenerCore_3_t3111 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m14944(__this, method) (( void (*) (TweenerCore_3_t3111 *, const MethodInfo*))TweenerCore_3__ctor_m14944_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m14946_gshared (TweenerCore_3_t3111 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m14946(__this, method) (( void (*) (TweenerCore_3_t3111 *, const MethodInfo*))TweenerCore_3_Reset_m14946_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m14948_gshared (TweenerCore_3_t3111 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m14948(__this, method) (( bool (*) (TweenerCore_3_t3111 *, const MethodInfo*))TweenerCore_3_Validate_m14948_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m14950_gshared (TweenerCore_3_t3111 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m14950(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t3111 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m14950_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m14952_gshared (TweenerCore_3_t3111 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m14952(__this, method) (( bool (*) (TweenerCore_3_t3111 *, const MethodInfo*))TweenerCore_3_Startup_m14952_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m14954_gshared (TweenerCore_3_t3111 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m14954(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t3111 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m14954_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
