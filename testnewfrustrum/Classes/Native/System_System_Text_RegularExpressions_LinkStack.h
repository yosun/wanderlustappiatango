﻿#pragma once
#include <stdint.h>
// System.Collections.Stack
struct Stack_t1353;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t1946  : public LinkRef_t1942
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t1353 * ___stack_0;
};
