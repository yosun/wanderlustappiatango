﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t1038;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23679_gshared (DOGetter_1_t1038 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23679(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1038 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23679_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C" uint64_t DOGetter_1_Invoke_m23680_gshared (DOGetter_1_t1038 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23680(__this, method) (( uint64_t (*) (DOGetter_1_t1038 *, const MethodInfo*))DOGetter_1_Invoke_m23680_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23681_gshared (DOGetter_1_t1038 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23681(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1038 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23681_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C" uint64_t DOGetter_1_EndInvoke_m23682_gshared (DOGetter_1_t1038 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23682(__this, ___result, method) (( uint64_t (*) (DOGetter_1_t1038 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23682_gshared)(__this, ___result, method)
