﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction
struct UnityAction_t275;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1340  : public BaseInvokableCall_t1339
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t275 * ___Delegate_0;
};
