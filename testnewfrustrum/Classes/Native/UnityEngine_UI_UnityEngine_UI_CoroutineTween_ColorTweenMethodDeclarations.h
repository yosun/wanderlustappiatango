﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t253;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t409;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"

// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_startColor()
extern "C" Color_t90  ColorTween_get_startColor_m1026 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_startColor(UnityEngine.Color)
extern "C" void ColorTween_set_startColor_m1027 (ColorTween_t253 * __this, Color_t90  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_targetColor()
extern "C" Color_t90  ColorTween_get_targetColor_m1028 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_targetColor(UnityEngine.Color)
extern "C" void ColorTween_set_targetColor_m1029 (ColorTween_t253 * __this, Color_t90  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::get_tweenMode()
extern "C" int32_t ColorTween_get_tweenMode_m1030 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_tweenMode(UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode)
extern "C" void ColorTween_set_tweenMode_m1031 (ColorTween_t253 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C" float ColorTween_get_duration_m1032 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_duration(System.Single)
extern "C" void ColorTween_set_duration_m1033 (ColorTween_t253 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C" bool ColorTween_get_ignoreTimeScale_m1034 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_ignoreTimeScale(System.Boolean)
extern "C" void ColorTween_set_ignoreTimeScale_m1035 (ColorTween_t253 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C" void ColorTween_TweenValue_m1036 (ColorTween_t253 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Color>)
extern "C" void ColorTween_AddOnChangedCallback_m1037 (ColorTween_t253 * __this, UnityAction_1_t409 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::GetIgnoreTimescale()
extern "C" bool ColorTween_GetIgnoreTimescale_m1038 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::GetDuration()
extern "C" float ColorTween_GetDuration_m1039 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C" bool ColorTween_ValidTarget_m1040 (ColorTween_t253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
