﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Enumerator_t3573;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22051_gshared (Enumerator_t3573 * __this, Dictionary_2_t870 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22051(__this, ___host, method) (( void (*) (Enumerator_t3573 *, Dictionary_2_t870 *, const MethodInfo*))Enumerator__ctor_m22051_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22052_gshared (Enumerator_t3573 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22052(__this, method) (( Object_t * (*) (Enumerator_t3573 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22052_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m22053_gshared (Enumerator_t3573 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22053(__this, method) (( void (*) (Enumerator_t3573 *, const MethodInfo*))Enumerator_Dispose_m22053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22054_gshared (Enumerator_t3573 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22054(__this, method) (( bool (*) (Enumerator_t3573 *, const MethodInfo*))Enumerator_MoveNext_m22054_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t643  Enumerator_get_Current_m22055_gshared (Enumerator_t3573 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22055(__this, method) (( VirtualButtonData_t643  (*) (Enumerator_t3573 *, const MethodInfo*))Enumerator_get_Current_m22055_gshared)(__this, method)
