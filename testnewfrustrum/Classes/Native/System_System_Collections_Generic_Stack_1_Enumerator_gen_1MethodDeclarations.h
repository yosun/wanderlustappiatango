﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Type>
struct Enumerator_t3884;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1430;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m26589(__this, ___t, method) (( void (*) (Enumerator_t3884 *, Stack_1_t1430 *, const MethodInfo*))Enumerator__ctor_m15431_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26590(__this, method) (( Object_t * (*) (Enumerator_t3884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15432_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m26591(__this, method) (( void (*) (Enumerator_t3884 *, const MethodInfo*))Enumerator_Dispose_m15433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m26592(__this, method) (( bool (*) (Enumerator_t3884 *, const MethodInfo*))Enumerator_MoveNext_m15434_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m26593(__this, method) (( Type_t * (*) (Enumerator_t3884 *, const MethodInfo*))Enumerator_get_Current_m15435_gshared)(__this, method)
