﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>
struct InternalEnumerator_1_t3551;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21846_gshared (InternalEnumerator_1_t3551 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21846(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3551 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21846_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21847_gshared (InternalEnumerator_1_t3551 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21847(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3551 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21847_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21848_gshared (InternalEnumerator_1_t3551 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21848(__this, method) (( void (*) (InternalEnumerator_1_t3551 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21848_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21849_gshared (InternalEnumerator_1_t3551 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21849(__this, method) (( bool (*) (InternalEnumerator_1_t3551 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21849_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::get_Current()
extern "C" KeyValuePair_2_t3550  InternalEnumerator_1_get_Current_m21850_gshared (InternalEnumerator_1_t3551 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21850(__this, method) (( KeyValuePair_2_t3550  (*) (InternalEnumerator_1_t3551 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21850_gshared)(__this, method)
