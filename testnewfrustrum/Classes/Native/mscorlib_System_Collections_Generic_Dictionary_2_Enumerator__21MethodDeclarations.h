﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>
struct Enumerator_t3531;
// System.Object
struct Object_t;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t65;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t711;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m21473(__this, ___dictionary, method) (( void (*) (Enumerator_t3531 *, Dictionary_2_t711 *, const MethodInfo*))Enumerator__ctor_m16259_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21474(__this, method) (( Object_t * (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21475(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21476(__this, method) (( Object_t * (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21477(__this, method) (( Object_t * (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m21478(__this, method) (( bool (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_MoveNext_m16264_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m21479(__this, method) (( KeyValuePair_2_t3529  (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_get_Current_m16265_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21480(__this, method) (( int32_t (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_get_CurrentKey_m16266_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21481(__this, method) (( PropAbstractBehaviour_t65 * (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_get_CurrentValue_m16267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m21482(__this, method) (( void (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_VerifyState_m16268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21483(__this, method) (( void (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_VerifyCurrent_m16269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m21484(__this, method) (( void (*) (Enumerator_t3531 *, const MethodInfo*))Enumerator_Dispose_m16270_gshared)(__this, method)
