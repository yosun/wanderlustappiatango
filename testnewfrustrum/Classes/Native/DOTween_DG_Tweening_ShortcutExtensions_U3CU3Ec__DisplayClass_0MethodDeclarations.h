﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
struct U3CU3Ec__DisplayClass96_t960;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::.ctor()
extern "C" void U3CU3Ec__DisplayClass96__ctor_m5369 (U3CU3Ec__DisplayClass96_t960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__94()
extern "C" Vector3_t15  U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5370 (U3CU3Ec__DisplayClass96_t960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__95(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371 (U3CU3Ec__DisplayClass96_t960 * __this, Vector3_t15  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
