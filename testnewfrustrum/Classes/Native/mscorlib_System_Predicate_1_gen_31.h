﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet
struct DataSet_t594;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.DataSet>
struct  Predicate_1_t3426  : public MulticastDelegate_t307
{
};
