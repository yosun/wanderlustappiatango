﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3820;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3814;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_36.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25717_gshared (Enumerator_t3820 * __this, Dictionary_2_t3814 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m25717(__this, ___dictionary, method) (( void (*) (Enumerator_t3820 *, Dictionary_2_t3814 *, const MethodInfo*))Enumerator__ctor_m25717_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25718_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25718(__this, method) (( Object_t * (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25718_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25719_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720(__this, method) (( Object_t * (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25720_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721(__this, method) (( Object_t * (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25722_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25722(__this, method) (( bool (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_MoveNext_m25722_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3815  Enumerator_get_Current_m25723_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25723(__this, method) (( KeyValuePair_2_t3815  (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_get_Current_m25723_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m25724_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m25724(__this, method) (( uint64_t (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_get_CurrentKey_m25724_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m25725_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m25725(__this, method) (( Object_t * (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_get_CurrentValue_m25725_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m25726_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25726(__this, method) (( void (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_VerifyState_m25726_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m25727_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m25727(__this, method) (( void (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_VerifyCurrent_m25727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25728_gshared (Enumerator_t3820 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25728(__this, method) (( void (*) (Enumerator_t3820 *, const MethodInfo*))Enumerator_Dispose_m25728_gshared)(__this, method)
