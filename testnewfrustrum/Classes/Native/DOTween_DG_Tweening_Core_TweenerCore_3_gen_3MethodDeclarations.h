﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1028;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m14931_gshared (TweenerCore_3_t1028 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m14931(__this, method) (( void (*) (TweenerCore_3_t1028 *, const MethodInfo*))TweenerCore_3__ctor_m14931_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m14932_gshared (TweenerCore_3_t1028 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m14932(__this, method) (( void (*) (TweenerCore_3_t1028 *, const MethodInfo*))TweenerCore_3_Reset_m14932_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m14933_gshared (TweenerCore_3_t1028 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m14933(__this, method) (( bool (*) (TweenerCore_3_t1028 *, const MethodInfo*))TweenerCore_3_Validate_m14933_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m14934_gshared (TweenerCore_3_t1028 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m14934(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1028 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m14934_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m14935_gshared (TweenerCore_3_t1028 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m14935(__this, method) (( bool (*) (TweenerCore_3_t1028 *, const MethodInfo*))TweenerCore_3_Startup_m14935_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m14936_gshared (TweenerCore_3_t1028 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m14936(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1028 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m14936_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
