﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t351;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct  Func_2_t354  : public MulticastDelegate_t307
{
};
