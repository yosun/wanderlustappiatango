﻿#pragma once
#include <stdint.h>
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t3504;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct  List_1_t704  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_items
	ILoadLevelEventHandlerU5BU5D_t3504* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t704_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::EmptyArray
	ILoadLevelEventHandlerU5BU5D_t3504* ___EmptyArray_4;
};
