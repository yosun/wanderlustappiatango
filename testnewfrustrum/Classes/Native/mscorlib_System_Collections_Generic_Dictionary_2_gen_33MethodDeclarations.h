﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3224;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t3230;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t3234;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
struct IDictionary_2_t4065;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4066;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t4067;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m16521_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m16521(__this, method) (( void (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2__ctor_m16521_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16522_gshared (Dictionary_2_t3224 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16522(__this, ___comparer, method) (( void (*) (Dictionary_2_t3224 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16522_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m16523_gshared (Dictionary_2_t3224 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m16523(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3224 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16523_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16524_gshared (Dictionary_2_t3224 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16524(__this, ___capacity, method) (( void (*) (Dictionary_2_t3224 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16524_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16525_gshared (Dictionary_2_t3224 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16525(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3224 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16525_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16526_gshared (Dictionary_2_t3224 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16526(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3224 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m16526_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527(__this, method) (( Object_t* (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16527_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528(__this, method) (( Object_t* (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16528_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16529_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16529(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16529_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16530_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16530(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16530_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16531_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16531(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16531_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16532_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16532(__this, ___key, method) (( bool (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16532_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16533_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16533(__this, ___key, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16533_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534(__this, method) (( bool (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16534_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535(__this, method) (( Object_t * (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16535_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536(__this, method) (( bool (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16536_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2_t3228  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3224 *, KeyValuePair_2_t3228 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16537_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2_t3228  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3224 *, KeyValuePair_2_t3228 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16538_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2U5BU5D_t4066* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3224 *, KeyValuePair_2U5BU5D_t4066*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16539_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2_t3228  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3224 *, KeyValuePair_2_t3228 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16540_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16541_gshared (Dictionary_2_t3224 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16541(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3224 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16541_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542(__this, method) (( Object_t * (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16542_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543(__this, method) (( Object_t* (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16543_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544(__this, method) (( Object_t * (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16544_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16545_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16545(__this, method) (( int32_t (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_get_Count_m16545_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m16546_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16546(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m16546_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16547_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16547(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m16547_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16548_gshared (Dictionary_2_t3224 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16548(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3224 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16548_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16549_gshared (Dictionary_2_t3224 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16549(__this, ___size, method) (( void (*) (Dictionary_2_t3224 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16549_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16550_gshared (Dictionary_2_t3224 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16550(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3224 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16550_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3228  Dictionary_2_make_pair_m16551_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16551(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3228  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m16551_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m16552_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16552(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m16552_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m16553_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16553(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m16553_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16554_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2U5BU5D_t4066* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16554(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3224 *, KeyValuePair_2U5BU5D_t4066*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16554_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m16555_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16555(__this, method) (( void (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_Resize_m16555_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16556_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16556(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m16556_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m16557_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16557(__this, method) (( void (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_Clear_m16557_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16558_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16558(__this, ___key, method) (( bool (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m16558_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16559_gshared (Dictionary_2_t3224 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16559(__this, ___value, method) (( bool (*) (Dictionary_2_t3224 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m16559_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16560_gshared (Dictionary_2_t3224 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16560(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3224 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m16560_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16561_gshared (Dictionary_2_t3224 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16561(__this, ___sender, method) (( void (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16561_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16562_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16562(__this, ___key, method) (( bool (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m16562_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16563_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16563(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3224 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m16563_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t3230 * Dictionary_2_get_Keys_m16564_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16564(__this, method) (( KeyCollection_t3230 * (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_get_Keys_m16564_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t3234 * Dictionary_2_get_Values_m16565_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16565(__this, method) (( ValueCollection_t3234 * (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_get_Values_m16565_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m16566_gshared (Dictionary_2_t3224 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16566(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16566_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m16567_gshared (Dictionary_2_t3224 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16567(__this, ___value, method) (( int32_t (*) (Dictionary_2_t3224 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16567_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16568_gshared (Dictionary_2_t3224 * __this, KeyValuePair_2_t3228  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16568(__this, ___pair, method) (( bool (*) (Dictionary_2_t3224 *, KeyValuePair_2_t3228 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16568_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t3232  Dictionary_2_GetEnumerator_m16569_gshared (Dictionary_2_t3224 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16569(__this, method) (( Enumerator_t3232  (*) (Dictionary_2_t3224 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16569_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m16570_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16570(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16570_gshared)(__this /* static, unused */, ___key, ___value, method)
