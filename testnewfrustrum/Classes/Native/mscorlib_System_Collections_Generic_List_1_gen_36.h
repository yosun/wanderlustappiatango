﻿#pragma once
#include <stdint.h>
// Vuforia.Prop[]
struct PropU5BU5D_t3526;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Prop>
struct  List_1_t780  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::_items
	PropU5BU5D_t3526* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_version
	int32_t ____version_3;
};
struct List_1_t780_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::EmptyArray
	PropU5BU5D_t3526* ___EmptyArray_4;
};
