﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3611;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3603;
// System.Collections.Generic.IEnumerator`1<Vuforia.WebCamProfile/ProfileData>
struct IEnumerator_1_t4259;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t3600;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_45.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m22578_gshared (ValueCollection_t3611 * __this, Dictionary_2_t3603 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m22578(__this, ___dictionary, method) (( void (*) (ValueCollection_t3611 *, Dictionary_2_t3603 *, const MethodInfo*))ValueCollection__ctor_m22578_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22579_gshared (ValueCollection_t3611 * __this, ProfileData_t734  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22579(__this, ___item, method) (( void (*) (ValueCollection_t3611 *, ProfileData_t734 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22579_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22580_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22580(__this, method) (( void (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22580_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22581_gshared (ValueCollection_t3611 * __this, ProfileData_t734  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22581(__this, ___item, method) (( bool (*) (ValueCollection_t3611 *, ProfileData_t734 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22581_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22582_gshared (ValueCollection_t3611 * __this, ProfileData_t734  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22582(__this, ___item, method) (( bool (*) (ValueCollection_t3611 *, ProfileData_t734 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22582_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22583_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22583(__this, method) (( Object_t* (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22583_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22584_gshared (ValueCollection_t3611 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m22584(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3611 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m22584_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22585_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22585(__this, method) (( Object_t * (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22586_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22586(__this, method) (( bool (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22586_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22587_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22587(__this, method) (( bool (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22587_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m22588_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m22588(__this, method) (( Object_t * (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m22588_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m22589_gshared (ValueCollection_t3611 * __this, ProfileDataU5BU5D_t3600* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m22589(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3611 *, ProfileDataU5BU5D_t3600*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m22589_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3612  ValueCollection_GetEnumerator_m22590_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m22590(__this, method) (( Enumerator_t3612  (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_GetEnumerator_m22590_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m22591_gshared (ValueCollection_t3611 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m22591(__this, method) (( int32_t (*) (ValueCollection_t3611 *, const MethodInfo*))ValueCollection_get_Count_m22591_gshared)(__this, method)
