﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t1782_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate__ctor_m8181_ParameterInfos[] = 
{
	{"context", 0, 134218531, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218532, 0, &ByteU5BU5D_t616_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificate__ctor_m8181_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificate__ctor_m8181/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate__ctor_m8181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Update()
extern const MethodInfo TlsServerCertificate_Update_m8182_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificate_Update_m8182/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m8183_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsSsl3_m8183/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsTls1()
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m8184_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsTls1_m8184/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1699_0_0_0;
extern const Il2CppType X509Certificate_t1699_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate_checkCertificateUsage_m8185_ParameterInfos[] = 
{
	{"cert", 0, 134218533, 0, &X509Certificate_t1699_0_0_0},
};
extern const Il2CppType Boolean_t169_0_0_0;
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkCertificateUsage_m8185_MethodInfo = 
{
	"checkCertificateUsage"/* name */
	, (methodPointerType)&TlsServerCertificate_checkCertificateUsage_m8185/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate_checkCertificateUsage_m8185_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1694_0_0_0;
extern const Il2CppType X509CertificateCollection_t1694_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate_validateCertificates_m8186_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, 0, &X509CertificateCollection_t1694_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo TlsServerCertificate_validateCertificates_m8186_MethodInfo = 
{
	"validateCertificates"/* name */
	, (methodPointerType)&TlsServerCertificate_validateCertificates_m8186/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate_validateCertificates_m8186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1699_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate_checkServerIdentity_m8187_ParameterInfos[] = 
{
	{"cert", 0, 134218535, 0, &X509Certificate_t1699_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkServerIdentity(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkServerIdentity_m8187_MethodInfo = 
{
	"checkServerIdentity"/* name */
	, (methodPointerType)&TlsServerCertificate_checkServerIdentity_m8187/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate_checkServerIdentity_m8187_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate_checkDomainName_m8188_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkDomainName(System.String)
extern const MethodInfo TlsServerCertificate_checkDomainName_m8188_MethodInfo = 
{
	"checkDomainName"/* name */
	, (methodPointerType)&TlsServerCertificate_checkDomainName_m8188/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate_checkDomainName_m8188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1782_TlsServerCertificate_Match_m8189_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, 0, &String_t_0_0_0},
	{"pattern", 1, 134218538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Match(System.String,System.String)
extern const MethodInfo TlsServerCertificate_Match_m8189_MethodInfo = 
{
	"Match"/* name */
	, (methodPointerType)&TlsServerCertificate_Match_m8189/* method */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1782_TlsServerCertificate_Match_m8189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificate_t1782_MethodInfos[] =
{
	&TlsServerCertificate__ctor_m8181_MethodInfo,
	&TlsServerCertificate_Update_m8182_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m8183_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m8184_MethodInfo,
	&TlsServerCertificate_checkCertificateUsage_m8185_MethodInfo,
	&TlsServerCertificate_validateCertificates_m8186_MethodInfo,
	&TlsServerCertificate_checkServerIdentity_m8187_MethodInfo,
	&TlsServerCertificate_checkDomainName_m8188_MethodInfo,
	&TlsServerCertificate_Match_m8189_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
extern const MethodInfo Stream_Dispose_m8421_MethodInfo;
extern const MethodInfo TlsStream_get_CanRead_m8122_MethodInfo;
extern const MethodInfo TlsStream_get_CanSeek_m8123_MethodInfo;
extern const MethodInfo TlsStream_get_CanWrite_m8121_MethodInfo;
extern const MethodInfo TlsStream_get_Length_m8126_MethodInfo;
extern const MethodInfo TlsStream_get_Position_m8124_MethodInfo;
extern const MethodInfo TlsStream_set_Position_m8125_MethodInfo;
extern const MethodInfo Stream_Dispose_m8345_MethodInfo;
extern const MethodInfo Stream_Close_m8344_MethodInfo;
extern const MethodInfo TlsStream_Flush_m8139_MethodInfo;
extern const MethodInfo TlsStream_Read_m8142_MethodInfo;
extern const MethodInfo Stream_ReadByte_m8422_MethodInfo;
extern const MethodInfo TlsStream_Seek_m8141_MethodInfo;
extern const MethodInfo TlsStream_SetLength_m8140_MethodInfo;
extern const MethodInfo TlsStream_Write_m8143_MethodInfo;
extern const MethodInfo Stream_WriteByte_m8423_MethodInfo;
extern const MethodInfo Stream_BeginRead_m8424_MethodInfo;
extern const MethodInfo Stream_BeginWrite_m8425_MethodInfo;
extern const MethodInfo Stream_EndRead_m8426_MethodInfo;
extern const MethodInfo Stream_EndWrite_m8427_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m8184_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m8183_MethodInfo;
extern const MethodInfo TlsServerCertificate_Update_m8182_MethodInfo;
extern const MethodInfo HandshakeMessage_EncodeMessage_m8152_MethodInfo;
static const Il2CppMethodReference TlsServerCertificate_t1782_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m8184_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m8183_MethodInfo,
	&TlsServerCertificate_Update_m8182_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerCertificate_t1782_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t144_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t1782_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificate_t1782_0_0_0;
extern const Il2CppType TlsServerCertificate_t1782_1_0_0;
extern const Il2CppType HandshakeMessage_t1753_0_0_0;
struct TlsServerCertificate_t1782;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t1782_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t1782_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerCertificate_t1782_VTable/* vtableMethods */
	, TlsServerCertificate_t1782_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 458/* fieldStart */

};
TypeInfo TlsServerCertificate_t1782_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificate_t1782_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificate_t1782_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t1782_0_0_0/* byval_arg */
	, &TlsServerCertificate_t1782_1_0_0/* this_arg */
	, &TlsServerCertificate_t1782_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t1782)/* instance_size */
	, sizeof (TlsServerCertificate_t1782)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t1783_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerCertificateRequest_t1783_TlsServerCertificateRequest__ctor_m8190_ParameterInfos[] = 
{
	{"context", 0, 134218539, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218540, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificateRequest__ctor_m8190_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificateRequest__ctor_m8190/* method */
	, &TlsServerCertificateRequest_t1783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerCertificateRequest_t1783_TlsServerCertificateRequest__ctor_m8190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::Update()
extern const MethodInfo TlsServerCertificateRequest_Update_m8191_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_Update_m8191/* method */
	, &TlsServerCertificateRequest_t1783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m8192_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsSsl3_m8192/* method */
	, &TlsServerCertificateRequest_t1783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsTls1()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m8193_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsTls1_m8193/* method */
	, &TlsServerCertificateRequest_t1783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificateRequest_t1783_MethodInfos[] =
{
	&TlsServerCertificateRequest__ctor_m8190_MethodInfo,
	&TlsServerCertificateRequest_Update_m8191_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m8192_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m8193_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m8193_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m8192_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_Update_m8191_MethodInfo;
static const Il2CppMethodReference TlsServerCertificateRequest_t1783_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m8193_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m8192_MethodInfo,
	&TlsServerCertificateRequest_Update_m8191_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerCertificateRequest_t1783_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t1783_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificateRequest_t1783_0_0_0;
extern const Il2CppType TlsServerCertificateRequest_t1783_1_0_0;
struct TlsServerCertificateRequest_t1783;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t1783_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t1783_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerCertificateRequest_t1783_VTable/* vtableMethods */
	, TlsServerCertificateRequest_t1783_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 459/* fieldStart */

};
TypeInfo TlsServerCertificateRequest_t1783_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificateRequest_t1783_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificateRequest_t1783_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t1783_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t1783_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t1783_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t1783)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t1783)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t1784_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerFinished_t1784_TlsServerFinished__ctor_m8194_ParameterInfos[] = 
{
	{"context", 0, 134218541, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218542, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerFinished__ctor_m8194_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerFinished__ctor_m8194/* method */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerFinished_t1784_TlsServerFinished__ctor_m8194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.cctor()
extern const MethodInfo TlsServerFinished__cctor_m8195_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TlsServerFinished__cctor_m8195/* method */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Update()
extern const MethodInfo TlsServerFinished_Update_m8196_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerFinished_Update_m8196/* method */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsSsl3()
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m8197_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsSsl3_m8197/* method */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsTls1()
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m8198_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsTls1_m8198/* method */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerFinished_t1784_MethodInfos[] =
{
	&TlsServerFinished__ctor_m8194_MethodInfo,
	&TlsServerFinished__cctor_m8195_MethodInfo,
	&TlsServerFinished_Update_m8196_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m8197_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m8198_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m8198_MethodInfo;
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m8197_MethodInfo;
extern const MethodInfo TlsServerFinished_Update_m8196_MethodInfo;
static const Il2CppMethodReference TlsServerFinished_t1784_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m8198_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m8197_MethodInfo,
	&TlsServerFinished_Update_m8196_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerFinished_t1784_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t1784_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerFinished_t1784_0_0_0;
extern const Il2CppType TlsServerFinished_t1784_1_0_0;
struct TlsServerFinished_t1784;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t1784_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t1784_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerFinished_t1784_VTable/* vtableMethods */
	, TlsServerFinished_t1784_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 461/* fieldStart */

};
TypeInfo TlsServerFinished_t1784_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerFinished_t1784_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerFinished_t1784_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t1784_0_0_0/* byval_arg */
	, &TlsServerFinished_t1784_1_0_0/* this_arg */
	, &TlsServerFinished_t1784_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t1784)/* instance_size */
	, sizeof (TlsServerFinished_t1784)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t1784_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t1785_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerHello_t1785_TlsServerHello__ctor_m8199_ParameterInfos[] = 
{
	{"context", 0, 134218543, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218544, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHello__ctor_m8199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHello__ctor_m8199/* method */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerHello_t1785_TlsServerHello__ctor_m8199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::Update()
extern const MethodInfo TlsServerHello_Update_m8200_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerHello_Update_m8200/* method */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsSsl3()
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m8201_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsSsl3_m8201/* method */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsTls1()
extern const MethodInfo TlsServerHello_ProcessAsTls1_m8202_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsTls1_m8202/* method */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int16_t534_0_0_0;
extern const Il2CppType Int16_t534_0_0_0;
static const ParameterInfo TlsServerHello_t1785_TlsServerHello_processProtocol_m8203_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, 0, &Int16_t534_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::processProtocol(System.Int16)
extern const MethodInfo TlsServerHello_processProtocol_m8203_MethodInfo = 
{
	"processProtocol"/* name */
	, (methodPointerType)&TlsServerHello_processProtocol_m8203/* method */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534/* invoker_method */
	, TlsServerHello_t1785_TlsServerHello_processProtocol_m8203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHello_t1785_MethodInfos[] =
{
	&TlsServerHello__ctor_m8199_MethodInfo,
	&TlsServerHello_Update_m8200_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m8201_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m8202_MethodInfo,
	&TlsServerHello_processProtocol_m8203_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHello_ProcessAsTls1_m8202_MethodInfo;
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m8201_MethodInfo;
extern const MethodInfo TlsServerHello_Update_m8200_MethodInfo;
static const Il2CppMethodReference TlsServerHello_t1785_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m8202_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m8201_MethodInfo,
	&TlsServerHello_Update_m8200_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerHello_t1785_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t1785_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHello_t1785_0_0_0;
extern const Il2CppType TlsServerHello_t1785_1_0_0;
struct TlsServerHello_t1785;
const Il2CppTypeDefinitionMetadata TlsServerHello_t1785_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t1785_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerHello_t1785_VTable/* vtableMethods */
	, TlsServerHello_t1785_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 462/* fieldStart */

};
TypeInfo TlsServerHello_t1785_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHello_t1785_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHello_t1785_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t1785_0_0_0/* byval_arg */
	, &TlsServerHello_t1785_1_0_0/* this_arg */
	, &TlsServerHello_t1785_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t1785)/* instance_size */
	, sizeof (TlsServerHello_t1785)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t1786_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerHelloDone_t1786_TlsServerHelloDone__ctor_m8204_ParameterInfos[] = 
{
	{"context", 0, 134218546, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218547, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHelloDone__ctor_m8204_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHelloDone__ctor_m8204/* method */
	, &TlsServerHelloDone_t1786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerHelloDone_t1786_TlsServerHelloDone__ctor_m8204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsSsl3()
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m8205_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsSsl3_m8205/* method */
	, &TlsServerHelloDone_t1786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsTls1()
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m8206_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsTls1_m8206/* method */
	, &TlsServerHelloDone_t1786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHelloDone_t1786_MethodInfos[] =
{
	&TlsServerHelloDone__ctor_m8204_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m8205_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m8206_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m8206_MethodInfo;
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m8205_MethodInfo;
extern const MethodInfo HandshakeMessage_Update_m8151_MethodInfo;
static const Il2CppMethodReference TlsServerHelloDone_t1786_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m8206_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m8205_MethodInfo,
	&HandshakeMessage_Update_m8151_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerHelloDone_t1786_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t1786_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHelloDone_t1786_0_0_0;
extern const Il2CppType TlsServerHelloDone_t1786_1_0_0;
struct TlsServerHelloDone_t1786;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t1786_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t1786_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerHelloDone_t1786_VTable/* vtableMethods */
	, TlsServerHelloDone_t1786_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TlsServerHelloDone_t1786_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHelloDone_t1786_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHelloDone_t1786_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t1786_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t1786_1_0_0/* this_arg */
	, &TlsServerHelloDone_t1786_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t1786)/* instance_size */
	, sizeof (TlsServerHelloDone_t1786)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t1787_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
extern const Il2CppType Context_t1726_0_0_0;
extern const Il2CppType ByteU5BU5D_t616_0_0_0;
static const ParameterInfo TlsServerKeyExchange_t1787_TlsServerKeyExchange__ctor_m8207_ParameterInfos[] = 
{
	{"context", 0, 134218548, 0, &Context_t1726_0_0_0},
	{"buffer", 1, 134218549, 0, &ByteU5BU5D_t616_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerKeyExchange__ctor_m8207_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerKeyExchange__ctor_m8207/* method */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, TlsServerKeyExchange_t1787_TlsServerKeyExchange__ctor_m8207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::Update()
extern const MethodInfo TlsServerKeyExchange_Update_m8208_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerKeyExchange_Update_m8208/* method */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsSsl3()
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m8209_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsSsl3_m8209/* method */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsTls1()
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m8210_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsTls1_m8210/* method */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::verifySignature()
extern const MethodInfo TlsServerKeyExchange_verifySignature_m8211_MethodInfo = 
{
	"verifySignature"/* name */
	, (methodPointerType)&TlsServerKeyExchange_verifySignature_m8211/* method */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerKeyExchange_t1787_MethodInfos[] =
{
	&TlsServerKeyExchange__ctor_m8207_MethodInfo,
	&TlsServerKeyExchange_Update_m8208_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m8209_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m8210_MethodInfo,
	&TlsServerKeyExchange_verifySignature_m8211_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m8210_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m8209_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_Update_m8208_MethodInfo;
static const Il2CppMethodReference TlsServerKeyExchange_t1787_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stream_Dispose_m8421_MethodInfo,
	&TlsStream_get_CanRead_m8122_MethodInfo,
	&TlsStream_get_CanSeek_m8123_MethodInfo,
	&TlsStream_get_CanWrite_m8121_MethodInfo,
	&TlsStream_get_Length_m8126_MethodInfo,
	&TlsStream_get_Position_m8124_MethodInfo,
	&TlsStream_set_Position_m8125_MethodInfo,
	&Stream_Dispose_m8345_MethodInfo,
	&Stream_Close_m8344_MethodInfo,
	&TlsStream_Flush_m8139_MethodInfo,
	&TlsStream_Read_m8142_MethodInfo,
	&Stream_ReadByte_m8422_MethodInfo,
	&TlsStream_Seek_m8141_MethodInfo,
	&TlsStream_SetLength_m8140_MethodInfo,
	&TlsStream_Write_m8143_MethodInfo,
	&Stream_WriteByte_m8423_MethodInfo,
	&Stream_BeginRead_m8424_MethodInfo,
	&Stream_BeginWrite_m8425_MethodInfo,
	&Stream_EndRead_m8426_MethodInfo,
	&Stream_EndWrite_m8427_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m8210_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m8209_MethodInfo,
	&TlsServerKeyExchange_Update_m8208_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8152_MethodInfo,
};
static bool TlsServerKeyExchange_t1787_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t1787_InterfacesOffsets[] = 
{
	{ &IDisposable_t144_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerKeyExchange_t1787_0_0_0;
extern const Il2CppType TlsServerKeyExchange_t1787_1_0_0;
struct TlsServerKeyExchange_t1787;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t1787_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t1787_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1753_0_0_0/* parent */
	, TlsServerKeyExchange_t1787_VTable/* vtableMethods */
	, TlsServerKeyExchange_t1787_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 466/* fieldStart */

};
TypeInfo TlsServerKeyExchange_t1787_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerKeyExchange_t1787_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerKeyExchange_t1787_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t1787_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t1787_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t1787_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t1787)/* instance_size */
	, sizeof (TlsServerKeyExchange_t1787)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1788_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t1788_PrimalityTest__ctor_m8212_ParameterInfos[] = 
{
	{"object", 0, 134218550, 0, &Object_t_0_0_0},
	{"method", 1, 134218551, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m8212_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m8212/* method */
	, &PrimalityTest_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t1788_PrimalityTest__ctor_m8212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1659_0_0_0;
extern const Il2CppType BigInteger_t1659_0_0_0;
extern const Il2CppType ConfidenceFactor_t1664_0_0_0;
extern const Il2CppType ConfidenceFactor_t1664_0_0_0;
static const ParameterInfo PrimalityTest_t1788_PrimalityTest_Invoke_m8213_ParameterInfos[] = 
{
	{"bi", 0, 134218552, 0, &BigInteger_t1659_0_0_0},
	{"confidence", 1, 134218553, 0, &ConfidenceFactor_t1664_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m8213_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m8213/* method */
	, &PrimalityTest_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Int32_t127/* invoker_method */
	, PrimalityTest_t1788_PrimalityTest_Invoke_m8213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1659_0_0_0;
extern const Il2CppType ConfidenceFactor_t1664_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t1788_PrimalityTest_BeginInvoke_m8214_ParameterInfos[] = 
{
	{"bi", 0, 134218554, 0, &BigInteger_t1659_0_0_0},
	{"confidence", 1, 134218555, 0, &ConfidenceFactor_t1664_0_0_0},
	{"callback", 2, 134218556, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134218557, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m8214_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m8214/* method */
	, &PrimalityTest_t1788_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t1788_PrimalityTest_BeginInvoke_m8214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo PrimalityTest_t1788_PrimalityTest_EndInvoke_m8215_ParameterInfos[] = 
{
	{"result", 0, 134218558, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m8215_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m8215/* method */
	, &PrimalityTest_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, PrimalityTest_t1788_PrimalityTest_EndInvoke_m8215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t1788_MethodInfos[] =
{
	&PrimalityTest__ctor_m8212_MethodInfo,
	&PrimalityTest_Invoke_m8213_MethodInfo,
	&PrimalityTest_BeginInvoke_m8214_MethodInfo,
	&PrimalityTest_EndInvoke_m8215_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m8213_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m8214_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m8215_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t1788_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&PrimalityTest_Invoke_m8213_MethodInfo,
	&PrimalityTest_BeginInvoke_m8214_MethodInfo,
	&PrimalityTest_EndInvoke_m8215_MethodInfo,
};
static bool PrimalityTest_t1788_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t1788_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTest_t1788_0_0_0;
extern const Il2CppType PrimalityTest_t1788_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct PrimalityTest_t1788;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1788_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, PrimalityTest_t1788_VTable/* vtableMethods */
	, PrimalityTest_t1788_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t1788_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t1788_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t1788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1788_0_0_0/* byval_arg */
	, &PrimalityTest_t1788_1_0_0/* this_arg */
	, &PrimalityTest_t1788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1788/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1788)/* instance_size */
	, sizeof (PrimalityTest_t1788)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t1763_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1763_CertificateValidationCallback__ctor_m8216_ParameterInfos[] = 
{
	{"object", 0, 134218559, 0, &Object_t_0_0_0},
	{"method", 1, 134218560, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback__ctor_m8216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback__ctor_m8216/* method */
	, &CertificateValidationCallback_t1763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback_t1763_CertificateValidationCallback__ctor_m8216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1763_CertificateValidationCallback_Invoke_m8217_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, 0, &X509Certificate_t1771_0_0_0},
	{"certificateErrors", 1, 134218562, 0, &Int32U5BU5D_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
extern const MethodInfo CertificateValidationCallback_Invoke_m8217_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_Invoke_m8217/* method */
	, &CertificateValidationCallback_t1763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1763_CertificateValidationCallback_Invoke_m8217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType Int32U5BU5D_t19_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1763_CertificateValidationCallback_BeginInvoke_m8218_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, 0, &X509Certificate_t1771_0_0_0},
	{"certificateErrors", 1, 134218564, 0, &Int32U5BU5D_t19_0_0_0},
	{"callback", 2, 134218565, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134218566, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[],System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m8218_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_BeginInvoke_m8218/* method */
	, &CertificateValidationCallback_t1763_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1763_CertificateValidationCallback_BeginInvoke_m8218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1763_CertificateValidationCallback_EndInvoke_m8219_ParameterInfos[] = 
{
	{"result", 0, 134218567, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback_EndInvoke_m8219_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_EndInvoke_m8219/* method */
	, &CertificateValidationCallback_t1763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, CertificateValidationCallback_t1763_CertificateValidationCallback_EndInvoke_m8219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback_t1763_MethodInfos[] =
{
	&CertificateValidationCallback__ctor_m8216_MethodInfo,
	&CertificateValidationCallback_Invoke_m8217_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m8218_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m8219_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback_Invoke_m8217_MethodInfo;
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m8218_MethodInfo;
extern const MethodInfo CertificateValidationCallback_EndInvoke_m8219_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback_t1763_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&CertificateValidationCallback_Invoke_m8217_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m8218_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m8219_MethodInfo,
};
static bool CertificateValidationCallback_t1763_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t1763_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback_t1763_0_0_0;
extern const Il2CppType CertificateValidationCallback_t1763_1_0_0;
struct CertificateValidationCallback_t1763;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t1763_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t1763_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, CertificateValidationCallback_t1763_VTable/* vtableMethods */
	, CertificateValidationCallback_t1763_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback_t1763_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback_t1763_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback_t1763_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t1763_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t1763_1_0_0/* this_arg */
	, &CertificateValidationCallback_t1763_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t1763/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t1763)/* instance_size */
	, sizeof (CertificateValidationCallback_t1763)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t1764_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1764_CertificateValidationCallback2__ctor_m8220_ParameterInfos[] = 
{
	{"object", 0, 134218568, 0, &Object_t_0_0_0},
	{"method", 1, 134218569, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback2__ctor_m8220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback2__ctor_m8220/* method */
	, &CertificateValidationCallback2_t1764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback2_t1764_CertificateValidationCallback2__ctor_m8220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1694_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1764_CertificateValidationCallback2_Invoke_m8221_ParameterInfos[] = 
{
	{"collection", 0, 134218570, 0, &X509CertificateCollection_t1694_0_0_0},
};
extern const Il2CppType ValidationResult_t1762_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::Invoke(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo CertificateValidationCallback2_Invoke_m8221_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_Invoke_m8221/* method */
	, &CertificateValidationCallback2_t1764_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1762_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1764_CertificateValidationCallback2_Invoke_m8221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1694_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1764_CertificateValidationCallback2_BeginInvoke_m8222_ParameterInfos[] = 
{
	{"collection", 0, 134218571, 0, &X509CertificateCollection_t1694_0_0_0},
	{"callback", 1, 134218572, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134218573, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::BeginInvoke(Mono.Security.X509.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m8222_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_BeginInvoke_m8222/* method */
	, &CertificateValidationCallback2_t1764_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1764_CertificateValidationCallback2_BeginInvoke_m8222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1764_CertificateValidationCallback2_EndInvoke_m8223_ParameterInfos[] = 
{
	{"result", 0, 134218574, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m8223_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_EndInvoke_m8223/* method */
	, &CertificateValidationCallback2_t1764_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1762_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1764_CertificateValidationCallback2_EndInvoke_m8223_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback2_t1764_MethodInfos[] =
{
	&CertificateValidationCallback2__ctor_m8220_MethodInfo,
	&CertificateValidationCallback2_Invoke_m8221_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m8222_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m8223_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback2_Invoke_m8221_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m8222_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m8223_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback2_t1764_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&CertificateValidationCallback2_Invoke_m8221_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m8222_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m8223_MethodInfo,
};
static bool CertificateValidationCallback2_t1764_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t1764_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback2_t1764_0_0_0;
extern const Il2CppType CertificateValidationCallback2_t1764_1_0_0;
struct CertificateValidationCallback2_t1764;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t1764_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t1764_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, CertificateValidationCallback2_t1764_VTable/* vtableMethods */
	, CertificateValidationCallback2_t1764_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback2_t1764_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback2_t1764_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback2_t1764_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t1764_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t1764_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t1764_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t1764/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t1764)/* instance_size */
	, sizeof (CertificateValidationCallback2_t1764)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t1747_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1747_CertificateSelectionCallback__ctor_m8224_ParameterInfos[] = 
{
	{"object", 0, 134218575, 0, &Object_t_0_0_0},
	{"method", 1, 134218576, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateSelectionCallback__ctor_m8224_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateSelectionCallback__ctor_m8224/* method */
	, &CertificateSelectionCallback_t1747_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, CertificateSelectionCallback_t1747_CertificateSelectionCallback__ctor_m8224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1770_0_0_0;
extern const Il2CppType X509CertificateCollection_t1770_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1770_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1747_CertificateSelectionCallback_Invoke_m8225_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, 0, &X509CertificateCollection_t1770_0_0_0},
	{"serverCertificate", 1, 134218578, 0, &X509Certificate_t1771_0_0_0},
	{"targetHost", 2, 134218579, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, 0, &X509CertificateCollection_t1770_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern const MethodInfo CertificateSelectionCallback_Invoke_m8225_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_Invoke_m8225/* method */
	, &CertificateSelectionCallback_t1747_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1771_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1747_CertificateSelectionCallback_Invoke_m8225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1770_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1770_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1747_CertificateSelectionCallback_BeginInvoke_m8226_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, 0, &X509CertificateCollection_t1770_0_0_0},
	{"serverCertificate", 1, 134218582, 0, &X509Certificate_t1771_0_0_0},
	{"targetHost", 2, 134218583, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, 0, &X509CertificateCollection_t1770_0_0_0},
	{"callback", 4, 134218585, 0, &AsyncCallback_t305_0_0_0},
	{"object", 5, 134218586, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateSelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m8226_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_BeginInvoke_m8226/* method */
	, &CertificateSelectionCallback_t1747_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1747_CertificateSelectionCallback_BeginInvoke_m8226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1747_CertificateSelectionCallback_EndInvoke_m8227_ParameterInfos[] = 
{
	{"result", 0, 134218587, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m8227_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_EndInvoke_m8227/* method */
	, &CertificateSelectionCallback_t1747_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1771_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1747_CertificateSelectionCallback_EndInvoke_m8227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateSelectionCallback_t1747_MethodInfos[] =
{
	&CertificateSelectionCallback__ctor_m8224_MethodInfo,
	&CertificateSelectionCallback_Invoke_m8225_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m8226_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m8227_MethodInfo,
	NULL
};
extern const MethodInfo CertificateSelectionCallback_Invoke_m8225_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m8226_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m8227_MethodInfo;
static const Il2CppMethodReference CertificateSelectionCallback_t1747_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&CertificateSelectionCallback_Invoke_m8225_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m8226_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m8227_MethodInfo,
};
static bool CertificateSelectionCallback_t1747_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t1747_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateSelectionCallback_t1747_0_0_0;
extern const Il2CppType CertificateSelectionCallback_t1747_1_0_0;
struct CertificateSelectionCallback_t1747;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t1747_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t1747_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, CertificateSelectionCallback_t1747_VTable/* vtableMethods */
	, CertificateSelectionCallback_t1747_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateSelectionCallback_t1747_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateSelectionCallback_t1747_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateSelectionCallback_t1747_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t1747_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t1747_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t1747_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t1747/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t1747)/* instance_size */
	, sizeof (CertificateSelectionCallback_t1747)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback__ctor_m8228_ParameterInfos[] = 
{
	{"object", 0, 134218588, 0, &Object_t_0_0_0},
	{"method", 1, 134218589, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrivateKeySelectionCallback__ctor_m8228_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback__ctor_m8228/* method */
	, &PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback__ctor_m8228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_Invoke_m8229_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, 0, &X509Certificate_t1771_0_0_0},
	{"targetHost", 1, 134218591, 0, &String_t_0_0_0},
};
extern const Il2CppType AsymmetricAlgorithm_t1789_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m8229_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_Invoke_m8229/* method */
	, &PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1789_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_Invoke_m8229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_BeginInvoke_m8230_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, 0, &X509Certificate_t1771_0_0_0},
	{"targetHost", 1, 134218593, 0, &String_t_0_0_0},
	{"callback", 2, 134218594, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134218595, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.AsyncCallback,System.Object)
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m8230_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_BeginInvoke_m8230/* method */
	, &PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_BeginInvoke_m8230_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_EndInvoke_m8231_ParameterInfos[] = 
{
	{"result", 0, 134218596, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m8231_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_EndInvoke_m8231/* method */
	, &PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1789_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1748_PrivateKeySelectionCallback_EndInvoke_m8231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrivateKeySelectionCallback_t1748_MethodInfos[] =
{
	&PrivateKeySelectionCallback__ctor_m8228_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m8229_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m8230_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m8231_MethodInfo,
	NULL
};
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m8229_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m8230_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m8231_MethodInfo;
static const Il2CppMethodReference PrivateKeySelectionCallback_t1748_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m8229_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m8230_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m8231_MethodInfo,
};
static bool PrivateKeySelectionCallback_t1748_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t1748_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeySelectionCallback_t1748_0_0_0;
extern const Il2CppType PrivateKeySelectionCallback_t1748_1_0_0;
struct PrivateKeySelectionCallback_t1748;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t1748_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t1748_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, PrivateKeySelectionCallback_t1748_VTable/* vtableMethods */
	, PrivateKeySelectionCallback_t1748_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, PrivateKeySelectionCallback_t1748_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrivateKeySelectionCallback_t1748_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t1748_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t1748_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t1748_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1748/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t1748)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t1748)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1790_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t1790_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU243132_t1790_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU243132_t1790_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1790_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t1790_1_0_0;
extern const Il2CppType ValueType_t524_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1799_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1799_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1790_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU243132_t1790_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t1790_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t1790_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t1790_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t1790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1790_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1790_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1790_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1790_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1790_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1790)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1790)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1790_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1791_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t1791_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t1791_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24256_t1791_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1791_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1791_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1791_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24256_t1791_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t1791_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t1791_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t1791_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t1791_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1791_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1791_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1791_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1791_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1791_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1791)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1791)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1791_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1792_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t1792_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t1792_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2420_t1792_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1792_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1792_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1792_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2420_t1792_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t1792_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t1792_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t1792_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t1792_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1792_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1792_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1792_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1792_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1792_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1792)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1792)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1792_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1793_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t1793_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t1793_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2432_t1793_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1793_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1793_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1793_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2432_t1793_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t1793_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t1793_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t1793_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t1793_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1793_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1793_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1793_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1793_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1793_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1793_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1793)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1793)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1793_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1794_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t1794_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t1794_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2448_t1794_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1794_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1794_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1794_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2448_t1794_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t1794_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t1794_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t1794_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t1794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1794_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1794_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1794_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1794_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1794_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1794)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1794)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1794_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1795_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t1795_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t1795_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2464_t1795_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1795_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1795_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1795_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2464_t1795_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t1795_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t1795_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t1795_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t1795_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1795_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1795_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1795_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1795_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1795_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1795_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1795)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1795)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1795_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1796_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1796_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1796_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2412_t1796_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1796_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1796_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1796_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2412_t1796_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1796_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1796_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1796_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1796_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1796_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1796_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1796_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1796_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1796_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1796_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1796)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1796)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1796_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1797_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t1797_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t1797_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2416_t1797_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1797_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1797_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1797_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2416_t1797_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t1797_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t1797_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t1797_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t1797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1797_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1797_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1797_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1797_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1797_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1797)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1797)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1797_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t1798_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU244_t1798_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU244_t1798_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU244_t1798_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU244_t1798_0_0_0;
extern const Il2CppType U24ArrayTypeU244_t1798_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t1798_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU244_t1798_VTable/* vtableMethods */
	, U24ArrayTypeU244_t1798_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU244_t1798_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU244_t1798_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU244_t1798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t1798_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t1798_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t1798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t1798_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1798_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1798_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t1798)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t1798)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t1798_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1799_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1799_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t1790_0_0_0,
	&U24ArrayTypeU24256_t1791_0_0_0,
	&U24ArrayTypeU2420_t1792_0_0_0,
	&U24ArrayTypeU2432_t1793_0_0_0,
	&U24ArrayTypeU2448_t1794_0_0_0,
	&U24ArrayTypeU2464_t1795_0_0_0,
	&U24ArrayTypeU2412_t1796_0_0_0,
	&U24ArrayTypeU2416_t1797_0_0_0,
	&U24ArrayTypeU244_t1798_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1799_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1799_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1799_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1799;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1799_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1799_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1799_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1799_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 468/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1799_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1799_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 38/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1799_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1799_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1799)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1799)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1799_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
