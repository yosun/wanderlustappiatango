﻿#pragma once
#include <stdint.h>
// System.Reflection.Missing
struct Missing_t2250;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Missing
struct  Missing_t2250  : public Object_t
{
};
struct Missing_t2250_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t2250 * ___Value_0;
};
