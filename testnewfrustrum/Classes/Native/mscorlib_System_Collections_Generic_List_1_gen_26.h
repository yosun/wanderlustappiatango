﻿#pragma once
#include <stdint.h>
// Vuforia.Marker[]
struct MarkerU5BU5D_t3430;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
struct  List_1_t813  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::_items
	MarkerU5BU5D_t3430* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_version
	int32_t ____version_3;
};
struct List_1_t813_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::EmptyArray
	MarkerU5BU5D_t3430* ___EmptyArray_4;
};
