﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t1003;
struct StringOptions_t1003_marshaled;

void StringOptions_t1003_marshal(const StringOptions_t1003& unmarshaled, StringOptions_t1003_marshaled& marshaled);
void StringOptions_t1003_marshal_back(const StringOptions_t1003_marshaled& marshaled, StringOptions_t1003& unmarshaled);
void StringOptions_t1003_marshal_cleanup(StringOptions_t1003_marshaled& marshaled);
