﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t103;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t374  : public MulticastDelegate_t307
{
};
