﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t38;
// System.String
struct String_t;

// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
extern "C" void DataSetLoadAbstractBehaviour_LoadDatasets_m2810 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2811 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern "C" void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2812 (DataSetLoadAbstractBehaviour_t38 * __this, String_t* ___searchDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
extern "C" void DataSetLoadAbstractBehaviour_Start_m2813 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
extern "C" void DataSetLoadAbstractBehaviour__ctor_m394 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
