﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t729;
// Vuforia.StateManager
struct StateManager_t713;

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
extern "C" StateManager_t713 * TrackerManagerImpl_GetStateManager_m4067 (TrackerManagerImpl_t729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
extern "C" void TrackerManagerImpl__ctor_m4068 (TrackerManagerImpl_t729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
