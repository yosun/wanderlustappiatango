﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1020;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23307_gshared (TweenerCore_3_t1020 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23307(__this, method) (( void (*) (TweenerCore_3_t1020 *, const MethodInfo*))TweenerCore_3__ctor_m23307_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23308_gshared (TweenerCore_3_t1020 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23308(__this, method) (( void (*) (TweenerCore_3_t1020 *, const MethodInfo*))TweenerCore_3_Reset_m23308_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23309_gshared (TweenerCore_3_t1020 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23309(__this, method) (( bool (*) (TweenerCore_3_t1020 *, const MethodInfo*))TweenerCore_3_Validate_m23309_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23310_gshared (TweenerCore_3_t1020 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23310(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1020 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23310_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23311_gshared (TweenerCore_3_t1020 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23311(__this, method) (( bool (*) (TweenerCore_3_t1020 *, const MethodInfo*))TweenerCore_3_Startup_m23311_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23312_gshared (TweenerCore_3_t1020 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23312(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1020 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23312_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
