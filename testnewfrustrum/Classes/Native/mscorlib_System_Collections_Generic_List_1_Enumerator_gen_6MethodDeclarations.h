﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t816;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t715;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18954_gshared (Enumerator_t816 * __this, List_1_t715 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18954(__this, ___l, method) (( void (*) (Enumerator_t816 *, List_1_t715 *, const MethodInfo*))Enumerator__ctor_m18954_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18955_gshared (Enumerator_t816 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18955(__this, method) (( Object_t * (*) (Enumerator_t816 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18955_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m18956_gshared (Enumerator_t816 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18956(__this, method) (( void (*) (Enumerator_t816 *, const MethodInfo*))Enumerator_Dispose_m18956_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m18957_gshared (Enumerator_t816 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18957(__this, method) (( void (*) (Enumerator_t816 *, const MethodInfo*))Enumerator_VerifyState_m18957_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m4422_gshared (Enumerator_t816 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4422(__this, method) (( bool (*) (Enumerator_t816 *, const MethodInfo*))Enumerator_MoveNext_m4422_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m4420_gshared (Enumerator_t816 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4420(__this, method) (( int32_t (*) (Enumerator_t816 *, const MethodInfo*))Enumerator_get_Current_m4420_gshared)(__this, method)
