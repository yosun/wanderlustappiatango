﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3864;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3861;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26435_gshared (Enumerator_t3864 * __this, Dictionary_2_t3861 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m26435(__this, ___dictionary, method) (( void (*) (Enumerator_t3864 *, Dictionary_2_t3861 *, const MethodInfo*))Enumerator__ctor_m26435_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26436_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26436(__this, method) (( Object_t * (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26436_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26437_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26437(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26437_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26438_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26438(__this, method) (( Object_t * (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26438_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26439_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26439(__this, method) (( Object_t * (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26439_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26440_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26440(__this, method) (( bool (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_MoveNext_m26440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3836  Enumerator_get_Current_m26441_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26441(__this, method) (( KeyValuePair_2_t3836  (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_get_Current_m26441_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m26442_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m26442(__this, method) (( Object_t * (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_get_CurrentKey_m26442_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C" KeyValuePair_2_t3254  Enumerator_get_CurrentValue_m26443_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m26443(__this, method) (( KeyValuePair_2_t3254  (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_get_CurrentValue_m26443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C" void Enumerator_VerifyState_m26444_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26444(__this, method) (( void (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_VerifyState_m26444_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m26445_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m26445(__this, method) (( void (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_VerifyCurrent_m26445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m26446_gshared (Enumerator_t3864 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26446(__this, method) (( void (*) (Enumerator_t3864 *, const MethodInfo*))Enumerator_Dispose_m26446_gshared)(__this, method)
