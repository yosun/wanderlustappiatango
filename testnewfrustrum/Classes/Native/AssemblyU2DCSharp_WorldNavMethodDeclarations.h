﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldNav
struct WorldNav_t26;

// System.Void WorldNav::.ctor()
extern "C" void WorldNav__ctor_m99 (WorldNav_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::.cctor()
extern "C" void WorldNav__cctor_m100 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Awake()
extern "C" void WorldNav_Awake_m101 (WorldNav_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Update()
extern "C" void WorldNav_Update_m102 (WorldNav_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Walk(System.Int32)
extern "C" void WorldNav_Walk_m103 (Object_t * __this /* static, unused */, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern "C" void WorldNav_WalkDirTrue_m104 (WorldNav_t26 * __this, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern "C" void WorldNav_WalkDirFalse_m105 (WorldNav_t26 * __this, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
