﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>
struct Enumerator_t3672;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t934;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t946;

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23498(__this, ___l, method) (( void (*) (Enumerator_t3672 *, List_1_t946 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23499(__this, method) (( Object_t * (*) (Enumerator_t3672 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::Dispose()
#define Enumerator_Dispose_m23500(__this, method) (( void (*) (Enumerator_t3672 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::VerifyState()
#define Enumerator_VerifyState_m23501(__this, method) (( void (*) (Enumerator_t3672 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::MoveNext()
#define Enumerator_MoveNext_m23502(__this, method) (( bool (*) (Enumerator_t3672 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::get_Current()
#define Enumerator_get_Current_m23503(__this, method) (( Tween_t934 * (*) (Enumerator_t3672 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
