﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
struct Transform_1_t3602;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22601_gshared (Transform_1_t3602 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22601(__this, ___object, ___method, method) (( void (*) (Transform_1_t3602 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22601_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t1996  Transform_1_Invoke_m22602_gshared (Transform_1_t3602 * __this, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22602(__this, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Transform_1_t3602 *, Object_t *, ProfileData_t734 , const MethodInfo*))Transform_1_Invoke_m22602_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22603_gshared (Transform_1_t3602 * __this, Object_t * ___key, ProfileData_t734  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22603(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3602 *, Object_t *, ProfileData_t734 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22603_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t1996  Transform_1_EndInvoke_m22604_gshared (Transform_1_t3602 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22604(__this, ___result, method) (( DictionaryEntry_t1996  (*) (Transform_1_t3602 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22604_gshared)(__this, ___result, method)
