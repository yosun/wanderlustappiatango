﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t78;

// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m218 (TurnOffWordBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern "C" void TurnOffWordBehaviour_Awake_m219 (TurnOffWordBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
