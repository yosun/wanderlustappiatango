﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t996 
{
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t996_marshaled
{
	int32_t ___snapping_0;
};
