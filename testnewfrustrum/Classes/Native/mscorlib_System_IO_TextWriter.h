﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t110;
// System.IO.TextWriter
struct TextWriter_t2006;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.TextWriter
struct  TextWriter_t2006  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t110* ___CoreNewLine_0;
};
struct TextWriter_t2006_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t2006 * ___Null_1;
};
