﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t609;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t4149;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t4146;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t4141;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t3398;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3382;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3399;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3401;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_45.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
#define List_1__ctor_m4354(__this, method) (( void (*) (List_1_t609 *, const MethodInfo*))List_1__ctor_m4477_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18858(__this, ___collection, method) (( void (*) (List_1_t609 *, Object_t*, const MethodInfo*))List_1__ctor_m4418_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
#define List_1__ctor_m18859(__this, ___capacity, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1__ctor_m18860_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
#define List_1__cctor_m18861(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18862_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18863(__this, method) (( Object_t* (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18865(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t609 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18866_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18867(__this, method) (( Object_t * (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18868_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18869(__this, ___item, method) (( int32_t (*) (List_1_t609 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18870_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18871(__this, ___item, method) (( bool (*) (List_1_t609 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m18872_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18873(__this, ___item, method) (( int32_t (*) (List_1_t609 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m18874_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18875(__this, ___index, ___item, method) (( void (*) (List_1_t609 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m18876_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18877(__this, ___item, method) (( void (*) (List_1_t609 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m18878_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18879(__this, method) (( bool (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18881(__this, method) (( bool (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m18882_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18883(__this, method) (( Object_t * (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m18884_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18885(__this, method) (( bool (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m18886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18887(__this, method) (( bool (*) (List_1_t609 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m18888_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18889(__this, ___index, method) (( Object_t * (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m18890_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18891(__this, ___index, ___value, method) (( void (*) (List_1_t609 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m18892_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
#define List_1_Add_m18893(__this, ___item, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_Add_m18894_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18895(__this, ___newCount, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m18896_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18897(__this, ___collection, method) (( void (*) (List_1_t609 *, Object_t*, const MethodInfo*))List_1_AddCollection_m18898_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18899(__this, ___enumerable, method) (( void (*) (List_1_t609 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m18900_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18901(__this, ___collection, method) (( void (*) (List_1_t609 *, Object_t*, const MethodInfo*))List_1_AddRange_m18902_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
#define List_1_AsReadOnly_m18903(__this, method) (( ReadOnlyCollection_1_t3398 * (*) (List_1_t609 *, const MethodInfo*))List_1_AsReadOnly_m18904_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
#define List_1_Clear_m18905(__this, method) (( void (*) (List_1_t609 *, const MethodInfo*))List_1_Clear_m18906_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
#define List_1_Contains_m18907(__this, ___item, method) (( bool (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_Contains_m18908_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18909(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t609 *, PIXEL_FORMATU5BU5D_t3382*, int32_t, const MethodInfo*))List_1_CopyTo_m18910_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
#define List_1_Find_m18911(__this, ___match, method) (( int32_t (*) (List_1_t609 *, Predicate_1_t3399 *, const MethodInfo*))List_1_Find_m18912_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18913(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3399 *, const MethodInfo*))List_1_CheckMatch_m18914_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18915(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t609 *, int32_t, int32_t, Predicate_1_t3399 *, const MethodInfo*))List_1_GetIndex_m18916_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
#define List_1_GetEnumerator_m18917(__this, method) (( Enumerator_t3400  (*) (List_1_t609 *, const MethodInfo*))List_1_GetEnumerator_m4419_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
#define List_1_IndexOf_m18918(__this, ___item, method) (( int32_t (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_IndexOf_m18919_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18920(__this, ___start, ___delta, method) (( void (*) (List_1_t609 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m18921_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18922(__this, ___index, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_CheckIndex_m18923_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
#define List_1_Insert_m18924(__this, ___index, ___item, method) (( void (*) (List_1_t609 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m18925_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18926(__this, ___collection, method) (( void (*) (List_1_t609 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m18927_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
#define List_1_Remove_m18928(__this, ___item, method) (( bool (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_Remove_m18929_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18930(__this, ___match, method) (( int32_t (*) (List_1_t609 *, Predicate_1_t3399 *, const MethodInfo*))List_1_RemoveAll_m18931_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18932(__this, ___index, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_RemoveAt_m18933_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
#define List_1_Reverse_m18934(__this, method) (( void (*) (List_1_t609 *, const MethodInfo*))List_1_Reverse_m18935_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
#define List_1_Sort_m18936(__this, method) (( void (*) (List_1_t609 *, const MethodInfo*))List_1_Sort_m18937_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18938(__this, ___comparison, method) (( void (*) (List_1_t609 *, Comparison_1_t3401 *, const MethodInfo*))List_1_Sort_m18939_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
#define List_1_ToArray_m18940(__this, method) (( PIXEL_FORMATU5BU5D_t3382* (*) (List_1_t609 *, const MethodInfo*))List_1_ToArray_m18941_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
#define List_1_TrimExcess_m18942(__this, method) (( void (*) (List_1_t609 *, const MethodInfo*))List_1_TrimExcess_m18943_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
#define List_1_get_Capacity_m18944(__this, method) (( int32_t (*) (List_1_t609 *, const MethodInfo*))List_1_get_Capacity_m18945_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18946(__this, ___value, method) (( void (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_set_Capacity_m18947_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
#define List_1_get_Count_m18948(__this, method) (( int32_t (*) (List_1_t609 *, const MethodInfo*))List_1_get_Count_m18949_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
#define List_1_get_Item_m18950(__this, ___index, method) (( int32_t (*) (List_1_t609 *, int32_t, const MethodInfo*))List_1_get_Item_m18951_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
#define List_1_set_Item_m18952(__this, ___index, ___value, method) (( void (*) (List_1_t609 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m18953_gshared)(__this, ___index, ___value, method)
