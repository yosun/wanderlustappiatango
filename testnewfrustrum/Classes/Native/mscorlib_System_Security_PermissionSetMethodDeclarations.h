﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.PermissionSet
struct PermissionSet_t2236;

// System.Void System.Security.PermissionSet::.ctor()
extern "C" void PermissionSet__ctor_m12832 (PermissionSet_t2236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
