﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t621;
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl.h"
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// Vuforia.ImageTargetImpl
struct  ImageTargetImpl_t622  : public ObjectTargetImpl_t579
{
	// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::mImageTargetType
	int32_t ___mImageTargetType_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton> Vuforia.ImageTargetImpl::mVirtualButtons
	Dictionary_2_t621 * ___mVirtualButtons_5;
};
