﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t52;

// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m174 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern "C" void ComponentFactoryStarterBehaviour_Awake_m175 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176 (ComponentFactoryStarterBehaviour_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
