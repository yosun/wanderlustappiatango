﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct InternalEnumerator_1_t3156;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15597_gshared (InternalEnumerator_1_t3156 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15597(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3156 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15597_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15598_gshared (InternalEnumerator_1_t3156 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15598(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3156 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15598_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15599_gshared (InternalEnumerator_1_t3156 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15599(__this, method) (( void (*) (InternalEnumerator_1_t3156 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15599_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15600_gshared (InternalEnumerator_1_t3156 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15600(__this, method) (( bool (*) (InternalEnumerator_1_t3156 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15600_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t231  InternalEnumerator_1_get_Current_m15601_gshared (InternalEnumerator_1_t3156 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15601(__this, method) (( RaycastResult_t231  (*) (InternalEnumerator_1_t3156 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15601_gshared)(__this, method)
