﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t3614;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22605_gshared (Transform_1_t3614 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22605(__this, ___object, ___method, method) (( void (*) (Transform_1_t3614 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22605_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3604  Transform_1_Invoke_m22606_gshared (Transform_1_t3614 * __this, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22606(__this, ___key, ___value, method) (( KeyValuePair_2_t3604  (*) (Transform_1_t3614 *, Object_t *, ProfileData_t734 , const MethodInfo*))Transform_1_Invoke_m22606_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22607_gshared (Transform_1_t3614 * __this, Object_t * ___key, ProfileData_t734  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22607(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3614 *, Object_t *, ProfileData_t734 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22607_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3604  Transform_1_EndInvoke_m22608_gshared (Transform_1_t3614 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22608(__this, ___result, method) (( KeyValuePair_2_t3604  (*) (Transform_1_t3614 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22608_gshared)(__this, ___result, method)
