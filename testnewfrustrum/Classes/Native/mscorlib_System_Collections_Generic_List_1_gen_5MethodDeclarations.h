﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t224;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t11;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
struct IEnumerable_1_t4045;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
struct IEnumerator_1_t4046;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<UnityEngine.Transform>
struct ICollection_1_t424;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Transform>
struct ReadOnlyCollection_1_t3182;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3181;
// System.Predicate`1<UnityEngine.Transform>
struct Predicate_1_t3183;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t3185;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_32.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m15941(__this, method) (( void (*) (List_1_t224 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15942(__this, ___collection, method) (( void (*) (List_1_t224 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor(System.Int32)
#define List_1__ctor_m1982(__this, ___capacity, method) (( void (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.cctor()
#define List_1__cctor_m15943(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15944(__this, method) (( Object_t* (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15945(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t224 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15946(__this, method) (( Object_t * (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15947(__this, ___item, method) (( int32_t (*) (List_1_t224 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15948(__this, ___item, method) (( bool (*) (List_1_t224 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15949(__this, ___item, method) (( int32_t (*) (List_1_t224 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15950(__this, ___index, ___item, method) (( void (*) (List_1_t224 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15951(__this, ___item, method) (( void (*) (List_1_t224 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15952(__this, method) (( bool (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15953(__this, method) (( bool (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15954(__this, method) (( Object_t * (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15955(__this, method) (( bool (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15956(__this, method) (( bool (*) (List_1_t224 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15957(__this, ___index, method) (( Object_t * (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15958(__this, ___index, ___value, method) (( void (*) (List_1_t224 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(T)
#define List_1_Add_m15959(__this, ___item, method) (( void (*) (List_1_t224 *, Transform_t11 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15960(__this, ___newCount, method) (( void (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15961(__this, ___collection, method) (( void (*) (List_1_t224 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15962(__this, ___enumerable, method) (( void (*) (List_1_t224 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15963(__this, ___collection, method) (( void (*) (List_1_t224 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Transform>::AsReadOnly()
#define List_1_AsReadOnly_m15964(__this, method) (( ReadOnlyCollection_1_t3182 * (*) (List_1_t224 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear()
#define List_1_Clear_m15965(__this, method) (( void (*) (List_1_t224 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Contains(T)
#define List_1_Contains_m15966(__this, ___item, method) (( bool (*) (List_1_t224 *, Transform_t11 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15967(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t224 *, TransformU5BU5D_t3181*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Transform>::Find(System.Predicate`1<T>)
#define List_1_Find_m15968(__this, ___match, method) (( Transform_t11 * (*) (List_1_t224 *, Predicate_1_t3183 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15969(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3183 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15970(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t224 *, int32_t, int32_t, Predicate_1_t3183 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Transform>::GetEnumerator()
#define List_1_GetEnumerator_m15971(__this, method) (( Enumerator_t3184  (*) (List_1_t224 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::IndexOf(T)
#define List_1_IndexOf_m15972(__this, ___item, method) (( int32_t (*) (List_1_t224 *, Transform_t11 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15973(__this, ___start, ___delta, method) (( void (*) (List_1_t224 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15974(__this, ___index, method) (( void (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Insert(System.Int32,T)
#define List_1_Insert_m15975(__this, ___index, ___item, method) (( void (*) (List_1_t224 *, int32_t, Transform_t11 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15976(__this, ___collection, method) (( void (*) (List_1_t224 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(T)
#define List_1_Remove_m15977(__this, ___item, method) (( bool (*) (List_1_t224 *, Transform_t11 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15978(__this, ___match, method) (( int32_t (*) (List_1_t224 *, Predicate_1_t3183 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15979(__this, ___index, method) (( void (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Reverse()
#define List_1_Reverse_m15980(__this, method) (( void (*) (List_1_t224 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Sort()
#define List_1_Sort_m15981(__this, method) (( void (*) (List_1_t224 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15982(__this, ___comparison, method) (( void (*) (List_1_t224 *, Comparison_1_t3185 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Transform>::ToArray()
#define List_1_ToArray_m15983(__this, method) (( TransformU5BU5D_t3181* (*) (List_1_t224 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::TrimExcess()
#define List_1_TrimExcess_m15984(__this, method) (( void (*) (List_1_t224 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Capacity()
#define List_1_get_Capacity_m15985(__this, method) (( int32_t (*) (List_1_t224 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15986(__this, ___value, method) (( void (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m15987(__this, method) (( int32_t (*) (List_1_t224 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m15988(__this, ___index, method) (( Transform_t11 * (*) (List_1_t224 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::set_Item(System.Int32,T)
#define List_1_set_Item_m15989(__this, ___index, ___value, method) (( void (*) (List_1_t224 *, int32_t, Transform_t11 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
