﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyValuePair_2_t3502;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t692;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20777(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3502 *, String_t*, List_1_t692 *, const MethodInfo*))KeyValuePair_2__ctor_m16908_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Key()
#define KeyValuePair_2_get_Key_m20778(__this, method) (( String_t* (*) (KeyValuePair_2_t3502 *, const MethodInfo*))KeyValuePair_2_get_Key_m16909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20779(__this, ___value, method) (( void (*) (KeyValuePair_2_t3502 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m16910_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Value()
#define KeyValuePair_2_get_Value_m20780(__this, method) (( List_1_t692 * (*) (KeyValuePair_2_t3502 *, const MethodInfo*))KeyValuePair_2_get_Value_m16911_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20781(__this, ___value, method) (( void (*) (KeyValuePair_2_t3502 *, List_1_t692 *, const MethodInfo*))KeyValuePair_2_set_Value_m16912_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToString()
#define KeyValuePair_2_ToString_m20782(__this, method) (( String_t* (*) (KeyValuePair_2_t3502 *, const MethodInfo*))KeyValuePair_2_ToString_m16913_gshared)(__this, method)
