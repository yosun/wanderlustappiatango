﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t1051;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23977_gshared (DOSetter_1_t1051 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23977(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1051 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23977_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23978_gshared (DOSetter_1_t1051 * __this, Color_t90  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23978(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1051 *, Color_t90 , const MethodInfo*))DOSetter_1_Invoke_m23978_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23979_gshared (DOSetter_1_t1051 * __this, Color_t90  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23979(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1051 *, Color_t90 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23979_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23980_gshared (DOSetter_1_t1051 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23980(__this, ___result, method) (( void (*) (DOSetter_1_t1051 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23980_gshared)(__this, ___result, method)
