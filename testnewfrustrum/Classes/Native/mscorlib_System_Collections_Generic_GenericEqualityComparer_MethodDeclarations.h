﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t2619;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14006_gshared (GenericEqualityComparer_1_t2619 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14006(__this, method) (( void (*) (GenericEqualityComparer_1_t2619 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14006_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m27671_gshared (GenericEqualityComparer_1_t2619 * __this, DateTime_t111  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m27671(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2619 *, DateTime_t111 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m27671_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m27672_gshared (GenericEqualityComparer_1_t2619 * __this, DateTime_t111  ___x, DateTime_t111  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m27672(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2619 *, DateTime_t111 , DateTime_t111 , const MethodInfo*))GenericEqualityComparer_1_Equals_m27672_gshared)(__this, ___x, ___y, method)
