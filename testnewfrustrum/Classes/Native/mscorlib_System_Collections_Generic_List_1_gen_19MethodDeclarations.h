﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t575;
// System.Object
struct Object_t;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t761;
// System.Collections.Generic.IEnumerable`1<Vuforia.ICloudRecoEventHandler>
struct IEnumerable_1_t4133;
// System.Collections.Generic.IEnumerator`1<Vuforia.ICloudRecoEventHandler>
struct IEnumerator_1_t4134;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ICloudRecoEventHandler>
struct ICollection_1_t4135;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>
struct ReadOnlyCollection_1_t3364;
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3362;
// System.Predicate`1<Vuforia.ICloudRecoEventHandler>
struct Predicate_1_t3365;
// System.Comparison`1<Vuforia.ICloudRecoEventHandler>
struct Comparison_1_t3366;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4320(__this, method) (( void (*) (List_1_t575 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18393(__this, ___collection, method) (( void (*) (List_1_t575 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m18394(__this, ___capacity, method) (( void (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::.cctor()
#define List_1__cctor_m18395(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18396(__this, method) (( Object_t* (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18397(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t575 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18398(__this, method) (( Object_t * (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18399(__this, ___item, method) (( int32_t (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18400(__this, ___item, method) (( bool (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18401(__this, ___item, method) (( int32_t (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18402(__this, ___index, ___item, method) (( void (*) (List_1_t575 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18403(__this, ___item, method) (( void (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18404(__this, method) (( bool (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18405(__this, method) (( bool (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18406(__this, method) (( Object_t * (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18407(__this, method) (( bool (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18408(__this, method) (( bool (*) (List_1_t575 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18409(__this, ___index, method) (( Object_t * (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18410(__this, ___index, ___value, method) (( void (*) (List_1_t575 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Add(T)
#define List_1_Add_m18411(__this, ___item, method) (( void (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18412(__this, ___newCount, method) (( void (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18413(__this, ___collection, method) (( void (*) (List_1_t575 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18414(__this, ___enumerable, method) (( void (*) (List_1_t575 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18415(__this, ___collection, method) (( void (*) (List_1_t575 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m18416(__this, method) (( ReadOnlyCollection_1_t3364 * (*) (List_1_t575 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Clear()
#define List_1_Clear_m18417(__this, method) (( void (*) (List_1_t575 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Contains(T)
#define List_1_Contains_m18418(__this, ___item, method) (( bool (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18419(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t575 *, ICloudRecoEventHandlerU5BU5D_t3362*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m18420(__this, ___match, method) (( Object_t * (*) (List_1_t575 *, Predicate_1_t3365 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18421(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3365 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18422(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t575 *, int32_t, int32_t, Predicate_1_t3365 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4316(__this, method) (( Enumerator_t795  (*) (List_1_t575 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::IndexOf(T)
#define List_1_IndexOf_m18423(__this, ___item, method) (( int32_t (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18424(__this, ___start, ___delta, method) (( void (*) (List_1_t575 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18425(__this, ___index, method) (( void (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m18426(__this, ___index, ___item, method) (( void (*) (List_1_t575 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18427(__this, ___collection, method) (( void (*) (List_1_t575 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Remove(T)
#define List_1_Remove_m18428(__this, ___item, method) (( bool (*) (List_1_t575 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18429(__this, ___match, method) (( int32_t (*) (List_1_t575 *, Predicate_1_t3365 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18430(__this, ___index, method) (( void (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Reverse()
#define List_1_Reverse_m18431(__this, method) (( void (*) (List_1_t575 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Sort()
#define List_1_Sort_m18432(__this, method) (( void (*) (List_1_t575 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18433(__this, ___comparison, method) (( void (*) (List_1_t575 *, Comparison_1_t3366 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::ToArray()
#define List_1_ToArray_m18434(__this, method) (( ICloudRecoEventHandlerU5BU5D_t3362* (*) (List_1_t575 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::TrimExcess()
#define List_1_TrimExcess_m18435(__this, method) (( void (*) (List_1_t575 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Capacity()
#define List_1_get_Capacity_m18436(__this, method) (( int32_t (*) (List_1_t575 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18437(__this, ___value, method) (( void (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Count()
#define List_1_get_Count_m18438(__this, method) (( int32_t (*) (List_1_t575 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m18439(__this, ___index, method) (( Object_t * (*) (List_1_t575 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m18440(__this, ___index, ___value, method) (( void (*) (List_1_t575 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
