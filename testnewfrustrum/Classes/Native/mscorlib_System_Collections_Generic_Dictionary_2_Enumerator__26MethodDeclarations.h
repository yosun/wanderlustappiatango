﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3609;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3603;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22562_gshared (Enumerator_t3609 * __this, Dictionary_2_t3603 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22562(__this, ___dictionary, method) (( void (*) (Enumerator_t3609 *, Dictionary_2_t3603 *, const MethodInfo*))Enumerator__ctor_m22562_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22563_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22563(__this, method) (( Object_t * (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22563_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565(__this, method) (( Object_t * (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566(__this, method) (( Object_t * (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22567_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22567(__this, method) (( bool (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_MoveNext_m22567_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" KeyValuePair_2_t3604  Enumerator_get_Current_m22568_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22568(__this, method) (( KeyValuePair_2_t3604  (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_get_Current_m22568_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m22569_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22569(__this, method) (( Object_t * (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_get_CurrentKey_m22569_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C" ProfileData_t734  Enumerator_get_CurrentValue_m22570_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22570(__this, method) (( ProfileData_t734  (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_get_CurrentValue_m22570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyState()
extern "C" void Enumerator_VerifyState_m22571_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22571(__this, method) (( void (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_VerifyState_m22571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22572_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22572(__this, method) (( void (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_VerifyCurrent_m22572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22573_gshared (Enumerator_t3609 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22573(__this, method) (( void (*) (Enumerator_t3609 *, const MethodInfo*))Enumerator_Dispose_m22573_gshared)(__this, method)
