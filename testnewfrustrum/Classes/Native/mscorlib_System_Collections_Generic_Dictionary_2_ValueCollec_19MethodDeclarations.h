﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>
struct ValueCollection_t865;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t714;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
struct IEnumerator_1_t888;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t818;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_20.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"
#define ValueCollection__ctor_m21757(__this, ___dictionary, method) (( void (*) (ValueCollection_t865 *, Dictionary_2_t714 *, const MethodInfo*))ValueCollection__ctor_m16275_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21758(__this, ___item, method) (( void (*) (ValueCollection_t865 *, TrackableBehaviour_t44 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16276_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21759(__this, method) (( void (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16277_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21760(__this, ___item, method) (( bool (*) (ValueCollection_t865 *, TrackableBehaviour_t44 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16278_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21761(__this, ___item, method) (( bool (*) (ValueCollection_t865 *, TrackableBehaviour_t44 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16279_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21762(__this, method) (( Object_t* (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m21763(__this, ___array, ___index, method) (( void (*) (ValueCollection_t865 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16281_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21764(__this, method) (( Object_t * (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21765(__this, method) (( bool (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21766(__this, method) (( bool (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16284_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m21767(__this, method) (( Object_t * (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16285_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m21768(__this, ___array, ___index, method) (( void (*) (ValueCollection_t865 *, TrackableBehaviourU5BU5D_t818*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16286_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4564(__this, method) (( Enumerator_t872  (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_GetEnumerator_m16287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour>::get_Count()
#define ValueCollection_get_Count_m21769(__this, method) (( int32_t (*) (ValueCollection_t865 *, const MethodInfo*))ValueCollection_get_Count_m16288_gshared)(__this, method)
