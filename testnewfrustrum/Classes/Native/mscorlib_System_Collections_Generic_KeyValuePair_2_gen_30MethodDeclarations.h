﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct KeyValuePair_2_t3623;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t86;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22725(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3623 *, int32_t, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))KeyValuePair_2__ctor_m16229_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m22726(__this, method) (( int32_t (*) (KeyValuePair_2_t3623 *, const MethodInfo*))KeyValuePair_2_get_Key_m16230_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22727(__this, ___value, method) (( void (*) (KeyValuePair_2_t3623 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16231_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m22728(__this, method) (( VirtualButtonAbstractBehaviour_t86 * (*) (KeyValuePair_2_t3623 *, const MethodInfo*))KeyValuePair_2_get_Value_m16232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22729(__this, ___value, method) (( void (*) (KeyValuePair_2_t3623 *, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))KeyValuePair_2_set_Value_m16233_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m22730(__this, method) (( String_t* (*) (KeyValuePair_2_t3623 *, const MethodInfo*))KeyValuePair_2_ToString_m16234_gshared)(__this, method)
