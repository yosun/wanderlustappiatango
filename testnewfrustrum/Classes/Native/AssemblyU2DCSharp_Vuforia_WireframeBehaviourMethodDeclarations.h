﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t89;

// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m225 (WireframeBehaviour_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern "C" void WireframeBehaviour_CreateLineMaterial_m226 (WireframeBehaviour_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern "C" void WireframeBehaviour_OnRenderObject_m227 (WireframeBehaviour_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern "C" void WireframeBehaviour_OnDrawGizmos_m228 (WireframeBehaviour_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
