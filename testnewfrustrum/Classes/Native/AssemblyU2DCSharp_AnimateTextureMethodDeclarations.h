﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AnimateTexture
struct AnimateTexture_t9;

// System.Void AnimateTexture::.ctor()
extern "C" void AnimateTexture__ctor_m10 (AnimateTexture_t9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Awake()
extern "C" void AnimateTexture_Awake_m11 (AnimateTexture_t9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Update()
extern "C" void AnimateTexture_Update_m12 (AnimateTexture_t9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
