﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t97;
// UnityEngine.BoxCollider
struct BoxCollider_t712;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t65  : public SmartTerrainTrackableBehaviour_t591
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t712 * ___mBoxColliderToUpdate_14;
};
