﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t7;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct  TweenRunner_1_t276  : public Object_t
{
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::m_CoroutineContainer
	MonoBehaviour_t7 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::m_Tween
	Object_t * ___m_Tween_1;
};
