﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyValuePair_2_t3843;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t1286;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26060(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3843 *, Type_t *, ConstructorDelegate_t1286 *, const MethodInfo*))KeyValuePair_2__ctor_m16908_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m26061(__this, method) (( Type_t * (*) (KeyValuePair_2_t3843 *, const MethodInfo*))KeyValuePair_2_get_Key_m16909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26062(__this, ___value, method) (( void (*) (KeyValuePair_2_t3843 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m16910_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m26063(__this, method) (( ConstructorDelegate_t1286 * (*) (KeyValuePair_2_t3843 *, const MethodInfo*))KeyValuePair_2_get_Value_m16911_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26064(__this, ___value, method) (( void (*) (KeyValuePair_2_t3843 *, ConstructorDelegate_t1286 *, const MethodInfo*))KeyValuePair_2_set_Value_m16912_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToString()
#define KeyValuePair_2_ToString_m26065(__this, method) (( String_t* (*) (KeyValuePair_2_t3843 *, const MethodInfo*))KeyValuePair_2_ToString_m16913_gshared)(__this, method)
