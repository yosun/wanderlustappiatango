﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t3258;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3253;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16933_gshared (Enumerator_t3258 * __this, Dictionary_2_t3253 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16933(__this, ___dictionary, method) (( void (*) (Enumerator_t3258 *, Dictionary_2_t3253 *, const MethodInfo*))Enumerator__ctor_m16933_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16934(__this, method) (( Object_t * (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936(__this, method) (( Object_t * (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937(__this, method) (( Object_t * (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16938_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16938(__this, method) (( bool (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_MoveNext_m16938_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3254  Enumerator_get_Current_m16939_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16939(__this, method) (( KeyValuePair_2_t3254  (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_get_Current_m16939_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m16940_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16940(__this, method) (( Object_t * (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_get_CurrentKey_m16940_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m16941_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16941(__this, method) (( Object_t * (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_get_CurrentValue_m16941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m16942_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16942(__this, method) (( void (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_VerifyState_m16942_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16943_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16943(__this, method) (( void (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_VerifyCurrent_m16943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16944_gshared (Enumerator_t3258 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16944(__this, method) (( void (*) (Enumerator_t3258 *, const MethodInfo*))Enumerator_Dispose_m16944_gshared)(__this, method)
