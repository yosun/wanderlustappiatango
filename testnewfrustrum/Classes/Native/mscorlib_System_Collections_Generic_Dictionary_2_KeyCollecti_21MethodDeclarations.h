﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>
struct Enumerator_t3465;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3461;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20145_gshared (Enumerator_t3465 * __this, Dictionary_2_t3461 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20145(__this, ___host, method) (( void (*) (Enumerator_t3465 *, Dictionary_2_t3461 *, const MethodInfo*))Enumerator__ctor_m20145_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20146_gshared (Enumerator_t3465 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20146(__this, method) (( Object_t * (*) (Enumerator_t3465 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m20147_gshared (Enumerator_t3465 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20147(__this, method) (( void (*) (Enumerator_t3465 *, const MethodInfo*))Enumerator_Dispose_m20147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20148_gshared (Enumerator_t3465 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20148(__this, method) (( bool (*) (Enumerator_t3465 *, const MethodInfo*))Enumerator_MoveNext_m20148_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20149_gshared (Enumerator_t3465 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20149(__this, method) (( Object_t * (*) (Enumerator_t3465 *, const MethodInfo*))Enumerator_get_Current_m20149_gshared)(__this, method)
