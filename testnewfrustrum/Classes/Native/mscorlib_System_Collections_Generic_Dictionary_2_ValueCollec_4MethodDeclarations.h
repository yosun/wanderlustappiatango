﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
struct Enumerator_t811;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t739;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t630;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m19672(__this, ___host, method) (( void (*) (Enumerator_t811 *, Dictionary_2_t630 *, const MethodInfo*))Enumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19673(__this, method) (( Object_t * (*) (Enumerator_t811 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m19674(__this, method) (( void (*) (Enumerator_t811 *, const MethodInfo*))Enumerator_Dispose_m16291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m4393(__this, method) (( bool (*) (Enumerator_t811 *, const MethodInfo*))Enumerator_MoveNext_m16292_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m4392(__this, method) (( Object_t * (*) (Enumerator_t811 *, const MethodInfo*))Enumerator_get_Current_m16293_gshared)(__this, method)
