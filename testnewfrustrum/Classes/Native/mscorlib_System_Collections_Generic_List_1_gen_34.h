﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t818;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct  List_1_t716  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_items
	TrackableBehaviourU5BU5D_t818* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t716_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::EmptyArray
	TrackableBehaviourU5BU5D_t818* ___EmptyArray_4;
};
