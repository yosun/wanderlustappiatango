﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t3765;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1246;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m25052_gshared (Enumerator_t3765 * __this, List_1_t1246 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m25052(__this, ___l, method) (( void (*) (Enumerator_t3765 *, List_1_t1246 *, const MethodInfo*))Enumerator__ctor_m25052_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25053_gshared (Enumerator_t3765 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25053(__this, method) (( Object_t * (*) (Enumerator_t3765 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25053_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m25054_gshared (Enumerator_t3765 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25054(__this, method) (( void (*) (Enumerator_t3765 *, const MethodInfo*))Enumerator_Dispose_m25054_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m25055_gshared (Enumerator_t3765 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25055(__this, method) (( void (*) (Enumerator_t3765 *, const MethodInfo*))Enumerator_VerifyState_m25055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25056_gshared (Enumerator_t3765 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25056(__this, method) (( bool (*) (Enumerator_t3765 *, const MethodInfo*))Enumerator_MoveNext_m25056_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t458  Enumerator_get_Current_m25057_gshared (Enumerator_t3765 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25057(__this, method) (( UILineInfo_t458  (*) (Enumerator_t3765 *, const MethodInfo*))Enumerator_get_Current_m25057_gshared)(__this, method)
