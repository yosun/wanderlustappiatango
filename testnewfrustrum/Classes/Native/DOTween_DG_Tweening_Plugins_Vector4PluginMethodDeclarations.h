﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Vector4Plugin
struct Vector4Plugin_t993;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1046;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t1047;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t1048;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.Vector4Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_Reset_m5470 (Vector4Plugin_t993 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 DG.Tweening.Plugins.Vector4Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector4)
extern "C" Vector4_t413  Vector4Plugin_ConvertToStartValue_m5471 (Vector4Plugin_t993 * __this, TweenerCore_3_t1046 * ___t, Vector4_t413  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_SetRelativeEndValue_m5472 (Vector4Plugin_t993 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_SetChangeValue_m5473 (Vector4Plugin_t993 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.Vector4Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector4)
extern "C" float Vector4Plugin_GetSpeedBasedDuration_m5474 (Vector4Plugin_t993 * __this, VectorOptions_t1002  ___options, float ___unitsXSecond, Vector4_t413  ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector4Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,System.Single,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector4Plugin_EvaluateAndApply_m5475 (Vector4Plugin_t993 * __this, VectorOptions_t1002  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1047 * ___getter, DOSetter_1_t1048 * ___setter, float ___elapsed, Vector4_t413  ___startValue, Vector4_t413  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector4Plugin::.ctor()
extern "C" void Vector4Plugin__ctor_m5476 (Vector4Plugin_t993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
