﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptor
struct WebCamTexAdaptor_t633;
// UnityEngine.Texture
struct Texture_t321;

// System.Boolean Vuforia.WebCamTexAdaptor::get_DidUpdateThisFrame()
// System.Boolean Vuforia.WebCamTexAdaptor::get_IsPlaying()
// UnityEngine.Texture Vuforia.WebCamTexAdaptor::get_Texture()
// System.Void Vuforia.WebCamTexAdaptor::Play()
// System.Void Vuforia.WebCamTexAdaptor::Stop()
// System.Void Vuforia.WebCamTexAdaptor::.ctor()
extern "C" void WebCamTexAdaptor__ctor_m2983 (WebCamTexAdaptor_t633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
