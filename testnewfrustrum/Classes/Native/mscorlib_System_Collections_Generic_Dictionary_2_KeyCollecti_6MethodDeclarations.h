﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t3201;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3196;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16254_gshared (Enumerator_t3201 * __this, Dictionary_2_t3196 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16254(__this, ___host, method) (( void (*) (Enumerator_t3201 *, Dictionary_2_t3196 *, const MethodInfo*))Enumerator__ctor_m16254_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16255_gshared (Enumerator_t3201 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16255(__this, method) (( Object_t * (*) (Enumerator_t3201 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16255_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16256_gshared (Enumerator_t3201 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16256(__this, method) (( void (*) (Enumerator_t3201 *, const MethodInfo*))Enumerator_Dispose_m16256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16257_gshared (Enumerator_t3201 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16257(__this, method) (( bool (*) (Enumerator_t3201 *, const MethodInfo*))Enumerator_MoveNext_m16257_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m16258_gshared (Enumerator_t3201 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16258(__this, method) (( int32_t (*) (Enumerator_t3201 *, const MethodInfo*))Enumerator_get_Current_m16258_gshared)(__this, method)
