﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct KeyValuePair_2_t3197;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m16229_gshared (KeyValuePair_2_t3197 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m16229(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m16229_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m16230_gshared (KeyValuePair_2_t3197 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m16230(__this, method) (( int32_t (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_get_Key_m16230_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m16231_gshared (KeyValuePair_2_t3197 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m16231(__this, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16231_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m16232_gshared (KeyValuePair_2_t3197 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m16232(__this, method) (( Object_t * (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_get_Value_m16232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m16233_gshared (KeyValuePair_2_t3197 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m16233(__this, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m16233_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m16234_gshared (KeyValuePair_2_t3197 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m16234(__this, method) (( String_t* (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_ToString_m16234_gshared)(__this, method)
