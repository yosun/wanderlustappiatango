﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamDevice
struct WebCamDevice_t879;
struct WebCamDevice_t879_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m4603 (WebCamDevice_t879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m6377 (WebCamDevice_t879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WebCamDevice_t879_marshal(const WebCamDevice_t879& unmarshaled, WebCamDevice_t879_marshaled& marshaled);
void WebCamDevice_t879_marshal_back(const WebCamDevice_t879_marshaled& marshaled, WebCamDevice_t879& unmarshaled);
void WebCamDevice_t879_marshal_cleanup(WebCamDevice_t879_marshaled& marshaled);
