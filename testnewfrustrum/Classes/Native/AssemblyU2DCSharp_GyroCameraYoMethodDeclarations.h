﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GyroCameraYo
struct GyroCameraYo_t14;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void GyroCameraYo::.ctor()
extern "C" void GyroCameraYo__ctor_m31 (GyroCameraYo_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Start()
extern "C" void GyroCameraYo_Start_m32 (GyroCameraYo_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Update()
extern "C" void GyroCameraYo_Update_m33 (GyroCameraYo_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t13  GyroCameraYo_ConvertRotation_m34 (Object_t * __this /* static, unused */, Quaternion_t13  ___q, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::GyroCam()
extern "C" void GyroCameraYo_GyroCam_m35 (GyroCameraYo_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
