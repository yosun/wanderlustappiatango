﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t988;
// DG.Tweening.TweenCallback
struct TweenCallback_t101;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>
struct  Enumerator_t3667 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::l
	List_1_t988 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::current
	TweenCallback_t101 * ___current_3;
};
