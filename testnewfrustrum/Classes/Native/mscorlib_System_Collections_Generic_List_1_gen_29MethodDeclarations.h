﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t671;
// System.Object
struct Object_t;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t68;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerable_1_t762;
// System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerator_1_t797;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ICollection_1_t4172;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ReadOnlyCollection_1_t3454;
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t824;
// System.Predicate`1<Vuforia.ReconstructionAbstractBehaviour>
struct Predicate_1_t3455;
// System.Comparison`1<Vuforia.ReconstructionAbstractBehaviour>
struct Comparison_1_t3456;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4443(__this, method) (( void (*) (List_1_t671 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19927(__this, ___collection, method) (( void (*) (List_1_t671 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m19928(__this, ___capacity, method) (( void (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.cctor()
#define List_1__cctor_m19929(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19930(__this, method) (( Object_t* (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19931(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t671 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19932(__this, method) (( Object_t * (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19933(__this, ___item, method) (( int32_t (*) (List_1_t671 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19934(__this, ___item, method) (( bool (*) (List_1_t671 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19935(__this, ___item, method) (( int32_t (*) (List_1_t671 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19936(__this, ___index, ___item, method) (( void (*) (List_1_t671 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19937(__this, ___item, method) (( void (*) (List_1_t671 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19938(__this, method) (( bool (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19939(__this, method) (( bool (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19940(__this, method) (( Object_t * (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19941(__this, method) (( bool (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19942(__this, method) (( bool (*) (List_1_t671 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19943(__this, ___index, method) (( Object_t * (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19944(__this, ___index, ___value, method) (( void (*) (List_1_t671 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
#define List_1_Add_m19945(__this, ___item, method) (( void (*) (List_1_t671 *, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19946(__this, ___newCount, method) (( void (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19947(__this, ___collection, method) (( void (*) (List_1_t671 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19948(__this, ___enumerable, method) (( void (*) (List_1_t671 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19949(__this, ___collection, method) (( void (*) (List_1_t671 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m19950(__this, method) (( ReadOnlyCollection_1_t3454 * (*) (List_1_t671 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
#define List_1_Clear_m19951(__this, method) (( void (*) (List_1_t671 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
#define List_1_Contains_m19952(__this, ___item, method) (( bool (*) (List_1_t671 *, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19953(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t671 *, ReconstructionAbstractBehaviourU5BU5D_t824*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m19954(__this, ___match, method) (( ReconstructionAbstractBehaviour_t68 * (*) (List_1_t671 *, Predicate_1_t3455 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19955(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3455 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19956(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t671 *, int32_t, int32_t, Predicate_1_t3455 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m4440(__this, method) (( Enumerator_t825  (*) (List_1_t671 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m19957(__this, ___item, method) (( int32_t (*) (List_1_t671 *, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19958(__this, ___start, ___delta, method) (( void (*) (List_1_t671 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19959(__this, ___index, method) (( void (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m19960(__this, ___index, ___item, method) (( void (*) (List_1_t671 *, int32_t, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19961(__this, ___collection, method) (( void (*) (List_1_t671 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
#define List_1_Remove_m19962(__this, ___item, method) (( bool (*) (List_1_t671 *, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19963(__this, ___match, method) (( int32_t (*) (List_1_t671 *, Predicate_1_t3455 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19964(__this, ___index, method) (( void (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Reverse()
#define List_1_Reverse_m19965(__this, method) (( void (*) (List_1_t671 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort()
#define List_1_Sort_m19966(__this, method) (( void (*) (List_1_t671 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19967(__this, ___comparison, method) (( void (*) (List_1_t671 *, Comparison_1_t3456 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::ToArray()
#define List_1_ToArray_m4439(__this, method) (( ReconstructionAbstractBehaviourU5BU5D_t824* (*) (List_1_t671 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m19968(__this, method) (( void (*) (List_1_t671 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m19969(__this, method) (( int32_t (*) (List_1_t671 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19970(__this, ___value, method) (( void (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
#define List_1_get_Count_m19971(__this, method) (( int32_t (*) (List_1_t671 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m19972(__this, ___index, method) (( ReconstructionAbstractBehaviour_t68 * (*) (List_1_t671 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m19973(__this, ___index, ___value, method) (( void (*) (List_1_t671 *, int32_t, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
