﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ValueCollection_t3572;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;
// System.Collections.Generic.IEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct IEnumerator_1_t4243;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.QCARManagerImpl/VirtualButtonData[]
struct VirtualButtonDataU5BU5D_t3563;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_43.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m22037_gshared (ValueCollection_t3572 * __this, Dictionary_2_t870 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m22037(__this, ___dictionary, method) (( void (*) (ValueCollection_t3572 *, Dictionary_2_t870 *, const MethodInfo*))ValueCollection__ctor_m22037_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22038_gshared (ValueCollection_t3572 * __this, VirtualButtonData_t643  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22038(__this, ___item, method) (( void (*) (ValueCollection_t3572 *, VirtualButtonData_t643 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22038_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22039_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22039(__this, method) (( void (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22039_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22040_gshared (ValueCollection_t3572 * __this, VirtualButtonData_t643  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22040(__this, ___item, method) (( bool (*) (ValueCollection_t3572 *, VirtualButtonData_t643 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22040_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22041_gshared (ValueCollection_t3572 * __this, VirtualButtonData_t643  ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22041(__this, ___item, method) (( bool (*) (ValueCollection_t3572 *, VirtualButtonData_t643 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22041_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22042_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22042(__this, method) (( Object_t* (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22042_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22043_gshared (ValueCollection_t3572 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m22043(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3572 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m22043_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22044_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22044(__this, method) (( Object_t * (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22045_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22045(__this, method) (( bool (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22045_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22046_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22046(__this, method) (( bool (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22046_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m22047_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m22047(__this, method) (( Object_t * (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m22047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m22048_gshared (ValueCollection_t3572 * __this, VirtualButtonDataU5BU5D_t3563* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m22048(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3572 *, VirtualButtonDataU5BU5D_t3563*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m22048_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3573  ValueCollection_GetEnumerator_m22049_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m22049(__this, method) (( Enumerator_t3573  (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_GetEnumerator_m22049_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m22050_gshared (ValueCollection_t3572 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m22050(__this, method) (( int32_t (*) (ValueCollection_t3572 *, const MethodInfo*))ValueCollection_get_Count_m22050_gshared)(__this, method)
