﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3447;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct  List_1_t668  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_items
	SmartTerrainTrackableU5BU5D_t3447* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_version
	int32_t ____version_3;
};
struct List_1_t668_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::EmptyArray
	SmartTerrainTrackableU5BU5D_t3447* ___EmptyArray_4;
};
