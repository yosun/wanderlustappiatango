﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Type>
struct InternalEnumerator_1_t3119;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Type>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15126(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3119 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Type>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3119 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Type>::Dispose()
#define InternalEnumerator_1_Dispose_m15128(__this, method) (( void (*) (InternalEnumerator_1_t3119 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Type>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15129(__this, method) (( bool (*) (InternalEnumerator_1_t3119 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Type>::get_Current()
#define InternalEnumerator_1_get_Current_m15130(__this, method) (( Type_t * (*) (InternalEnumerator_1_t3119 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
