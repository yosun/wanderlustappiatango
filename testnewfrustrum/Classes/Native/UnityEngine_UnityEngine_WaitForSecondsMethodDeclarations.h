﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t452;
struct WaitForSeconds_t452_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m2185 (WaitForSeconds_t452 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t452_marshal(const WaitForSeconds_t452& unmarshaled, WaitForSeconds_t452_marshaled& marshaled);
void WaitForSeconds_t452_marshal_back(const WaitForSeconds_t452_marshaled& marshaled, WaitForSeconds_t452& unmarshaled);
void WaitForSeconds_t452_marshal_cleanup(WaitForSeconds_t452_marshaled& marshaled);
