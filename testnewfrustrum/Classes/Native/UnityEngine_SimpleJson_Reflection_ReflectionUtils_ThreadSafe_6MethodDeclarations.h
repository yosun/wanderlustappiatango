﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t3840;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t3838;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3859;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4082;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m25973_gshared (ThreadSafeDictionary_2_t3840 * __this, ThreadSafeDictionaryValueFactory_2_t3838 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m25973(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, ThreadSafeDictionaryValueFactory_2_t3838 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m25973_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m25975_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m25977_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m25977(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m25977_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m25979_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m25979(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m25979_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m25981_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m25981(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m25981_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m25983_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m25983(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m25983_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool ThreadSafeDictionary_2_Remove_m25985_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m25985(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m25985_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m25987_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m25987(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m25987_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m25989_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m25989(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m25989_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m25991_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m25991(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m25991_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m25993_gshared (ThreadSafeDictionary_2_t3840 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m25993(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m25993_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m25995_gshared (ThreadSafeDictionary_2_t3840 * __this, KeyValuePair_2_t3254  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m25995(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, KeyValuePair_2_t3254 , const MethodInfo*))ThreadSafeDictionary_2_Add_m25995_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m25997_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m25997(__this, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m25997_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m25999_gshared (ThreadSafeDictionary_2_t3840 * __this, KeyValuePair_2_t3254  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m25999(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3840 *, KeyValuePair_2_t3254 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m25999_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m26001_gshared (ThreadSafeDictionary_2_t3840 * __this, KeyValuePair_2U5BU5D_t3859* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m26001(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t3840 *, KeyValuePair_2U5BU5D_t3859*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m26001_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m26003_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m26003(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m26003_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m26005_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m26005(__this, method) (( bool (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m26005_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m26007_gshared (ThreadSafeDictionary_2_t3840 * __this, KeyValuePair_2_t3254  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m26007(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3840 *, KeyValuePair_2_t3254 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m26007_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m26009_gshared (ThreadSafeDictionary_2_t3840 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m26009(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3840 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m26009_gshared)(__this, method)
