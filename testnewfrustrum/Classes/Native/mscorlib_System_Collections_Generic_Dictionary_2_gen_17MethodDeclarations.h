﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Collections.Generic.ICollection`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct ICollection_1_t4234;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyCollection_t3552;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct ValueCollection_t3556;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3193;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct IDictionary_2_t4235;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t4236;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>
struct IEnumerator_1_t4237;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
extern "C" void Dictionary_2__ctor_m4561_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4561(__this, method) (( void (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2__ctor_m4561_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21797_gshared (Dictionary_2_t869 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21797(__this, ___comparer, method) (( void (*) (Dictionary_2_t869 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21797_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21798_gshared (Dictionary_2_t869 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21798(__this, ___dictionary, method) (( void (*) (Dictionary_2_t869 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21798_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21799_gshared (Dictionary_2_t869 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21799(__this, ___capacity, method) (( void (*) (Dictionary_2_t869 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21799_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21800_gshared (Dictionary_2_t869 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21800(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t869 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21800_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21801_gshared (Dictionary_2_t869 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21801(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t869 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m21801_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21802_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21802(__this, method) (( Object_t* (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21802_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21803_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21803(__this, method) (( Object_t* (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21803_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21804_gshared (Dictionary_2_t869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21804(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21804_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21805_gshared (Dictionary_2_t869 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21805(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t869 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21805_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21806_gshared (Dictionary_2_t869 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21806(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t869 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21806_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21807_gshared (Dictionary_2_t869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21807(__this, ___key, method) (( bool (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21807_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21808_gshared (Dictionary_2_t869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21808(__this, ___key, method) (( void (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21808_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21809_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21809(__this, method) (( bool (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21809_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21810_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21810(__this, method) (( Object_t * (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21811_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21811(__this, method) (( bool (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21811_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21812_gshared (Dictionary_2_t869 * __this, KeyValuePair_2_t3550  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21812(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t869 *, KeyValuePair_2_t3550 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21812_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21813_gshared (Dictionary_2_t869 * __this, KeyValuePair_2_t3550  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21813(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t869 *, KeyValuePair_2_t3550 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21813_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared (Dictionary_2_t869 * __this, KeyValuePair_2U5BU5D_t4236* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t869 *, KeyValuePair_2U5BU5D_t4236*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815_gshared (Dictionary_2_t869 * __this, KeyValuePair_2_t3550  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t869 *, KeyValuePair_2_t3550 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21816_gshared (Dictionary_2_t869 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21816(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t869 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21816_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21817_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21817(__this, method) (( Object_t * (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21817_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21818_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21818(__this, method) (( Object_t* (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21818_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21819_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21819(__this, method) (( Object_t * (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21819_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21820_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21820(__this, method) (( int32_t (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_get_Count_m21820_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C" TrackableResultData_t642  Dictionary_2_get_Item_m21821_gshared (Dictionary_2_t869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21821(__this, ___key, method) (( TrackableResultData_t642  (*) (Dictionary_2_t869 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m21821_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21822_gshared (Dictionary_2_t869 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21822(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t869 *, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_set_Item_m21822_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21823_gshared (Dictionary_2_t869 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21823(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t869 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21823_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21824_gshared (Dictionary_2_t869 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21824(__this, ___size, method) (( void (*) (Dictionary_2_t869 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21824_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21825_gshared (Dictionary_2_t869 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21825(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t869 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21825_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3550  Dictionary_2_make_pair_m21826_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21826(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3550  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_make_pair_m21826_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m21827_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21827(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_pick_key_m21827_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C" TrackableResultData_t642  Dictionary_2_pick_value_m21828_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21828(__this /* static, unused */, ___key, ___value, method) (( TrackableResultData_t642  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_pick_value_m21828_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21829_gshared (Dictionary_2_t869 * __this, KeyValuePair_2U5BU5D_t4236* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21829(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t869 *, KeyValuePair_2U5BU5D_t4236*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21829_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Resize()
extern "C" void Dictionary_2_Resize_m21830_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21830(__this, method) (( void (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_Resize_m21830_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21831_gshared (Dictionary_2_t869 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21831(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t869 *, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_Add_m21831_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Clear()
extern "C" void Dictionary_2_Clear_m21832_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21832(__this, method) (( void (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_Clear_m21832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21833_gshared (Dictionary_2_t869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21833(__this, ___key, method) (( bool (*) (Dictionary_2_t869 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m21833_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21834_gshared (Dictionary_2_t869 * __this, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21834(__this, ___value, method) (( bool (*) (Dictionary_2_t869 *, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_ContainsValue_m21834_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21835_gshared (Dictionary_2_t869 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21835(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t869 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m21835_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21836_gshared (Dictionary_2_t869 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21836(__this, ___sender, method) (( void (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21836_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21837_gshared (Dictionary_2_t869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21837(__this, ___key, method) (( bool (*) (Dictionary_2_t869 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m21837_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21838_gshared (Dictionary_2_t869 * __this, int32_t ___key, TrackableResultData_t642 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21838(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t869 *, int32_t, TrackableResultData_t642 *, const MethodInfo*))Dictionary_2_TryGetValue_m21838_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Keys()
extern "C" KeyCollection_t3552 * Dictionary_2_get_Keys_m21839_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21839(__this, method) (( KeyCollection_t3552 * (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_get_Keys_m21839_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Values()
extern "C" ValueCollection_t3556 * Dictionary_2_get_Values_m21840_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21840(__this, method) (( ValueCollection_t3556 * (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_get_Values_m21840_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m21841_gshared (Dictionary_2_t869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21841(__this, ___key, method) (( int32_t (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21841_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C" TrackableResultData_t642  Dictionary_2_ToTValue_m21842_gshared (Dictionary_2_t869 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21842(__this, ___value, method) (( TrackableResultData_t642  (*) (Dictionary_2_t869 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21842_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21843_gshared (Dictionary_2_t869 * __this, KeyValuePair_2_t3550  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21843(__this, ___pair, method) (( bool (*) (Dictionary_2_t869 *, KeyValuePair_2_t3550 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21843_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t3554  Dictionary_2_GetEnumerator_m21844_gshared (Dictionary_2_t869 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21844(__this, method) (( Enumerator_t3554  (*) (Dictionary_2_t869 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21844_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m21845_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21845(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t642 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21845_gshared)(__this /* static, unused */, ___key, ___value, method)
