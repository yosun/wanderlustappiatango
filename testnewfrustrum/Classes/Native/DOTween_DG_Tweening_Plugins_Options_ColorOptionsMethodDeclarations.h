﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1011;
struct ColorOptions_t1011_marshaled;

void ColorOptions_t1011_marshal(const ColorOptions_t1011& unmarshaled, ColorOptions_t1011_marshaled& marshaled);
void ColorOptions_t1011_marshal_back(const ColorOptions_t1011_marshaled& marshaled, ColorOptions_t1011& unmarshaled);
void ColorOptions_t1011_marshal_cleanup(ColorOptions_t1011_marshaled& marshaled);
