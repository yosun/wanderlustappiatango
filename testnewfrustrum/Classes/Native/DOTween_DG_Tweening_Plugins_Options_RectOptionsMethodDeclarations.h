﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1004;
struct RectOptions_t1004_marshaled;

void RectOptions_t1004_marshal(const RectOptions_t1004& unmarshaled, RectOptions_t1004_marshaled& marshaled);
void RectOptions_t1004_marshal_back(const RectOptions_t1004_marshaled& marshaled, RectOptions_t1004& unmarshaled);
void RectOptions_t1004_marshal_cleanup(RectOptions_t1004_marshaled& marshaled);
