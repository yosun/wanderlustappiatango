﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t368;

// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern "C" void HorizontalLayoutGroup__ctor_m1792 (HorizontalLayoutGroup_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793 (HorizontalLayoutGroup_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794 (HorizontalLayoutGroup_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m1795 (HorizontalLayoutGroup_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m1796 (HorizontalLayoutGroup_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
