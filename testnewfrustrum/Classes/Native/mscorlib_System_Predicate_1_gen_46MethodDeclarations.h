﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>
struct Predicate_1_t3635;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t171;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m22944(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3635 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::Invoke(T)
#define Predicate_1_Invoke_m22945(__this, ___obj, method) (( bool (*) (Predicate_1_t3635 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m22946(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3635 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m22947(__this, ___result, method) (( bool (*) (Predicate_1_t3635 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
