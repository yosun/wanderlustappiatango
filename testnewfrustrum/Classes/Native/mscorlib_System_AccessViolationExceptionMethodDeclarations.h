﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AccessViolationException
struct AccessViolationException_t2482;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.AccessViolationException::.ctor()
extern "C" void AccessViolationException__ctor_m13165 (AccessViolationException_t2482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AccessViolationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AccessViolationException__ctor_m13166 (AccessViolationException_t2482 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
