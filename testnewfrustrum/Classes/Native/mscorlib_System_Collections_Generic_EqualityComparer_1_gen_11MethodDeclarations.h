﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct EqualityComparer_1_t3827;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt64>::.ctor()
extern "C" void EqualityComparer_1__ctor_m25770_gshared (EqualityComparer_1_t3827 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m25770(__this, method) (( void (*) (EqualityComparer_1_t3827 *, const MethodInfo*))EqualityComparer_1__ctor_m25770_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt64>::.cctor()
extern "C" void EqualityComparer_1__cctor_m25771_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m25771(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m25771_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt64>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25772_gshared (EqualityComparer_1_t3827 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25772(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3827 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25772_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt64>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25773_gshared (EqualityComparer_1_t3827 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25773(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3827 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25773_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt64>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt64>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt64>::get_Default()
extern "C" EqualityComparer_1_t3827 * EqualityComparer_1_get_Default_m25774_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m25774(__this /* static, unused */, method) (( EqualityComparer_1_t3827 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m25774_gshared)(__this /* static, unused */, method)
