﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t3764;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Comparison`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m24999_gshared (Comparison_1_t3764 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m24999(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3764 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m24999_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m25000_gshared (Comparison_1_t3764 * __this, UICharInfo_t460  ___x, UICharInfo_t460  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m25000(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3764 *, UICharInfo_t460 , UICharInfo_t460 , const MethodInfo*))Comparison_1_Invoke_m25000_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UICharInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m25001_gshared (Comparison_1_t3764 * __this, UICharInfo_t460  ___x, UICharInfo_t460  ___y, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m25001(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3764 *, UICharInfo_t460 , UICharInfo_t460 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m25001_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m25002_gshared (Comparison_1_t3764 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m25002(__this, ___result, method) (( int32_t (*) (Comparison_1_t3764 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m25002_gshared)(__this, ___result, method)
