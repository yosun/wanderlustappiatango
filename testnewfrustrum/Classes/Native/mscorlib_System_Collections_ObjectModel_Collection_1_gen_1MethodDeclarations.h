﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t3274;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t312;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4089;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t467;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m17191_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17191(__this, method) (( void (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1__ctor_m17191_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17192_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17192(__this, method) (( bool (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17193_gshared (Collection_1_t3274 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17193(__this, ___array, ___index, method) (( void (*) (Collection_1_t3274 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17193_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17194_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17194(__this, method) (( Object_t * (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17194_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17195_gshared (Collection_1_t3274 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17195(__this, ___value, method) (( int32_t (*) (Collection_1_t3274 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17195_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17196_gshared (Collection_1_t3274 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17196(__this, ___value, method) (( bool (*) (Collection_1_t3274 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17196_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17197_gshared (Collection_1_t3274 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17197(__this, ___value, method) (( int32_t (*) (Collection_1_t3274 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17197_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17198_gshared (Collection_1_t3274 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17198(__this, ___index, ___value, method) (( void (*) (Collection_1_t3274 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17198_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17199_gshared (Collection_1_t3274 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17199(__this, ___value, method) (( void (*) (Collection_1_t3274 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17199_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17200_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17200(__this, method) (( bool (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17200_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17201_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17201(__this, method) (( Object_t * (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17201_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17202_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17202(__this, method) (( bool (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17202_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17203_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17203(__this, method) (( bool (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17203_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17204_gshared (Collection_1_t3274 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17204(__this, ___index, method) (( Object_t * (*) (Collection_1_t3274 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17204_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17205_gshared (Collection_1_t3274 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17205(__this, ___index, ___value, method) (( void (*) (Collection_1_t3274 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17205_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m17206_gshared (Collection_1_t3274 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_Add_m17206(__this, ___item, method) (( void (*) (Collection_1_t3274 *, UIVertex_t313 , const MethodInfo*))Collection_1_Add_m17206_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m17207_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17207(__this, method) (( void (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_Clear_m17207_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m17208_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17208(__this, method) (( void (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_ClearItems_m17208_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m17209_gshared (Collection_1_t3274 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17209(__this, ___item, method) (( bool (*) (Collection_1_t3274 *, UIVertex_t313 , const MethodInfo*))Collection_1_Contains_m17209_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17210_gshared (Collection_1_t3274 * __this, UIVertexU5BU5D_t312* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17210(__this, ___array, ___index, method) (( void (*) (Collection_1_t3274 *, UIVertexU5BU5D_t312*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17210_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17211_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17211(__this, method) (( Object_t* (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_GetEnumerator_m17211_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17212_gshared (Collection_1_t3274 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17212(__this, ___item, method) (( int32_t (*) (Collection_1_t3274 *, UIVertex_t313 , const MethodInfo*))Collection_1_IndexOf_m17212_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17213_gshared (Collection_1_t3274 * __this, int32_t ___index, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17213(__this, ___index, ___item, method) (( void (*) (Collection_1_t3274 *, int32_t, UIVertex_t313 , const MethodInfo*))Collection_1_Insert_m17213_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17214_gshared (Collection_1_t3274 * __this, int32_t ___index, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17214(__this, ___index, ___item, method) (( void (*) (Collection_1_t3274 *, int32_t, UIVertex_t313 , const MethodInfo*))Collection_1_InsertItem_m17214_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m17215_gshared (Collection_1_t3274 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17215(__this, ___item, method) (( bool (*) (Collection_1_t3274 *, UIVertex_t313 , const MethodInfo*))Collection_1_Remove_m17215_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17216_gshared (Collection_1_t3274 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17216(__this, ___index, method) (( void (*) (Collection_1_t3274 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17216_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17217_gshared (Collection_1_t3274 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17217(__this, ___index, method) (( void (*) (Collection_1_t3274 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17217_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17218_gshared (Collection_1_t3274 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17218(__this, method) (( int32_t (*) (Collection_1_t3274 *, const MethodInfo*))Collection_1_get_Count_m17218_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t313  Collection_1_get_Item_m17219_gshared (Collection_1_t3274 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17219(__this, ___index, method) (( UIVertex_t313  (*) (Collection_1_t3274 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17219_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17220_gshared (Collection_1_t3274 * __this, int32_t ___index, UIVertex_t313  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17220(__this, ___index, ___value, method) (( void (*) (Collection_1_t3274 *, int32_t, UIVertex_t313 , const MethodInfo*))Collection_1_set_Item_m17220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17221_gshared (Collection_1_t3274 * __this, int32_t ___index, UIVertex_t313  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17221(__this, ___index, ___item, method) (( void (*) (Collection_1_t3274 *, int32_t, UIVertex_t313 , const MethodInfo*))Collection_1_SetItem_m17221_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17222_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17222(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17222_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t313  Collection_1_ConvertItem_m17223_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17223(__this /* static, unused */, ___item, method) (( UIVertex_t313  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17223_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17224_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17224(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17224_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17225_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17225(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17225_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17226_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17226(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17226_gshared)(__this /* static, unused */, ___list, method)
