﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>
struct Enumerator_t3684;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t934;
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t979;

// System.Void System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m23654(__this, ___t, method) (( void (*) (Enumerator_t3684 *, Stack_1_t979 *, const MethodInfo*))Enumerator__ctor_m15431_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23655(__this, method) (( Object_t * (*) (Enumerator_t3684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15432_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::Dispose()
#define Enumerator_Dispose_m23656(__this, method) (( void (*) (Enumerator_t3684 *, const MethodInfo*))Enumerator_Dispose_m15433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::MoveNext()
#define Enumerator_MoveNext_m23657(__this, method) (( bool (*) (Enumerator_t3684 *, const MethodInfo*))Enumerator_MoveNext_m15434_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::get_Current()
#define Enumerator_get_Current_m23658(__this, method) (( Tween_t934 * (*) (Enumerator_t3684 *, const MethodInfo*))Enumerator_get_Current_m15435_gshared)(__this, method)
