﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t683;
// UnityEngine.Texture
struct Texture_t321;
// System.String
struct String_t;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_DidUpdateThisFrame()
extern "C" bool WebCamTexAdaptorImpl_get_DidUpdateThisFrame_m3105 (WebCamTexAdaptorImpl_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_IsPlaying()
extern "C" bool WebCamTexAdaptorImpl_get_IsPlaying_m3106 (WebCamTexAdaptorImpl_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.WebCamTexAdaptorImpl::get_Texture()
extern "C" Texture_t321 * WebCamTexAdaptorImpl_get_Texture_m3107 (WebCamTexAdaptorImpl_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::.ctor(System.String,System.Int32,Vuforia.QCARRenderer/Vec2I)
extern "C" void WebCamTexAdaptorImpl__ctor_m3108 (WebCamTexAdaptorImpl_t683 * __this, String_t* ___deviceName, int32_t ___requestedFPS, Vec2I_t663  ___requestedTextureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Play()
extern "C" void WebCamTexAdaptorImpl_Play_m3109 (WebCamTexAdaptorImpl_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Stop()
extern "C" void WebCamTexAdaptorImpl_Stop_m3110 (WebCamTexAdaptorImpl_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
