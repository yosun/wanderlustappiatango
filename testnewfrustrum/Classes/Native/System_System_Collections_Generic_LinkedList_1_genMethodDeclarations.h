﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t659;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t817;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4058;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Int32[]
struct Int32U5BU5D_t19;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C" void LinkedList_1__ctor_m4408_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m4408(__this, method) (( void (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1__ctor_m4408_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m19780_gshared (LinkedList_1_t659 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m19780(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t659 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))LinkedList_1__ctor_m19780_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19781_gshared (LinkedList_1_t659 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19781(__this, ___value, method) (( void (*) (LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19781_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m19782_gshared (LinkedList_1_t659 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m19782(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t659 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m19782_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19783_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19783(__this, method) (( Object_t* (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19783_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19784_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19784(__this, method) (( Object_t * (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19785_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19785(__this, method) (( bool (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19785_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19786_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19786(__this, method) (( bool (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19786_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19787_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19787(__this, method) (( Object_t * (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19787_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m19788_gshared (LinkedList_1_t659 * __this, LinkedListNode_1_t817 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m19788(__this, ___node, method) (( void (*) (LinkedList_1_t659 *, LinkedListNode_1_t817 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m19788_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C" LinkedListNode_1_t817 * LinkedList_1_AddLast_m4417_gshared (LinkedList_1_t659 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m4417(__this, ___value, method) (( LinkedListNode_1_t817 * (*) (LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m4417_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C" void LinkedList_1_Clear_m19789_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m19789(__this, method) (( void (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_Clear_m19789_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C" bool LinkedList_1_Contains_m19790_gshared (LinkedList_1_t659 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m19790(__this, ___value, method) (( bool (*) (LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m19790_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m19791_gshared (LinkedList_1_t659 * __this, Int32U5BU5D_t19* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m19791(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t659 *, Int32U5BU5D_t19*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m19791_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C" LinkedListNode_1_t817 * LinkedList_1_Find_m19792_gshared (LinkedList_1_t659 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Find_m19792(__this, ___value, method) (( LinkedListNode_1_t817 * (*) (LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedList_1_Find_m19792_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t3442  LinkedList_1_GetEnumerator_m19793_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m19793(__this, method) (( Enumerator_t3442  (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_GetEnumerator_m19793_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m19794_gshared (LinkedList_1_t659 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m19794(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t659 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))LinkedList_1_GetObjectData_m19794_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m19795_gshared (LinkedList_1_t659 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m19795(__this, ___sender, method) (( void (*) (LinkedList_1_t659 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m19795_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C" bool LinkedList_1_Remove_m19796_gshared (LinkedList_1_t659 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m19796(__this, ___value, method) (( bool (*) (LinkedList_1_t659 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m19796_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m4560_gshared (LinkedList_1_t659 * __this, LinkedListNode_1_t817 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m4560(__this, ___node, method) (( void (*) (LinkedList_1_t659 *, LinkedListNode_1_t817 *, const MethodInfo*))LinkedList_1_Remove_m4560_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m19797_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m19797(__this, method) (( void (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_RemoveLast_m19797_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m19798_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m19798(__this, method) (( int32_t (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_get_Count_m19798_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C" LinkedListNode_1_t817 * LinkedList_1_get_First_m4423_gshared (LinkedList_1_t659 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m4423(__this, method) (( LinkedListNode_1_t817 * (*) (LinkedList_1_t659 *, const MethodInfo*))LinkedList_1_get_First_m4423_gshared)(__this, method)
