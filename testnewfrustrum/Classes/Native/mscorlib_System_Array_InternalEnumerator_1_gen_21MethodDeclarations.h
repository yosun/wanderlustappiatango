﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
struct InternalEnumerator_1_t3307;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17698_gshared (InternalEnumerator_1_t3307 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17698(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3307 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17698_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17699_gshared (InternalEnumerator_1_t3307 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17699(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3307 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17699_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17700_gshared (InternalEnumerator_1_t3307 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17700(__this, method) (( void (*) (InternalEnumerator_1_t3307 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17700_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17701_gshared (InternalEnumerator_1_t3307 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17701(__this, method) (( bool (*) (InternalEnumerator_1_t3307 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17701_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t10  InternalEnumerator_1_get_Current_m17702_gshared (InternalEnumerator_1_t3307 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17702(__this, method) (( Vector2_t10  (*) (InternalEnumerator_1_t3307 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17702_gshared)(__this, method)
