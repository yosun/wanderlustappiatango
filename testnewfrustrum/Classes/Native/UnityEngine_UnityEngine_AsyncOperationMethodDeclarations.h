﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t1134;
struct AsyncOperation_t1134_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m6197 (AsyncOperation_t1134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m6198 (AsyncOperation_t1134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m6199 (AsyncOperation_t1134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t1134_marshal(const AsyncOperation_t1134& unmarshaled, AsyncOperation_t1134_marshaled& marshaled);
void AsyncOperation_t1134_marshal_back(const AsyncOperation_t1134_marshaled& marshaled, AsyncOperation_t1134& unmarshaled);
void AsyncOperation_t1134_marshal_cleanup(AsyncOperation_t1134_marshaled& marshaled);
