﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct  UnityAction_1_t409  : public MulticastDelegate_t307
{
};
