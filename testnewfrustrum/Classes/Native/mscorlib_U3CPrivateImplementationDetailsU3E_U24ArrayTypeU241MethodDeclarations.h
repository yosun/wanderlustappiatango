﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct U24ArrayTypeU2416_t2564;
struct U24ArrayTypeU2416_t2564_marshaled;

void U24ArrayTypeU2416_t2564_marshal(const U24ArrayTypeU2416_t2564& unmarshaled, U24ArrayTypeU2416_t2564_marshaled& marshaled);
void U24ArrayTypeU2416_t2564_marshal_back(const U24ArrayTypeU2416_t2564_marshaled& marshaled, U24ArrayTypeU2416_t2564& unmarshaled);
void U24ArrayTypeU2416_t2564_marshal_cleanup(U24ArrayTypeU2416_t2564_marshaled& marshaled);
