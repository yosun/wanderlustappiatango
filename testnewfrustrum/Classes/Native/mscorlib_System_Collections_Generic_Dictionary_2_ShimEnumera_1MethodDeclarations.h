﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t3263;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3253;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16976_gshared (ShimEnumerator_t3263 * __this, Dictionary_2_t3253 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16976(__this, ___host, method) (( void (*) (ShimEnumerator_t3263 *, Dictionary_2_t3253 *, const MethodInfo*))ShimEnumerator__ctor_m16976_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16977_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16977(__this, method) (( bool (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_MoveNext_m16977_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m16978_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16978(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Entry_m16978_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16979_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16979(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Key_m16979_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16980_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16980(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Value_m16980_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16981_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16981(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Current_m16981_gshared)(__this, method)
