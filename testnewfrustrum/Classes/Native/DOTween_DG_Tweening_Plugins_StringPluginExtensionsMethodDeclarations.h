﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.StringPluginExtensions
struct StringPluginExtensions_t992;
// System.Char[]
struct CharU5BU5D_t110;
// System.Text.StringBuilder
struct StringBuilder_t423;

// System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern "C" void StringPluginExtensions__cctor_m5467 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern "C" void StringPluginExtensions_ScrambleChars_m5468 (Object_t * __this /* static, unused */, CharU5BU5D_t110* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern "C" StringBuilder_t423 * StringPluginExtensions_AppendScrambledChars_m5469 (Object_t * __this /* static, unused */, StringBuilder_t423 * ___buffer, int32_t ___length, CharU5BU5D_t110* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
