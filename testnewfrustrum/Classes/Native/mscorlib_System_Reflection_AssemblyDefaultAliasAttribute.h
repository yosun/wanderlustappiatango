﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
struct  AssemblyDefaultAliasAttribute_t1584  : public Attribute_t138
{
	// System.String System.Reflection.AssemblyDefaultAliasAttribute::name
	String_t* ___name_0;
};
