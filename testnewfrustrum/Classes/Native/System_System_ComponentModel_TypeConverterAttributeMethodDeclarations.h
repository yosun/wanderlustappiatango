﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t1862;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

// System.Void System.ComponentModel.TypeConverterAttribute::.ctor()
extern "C" void TypeConverterAttribute__ctor_m8515 (TypeConverterAttribute_t1862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
extern "C" void TypeConverterAttribute__ctor_m8516 (TypeConverterAttribute_t1862 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.cctor()
extern "C" void TypeConverterAttribute__cctor_m8517 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.TypeConverterAttribute::Equals(System.Object)
extern "C" bool TypeConverterAttribute_Equals_m8518 (TypeConverterAttribute_t1862 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.TypeConverterAttribute::GetHashCode()
extern "C" int32_t TypeConverterAttribute_GetHashCode_m8519 (TypeConverterAttribute_t1862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.TypeConverterAttribute::get_ConverterTypeName()
extern "C" String_t* TypeConverterAttribute_get_ConverterTypeName_m8520 (TypeConverterAttribute_t1862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
