﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t680;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4138;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4173;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>
struct KeyCollection_t3477;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>
struct ValueCollection_t3478;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t3458;
// System.Collections.Generic.IDictionary`2<System.Type,System.UInt16>
struct IDictionary_2_t4174;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t4175;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>>
struct IEnumerator_1_t4176;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_35MethodDeclarations.h"
#define Dictionary_2__ctor_m4444(__this, method) (( void (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2__ctor_m20021_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20022(__this, ___comparer, method) (( void (*) (Dictionary_2_t680 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20023_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m20024(__this, ___dictionary, method) (( void (*) (Dictionary_2_t680 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20025_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20026(__this, ___capacity, method) (( void (*) (Dictionary_2_t680 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m20027_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20028(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t680 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20029_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20030(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t680 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m20031_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20032(__this, method) (( Object_t* (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20034(__this, method) (( Object_t* (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20036(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20037_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20038(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t680 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20039_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20040(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t680 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20041_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m20042(__this, ___key, method) (( bool (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m20043_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20044(__this, ___key, method) (( void (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20045_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20046(__this, method) (( bool (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20048(__this, method) (( Object_t * (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20050(__this, method) (( bool (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20052(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t680 *, KeyValuePair_2_t3476 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20054(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t680 *, KeyValuePair_2_t3476 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20056(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t680 *, KeyValuePair_2U5BU5D_t4175*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20058(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t680 *, KeyValuePair_2_t3476 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20060(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t680 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20061_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20062(__this, method) (( Object_t * (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20064(__this, method) (( Object_t* (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20066(__this, method) (( Object_t * (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Count()
#define Dictionary_2_get_Count_m20068(__this, method) (( int32_t (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_get_Count_m20069_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Item(TKey)
#define Dictionary_2_get_Item_m20070(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t680 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m20071_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20072(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t680 *, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m20073_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20074(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t680 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m20075_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20076(__this, ___size, method) (( void (*) (Dictionary_2_t680 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m20077_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20078(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t680 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m20079_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20080(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3476  (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m20081_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20082(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m20083_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20084(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m20085_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20086(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t680 *, KeyValuePair_2U5BU5D_t4175*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m20087_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Resize()
#define Dictionary_2_Resize_m20088(__this, method) (( void (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_Resize_m20089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Add(TKey,TValue)
#define Dictionary_2_Add_m20090(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t680 *, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m20091_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Clear()
#define Dictionary_2_Clear_m20092(__this, method) (( void (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_Clear_m20093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20094(__this, ___key, method) (( bool (*) (Dictionary_2_t680 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m20095_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20096(__this, ___value, method) (( bool (*) (Dictionary_2_t680 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m20097_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20098(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t680 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m20099_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20100(__this, ___sender, method) (( void (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m20101_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Remove(TKey)
#define Dictionary_2_Remove_m20102(__this, ___key, method) (( bool (*) (Dictionary_2_t680 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m20103_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20104(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t680 *, Type_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m20105_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Keys()
#define Dictionary_2_get_Keys_m20106(__this, method) (( KeyCollection_t3477 * (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_get_Keys_m20107_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Values()
#define Dictionary_2_get_Values_m20108(__this, method) (( ValueCollection_t3478 * (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_get_Values_m20109_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20110(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m20111_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20112(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t680 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m20113_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20114(__this, ___pair, method) (( bool (*) (Dictionary_2_t680 *, KeyValuePair_2_t3476 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20115_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20116(__this, method) (( Enumerator_t3479  (*) (Dictionary_2_t680 *, const MethodInfo*))Dictionary_2_GetEnumerator_m20117_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20118(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20119_gshared)(__this /* static, unused */, ___key, ___value, method)
