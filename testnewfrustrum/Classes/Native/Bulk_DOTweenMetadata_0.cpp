﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "DOTween_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t925_il2cpp_TypeInfo;
// <Module>
#include "DOTween_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t925_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CModuleU3E_t925_0_0_0;
extern const Il2CppType U3CModuleU3E_t925_1_0_0;
struct U3CModuleU3E_t925;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t925_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t925_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t925_0_0_0/* byval_arg */
	, &U3CModuleU3E_t925_1_0_0/* this_arg */
	, &U3CModuleU3E_t925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t925)/* instance_size */
	, sizeof (U3CModuleU3E_t925)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
// Metadata Definition DG.Tweening.UpdateType
extern TypeInfo UpdateType_t926_il2cpp_TypeInfo;
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateTypeMethodDeclarations.h"
static const MethodInfo* UpdateType_t926_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference UpdateType_t926_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UpdateType_t926_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair UpdateType_t926_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateType_t926_0_0_0;
extern const Il2CppType UpdateType_t926_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UpdateType_t926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateType_t926_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UpdateType_t926_VTable/* vtableMethods */
	, UpdateType_t926_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo UpdateType_t926_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateType"/* name */
	, "DG.Tweening"/* namespaze */
	, UpdateType_t926_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateType_t926_0_0_0/* byval_arg */
	, &UpdateType_t926_1_0_0/* this_arg */
	, &UpdateType_t926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateType_t926)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateType_t926)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Plugins.Core.ITweenPlugin
extern TypeInfo ITweenPlugin_t969_il2cpp_TypeInfo;
static const MethodInfo* ITweenPlugin_t969_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ITweenPlugin_t969_0_0_0;
extern const Il2CppType ITweenPlugin_t969_1_0_0;
struct ITweenPlugin_t969;
const Il2CppTypeDefinitionMetadata ITweenPlugin_t969_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ITweenPlugin_t969_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITweenPlugin"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, ITweenPlugin_t969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ITweenPlugin_t969_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ITweenPlugin_t969_0_0_0/* byval_arg */
	, &ITweenPlugin_t969_1_0_0/* this_arg */
	, &ITweenPlugin_t969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Plugins.Core.ABSTweenPlugin`3
extern TypeInfo ABSTweenPlugin_3_t1064_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ABSTweenPlugin_3_t1064_Il2CppGenericContainer;
extern TypeInfo ABSTweenPlugin_3_t1064_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1064_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1064_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo ABSTweenPlugin_3_t1064_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1064_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1064_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo ABSTweenPlugin_3_t1064_gp_TPlugOptions_2_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t524_0_0_0;
static const Il2CppType* ABSTweenPlugin_3_t1064_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1064_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1064_Il2CppGenericContainer, ABSTweenPlugin_3_t1064_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* ABSTweenPlugin_3_t1064_Il2CppGenericParametersArray[3] = 
{
	&ABSTweenPlugin_3_t1064_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&ABSTweenPlugin_3_t1064_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&ABSTweenPlugin_3_t1064_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ABSTweenPlugin_3_t1064_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ABSTweenPlugin_3_t1064_il2cpp_TypeInfo, 3, 0, ABSTweenPlugin_3_t1064_Il2CppGenericParametersArray };
extern const Il2CppType TweenerCore_3_t1072_0_0_0;
extern const Il2CppType TweenerCore_3_t1072_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_Reset_m5585_ParameterInfos[] = 
{
	{"t", 0, 134217729, 0, &TweenerCore_3_t1072_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_Reset_m5585_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_Reset_m5585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1072_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_0_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_0_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_ConvertToStartValue_m5586_ParameterInfos[] = 
{
	{"t", 0, 134217730, 0, &TweenerCore_3_t1072_0_0_0},
	{"value", 1, 134217731, 0, &ABSTweenPlugin_3_t1064_gp_0_0_0_0},
};
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_1_0_0_0;
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
extern const MethodInfo ABSTweenPlugin_3_ConvertToStartValue_m5586_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &ABSTweenPlugin_3_t1064_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_ConvertToStartValue_m5586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1072_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_SetRelativeEndValue_m5587_ParameterInfos[] = 
{
	{"t", 0, 134217732, 0, &TweenerCore_3_t1072_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_SetRelativeEndValue_m5587_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_SetRelativeEndValue_m5587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1072_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_SetChangeValue_m5588_ParameterInfos[] = 
{
	{"t", 0, 134217733, 0, &TweenerCore_3_t1072_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_SetChangeValue_m5588_MethodInfo = 
{
	"SetChangeValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_SetChangeValue_m5588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_2_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_2_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_1_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_GetSpeedBasedDuration_m5589_ParameterInfos[] = 
{
	{"options", 0, 134217734, 0, &ABSTweenPlugin_3_t1064_gp_2_0_0_0},
	{"unitsXSecond", 1, 134217735, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217736, 0, &ABSTweenPlugin_3_t1064_gp_1_0_0_0},
};
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
extern const MethodInfo ABSTweenPlugin_3_GetSpeedBasedDuration_m5589_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_GetSpeedBasedDuration_m5589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_2_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1073_0_0_0;
extern const Il2CppType DOGetter_1_t1073_0_0_0;
extern const Il2CppType DOSetter_1_t1074_0_0_0;
extern const Il2CppType DOSetter_1_t1074_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_1_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_gp_1_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_EvaluateAndApply_m5590_ParameterInfos[] = 
{
	{"options", 0, 134217737, 0, &ABSTweenPlugin_3_t1064_gp_2_0_0_0},
	{"t", 1, 134217738, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217739, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217740, 0, &DOGetter_1_t1073_0_0_0},
	{"setter", 4, 134217741, 0, &DOSetter_1_t1074_0_0_0},
	{"elapsed", 5, 134217742, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217743, 0, &ABSTweenPlugin_3_t1064_gp_1_0_0_0},
	{"changeValue", 7, 134217744, 0, &ABSTweenPlugin_3_t1064_gp_1_0_0_0},
	{"duration", 8, 134217745, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217746, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217747, 0, &UpdateNotice_t1012_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo ABSTweenPlugin_3_EvaluateAndApply_m5590_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1064_ABSTweenPlugin_3_EvaluateAndApply_m5590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::.ctor()
extern const MethodInfo ABSTweenPlugin_3__ctor_m5591_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ABSTweenPlugin_3_t1064_MethodInfos[] =
{
	&ABSTweenPlugin_3_Reset_m5585_MethodInfo,
	&ABSTweenPlugin_3_ConvertToStartValue_m5586_MethodInfo,
	&ABSTweenPlugin_3_SetRelativeEndValue_m5587_MethodInfo,
	&ABSTweenPlugin_3_SetChangeValue_m5588_MethodInfo,
	&ABSTweenPlugin_3_GetSpeedBasedDuration_m5589_MethodInfo,
	&ABSTweenPlugin_3_EvaluateAndApply_m5590_MethodInfo,
	&ABSTweenPlugin_3__ctor_m5591_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference ABSTweenPlugin_3_t1064_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool ABSTweenPlugin_3_t1064_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ABSTweenPlugin_3_t1064_InterfacesTypeInfos[] = 
{
	&ITweenPlugin_t969_0_0_0,
};
static Il2CppInterfaceOffsetPair ABSTweenPlugin_3_t1064_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ABSTweenPlugin_3_t1064_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1064_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ABSTweenPlugin_3_t1064;
const Il2CppTypeDefinitionMetadata ABSTweenPlugin_3_t1064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ABSTweenPlugin_3_t1064_InterfacesTypeInfos/* implementedInterfaces */
	, ABSTweenPlugin_3_t1064_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ABSTweenPlugin_3_t1064_VTable/* vtableMethods */
	, ABSTweenPlugin_3_t1064_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ABSTweenPlugin_3_t1064_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ABSTweenPlugin`3"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, ABSTweenPlugin_3_t1064_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ABSTweenPlugin_3_t1064_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ABSTweenPlugin_3_t1064_0_0_0/* byval_arg */
	, &ABSTweenPlugin_3_t1064_1_0_0/* this_arg */
	, &ABSTweenPlugin_3_t1064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ABSTweenPlugin_3_t1064_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPlugin.h"
// Metadata Definition DG.Tweening.Plugins.UintPlugin
extern TypeInfo UintPlugin_t927_il2cpp_TypeInfo;
// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1017_0_0_0;
extern const Il2CppType TweenerCore_3_t1017_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_Reset_m5263_ParameterInfos[] = 
{
	{"t", 0, 134217748, 0, &TweenerCore_3_t1017_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_Reset_m5263_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&UintPlugin_Reset_m5263/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UintPlugin_t927_UintPlugin_Reset_m5263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1017_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_ConvertToStartValue_m5264_ParameterInfos[] = 
{
	{"t", 0, 134217749, 0, &TweenerCore_3_t1017_0_0_0},
	{"value", 1, 134217750, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1075_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 DG.Tweening.Plugins.UintPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>,System.UInt32)
extern const MethodInfo UintPlugin_ConvertToStartValue_m5264_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&UintPlugin_ConvertToStartValue_m5264/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1075_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1075_Object_t_Int32_t127/* invoker_method */
	, UintPlugin_t927_UintPlugin_ConvertToStartValue_m5264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1017_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_SetRelativeEndValue_m5265_ParameterInfos[] = 
{
	{"t", 0, 134217751, 0, &TweenerCore_3_t1017_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_SetRelativeEndValue_m5265_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&UintPlugin_SetRelativeEndValue_m5265/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UintPlugin_t927_UintPlugin_SetRelativeEndValue_m5265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1017_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_SetChangeValue_m5266_ParameterInfos[] = 
{
	{"t", 0, 134217752, 0, &TweenerCore_3_t1017_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_SetChangeValue_m5266_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&UintPlugin_SetChangeValue_m5266/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UintPlugin_t927_UintPlugin_SetChangeValue_m5266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_GetSpeedBasedDuration_m5267_ParameterInfos[] = 
{
	{"options", 0, 134217753, 0, &NoOptions_t933_0_0_0},
	{"unitsXSecond", 1, 134217754, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217755, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.UintPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt32)
extern const MethodInfo UintPlugin_GetSpeedBasedDuration_m5267_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&UintPlugin_GetSpeedBasedDuration_m5267/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int32_t127/* invoker_method */
	, UintPlugin_t927_UintPlugin_GetSpeedBasedDuration_m5267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1018_0_0_0;
extern const Il2CppType DOGetter_1_t1018_0_0_0;
extern const Il2CppType DOSetter_1_t1019_0_0_0;
extern const Il2CppType DOSetter_1_t1019_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo UintPlugin_t927_UintPlugin_EvaluateAndApply_m5268_ParameterInfos[] = 
{
	{"options", 0, 134217756, 0, &NoOptions_t933_0_0_0},
	{"t", 1, 134217757, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217758, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217759, 0, &DOGetter_1_t1018_0_0_0},
	{"setter", 4, 134217760, 0, &DOSetter_1_t1019_0_0_0},
	{"elapsed", 5, 134217761, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217762, 0, &UInt32_t1075_0_0_0},
	{"changeValue", 7, 134217763, 0, &UInt32_t1075_0_0_0},
	{"duration", 8, 134217764, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217765, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217766, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int32_t127_Int32_t127_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.Single,System.UInt32,System.UInt32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo UintPlugin_EvaluateAndApply_m5268_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&UintPlugin_EvaluateAndApply_m5268/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int32_t127_Int32_t127_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, UintPlugin_t927_UintPlugin_EvaluateAndApply_m5268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::.ctor()
extern const MethodInfo UintPlugin__ctor_m5269_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UintPlugin__ctor_m5269/* method */
	, &UintPlugin_t927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UintPlugin_t927_MethodInfos[] =
{
	&UintPlugin_Reset_m5263_MethodInfo,
	&UintPlugin_ConvertToStartValue_m5264_MethodInfo,
	&UintPlugin_SetRelativeEndValue_m5265_MethodInfo,
	&UintPlugin_SetChangeValue_m5266_MethodInfo,
	&UintPlugin_GetSpeedBasedDuration_m5267_MethodInfo,
	&UintPlugin_EvaluateAndApply_m5268_MethodInfo,
	&UintPlugin__ctor_m5269_MethodInfo,
	NULL
};
extern const MethodInfo UintPlugin_Reset_m5263_MethodInfo;
extern const MethodInfo UintPlugin_ConvertToStartValue_m5264_MethodInfo;
extern const MethodInfo UintPlugin_SetRelativeEndValue_m5265_MethodInfo;
extern const MethodInfo UintPlugin_SetChangeValue_m5266_MethodInfo;
extern const MethodInfo UintPlugin_GetSpeedBasedDuration_m5267_MethodInfo;
extern const MethodInfo UintPlugin_EvaluateAndApply_m5268_MethodInfo;
static const Il2CppMethodReference UintPlugin_t927_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UintPlugin_Reset_m5263_MethodInfo,
	&UintPlugin_ConvertToStartValue_m5264_MethodInfo,
	&UintPlugin_SetRelativeEndValue_m5265_MethodInfo,
	&UintPlugin_SetChangeValue_m5266_MethodInfo,
	&UintPlugin_GetSpeedBasedDuration_m5267_MethodInfo,
	&UintPlugin_EvaluateAndApply_m5268_MethodInfo,
};
static bool UintPlugin_t927_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UintPlugin_t927_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UintPlugin_t927_0_0_0;
extern const Il2CppType UintPlugin_t927_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t928_0_0_0;
struct UintPlugin_t927;
const Il2CppTypeDefinitionMetadata UintPlugin_t927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UintPlugin_t927_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t928_0_0_0/* parent */
	, UintPlugin_t927_VTable/* vtableMethods */
	, UintPlugin_t927_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UintPlugin_t927_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UintPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, UintPlugin_t927_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UintPlugin_t927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UintPlugin_t927_0_0_0/* byval_arg */
	, &UintPlugin_t927_1_0_0/* this_arg */
	, &UintPlugin_t927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UintPlugin_t927)/* instance_size */
	, sizeof (UintPlugin_t927)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// Metadata Definition DG.Tweening.Core.Enums.UpdateMode
extern TypeInfo UpdateMode_t929_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateModeMethodDeclarations.h"
static const MethodInfo* UpdateMode_t929_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UpdateMode_t929_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UpdateMode_t929_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UpdateMode_t929_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateMode_t929_0_0_0;
extern const Il2CppType UpdateMode_t929_1_0_0;
const Il2CppTypeDefinitionMetadata UpdateMode_t929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateMode_t929_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UpdateMode_t929_VTable/* vtableMethods */
	, UpdateMode_t929_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 4/* fieldStart */

};
TypeInfo UpdateMode_t929_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateMode"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, UpdateMode_t929_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateMode_t929_0_0_0/* byval_arg */
	, &UpdateMode_t929_1_0_0/* this_arg */
	, &UpdateMode_t929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateMode_t929)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateMode_t929)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenType.h"
// Metadata Definition DG.Tweening.TweenType
extern TypeInfo TweenType_t930_il2cpp_TypeInfo;
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenTypeMethodDeclarations.h"
static const MethodInfo* TweenType_t930_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TweenType_t930_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TweenType_t930_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweenType_t930_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenType_t930_0_0_0;
extern const Il2CppType TweenType_t930_1_0_0;
const Il2CppTypeDefinitionMetadata TweenType_t930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenType_t930_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TweenType_t930_VTable/* vtableMethods */
	, TweenType_t930_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 8/* fieldStart */

};
TypeInfo TweenType_t930_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenType"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenType_t930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenType_t930_0_0_0/* byval_arg */
	, &TweenType_t930_1_0_0/* this_arg */
	, &TweenType_t930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenType_t930)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TweenType_t930)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector2Plugin
extern TypeInfo Vector2Plugin_t931_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1020_0_0_0;
extern const Il2CppType TweenerCore_3_t1020_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_Reset_m5270_ParameterInfos[] = 
{
	{"t", 0, 134217767, 0, &TweenerCore_3_t1020_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_Reset_m5270_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector2Plugin_Reset_m5270/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_Reset_m5270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1020_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_ConvertToStartValue_m5271_ParameterInfos[] = 
{
	{"t", 0, 134217768, 0, &TweenerCore_3_t1020_0_0_0},
	{"value", 1, 134217769, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t10_Object_t_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 DG.Tweening.Plugins.Vector2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector2)
extern const MethodInfo Vector2Plugin_ConvertToStartValue_m5271_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector2Plugin_ConvertToStartValue_m5271/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10_Object_t_Vector2_t10/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_ConvertToStartValue_m5271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1020_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_SetRelativeEndValue_m5272_ParameterInfos[] = 
{
	{"t", 0, 134217770, 0, &TweenerCore_3_t1020_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_SetRelativeEndValue_m5272_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector2Plugin_SetRelativeEndValue_m5272/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_SetRelativeEndValue_m5272_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1020_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_SetChangeValue_m5273_ParameterInfos[] = 
{
	{"t", 0, 134217771, 0, &TweenerCore_3_t1020_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_SetChangeValue_m5273_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector2Plugin_SetChangeValue_m5273/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_SetChangeValue_m5273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_GetSpeedBasedDuration_m5274_ParameterInfos[] = 
{
	{"options", 0, 134217772, 0, &VectorOptions_t1002_0_0_0},
	{"unitsXSecond", 1, 134217773, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217774, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector2)
extern const MethodInfo Vector2Plugin_GetSpeedBasedDuration_m5274_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector2Plugin_GetSpeedBasedDuration_m5274/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector2_t10/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_GetSpeedBasedDuration_m5274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1021_0_0_0;
extern const Il2CppType DOGetter_1_t1021_0_0_0;
extern const Il2CppType DOSetter_1_t1022_0_0_0;
extern const Il2CppType DOSetter_1_t1022_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Vector2Plugin_t931_Vector2Plugin_EvaluateAndApply_m5275_ParameterInfos[] = 
{
	{"options", 0, 134217775, 0, &VectorOptions_t1002_0_0_0},
	{"t", 1, 134217776, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217777, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217778, 0, &DOGetter_1_t1021_0_0_0},
	{"setter", 4, 134217779, 0, &DOSetter_1_t1022_0_0_0},
	{"elapsed", 5, 134217780, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217781, 0, &Vector2_t10_0_0_0},
	{"changeValue", 7, 134217782, 0, &Vector2_t10_0_0_0},
	{"duration", 8, 134217783, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217784, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217785, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector2_t10_Vector2_t10_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,System.Single,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector2Plugin_EvaluateAndApply_m5275_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector2Plugin_EvaluateAndApply_m5275/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector2_t10_Vector2_t10_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, Vector2Plugin_t931_Vector2Plugin_EvaluateAndApply_m5275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::.ctor()
extern const MethodInfo Vector2Plugin__ctor_m5276_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector2Plugin__ctor_m5276/* method */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector2Plugin_t931_MethodInfos[] =
{
	&Vector2Plugin_Reset_m5270_MethodInfo,
	&Vector2Plugin_ConvertToStartValue_m5271_MethodInfo,
	&Vector2Plugin_SetRelativeEndValue_m5272_MethodInfo,
	&Vector2Plugin_SetChangeValue_m5273_MethodInfo,
	&Vector2Plugin_GetSpeedBasedDuration_m5274_MethodInfo,
	&Vector2Plugin_EvaluateAndApply_m5275_MethodInfo,
	&Vector2Plugin__ctor_m5276_MethodInfo,
	NULL
};
extern const MethodInfo Vector2Plugin_Reset_m5270_MethodInfo;
extern const MethodInfo Vector2Plugin_ConvertToStartValue_m5271_MethodInfo;
extern const MethodInfo Vector2Plugin_SetRelativeEndValue_m5272_MethodInfo;
extern const MethodInfo Vector2Plugin_SetChangeValue_m5273_MethodInfo;
extern const MethodInfo Vector2Plugin_GetSpeedBasedDuration_m5274_MethodInfo;
extern const MethodInfo Vector2Plugin_EvaluateAndApply_m5275_MethodInfo;
static const Il2CppMethodReference Vector2Plugin_t931_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Vector2Plugin_Reset_m5270_MethodInfo,
	&Vector2Plugin_ConvertToStartValue_m5271_MethodInfo,
	&Vector2Plugin_SetRelativeEndValue_m5272_MethodInfo,
	&Vector2Plugin_SetChangeValue_m5273_MethodInfo,
	&Vector2Plugin_GetSpeedBasedDuration_m5274_MethodInfo,
	&Vector2Plugin_EvaluateAndApply_m5275_MethodInfo,
};
static bool Vector2Plugin_t931_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector2Plugin_t931_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector2Plugin_t931_0_0_0;
extern const Il2CppType Vector2Plugin_t931_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t932_0_0_0;
struct Vector2Plugin_t931;
const Il2CppTypeDefinitionMetadata Vector2Plugin_t931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector2Plugin_t931_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t932_0_0_0/* parent */
	, Vector2Plugin_t931_VTable/* vtableMethods */
	, Vector2Plugin_t931_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector2Plugin_t931_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector2Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector2Plugin_t931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector2Plugin_t931_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector2Plugin_t931_0_0_0/* byval_arg */
	, &Vector2Plugin_t931_1_0_0/* this_arg */
	, &Vector2Plugin_t931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector2Plugin_t931)/* instance_size */
	, sizeof (Vector2Plugin_t931)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.NoOptions
extern TypeInfo NoOptions_t933_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptionsMethodDeclarations.h"
static const MethodInfo* NoOptions_t933_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference NoOptions_t933_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool NoOptions_t933_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType NoOptions_t933_1_0_0;
const Il2CppTypeDefinitionMetadata NoOptions_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, NoOptions_t933_VTable/* vtableMethods */
	, NoOptions_t933_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NoOptions_t933_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "NoOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, NoOptions_t933_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NoOptions_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NoOptions_t933_0_0_0/* byval_arg */
	, &NoOptions_t933_1_0_0/* this_arg */
	, &NoOptions_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NoOptions_t933)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NoOptions_t933)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(NoOptions_t933 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.IDOTweenInit
extern TypeInfo IDOTweenInit_t1023_il2cpp_TypeInfo;
static const MethodInfo* IDOTweenInit_t1023_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType IDOTweenInit_t1023_0_0_0;
extern const Il2CppType IDOTweenInit_t1023_1_0_0;
struct IDOTweenInit_t1023;
const Il2CppTypeDefinitionMetadata IDOTweenInit_t1023_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IDOTweenInit_t1023_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDOTweenInit"/* name */
	, "DG.Tweening"/* namespaze */
	, IDOTweenInit_t1023_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IDOTweenInit_t1023_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IDOTweenInit_t1023_0_0_0/* byval_arg */
	, &IDOTweenInit_t1023_1_0_0/* this_arg */
	, &IDOTweenInit_t1023_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompleti.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
extern TypeInfo U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompletiMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::MoveNext()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_MoveNext_m5277_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_MoveNext_m5277/* method */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278/* method */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 4/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279/* method */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280/* method */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 5/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForCompletionU3Ed__0_t936_U3CWaitForCompletionU3Ed__0__ctor_m5281_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217796, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::.ctor(System.Int32)
extern const MethodInfo U3CWaitForCompletionU3Ed__0__ctor_m5281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0__ctor_m5281/* method */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForCompletionU3Ed__0_t936_U3CWaitForCompletionU3Ed__0__ctor_m5281_ParameterInfos/* parameters */
	, 6/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForCompletionU3Ed__0_t936_MethodInfos[] =
{
	&U3CWaitForCompletionU3Ed__0_MoveNext_m5277_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280_MethodInfo,
	&U3CWaitForCompletionU3Ed__0__ctor_m5281_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278_MethodInfo;
static const PropertyInfo U3CWaitForCompletionU3Ed__0_t936____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280_MethodInfo;
static const PropertyInfo U3CWaitForCompletionU3Ed__0_t936____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForCompletionU3Ed__0_t936_PropertyInfos[] =
{
	&U3CWaitForCompletionU3Ed__0_t936____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForCompletionU3Ed__0_t936____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_MoveNext_m5277_MethodInfo;
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279_MethodInfo;
static const Il2CppMethodReference U3CWaitForCompletionU3Ed__0_t936_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_MoveNext_m5277_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5279_MethodInfo,
};
static bool U3CWaitForCompletionU3Ed__0_t936_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_1_t528_0_0_0;
extern const Il2CppType IEnumerator_t410_0_0_0;
extern const Il2CppType IDisposable_t144_0_0_0;
static const Il2CppType* U3CWaitForCompletionU3Ed__0_t936_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForCompletionU3Ed__0_t936_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForCompletionU3Ed__0_t936_0_0_0;
extern const Il2CppType U3CWaitForCompletionU3Ed__0_t936_1_0_0;
extern TypeInfo DOTweenComponent_t935_il2cpp_TypeInfo;
extern const Il2CppType DOTweenComponent_t935_0_0_0;
struct U3CWaitForCompletionU3Ed__0_t936;
const Il2CppTypeDefinitionMetadata U3CWaitForCompletionU3Ed__0_t936_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForCompletionU3Ed__0_t936_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForCompletionU3Ed__0_t936_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForCompletionU3Ed__0_t936_VTable/* vtableMethods */
	, U3CWaitForCompletionU3Ed__0_t936_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 12/* fieldStart */

};
TypeInfo U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForCompletion>d__0"/* name */
	, ""/* namespaze */
	, U3CWaitForCompletionU3Ed__0_t936_MethodInfos/* methods */
	, U3CWaitForCompletionU3Ed__0_t936_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForCompletionU3Ed__0_t936_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &U3CWaitForCompletionU3Ed__0_t936_0_0_0/* byval_arg */
	, &U3CWaitForCompletionU3Ed__0_t936_1_0_0/* this_arg */
	, &U3CWaitForCompletionU3Ed__0_t936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForCompletionU3Ed__0_t936)/* instance_size */
	, sizeof (U3CWaitForCompletionU3Ed__0_t936)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
extern TypeInfo U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3MethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::MoveNext()
extern const MethodInfo U3CWaitForRewindU3Ed__2_MoveNext_m5282_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_MoveNext_m5282/* method */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283/* method */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 8/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284/* method */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285/* method */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 9/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForRewindU3Ed__2_t937_U3CWaitForRewindU3Ed__2__ctor_m5286_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217797, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::.ctor(System.Int32)
extern const MethodInfo U3CWaitForRewindU3Ed__2__ctor_m5286_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2__ctor_m5286/* method */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForRewindU3Ed__2_t937_U3CWaitForRewindU3Ed__2__ctor_m5286_ParameterInfos/* parameters */
	, 10/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForRewindU3Ed__2_t937_MethodInfos[] =
{
	&U3CWaitForRewindU3Ed__2_MoveNext_m5282_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285_MethodInfo,
	&U3CWaitForRewindU3Ed__2__ctor_m5286_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283_MethodInfo;
static const PropertyInfo U3CWaitForRewindU3Ed__2_t937____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285_MethodInfo;
static const PropertyInfo U3CWaitForRewindU3Ed__2_t937____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForRewindU3Ed__2_t937_PropertyInfos[] =
{
	&U3CWaitForRewindU3Ed__2_t937____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForRewindU3Ed__2_t937____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForRewindU3Ed__2_MoveNext_m5282_MethodInfo;
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284_MethodInfo;
static const Il2CppMethodReference U3CWaitForRewindU3Ed__2_t937_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285_MethodInfo,
	&U3CWaitForRewindU3Ed__2_MoveNext_m5282_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5284_MethodInfo,
};
static bool U3CWaitForRewindU3Ed__2_t937_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForRewindU3Ed__2_t937_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForRewindU3Ed__2_t937_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForRewindU3Ed__2_t937_0_0_0;
extern const Il2CppType U3CWaitForRewindU3Ed__2_t937_1_0_0;
struct U3CWaitForRewindU3Ed__2_t937;
const Il2CppTypeDefinitionMetadata U3CWaitForRewindU3Ed__2_t937_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForRewindU3Ed__2_t937_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForRewindU3Ed__2_t937_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForRewindU3Ed__2_t937_VTable/* vtableMethods */
	, U3CWaitForRewindU3Ed__2_t937_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 16/* fieldStart */

};
TypeInfo U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForRewind>d__2"/* name */
	, ""/* namespaze */
	, U3CWaitForRewindU3Ed__2_t937_MethodInfos/* methods */
	, U3CWaitForRewindU3Ed__2_t937_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForRewindU3Ed__2_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 7/* custom_attributes_cache */
	, &U3CWaitForRewindU3Ed__2_t937_0_0_0/* byval_arg */
	, &U3CWaitForRewindU3Ed__2_t937_1_0_0/* this_arg */
	, &U3CWaitForRewindU3Ed__2_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForRewindU3Ed__2_t937)/* instance_size */
	, sizeof (U3CWaitForRewindU3Ed__2_t937)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3Ed.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
extern TypeInfo U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3EdMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::MoveNext()
extern const MethodInfo U3CWaitForKillU3Ed__4_MoveNext_m5287_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_MoveNext_m5287/* method */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288/* method */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 12/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289/* method */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290/* method */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 13/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForKillU3Ed__4_t938_U3CWaitForKillU3Ed__4__ctor_m5291_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217798, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::.ctor(System.Int32)
extern const MethodInfo U3CWaitForKillU3Ed__4__ctor_m5291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4__ctor_m5291/* method */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForKillU3Ed__4_t938_U3CWaitForKillU3Ed__4__ctor_m5291_ParameterInfos/* parameters */
	, 14/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForKillU3Ed__4_t938_MethodInfos[] =
{
	&U3CWaitForKillU3Ed__4_MoveNext_m5287_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290_MethodInfo,
	&U3CWaitForKillU3Ed__4__ctor_m5291_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288_MethodInfo;
static const PropertyInfo U3CWaitForKillU3Ed__4_t938____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290_MethodInfo;
static const PropertyInfo U3CWaitForKillU3Ed__4_t938____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForKillU3Ed__4_t938_PropertyInfos[] =
{
	&U3CWaitForKillU3Ed__4_t938____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForKillU3Ed__4_t938____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForKillU3Ed__4_MoveNext_m5287_MethodInfo;
extern const MethodInfo U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289_MethodInfo;
static const Il2CppMethodReference U3CWaitForKillU3Ed__4_t938_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290_MethodInfo,
	&U3CWaitForKillU3Ed__4_MoveNext_m5287_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289_MethodInfo,
};
static bool U3CWaitForKillU3Ed__4_t938_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForKillU3Ed__4_t938_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForKillU3Ed__4_t938_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForKillU3Ed__4_t938_0_0_0;
extern const Il2CppType U3CWaitForKillU3Ed__4_t938_1_0_0;
struct U3CWaitForKillU3Ed__4_t938;
const Il2CppTypeDefinitionMetadata U3CWaitForKillU3Ed__4_t938_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForKillU3Ed__4_t938_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForKillU3Ed__4_t938_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForKillU3Ed__4_t938_VTable/* vtableMethods */
	, U3CWaitForKillU3Ed__4_t938_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 20/* fieldStart */

};
TypeInfo U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForKill>d__4"/* name */
	, ""/* namespaze */
	, U3CWaitForKillU3Ed__4_t938_MethodInfos/* methods */
	, U3CWaitForKillU3Ed__4_t938_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForKillU3Ed__4_t938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 11/* custom_attributes_cache */
	, &U3CWaitForKillU3Ed__4_t938_0_0_0/* byval_arg */
	, &U3CWaitForKillU3Ed__4_t938_1_0_0/* this_arg */
	, &U3CWaitForKillU3Ed__4_t938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForKillU3Ed__4_t938)/* instance_size */
	, sizeof (U3CWaitForKillU3Ed__4_t938)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedL.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
extern TypeInfo U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedLMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::MoveNext()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5292_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5292/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 16/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5294_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5294/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 17/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForElapsedLoopsU3Ed__6_t939_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217799, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::.ctor(System.Int32)
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296_ParameterInfos/* parameters */
	, 18/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForElapsedLoopsU3Ed__6_t939_MethodInfos[] =
{
	&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5292_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5294_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293_MethodInfo;
static const PropertyInfo U3CWaitForElapsedLoopsU3Ed__6_t939____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295_MethodInfo;
static const PropertyInfo U3CWaitForElapsedLoopsU3Ed__6_t939____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForElapsedLoopsU3Ed__6_t939_PropertyInfos[] =
{
	&U3CWaitForElapsedLoopsU3Ed__6_t939____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_t939____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5292_MethodInfo;
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5294_MethodInfo;
static const Il2CppMethodReference U3CWaitForElapsedLoopsU3Ed__6_t939_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5292_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5294_MethodInfo,
};
static bool U3CWaitForElapsedLoopsU3Ed__6_t939_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForElapsedLoopsU3Ed__6_t939_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForElapsedLoopsU3Ed__6_t939_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForElapsedLoopsU3Ed__6_t939_0_0_0;
extern const Il2CppType U3CWaitForElapsedLoopsU3Ed__6_t939_1_0_0;
struct U3CWaitForElapsedLoopsU3Ed__6_t939;
const Il2CppTypeDefinitionMetadata U3CWaitForElapsedLoopsU3Ed__6_t939_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_VTable/* vtableMethods */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 24/* fieldStart */

};
TypeInfo U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForElapsedLoops>d__6"/* name */
	, ""/* namespaze */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_MethodInfos/* methods */
	, U3CWaitForElapsedLoopsU3Ed__6_t939_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 15/* custom_attributes_cache */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_0_0_0/* byval_arg */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_1_0_0/* this_arg */
	, &U3CWaitForElapsedLoopsU3Ed__6_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForElapsedLoopsU3Ed__6_t939)/* instance_size */
	, sizeof (U3CWaitForElapsedLoopsU3Ed__6_t939)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPosition.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
extern TypeInfo U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPositionMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::MoveNext()
extern const MethodInfo U3CWaitForPositionU3Ed__8_MoveNext_m5297_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_MoveNext_m5297/* method */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298/* method */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 20/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5299_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5299/* method */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300/* method */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 21/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForPositionU3Ed__8_t940_U3CWaitForPositionU3Ed__8__ctor_m5301_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217800, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::.ctor(System.Int32)
extern const MethodInfo U3CWaitForPositionU3Ed__8__ctor_m5301_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8__ctor_m5301/* method */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForPositionU3Ed__8_t940_U3CWaitForPositionU3Ed__8__ctor_m5301_ParameterInfos/* parameters */
	, 22/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForPositionU3Ed__8_t940_MethodInfos[] =
{
	&U3CWaitForPositionU3Ed__8_MoveNext_m5297_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5299_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300_MethodInfo,
	&U3CWaitForPositionU3Ed__8__ctor_m5301_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298_MethodInfo;
static const PropertyInfo U3CWaitForPositionU3Ed__8_t940____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300_MethodInfo;
static const PropertyInfo U3CWaitForPositionU3Ed__8_t940____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForPositionU3Ed__8_t940_PropertyInfos[] =
{
	&U3CWaitForPositionU3Ed__8_t940____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForPositionU3Ed__8_t940____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForPositionU3Ed__8_MoveNext_m5297_MethodInfo;
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5299_MethodInfo;
static const Il2CppMethodReference U3CWaitForPositionU3Ed__8_t940_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300_MethodInfo,
	&U3CWaitForPositionU3Ed__8_MoveNext_m5297_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5299_MethodInfo,
};
static bool U3CWaitForPositionU3Ed__8_t940_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForPositionU3Ed__8_t940_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForPositionU3Ed__8_t940_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForPositionU3Ed__8_t940_0_0_0;
extern const Il2CppType U3CWaitForPositionU3Ed__8_t940_1_0_0;
struct U3CWaitForPositionU3Ed__8_t940;
const Il2CppTypeDefinitionMetadata U3CWaitForPositionU3Ed__8_t940_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForPositionU3Ed__8_t940_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForPositionU3Ed__8_t940_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForPositionU3Ed__8_t940_VTable/* vtableMethods */
	, U3CWaitForPositionU3Ed__8_t940_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 29/* fieldStart */

};
TypeInfo U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForPosition>d__8"/* name */
	, ""/* namespaze */
	, U3CWaitForPositionU3Ed__8_t940_MethodInfos/* methods */
	, U3CWaitForPositionU3Ed__8_t940_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForPositionU3Ed__8_t940_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 19/* custom_attributes_cache */
	, &U3CWaitForPositionU3Ed__8_t940_0_0_0/* byval_arg */
	, &U3CWaitForPositionU3Ed__8_t940_1_0_0/* this_arg */
	, &U3CWaitForPositionU3Ed__8_t940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForPositionU3Ed__8_t940)/* instance_size */
	, sizeof (U3CWaitForPositionU3Ed__8_t940)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3E.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
extern TypeInfo U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3EMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::MoveNext()
extern const MethodInfo U3CWaitForStartU3Ed__a_MoveNext_m5302_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_MoveNext_m5302/* method */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303/* method */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 24/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5304_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5304/* method */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305/* method */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 25/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo U3CWaitForStartU3Ed__a_t941_U3CWaitForStartU3Ed__a__ctor_m5306_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217801, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::.ctor(System.Int32)
extern const MethodInfo U3CWaitForStartU3Ed__a__ctor_m5306_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a__ctor_m5306/* method */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, U3CWaitForStartU3Ed__a_t941_U3CWaitForStartU3Ed__a__ctor_m5306_ParameterInfos/* parameters */
	, 26/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForStartU3Ed__a_t941_MethodInfos[] =
{
	&U3CWaitForStartU3Ed__a_MoveNext_m5302_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5304_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305_MethodInfo,
	&U3CWaitForStartU3Ed__a__ctor_m5306_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303_MethodInfo;
static const PropertyInfo U3CWaitForStartU3Ed__a_t941____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305_MethodInfo;
static const PropertyInfo U3CWaitForStartU3Ed__a_t941____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForStartU3Ed__a_t941_PropertyInfos[] =
{
	&U3CWaitForStartU3Ed__a_t941____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForStartU3Ed__a_t941____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForStartU3Ed__a_MoveNext_m5302_MethodInfo;
extern const MethodInfo U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5304_MethodInfo;
static const Il2CppMethodReference U3CWaitForStartU3Ed__a_t941_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305_MethodInfo,
	&U3CWaitForStartU3Ed__a_MoveNext_m5302_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5304_MethodInfo,
};
static bool U3CWaitForStartU3Ed__a_t941_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForStartU3Ed__a_t941_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t528_0_0_0,
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForStartU3Ed__a_t941_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t528_0_0_0, 4},
	{ &IEnumerator_t410_0_0_0, 5},
	{ &IDisposable_t144_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForStartU3Ed__a_t941_0_0_0;
extern const Il2CppType U3CWaitForStartU3Ed__a_t941_1_0_0;
struct U3CWaitForStartU3Ed__a_t941;
const Il2CppTypeDefinitionMetadata U3CWaitForStartU3Ed__a_t941_DefinitionMetadata = 
{
	&DOTweenComponent_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForStartU3Ed__a_t941_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForStartU3Ed__a_t941_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForStartU3Ed__a_t941_VTable/* vtableMethods */
	, U3CWaitForStartU3Ed__a_t941_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 34/* fieldStart */

};
TypeInfo U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForStart>d__a"/* name */
	, ""/* namespaze */
	, U3CWaitForStartU3Ed__a_t941_MethodInfos/* methods */
	, U3CWaitForStartU3Ed__a_t941_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForStartU3Ed__a_t941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 23/* custom_attributes_cache */
	, &U3CWaitForStartU3Ed__a_t941_0_0_0/* byval_arg */
	, &U3CWaitForStartU3Ed__a_t941_1_0_0/* this_arg */
	, &U3CWaitForStartU3Ed__a_t941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForStartU3Ed__a_t941)/* instance_size */
	, sizeof (U3CWaitForStartU3Ed__a_t941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponent.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Awake()
extern const MethodInfo DOTweenComponent_Awake_m5307_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DOTweenComponent_Awake_m5307/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Start()
extern const MethodInfo DOTweenComponent_Start_m5308_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DOTweenComponent_Start_m5308/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Update()
extern const MethodInfo DOTweenComponent_Update_m5309_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&DOTweenComponent_Update_m5309/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::LateUpdate()
extern const MethodInfo DOTweenComponent_LateUpdate_m5310_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&DOTweenComponent_LateUpdate_m5310/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::FixedUpdate()
extern const MethodInfo DOTweenComponent_FixedUpdate_m5311_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&DOTweenComponent_FixedUpdate_m5311/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnLevelWasLoaded()
extern const MethodInfo DOTweenComponent_OnLevelWasLoaded_m5312_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&DOTweenComponent_OnLevelWasLoaded_m5312/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnDrawGizmos()
extern const MethodInfo DOTweenComponent_OnDrawGizmos_m5313_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&DOTweenComponent_OnDrawGizmos_m5313/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnDestroy()
extern const MethodInfo DOTweenComponent_OnDestroy_m5314_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DOTweenComponent_OnDestroy_m5314/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnApplicationQuit()
extern const MethodInfo DOTweenComponent_OnApplicationQuit_m5315_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&DOTweenComponent_OnApplicationQuit_m5315/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_SetCapacity_m5316_ParameterInfos[] = 
{
	{"tweenersCapacity", 0, 134217786, 0, &Int32_t127_0_0_0},
	{"sequencesCapacity", 1, 134217787, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.Core.DOTweenComponent::SetCapacity(System.Int32,System.Int32)
extern const MethodInfo DOTweenComponent_SetCapacity_m5316_MethodInfo = 
{
	"SetCapacity"/* name */
	, (methodPointerType)&DOTweenComponent_SetCapacity_m5316/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_SetCapacity_m5316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForCompletion_m5317_ParameterInfos[] = 
{
	{"t", 0, 134217788, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForCompletion(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForCompletion_m5317_MethodInfo = 
{
	"WaitForCompletion"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForCompletion_m5317/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForCompletion_m5317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForRewind_m5318_ParameterInfos[] = 
{
	{"t", 0, 134217789, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForRewind(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForRewind_m5318_MethodInfo = 
{
	"WaitForRewind"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForRewind_m5318/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForRewind_m5318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForKill_m5319_ParameterInfos[] = 
{
	{"t", 0, 134217790, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForKill(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForKill_m5319_MethodInfo = 
{
	"WaitForKill"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForKill_m5319/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForKill_m5319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForElapsedLoops_m5320_ParameterInfos[] = 
{
	{"t", 0, 134217791, 0, &Tween_t934_0_0_0},
	{"elapsedLoops", 1, 134217792, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern const MethodInfo DOTweenComponent_WaitForElapsedLoops_m5320_MethodInfo = 
{
	"WaitForElapsedLoops"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForElapsedLoops_m5320/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForElapsedLoops_m5320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForPosition_m5321_ParameterInfos[] = 
{
	{"t", 0, 134217793, 0, &Tween_t934_0_0_0},
	{"position", 1, 134217794, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForPosition(DG.Tweening.Tween,System.Single)
extern const MethodInfo DOTweenComponent_WaitForPosition_m5321_MethodInfo = 
{
	"WaitForPosition"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForPosition_m5321/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForPosition_m5321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo DOTweenComponent_t935_DOTweenComponent_WaitForStart_m5322_ParameterInfos[] = 
{
	{"t", 0, 134217795, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForStart(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForStart_m5322_MethodInfo = 
{
	"WaitForStart"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForStart_m5322/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t935_DOTweenComponent_WaitForStart_m5322_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Create()
extern const MethodInfo DOTweenComponent_Create_m5323_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DOTweenComponent_Create_m5323/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::DestroyInstance()
extern const MethodInfo DOTweenComponent_DestroyInstance_m5324_MethodInfo = 
{
	"DestroyInstance"/* name */
	, (methodPointerType)&DOTweenComponent_DestroyInstance_m5324/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::.ctor()
extern const MethodInfo DOTweenComponent__ctor_m5325_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DOTweenComponent__ctor_m5325/* method */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOTweenComponent_t935_MethodInfos[] =
{
	&DOTweenComponent_Awake_m5307_MethodInfo,
	&DOTweenComponent_Start_m5308_MethodInfo,
	&DOTweenComponent_Update_m5309_MethodInfo,
	&DOTweenComponent_LateUpdate_m5310_MethodInfo,
	&DOTweenComponent_FixedUpdate_m5311_MethodInfo,
	&DOTweenComponent_OnLevelWasLoaded_m5312_MethodInfo,
	&DOTweenComponent_OnDrawGizmos_m5313_MethodInfo,
	&DOTweenComponent_OnDestroy_m5314_MethodInfo,
	&DOTweenComponent_OnApplicationQuit_m5315_MethodInfo,
	&DOTweenComponent_SetCapacity_m5316_MethodInfo,
	&DOTweenComponent_WaitForCompletion_m5317_MethodInfo,
	&DOTweenComponent_WaitForRewind_m5318_MethodInfo,
	&DOTweenComponent_WaitForKill_m5319_MethodInfo,
	&DOTweenComponent_WaitForElapsedLoops_m5320_MethodInfo,
	&DOTweenComponent_WaitForPosition_m5321_MethodInfo,
	&DOTweenComponent_WaitForStart_m5322_MethodInfo,
	&DOTweenComponent_Create_m5323_MethodInfo,
	&DOTweenComponent_DestroyInstance_m5324_MethodInfo,
	&DOTweenComponent__ctor_m5325_MethodInfo,
	NULL
};
static const Il2CppType* DOTweenComponent_t935_il2cpp_TypeInfo__nestedTypes[6] =
{
	&U3CWaitForCompletionU3Ed__0_t936_0_0_0,
	&U3CWaitForRewindU3Ed__2_t937_0_0_0,
	&U3CWaitForKillU3Ed__4_t938_0_0_0,
	&U3CWaitForElapsedLoopsU3Ed__6_t939_0_0_0,
	&U3CWaitForPositionU3Ed__8_t940_0_0_0,
	&U3CWaitForStartU3Ed__a_t941_0_0_0,
};
extern const MethodInfo Object_Equals_m537_MethodInfo;
extern const MethodInfo Object_GetHashCode_m538_MethodInfo;
extern const MethodInfo Object_ToString_m539_MethodInfo;
extern const MethodInfo DOTweenComponent_SetCapacity_m5316_MethodInfo;
static const Il2CppMethodReference DOTweenComponent_t935_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&DOTweenComponent_SetCapacity_m5316_MethodInfo,
};
static bool DOTweenComponent_t935_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* DOTweenComponent_t935_InterfacesTypeInfos[] = 
{
	&IDOTweenInit_t1023_0_0_0,
};
static Il2CppInterfaceOffsetPair DOTweenComponent_t935_InterfacesOffsets[] = 
{
	{ &IDOTweenInit_t1023_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTweenComponent_t935_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct DOTweenComponent_t935;
const Il2CppTypeDefinitionMetadata DOTweenComponent_t935_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DOTweenComponent_t935_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DOTweenComponent_t935_InterfacesTypeInfos/* implementedInterfaces */
	, DOTweenComponent_t935_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DOTweenComponent_t935_VTable/* vtableMethods */
	, DOTweenComponent_t935_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 38/* fieldStart */

};
TypeInfo DOTweenComponent_t935_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTweenComponent"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOTweenComponent_t935_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOTweenComponent_t935_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &DOTweenComponent_t935_0_0_0/* byval_arg */
	, &DOTweenComponent_t935_1_0_0/* this_arg */
	, &DOTweenComponent_t935_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTweenComponent_t935)/* instance_size */
	, sizeof (DOTweenComponent_t935)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_Debugger.h"
// Metadata Definition DG.Tweening.Core.Debugger
extern TypeInfo Debugger_t942_il2cpp_TypeInfo;
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_DebuggerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_Log_m5326_ParameterInfos[] = 
{
	{"message", 0, 134217802, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::Log(System.Object)
extern const MethodInfo Debugger_Log_m5326_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Debugger_Log_m5326/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Debugger_t942_Debugger_Log_m5326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_LogWarning_m5327_ParameterInfos[] = 
{
	{"message", 0, 134217803, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern const MethodInfo Debugger_LogWarning_m5327_MethodInfo = 
{
	"LogWarning"/* name */
	, (methodPointerType)&Debugger_LogWarning_m5327/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Debugger_t942_Debugger_LogWarning_m5327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_LogError_m5328_ParameterInfos[] = 
{
	{"message", 0, 134217804, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogError(System.Object)
extern const MethodInfo Debugger_LogError_m5328_MethodInfo = 
{
	"LogError"/* name */
	, (methodPointerType)&Debugger_LogError_m5328/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Debugger_t942_Debugger_LogError_m5328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_LogReport_m5329_ParameterInfos[] = 
{
	{"message", 0, 134217805, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogReport(System.Object)
extern const MethodInfo Debugger_LogReport_m5329_MethodInfo = 
{
	"LogReport"/* name */
	, (methodPointerType)&Debugger_LogReport_m5329/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Debugger_t942_Debugger_LogReport_m5329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_LogInvalidTween_m5330_ParameterInfos[] = 
{
	{"t", 0, 134217806, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern const MethodInfo Debugger_LogInvalidTween_m5330_MethodInfo = 
{
	"LogInvalidTween"/* name */
	, (methodPointerType)&Debugger_LogInvalidTween_m5330/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Debugger_t942_Debugger_LogInvalidTween_m5330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogBehaviour_t956_0_0_0;
extern const Il2CppType LogBehaviour_t956_0_0_0;
static const ParameterInfo Debugger_t942_Debugger_SetLogPriority_m5331_ParameterInfos[] = 
{
	{"logBehaviour", 0, 134217807, 0, &LogBehaviour_t956_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::SetLogPriority(DG.Tweening.LogBehaviour)
extern const MethodInfo Debugger_SetLogPriority_m5331_MethodInfo = 
{
	"SetLogPriority"/* name */
	, (methodPointerType)&Debugger_SetLogPriority_m5331/* method */
	, &Debugger_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Debugger_t942_Debugger_SetLogPriority_m5331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Debugger_t942_MethodInfos[] =
{
	&Debugger_Log_m5326_MethodInfo,
	&Debugger_LogWarning_m5327_MethodInfo,
	&Debugger_LogError_m5328_MethodInfo,
	&Debugger_LogReport_m5329_MethodInfo,
	&Debugger_LogInvalidTween_m5330_MethodInfo,
	&Debugger_SetLogPriority_m5331_MethodInfo,
	NULL
};
static const Il2CppMethodReference Debugger_t942_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Debugger_t942_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Debugger_t942_0_0_0;
extern const Il2CppType Debugger_t942_1_0_0;
struct Debugger_t942;
const Il2CppTypeDefinitionMetadata Debugger_t942_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Debugger_t942_VTable/* vtableMethods */
	, Debugger_t942_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 42/* fieldStart */

};
TypeInfo Debugger_t942_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Debugger"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, Debugger_t942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Debugger_t942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Debugger_t942_0_0_0/* byval_arg */
	, &Debugger_t942_1_0_0/* this_arg */
	, &Debugger_t942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Debugger_t942)/* instance_size */
	, sizeof (Debugger_t942)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Debugger_t942_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiable.h"
// Metadata Definition DG.Tweening.Core.ABSSequentiable
extern TypeInfo ABSSequentiable_t943_il2cpp_TypeInfo;
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.ABSSequentiable::.ctor()
extern const MethodInfo ABSSequentiable__ctor_m5332_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ABSSequentiable__ctor_m5332/* method */
	, &ABSSequentiable_t943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ABSSequentiable_t943_MethodInfos[] =
{
	&ABSSequentiable__ctor_m5332_MethodInfo,
	NULL
};
static const Il2CppMethodReference ABSSequentiable_t943_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ABSSequentiable_t943_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ABSSequentiable_t943_0_0_0;
extern const Il2CppType ABSSequentiable_t943_1_0_0;
struct ABSSequentiable_t943;
const Il2CppTypeDefinitionMetadata ABSSequentiable_t943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ABSSequentiable_t943_VTable/* vtableMethods */
	, ABSSequentiable_t943_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 43/* fieldStart */

};
TypeInfo ABSSequentiable_t943_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ABSSequentiable"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, ABSSequentiable_t943_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ABSSequentiable_t943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ABSSequentiable_t943_0_0_0/* byval_arg */
	, &ABSSequentiable_t943_1_0_0/* this_arg */
	, &ABSSequentiable_t943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ABSSequentiable_t943)/* instance_size */
	, sizeof (ABSSequentiable_t943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// Metadata Definition DG.Tweening.Tween
extern TypeInfo Tween_t934_il2cpp_TypeInfo;
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_TweenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tween::Reset()
extern const MethodInfo Tween_Reset_m5333_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Tween_Reset_m5333/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 963/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::Validate()
extern const MethodInfo Tween_Validate_m5592_MethodInfo = 
{
	"Validate"/* name */
	, NULL/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Tween_t934_Tween_UpdateDelay_m5334_ParameterInfos[] = 
{
	{"elapsed", 0, 134217808, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Tween::UpdateDelay(System.Single)
extern const MethodInfo Tween_UpdateDelay_m5334_MethodInfo = 
{
	"UpdateDelay"/* name */
	, (methodPointerType)&Tween_UpdateDelay_m5334/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151/* invoker_method */
	, Tween_t934_Tween_UpdateDelay_m5334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 963/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::Startup()
extern const MethodInfo Tween_Startup_m5593_MethodInfo = 
{
	"Startup"/* name */
	, NULL/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Tween_t934_Tween_ApplyTween_m5594_ParameterInfos[] = 
{
	{"prevPosition", 0, 134217809, 0, &Single_t151_0_0_0},
	{"prevCompletedLoops", 1, 134217810, 0, &Int32_t127_0_0_0},
	{"newCompletedSteps", 2, 134217811, 0, &Int32_t127_0_0_0},
	{"useInversePosition", 3, 134217812, 0, &Boolean_t169_0_0_0},
	{"updateMode", 4, 134217813, 0, &UpdateMode_t929_0_0_0},
	{"updateNotice", 5, 134217814, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Tween_ApplyTween_m5594_MethodInfo = 
{
	"ApplyTween"/* name */
	, NULL/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127/* invoker_method */
	, Tween_t934_Tween_ApplyTween_m5594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
static const ParameterInfo Tween_t934_Tween_DoGoto_m5335_ParameterInfos[] = 
{
	{"t", 0, 134217815, 0, &Tween_t934_0_0_0},
	{"toPosition", 1, 134217816, 0, &Single_t151_0_0_0},
	{"toCompletedLoops", 2, 134217817, 0, &Int32_t127_0_0_0},
	{"updateMode", 3, 134217818, 0, &UpdateMode_t929_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::DoGoto(DG.Tweening.Tween,System.Single,System.Int32,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo Tween_DoGoto_m5335_MethodInfo = 
{
	"DoGoto"/* name */
	, (methodPointerType)&Tween_DoGoto_m5335/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Int32_t127_Int32_t127/* invoker_method */
	, Tween_t934_Tween_DoGoto_m5335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_t101_0_0_0;
extern const Il2CppType TweenCallback_t101_0_0_0;
static const ParameterInfo Tween_t934_Tween_OnTweenCallback_m5336_ParameterInfos[] = 
{
	{"callback", 0, 134217819, 0, &TweenCallback_t101_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback)
extern const MethodInfo Tween_OnTweenCallback_m5336_MethodInfo = 
{
	"OnTweenCallback"/* name */
	, (methodPointerType)&Tween_OnTweenCallback_m5336/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Tween_t934_Tween_OnTweenCallback_m5336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tween::.ctor()
extern const MethodInfo Tween__ctor_m5337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Tween__ctor_m5337/* method */
	, &Tween_t934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Tween_t934_MethodInfos[] =
{
	&Tween_Reset_m5333_MethodInfo,
	&Tween_Validate_m5592_MethodInfo,
	&Tween_UpdateDelay_m5334_MethodInfo,
	&Tween_Startup_m5593_MethodInfo,
	&Tween_ApplyTween_m5594_MethodInfo,
	&Tween_DoGoto_m5335_MethodInfo,
	&Tween_OnTweenCallback_m5336_MethodInfo,
	&Tween__ctor_m5337_MethodInfo,
	NULL
};
extern const MethodInfo Tween_Reset_m5333_MethodInfo;
extern const MethodInfo Tween_UpdateDelay_m5334_MethodInfo;
static const Il2CppMethodReference Tween_t934_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Tween_Reset_m5333_MethodInfo,
	NULL,
	&Tween_UpdateDelay_m5334_MethodInfo,
	NULL,
	NULL,
};
static bool Tween_t934_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Tween_t934_1_0_0;
struct Tween_t934;
const Il2CppTypeDefinitionMetadata Tween_t934_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ABSSequentiable_t943_0_0_0/* parent */
	, Tween_t934_VTable/* vtableMethods */
	, Tween_t934_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 47/* fieldStart */

};
TypeInfo Tween_t934_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tween"/* name */
	, "DG.Tweening"/* namespaze */
	, Tween_t934_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tween_t934_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Tween_t934_0_0_0/* byval_arg */
	, &Tween_t934_1_0_0/* this_arg */
	, &Tween_t934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tween_t934)/* instance_size */
	, sizeof (Tween_t934)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 47/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_Sequence.h"
// Metadata Definition DG.Tweening.Sequence
extern TypeInfo Sequence_t122_il2cpp_TypeInfo;
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_SequenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::.ctor()
extern const MethodInfo Sequence__ctor_m5338_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Sequence__ctor_m5338/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_DoInsert_m5339_ParameterInfos[] = 
{
	{"inSequence", 0, 134217820, 0, &Sequence_t122_0_0_0},
	{"t", 1, 134217821, 0, &Tween_t934_0_0_0},
	{"atPosition", 2, 134217822, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern const MethodInfo Sequence_DoInsert_m5339_MethodInfo = 
{
	"DoInsert"/* name */
	, (methodPointerType)&Sequence_DoInsert_m5339/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Single_t151/* invoker_method */
	, Sequence_t122_Sequence_DoInsert_m5339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::Reset()
extern const MethodInfo Sequence_Reset_m5340_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Sequence_Reset_m5340/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::Validate()
extern const MethodInfo Sequence_Validate_m5341_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&Sequence_Validate_m5341/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::Startup()
extern const MethodInfo Sequence_Startup_m5342_MethodInfo = 
{
	"Startup"/* name */
	, (methodPointerType)&Sequence_Startup_m5342/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_ApplyTween_m5343_ParameterInfos[] = 
{
	{"prevPosition", 0, 134217823, 0, &Single_t151_0_0_0},
	{"prevCompletedLoops", 1, 134217824, 0, &Int32_t127_0_0_0},
	{"newCompletedSteps", 2, 134217825, 0, &Int32_t127_0_0_0},
	{"useInversePosition", 3, 134217826, 0, &Boolean_t169_0_0_0},
	{"updateMode", 4, 134217827, 0, &UpdateMode_t929_0_0_0},
	{"updateNotice", 5, 134217828, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Sequence_ApplyTween_m5343_MethodInfo = 
{
	"ApplyTween"/* name */
	, (methodPointerType)&Sequence_ApplyTween_m5343/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127_Int32_t127/* invoker_method */
	, Sequence_t122_Sequence_ApplyTween_m5343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_Setup_m5344_ParameterInfos[] = 
{
	{"s", 0, 134217829, 0, &Sequence_t122_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern const MethodInfo Sequence_Setup_m5344_MethodInfo = 
{
	"Setup"/* name */
	, (methodPointerType)&Sequence_Setup_m5344/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Sequence_t122_Sequence_Setup_m5344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_DoStartup_m5345_ParameterInfos[] = 
{
	{"s", 0, 134217830, 0, &Sequence_t122_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern const MethodInfo Sequence_DoStartup_m5345_MethodInfo = 
{
	"DoStartup"/* name */
	, (methodPointerType)&Sequence_DoStartup_m5345/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Sequence_t122_Sequence_DoStartup_m5345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_DoApplyTween_m5346_ParameterInfos[] = 
{
	{"s", 0, 134217831, 0, &Sequence_t122_0_0_0},
	{"prevPosition", 1, 134217832, 0, &Single_t151_0_0_0},
	{"prevCompletedLoops", 2, 134217833, 0, &Int32_t127_0_0_0},
	{"newCompletedSteps", 3, 134217834, 0, &Int32_t127_0_0_0},
	{"useInversePosition", 4, 134217835, 0, &Boolean_t169_0_0_0},
	{"updateMode", 5, 134217836, 0, &UpdateMode_t929_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo Sequence_DoApplyTween_m5346_MethodInfo = 
{
	"DoApplyTween"/* name */
	, (methodPointerType)&Sequence_DoApplyTween_m5346/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Int32_t127_Int32_t127_SByte_t170_Int32_t127/* invoker_method */
	, Sequence_t122_Sequence_DoApplyTween_m5346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo Sequence_t122_Sequence_ApplyInternalCycle_m5347_ParameterInfos[] = 
{
	{"s", 0, 134217837, 0, &Sequence_t122_0_0_0},
	{"fromPos", 1, 134217838, 0, &Single_t151_0_0_0},
	{"toPos", 2, 134217839, 0, &Single_t151_0_0_0},
	{"updateMode", 3, 134217840, 0, &UpdateMode_t929_0_0_0},
	{"useInverse", 4, 134217841, 0, &Boolean_t169_0_0_0},
	{"prevPosIsInverse", 5, 134217842, 0, &Boolean_t169_0_0_0},
	{"multiCycleStep", 6, 134217843, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Single_t151_Int32_t127_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Sequence_ApplyInternalCycle_m5347_MethodInfo = 
{
	"ApplyInternalCycle"/* name */
	, (methodPointerType)&Sequence_ApplyInternalCycle_m5347/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Single_t151_Single_t151_Int32_t127_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, Sequence_t122_Sequence_ApplyInternalCycle_m5347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSSequentiable_t943_0_0_0;
extern const Il2CppType ABSSequentiable_t943_0_0_0;
static const ParameterInfo Sequence_t122_Sequence_SortSequencedObjs_m5348_ParameterInfos[] = 
{
	{"a", 0, 134217844, 0, &ABSSequentiable_t943_0_0_0},
	{"b", 1, 134217845, 0, &ABSSequentiable_t943_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern const MethodInfo Sequence_SortSequencedObjs_m5348_MethodInfo = 
{
	"SortSequencedObjs"/* name */
	, (methodPointerType)&Sequence_SortSequencedObjs_m5348/* method */
	, &Sequence_t122_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Object_t/* invoker_method */
	, Sequence_t122_Sequence_SortSequencedObjs_m5348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Sequence_t122_MethodInfos[] =
{
	&Sequence__ctor_m5338_MethodInfo,
	&Sequence_DoInsert_m5339_MethodInfo,
	&Sequence_Reset_m5340_MethodInfo,
	&Sequence_Validate_m5341_MethodInfo,
	&Sequence_Startup_m5342_MethodInfo,
	&Sequence_ApplyTween_m5343_MethodInfo,
	&Sequence_Setup_m5344_MethodInfo,
	&Sequence_DoStartup_m5345_MethodInfo,
	&Sequence_DoApplyTween_m5346_MethodInfo,
	&Sequence_ApplyInternalCycle_m5347_MethodInfo,
	&Sequence_SortSequencedObjs_m5348_MethodInfo,
	NULL
};
extern const MethodInfo Sequence_Reset_m5340_MethodInfo;
extern const MethodInfo Sequence_Validate_m5341_MethodInfo;
extern const MethodInfo Sequence_Startup_m5342_MethodInfo;
extern const MethodInfo Sequence_ApplyTween_m5343_MethodInfo;
static const Il2CppMethodReference Sequence_t122_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Sequence_Reset_m5340_MethodInfo,
	&Sequence_Validate_m5341_MethodInfo,
	&Tween_UpdateDelay_m5334_MethodInfo,
	&Sequence_Startup_m5342_MethodInfo,
	&Sequence_ApplyTween_m5343_MethodInfo,
};
static bool Sequence_t122_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Sequence_t122_1_0_0;
struct Sequence_t122;
const Il2CppTypeDefinitionMetadata Sequence_t122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tween_t934_0_0_0/* parent */
	, Sequence_t122_VTable/* vtableMethods */
	, Sequence_t122_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 94/* fieldStart */

};
TypeInfo Sequence_t122_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sequence"/* name */
	, "DG.Tweening"/* namespaze */
	, Sequence_t122_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Sequence_t122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sequence_t122_0_0_0/* byval_arg */
	, &Sequence_t122_1_0_0/* this_arg */
	, &Sequence_t122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sequence_t122)/* instance_size */
	, sizeof (Sequence_t122)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
// Metadata Definition DG.Tweening.RotateMode
extern TypeInfo RotateMode_t948_il2cpp_TypeInfo;
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateModeMethodDeclarations.h"
static const MethodInfo* RotateMode_t948_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RotateMode_t948_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool RotateMode_t948_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RotateMode_t948_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RotateMode_t948_0_0_0;
extern const Il2CppType RotateMode_t948_1_0_0;
const Il2CppTypeDefinitionMetadata RotateMode_t948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RotateMode_t948_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, RotateMode_t948_VTable/* vtableMethods */
	, RotateMode_t948_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 97/* fieldStart */

};
TypeInfo RotateMode_t948_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RotateMode"/* name */
	, "DG.Tweening"/* namespaze */
	, RotateMode_t948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RotateMode_t948_0_0_0/* byval_arg */
	, &RotateMode_t948_1_0_0/* this_arg */
	, &RotateMode_t948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RotateMode_t948)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RotateMode_t948)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector3ArrayPlugin
extern TypeInfo Vector3ArrayPlugin_t949_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_Reset_m5349_ParameterInfos[] = 
{
	{"t", 0, 134217846, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_Reset_m5349_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_Reset_m5349/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_Reset_m5349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_ConvertToStartValue_m5350_ParameterInfos[] = 
{
	{"t", 0, 134217847, 0, &TweenerCore_3_t1024_0_0_0},
	{"value", 1, 134217848, 0, &Vector3_t15_0_0_0},
};
extern const Il2CppType Vector3U5BU5D_t154_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3[] DG.Tweening.Plugins.Vector3ArrayPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,UnityEngine.Vector3)
extern const MethodInfo Vector3ArrayPlugin_ConvertToStartValue_m5350_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_ConvertToStartValue_m5350/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Vector3U5BU5D_t154_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t15/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_ConvertToStartValue_m5350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_SetRelativeEndValue_m5351_ParameterInfos[] = 
{
	{"t", 0, 134217849, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_SetRelativeEndValue_m5351_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_SetRelativeEndValue_m5351/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_SetRelativeEndValue_m5351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_SetChangeValue_m5352_ParameterInfos[] = 
{
	{"t", 0, 134217850, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_SetChangeValue_m5352_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_SetChangeValue_m5352/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_SetChangeValue_m5352_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3ArrayOptions_t951_0_0_0;
extern const Il2CppType Vector3ArrayOptions_t951_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3U5BU5D_t154_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_ParameterInfos[] = 
{
	{"options", 0, 134217851, 0, &Vector3ArrayOptions_t951_0_0_0},
	{"unitsXSecond", 1, 134217852, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217853, 0, &Vector3U5BU5D_t154_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Vector3ArrayOptions_t951_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector3ArrayPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.Vector3ArrayOptions,System.Single,UnityEngine.Vector3[])
extern const MethodInfo Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_GetSpeedBasedDuration_m5353/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Vector3ArrayOptions_t951_Single_t151_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3ArrayOptions_t951_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t120_0_0_0;
extern const Il2CppType DOGetter_1_t120_0_0_0;
extern const Il2CppType DOSetter_1_t121_0_0_0;
extern const Il2CppType DOSetter_1_t121_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3U5BU5D_t154_0_0_0;
extern const Il2CppType Vector3U5BU5D_t154_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t949_Vector3ArrayPlugin_EvaluateAndApply_m5354_ParameterInfos[] = 
{
	{"options", 0, 134217854, 0, &Vector3ArrayOptions_t951_0_0_0},
	{"t", 1, 134217855, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217856, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217857, 0, &DOGetter_1_t120_0_0_0},
	{"setter", 4, 134217858, 0, &DOSetter_1_t121_0_0_0},
	{"elapsed", 5, 134217859, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217860, 0, &Vector3U5BU5D_t154_0_0_0},
	{"changeValue", 7, 134217861, 0, &Vector3U5BU5D_t154_0_0_0},
	{"duration", 8, 134217862, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217863, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217864, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3ArrayOptions_t951_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.Vector3ArrayOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector3ArrayPlugin_EvaluateAndApply_m5354_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_EvaluateAndApply_m5354/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3ArrayOptions_t951_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, Vector3ArrayPlugin_t949_Vector3ArrayPlugin_EvaluateAndApply_m5354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::.ctor()
extern const MethodInfo Vector3ArrayPlugin__ctor_m5355_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector3ArrayPlugin__ctor_m5355/* method */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector3ArrayPlugin_t949_MethodInfos[] =
{
	&Vector3ArrayPlugin_Reset_m5349_MethodInfo,
	&Vector3ArrayPlugin_ConvertToStartValue_m5350_MethodInfo,
	&Vector3ArrayPlugin_SetRelativeEndValue_m5351_MethodInfo,
	&Vector3ArrayPlugin_SetChangeValue_m5352_MethodInfo,
	&Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_MethodInfo,
	&Vector3ArrayPlugin_EvaluateAndApply_m5354_MethodInfo,
	&Vector3ArrayPlugin__ctor_m5355_MethodInfo,
	NULL
};
extern const MethodInfo Vector3ArrayPlugin_Reset_m5349_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_ConvertToStartValue_m5350_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_SetRelativeEndValue_m5351_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_SetChangeValue_m5352_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_EvaluateAndApply_m5354_MethodInfo;
static const Il2CppMethodReference Vector3ArrayPlugin_t949_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Vector3ArrayPlugin_Reset_m5349_MethodInfo,
	&Vector3ArrayPlugin_ConvertToStartValue_m5350_MethodInfo,
	&Vector3ArrayPlugin_SetRelativeEndValue_m5351_MethodInfo,
	&Vector3ArrayPlugin_SetChangeValue_m5352_MethodInfo,
	&Vector3ArrayPlugin_GetSpeedBasedDuration_m5353_MethodInfo,
	&Vector3ArrayPlugin_EvaluateAndApply_m5354_MethodInfo,
};
static bool Vector3ArrayPlugin_t949_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector3ArrayPlugin_t949_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3ArrayPlugin_t949_0_0_0;
extern const Il2CppType Vector3ArrayPlugin_t949_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t950_0_0_0;
struct Vector3ArrayPlugin_t949;
const Il2CppTypeDefinitionMetadata Vector3ArrayPlugin_t949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector3ArrayPlugin_t949_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t950_0_0_0/* parent */
	, Vector3ArrayPlugin_t949_VTable/* vtableMethods */
	, Vector3ArrayPlugin_t949_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector3ArrayPlugin_t949_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3ArrayPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector3ArrayPlugin_t949_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3ArrayPlugin_t949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3ArrayPlugin_t949_0_0_0/* byval_arg */
	, &Vector3ArrayPlugin_t949_1_0_0/* this_arg */
	, &Vector3ArrayPlugin_t949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3ArrayPlugin_t949)/* instance_size */
	, sizeof (Vector3ArrayPlugin_t949)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.Vector3ArrayOptions
extern TypeInfo Vector3ArrayOptions_t951_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptionsMethodDeclarations.h"
static const MethodInfo* Vector3ArrayOptions_t951_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Vector3ArrayOptions_t951_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Vector3ArrayOptions_t951_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3ArrayOptions_t951_1_0_0;
const Il2CppTypeDefinitionMetadata Vector3ArrayOptions_t951_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Vector3ArrayOptions_t951_VTable/* vtableMethods */
	, Vector3ArrayOptions_t951_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 102/* fieldStart */

};
TypeInfo Vector3ArrayOptions_t951_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3ArrayOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, Vector3ArrayOptions_t951_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3ArrayOptions_t951_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3ArrayOptions_t951_0_0_0/* byval_arg */
	, &Vector3ArrayOptions_t951_1_0_0/* this_arg */
	, &Vector3ArrayOptions_t951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Vector3ArrayOptions_t951_marshal/* marshal_to_native_func */
	, (methodPointerType)Vector3ArrayOptions_t951_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Vector3ArrayOptions_t951_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Vector3ArrayOptions_t951)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector3ArrayOptions_t951)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector3ArrayOptions_t951_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_Ease.h"
// Metadata Definition DG.Tweening.Ease
extern TypeInfo Ease_t952_il2cpp_TypeInfo;
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_EaseMethodDeclarations.h"
static const MethodInfo* Ease_t952_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Ease_t952_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Ease_t952_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Ease_t952_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Ease_t952_0_0_0;
extern const Il2CppType Ease_t952_1_0_0;
const Il2CppTypeDefinitionMetadata Ease_t952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Ease_t952_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Ease_t952_VTable/* vtableMethods */
	, Ease_t952_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 105/* fieldStart */

};
TypeInfo Ease_t952_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Ease"/* name */
	, "DG.Tweening"/* namespaze */
	, Ease_t952_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Ease_t952_0_0_0/* byval_arg */
	, &Ease_t952_1_0_0/* this_arg */
	, &Ease_t952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Ease_t952)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Ease_t952)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 35/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocation.h"
// Metadata Definition DG.Tweening.Core.DOTweenSettings/SettingsLocation
extern TypeInfo SettingsLocation_t953_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocationMethodDeclarations.h"
static const MethodInfo* SettingsLocation_t953_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SettingsLocation_t953_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool SettingsLocation_t953_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SettingsLocation_t953_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SettingsLocation_t953_0_0_0;
extern const Il2CppType SettingsLocation_t953_1_0_0;
extern TypeInfo DOTweenSettings_t954_il2cpp_TypeInfo;
extern const Il2CppType DOTweenSettings_t954_0_0_0;
const Il2CppTypeDefinitionMetadata SettingsLocation_t953_DefinitionMetadata = 
{
	&DOTweenSettings_t954_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SettingsLocation_t953_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, SettingsLocation_t953_VTable/* vtableMethods */
	, SettingsLocation_t953_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 140/* fieldStart */

};
TypeInfo SettingsLocation_t953_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SettingsLocation"/* name */
	, ""/* namespaze */
	, SettingsLocation_t953_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SettingsLocation_t953_0_0_0/* byval_arg */
	, &SettingsLocation_t953_1_0_0/* this_arg */
	, &SettingsLocation_t953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SettingsLocation_t953)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SettingsLocation_t953)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettings.h"
// Metadata Definition DG.Tweening.Core.DOTweenSettings
// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettingsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern const MethodInfo DOTweenSettings__ctor_m5356_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DOTweenSettings__ctor_m5356/* method */
	, &DOTweenSettings_t954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOTweenSettings_t954_MethodInfos[] =
{
	&DOTweenSettings__ctor_m5356_MethodInfo,
	NULL
};
static const Il2CppType* DOTweenSettings_t954_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SettingsLocation_t953_0_0_0,
};
static const Il2CppMethodReference DOTweenSettings_t954_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool DOTweenSettings_t954_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTweenSettings_t954_1_0_0;
extern const Il2CppType ScriptableObject_t955_0_0_0;
struct DOTweenSettings_t954;
const Il2CppTypeDefinitionMetadata DOTweenSettings_t954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DOTweenSettings_t954_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t955_0_0_0/* parent */
	, DOTweenSettings_t954_VTable/* vtableMethods */
	, DOTweenSettings_t954_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 144/* fieldStart */

};
TypeInfo DOTweenSettings_t954_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTweenSettings"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOTweenSettings_t954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOTweenSettings_t954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOTweenSettings_t954_0_0_0/* byval_arg */
	, &DOTweenSettings_t954_1_0_0/* this_arg */
	, &DOTweenSettings_t954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTweenSettings_t954)/* instance_size */
	, sizeof (DOTweenSettings_t954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// Metadata Definition DG.Tweening.LogBehaviour
extern TypeInfo LogBehaviour_t956_il2cpp_TypeInfo;
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviourMethodDeclarations.h"
static const MethodInfo* LogBehaviour_t956_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LogBehaviour_t956_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool LogBehaviour_t956_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LogBehaviour_t956_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LogBehaviour_t956_1_0_0;
const Il2CppTypeDefinitionMetadata LogBehaviour_t956_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogBehaviour_t956_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, LogBehaviour_t956_VTable/* vtableMethods */
	, LogBehaviour_t956_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 159/* fieldStart */

};
TypeInfo LogBehaviour_t956_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogBehaviour"/* name */
	, "DG.Tweening"/* namespaze */
	, LogBehaviour_t956_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogBehaviour_t956_0_0_0/* byval_arg */
	, &LogBehaviour_t956_1_0_0/* this_arg */
	, &LogBehaviour_t956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogBehaviour_t956)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LogBehaviour_t956)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensions.h"
// Metadata Definition DG.Tweening.TweenSettingsExtensions
extern TypeInfo TweenSettingsExtensions_t100_il2cpp_TypeInfo;
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensionsMethodDeclarations.h"
extern const Il2CppType TweenSettingsExtensions_SetTarget_m5595_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetTarget_m5595_gp_0_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_SetTarget_m5595_ParameterInfos[] = 
{
	{"t", 0, 134217865, 0, &TweenSettingsExtensions_SetTarget_m5595_gp_0_0_0_0},
	{"target", 1, 134217866, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetTarget_m5595_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetTarget_m5595_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t934_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetTarget_m5595_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericContainer, TweenSettingsExtensions_SetTarget_m5595_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetTarget_m5595_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetTarget_m5595_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetTarget_m5595_MethodInfo, 1, 1, TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetTarget_m5595_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetTarget_m5595_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetTarget(T,System.Object)
extern const MethodInfo TweenSettingsExtensions_SetTarget_m5595_MethodInfo = 
{
	"SetTarget"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetTarget_m5595_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_SetTarget_m5595_ParameterInfos/* parameters */
	, 28/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, TweenSettingsExtensions_SetTarget_m5595_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetTarget_m5595_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenSettingsExtensions_SetLoops_m5596_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetLoops_m5596_gp_0_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LoopType_t995_0_0_0;
extern const Il2CppType LoopType_t995_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_SetLoops_m5596_ParameterInfos[] = 
{
	{"t", 0, 134217867, 0, &TweenSettingsExtensions_SetLoops_m5596_gp_0_0_0_0},
	{"loops", 1, 134217868, 0, &Int32_t127_0_0_0},
	{"loopType", 2, 134217869, 0, &LoopType_t995_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetLoops_m5596_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetLoops_m5596_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t934_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetLoops_m5596_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericContainer, TweenSettingsExtensions_SetLoops_m5596_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetLoops_m5596_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetLoops_m5596_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetLoops_m5596_MethodInfo, 1, 1, TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetLoops_m5596_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetLoops_m5596_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetLoops(T,System.Int32,DG.Tweening.LoopType)
extern const MethodInfo TweenSettingsExtensions_SetLoops_m5596_MethodInfo = 
{
	"SetLoops"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetLoops_m5596_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_SetLoops_m5596_ParameterInfos/* parameters */
	, 29/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, TweenSettingsExtensions_SetLoops_m5596_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetLoops_m5596_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenSettingsExtensions_OnComplete_m5597_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_OnComplete_m5597_gp_0_0_0_0;
extern const Il2CppType TweenCallback_t101_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_OnComplete_m5597_ParameterInfos[] = 
{
	{"t", 0, 134217870, 0, &TweenSettingsExtensions_OnComplete_m5597_gp_0_0_0_0},
	{"action", 1, 134217871, 0, &TweenCallback_t101_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_OnComplete_m5597_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_OnComplete_m5597_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t934_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_OnComplete_m5597_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericContainer, TweenSettingsExtensions_OnComplete_m5597_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_OnComplete_m5597_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_OnComplete_m5597_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_OnComplete_m5597_MethodInfo, 1, 1, TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_OnComplete_m5597_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_OnComplete_m5597_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::OnComplete(T,DG.Tweening.TweenCallback)
extern const MethodInfo TweenSettingsExtensions_OnComplete_m5597_MethodInfo = 
{
	"OnComplete"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_OnComplete_m5597_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_OnComplete_m5597_ParameterInfos/* parameters */
	, 30/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, TweenSettingsExtensions_OnComplete_m5597_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_OnComplete_m5597_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_Append_m367_ParameterInfos[] = 
{
	{"s", 0, 134217872, 0, &Sequence_t122_0_0_0},
	{"t", 1, 134217873, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Append_m367_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Append_m367/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_Append_m367_ParameterInfos/* parameters */
	, 31/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_Join_m368_ParameterInfos[] = 
{
	{"s", 0, 134217874, 0, &Sequence_t122_0_0_0},
	{"t", 1, 134217875, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Join_m368_MethodInfo = 
{
	"Join"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Join_m368/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_Join_m368_ParameterInfos/* parameters */
	, 32/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t122_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_Insert_m372_ParameterInfos[] = 
{
	{"s", 0, 134217876, 0, &Sequence_t122_0_0_0},
	{"atPosition", 1, 134217877, 0, &Single_t151_0_0_0},
	{"t", 2, 134217878, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Insert_m372_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Insert_m372/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151_Object_t/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_Insert_m372_ParameterInfos/* parameters */
	, 33/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenSettingsExtensions_SetRelative_m5598_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetRelative_m5598_gp_0_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_SetRelative_m5598_ParameterInfos[] = 
{
	{"t", 0, 134217879, 0, &TweenSettingsExtensions_SetRelative_m5598_gp_0_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetRelative_m5598_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetRelative_m5598_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t934_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetRelative_m5598_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericContainer, TweenSettingsExtensions_SetRelative_m5598_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetRelative_m5598_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetRelative_m5598_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetRelative_m5598_MethodInfo, 1, 1, TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetRelative_m5598_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetRelative_m5598_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetRelative(T)
extern const MethodInfo TweenSettingsExtensions_SetRelative_m5598_MethodInfo = 
{
	"SetRelative"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetRelative_m5598_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_SetRelative_m5598_ParameterInfos/* parameters */
	, 34/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, TweenSettingsExtensions_SetRelative_m5598_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetRelative_m5598_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t116_0_0_0;
extern const Il2CppType TweenerCore_3_t116_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_SetOptions_m5357_ParameterInfos[] = 
{
	{"t", 0, 134217880, 0, &TweenerCore_3_t116_0_0_0},
	{"snapping", 1, 134217881, 0, &Boolean_t169_0_0_0},
};
extern const Il2CppType Tweener_t99_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern const MethodInfo TweenSettingsExtensions_SetOptions_m5357_MethodInfo = 
{
	"SetOptions"/* name */
	, (methodPointerType)&TweenSettingsExtensions_SetOptions_m5357/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_SetOptions_m5357_ParameterInfos/* parameters */
	, 35/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t116_0_0_0;
extern const Il2CppType AxisConstraint_t1001_0_0_0;
extern const Il2CppType AxisConstraint_t1001_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo TweenSettingsExtensions_t100_TweenSettingsExtensions_SetOptions_m5358_ParameterInfos[] = 
{
	{"t", 0, 134217882, 0, &TweenerCore_3_t116_0_0_0},
	{"axisConstraint", 1, 134217883, 0, &AxisConstraint_t1001_0_0_0},
	{"snapping", 2, 134217884, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern const MethodInfo TweenSettingsExtensions_SetOptions_m5358_MethodInfo = 
{
	"SetOptions"/* name */
	, (methodPointerType)&TweenSettingsExtensions_SetOptions_m5358/* method */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_SByte_t170/* invoker_method */
	, TweenSettingsExtensions_t100_TweenSettingsExtensions_SetOptions_m5358_ParameterInfos/* parameters */
	, 36/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenSettingsExtensions_t100_MethodInfos[] =
{
	&TweenSettingsExtensions_SetTarget_m5595_MethodInfo,
	&TweenSettingsExtensions_SetLoops_m5596_MethodInfo,
	&TweenSettingsExtensions_OnComplete_m5597_MethodInfo,
	&TweenSettingsExtensions_Append_m367_MethodInfo,
	&TweenSettingsExtensions_Join_m368_MethodInfo,
	&TweenSettingsExtensions_Insert_m372_MethodInfo,
	&TweenSettingsExtensions_SetRelative_m5598_MethodInfo,
	&TweenSettingsExtensions_SetOptions_m5357_MethodInfo,
	&TweenSettingsExtensions_SetOptions_m5358_MethodInfo,
	NULL
};
static const Il2CppMethodReference TweenSettingsExtensions_t100_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TweenSettingsExtensions_t100_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenSettingsExtensions_t100_0_0_0;
extern const Il2CppType TweenSettingsExtensions_t100_1_0_0;
struct TweenSettingsExtensions_t100;
const Il2CppTypeDefinitionMetadata TweenSettingsExtensions_t100_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenSettingsExtensions_t100_VTable/* vtableMethods */
	, TweenSettingsExtensions_t100_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenSettingsExtensions_t100_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenSettingsExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenSettingsExtensions_t100_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenSettingsExtensions_t100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 27/* custom_attributes_cache */
	, &TweenSettingsExtensions_t100_0_0_0/* byval_arg */
	, &TweenSettingsExtensions_t100_1_0_0/* this_arg */
	, &TweenSettingsExtensions_t100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenSettingsExtensions_t100)/* instance_size */
	, sizeof (TweenSettingsExtensions_t100)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensions.h"
// Metadata Definition DG.Tweening.TweenExtensions
extern TypeInfo TweenExtensions_t957_il2cpp_TypeInfo;
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensionsMethodDeclarations.h"
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo TweenExtensions_t957_TweenExtensions_Duration_m370_ParameterInfos[] = 
{
	{"t", 0, 134217885, 0, &Tween_t934_0_0_0},
	{"includeLoops", 1, 134217886, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Single_t151_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.TweenExtensions::Duration(DG.Tweening.Tween,System.Boolean)
extern const MethodInfo TweenExtensions_Duration_m370_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TweenExtensions_Duration_m370/* method */
	, &TweenExtensions_t957_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_SByte_t170/* invoker_method */
	, TweenExtensions_t957_TweenExtensions_Duration_m370_ParameterInfos/* parameters */
	, 38/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenExtensions_t957_MethodInfos[] =
{
	&TweenExtensions_Duration_m370_MethodInfo,
	NULL
};
static const Il2CppMethodReference TweenExtensions_t957_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TweenExtensions_t957_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenExtensions_t957_0_0_0;
extern const Il2CppType TweenExtensions_t957_1_0_0;
struct TweenExtensions_t957;
const Il2CppTypeDefinitionMetadata TweenExtensions_t957_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenExtensions_t957_VTable/* vtableMethods */
	, TweenExtensions_t957_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenExtensions_t957_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenExtensions_t957_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenExtensions_t957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 37/* custom_attributes_cache */
	, &TweenExtensions_t957_0_0_0/* byval_arg */
	, &TweenExtensions_t957_1_0_0/* this_arg */
	, &TweenExtensions_t957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenExtensions_t957)/* instance_size */
	, sizeof (TweenExtensions_t957)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallback.h"
// Metadata Definition DG.Tweening.TweenCallback
extern TypeInfo TweenCallback_t101_il2cpp_TypeInfo;
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallbackMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TweenCallback_t101_TweenCallback__ctor_m251_ParameterInfos[] = 
{
	{"object", 0, 134217887, 0, &Object_t_0_0_0},
	{"method", 1, 134217888, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TweenCallback__ctor_m251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TweenCallback__ctor_m251/* method */
	, &TweenCallback_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, TweenCallback_t101_TweenCallback__ctor_m251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::Invoke()
extern const MethodInfo TweenCallback_Invoke_m5359_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TweenCallback_Invoke_m5359/* method */
	, &TweenCallback_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenCallback_t101_TweenCallback_BeginInvoke_m5360_ParameterInfos[] = 
{
	{"callback", 0, 134217889, 0, &AsyncCallback_t305_0_0_0},
	{"object", 1, 134217890, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult DG.Tweening.TweenCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo TweenCallback_BeginInvoke_m5360_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TweenCallback_BeginInvoke_m5360/* method */
	, &TweenCallback_t101_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenCallback_t101_TweenCallback_BeginInvoke_m5360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo TweenCallback_t101_TweenCallback_EndInvoke_m5361_ParameterInfos[] = 
{
	{"result", 0, 134217891, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo TweenCallback_EndInvoke_m5361_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TweenCallback_EndInvoke_m5361/* method */
	, &TweenCallback_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TweenCallback_t101_TweenCallback_EndInvoke_m5361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenCallback_t101_MethodInfos[] =
{
	&TweenCallback__ctor_m251_MethodInfo,
	&TweenCallback_Invoke_m5359_MethodInfo,
	&TweenCallback_BeginInvoke_m5360_MethodInfo,
	&TweenCallback_EndInvoke_m5361_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo TweenCallback_Invoke_m5359_MethodInfo;
extern const MethodInfo TweenCallback_BeginInvoke_m5360_MethodInfo;
extern const MethodInfo TweenCallback_EndInvoke_m5361_MethodInfo;
static const Il2CppMethodReference TweenCallback_t101_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&TweenCallback_Invoke_m5359_MethodInfo,
	&TweenCallback_BeginInvoke_m5360_MethodInfo,
	&TweenCallback_EndInvoke_m5361_MethodInfo,
};
static bool TweenCallback_t101_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
static Il2CppInterfaceOffsetPair TweenCallback_t101_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenCallback_t101_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct TweenCallback_t101;
const Il2CppTypeDefinitionMetadata TweenCallback_t101_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenCallback_t101_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, TweenCallback_t101_VTable/* vtableMethods */
	, TweenCallback_t101_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenCallback_t101_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenCallback"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenCallback_t101_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenCallback_t101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenCallback_t101_0_0_0/* byval_arg */
	, &TweenCallback_t101_1_0_0/* this_arg */
	, &TweenCallback_t101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TweenCallback_t101/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenCallback_t101)/* instance_size */
	, sizeof (TweenCallback_t101)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.TweenCallback`1
extern TypeInfo TweenCallback_1_t1065_il2cpp_TypeInfo;
extern const Il2CppGenericContainer TweenCallback_1_t1065_Il2CppGenericContainer;
extern TypeInfo TweenCallback_1_t1065_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenCallback_1_t1065_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenCallback_1_t1065_Il2CppGenericContainer, NULL, "T", 0, 2 };
static const Il2CppGenericParameter* TweenCallback_1_t1065_Il2CppGenericParametersArray[1] = 
{
	&TweenCallback_1_t1065_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer TweenCallback_1_t1065_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenCallback_1_t1065_il2cpp_TypeInfo, 1, 0, TweenCallback_1_t1065_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TweenCallback_1_t1065_TweenCallback_1__ctor_m5599_ParameterInfos[] = 
{
	{"object", 0, 134217892, 0, &Object_t_0_0_0},
	{"method", 1, 134217893, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TweenCallback_1__ctor_m5599_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1065_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1065_TweenCallback_1__ctor_m5599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_1_t1065_gp_0_0_0_0;
extern const Il2CppType TweenCallback_1_t1065_gp_0_0_0_0;
static const ParameterInfo TweenCallback_1_t1065_TweenCallback_1_Invoke_m5600_ParameterInfos[] = 
{
	{"value", 0, 134217894, 0, &TweenCallback_1_t1065_gp_0_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::Invoke(T)
extern const MethodInfo TweenCallback_1_Invoke_m5600_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1065_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1065_TweenCallback_1_Invoke_m5600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_1_t1065_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenCallback_1_t1065_TweenCallback_1_BeginInvoke_m5601_ParameterInfos[] = 
{
	{"value", 0, 134217895, 0, &TweenCallback_1_t1065_gp_0_0_0_0},
	{"callback", 1, 134217896, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134217897, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.TweenCallback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo TweenCallback_1_BeginInvoke_m5601_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1065_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1065_TweenCallback_1_BeginInvoke_m5601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo TweenCallback_1_t1065_TweenCallback_1_EndInvoke_m5602_ParameterInfos[] = 
{
	{"result", 0, 134217898, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo TweenCallback_1_EndInvoke_m5602_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1065_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1065_TweenCallback_1_EndInvoke_m5602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenCallback_1_t1065_MethodInfos[] =
{
	&TweenCallback_1__ctor_m5599_MethodInfo,
	&TweenCallback_1_Invoke_m5600_MethodInfo,
	&TweenCallback_1_BeginInvoke_m5601_MethodInfo,
	&TweenCallback_1_EndInvoke_m5602_MethodInfo,
	NULL
};
extern const MethodInfo TweenCallback_1_Invoke_m5600_MethodInfo;
extern const MethodInfo TweenCallback_1_BeginInvoke_m5601_MethodInfo;
extern const MethodInfo TweenCallback_1_EndInvoke_m5602_MethodInfo;
static const Il2CppMethodReference TweenCallback_1_t1065_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&TweenCallback_1_Invoke_m5600_MethodInfo,
	&TweenCallback_1_BeginInvoke_m5601_MethodInfo,
	&TweenCallback_1_EndInvoke_m5602_MethodInfo,
};
static bool TweenCallback_1_t1065_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweenCallback_1_t1065_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenCallback_1_t1065_0_0_0;
extern const Il2CppType TweenCallback_1_t1065_1_0_0;
struct TweenCallback_1_t1065;
const Il2CppTypeDefinitionMetadata TweenCallback_1_t1065_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenCallback_1_t1065_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, TweenCallback_1_t1065_VTable/* vtableMethods */
	, TweenCallback_1_t1065_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenCallback_1_t1065_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenCallback`1"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenCallback_1_t1065_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenCallback_1_t1065_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenCallback_1_t1065_0_0_0/* byval_arg */
	, &TweenCallback_1_t1065_1_0_0/* this_arg */
	, &TweenCallback_1_t1065_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &TweenCallback_1_t1065_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunction.h"
// Metadata Definition DG.Tweening.EaseFunction
extern TypeInfo EaseFunction_t945_il2cpp_TypeInfo;
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunctionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EaseFunction_t945_EaseFunction__ctor_m5362_ParameterInfos[] = 
{
	{"object", 0, 134217899, 0, &Object_t_0_0_0},
	{"method", 1, 134217900, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EaseFunction__ctor_m5362_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EaseFunction__ctor_m5362/* method */
	, &EaseFunction_t945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, EaseFunction_t945_EaseFunction__ctor_m5362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo EaseFunction_t945_EaseFunction_Invoke_m5363_ParameterInfos[] = 
{
	{"time", 0, 134217901, 0, &Single_t151_0_0_0},
	{"duration", 1, 134217902, 0, &Single_t151_0_0_0},
	{"overshootOrAmplitude", 2, 134217903, 0, &Single_t151_0_0_0},
	{"period", 3, 134217904, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo EaseFunction_Invoke_m5363_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EaseFunction_Invoke_m5363/* method */
	, &EaseFunction_t945_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, EaseFunction_t945_EaseFunction_Invoke_m5363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EaseFunction_t945_EaseFunction_BeginInvoke_m5364_ParameterInfos[] = 
{
	{"time", 0, 134217905, 0, &Single_t151_0_0_0},
	{"duration", 1, 134217906, 0, &Single_t151_0_0_0},
	{"overshootOrAmplitude", 2, 134217907, 0, &Single_t151_0_0_0},
	{"period", 3, 134217908, 0, &Single_t151_0_0_0},
	{"callback", 4, 134217909, 0, &AsyncCallback_t305_0_0_0},
	{"object", 5, 134217910, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t151_Single_t151_Single_t151_Single_t151_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern const MethodInfo EaseFunction_BeginInvoke_m5364_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EaseFunction_BeginInvoke_m5364/* method */
	, &EaseFunction_t945_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t151_Single_t151_Single_t151_Single_t151_Object_t_Object_t/* invoker_method */
	, EaseFunction_t945_EaseFunction_BeginInvoke_m5364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo EaseFunction_t945_EaseFunction_EndInvoke_m5365_ParameterInfos[] = 
{
	{"result", 0, 134217911, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern const MethodInfo EaseFunction_EndInvoke_m5365_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EaseFunction_EndInvoke_m5365/* method */
	, &EaseFunction_t945_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, EaseFunction_t945_EaseFunction_EndInvoke_m5365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EaseFunction_t945_MethodInfos[] =
{
	&EaseFunction__ctor_m5362_MethodInfo,
	&EaseFunction_Invoke_m5363_MethodInfo,
	&EaseFunction_BeginInvoke_m5364_MethodInfo,
	&EaseFunction_EndInvoke_m5365_MethodInfo,
	NULL
};
extern const MethodInfo EaseFunction_Invoke_m5363_MethodInfo;
extern const MethodInfo EaseFunction_BeginInvoke_m5364_MethodInfo;
extern const MethodInfo EaseFunction_EndInvoke_m5365_MethodInfo;
static const Il2CppMethodReference EaseFunction_t945_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&EaseFunction_Invoke_m5363_MethodInfo,
	&EaseFunction_BeginInvoke_m5364_MethodInfo,
	&EaseFunction_EndInvoke_m5365_MethodInfo,
};
static bool EaseFunction_t945_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EaseFunction_t945_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType EaseFunction_t945_0_0_0;
extern const Il2CppType EaseFunction_t945_1_0_0;
struct EaseFunction_t945;
const Il2CppTypeDefinitionMetadata EaseFunction_t945_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EaseFunction_t945_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, EaseFunction_t945_VTable/* vtableMethods */
	, EaseFunction_t945_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EaseFunction_t945_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "EaseFunction"/* name */
	, "DG.Tweening"/* namespaze */
	, EaseFunction_t945_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EaseFunction_t945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EaseFunction_t945_0_0_0/* byval_arg */
	, &EaseFunction_t945_1_0_0/* this_arg */
	, &EaseFunction_t945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EaseFunction_t945/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EaseFunction_t945)/* instance_size */
	, sizeof (EaseFunction_t945)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.DOGetter`1
extern TypeInfo DOGetter_1_t1066_il2cpp_TypeInfo;
extern const Il2CppGenericContainer DOGetter_1_t1066_Il2CppGenericContainer;
extern TypeInfo DOGetter_1_t1066_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOGetter_1_t1066_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &DOGetter_1_t1066_Il2CppGenericContainer, NULL, "T", 0, 1 };
static const Il2CppGenericParameter* DOGetter_1_t1066_Il2CppGenericParametersArray[1] = 
{
	&DOGetter_1_t1066_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer DOGetter_1_t1066_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOGetter_1_t1066_il2cpp_TypeInfo, 1, 0, DOGetter_1_t1066_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo DOGetter_1_t1066_DOGetter_1__ctor_m5603_ParameterInfos[] = 
{
	{"object", 0, 134217912, 0, &Object_t_0_0_0},
	{"method", 1, 134217913, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.Core.DOGetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo DOGetter_1__ctor_m5603_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &DOGetter_1_t1066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1066_DOGetter_1__ctor_m5603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1066_gp_0_0_0_0;
// T DG.Tweening.Core.DOGetter`1::Invoke()
extern const MethodInfo DOGetter_1_Invoke_m5604_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1066_il2cpp_TypeInfo/* declaring_type */
	, &DOGetter_1_t1066_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DOGetter_1_t1066_DOGetter_1_BeginInvoke_m5605_ParameterInfos[] = 
{
	{"callback", 0, 134217914, 0, &AsyncCallback_t305_0_0_0},
	{"object", 1, 134217915, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.Core.DOGetter`1::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo DOGetter_1_BeginInvoke_m5605_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1066_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1066_DOGetter_1_BeginInvoke_m5605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo DOGetter_1_t1066_DOGetter_1_EndInvoke_m5606_ParameterInfos[] = 
{
	{"result", 0, 134217916, 0, &IAsyncResult_t304_0_0_0},
};
// T DG.Tweening.Core.DOGetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo DOGetter_1_EndInvoke_m5606_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1066_il2cpp_TypeInfo/* declaring_type */
	, &DOGetter_1_t1066_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1066_DOGetter_1_EndInvoke_m5606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOGetter_1_t1066_MethodInfos[] =
{
	&DOGetter_1__ctor_m5603_MethodInfo,
	&DOGetter_1_Invoke_m5604_MethodInfo,
	&DOGetter_1_BeginInvoke_m5605_MethodInfo,
	&DOGetter_1_EndInvoke_m5606_MethodInfo,
	NULL
};
extern const MethodInfo DOGetter_1_Invoke_m5604_MethodInfo;
extern const MethodInfo DOGetter_1_BeginInvoke_m5605_MethodInfo;
extern const MethodInfo DOGetter_1_EndInvoke_m5606_MethodInfo;
static const Il2CppMethodReference DOGetter_1_t1066_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&DOGetter_1_Invoke_m5604_MethodInfo,
	&DOGetter_1_BeginInvoke_m5605_MethodInfo,
	&DOGetter_1_EndInvoke_m5606_MethodInfo,
};
static bool DOGetter_1_t1066_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DOGetter_1_t1066_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOGetter_1_t1066_0_0_0;
extern const Il2CppType DOGetter_1_t1066_1_0_0;
struct DOGetter_1_t1066;
const Il2CppTypeDefinitionMetadata DOGetter_1_t1066_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DOGetter_1_t1066_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, DOGetter_1_t1066_VTable/* vtableMethods */
	, DOGetter_1_t1066_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DOGetter_1_t1066_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOGetter`1"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOGetter_1_t1066_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOGetter_1_t1066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOGetter_1_t1066_0_0_0/* byval_arg */
	, &DOGetter_1_t1066_1_0_0/* this_arg */
	, &DOGetter_1_t1066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &DOGetter_1_t1066_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.DOSetter`1
extern TypeInfo DOSetter_1_t1067_il2cpp_TypeInfo;
extern const Il2CppGenericContainer DOSetter_1_t1067_Il2CppGenericContainer;
extern TypeInfo DOSetter_1_t1067_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOSetter_1_t1067_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &DOSetter_1_t1067_Il2CppGenericContainer, NULL, "T", 0, 2 };
static const Il2CppGenericParameter* DOSetter_1_t1067_Il2CppGenericParametersArray[1] = 
{
	&DOSetter_1_t1067_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer DOSetter_1_t1067_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOSetter_1_t1067_il2cpp_TypeInfo, 1, 0, DOSetter_1_t1067_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo DOSetter_1_t1067_DOSetter_1__ctor_m5607_ParameterInfos[] = 
{
	{"object", 0, 134217917, 0, &Object_t_0_0_0},
	{"method", 1, 134217918, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo DOSetter_1__ctor_m5607_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &DOSetter_1_t1067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1067_DOSetter_1__ctor_m5607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOSetter_1_t1067_gp_0_0_0_0;
extern const Il2CppType DOSetter_1_t1067_gp_0_0_0_0;
static const ParameterInfo DOSetter_1_t1067_DOSetter_1_Invoke_m5608_ParameterInfos[] = 
{
	{"pNewValue", 0, 134217919, 0, &DOSetter_1_t1067_gp_0_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::Invoke(T)
extern const MethodInfo DOSetter_1_Invoke_m5608_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1067_DOSetter_1_Invoke_m5608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOSetter_1_t1067_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DOSetter_1_t1067_DOSetter_1_BeginInvoke_m5609_ParameterInfos[] = 
{
	{"pNewValue", 0, 134217920, 0, &DOSetter_1_t1067_gp_0_0_0_0},
	{"callback", 1, 134217921, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134217922, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.Core.DOSetter`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo DOSetter_1_BeginInvoke_m5609_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1067_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1067_DOSetter_1_BeginInvoke_m5609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo DOSetter_1_t1067_DOSetter_1_EndInvoke_m5610_ParameterInfos[] = 
{
	{"result", 0, 134217923, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo DOSetter_1_EndInvoke_m5610_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1067_DOSetter_1_EndInvoke_m5610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOSetter_1_t1067_MethodInfos[] =
{
	&DOSetter_1__ctor_m5607_MethodInfo,
	&DOSetter_1_Invoke_m5608_MethodInfo,
	&DOSetter_1_BeginInvoke_m5609_MethodInfo,
	&DOSetter_1_EndInvoke_m5610_MethodInfo,
	NULL
};
extern const MethodInfo DOSetter_1_Invoke_m5608_MethodInfo;
extern const MethodInfo DOSetter_1_BeginInvoke_m5609_MethodInfo;
extern const MethodInfo DOSetter_1_EndInvoke_m5610_MethodInfo;
static const Il2CppMethodReference DOSetter_1_t1067_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&DOSetter_1_Invoke_m5608_MethodInfo,
	&DOSetter_1_BeginInvoke_m5609_MethodInfo,
	&DOSetter_1_EndInvoke_m5610_MethodInfo,
};
static bool DOSetter_1_t1067_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DOSetter_1_t1067_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOSetter_1_t1067_0_0_0;
extern const Il2CppType DOSetter_1_t1067_1_0_0;
struct DOSetter_1_t1067;
const Il2CppTypeDefinitionMetadata DOSetter_1_t1067_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DOSetter_1_t1067_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, DOSetter_1_t1067_VTable/* vtableMethods */
	, DOSetter_1_t1067_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DOSetter_1_t1067_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOSetter`1"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOSetter_1_t1067_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOSetter_1_t1067_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOSetter_1_t1067_0_0_0/* byval_arg */
	, &DOSetter_1_t1067_1_0_0/* this_arg */
	, &DOSetter_1_t1067_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &DOSetter_1_t1067_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlay.h"
// Metadata Definition DG.Tweening.AutoPlay
extern TypeInfo AutoPlay_t958_il2cpp_TypeInfo;
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlayMethodDeclarations.h"
static const MethodInfo* AutoPlay_t958_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AutoPlay_t958_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool AutoPlay_t958_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AutoPlay_t958_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType AutoPlay_t958_0_0_0;
extern const Il2CppType AutoPlay_t958_1_0_0;
const Il2CppTypeDefinitionMetadata AutoPlay_t958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AutoPlay_t958_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, AutoPlay_t958_VTable/* vtableMethods */
	, AutoPlay_t958_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 163/* fieldStart */

};
TypeInfo AutoPlay_t958_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "AutoPlay"/* name */
	, "DG.Tweening"/* namespaze */
	, AutoPlay_t958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AutoPlay_t958_0_0_0/* byval_arg */
	, &AutoPlay_t958_1_0_0/* this_arg */
	, &AutoPlay_t958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AutoPlay_t958)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AutoPlay_t958)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
extern TypeInfo U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClassMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass92__ctor_m5366_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92__ctor_m5366/* method */
	, &U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__90()
extern const MethodInfo U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5367_MethodInfo = 
{
	"<DOMove>b__90"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5367/* method */
	, &U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass92_t959_U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368_ParameterInfos[] = 
{
	{"x", 0, 134217943, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__91(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368_MethodInfo = 
{
	"<DOMove>b__91"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368/* method */
	, &U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15/* invoker_method */
	, U3CU3Ec__DisplayClass92_t959_U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass92_t959_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass92__ctor_m5366_MethodInfo,
	&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5367_MethodInfo,
	&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass92_t959_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CU3Ec__DisplayClass92_t959_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass92_t959_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass92_t959_1_0_0;
extern TypeInfo ShortcutExtensions_t964_il2cpp_TypeInfo;
extern const Il2CppType ShortcutExtensions_t964_0_0_0;
struct U3CU3Ec__DisplayClass92_t959;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass92_t959_DefinitionMetadata = 
{
	&ShortcutExtensions_t964_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass92_t959_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass92_t959_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 168/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass92"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass92_t959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass92_t959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 45/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass92_t959_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass92_t959_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass92_t959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass92_t959)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass92_t959)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
extern TypeInfo U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass96__ctor_m5369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96__ctor_m5369/* method */
	, &U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__94()
extern const MethodInfo U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5370_MethodInfo = 
{
	"<DOMoveX>b__94"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5370/* method */
	, &U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass96_t960_U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371_ParameterInfos[] = 
{
	{"x", 0, 134217944, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__95(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371_MethodInfo = 
{
	"<DOMoveX>b__95"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371/* method */
	, &U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15/* invoker_method */
	, U3CU3Ec__DisplayClass96_t960_U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass96_t960_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass96__ctor_m5369_MethodInfo,
	&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5370_MethodInfo,
	&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5371_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass96_t960_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CU3Ec__DisplayClass96_t960_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass96_t960_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass96_t960_1_0_0;
struct U3CU3Ec__DisplayClass96_t960;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass96_t960_DefinitionMetadata = 
{
	&ShortcutExtensions_t964_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass96_t960_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass96_t960_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 169/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass96"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass96_t960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass96_t960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 46/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass96_t960_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass96_t960_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass96_t960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass96_t960)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass96_t960)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
extern TypeInfo U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass9a__ctor_m5372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a__ctor_m5372/* method */
	, &U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__98()
extern const MethodInfo U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5373_MethodInfo = 
{
	"<DOMoveY>b__98"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5373/* method */
	, &U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass9a_t961_U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374_ParameterInfos[] = 
{
	{"x", 0, 134217945, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__99(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374_MethodInfo = 
{
	"<DOMoveY>b__99"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374/* method */
	, &U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15/* invoker_method */
	, U3CU3Ec__DisplayClass9a_t961_U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass9a_t961_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass9a__ctor_m5372_MethodInfo,
	&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5373_MethodInfo,
	&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass9a_t961_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CU3Ec__DisplayClass9a_t961_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass9a_t961_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass9a_t961_1_0_0;
struct U3CU3Ec__DisplayClass9a_t961;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass9a_t961_DefinitionMetadata = 
{
	&ShortcutExtensions_t964_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass9a_t961_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass9a_t961_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 170/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass9a"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass9a_t961_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass9a_t961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 47/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass9a_t961_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass9a_t961_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass9a_t961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass9a_t961)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass9a_t961)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
extern TypeInfo U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClassb2__ctor_m5375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2__ctor_m5375/* method */
	, &U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t13_0_0_0;
extern void* RuntimeInvoker_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b0()
extern const MethodInfo U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5376_MethodInfo = 
{
	"<DORotate>b__b0"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5376/* method */
	, &U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t13_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t13/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t13_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClassb2_t962_U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377_ParameterInfos[] = 
{
	{"x", 0, 134217946, 0, &Quaternion_t13_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b1(UnityEngine.Quaternion)
extern const MethodInfo U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377_MethodInfo = 
{
	"<DORotate>b__b1"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377/* method */
	, &U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Quaternion_t13/* invoker_method */
	, U3CU3Ec__DisplayClassb2_t962_U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClassb2_t962_MethodInfos[] =
{
	&U3CU3Ec__DisplayClassb2__ctor_m5375_MethodInfo,
	&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5376_MethodInfo,
	&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5377_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClassb2_t962_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CU3Ec__DisplayClassb2_t962_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClassb2_t962_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClassb2_t962_1_0_0;
struct U3CU3Ec__DisplayClassb2_t962;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClassb2_t962_DefinitionMetadata = 
{
	&ShortcutExtensions_t964_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClassb2_t962_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClassb2_t962_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 171/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClassb2"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClassb2_t962_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClassb2_t962_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 48/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClassb2_t962_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClassb2_t962_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClassb2_t962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClassb2_t962)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClassb2_t962)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
extern TypeInfo U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClassc6__ctor_m5378_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6__ctor_m5378/* method */
	, &U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c4()
extern const MethodInfo U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5379_MethodInfo = 
{
	"<DOScaleY>b__c4"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5379/* method */
	, &U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClassc6_t963_U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5380_ParameterInfos[] = 
{
	{"x", 0, 134217947, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c5(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5380_MethodInfo = 
{
	"<DOScaleY>b__c5"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5380/* method */
	, &U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector3_t15/* invoker_method */
	, U3CU3Ec__DisplayClassc6_t963_U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClassc6_t963_MethodInfos[] =
{
	&U3CU3Ec__DisplayClassc6__ctor_m5378_MethodInfo,
	&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5379_MethodInfo,
	&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5380_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClassc6_t963_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CU3Ec__DisplayClassc6_t963_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClassc6_t963_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClassc6_t963_1_0_0;
struct U3CU3Ec__DisplayClassc6_t963;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClassc6_t963_DefinitionMetadata = 
{
	&ShortcutExtensions_t964_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClassc6_t963_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClassc6_t963_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 172/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClassc6"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClassc6_t963_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClassc6_t963_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 49/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClassc6_t963_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClassc6_t963_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClassc6_t963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClassc6_t963)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClassc6_t963)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensions.h"
// Metadata Definition DG.Tweening.ShortcutExtensions
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensionsMethodDeclarations.h"
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo ShortcutExtensions_t964_ShortcutExtensions_DOMove_m360_ParameterInfos[] = 
{
	{"target", 0, 134217924, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217925, 0, &Vector3_t15_0_0_0},
	{"duration", 2, 134217926, 0, &Single_t151_0_0_0},
	{"snapping", 3, 134217927, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t15_Single_t151_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMove_m360_MethodInfo = 
{
	"DOMove"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMove_m360/* method */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t15_Single_t151_SByte_t170/* invoker_method */
	, ShortcutExtensions_t964_ShortcutExtensions_DOMove_m360_ParameterInfos/* parameters */
	, 40/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo ShortcutExtensions_t964_ShortcutExtensions_DOMoveX_m371_ParameterInfos[] = 
{
	{"target", 0, 134217928, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217929, 0, &Single_t151_0_0_0},
	{"duration", 2, 134217930, 0, &Single_t151_0_0_0},
	{"snapping", 3, 134217931, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMoveX_m371_MethodInfo = 
{
	"DOMoveX"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMoveX_m371/* method */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151_SByte_t170/* invoker_method */
	, ShortcutExtensions_t964_ShortcutExtensions_DOMoveX_m371_ParameterInfos/* parameters */
	, 41/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo ShortcutExtensions_t964_ShortcutExtensions_DOMoveY_m366_ParameterInfos[] = 
{
	{"target", 0, 134217932, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217933, 0, &Single_t151_0_0_0},
	{"duration", 2, 134217934, 0, &Single_t151_0_0_0},
	{"snapping", 3, 134217935, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMoveY_m366_MethodInfo = 
{
	"DOMoveY"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMoveY_m366/* method */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151_SByte_t170/* invoker_method */
	, ShortcutExtensions_t964_ShortcutExtensions_DOMoveY_m366_ParameterInfos/* parameters */
	, 42/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType RotateMode_t948_0_0_4112;
static const ParameterInfo ShortcutExtensions_t964_ShortcutExtensions_DORotate_m250_ParameterInfos[] = 
{
	{"target", 0, 134217936, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217937, 0, &Vector3_t15_0_0_0},
	{"duration", 2, 134217938, 0, &Single_t151_0_0_0},
	{"mode", 3, 134217939, 0, &RotateMode_t948_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t15_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern const MethodInfo ShortcutExtensions_DORotate_m250_MethodInfo = 
{
	"DORotate"/* name */
	, (methodPointerType)&ShortcutExtensions_DORotate_m250/* method */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t15_Single_t151_Int32_t127/* invoker_method */
	, ShortcutExtensions_t964_ShortcutExtensions_DORotate_m250_ParameterInfos/* parameters */
	, 43/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ShortcutExtensions_t964_ShortcutExtensions_DOScaleY_m369_ParameterInfos[] = 
{
	{"target", 0, 134217940, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217941, 0, &Single_t151_0_0_0},
	{"duration", 2, 134217942, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern const MethodInfo ShortcutExtensions_DOScaleY_m369_MethodInfo = 
{
	"DOScaleY"/* name */
	, (methodPointerType)&ShortcutExtensions_DOScaleY_m369/* method */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t99_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t151_Single_t151/* invoker_method */
	, ShortcutExtensions_t964_ShortcutExtensions_DOScaleY_m369_ParameterInfos/* parameters */
	, 44/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ShortcutExtensions_t964_MethodInfos[] =
{
	&ShortcutExtensions_DOMove_m360_MethodInfo,
	&ShortcutExtensions_DOMoveX_m371_MethodInfo,
	&ShortcutExtensions_DOMoveY_m366_MethodInfo,
	&ShortcutExtensions_DORotate_m250_MethodInfo,
	&ShortcutExtensions_DOScaleY_m369_MethodInfo,
	NULL
};
static const Il2CppType* ShortcutExtensions_t964_il2cpp_TypeInfo__nestedTypes[5] =
{
	&U3CU3Ec__DisplayClass92_t959_0_0_0,
	&U3CU3Ec__DisplayClass96_t960_0_0_0,
	&U3CU3Ec__DisplayClass9a_t961_0_0_0,
	&U3CU3Ec__DisplayClassb2_t962_0_0_0,
	&U3CU3Ec__DisplayClassc6_t963_0_0_0,
};
static const Il2CppMethodReference ShortcutExtensions_t964_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ShortcutExtensions_t964_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ShortcutExtensions_t964_1_0_0;
struct ShortcutExtensions_t964;
const Il2CppTypeDefinitionMetadata ShortcutExtensions_t964_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ShortcutExtensions_t964_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShortcutExtensions_t964_VTable/* vtableMethods */
	, ShortcutExtensions_t964_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ShortcutExtensions_t964_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShortcutExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, ShortcutExtensions_t964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ShortcutExtensions_t964_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 39/* custom_attributes_cache */
	, &ShortcutExtensions_t964_0_0_0/* byval_arg */
	, &ShortcutExtensions_t964_1_0_0/* this_arg */
	, &ShortcutExtensions_t964_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShortcutExtensions_t964)/* instance_size */
	, sizeof (ShortcutExtensions_t964)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin.h"
// Metadata Definition DG.Tweening.Plugins.RectOffsetPlugin
extern TypeInfo RectOffsetPlugin_t965_il2cpp_TypeInfo;
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1025_0_0_0;
extern const Il2CppType TweenerCore_3_t1025_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_Reset_m5381_ParameterInfos[] = 
{
	{"t", 0, 134217948, 0, &TweenerCore_3_t1025_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_Reset_m5381_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&RectOffsetPlugin_Reset_m5381/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_Reset_m5381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1025_0_0_0;
extern const Il2CppType RectOffset_t371_0_0_0;
extern const Il2CppType RectOffset_t371_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_ConvertToStartValue_m5382_ParameterInfos[] = 
{
	{"t", 0, 134217949, 0, &TweenerCore_3_t1025_0_0_0},
	{"value", 1, 134217950, 0, &RectOffset_t371_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>,UnityEngine.RectOffset)
extern const MethodInfo RectOffsetPlugin_ConvertToStartValue_m5382_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_ConvertToStartValue_m5382/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t371_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_ConvertToStartValue_m5382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1025_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_SetRelativeEndValue_m5383_ParameterInfos[] = 
{
	{"t", 0, 134217951, 0, &TweenerCore_3_t1025_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_SetRelativeEndValue_m5383_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_SetRelativeEndValue_m5383/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_SetRelativeEndValue_m5383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1025_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_SetChangeValue_m5384_ParameterInfos[] = 
{
	{"t", 0, 134217952, 0, &TweenerCore_3_t1025_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_SetChangeValue_m5384_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_SetChangeValue_m5384/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_SetChangeValue_m5384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType RectOffset_t371_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_GetSpeedBasedDuration_m5385_ParameterInfos[] = 
{
	{"options", 0, 134217953, 0, &NoOptions_t933_0_0_0},
	{"unitsXSecond", 1, 134217954, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217955, 0, &RectOffset_t371_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.RectOffsetPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,UnityEngine.RectOffset)
extern const MethodInfo RectOffsetPlugin_GetSpeedBasedDuration_m5385_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&RectOffsetPlugin_GetSpeedBasedDuration_m5385/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Object_t/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_GetSpeedBasedDuration_m5385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1026_0_0_0;
extern const Il2CppType DOGetter_1_t1026_0_0_0;
extern const Il2CppType DOSetter_1_t1027_0_0_0;
extern const Il2CppType DOSetter_1_t1027_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType RectOffset_t371_0_0_0;
extern const Il2CppType RectOffset_t371_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo RectOffsetPlugin_t965_RectOffsetPlugin_EvaluateAndApply_m5386_ParameterInfos[] = 
{
	{"options", 0, 134217956, 0, &NoOptions_t933_0_0_0},
	{"t", 1, 134217957, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217958, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217959, 0, &DOGetter_1_t1026_0_0_0},
	{"setter", 4, 134217960, 0, &DOSetter_1_t1027_0_0_0},
	{"elapsed", 5, 134217961, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217962, 0, &RectOffset_t371_0_0_0},
	{"changeValue", 7, 134217963, 0, &RectOffset_t371_0_0_0},
	{"duration", 8, 134217964, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217965, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217966, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,System.Single,UnityEngine.RectOffset,UnityEngine.RectOffset,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo RectOffsetPlugin_EvaluateAndApply_m5386_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&RectOffsetPlugin_EvaluateAndApply_m5386/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, RectOffsetPlugin_t965_RectOffsetPlugin_EvaluateAndApply_m5386_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.ctor()
extern const MethodInfo RectOffsetPlugin__ctor_m5387_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RectOffsetPlugin__ctor_m5387/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.cctor()
extern const MethodInfo RectOffsetPlugin__cctor_m5388_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RectOffsetPlugin__cctor_m5388/* method */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RectOffsetPlugin_t965_MethodInfos[] =
{
	&RectOffsetPlugin_Reset_m5381_MethodInfo,
	&RectOffsetPlugin_ConvertToStartValue_m5382_MethodInfo,
	&RectOffsetPlugin_SetRelativeEndValue_m5383_MethodInfo,
	&RectOffsetPlugin_SetChangeValue_m5384_MethodInfo,
	&RectOffsetPlugin_GetSpeedBasedDuration_m5385_MethodInfo,
	&RectOffsetPlugin_EvaluateAndApply_m5386_MethodInfo,
	&RectOffsetPlugin__ctor_m5387_MethodInfo,
	&RectOffsetPlugin__cctor_m5388_MethodInfo,
	NULL
};
extern const MethodInfo RectOffsetPlugin_Reset_m5381_MethodInfo;
extern const MethodInfo RectOffsetPlugin_ConvertToStartValue_m5382_MethodInfo;
extern const MethodInfo RectOffsetPlugin_SetRelativeEndValue_m5383_MethodInfo;
extern const MethodInfo RectOffsetPlugin_SetChangeValue_m5384_MethodInfo;
extern const MethodInfo RectOffsetPlugin_GetSpeedBasedDuration_m5385_MethodInfo;
extern const MethodInfo RectOffsetPlugin_EvaluateAndApply_m5386_MethodInfo;
static const Il2CppMethodReference RectOffsetPlugin_t965_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RectOffsetPlugin_Reset_m5381_MethodInfo,
	&RectOffsetPlugin_ConvertToStartValue_m5382_MethodInfo,
	&RectOffsetPlugin_SetRelativeEndValue_m5383_MethodInfo,
	&RectOffsetPlugin_SetChangeValue_m5384_MethodInfo,
	&RectOffsetPlugin_GetSpeedBasedDuration_m5385_MethodInfo,
	&RectOffsetPlugin_EvaluateAndApply_m5386_MethodInfo,
};
static bool RectOffsetPlugin_t965_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RectOffsetPlugin_t965_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectOffsetPlugin_t965_0_0_0;
extern const Il2CppType RectOffsetPlugin_t965_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t966_0_0_0;
struct RectOffsetPlugin_t965;
const Il2CppTypeDefinitionMetadata RectOffsetPlugin_t965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectOffsetPlugin_t965_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t966_0_0_0/* parent */
	, RectOffsetPlugin_t965_VTable/* vtableMethods */
	, RectOffsetPlugin_t965_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 173/* fieldStart */

};
TypeInfo RectOffsetPlugin_t965_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOffsetPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, RectOffsetPlugin_t965_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectOffsetPlugin_t965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOffsetPlugin_t965_0_0_0/* byval_arg */
	, &RectOffsetPlugin_t965_1_0_0/* this_arg */
	, &RectOffsetPlugin_t965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectOffsetPlugin_t965)/* instance_size */
	, sizeof (RectOffsetPlugin_t965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectOffsetPlugin_t965_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin.h"
// Metadata Definition DG.Tweening.Plugins.QuaternionPlugin
extern TypeInfo QuaternionPlugin_t967_il2cpp_TypeInfo;
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_Reset_m5389_ParameterInfos[] = 
{
	{"t", 0, 134217967, 0, &TweenerCore_3_t1028_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_Reset_m5389_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&QuaternionPlugin_Reset_m5389/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_Reset_m5389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
extern const Il2CppType Quaternion_t13_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_ConvertToStartValue_m5390_ParameterInfos[] = 
{
	{"t", 0, 134217968, 0, &TweenerCore_3_t1028_0_0_0},
	{"value", 1, 134217969, 0, &Quaternion_t13_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Object_t_Quaternion_t13 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.Plugins.QuaternionPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,UnityEngine.Quaternion)
extern const MethodInfo QuaternionPlugin_ConvertToStartValue_m5390_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&QuaternionPlugin_ConvertToStartValue_m5390/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Object_t_Quaternion_t13/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_ConvertToStartValue_m5390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_SetRelativeEndValue_m5391_ParameterInfos[] = 
{
	{"t", 0, 134217970, 0, &TweenerCore_3_t1028_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_SetRelativeEndValue_m5391_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&QuaternionPlugin_SetRelativeEndValue_m5391/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_SetRelativeEndValue_m5391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_SetChangeValue_m5392_ParameterInfos[] = 
{
	{"t", 0, 134217971, 0, &TweenerCore_3_t1028_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_SetChangeValue_m5392_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&QuaternionPlugin_SetChangeValue_m5392/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_SetChangeValue_m5392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType QuaternionOptions_t971_0_0_0;
extern const Il2CppType QuaternionOptions_t971_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_GetSpeedBasedDuration_m5393_ParameterInfos[] = 
{
	{"options", 0, 134217972, 0, &QuaternionOptions_t971_0_0_0},
	{"unitsXSecond", 1, 134217973, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217974, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_QuaternionOptions_t971_Single_t151_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.QuaternionPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.QuaternionOptions,System.Single,UnityEngine.Vector3)
extern const MethodInfo QuaternionPlugin_GetSpeedBasedDuration_m5393_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&QuaternionPlugin_GetSpeedBasedDuration_m5393/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_QuaternionOptions_t971_Single_t151_Vector3_t15/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_GetSpeedBasedDuration_m5393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType QuaternionOptions_t971_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1029_0_0_0;
extern const Il2CppType DOGetter_1_t1029_0_0_0;
extern const Il2CppType DOSetter_1_t1030_0_0_0;
extern const Il2CppType DOSetter_1_t1030_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo QuaternionPlugin_t967_QuaternionPlugin_EvaluateAndApply_m5394_ParameterInfos[] = 
{
	{"options", 0, 134217975, 0, &QuaternionOptions_t971_0_0_0},
	{"t", 1, 134217976, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217977, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217978, 0, &DOGetter_1_t1029_0_0_0},
	{"setter", 4, 134217979, 0, &DOSetter_1_t1030_0_0_0},
	{"elapsed", 5, 134217980, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134217981, 0, &Vector3_t15_0_0_0},
	{"changeValue", 7, 134217982, 0, &Vector3_t15_0_0_0},
	{"duration", 8, 134217983, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134217984, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134217985, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_QuaternionOptions_t971_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector3_t15_Vector3_t15_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.QuaternionOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo QuaternionPlugin_EvaluateAndApply_m5394_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&QuaternionPlugin_EvaluateAndApply_m5394/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_QuaternionOptions_t971_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector3_t15_Vector3_t15_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, QuaternionPlugin_t967_QuaternionPlugin_EvaluateAndApply_m5394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::.ctor()
extern const MethodInfo QuaternionPlugin__ctor_m5395_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuaternionPlugin__ctor_m5395/* method */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QuaternionPlugin_t967_MethodInfos[] =
{
	&QuaternionPlugin_Reset_m5389_MethodInfo,
	&QuaternionPlugin_ConvertToStartValue_m5390_MethodInfo,
	&QuaternionPlugin_SetRelativeEndValue_m5391_MethodInfo,
	&QuaternionPlugin_SetChangeValue_m5392_MethodInfo,
	&QuaternionPlugin_GetSpeedBasedDuration_m5393_MethodInfo,
	&QuaternionPlugin_EvaluateAndApply_m5394_MethodInfo,
	&QuaternionPlugin__ctor_m5395_MethodInfo,
	NULL
};
extern const MethodInfo QuaternionPlugin_Reset_m5389_MethodInfo;
extern const MethodInfo QuaternionPlugin_ConvertToStartValue_m5390_MethodInfo;
extern const MethodInfo QuaternionPlugin_SetRelativeEndValue_m5391_MethodInfo;
extern const MethodInfo QuaternionPlugin_SetChangeValue_m5392_MethodInfo;
extern const MethodInfo QuaternionPlugin_GetSpeedBasedDuration_m5393_MethodInfo;
extern const MethodInfo QuaternionPlugin_EvaluateAndApply_m5394_MethodInfo;
static const Il2CppMethodReference QuaternionPlugin_t967_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&QuaternionPlugin_Reset_m5389_MethodInfo,
	&QuaternionPlugin_ConvertToStartValue_m5390_MethodInfo,
	&QuaternionPlugin_SetRelativeEndValue_m5391_MethodInfo,
	&QuaternionPlugin_SetChangeValue_m5392_MethodInfo,
	&QuaternionPlugin_GetSpeedBasedDuration_m5393_MethodInfo,
	&QuaternionPlugin_EvaluateAndApply_m5394_MethodInfo,
};
static bool QuaternionPlugin_t967_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair QuaternionPlugin_t967_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType QuaternionPlugin_t967_0_0_0;
extern const Il2CppType QuaternionPlugin_t967_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t968_0_0_0;
struct QuaternionPlugin_t967;
const Il2CppTypeDefinitionMetadata QuaternionPlugin_t967_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, QuaternionPlugin_t967_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t968_0_0_0/* parent */
	, QuaternionPlugin_t967_VTable/* vtableMethods */
	, QuaternionPlugin_t967_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo QuaternionPlugin_t967_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuaternionPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, QuaternionPlugin_t967_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QuaternionPlugin_t967_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuaternionPlugin_t967_0_0_0/* byval_arg */
	, &QuaternionPlugin_t967_1_0_0/* this_arg */
	, &QuaternionPlugin_t967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuaternionPlugin_t967)/* instance_size */
	, sizeof (QuaternionPlugin_t967)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager.h"
// Metadata Definition DG.Tweening.Plugins.Core.PluginsManager
extern TypeInfo PluginsManager_t970_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManagerMethodDeclarations.h"
extern const Il2CppType ABSTweenPlugin_3_t1083_0_0_0;
extern const Il2CppGenericContainer PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer;
extern TypeInfo PluginsManager_GetDefaultPlugin_m5611_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5611_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo PluginsManager_GetDefaultPlugin_m5611_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5611_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo PluginsManager_GetDefaultPlugin_m5611_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* PluginsManager_GetDefaultPlugin_m5611_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5611_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer, PluginsManager_GetDefaultPlugin_m5611_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericParametersArray[3] = 
{
	&PluginsManager_GetDefaultPlugin_m5611_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&PluginsManager_GetDefaultPlugin_m5611_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&PluginsManager_GetDefaultPlugin_m5611_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo PluginsManager_GetDefaultPlugin_m5611_MethodInfo;
extern const Il2CppGenericContainer PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&PluginsManager_GetDefaultPlugin_m5611_MethodInfo, 3, 1, PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericParametersArray };
extern const Il2CppType PluginsManager_GetDefaultPlugin_m5611_gp_0_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m5611_gp_1_0_0_0;
static Il2CppRGCTXDefinition PluginsManager_GetDefaultPlugin_m5611_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&PluginsManager_GetDefaultPlugin_m5611_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&PluginsManager_GetDefaultPlugin_m5611_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ABSTweenPlugin_3_t1083_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Plugins.Core.PluginsManager::GetDefaultPlugin()
extern const MethodInfo PluginsManager_GetDefaultPlugin_m5611_MethodInfo = 
{
	"GetDefaultPlugin"/* name */
	, NULL/* method */
	, &PluginsManager_t970_il2cpp_TypeInfo/* declaring_type */
	, &ABSTweenPlugin_3_t1083_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, PluginsManager_GetDefaultPlugin_m5611_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &PluginsManager_GetDefaultPlugin_m5611_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* PluginsManager_t970_MethodInfos[] =
{
	&PluginsManager_GetDefaultPlugin_m5611_MethodInfo,
	NULL
};
static const Il2CppMethodReference PluginsManager_t970_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool PluginsManager_t970_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType PluginsManager_t970_0_0_0;
extern const Il2CppType PluginsManager_t970_1_0_0;
struct PluginsManager_t970;
const Il2CppTypeDefinitionMetadata PluginsManager_t970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PluginsManager_t970_VTable/* vtableMethods */
	, PluginsManager_t970_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 174/* fieldStart */

};
TypeInfo PluginsManager_t970_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "PluginsManager"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, PluginsManager_t970_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PluginsManager_t970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PluginsManager_t970_0_0_0/* byval_arg */
	, &PluginsManager_t970_1_0_0/* this_arg */
	, &PluginsManager_t970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PluginsManager_t970)/* instance_size */
	, sizeof (PluginsManager_t970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PluginsManager_t970_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.QuaternionOptions
extern TypeInfo QuaternionOptions_t971_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptionsMethodDeclarations.h"
static const MethodInfo* QuaternionOptions_t971_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference QuaternionOptions_t971_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool QuaternionOptions_t971_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType QuaternionOptions_t971_1_0_0;
const Il2CppTypeDefinitionMetadata QuaternionOptions_t971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, QuaternionOptions_t971_VTable/* vtableMethods */
	, QuaternionOptions_t971_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 189/* fieldStart */

};
TypeInfo QuaternionOptions_t971_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuaternionOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, QuaternionOptions_t971_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QuaternionOptions_t971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuaternionOptions_t971_0_0_0/* byval_arg */
	, &QuaternionOptions_t971_1_0_0/* this_arg */
	, &QuaternionOptions_t971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)QuaternionOptions_t971_marshal/* marshal_to_native_func */
	, (methodPointerType)QuaternionOptions_t971_marshal_back/* marshal_from_native_func */
	, (methodPointerType)QuaternionOptions_t971_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (QuaternionOptions_t971)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (QuaternionOptions_t971)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(QuaternionOptions_t971_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode.h"
// Metadata Definition DG.Tweening.Core.Enums.SpecialStartupMode
extern TypeInfo SpecialStartupMode_t972_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupModeMethodDeclarations.h"
static const MethodInfo* SpecialStartupMode_t972_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SpecialStartupMode_t972_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool SpecialStartupMode_t972_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpecialStartupMode_t972_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SpecialStartupMode_t972_0_0_0;
extern const Il2CppType SpecialStartupMode_t972_1_0_0;
const Il2CppTypeDefinitionMetadata SpecialStartupMode_t972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpecialStartupMode_t972_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, SpecialStartupMode_t972_VTable/* vtableMethods */
	, SpecialStartupMode_t972_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 192/* fieldStart */

};
TypeInfo SpecialStartupMode_t972_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialStartupMode"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, SpecialStartupMode_t972_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpecialStartupMode_t972_0_0_0/* byval_arg */
	, &SpecialStartupMode_t972_1_0_0/* this_arg */
	, &SpecialStartupMode_t972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialStartupMode_t972)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpecialStartupMode_t972)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector3Plugin
extern TypeInfo Vector3Plugin_t973_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t116_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_Reset_m5396_ParameterInfos[] = 
{
	{"t", 0, 134217986, 0, &TweenerCore_3_t116_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_Reset_m5396_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector3Plugin_Reset_m5396/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_Reset_m5396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t116_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_ConvertToStartValue_m5397_ParameterInfos[] = 
{
	{"t", 0, 134217987, 0, &TweenerCore_3_t116_0_0_0},
	{"value", 1, 134217988, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Object_t_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.Plugins.Vector3Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector3)
extern const MethodInfo Vector3Plugin_ConvertToStartValue_m5397_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector3Plugin_ConvertToStartValue_m5397/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Object_t_Vector3_t15/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_ConvertToStartValue_m5397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t116_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_SetRelativeEndValue_m5398_ParameterInfos[] = 
{
	{"t", 0, 134217989, 0, &TweenerCore_3_t116_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_SetRelativeEndValue_m5398_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector3Plugin_SetRelativeEndValue_m5398/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_SetRelativeEndValue_m5398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t116_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_SetChangeValue_m5399_ParameterInfos[] = 
{
	{"t", 0, 134217990, 0, &TweenerCore_3_t116_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_SetChangeValue_m5399_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector3Plugin_SetChangeValue_m5399/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_SetChangeValue_m5399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_GetSpeedBasedDuration_m5400_ParameterInfos[] = 
{
	{"options", 0, 134217991, 0, &VectorOptions_t1002_0_0_0},
	{"unitsXSecond", 1, 134217992, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134217993, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector3Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector3)
extern const MethodInfo Vector3Plugin_GetSpeedBasedDuration_m5400_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector3Plugin_GetSpeedBasedDuration_m5400/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector3_t15/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_GetSpeedBasedDuration_m5400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t120_0_0_0;
extern const Il2CppType DOSetter_1_t121_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Vector3Plugin_t973_Vector3Plugin_EvaluateAndApply_m5401_ParameterInfos[] = 
{
	{"options", 0, 134217994, 0, &VectorOptions_t1002_0_0_0},
	{"t", 1, 134217995, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134217996, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134217997, 0, &DOGetter_1_t120_0_0_0},
	{"setter", 4, 134217998, 0, &DOSetter_1_t121_0_0_0},
	{"elapsed", 5, 134217999, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218000, 0, &Vector3_t15_0_0_0},
	{"changeValue", 7, 134218001, 0, &Vector3_t15_0_0_0},
	{"duration", 8, 134218002, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218003, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218004, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector3_t15_Vector3_t15_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector3Plugin_EvaluateAndApply_m5401_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector3Plugin_EvaluateAndApply_m5401/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector3_t15_Vector3_t15_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, Vector3Plugin_t973_Vector3Plugin_EvaluateAndApply_m5401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::.ctor()
extern const MethodInfo Vector3Plugin__ctor_m5402_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector3Plugin__ctor_m5402/* method */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector3Plugin_t973_MethodInfos[] =
{
	&Vector3Plugin_Reset_m5396_MethodInfo,
	&Vector3Plugin_ConvertToStartValue_m5397_MethodInfo,
	&Vector3Plugin_SetRelativeEndValue_m5398_MethodInfo,
	&Vector3Plugin_SetChangeValue_m5399_MethodInfo,
	&Vector3Plugin_GetSpeedBasedDuration_m5400_MethodInfo,
	&Vector3Plugin_EvaluateAndApply_m5401_MethodInfo,
	&Vector3Plugin__ctor_m5402_MethodInfo,
	NULL
};
extern const MethodInfo Vector3Plugin_Reset_m5396_MethodInfo;
extern const MethodInfo Vector3Plugin_ConvertToStartValue_m5397_MethodInfo;
extern const MethodInfo Vector3Plugin_SetRelativeEndValue_m5398_MethodInfo;
extern const MethodInfo Vector3Plugin_SetChangeValue_m5399_MethodInfo;
extern const MethodInfo Vector3Plugin_GetSpeedBasedDuration_m5400_MethodInfo;
extern const MethodInfo Vector3Plugin_EvaluateAndApply_m5401_MethodInfo;
static const Il2CppMethodReference Vector3Plugin_t973_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Vector3Plugin_Reset_m5396_MethodInfo,
	&Vector3Plugin_ConvertToStartValue_m5397_MethodInfo,
	&Vector3Plugin_SetRelativeEndValue_m5398_MethodInfo,
	&Vector3Plugin_SetChangeValue_m5399_MethodInfo,
	&Vector3Plugin_GetSpeedBasedDuration_m5400_MethodInfo,
	&Vector3Plugin_EvaluateAndApply_m5401_MethodInfo,
};
static bool Vector3Plugin_t973_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector3Plugin_t973_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3Plugin_t973_0_0_0;
extern const Il2CppType Vector3Plugin_t973_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t974_0_0_0;
struct Vector3Plugin_t973;
const Il2CppTypeDefinitionMetadata Vector3Plugin_t973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector3Plugin_t973_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t974_0_0_0/* parent */
	, Vector3Plugin_t973_VTable/* vtableMethods */
	, Vector3Plugin_t973_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector3Plugin_t973_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector3Plugin_t973_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3Plugin_t973_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3Plugin_t973_0_0_0/* byval_arg */
	, &Vector3Plugin_t973_1_0_0/* this_arg */
	, &Vector3Plugin_t973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3Plugin_t973)/* instance_size */
	, sizeof (Vector3Plugin_t973)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Color2Plugin
extern TypeInfo Color2Plugin_t975_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_Reset_m5403_ParameterInfos[] = 
{
	{"t", 0, 134218005, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_Reset_m5403_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Color2Plugin_Reset_m5403/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_Reset_m5403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_ConvertToStartValue_m5404_ParameterInfos[] = 
{
	{"t", 0, 134218006, 0, &TweenerCore_3_t1031_0_0_0},
	{"value", 1, 134218007, 0, &Color2_t1000_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1000_Object_t_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Plugins.Color2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>,DG.Tweening.Color2)
extern const MethodInfo Color2Plugin_ConvertToStartValue_m5404_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Color2Plugin_ConvertToStartValue_m5404/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1000_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1000_Object_t_Color2_t1000/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_ConvertToStartValue_m5404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_SetRelativeEndValue_m5405_ParameterInfos[] = 
{
	{"t", 0, 134218008, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_SetRelativeEndValue_m5405_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Color2Plugin_SetRelativeEndValue_m5405/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_SetRelativeEndValue_m5405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_SetChangeValue_m5406_ParameterInfos[] = 
{
	{"t", 0, 134218009, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_SetChangeValue_m5406_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Color2Plugin_SetChangeValue_m5406/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_SetChangeValue_m5406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1011_0_0_0;
extern const Il2CppType ColorOptions_t1011_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_GetSpeedBasedDuration_m5407_ParameterInfos[] = 
{
	{"options", 0, 134218010, 0, &ColorOptions_t1011_0_0_0},
	{"unitsXSecond", 1, 134218011, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218012, 0, &Color2_t1000_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_ColorOptions_t1011_Single_t151_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Color2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,DG.Tweening.Color2)
extern const MethodInfo Color2Plugin_GetSpeedBasedDuration_m5407_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Color2Plugin_GetSpeedBasedDuration_m5407/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_ColorOptions_t1011_Single_t151_Color2_t1000/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_GetSpeedBasedDuration_m5407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1011_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1032_0_0_0;
extern const Il2CppType DOGetter_1_t1032_0_0_0;
extern const Il2CppType DOSetter_1_t1033_0_0_0;
extern const Il2CppType DOSetter_1_t1033_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Color2Plugin_t975_Color2Plugin_EvaluateAndApply_m5408_ParameterInfos[] = 
{
	{"options", 0, 134218013, 0, &ColorOptions_t1011_0_0_0},
	{"t", 1, 134218014, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218015, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218016, 0, &DOGetter_1_t1032_0_0_0},
	{"setter", 4, 134218017, 0, &DOSetter_1_t1033_0_0_0},
	{"elapsed", 5, 134218018, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218019, 0, &Color2_t1000_0_0_0},
	{"changeValue", 7, 134218020, 0, &Color2_t1000_0_0_0},
	{"duration", 8, 134218021, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218022, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218023, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_ColorOptions_t1011_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Color2_t1000_Color2_t1000_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,System.Single,DG.Tweening.Color2,DG.Tweening.Color2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Color2Plugin_EvaluateAndApply_m5408_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Color2Plugin_EvaluateAndApply_m5408/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_ColorOptions_t1011_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Color2_t1000_Color2_t1000_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, Color2Plugin_t975_Color2Plugin_EvaluateAndApply_m5408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::.ctor()
extern const MethodInfo Color2Plugin__ctor_m5409_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Color2Plugin__ctor_m5409/* method */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Color2Plugin_t975_MethodInfos[] =
{
	&Color2Plugin_Reset_m5403_MethodInfo,
	&Color2Plugin_ConvertToStartValue_m5404_MethodInfo,
	&Color2Plugin_SetRelativeEndValue_m5405_MethodInfo,
	&Color2Plugin_SetChangeValue_m5406_MethodInfo,
	&Color2Plugin_GetSpeedBasedDuration_m5407_MethodInfo,
	&Color2Plugin_EvaluateAndApply_m5408_MethodInfo,
	&Color2Plugin__ctor_m5409_MethodInfo,
	NULL
};
extern const MethodInfo Color2Plugin_Reset_m5403_MethodInfo;
extern const MethodInfo Color2Plugin_ConvertToStartValue_m5404_MethodInfo;
extern const MethodInfo Color2Plugin_SetRelativeEndValue_m5405_MethodInfo;
extern const MethodInfo Color2Plugin_SetChangeValue_m5406_MethodInfo;
extern const MethodInfo Color2Plugin_GetSpeedBasedDuration_m5407_MethodInfo;
extern const MethodInfo Color2Plugin_EvaluateAndApply_m5408_MethodInfo;
static const Il2CppMethodReference Color2Plugin_t975_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Color2Plugin_Reset_m5403_MethodInfo,
	&Color2Plugin_ConvertToStartValue_m5404_MethodInfo,
	&Color2Plugin_SetRelativeEndValue_m5405_MethodInfo,
	&Color2Plugin_SetChangeValue_m5406_MethodInfo,
	&Color2Plugin_GetSpeedBasedDuration_m5407_MethodInfo,
	&Color2Plugin_EvaluateAndApply_m5408_MethodInfo,
};
static bool Color2Plugin_t975_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Color2Plugin_t975_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Color2Plugin_t975_0_0_0;
extern const Il2CppType Color2Plugin_t975_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t976_0_0_0;
struct Color2Plugin_t975;
const Il2CppTypeDefinitionMetadata Color2Plugin_t975_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Color2Plugin_t975_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t976_0_0_0/* parent */
	, Color2Plugin_t975_VTable/* vtableMethods */
	, Color2Plugin_t975_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Color2Plugin_t975_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color2Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Color2Plugin_t975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Color2Plugin_t975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Color2Plugin_t975_0_0_0/* byval_arg */
	, &Color2Plugin_t975_1_0_0/* this_arg */
	, &Color2Plugin_t975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color2Plugin_t975)/* instance_size */
	, sizeof (Color2Plugin_t975)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"
// Metadata Definition DG.Tweening.Core.TweenManager/CapacityIncreaseMode
extern TypeInfo CapacityIncreaseMode_t977_il2cpp_TypeInfo;
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseModeMethodDeclarations.h"
static const MethodInfo* CapacityIncreaseMode_t977_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CapacityIncreaseMode_t977_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool CapacityIncreaseMode_t977_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CapacityIncreaseMode_t977_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType CapacityIncreaseMode_t977_0_0_0;
extern const Il2CppType CapacityIncreaseMode_t977_1_0_0;
extern TypeInfo TweenManager_t980_il2cpp_TypeInfo;
extern const Il2CppType TweenManager_t980_0_0_0;
const Il2CppTypeDefinitionMetadata CapacityIncreaseMode_t977_DefinitionMetadata = 
{
	&TweenManager_t980_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CapacityIncreaseMode_t977_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, CapacityIncreaseMode_t977_VTable/* vtableMethods */
	, CapacityIncreaseMode_t977_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 198/* fieldStart */

};
TypeInfo CapacityIncreaseMode_t977_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapacityIncreaseMode"/* name */
	, ""/* namespaze */
	, CapacityIncreaseMode_t977_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapacityIncreaseMode_t977_0_0_0/* byval_arg */
	, &CapacityIncreaseMode_t977_1_0_0/* this_arg */
	, &CapacityIncreaseMode_t977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapacityIncreaseMode_t977)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CapacityIncreaseMode_t977)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManager.h"
// Metadata Definition DG.Tweening.Core.TweenManager
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManagerMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1087_0_0_0;
extern const Il2CppGenericContainer TweenManager_GetTweener_m5612_Il2CppGenericContainer;
extern TypeInfo TweenManager_GetTweener_m5612_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenManager_GetTweener_m5612_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5612_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo TweenManager_GetTweener_m5612_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenManager_GetTweener_m5612_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5612_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo TweenManager_GetTweener_m5612_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* TweenManager_GetTweener_m5612_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter TweenManager_GetTweener_m5612_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5612_Il2CppGenericContainer, TweenManager_GetTweener_m5612_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* TweenManager_GetTweener_m5612_Il2CppGenericParametersArray[3] = 
{
	&TweenManager_GetTweener_m5612_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&TweenManager_GetTweener_m5612_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&TweenManager_GetTweener_m5612_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenManager_GetTweener_m5612_MethodInfo;
extern const Il2CppGenericContainer TweenManager_GetTweener_m5612_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenManager_GetTweener_m5612_MethodInfo, 3, 1, TweenManager_GetTweener_m5612_Il2CppGenericParametersArray };
extern const Il2CppType TweenManager_GetTweener_m5612_gp_0_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m5612_gp_1_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m5612_gp_2_0_0_0;
extern const Il2CppGenericMethod TweenerCore_3__ctor_m5625_GenericMethod;
static Il2CppRGCTXDefinition TweenManager_GetTweener_m5612_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5612_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5612_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5612_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenerCore_3_t1087_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &TweenerCore_3__ctor_m5625_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenManager::GetTweener()
extern const MethodInfo TweenManager_GetTweener_m5612_MethodInfo = 
{
	"GetTweener"/* name */
	, NULL/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1087_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, TweenManager_GetTweener_m5612_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenManager_GetTweener_m5612_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern const MethodInfo TweenManager_GetSequence_m5410_MethodInfo = 
{
	"GetSequence"/* name */
	, (methodPointerType)&TweenManager_GetSequence_m5410/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_AddActiveTweenToSequence_m5411_ParameterInfos[] = 
{
	{"t", 0, 134218024, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern const MethodInfo TweenManager_AddActiveTweenToSequence_m5411_MethodInfo = 
{
	"AddActiveTweenToSequence"/* name */
	, (methodPointerType)&TweenManager_AddActiveTweenToSequence_m5411/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TweenManager_t980_TweenManager_AddActiveTweenToSequence_m5411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo TweenManager_t980_TweenManager_Despawn_m5412_ParameterInfos[] = 
{
	{"t", 0, 134218025, 0, &Tween_t934_0_0_0},
	{"modifyActiveLists", 1, 134218026, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern const MethodInfo TweenManager_Despawn_m5412_MethodInfo = 
{
	"Despawn"/* name */
	, (methodPointerType)&TweenManager_Despawn_m5412/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, TweenManager_t980_TweenManager_Despawn_m5412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_SetCapacities_m5413_ParameterInfos[] = 
{
	{"tweenersCapacity", 0, 134218027, 0, &Int32_t127_0_0_0},
	{"sequencesCapacity", 1, 134218028, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern const MethodInfo TweenManager_SetCapacities_m5413_MethodInfo = 
{
	"SetCapacities"/* name */
	, (methodPointerType)&TweenManager_SetCapacities_m5413/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, TweenManager_t980_TweenManager_SetCapacities_m5413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern const MethodInfo TweenManager_Validate_m5414_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&TweenManager_Validate_m5414/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UpdateType_t926_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_Update_m5415_ParameterInfos[] = 
{
	{"updateType", 0, 134218029, 0, &UpdateType_t926_0_0_0},
	{"deltaTime", 1, 134218030, 0, &Single_t151_0_0_0},
	{"independentTime", 2, 134218031, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern const MethodInfo TweenManager_Update_m5415_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TweenManager_Update_m5415/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Single_t151_Single_t151/* invoker_method */
	, TweenManager_t980_TweenManager_Update_m5415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
extern const Il2CppType UpdateMode_t929_0_0_4112;
static const ParameterInfo TweenManager_t980_TweenManager_Goto_m5416_ParameterInfos[] = 
{
	{"t", 0, 134218032, 0, &Tween_t934_0_0_0},
	{"to", 1, 134218033, 0, &Single_t151_0_0_0},
	{"andPlay", 2, 134218034, 0, &Boolean_t169_0_0_4112},
	{"updateMode", 3, 134218035, 0, &UpdateMode_t929_0_0_4112},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo TweenManager_Goto_m5416_MethodInfo = 
{
	"Goto"/* name */
	, (methodPointerType)&TweenManager_Goto_m5416/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, TweenManager_t980_TweenManager_Goto_m5416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_MarkForKilling_m5417_ParameterInfos[] = 
{
	{"t", 0, 134218036, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern const MethodInfo TweenManager_MarkForKilling_m5417_MethodInfo = 
{
	"MarkForKilling"/* name */
	, (methodPointerType)&TweenManager_MarkForKilling_m5417/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TweenManager_t980_TweenManager_MarkForKilling_m5417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_AddActiveTween_m5418_ParameterInfos[] = 
{
	{"t", 0, 134218037, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern const MethodInfo TweenManager_AddActiveTween_m5418_MethodInfo = 
{
	"AddActiveTween"/* name */
	, (methodPointerType)&TweenManager_AddActiveTween_m5418/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TweenManager_t980_TweenManager_AddActiveTween_m5418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern const MethodInfo TweenManager_ReorganizeActiveTweens_m5419_MethodInfo = 
{
	"ReorganizeActiveTweens"/* name */
	, (methodPointerType)&TweenManager_ReorganizeActiveTweens_m5419/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t946_0_0_0;
extern const Il2CppType List_1_t946_0_0_0;
extern const Il2CppType Boolean_t169_0_0_4112;
static const ParameterInfo TweenManager_t980_TweenManager_DespawnTweens_m5420_ParameterInfos[] = 
{
	{"tweens", 0, 134218038, 0, &List_1_t946_0_0_0},
	{"modifyActiveLists", 1, 134218039, 0, &Boolean_t169_0_0_4112},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::DespawnTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>,System.Boolean)
extern const MethodInfo TweenManager_DespawnTweens_m5420_MethodInfo = 
{
	"DespawnTweens"/* name */
	, (methodPointerType)&TweenManager_DespawnTweens_m5420/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, TweenManager_t980_TweenManager_DespawnTweens_m5420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t934_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_RemoveActiveTween_m5421_ParameterInfos[] = 
{
	{"t", 0, 134218040, 0, &Tween_t934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern const MethodInfo TweenManager_RemoveActiveTween_m5421_MethodInfo = 
{
	"RemoveActiveTween"/* name */
	, (methodPointerType)&TweenManager_RemoveActiveTween_m5421/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TweenManager_t980_TweenManager_RemoveActiveTween_m5421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapacityIncreaseMode_t977_0_0_0;
static const ParameterInfo TweenManager_t980_TweenManager_IncreaseCapacities_m5422_ParameterInfos[] = 
{
	{"increaseMode", 0, 134218041, 0, &CapacityIncreaseMode_t977_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern const MethodInfo TweenManager_IncreaseCapacities_m5422_MethodInfo = 
{
	"IncreaseCapacities"/* name */
	, (methodPointerType)&TweenManager_IncreaseCapacities_m5422/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, TweenManager_t980_TweenManager_IncreaseCapacities_m5422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::.cctor()
extern const MethodInfo TweenManager__cctor_m5423_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TweenManager__cctor_m5423/* method */
	, &TweenManager_t980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenManager_t980_MethodInfos[] =
{
	&TweenManager_GetTweener_m5612_MethodInfo,
	&TweenManager_GetSequence_m5410_MethodInfo,
	&TweenManager_AddActiveTweenToSequence_m5411_MethodInfo,
	&TweenManager_Despawn_m5412_MethodInfo,
	&TweenManager_SetCapacities_m5413_MethodInfo,
	&TweenManager_Validate_m5414_MethodInfo,
	&TweenManager_Update_m5415_MethodInfo,
	&TweenManager_Goto_m5416_MethodInfo,
	&TweenManager_MarkForKilling_m5417_MethodInfo,
	&TweenManager_AddActiveTween_m5418_MethodInfo,
	&TweenManager_ReorganizeActiveTweens_m5419_MethodInfo,
	&TweenManager_DespawnTweens_m5420_MethodInfo,
	&TweenManager_RemoveActiveTween_m5421_MethodInfo,
	&TweenManager_IncreaseCapacities_m5422_MethodInfo,
	&TweenManager__cctor_m5423_MethodInfo,
	NULL
};
static const Il2CppType* TweenManager_t980_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CapacityIncreaseMode_t977_0_0_0,
};
static const Il2CppMethodReference TweenManager_t980_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TweenManager_t980_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenManager_t980_1_0_0;
struct TweenManager_t980;
const Il2CppTypeDefinitionMetadata TweenManager_t980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TweenManager_t980_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenManager_t980_VTable/* vtableMethods */
	, TweenManager_t980_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 202/* fieldStart */

};
TypeInfo TweenManager_t980_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenManager"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, TweenManager_t980_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenManager_t980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenManager_t980_0_0_0/* byval_arg */
	, &TweenManager_t980_1_0_0/* this_arg */
	, &TweenManager_t980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenManager_t980)/* instance_size */
	, sizeof (TweenManager_t980)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TweenManager_t980_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPlugin.h"
// Metadata Definition DG.Tweening.Plugins.RectPlugin
extern TypeInfo RectPlugin_t981_il2cpp_TypeInfo;
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_Reset_m5424_ParameterInfos[] = 
{
	{"t", 0, 134218042, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_Reset_m5424_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&RectPlugin_Reset_m5424/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectPlugin_t981_RectPlugin_Reset_m5424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
extern const Il2CppType Rect_t124_0_0_0;
extern const Il2CppType Rect_t124_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_ConvertToStartValue_m5425_ParameterInfos[] = 
{
	{"t", 0, 134218043, 0, &TweenerCore_3_t1034_0_0_0},
	{"value", 1, 134218044, 0, &Rect_t124_0_0_0},
};
extern void* RuntimeInvoker_Rect_t124_Object_t_Rect_t124 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern const MethodInfo RectPlugin_ConvertToStartValue_m5425_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&RectPlugin_ConvertToStartValue_m5425/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t124_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t124_Object_t_Rect_t124/* invoker_method */
	, RectPlugin_t981_RectPlugin_ConvertToStartValue_m5425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_SetRelativeEndValue_m5426_ParameterInfos[] = 
{
	{"t", 0, 134218045, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_SetRelativeEndValue_m5426_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&RectPlugin_SetRelativeEndValue_m5426/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectPlugin_t981_RectPlugin_SetRelativeEndValue_m5426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_SetChangeValue_m5427_ParameterInfos[] = 
{
	{"t", 0, 134218046, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_SetChangeValue_m5427_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&RectPlugin_SetChangeValue_m5427/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RectPlugin_t981_RectPlugin_SetChangeValue_m5427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOptions_t1004_0_0_0;
extern const Il2CppType RectOptions_t1004_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Rect_t124_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_GetSpeedBasedDuration_m5428_ParameterInfos[] = 
{
	{"options", 0, 134218047, 0, &RectOptions_t1004_0_0_0},
	{"unitsXSecond", 1, 134218048, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218049, 0, &Rect_t124_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_RectOptions_t1004_Single_t151_Rect_t124 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern const MethodInfo RectPlugin_GetSpeedBasedDuration_m5428_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&RectPlugin_GetSpeedBasedDuration_m5428/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_RectOptions_t1004_Single_t151_Rect_t124/* invoker_method */
	, RectPlugin_t981_RectPlugin_GetSpeedBasedDuration_m5428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOptions_t1004_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1035_0_0_0;
extern const Il2CppType DOGetter_1_t1035_0_0_0;
extern const Il2CppType DOSetter_1_t1036_0_0_0;
extern const Il2CppType DOSetter_1_t1036_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Rect_t124_0_0_0;
extern const Il2CppType Rect_t124_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo RectPlugin_t981_RectPlugin_EvaluateAndApply_m5429_ParameterInfos[] = 
{
	{"options", 0, 134218050, 0, &RectOptions_t1004_0_0_0},
	{"t", 1, 134218051, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218052, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218053, 0, &DOGetter_1_t1035_0_0_0},
	{"setter", 4, 134218054, 0, &DOSetter_1_t1036_0_0_0},
	{"elapsed", 5, 134218055, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218056, 0, &Rect_t124_0_0_0},
	{"changeValue", 7, 134218057, 0, &Rect_t124_0_0_0},
	{"duration", 8, 134218058, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218059, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218060, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_RectOptions_t1004_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Rect_t124_Rect_t124_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo RectPlugin_EvaluateAndApply_m5429_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&RectPlugin_EvaluateAndApply_m5429/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_RectOptions_t1004_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Rect_t124_Rect_t124_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, RectPlugin_t981_RectPlugin_EvaluateAndApply_m5429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern const MethodInfo RectPlugin__ctor_m5430_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RectPlugin__ctor_m5430/* method */
	, &RectPlugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RectPlugin_t981_MethodInfos[] =
{
	&RectPlugin_Reset_m5424_MethodInfo,
	&RectPlugin_ConvertToStartValue_m5425_MethodInfo,
	&RectPlugin_SetRelativeEndValue_m5426_MethodInfo,
	&RectPlugin_SetChangeValue_m5427_MethodInfo,
	&RectPlugin_GetSpeedBasedDuration_m5428_MethodInfo,
	&RectPlugin_EvaluateAndApply_m5429_MethodInfo,
	&RectPlugin__ctor_m5430_MethodInfo,
	NULL
};
extern const MethodInfo RectPlugin_Reset_m5424_MethodInfo;
extern const MethodInfo RectPlugin_ConvertToStartValue_m5425_MethodInfo;
extern const MethodInfo RectPlugin_SetRelativeEndValue_m5426_MethodInfo;
extern const MethodInfo RectPlugin_SetChangeValue_m5427_MethodInfo;
extern const MethodInfo RectPlugin_GetSpeedBasedDuration_m5428_MethodInfo;
extern const MethodInfo RectPlugin_EvaluateAndApply_m5429_MethodInfo;
static const Il2CppMethodReference RectPlugin_t981_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RectPlugin_Reset_m5424_MethodInfo,
	&RectPlugin_ConvertToStartValue_m5425_MethodInfo,
	&RectPlugin_SetRelativeEndValue_m5426_MethodInfo,
	&RectPlugin_SetChangeValue_m5427_MethodInfo,
	&RectPlugin_GetSpeedBasedDuration_m5428_MethodInfo,
	&RectPlugin_EvaluateAndApply_m5429_MethodInfo,
};
static bool RectPlugin_t981_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RectPlugin_t981_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectPlugin_t981_0_0_0;
extern const Il2CppType RectPlugin_t981_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t982_0_0_0;
struct RectPlugin_t981;
const Il2CppTypeDefinitionMetadata RectPlugin_t981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectPlugin_t981_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t982_0_0_0/* parent */
	, RectPlugin_t981_VTable/* vtableMethods */
	, RectPlugin_t981_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RectPlugin_t981_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, RectPlugin_t981_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectPlugin_t981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectPlugin_t981_0_0_0/* byval_arg */
	, &RectPlugin_t981_1_0_0/* this_arg */
	, &RectPlugin_t981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectPlugin_t981)/* instance_size */
	, sizeof (RectPlugin_t981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtils.h"
// Metadata Definition DG.Tweening.Plugins.Core.SpecialPluginsUtils
extern TypeInfo SpecialPluginsUtils_t983_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtilsMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1028_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t983_SpecialPluginsUtils_SetLookAt_m5431_ParameterInfos[] = 
{
	{"t", 0, 134218061, 0, &TweenerCore_3_t1028_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo SpecialPluginsUtils_SetLookAt_m5431_MethodInfo = 
{
	"SetLookAt"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetLookAt_m5431/* method */
	, &SpecialPluginsUtils_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, SpecialPluginsUtils_t983_SpecialPluginsUtils_SetLookAt_m5431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t983_SpecialPluginsUtils_SetPunch_m5432_ParameterInfos[] = 
{
	{"t", 0, 134218062, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetPunch(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetPunch_m5432_MethodInfo = 
{
	"SetPunch"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetPunch_m5432/* method */
	, &SpecialPluginsUtils_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, SpecialPluginsUtils_t983_SpecialPluginsUtils_SetPunch_m5432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t983_SpecialPluginsUtils_SetShake_m5433_ParameterInfos[] = 
{
	{"t", 0, 134218063, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetShake(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetShake_m5433_MethodInfo = 
{
	"SetShake"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetShake_m5433/* method */
	, &SpecialPluginsUtils_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, SpecialPluginsUtils_t983_SpecialPluginsUtils_SetShake_m5433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1024_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t983_SpecialPluginsUtils_SetCameraShakePosition_m5434_ParameterInfos[] = 
{
	{"t", 0, 134218064, 0, &TweenerCore_3_t1024_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetCameraShakePosition(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetCameraShakePosition_m5434_MethodInfo = 
{
	"SetCameraShakePosition"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetCameraShakePosition_m5434/* method */
	, &SpecialPluginsUtils_t983_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, SpecialPluginsUtils_t983_SpecialPluginsUtils_SetCameraShakePosition_m5434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpecialPluginsUtils_t983_MethodInfos[] =
{
	&SpecialPluginsUtils_SetLookAt_m5431_MethodInfo,
	&SpecialPluginsUtils_SetPunch_m5432_MethodInfo,
	&SpecialPluginsUtils_SetShake_m5433_MethodInfo,
	&SpecialPluginsUtils_SetCameraShakePosition_m5434_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpecialPluginsUtils_t983_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SpecialPluginsUtils_t983_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SpecialPluginsUtils_t983_0_0_0;
extern const Il2CppType SpecialPluginsUtils_t983_1_0_0;
struct SpecialPluginsUtils_t983;
const Il2CppTypeDefinitionMetadata SpecialPluginsUtils_t983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SpecialPluginsUtils_t983_VTable/* vtableMethods */
	, SpecialPluginsUtils_t983_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SpecialPluginsUtils_t983_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialPluginsUtils"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, SpecialPluginsUtils_t983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpecialPluginsUtils_t983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpecialPluginsUtils_t983_0_0_0/* byval_arg */
	, &SpecialPluginsUtils_t983_1_0_0/* this_arg */
	, &SpecialPluginsUtils_t983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialPluginsUtils_t983)/* instance_size */
	, sizeof (SpecialPluginsUtils_t983)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPlugin.h"
// Metadata Definition DG.Tweening.Plugins.UlongPlugin
extern TypeInfo UlongPlugin_t984_il2cpp_TypeInfo;
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_Reset_m5435_ParameterInfos[] = 
{
	{"t", 0, 134218065, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_Reset_m5435_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&UlongPlugin_Reset_m5435/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_Reset_m5435_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_ConvertToStartValue_m5436_ParameterInfos[] = 
{
	{"t", 0, 134218066, 0, &TweenerCore_3_t1037_0_0_0},
	{"value", 1, 134218067, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_UInt64_t1091_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.UInt64 DG.Tweening.Plugins.UlongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.UInt64)
extern const MethodInfo UlongPlugin_ConvertToStartValue_m5436_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&UlongPlugin_ConvertToStartValue_m5436/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &UInt64_t1091_0_0_0/* return_type */
	, RuntimeInvoker_UInt64_t1091_Object_t_Int64_t1092/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_ConvertToStartValue_m5436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_SetRelativeEndValue_m5437_ParameterInfos[] = 
{
	{"t", 0, 134218068, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_SetRelativeEndValue_m5437_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&UlongPlugin_SetRelativeEndValue_m5437/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_SetRelativeEndValue_m5437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_SetChangeValue_m5438_ParameterInfos[] = 
{
	{"t", 0, 134218069, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_SetChangeValue_m5438_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&UlongPlugin_SetChangeValue_m5438/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_SetChangeValue_m5438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_GetSpeedBasedDuration_m5439_ParameterInfos[] = 
{
	{"options", 0, 134218070, 0, &NoOptions_t933_0_0_0},
	{"unitsXSecond", 1, 134218071, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218072, 0, &UInt64_t1091_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.UlongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt64)
extern const MethodInfo UlongPlugin_GetSpeedBasedDuration_m5439_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&UlongPlugin_GetSpeedBasedDuration_m5439/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int64_t1092/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_GetSpeedBasedDuration_m5439_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1038_0_0_0;
extern const Il2CppType DOGetter_1_t1038_0_0_0;
extern const Il2CppType DOSetter_1_t1039_0_0_0;
extern const Il2CppType DOSetter_1_t1039_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType UInt64_t1091_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo UlongPlugin_t984_UlongPlugin_EvaluateAndApply_m5440_ParameterInfos[] = 
{
	{"options", 0, 134218073, 0, &NoOptions_t933_0_0_0},
	{"t", 1, 134218074, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218075, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218076, 0, &DOGetter_1_t1038_0_0_0},
	{"setter", 4, 134218077, 0, &DOSetter_1_t1039_0_0_0},
	{"elapsed", 5, 134218078, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218079, 0, &UInt64_t1091_0_0_0},
	{"changeValue", 7, 134218080, 0, &UInt64_t1091_0_0_0},
	{"duration", 8, 134218081, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218082, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218083, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int64_t1092_Int64_t1092_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.Single,System.UInt64,System.UInt64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo UlongPlugin_EvaluateAndApply_m5440_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&UlongPlugin_EvaluateAndApply_m5440/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int64_t1092_Int64_t1092_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, UlongPlugin_t984_UlongPlugin_EvaluateAndApply_m5440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::.ctor()
extern const MethodInfo UlongPlugin__ctor_m5441_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UlongPlugin__ctor_m5441/* method */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UlongPlugin_t984_MethodInfos[] =
{
	&UlongPlugin_Reset_m5435_MethodInfo,
	&UlongPlugin_ConvertToStartValue_m5436_MethodInfo,
	&UlongPlugin_SetRelativeEndValue_m5437_MethodInfo,
	&UlongPlugin_SetChangeValue_m5438_MethodInfo,
	&UlongPlugin_GetSpeedBasedDuration_m5439_MethodInfo,
	&UlongPlugin_EvaluateAndApply_m5440_MethodInfo,
	&UlongPlugin__ctor_m5441_MethodInfo,
	NULL
};
extern const MethodInfo UlongPlugin_Reset_m5435_MethodInfo;
extern const MethodInfo UlongPlugin_ConvertToStartValue_m5436_MethodInfo;
extern const MethodInfo UlongPlugin_SetRelativeEndValue_m5437_MethodInfo;
extern const MethodInfo UlongPlugin_SetChangeValue_m5438_MethodInfo;
extern const MethodInfo UlongPlugin_GetSpeedBasedDuration_m5439_MethodInfo;
extern const MethodInfo UlongPlugin_EvaluateAndApply_m5440_MethodInfo;
static const Il2CppMethodReference UlongPlugin_t984_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UlongPlugin_Reset_m5435_MethodInfo,
	&UlongPlugin_ConvertToStartValue_m5436_MethodInfo,
	&UlongPlugin_SetRelativeEndValue_m5437_MethodInfo,
	&UlongPlugin_SetChangeValue_m5438_MethodInfo,
	&UlongPlugin_GetSpeedBasedDuration_m5439_MethodInfo,
	&UlongPlugin_EvaluateAndApply_m5440_MethodInfo,
};
static bool UlongPlugin_t984_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UlongPlugin_t984_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UlongPlugin_t984_0_0_0;
extern const Il2CppType UlongPlugin_t984_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t985_0_0_0;
struct UlongPlugin_t984;
const Il2CppTypeDefinitionMetadata UlongPlugin_t984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UlongPlugin_t984_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t985_0_0_0/* parent */
	, UlongPlugin_t984_VTable/* vtableMethods */
	, UlongPlugin_t984_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UlongPlugin_t984_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UlongPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, UlongPlugin_t984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UlongPlugin_t984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UlongPlugin_t984_0_0_0/* byval_arg */
	, &UlongPlugin_t984_1_0_0/* this_arg */
	, &UlongPlugin_t984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UlongPlugin_t984)/* instance_size */
	, sizeof (UlongPlugin_t984)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPlugin.h"
// Metadata Definition DG.Tweening.Plugins.IntPlugin
extern TypeInfo IntPlugin_t986_il2cpp_TypeInfo;
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_Reset_m5442_ParameterInfos[] = 
{
	{"t", 0, 134218084, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_Reset_m5442_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&IntPlugin_Reset_m5442/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IntPlugin_t986_IntPlugin_Reset_m5442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_ConvertToStartValue_m5443_ParameterInfos[] = 
{
	{"t", 0, 134218085, 0, &TweenerCore_3_t1040_0_0_0},
	{"value", 1, 134218086, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern const MethodInfo IntPlugin_ConvertToStartValue_m5443_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&IntPlugin_ConvertToStartValue_m5443/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32_t127/* invoker_method */
	, IntPlugin_t986_IntPlugin_ConvertToStartValue_m5443_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_SetRelativeEndValue_m5444_ParameterInfos[] = 
{
	{"t", 0, 134218087, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_SetRelativeEndValue_m5444_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&IntPlugin_SetRelativeEndValue_m5444/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IntPlugin_t986_IntPlugin_SetRelativeEndValue_m5444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_SetChangeValue_m5445_ParameterInfos[] = 
{
	{"t", 0, 134218088, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_SetChangeValue_m5445_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&IntPlugin_SetChangeValue_m5445/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IntPlugin_t986_IntPlugin_SetChangeValue_m5445_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_GetSpeedBasedDuration_m5446_ParameterInfos[] = 
{
	{"options", 0, 134218089, 0, &NoOptions_t933_0_0_0},
	{"unitsXSecond", 1, 134218090, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218091, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern const MethodInfo IntPlugin_GetSpeedBasedDuration_m5446_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&IntPlugin_GetSpeedBasedDuration_m5446/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int32_t127/* invoker_method */
	, IntPlugin_t986_IntPlugin_GetSpeedBasedDuration_m5446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1041_0_0_0;
extern const Il2CppType DOGetter_1_t1041_0_0_0;
extern const Il2CppType DOSetter_1_t1042_0_0_0;
extern const Il2CppType DOSetter_1_t1042_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo IntPlugin_t986_IntPlugin_EvaluateAndApply_m5447_ParameterInfos[] = 
{
	{"options", 0, 134218092, 0, &NoOptions_t933_0_0_0},
	{"t", 1, 134218093, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218094, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218095, 0, &DOGetter_1_t1041_0_0_0},
	{"setter", 4, 134218096, 0, &DOSetter_1_t1042_0_0_0},
	{"elapsed", 5, 134218097, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218098, 0, &Int32_t127_0_0_0},
	{"changeValue", 7, 134218099, 0, &Int32_t127_0_0_0},
	{"duration", 8, 134218100, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218101, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218102, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int32_t127_Int32_t127_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo IntPlugin_EvaluateAndApply_m5447_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&IntPlugin_EvaluateAndApply_m5447/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int32_t127_Int32_t127_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, IntPlugin_t986_IntPlugin_EvaluateAndApply_m5447_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern const MethodInfo IntPlugin__ctor_m5448_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntPlugin__ctor_m5448/* method */
	, &IntPlugin_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntPlugin_t986_MethodInfos[] =
{
	&IntPlugin_Reset_m5442_MethodInfo,
	&IntPlugin_ConvertToStartValue_m5443_MethodInfo,
	&IntPlugin_SetRelativeEndValue_m5444_MethodInfo,
	&IntPlugin_SetChangeValue_m5445_MethodInfo,
	&IntPlugin_GetSpeedBasedDuration_m5446_MethodInfo,
	&IntPlugin_EvaluateAndApply_m5447_MethodInfo,
	&IntPlugin__ctor_m5448_MethodInfo,
	NULL
};
extern const MethodInfo IntPlugin_Reset_m5442_MethodInfo;
extern const MethodInfo IntPlugin_ConvertToStartValue_m5443_MethodInfo;
extern const MethodInfo IntPlugin_SetRelativeEndValue_m5444_MethodInfo;
extern const MethodInfo IntPlugin_SetChangeValue_m5445_MethodInfo;
extern const MethodInfo IntPlugin_GetSpeedBasedDuration_m5446_MethodInfo;
extern const MethodInfo IntPlugin_EvaluateAndApply_m5447_MethodInfo;
static const Il2CppMethodReference IntPlugin_t986_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&IntPlugin_Reset_m5442_MethodInfo,
	&IntPlugin_ConvertToStartValue_m5443_MethodInfo,
	&IntPlugin_SetRelativeEndValue_m5444_MethodInfo,
	&IntPlugin_SetChangeValue_m5445_MethodInfo,
	&IntPlugin_GetSpeedBasedDuration_m5446_MethodInfo,
	&IntPlugin_EvaluateAndApply_m5447_MethodInfo,
};
static bool IntPlugin_t986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IntPlugin_t986_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType IntPlugin_t986_0_0_0;
extern const Il2CppType IntPlugin_t986_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t987_0_0_0;
struct IntPlugin_t986;
const Il2CppTypeDefinitionMetadata IntPlugin_t986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IntPlugin_t986_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t987_0_0_0/* parent */
	, IntPlugin_t986_VTable/* vtableMethods */
	, IntPlugin_t986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IntPlugin_t986_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, IntPlugin_t986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IntPlugin_t986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntPlugin_t986_0_0_0/* byval_arg */
	, &IntPlugin_t986_1_0_0/* this_arg */
	, &IntPlugin_t986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntPlugin_t986)/* instance_size */
	, sizeof (IntPlugin_t986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTween.h"
// Metadata Definition DG.Tweening.DOTween
extern TypeInfo DOTween_t119_il2cpp_TypeInfo;
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
extern void* RuntimeInvoker_LogBehaviour_t956 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern const MethodInfo DOTween_get_logBehaviour_m5449_MethodInfo = 
{
	"get_logBehaviour"/* name */
	, (methodPointerType)&DOTween_get_logBehaviour_m5449/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &LogBehaviour_t956_0_0_0/* return_type */
	, RuntimeInvoker_LogBehaviour_t956/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogBehaviour_t956_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_set_logBehaviour_m5450_ParameterInfos[] = 
{
	{"value", 0, 134218103, 0, &LogBehaviour_t956_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern const MethodInfo DOTween_set_logBehaviour_m5450_MethodInfo = 
{
	"set_logBehaviour"/* name */
	, (methodPointerType)&DOTween_set_logBehaviour_m5450/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, DOTween_t119_DOTween_set_logBehaviour_m5450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::.cctor()
extern const MethodInfo DOTween__cctor_m5451_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DOTween__cctor_m5451/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Nullable_1_t117_0_0_4112;
extern const Il2CppType Nullable_1_t117_0_0_0;
extern const Il2CppType Nullable_1_t117_0_0_4112;
extern const Il2CppType Nullable_1_t118_0_0_4112;
extern const Il2CppType Nullable_1_t118_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_Init_m359_ParameterInfos[] = 
{
	{"recycleAllByDefault", 0, 134218104, 0, &Nullable_1_t117_0_0_4112},
	{"useSafeMode", 1, 134218105, 0, &Nullable_1_t117_0_0_4112},
	{"logBehaviour", 2, 134218106, 0, &Nullable_1_t118_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Nullable_1_t117_Nullable_1_t117_Nullable_1_t118 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern const MethodInfo DOTween_Init_m359_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&DOTween_Init_m359/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Nullable_1_t117_Nullable_1_t117_Nullable_1_t118/* invoker_method */
	, DOTween_t119_DOTween_Init_m359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::AutoInit()
extern const MethodInfo DOTween_AutoInit_m5452_MethodInfo = 
{
	"AutoInit"/* name */
	, (methodPointerType)&DOTween_AutoInit_m5452/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOTweenSettings_t954_0_0_0;
extern const Il2CppType Nullable_1_t117_0_0_0;
extern const Il2CppType Nullable_1_t117_0_0_0;
extern const Il2CppType Nullable_1_t118_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_Init_m5453_ParameterInfos[] = 
{
	{"settings", 0, 134218107, 0, &DOTweenSettings_t954_0_0_0},
	{"recycleAllByDefault", 1, 134218108, 0, &Nullable_1_t117_0_0_0},
	{"useSafeMode", 2, 134218109, 0, &Nullable_1_t117_0_0_0},
	{"logBehaviour", 3, 134218110, 0, &Nullable_1_t118_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Nullable_1_t117_Nullable_1_t117_Nullable_1_t118 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern const MethodInfo DOTween_Init_m5453_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&DOTween_Init_m5453/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Nullable_1_t117_Nullable_1_t117_Nullable_1_t118/* invoker_method */
	, DOTween_t119_DOTween_Init_m5453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.DOTween::Validate()
extern const MethodInfo DOTween_Validate_m5454_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&DOTween_Validate_m5454/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t120_0_0_0;
extern const Il2CppType DOSetter_1_t121_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_To_m363_ParameterInfos[] = 
{
	{"getter", 0, 134218111, 0, &DOGetter_1_t120_0_0_0},
	{"setter", 1, 134218112, 0, &DOSetter_1_t121_0_0_0},
	{"endValue", 2, 134218113, 0, &Vector3_t15_0_0_0},
	{"duration", 3, 134218114, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern const MethodInfo DOTween_To_m363_MethodInfo = 
{
	"To"/* name */
	, (methodPointerType)&DOTween_To_m363/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151/* invoker_method */
	, DOTween_t119_DOTween_To_m363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1029_0_0_0;
extern const Il2CppType DOSetter_1_t1030_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_To_m5455_ParameterInfos[] = 
{
	{"getter", 0, 134218115, 0, &DOGetter_1_t1029_0_0_0},
	{"setter", 1, 134218116, 0, &DOSetter_1_t1030_0_0_0},
	{"endValue", 2, 134218117, 0, &Vector3_t15_0_0_0},
	{"duration", 3, 134218118, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern const MethodInfo DOTween_To_m5455_MethodInfo = 
{
	"To"/* name */
	, (methodPointerType)&DOTween_To_m5455/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1028_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t15_Single_t151/* invoker_method */
	, DOTween_t119_DOTween_To_m5455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern const MethodInfo DOTween_Sequence_m365_MethodInfo = 
{
	"Sequence"/* name */
	, (methodPointerType)&DOTween_Sequence_m365/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::InitCheck()
extern const MethodInfo DOTween_InitCheck_m5456_MethodInfo = 
{
	"InitCheck"/* name */
	, (methodPointerType)&DOTween_InitCheck_m5456/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1093_0_0_0;
extern const Il2CppType DOGetter_1_t1093_0_0_0;
extern const Il2CppType DOSetter_1_t1094_0_0_0;
extern const Il2CppType DOSetter_1_t1094_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m5613_gp_1_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m5613_gp_1_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1096_0_0_4112;
extern const Il2CppType ABSTweenPlugin_3_t1096_0_0_0;
static const ParameterInfo DOTween_t119_DOTween_ApplyTo_m5613_ParameterInfos[] = 
{
	{"getter", 0, 134218119, 0, &DOGetter_1_t1093_0_0_0},
	{"setter", 1, 134218120, 0, &DOSetter_1_t1094_0_0_0},
	{"endValue", 2, 134218121, 0, &DOTween_ApplyTo_m5613_gp_1_0_0_0},
	{"duration", 3, 134218122, 0, &Single_t151_0_0_0},
	{"plugin", 4, 134218123, 0, &ABSTweenPlugin_3_t1096_0_0_4112},
};
extern const Il2CppType TweenerCore_3_t1097_0_0_0;
extern const Il2CppGenericContainer DOTween_ApplyTo_m5613_Il2CppGenericContainer;
extern TypeInfo DOTween_ApplyTo_m5613_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOTween_ApplyTo_m5613_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5613_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo DOTween_ApplyTo_m5613_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOTween_ApplyTo_m5613_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5613_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo DOTween_ApplyTo_m5613_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* DOTween_ApplyTo_m5613_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter DOTween_ApplyTo_m5613_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5613_Il2CppGenericContainer, DOTween_ApplyTo_m5613_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* DOTween_ApplyTo_m5613_Il2CppGenericParametersArray[3] = 
{
	&DOTween_ApplyTo_m5613_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&DOTween_ApplyTo_m5613_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&DOTween_ApplyTo_m5613_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo DOTween_ApplyTo_m5613_MethodInfo;
extern const Il2CppGenericContainer DOTween_ApplyTo_m5613_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOTween_ApplyTo_m5613_MethodInfo, 3, 1, DOTween_ApplyTo_m5613_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod TweenManager_GetTweener_TisT1_t1098_TisT2_t1095_TisTPlugOptions_t1099_m5626_GenericMethod;
extern const Il2CppGenericMethod Tweener_Setup_TisT1_t1098_TisT2_t1095_TisTPlugOptions_t1099_m5627_GenericMethod;
static Il2CppRGCTXDefinition DOTween_ApplyTo_m5613_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &TweenManager_GetTweener_TisT1_t1098_TisT2_t1095_TisTPlugOptions_t1099_m5626_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_Setup_TisT1_t1098_TisT2_t1095_TisTPlugOptions_t1099_m5627_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern const MethodInfo DOTween_ApplyTo_m5613_MethodInfo = 
{
	"ApplyTo"/* name */
	, NULL/* method */
	, &DOTween_t119_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1097_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOTween_t119_DOTween_ApplyTo_m5613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 236/* token */
	, DOTween_ApplyTo_m5613_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DOTween_ApplyTo_m5613_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* DOTween_t119_MethodInfos[] =
{
	&DOTween_get_logBehaviour_m5449_MethodInfo,
	&DOTween_set_logBehaviour_m5450_MethodInfo,
	&DOTween__cctor_m5451_MethodInfo,
	&DOTween_Init_m359_MethodInfo,
	&DOTween_AutoInit_m5452_MethodInfo,
	&DOTween_Init_m5453_MethodInfo,
	&DOTween_Validate_m5454_MethodInfo,
	&DOTween_To_m363_MethodInfo,
	&DOTween_To_m5455_MethodInfo,
	&DOTween_Sequence_m365_MethodInfo,
	&DOTween_InitCheck_m5456_MethodInfo,
	&DOTween_ApplyTo_m5613_MethodInfo,
	NULL
};
extern const MethodInfo DOTween_get_logBehaviour_m5449_MethodInfo;
extern const MethodInfo DOTween_set_logBehaviour_m5450_MethodInfo;
static const PropertyInfo DOTween_t119____logBehaviour_PropertyInfo = 
{
	&DOTween_t119_il2cpp_TypeInfo/* parent */
	, "logBehaviour"/* name */
	, &DOTween_get_logBehaviour_m5449_MethodInfo/* get */
	, &DOTween_set_logBehaviour_m5450_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DOTween_t119_PropertyInfos[] =
{
	&DOTween_t119____logBehaviour_PropertyInfo,
	NULL
};
static const Il2CppMethodReference DOTween_t119_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool DOTween_t119_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTween_t119_0_0_0;
extern const Il2CppType DOTween_t119_1_0_0;
struct DOTween_t119;
const Il2CppTypeDefinitionMetadata DOTween_t119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DOTween_t119_VTable/* vtableMethods */
	, DOTween_t119_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 229/* fieldStart */

};
TypeInfo DOTween_t119_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTween"/* name */
	, "DG.Tweening"/* namespaze */
	, DOTween_t119_MethodInfos/* methods */
	, DOTween_t119_PropertyInfos/* properties */
	, NULL/* events */
	, &DOTween_t119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOTween_t119_0_0_0/* byval_arg */
	, &DOTween_t119_1_0_0/* this_arg */
	, &DOTween_t119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTween_t119)/* instance_size */
	, sizeof (DOTween_t119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DOTween_t119_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPlugin.h"
// Metadata Definition DG.Tweening.Plugins.StringPlugin
extern TypeInfo StringPlugin_t990_il2cpp_TypeInfo;
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_Reset_m5457_ParameterInfos[] = 
{
	{"t", 0, 134218124, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_Reset_m5457_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&StringPlugin_Reset_m5457/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StringPlugin_t990_StringPlugin_Reset_m5457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_ConvertToStartValue_m5458_ParameterInfos[] = 
{
	{"t", 0, 134218125, 0, &TweenerCore_3_t1043_0_0_0},
	{"value", 1, 134218126, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String DG.Tweening.Plugins.StringPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.String)
extern const MethodInfo StringPlugin_ConvertToStartValue_m5458_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&StringPlugin_ConvertToStartValue_m5458/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, StringPlugin_t990_StringPlugin_ConvertToStartValue_m5458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_SetRelativeEndValue_m5459_ParameterInfos[] = 
{
	{"t", 0, 134218127, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_SetRelativeEndValue_m5459_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&StringPlugin_SetRelativeEndValue_m5459/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StringPlugin_t990_StringPlugin_SetRelativeEndValue_m5459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_SetChangeValue_m5460_ParameterInfos[] = 
{
	{"t", 0, 134218128, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_SetChangeValue_m5460_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&StringPlugin_SetChangeValue_m5460/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StringPlugin_t990_StringPlugin_SetChangeValue_m5460_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1003_0_0_0;
extern const Il2CppType StringOptions_t1003_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_GetSpeedBasedDuration_m5461_ParameterInfos[] = 
{
	{"options", 0, 134218129, 0, &StringOptions_t1003_0_0_0},
	{"unitsXSecond", 1, 134218130, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218131, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_StringOptions_t1003_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.StringPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.StringOptions,System.Single,System.String)
extern const MethodInfo StringPlugin_GetSpeedBasedDuration_m5461_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&StringPlugin_GetSpeedBasedDuration_m5461/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_StringOptions_t1003_Single_t151_Object_t/* invoker_method */
	, StringPlugin_t990_StringPlugin_GetSpeedBasedDuration_m5461_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1003_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1044_0_0_0;
extern const Il2CppType DOGetter_1_t1044_0_0_0;
extern const Il2CppType DOSetter_1_t1045_0_0_0;
extern const Il2CppType DOSetter_1_t1045_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_EvaluateAndApply_m5462_ParameterInfos[] = 
{
	{"options", 0, 134218132, 0, &StringOptions_t1003_0_0_0},
	{"t", 1, 134218133, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218134, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218135, 0, &DOGetter_1_t1044_0_0_0},
	{"setter", 4, 134218136, 0, &DOSetter_1_t1045_0_0_0},
	{"elapsed", 5, 134218137, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218138, 0, &String_t_0_0_0},
	{"changeValue", 7, 134218139, 0, &String_t_0_0_0},
	{"duration", 8, 134218140, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218141, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218142, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_StringOptions_t1003_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.StringOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.Single,System.String,System.String,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo StringPlugin_EvaluateAndApply_m5462_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&StringPlugin_EvaluateAndApply_m5462/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_StringOptions_t1003_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Object_t_Object_t_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, StringPlugin_t990_StringPlugin_EvaluateAndApply_m5462_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_Append_m5463_ParameterInfos[] = 
{
	{"value", 0, 134218143, 0, &String_t_0_0_0},
	{"startIndex", 1, 134218144, 0, &Int32_t127_0_0_0},
	{"length", 2, 134218145, 0, &Int32_t127_0_0_0},
	{"richTextEnabled", 3, 134218146, 0, &Boolean_t169_0_0_0},
};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::Append(System.String,System.Int32,System.Int32,System.Boolean)
extern const MethodInfo StringPlugin_Append_m5463_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&StringPlugin_Append_m5463/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &StringBuilder_t423_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Int32_t127_SByte_t170/* invoker_method */
	, StringPlugin_t990_StringPlugin_Append_m5463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1003_0_0_0;
static const ParameterInfo StringPlugin_t990_StringPlugin_ScrambledCharsToUse_m5464_ParameterInfos[] = 
{
	{"options", 0, 134218147, 0, &StringOptions_t1003_0_0_0},
};
extern const Il2CppType CharU5BU5D_t110_0_0_0;
extern void* RuntimeInvoker_Object_t_StringOptions_t1003 (const MethodInfo* method, void* obj, void** args);
// System.Char[] DG.Tweening.Plugins.StringPlugin::ScrambledCharsToUse(DG.Tweening.Plugins.Options.StringOptions)
extern const MethodInfo StringPlugin_ScrambledCharsToUse_m5464_MethodInfo = 
{
	"ScrambledCharsToUse"/* name */
	, (methodPointerType)&StringPlugin_ScrambledCharsToUse_m5464/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &CharU5BU5D_t110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StringOptions_t1003/* invoker_method */
	, StringPlugin_t990_StringPlugin_ScrambledCharsToUse_m5464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::.ctor()
extern const MethodInfo StringPlugin__ctor_m5465_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringPlugin__ctor_m5465/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::.cctor()
extern const MethodInfo StringPlugin__cctor_m5466_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringPlugin__cctor_m5466/* method */
	, &StringPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringPlugin_t990_MethodInfos[] =
{
	&StringPlugin_Reset_m5457_MethodInfo,
	&StringPlugin_ConvertToStartValue_m5458_MethodInfo,
	&StringPlugin_SetRelativeEndValue_m5459_MethodInfo,
	&StringPlugin_SetChangeValue_m5460_MethodInfo,
	&StringPlugin_GetSpeedBasedDuration_m5461_MethodInfo,
	&StringPlugin_EvaluateAndApply_m5462_MethodInfo,
	&StringPlugin_Append_m5463_MethodInfo,
	&StringPlugin_ScrambledCharsToUse_m5464_MethodInfo,
	&StringPlugin__ctor_m5465_MethodInfo,
	&StringPlugin__cctor_m5466_MethodInfo,
	NULL
};
extern const MethodInfo StringPlugin_Reset_m5457_MethodInfo;
extern const MethodInfo StringPlugin_ConvertToStartValue_m5458_MethodInfo;
extern const MethodInfo StringPlugin_SetRelativeEndValue_m5459_MethodInfo;
extern const MethodInfo StringPlugin_SetChangeValue_m5460_MethodInfo;
extern const MethodInfo StringPlugin_GetSpeedBasedDuration_m5461_MethodInfo;
extern const MethodInfo StringPlugin_EvaluateAndApply_m5462_MethodInfo;
static const Il2CppMethodReference StringPlugin_t990_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&StringPlugin_Reset_m5457_MethodInfo,
	&StringPlugin_ConvertToStartValue_m5458_MethodInfo,
	&StringPlugin_SetRelativeEndValue_m5459_MethodInfo,
	&StringPlugin_SetChangeValue_m5460_MethodInfo,
	&StringPlugin_GetSpeedBasedDuration_m5461_MethodInfo,
	&StringPlugin_EvaluateAndApply_m5462_MethodInfo,
};
static bool StringPlugin_t990_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringPlugin_t990_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringPlugin_t990_0_0_0;
extern const Il2CppType StringPlugin_t990_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t991_0_0_0;
struct StringPlugin_t990;
const Il2CppTypeDefinitionMetadata StringPlugin_t990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringPlugin_t990_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t991_0_0_0/* parent */
	, StringPlugin_t990_VTable/* vtableMethods */
	, StringPlugin_t990_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 251/* fieldStart */

};
TypeInfo StringPlugin_t990_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, StringPlugin_t990_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringPlugin_t990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StringPlugin_t990_0_0_0/* byval_arg */
	, &StringPlugin_t990_1_0_0/* this_arg */
	, &StringPlugin_t990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringPlugin_t990)/* instance_size */
	, sizeof (StringPlugin_t990)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringPlugin_t990_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions.h"
// Metadata Definition DG.Tweening.Plugins.StringPluginExtensions
extern TypeInfo StringPluginExtensions_t992_il2cpp_TypeInfo;
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensionsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern const MethodInfo StringPluginExtensions__cctor_m5467_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringPluginExtensions__cctor_m5467/* method */
	, &StringPluginExtensions_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t110_0_0_0;
static const ParameterInfo StringPluginExtensions_t992_StringPluginExtensions_ScrambleChars_m5468_ParameterInfos[] = 
{
	{"chars", 0, 134218148, 0, &CharU5BU5D_t110_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern const MethodInfo StringPluginExtensions_ScrambleChars_m5468_MethodInfo = 
{
	"ScrambleChars"/* name */
	, (methodPointerType)&StringPluginExtensions_ScrambleChars_m5468/* method */
	, &StringPluginExtensions_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StringPluginExtensions_t992_StringPluginExtensions_ScrambleChars_m5468_ParameterInfos/* parameters */
	, 51/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType CharU5BU5D_t110_0_0_0;
static const ParameterInfo StringPluginExtensions_t992_StringPluginExtensions_AppendScrambledChars_m5469_ParameterInfos[] = 
{
	{"buffer", 0, 134218149, 0, &StringBuilder_t423_0_0_0},
	{"length", 1, 134218150, 0, &Int32_t127_0_0_0},
	{"chars", 2, 134218151, 0, &CharU5BU5D_t110_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern const MethodInfo StringPluginExtensions_AppendScrambledChars_m5469_MethodInfo = 
{
	"AppendScrambledChars"/* name */
	, (methodPointerType)&StringPluginExtensions_AppendScrambledChars_m5469/* method */
	, &StringPluginExtensions_t992_il2cpp_TypeInfo/* declaring_type */
	, &StringBuilder_t423_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127_Object_t/* invoker_method */
	, StringPluginExtensions_t992_StringPluginExtensions_AppendScrambledChars_m5469_ParameterInfos/* parameters */
	, 52/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringPluginExtensions_t992_MethodInfos[] =
{
	&StringPluginExtensions__cctor_m5467_MethodInfo,
	&StringPluginExtensions_ScrambleChars_m5468_MethodInfo,
	&StringPluginExtensions_AppendScrambledChars_m5469_MethodInfo,
	NULL
};
static const Il2CppMethodReference StringPluginExtensions_t992_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool StringPluginExtensions_t992_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringPluginExtensions_t992_0_0_0;
extern const Il2CppType StringPluginExtensions_t992_1_0_0;
struct StringPluginExtensions_t992;
const Il2CppTypeDefinitionMetadata StringPluginExtensions_t992_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringPluginExtensions_t992_VTable/* vtableMethods */
	, StringPluginExtensions_t992_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 253/* fieldStart */

};
TypeInfo StringPluginExtensions_t992_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringPluginExtensions"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, StringPluginExtensions_t992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringPluginExtensions_t992_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 50/* custom_attributes_cache */
	, &StringPluginExtensions_t992_0_0_0/* byval_arg */
	, &StringPluginExtensions_t992_1_0_0/* this_arg */
	, &StringPluginExtensions_t992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringPluginExtensions_t992)/* instance_size */
	, sizeof (StringPluginExtensions_t992)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringPluginExtensions_t992_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 384/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector4Plugin
extern TypeInfo Vector4Plugin_t993_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_Reset_m5470_ParameterInfos[] = 
{
	{"t", 0, 134218152, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_Reset_m5470_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector4Plugin_Reset_m5470/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_Reset_m5470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
extern const Il2CppType Vector4_t413_0_0_0;
extern const Il2CppType Vector4_t413_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_ConvertToStartValue_m5471_ParameterInfos[] = 
{
	{"t", 0, 134218153, 0, &TweenerCore_3_t1046_0_0_0},
	{"value", 1, 134218154, 0, &Vector4_t413_0_0_0},
};
extern void* RuntimeInvoker_Vector4_t413_Object_t_Vector4_t413 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector4 DG.Tweening.Plugins.Vector4Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector4)
extern const MethodInfo Vector4Plugin_ConvertToStartValue_m5471_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector4Plugin_ConvertToStartValue_m5471/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Vector4_t413_0_0_0/* return_type */
	, RuntimeInvoker_Vector4_t413_Object_t_Vector4_t413/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_ConvertToStartValue_m5471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_SetRelativeEndValue_m5472_ParameterInfos[] = 
{
	{"t", 0, 134218155, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_SetRelativeEndValue_m5472_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector4Plugin_SetRelativeEndValue_m5472/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_SetRelativeEndValue_m5472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_SetChangeValue_m5473_ParameterInfos[] = 
{
	{"t", 0, 134218156, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_SetChangeValue_m5473_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector4Plugin_SetChangeValue_m5473/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_SetChangeValue_m5473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector4_t413_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_GetSpeedBasedDuration_m5474_ParameterInfos[] = 
{
	{"options", 0, 134218157, 0, &VectorOptions_t1002_0_0_0},
	{"unitsXSecond", 1, 134218158, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218159, 0, &Vector4_t413_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector4_t413 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector4Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector4)
extern const MethodInfo Vector4Plugin_GetSpeedBasedDuration_m5474_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector4Plugin_GetSpeedBasedDuration_m5474/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_VectorOptions_t1002_Single_t151_Vector4_t413/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_GetSpeedBasedDuration_m5474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1002_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1047_0_0_0;
extern const Il2CppType DOGetter_1_t1047_0_0_0;
extern const Il2CppType DOSetter_1_t1048_0_0_0;
extern const Il2CppType DOSetter_1_t1048_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Vector4_t413_0_0_0;
extern const Il2CppType Vector4_t413_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo Vector4Plugin_t993_Vector4Plugin_EvaluateAndApply_m5475_ParameterInfos[] = 
{
	{"options", 0, 134218160, 0, &VectorOptions_t1002_0_0_0},
	{"t", 1, 134218161, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218162, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218163, 0, &DOGetter_1_t1047_0_0_0},
	{"setter", 4, 134218164, 0, &DOSetter_1_t1048_0_0_0},
	{"elapsed", 5, 134218165, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218166, 0, &Vector4_t413_0_0_0},
	{"changeValue", 7, 134218167, 0, &Vector4_t413_0_0_0},
	{"duration", 8, 134218168, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218169, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218170, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector4_t413_Vector4_t413_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,System.Single,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector4Plugin_EvaluateAndApply_m5475_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector4Plugin_EvaluateAndApply_m5475/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_VectorOptions_t1002_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Vector4_t413_Vector4_t413_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, Vector4Plugin_t993_Vector4Plugin_EvaluateAndApply_m5475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::.ctor()
extern const MethodInfo Vector4Plugin__ctor_m5476_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector4Plugin__ctor_m5476/* method */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector4Plugin_t993_MethodInfos[] =
{
	&Vector4Plugin_Reset_m5470_MethodInfo,
	&Vector4Plugin_ConvertToStartValue_m5471_MethodInfo,
	&Vector4Plugin_SetRelativeEndValue_m5472_MethodInfo,
	&Vector4Plugin_SetChangeValue_m5473_MethodInfo,
	&Vector4Plugin_GetSpeedBasedDuration_m5474_MethodInfo,
	&Vector4Plugin_EvaluateAndApply_m5475_MethodInfo,
	&Vector4Plugin__ctor_m5476_MethodInfo,
	NULL
};
extern const MethodInfo Vector4Plugin_Reset_m5470_MethodInfo;
extern const MethodInfo Vector4Plugin_ConvertToStartValue_m5471_MethodInfo;
extern const MethodInfo Vector4Plugin_SetRelativeEndValue_m5472_MethodInfo;
extern const MethodInfo Vector4Plugin_SetChangeValue_m5473_MethodInfo;
extern const MethodInfo Vector4Plugin_GetSpeedBasedDuration_m5474_MethodInfo;
extern const MethodInfo Vector4Plugin_EvaluateAndApply_m5475_MethodInfo;
static const Il2CppMethodReference Vector4Plugin_t993_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Vector4Plugin_Reset_m5470_MethodInfo,
	&Vector4Plugin_ConvertToStartValue_m5471_MethodInfo,
	&Vector4Plugin_SetRelativeEndValue_m5472_MethodInfo,
	&Vector4Plugin_SetChangeValue_m5473_MethodInfo,
	&Vector4Plugin_GetSpeedBasedDuration_m5474_MethodInfo,
	&Vector4Plugin_EvaluateAndApply_m5475_MethodInfo,
};
static bool Vector4Plugin_t993_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector4Plugin_t993_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector4Plugin_t993_0_0_0;
extern const Il2CppType Vector4Plugin_t993_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t994_0_0_0;
struct Vector4Plugin_t993;
const Il2CppTypeDefinitionMetadata Vector4Plugin_t993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector4Plugin_t993_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t994_0_0_0/* parent */
	, Vector4Plugin_t993_VTable/* vtableMethods */
	, Vector4Plugin_t993_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector4Plugin_t993_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector4Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector4Plugin_t993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector4Plugin_t993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector4Plugin_t993_0_0_0/* byval_arg */
	, &Vector4Plugin_t993_1_0_0/* this_arg */
	, &Vector4Plugin_t993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector4Plugin_t993)/* instance_size */
	, sizeof (Vector4Plugin_t993)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// Metadata Definition DG.Tweening.LoopType
extern TypeInfo LoopType_t995_il2cpp_TypeInfo;
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopTypeMethodDeclarations.h"
static const MethodInfo* LoopType_t995_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LoopType_t995_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool LoopType_t995_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LoopType_t995_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LoopType_t995_1_0_0;
const Il2CppTypeDefinitionMetadata LoopType_t995_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoopType_t995_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, LoopType_t995_VTable/* vtableMethods */
	, LoopType_t995_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 258/* fieldStart */

};
TypeInfo LoopType_t995_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoopType"/* name */
	, "DG.Tweening"/* namespaze */
	, LoopType_t995_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoopType_t995_0_0_0/* byval_arg */
	, &LoopType_t995_1_0_0/* this_arg */
	, &LoopType_t995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoopType_t995)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoopType_t995)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// Metadata Definition DG.Tweening.Tweener
extern TypeInfo Tweener_t99_il2cpp_TypeInfo;
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_TweenerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tweener::.ctor()
extern const MethodInfo Tweener__ctor_m5477_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Tweener__ctor_m5477/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1100_0_0_0;
extern const Il2CppType TweenerCore_3_t1100_0_0_0;
extern const Il2CppType DOGetter_1_t1101_0_0_0;
extern const Il2CppType DOGetter_1_t1101_0_0_0;
extern const Il2CppType DOSetter_1_t1102_0_0_0;
extern const Il2CppType DOSetter_1_t1102_0_0_0;
extern const Il2CppType Tweener_Setup_m5614_gp_1_0_0_0;
extern const Il2CppType Tweener_Setup_m5614_gp_1_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1104_0_0_4112;
extern const Il2CppType ABSTweenPlugin_3_t1104_0_0_0;
static const ParameterInfo Tweener_t99_Tweener_Setup_m5614_ParameterInfos[] = 
{
	{"t", 0, 134218171, 0, &TweenerCore_3_t1100_0_0_0},
	{"getter", 1, 134218172, 0, &DOGetter_1_t1101_0_0_0},
	{"setter", 2, 134218173, 0, &DOSetter_1_t1102_0_0_0},
	{"endValue", 3, 134218174, 0, &Tweener_Setup_m5614_gp_1_0_0_0},
	{"duration", 4, 134218175, 0, &Single_t151_0_0_0},
	{"plugin", 5, 134218176, 0, &ABSTweenPlugin_3_t1104_0_0_4112},
};
extern const Il2CppGenericContainer Tweener_Setup_m5614_Il2CppGenericContainer;
extern TypeInfo Tweener_Setup_m5614_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_Setup_m5614_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5614_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_Setup_m5614_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_Setup_m5614_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5614_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_Setup_m5614_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_Setup_m5614_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_Setup_m5614_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5614_Il2CppGenericContainer, Tweener_Setup_m5614_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_Setup_m5614_Il2CppGenericParametersArray[3] = 
{
	&Tweener_Setup_m5614_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_Setup_m5614_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_Setup_m5614_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_Setup_m5614_MethodInfo;
extern const Il2CppGenericContainer Tweener_Setup_m5614_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_Setup_m5614_MethodInfo, 3, 1, Tweener_Setup_m5614_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod PluginsManager_GetDefaultPlugin_TisT1_t1105_TisT2_t1103_TisTPlugOptions_t1106_m5628_GenericMethod;
static Il2CppRGCTXDefinition Tweener_Setup_m5614_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &PluginsManager_GetDefaultPlugin_TisT1_t1105_TisT2_t1103_TisTPlugOptions_t1106_m5628_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean DG.Tweening.Tweener::Setup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_Setup_m5614_MethodInfo = 
{
	"Setup"/* name */
	, NULL/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t99_Tweener_Setup_m5614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 258/* token */
	, Tweener_Setup_m5614_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_Setup_m5614_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1107_0_0_0;
extern const Il2CppType TweenerCore_3_t1107_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Tweener_t99_Tweener_DoUpdateDelay_m5615_ParameterInfos[] = 
{
	{"t", 0, 134218177, 0, &TweenerCore_3_t1107_0_0_0},
	{"elapsed", 1, 134218178, 0, &Single_t151_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer;
extern TypeInfo Tweener_DoUpdateDelay_m5615_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5615_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DoUpdateDelay_m5615_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5615_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DoUpdateDelay_m5615_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DoUpdateDelay_m5615_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5615_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer, Tweener_DoUpdateDelay_m5615_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DoUpdateDelay_m5615_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DoUpdateDelay_m5615_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoUpdateDelay_m5615_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoUpdateDelay_m5615_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DoUpdateDelay_m5615_MethodInfo;
extern const Il2CppGenericContainer Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DoUpdateDelay_m5615_MethodInfo, 3, 1, Tweener_DoUpdateDelay_m5615_Il2CppGenericParametersArray };
// System.Single DG.Tweening.Tweener::DoUpdateDelay(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern const MethodInfo Tweener_DoUpdateDelay_m5615_MethodInfo = 
{
	"DoUpdateDelay"/* name */
	, NULL/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t99_Tweener_DoUpdateDelay_m5615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DoUpdateDelay_m5615_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1111_0_0_0;
extern const Il2CppType TweenerCore_3_t1111_0_0_0;
static const ParameterInfo Tweener_t99_Tweener_DoStartup_m5616_ParameterInfos[] = 
{
	{"t", 0, 134218179, 0, &TweenerCore_3_t1111_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DoStartup_m5616_Il2CppGenericContainer;
extern TypeInfo Tweener_DoStartup_m5616_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoStartup_m5616_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5616_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DoStartup_m5616_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoStartup_m5616_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5616_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DoStartup_m5616_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DoStartup_m5616_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DoStartup_m5616_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5616_Il2CppGenericContainer, Tweener_DoStartup_m5616_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DoStartup_m5616_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DoStartup_m5616_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoStartup_m5616_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoStartup_m5616_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DoStartup_m5616_MethodInfo;
extern const Il2CppGenericContainer Tweener_DoStartup_m5616_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DoStartup_m5616_MethodInfo, 3, 1, Tweener_DoStartup_m5616_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Tweener_DOStartupSpecials_TisT1_t1112_TisT2_t1113_TisTPlugOptions_t1114_m5629_GenericMethod;
extern const Il2CppGenericMethod DOGetter_1_Invoke_m5630_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_ConvertToStartValue_m5631_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_SetRelativeEndValue_m5632_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_SetChangeValue_m5633_GenericMethod;
extern const Il2CppGenericMethod Tweener_DOStartupDurationBased_TisT1_t1112_TisT2_t1113_TisTPlugOptions_t1114_m5634_GenericMethod;
static Il2CppRGCTXDefinition Tweener_DoStartup_m5616_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DOStartupSpecials_TisT1_t1112_TisT2_t1113_TisTPlugOptions_t1114_m5629_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &DOGetter_1_Invoke_m5630_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_ConvertToStartValue_m5631_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_SetRelativeEndValue_m5632_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_SetChangeValue_m5633_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DOStartupDurationBased_TisT1_t1112_TisT2_t1113_TisTPlugOptions_t1114_m5634_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean DG.Tweening.Tweener::DoStartup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DoStartup_m5616_MethodInfo = 
{
	"DoStartup"/* name */
	, NULL/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t99_Tweener_DoStartup_m5616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 260/* token */
	, Tweener_DoStartup_m5616_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DoStartup_m5616_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1115_0_0_0;
extern const Il2CppType TweenerCore_3_t1115_0_0_0;
static const ParameterInfo Tweener_t99_Tweener_DOStartupSpecials_m5617_ParameterInfos[] = 
{
	{"t", 0, 134218180, 0, &TweenerCore_3_t1115_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer;
extern TypeInfo Tweener_DOStartupSpecials_m5617_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5617_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DOStartupSpecials_m5617_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5617_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DOStartupSpecials_m5617_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DOStartupSpecials_m5617_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5617_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer, Tweener_DOStartupSpecials_m5617_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DOStartupSpecials_m5617_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DOStartupSpecials_m5617_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupSpecials_m5617_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupSpecials_m5617_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DOStartupSpecials_m5617_MethodInfo;
extern const Il2CppGenericContainer Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DOStartupSpecials_m5617_MethodInfo, 3, 1, Tweener_DOStartupSpecials_m5617_Il2CppGenericParametersArray };
// System.Boolean DG.Tweening.Tweener::DOStartupSpecials(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DOStartupSpecials_m5617_MethodInfo = 
{
	"DOStartupSpecials"/* name */
	, NULL/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t99_Tweener_DOStartupSpecials_m5617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DOStartupSpecials_m5617_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1119_0_0_0;
extern const Il2CppType TweenerCore_3_t1119_0_0_0;
static const ParameterInfo Tweener_t99_Tweener_DOStartupDurationBased_m5618_ParameterInfos[] = 
{
	{"t", 0, 134218181, 0, &TweenerCore_3_t1119_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer;
extern TypeInfo Tweener_DOStartupDurationBased_m5618_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5618_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DOStartupDurationBased_m5618_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5618_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DOStartupDurationBased_m5618_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DOStartupDurationBased_m5618_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5618_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer, Tweener_DOStartupDurationBased_m5618_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DOStartupDurationBased_m5618_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DOStartupDurationBased_m5618_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupDurationBased_m5618_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupDurationBased_m5618_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DOStartupDurationBased_m5618_MethodInfo;
extern const Il2CppGenericContainer Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DOStartupDurationBased_m5618_MethodInfo, 3, 1, Tweener_DOStartupDurationBased_m5618_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod ABSTweenPlugin_3_GetSpeedBasedDuration_m5635_GenericMethod;
static Il2CppRGCTXDefinition Tweener_DOStartupDurationBased_m5618_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_GetSpeedBasedDuration_m5635_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void DG.Tweening.Tweener::DOStartupDurationBased(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DOStartupDurationBased_m5618_MethodInfo = 
{
	"DOStartupDurationBased"/* name */
	, NULL/* method */
	, &Tweener_t99_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t99_Tweener_DOStartupDurationBased_m5618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 262/* token */
	, Tweener_DOStartupDurationBased_m5618_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DOStartupDurationBased_m5618_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Tweener_t99_MethodInfos[] =
{
	&Tweener__ctor_m5477_MethodInfo,
	&Tweener_Setup_m5614_MethodInfo,
	&Tweener_DoUpdateDelay_m5615_MethodInfo,
	&Tweener_DoStartup_m5616_MethodInfo,
	&Tweener_DOStartupSpecials_m5617_MethodInfo,
	&Tweener_DOStartupDurationBased_m5618_MethodInfo,
	NULL
};
static const Il2CppMethodReference Tweener_t99_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Tween_Reset_m5333_MethodInfo,
	NULL,
	&Tween_UpdateDelay_m5334_MethodInfo,
	NULL,
	NULL,
};
static bool Tweener_t99_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Tweener_t99_1_0_0;
struct Tweener_t99;
const Il2CppTypeDefinitionMetadata Tweener_t99_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tween_t934_0_0_0/* parent */
	, Tweener_t99_VTable/* vtableMethods */
	, Tweener_t99_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 262/* fieldStart */

};
TypeInfo Tweener_t99_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tweener"/* name */
	, "DG.Tweening"/* namespaze */
	, Tweener_t99_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tweener_t99_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Tweener_t99_0_0_0/* byval_arg */
	, &Tweener_t99_1_0_0/* this_arg */
	, &Tweener_t99_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tweener_t99)/* instance_size */
	, sizeof (Tweener_t99)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.FloatOptions
extern TypeInfo FloatOptions_t996_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptionsMethodDeclarations.h"
static const MethodInfo* FloatOptions_t996_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FloatOptions_t996_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool FloatOptions_t996_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType FloatOptions_t996_0_0_0;
extern const Il2CppType FloatOptions_t996_1_0_0;
const Il2CppTypeDefinitionMetadata FloatOptions_t996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, FloatOptions_t996_VTable/* vtableMethods */
	, FloatOptions_t996_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 264/* fieldStart */

};
TypeInfo FloatOptions_t996_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "FloatOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, FloatOptions_t996_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FloatOptions_t996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FloatOptions_t996_0_0_0/* byval_arg */
	, &FloatOptions_t996_1_0_0/* this_arg */
	, &FloatOptions_t996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)FloatOptions_t996_marshal/* marshal_to_native_func */
	, (methodPointerType)FloatOptions_t996_marshal_back/* marshal_from_native_func */
	, (methodPointerType)FloatOptions_t996_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (FloatOptions_t996)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FloatOptions_t996)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(FloatOptions_t996_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPlugin.h"
// Metadata Definition DG.Tweening.Plugins.ColorPlugin
extern TypeInfo ColorPlugin_t997_il2cpp_TypeInfo;
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_Reset_m5478_ParameterInfos[] = 
{
	{"t", 0, 134218182, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_Reset_m5478_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&ColorPlugin_Reset_m5478/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_Reset_m5478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_ConvertToStartValue_m5479_ParameterInfos[] = 
{
	{"t", 0, 134218183, 0, &TweenerCore_3_t1049_0_0_0},
	{"value", 1, 134218184, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Color_t90_Object_t_Color_t90 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color DG.Tweening.Plugins.ColorPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,UnityEngine.Color)
extern const MethodInfo ColorPlugin_ConvertToStartValue_m5479_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&ColorPlugin_ConvertToStartValue_m5479/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Color_t90_0_0_0/* return_type */
	, RuntimeInvoker_Color_t90_Object_t_Color_t90/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_ConvertToStartValue_m5479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_SetRelativeEndValue_m5480_ParameterInfos[] = 
{
	{"t", 0, 134218185, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_SetRelativeEndValue_m5480_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&ColorPlugin_SetRelativeEndValue_m5480/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_SetRelativeEndValue_m5480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_SetChangeValue_m5481_ParameterInfos[] = 
{
	{"t", 0, 134218186, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_SetChangeValue_m5481_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&ColorPlugin_SetChangeValue_m5481/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_SetChangeValue_m5481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1011_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_GetSpeedBasedDuration_m5482_ParameterInfos[] = 
{
	{"options", 0, 134218187, 0, &ColorOptions_t1011_0_0_0},
	{"unitsXSecond", 1, 134218188, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218189, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_ColorOptions_t1011_Single_t151_Color_t90 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.ColorPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,UnityEngine.Color)
extern const MethodInfo ColorPlugin_GetSpeedBasedDuration_m5482_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&ColorPlugin_GetSpeedBasedDuration_m5482/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_ColorOptions_t1011_Single_t151_Color_t90/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_GetSpeedBasedDuration_m5482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1011_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1050_0_0_0;
extern const Il2CppType DOGetter_1_t1050_0_0_0;
extern const Il2CppType DOSetter_1_t1051_0_0_0;
extern const Il2CppType DOSetter_1_t1051_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo ColorPlugin_t997_ColorPlugin_EvaluateAndApply_m5483_ParameterInfos[] = 
{
	{"options", 0, 134218190, 0, &ColorOptions_t1011_0_0_0},
	{"t", 1, 134218191, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218192, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218193, 0, &DOGetter_1_t1050_0_0_0},
	{"setter", 4, 134218194, 0, &DOSetter_1_t1051_0_0_0},
	{"elapsed", 5, 134218195, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218196, 0, &Color_t90_0_0_0},
	{"changeValue", 7, 134218197, 0, &Color_t90_0_0_0},
	{"duration", 8, 134218198, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218199, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218200, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_ColorOptions_t1011_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Color_t90_Color_t90_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,UnityEngine.Color,UnityEngine.Color,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo ColorPlugin_EvaluateAndApply_m5483_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&ColorPlugin_EvaluateAndApply_m5483/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_ColorOptions_t1011_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Color_t90_Color_t90_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, ColorPlugin_t997_ColorPlugin_EvaluateAndApply_m5483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::.ctor()
extern const MethodInfo ColorPlugin__ctor_m5484_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ColorPlugin__ctor_m5484/* method */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ColorPlugin_t997_MethodInfos[] =
{
	&ColorPlugin_Reset_m5478_MethodInfo,
	&ColorPlugin_ConvertToStartValue_m5479_MethodInfo,
	&ColorPlugin_SetRelativeEndValue_m5480_MethodInfo,
	&ColorPlugin_SetChangeValue_m5481_MethodInfo,
	&ColorPlugin_GetSpeedBasedDuration_m5482_MethodInfo,
	&ColorPlugin_EvaluateAndApply_m5483_MethodInfo,
	&ColorPlugin__ctor_m5484_MethodInfo,
	NULL
};
extern const MethodInfo ColorPlugin_Reset_m5478_MethodInfo;
extern const MethodInfo ColorPlugin_ConvertToStartValue_m5479_MethodInfo;
extern const MethodInfo ColorPlugin_SetRelativeEndValue_m5480_MethodInfo;
extern const MethodInfo ColorPlugin_SetChangeValue_m5481_MethodInfo;
extern const MethodInfo ColorPlugin_GetSpeedBasedDuration_m5482_MethodInfo;
extern const MethodInfo ColorPlugin_EvaluateAndApply_m5483_MethodInfo;
static const Il2CppMethodReference ColorPlugin_t997_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ColorPlugin_Reset_m5478_MethodInfo,
	&ColorPlugin_ConvertToStartValue_m5479_MethodInfo,
	&ColorPlugin_SetRelativeEndValue_m5480_MethodInfo,
	&ColorPlugin_SetChangeValue_m5481_MethodInfo,
	&ColorPlugin_GetSpeedBasedDuration_m5482_MethodInfo,
	&ColorPlugin_EvaluateAndApply_m5483_MethodInfo,
};
static bool ColorPlugin_t997_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ColorPlugin_t997_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ColorPlugin_t997_0_0_0;
extern const Il2CppType ColorPlugin_t997_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t998_0_0_0;
struct ColorPlugin_t997;
const Il2CppTypeDefinitionMetadata ColorPlugin_t997_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorPlugin_t997_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t998_0_0_0/* parent */
	, ColorPlugin_t997_VTable/* vtableMethods */
	, ColorPlugin_t997_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ColorPlugin_t997_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, ColorPlugin_t997_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorPlugin_t997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorPlugin_t997_0_0_0/* byval_arg */
	, &ColorPlugin_t997_1_0_0/* this_arg */
	, &ColorPlugin_t997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorPlugin_t997)/* instance_size */
	, sizeof (ColorPlugin_t997)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_Bounce.h"
// Metadata Definition DG.Tweening.Core.Easing.Bounce
extern TypeInfo Bounce_t999_il2cpp_TypeInfo;
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_BounceMethodDeclarations.h"
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Bounce_t999_Bounce_EaseIn_m5485_ParameterInfos[] = 
{
	{"time", 0, 134218201, 0, &Single_t151_0_0_0},
	{"duration", 1, 134218202, 0, &Single_t151_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218203, 0, &Single_t151_0_0_0},
	{"unusedPeriod", 3, 134218204, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseIn_m5485_MethodInfo = 
{
	"EaseIn"/* name */
	, (methodPointerType)&Bounce_EaseIn_m5485/* method */
	, &Bounce_t999_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, Bounce_t999_Bounce_EaseIn_m5485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Bounce_t999_Bounce_EaseOut_m5486_ParameterInfos[] = 
{
	{"time", 0, 134218205, 0, &Single_t151_0_0_0},
	{"duration", 1, 134218206, 0, &Single_t151_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218207, 0, &Single_t151_0_0_0},
	{"unusedPeriod", 3, 134218208, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseOut_m5486_MethodInfo = 
{
	"EaseOut"/* name */
	, (methodPointerType)&Bounce_EaseOut_m5486/* method */
	, &Bounce_t999_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, Bounce_t999_Bounce_EaseOut_m5486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Bounce_t999_Bounce_EaseInOut_m5487_ParameterInfos[] = 
{
	{"time", 0, 134218209, 0, &Single_t151_0_0_0},
	{"duration", 1, 134218210, 0, &Single_t151_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218211, 0, &Single_t151_0_0_0},
	{"unusedPeriod", 3, 134218212, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseInOut_m5487_MethodInfo = 
{
	"EaseInOut"/* name */
	, (methodPointerType)&Bounce_EaseInOut_m5487/* method */
	, &Bounce_t999_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, Bounce_t999_Bounce_EaseInOut_m5487_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Bounce_t999_MethodInfos[] =
{
	&Bounce_EaseIn_m5485_MethodInfo,
	&Bounce_EaseOut_m5486_MethodInfo,
	&Bounce_EaseInOut_m5487_MethodInfo,
	NULL
};
static const Il2CppMethodReference Bounce_t999_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Bounce_t999_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Bounce_t999_0_0_0;
extern const Il2CppType Bounce_t999_1_0_0;
struct Bounce_t999;
const Il2CppTypeDefinitionMetadata Bounce_t999_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Bounce_t999_VTable/* vtableMethods */
	, Bounce_t999_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Bounce_t999_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Bounce"/* name */
	, "DG.Tweening.Core.Easing"/* namespaze */
	, Bounce_t999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Bounce_t999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Bounce_t999_0_0_0/* byval_arg */
	, &Bounce_t999_1_0_0/* this_arg */
	, &Bounce_t999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Bounce_t999)/* instance_size */
	, sizeof (Bounce_t999)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// Metadata Definition DG.Tweening.Color2
extern TypeInfo Color2_t1000_il2cpp_TypeInfo;
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2MethodDeclarations.h"
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo Color2_t1000_Color2__ctor_m5488_ParameterInfos[] = 
{
	{"ca", 0, 134218213, 0, &Color_t90_0_0_0},
	{"cb", 1, 134218214, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Color_t90_Color_t90 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo Color2__ctor_m5488_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Color2__ctor_m5488/* method */
	, &Color2_t1000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Color_t90_Color_t90/* invoker_method */
	, Color2_t1000_Color2__ctor_m5488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
static const ParameterInfo Color2_t1000_Color2_op_Addition_m5489_ParameterInfos[] = 
{
	{"c1", 0, 134218215, 0, &Color2_t1000_0_0_0},
	{"c2", 1, 134218216, 0, &Color2_t1000_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1000_Color2_t1000_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern const MethodInfo Color2_op_Addition_m5489_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&Color2_op_Addition_m5489/* method */
	, &Color2_t1000_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1000_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1000_Color2_t1000_Color2_t1000/* invoker_method */
	, Color2_t1000_Color2_op_Addition_m5489_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Color2_t1000_0_0_0;
static const ParameterInfo Color2_t1000_Color2_op_Subtraction_m5490_ParameterInfos[] = 
{
	{"c1", 0, 134218217, 0, &Color2_t1000_0_0_0},
	{"c2", 1, 134218218, 0, &Color2_t1000_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1000_Color2_t1000_Color2_t1000 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern const MethodInfo Color2_op_Subtraction_m5490_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&Color2_op_Subtraction_m5490/* method */
	, &Color2_t1000_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1000_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1000_Color2_t1000_Color2_t1000/* invoker_method */
	, Color2_t1000_Color2_op_Subtraction_m5490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1000_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Color2_t1000_Color2_op_Multiply_m5491_ParameterInfos[] = 
{
	{"c1", 0, 134218219, 0, &Color2_t1000_0_0_0},
	{"f", 1, 134218220, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1000_Color2_t1000_Single_t151 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern const MethodInfo Color2_op_Multiply_m5491_MethodInfo = 
{
	"op_Multiply"/* name */
	, (methodPointerType)&Color2_op_Multiply_m5491/* method */
	, &Color2_t1000_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1000_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1000_Color2_t1000_Single_t151/* invoker_method */
	, Color2_t1000_Color2_op_Multiply_m5491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Color2_t1000_MethodInfos[] =
{
	&Color2__ctor_m5488_MethodInfo,
	&Color2_op_Addition_m5489_MethodInfo,
	&Color2_op_Subtraction_m5490_MethodInfo,
	&Color2_op_Multiply_m5491_MethodInfo,
	NULL
};
static const Il2CppMethodReference Color2_t1000_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Color2_t1000_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Color2_t1000_1_0_0;
const Il2CppTypeDefinitionMetadata Color2_t1000_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Color2_t1000_VTable/* vtableMethods */
	, Color2_t1000_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 265/* fieldStart */

};
TypeInfo Color2_t1000_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color2"/* name */
	, "DG.Tweening"/* namespaze */
	, Color2_t1000_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Color2_t1000_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Color2_t1000_0_0_0/* byval_arg */
	, &Color2_t1000_1_0_0/* this_arg */
	, &Color2_t1000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color2_t1000)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color2_t1000)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color2_t1000 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// Metadata Definition DG.Tweening.AxisConstraint
extern TypeInfo AxisConstraint_t1001_il2cpp_TypeInfo;
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraintMethodDeclarations.h"
static const MethodInfo* AxisConstraint_t1001_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AxisConstraint_t1001_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool AxisConstraint_t1001_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AxisConstraint_t1001_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType AxisConstraint_t1001_1_0_0;
const Il2CppTypeDefinitionMetadata AxisConstraint_t1001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AxisConstraint_t1001_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, AxisConstraint_t1001_VTable/* vtableMethods */
	, AxisConstraint_t1001_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 267/* fieldStart */

};
TypeInfo AxisConstraint_t1001_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisConstraint"/* name */
	, "DG.Tweening"/* namespaze */
	, AxisConstraint_t1001_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 53/* custom_attributes_cache */
	, &AxisConstraint_t1001_0_0_0/* byval_arg */
	, &AxisConstraint_t1001_1_0_0/* this_arg */
	, &AxisConstraint_t1001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisConstraint_t1001)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AxisConstraint_t1001)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.VectorOptions
extern TypeInfo VectorOptions_t1002_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptionsMethodDeclarations.h"
static const MethodInfo* VectorOptions_t1002_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference VectorOptions_t1002_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool VectorOptions_t1002_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType VectorOptions_t1002_1_0_0;
const Il2CppTypeDefinitionMetadata VectorOptions_t1002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, VectorOptions_t1002_VTable/* vtableMethods */
	, VectorOptions_t1002_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 273/* fieldStart */

};
TypeInfo VectorOptions_t1002_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "VectorOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, VectorOptions_t1002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VectorOptions_t1002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VectorOptions_t1002_0_0_0/* byval_arg */
	, &VectorOptions_t1002_1_0_0/* this_arg */
	, &VectorOptions_t1002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)VectorOptions_t1002_marshal/* marshal_to_native_func */
	, (methodPointerType)VectorOptions_t1002_marshal_back/* marshal_from_native_func */
	, (methodPointerType)VectorOptions_t1002_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (VectorOptions_t1002)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (VectorOptions_t1002)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(VectorOptions_t1002_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.StringOptions
extern TypeInfo StringOptions_t1003_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptionsMethodDeclarations.h"
static const MethodInfo* StringOptions_t1003_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringOptions_t1003_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool StringOptions_t1003_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringOptions_t1003_1_0_0;
const Il2CppTypeDefinitionMetadata StringOptions_t1003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, StringOptions_t1003_VTable/* vtableMethods */
	, StringOptions_t1003_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 275/* fieldStart */

};
TypeInfo StringOptions_t1003_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, StringOptions_t1003_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringOptions_t1003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StringOptions_t1003_0_0_0/* byval_arg */
	, &StringOptions_t1003_1_0_0/* this_arg */
	, &StringOptions_t1003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)StringOptions_t1003_marshal/* marshal_to_native_func */
	, (methodPointerType)StringOptions_t1003_marshal_back/* marshal_from_native_func */
	, (methodPointerType)StringOptions_t1003_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (StringOptions_t1003)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringOptions_t1003)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(StringOptions_t1003_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.RectOptions
extern TypeInfo RectOptions_t1004_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptionsMethodDeclarations.h"
static const MethodInfo* RectOptions_t1004_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RectOptions_t1004_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool RectOptions_t1004_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectOptions_t1004_1_0_0;
const Il2CppTypeDefinitionMetadata RectOptions_t1004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, RectOptions_t1004_VTable/* vtableMethods */
	, RectOptions_t1004_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 280/* fieldStart */

};
TypeInfo RectOptions_t1004_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, RectOptions_t1004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectOptions_t1004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOptions_t1004_0_0_0/* byval_arg */
	, &RectOptions_t1004_1_0_0/* this_arg */
	, &RectOptions_t1004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)RectOptions_t1004_marshal/* marshal_to_native_func */
	, (methodPointerType)RectOptions_t1004_marshal_back/* marshal_from_native_func */
	, (methodPointerType)RectOptions_t1004_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (RectOptions_t1004)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RectOptions_t1004)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RectOptions_t1004_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleMode.h"
// Metadata Definition DG.Tweening.ScrambleMode
extern TypeInfo ScrambleMode_t1005_il2cpp_TypeInfo;
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleModeMethodDeclarations.h"
static const MethodInfo* ScrambleMode_t1005_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScrambleMode_t1005_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ScrambleMode_t1005_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScrambleMode_t1005_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ScrambleMode_t1005_0_0_0;
extern const Il2CppType ScrambleMode_t1005_1_0_0;
const Il2CppTypeDefinitionMetadata ScrambleMode_t1005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrambleMode_t1005_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ScrambleMode_t1005_VTable/* vtableMethods */
	, ScrambleMode_t1005_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 281/* fieldStart */

};
TypeInfo ScrambleMode_t1005_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrambleMode"/* name */
	, "DG.Tweening"/* namespaze */
	, ScrambleMode_t1005_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrambleMode_t1005_0_0_0/* byval_arg */
	, &ScrambleMode_t1005_1_0_0/* this_arg */
	, &ScrambleMode_t1005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrambleMode_t1005)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScrambleMode_t1005)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManager.h"
// Metadata Definition DG.Tweening.Core.Easing.EaseManager
extern TypeInfo EaseManager_t1006_il2cpp_TypeInfo;
// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManagerMethodDeclarations.h"
extern const Il2CppType Ease_t952_0_0_0;
extern const Il2CppType EaseFunction_t945_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo EaseManager_t1006_EaseManager_Evaluate_m5492_ParameterInfos[] = 
{
	{"easeType", 0, 134218221, 0, &Ease_t952_0_0_0},
	{"customEase", 1, 134218222, 0, &EaseFunction_t945_0_0_0},
	{"time", 2, 134218223, 0, &Single_t151_0_0_0},
	{"duration", 3, 134218224, 0, &Single_t151_0_0_0},
	{"overshootOrAmplitude", 4, 134218225, 0, &Single_t151_0_0_0},
	{"period", 5, 134218226, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Int32_t127_Object_t_Single_t151_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Ease,DG.Tweening.EaseFunction,System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo EaseManager_Evaluate_m5492_MethodInfo = 
{
	"Evaluate"/* name */
	, (methodPointerType)&EaseManager_Evaluate_m5492/* method */
	, &EaseManager_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Int32_t127_Object_t_Single_t151_Single_t151_Single_t151_Single_t151/* invoker_method */
	, EaseManager_t1006_EaseManager_Evaluate_m5492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EaseManager_t1006_MethodInfos[] =
{
	&EaseManager_Evaluate_m5492_MethodInfo,
	NULL
};
static const Il2CppMethodReference EaseManager_t1006_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool EaseManager_t1006_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType EaseManager_t1006_0_0_0;
extern const Il2CppType EaseManager_t1006_1_0_0;
struct EaseManager_t1006;
const Il2CppTypeDefinitionMetadata EaseManager_t1006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EaseManager_t1006_VTable/* vtableMethods */
	, EaseManager_t1006_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EaseManager_t1006_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "EaseManager"/* name */
	, "DG.Tweening.Core.Easing"/* namespaze */
	, EaseManager_t1006_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EaseManager_t1006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EaseManager_t1006_0_0_0/* byval_arg */
	, &EaseManager_t1006_1_0_0/* this_arg */
	, &EaseManager_t1006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EaseManager_t1006)/* instance_size */
	, sizeof (EaseManager_t1006)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPlugin.h"
// Metadata Definition DG.Tweening.Plugins.FloatPlugin
extern TypeInfo FloatPlugin_t1007_il2cpp_TypeInfo;
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_Reset_m5493_ParameterInfos[] = 
{
	{"t", 0, 134218227, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_Reset_m5493_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&FloatPlugin_Reset_m5493/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_Reset_m5493_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_ConvertToStartValue_m5494_ParameterInfos[] = 
{
	{"t", 0, 134218228, 0, &TweenerCore_3_t1052_0_0_0},
	{"value", 1, 134218229, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.FloatPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Single)
extern const MethodInfo FloatPlugin_ConvertToStartValue_m5494_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&FloatPlugin_ConvertToStartValue_m5494/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Single_t151/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_ConvertToStartValue_m5494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_SetRelativeEndValue_m5495_ParameterInfos[] = 
{
	{"t", 0, 134218230, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_SetRelativeEndValue_m5495_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&FloatPlugin_SetRelativeEndValue_m5495/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_SetRelativeEndValue_m5495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_SetChangeValue_m5496_ParameterInfos[] = 
{
	{"t", 0, 134218231, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_SetChangeValue_m5496_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&FloatPlugin_SetChangeValue_m5496/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_SetChangeValue_m5496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FloatOptions_t996_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_GetSpeedBasedDuration_m5497_ParameterInfos[] = 
{
	{"options", 0, 134218232, 0, &FloatOptions_t996_0_0_0},
	{"unitsXSecond", 1, 134218233, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218234, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_FloatOptions_t996_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.FloatPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.FloatOptions,System.Single,System.Single)
extern const MethodInfo FloatPlugin_GetSpeedBasedDuration_m5497_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&FloatPlugin_GetSpeedBasedDuration_m5497/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_FloatOptions_t996_Single_t151_Single_t151/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_GetSpeedBasedDuration_m5497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FloatOptions_t996_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1053_0_0_0;
extern const Il2CppType DOGetter_1_t1053_0_0_0;
extern const Il2CppType DOSetter_1_t1054_0_0_0;
extern const Il2CppType DOSetter_1_t1054_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo FloatPlugin_t1007_FloatPlugin_EvaluateAndApply_m5498_ParameterInfos[] = 
{
	{"options", 0, 134218235, 0, &FloatOptions_t996_0_0_0},
	{"t", 1, 134218236, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218237, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218238, 0, &DOGetter_1_t1053_0_0_0},
	{"setter", 4, 134218239, 0, &DOSetter_1_t1054_0_0_0},
	{"elapsed", 5, 134218240, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218241, 0, &Single_t151_0_0_0},
	{"changeValue", 7, 134218242, 0, &Single_t151_0_0_0},
	{"duration", 8, 134218243, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218244, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218245, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_FloatOptions_t996_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Single_t151_Single_t151_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.FloatOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo FloatPlugin_EvaluateAndApply_m5498_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&FloatPlugin_EvaluateAndApply_m5498/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_FloatOptions_t996_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Single_t151_Single_t151_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, FloatPlugin_t1007_FloatPlugin_EvaluateAndApply_m5498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::.ctor()
extern const MethodInfo FloatPlugin__ctor_m5499_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FloatPlugin__ctor_m5499/* method */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FloatPlugin_t1007_MethodInfos[] =
{
	&FloatPlugin_Reset_m5493_MethodInfo,
	&FloatPlugin_ConvertToStartValue_m5494_MethodInfo,
	&FloatPlugin_SetRelativeEndValue_m5495_MethodInfo,
	&FloatPlugin_SetChangeValue_m5496_MethodInfo,
	&FloatPlugin_GetSpeedBasedDuration_m5497_MethodInfo,
	&FloatPlugin_EvaluateAndApply_m5498_MethodInfo,
	&FloatPlugin__ctor_m5499_MethodInfo,
	NULL
};
extern const MethodInfo FloatPlugin_Reset_m5493_MethodInfo;
extern const MethodInfo FloatPlugin_ConvertToStartValue_m5494_MethodInfo;
extern const MethodInfo FloatPlugin_SetRelativeEndValue_m5495_MethodInfo;
extern const MethodInfo FloatPlugin_SetChangeValue_m5496_MethodInfo;
extern const MethodInfo FloatPlugin_GetSpeedBasedDuration_m5497_MethodInfo;
extern const MethodInfo FloatPlugin_EvaluateAndApply_m5498_MethodInfo;
static const Il2CppMethodReference FloatPlugin_t1007_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&FloatPlugin_Reset_m5493_MethodInfo,
	&FloatPlugin_ConvertToStartValue_m5494_MethodInfo,
	&FloatPlugin_SetRelativeEndValue_m5495_MethodInfo,
	&FloatPlugin_SetChangeValue_m5496_MethodInfo,
	&FloatPlugin_GetSpeedBasedDuration_m5497_MethodInfo,
	&FloatPlugin_EvaluateAndApply_m5498_MethodInfo,
};
static bool FloatPlugin_t1007_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FloatPlugin_t1007_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType FloatPlugin_t1007_0_0_0;
extern const Il2CppType FloatPlugin_t1007_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1008_0_0_0;
struct FloatPlugin_t1007;
const Il2CppTypeDefinitionMetadata FloatPlugin_t1007_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FloatPlugin_t1007_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1008_0_0_0/* parent */
	, FloatPlugin_t1007_VTable/* vtableMethods */
	, FloatPlugin_t1007_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FloatPlugin_t1007_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "FloatPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, FloatPlugin_t1007_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FloatPlugin_t1007_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FloatPlugin_t1007_0_0_0/* byval_arg */
	, &FloatPlugin_t1007_1_0_0/* this_arg */
	, &FloatPlugin_t1007_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FloatPlugin_t1007)/* instance_size */
	, sizeof (FloatPlugin_t1007)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.TweenerCore`3
extern TypeInfo TweenerCore_3_t1068_il2cpp_TypeInfo;
extern const Il2CppGenericContainer TweenerCore_3_t1068_Il2CppGenericContainer;
extern TypeInfo TweenerCore_3_t1068_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenerCore_3_t1068_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1068_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo TweenerCore_3_t1068_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenerCore_3_t1068_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1068_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo TweenerCore_3_t1068_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* TweenerCore_3_t1068_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter TweenerCore_3_t1068_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1068_Il2CppGenericContainer, TweenerCore_3_t1068_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* TweenerCore_3_t1068_Il2CppGenericParametersArray[3] = 
{
	&TweenerCore_3_t1068_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&TweenerCore_3_t1068_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&TweenerCore_3_t1068_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer TweenerCore_3_t1068_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenerCore_3_t1068_il2cpp_TypeInfo, 3, 0, TweenerCore_3_t1068_Il2CppGenericParametersArray };
// System.Void DG.Tweening.Core.TweenerCore`3::.ctor()
extern const MethodInfo TweenerCore_3__ctor_m5619_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void DG.Tweening.Core.TweenerCore`3::Reset()
extern const MethodInfo TweenerCore_3_Reset_m5620_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 227/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean DG.Tweening.Core.TweenerCore`3::Validate()
extern const MethodInfo TweenerCore_3_Validate_m5621_MethodInfo = 
{
	"Validate"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo TweenerCore_3_t1068_TweenerCore_3_UpdateDelay_m5622_ParameterInfos[] = 
{
	{"elapsed", 0, 134218246, 0, &Single_t151_0_0_0},
};
// System.Single DG.Tweening.Core.TweenerCore`3::UpdateDelay(System.Single)
extern const MethodInfo TweenerCore_3_UpdateDelay_m5622_MethodInfo = 
{
	"UpdateDelay"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenerCore_3_t1068_TweenerCore_3_UpdateDelay_m5622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean DG.Tweening.Core.TweenerCore`3::Startup()
extern const MethodInfo TweenerCore_3_Startup_m5623_MethodInfo = 
{
	"Startup"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateMode_t929_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo TweenerCore_3_t1068_TweenerCore_3_ApplyTween_m5624_ParameterInfos[] = 
{
	{"prevPosition", 0, 134218247, 0, &Single_t151_0_0_0},
	{"prevCompletedLoops", 1, 134218248, 0, &Int32_t127_0_0_0},
	{"newCompletedSteps", 2, 134218249, 0, &Int32_t127_0_0_0},
	{"useInversePosition", 3, 134218250, 0, &Boolean_t169_0_0_0},
	{"updateMode", 4, 134218251, 0, &UpdateMode_t929_0_0_0},
	{"updateNotice", 5, 134218252, 0, &UpdateNotice_t1012_0_0_0},
};
// System.Boolean DG.Tweening.Core.TweenerCore`3::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo TweenerCore_3_ApplyTween_m5624_MethodInfo = 
{
	"ApplyTween"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenerCore_3_t1068_TweenerCore_3_ApplyTween_m5624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenerCore_3_t1068_MethodInfos[] =
{
	&TweenerCore_3__ctor_m5619_MethodInfo,
	&TweenerCore_3_Reset_m5620_MethodInfo,
	&TweenerCore_3_Validate_m5621_MethodInfo,
	&TweenerCore_3_UpdateDelay_m5622_MethodInfo,
	&TweenerCore_3_Startup_m5623_MethodInfo,
	&TweenerCore_3_ApplyTween_m5624_MethodInfo,
	NULL
};
extern const MethodInfo TweenerCore_3_Reset_m5620_MethodInfo;
extern const MethodInfo TweenerCore_3_Validate_m5621_MethodInfo;
extern const MethodInfo TweenerCore_3_UpdateDelay_m5622_MethodInfo;
extern const MethodInfo TweenerCore_3_Startup_m5623_MethodInfo;
extern const MethodInfo TweenerCore_3_ApplyTween_m5624_MethodInfo;
static const Il2CppMethodReference TweenerCore_3_t1068_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&TweenerCore_3_Reset_m5620_MethodInfo,
	&TweenerCore_3_Validate_m5621_MethodInfo,
	&TweenerCore_3_UpdateDelay_m5622_MethodInfo,
	&TweenerCore_3_Startup_m5623_MethodInfo,
	&TweenerCore_3_ApplyTween_m5624_MethodInfo,
};
static bool TweenerCore_3_t1068_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType TweenerCore_3_t1068_gp_0_0_0_0;
extern const Il2CppType TweenerCore_3_t1068_gp_1_0_0_0;
extern const Il2CppType TweenerCore_3_t1068_gp_2_0_0_0;
extern const Il2CppGenericMethod ABSTweenPlugin_3_Reset_m5636_GenericMethod;
extern const Il2CppGenericMethod DOGetter_1_Invoke_m5637_GenericMethod;
extern const Il2CppGenericMethod Tweener_DoUpdateDelay_TisT1_t1123_TisT2_t1124_TisTPlugOptions_t1125_m5638_GenericMethod;
extern const Il2CppGenericMethod Tweener_DoStartup_TisT1_t1123_TisT2_t1124_TisTPlugOptions_t1125_m5639_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_EvaluateAndApply_m5640_GenericMethod;
static Il2CppRGCTXDefinition TweenerCore_3_t1068_RGCTXData[9] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1068_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1068_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1068_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_Reset_m5636_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &DOGetter_1_Invoke_m5637_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DoUpdateDelay_TisT1_t1123_TisT2_t1124_TisTPlugOptions_t1125_m5638_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DoStartup_TisT1_t1123_TisT2_t1124_TisTPlugOptions_t1125_m5639_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_EvaluateAndApply_m5640_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenerCore_3_t1068_0_0_0;
extern const Il2CppType TweenerCore_3_t1068_1_0_0;
struct TweenerCore_3_t1068;
const Il2CppTypeDefinitionMetadata TweenerCore_3_t1068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tweener_t99_0_0_0/* parent */
	, TweenerCore_3_t1068_VTable/* vtableMethods */
	, TweenerCore_3_t1068_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, TweenerCore_3_t1068_RGCTXData/* rgctxDefinition */
	, 288/* fieldStart */

};
TypeInfo TweenerCore_3_t1068_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenerCore`3"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, TweenerCore_3_t1068_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenerCore_3_t1068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenerCore_3_t1068_0_0_0/* byval_arg */
	, &TweenerCore_3_t1068_1_0_0/* this_arg */
	, &TweenerCore_3_t1068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &TweenerCore_3_t1068_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPlugin.h"
// Metadata Definition DG.Tweening.Plugins.LongPlugin
extern TypeInfo LongPlugin_t1009_il2cpp_TypeInfo;
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_Reset_m5500_ParameterInfos[] = 
{
	{"t", 0, 134218253, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_Reset_m5500_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&LongPlugin_Reset_m5500/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LongPlugin_t1009_LongPlugin_Reset_m5500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_ConvertToStartValue_m5501_ParameterInfos[] = 
{
	{"t", 0, 134218254, 0, &TweenerCore_3_t1055_0_0_0},
	{"value", 1, 134218255, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1092_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Int64 DG.Tweening.Plugins.LongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Int64)
extern const MethodInfo LongPlugin_ConvertToStartValue_m5501_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&LongPlugin_ConvertToStartValue_m5501/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092_Object_t_Int64_t1092/* invoker_method */
	, LongPlugin_t1009_LongPlugin_ConvertToStartValue_m5501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_SetRelativeEndValue_m5502_ParameterInfos[] = 
{
	{"t", 0, 134218256, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_SetRelativeEndValue_m5502_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&LongPlugin_SetRelativeEndValue_m5502/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LongPlugin_t1009_LongPlugin_SetRelativeEndValue_m5502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_SetChangeValue_m5503_ParameterInfos[] = 
{
	{"t", 0, 134218257, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_SetChangeValue_m5503_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&LongPlugin_SetChangeValue_m5503/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LongPlugin_t1009_LongPlugin_SetChangeValue_m5503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_GetSpeedBasedDuration_m5504_ParameterInfos[] = 
{
	{"options", 0, 134218258, 0, &NoOptions_t933_0_0_0},
	{"unitsXSecond", 1, 134218259, 0, &Single_t151_0_0_0},
	{"changeValue", 2, 134218260, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.LongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int64)
extern const MethodInfo LongPlugin_GetSpeedBasedDuration_m5504_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&LongPlugin_GetSpeedBasedDuration_m5504/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_NoOptions_t933_Single_t151_Int64_t1092/* invoker_method */
	, LongPlugin_t1009_LongPlugin_GetSpeedBasedDuration_m5504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t933_0_0_0;
extern const Il2CppType Tween_t934_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DOGetter_1_t1056_0_0_0;
extern const Il2CppType DOGetter_1_t1056_0_0_0;
extern const Il2CppType DOSetter_1_t1057_0_0_0;
extern const Il2CppType DOSetter_1_t1057_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UpdateNotice_t1012_0_0_0;
static const ParameterInfo LongPlugin_t1009_LongPlugin_EvaluateAndApply_m5505_ParameterInfos[] = 
{
	{"options", 0, 134218261, 0, &NoOptions_t933_0_0_0},
	{"t", 1, 134218262, 0, &Tween_t934_0_0_0},
	{"isRelative", 2, 134218263, 0, &Boolean_t169_0_0_0},
	{"getter", 3, 134218264, 0, &DOGetter_1_t1056_0_0_0},
	{"setter", 4, 134218265, 0, &DOSetter_1_t1057_0_0_0},
	{"elapsed", 5, 134218266, 0, &Single_t151_0_0_0},
	{"startValue", 6, 134218267, 0, &Int64_t1092_0_0_0},
	{"changeValue", 7, 134218268, 0, &Int64_t1092_0_0_0},
	{"duration", 8, 134218269, 0, &Single_t151_0_0_0},
	{"usingInversePosition", 9, 134218270, 0, &Boolean_t169_0_0_0},
	{"updateNotice", 10, 134218271, 0, &UpdateNotice_t1012_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int64_t1092_Int64_t1092_Single_t151_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Single,System.Int64,System.Int64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo LongPlugin_EvaluateAndApply_m5505_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&LongPlugin_EvaluateAndApply_m5505/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_NoOptions_t933_Object_t_SByte_t170_Object_t_Object_t_Single_t151_Int64_t1092_Int64_t1092_Single_t151_SByte_t170_Int32_t127/* invoker_method */
	, LongPlugin_t1009_LongPlugin_EvaluateAndApply_m5505_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::.ctor()
extern const MethodInfo LongPlugin__ctor_m5506_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LongPlugin__ctor_m5506/* method */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LongPlugin_t1009_MethodInfos[] =
{
	&LongPlugin_Reset_m5500_MethodInfo,
	&LongPlugin_ConvertToStartValue_m5501_MethodInfo,
	&LongPlugin_SetRelativeEndValue_m5502_MethodInfo,
	&LongPlugin_SetChangeValue_m5503_MethodInfo,
	&LongPlugin_GetSpeedBasedDuration_m5504_MethodInfo,
	&LongPlugin_EvaluateAndApply_m5505_MethodInfo,
	&LongPlugin__ctor_m5506_MethodInfo,
	NULL
};
extern const MethodInfo LongPlugin_Reset_m5500_MethodInfo;
extern const MethodInfo LongPlugin_ConvertToStartValue_m5501_MethodInfo;
extern const MethodInfo LongPlugin_SetRelativeEndValue_m5502_MethodInfo;
extern const MethodInfo LongPlugin_SetChangeValue_m5503_MethodInfo;
extern const MethodInfo LongPlugin_GetSpeedBasedDuration_m5504_MethodInfo;
extern const MethodInfo LongPlugin_EvaluateAndApply_m5505_MethodInfo;
static const Il2CppMethodReference LongPlugin_t1009_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&LongPlugin_Reset_m5500_MethodInfo,
	&LongPlugin_ConvertToStartValue_m5501_MethodInfo,
	&LongPlugin_SetRelativeEndValue_m5502_MethodInfo,
	&LongPlugin_SetChangeValue_m5503_MethodInfo,
	&LongPlugin_GetSpeedBasedDuration_m5504_MethodInfo,
	&LongPlugin_EvaluateAndApply_m5505_MethodInfo,
};
static bool LongPlugin_t1009_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LongPlugin_t1009_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t969_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LongPlugin_t1009_0_0_0;
extern const Il2CppType LongPlugin_t1009_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1010_0_0_0;
struct LongPlugin_t1009;
const Il2CppTypeDefinitionMetadata LongPlugin_t1009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LongPlugin_t1009_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1010_0_0_0/* parent */
	, LongPlugin_t1009_VTable/* vtableMethods */
	, LongPlugin_t1009_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LongPlugin_t1009_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LongPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, LongPlugin_t1009_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LongPlugin_t1009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LongPlugin_t1009_0_0_0/* byval_arg */
	, &LongPlugin_t1009_1_0_0/* this_arg */
	, &LongPlugin_t1009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LongPlugin_t1009)/* instance_size */
	, sizeof (LongPlugin_t1009)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.ColorOptions
extern TypeInfo ColorOptions_t1011_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptionsMethodDeclarations.h"
static const MethodInfo* ColorOptions_t1011_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ColorOptions_t1011_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool ColorOptions_t1011_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ColorOptions_t1011_1_0_0;
const Il2CppTypeDefinitionMetadata ColorOptions_t1011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, ColorOptions_t1011_VTable/* vtableMethods */
	, ColorOptions_t1011_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 295/* fieldStart */

};
TypeInfo ColorOptions_t1011_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, ColorOptions_t1011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorOptions_t1011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorOptions_t1011_0_0_0/* byval_arg */
	, &ColorOptions_t1011_1_0_0/* this_arg */
	, &ColorOptions_t1011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ColorOptions_t1011_marshal/* marshal_to_native_func */
	, (methodPointerType)ColorOptions_t1011_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ColorOptions_t1011_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ColorOptions_t1011)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorOptions_t1011)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ColorOptions_t1011_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"
// Metadata Definition DG.Tweening.Core.Enums.UpdateNotice
extern TypeInfo UpdateNotice_t1012_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNoticeMethodDeclarations.h"
static const MethodInfo* UpdateNotice_t1012_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UpdateNotice_t1012_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UpdateNotice_t1012_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UpdateNotice_t1012_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateNotice_t1012_1_0_0;
const Il2CppTypeDefinitionMetadata UpdateNotice_t1012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateNotice_t1012_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UpdateNotice_t1012_VTable/* vtableMethods */
	, UpdateNotice_t1012_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 296/* fieldStart */

};
TypeInfo UpdateNotice_t1012_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateNotice"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, UpdateNotice_t1012_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateNotice_t1012_0_0_0/* byval_arg */
	, &UpdateNotice_t1012_1_0_0/* this_arg */
	, &UpdateNotice_t1012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateNotice_t1012)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateNotice_t1012)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
extern TypeInfo __StaticArrayInitTypeSizeU3D120_t1013_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D120_t1013_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D120_t1013_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D120_t1013_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D120_t1013_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D120_t1013_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_0_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D120_t1013_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D120_t1013_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D120_t1013_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D120_t1013_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=120"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D120_t1013_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D120_t1013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D120_t1013_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D120_t1013_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D120_t1013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1013_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1013_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1013_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D120_t1013)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D120_t1013)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D120_t1013_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
extern TypeInfo __StaticArrayInitTypeSizeU3D50_t1014_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D50_t1014_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D50_t1014_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D50_t1014_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D50_t1014_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D50_t1014_1_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D50_t1014_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D50_t1014_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D50_t1014_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D50_t1014_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=50"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D50_t1014_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D50_t1014_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D50_t1014_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D50_t1014_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D50_t1014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1014_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1014_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1014_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D50_t1014)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D50_t1014)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D50_t1014_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
extern TypeInfo __StaticArrayInitTypeSizeU3D20_t1015_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D20_t1015_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D20_t1015_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D20_t1015_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D20_t1015_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D20_t1015_1_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D20_t1015_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D20_t1015_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D20_t1015_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D20_t1015_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=20"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D20_t1015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D20_t1015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D20_t1015_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D20_t1015_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D20_t1015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1015_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1015_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1015_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D20_t1015)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D20_t1015)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D20_t1015_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2MethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_il2cpp_TypeInfo__nestedTypes[3] =
{
	&__StaticArrayInitTypeSizeU3D120_t1013_0_0_0,
	&__StaticArrayInitTypeSizeU3D50_t1014_0_0_0,
	&__StaticArrayInitTypeSizeU3D20_t1015_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_1_0_0;
struct U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 299/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 54/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
