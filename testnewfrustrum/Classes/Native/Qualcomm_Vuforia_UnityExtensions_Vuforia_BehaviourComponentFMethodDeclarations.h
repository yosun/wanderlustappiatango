﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory
struct BehaviourComponentFactory_t601;
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t181;

// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
extern "C" Object_t * BehaviourComponentFactory_get_Instance_m2839 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
extern "C" void BehaviourComponentFactory_set_Instance_m435 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
extern "C" void BehaviourComponentFactory__ctor_m2840 (BehaviourComponentFactory_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
