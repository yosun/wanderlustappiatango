﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
struct Enumerator_t3756;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1245;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m24909_gshared (Enumerator_t3756 * __this, List_1_t1245 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m24909(__this, ___l, method) (( void (*) (Enumerator_t3756 *, List_1_t1245 *, const MethodInfo*))Enumerator__ctor_m24909_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24910_gshared (Enumerator_t3756 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24910(__this, method) (( Object_t * (*) (Enumerator_t3756 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24910_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m24911_gshared (Enumerator_t3756 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24911(__this, method) (( void (*) (Enumerator_t3756 *, const MethodInfo*))Enumerator_Dispose_m24911_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m24912_gshared (Enumerator_t3756 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m24912(__this, method) (( void (*) (Enumerator_t3756 *, const MethodInfo*))Enumerator_VerifyState_m24912_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24913_gshared (Enumerator_t3756 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24913(__this, method) (( bool (*) (Enumerator_t3756 *, const MethodInfo*))Enumerator_MoveNext_m24913_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t460  Enumerator_get_Current_m24914_gshared (Enumerator_t3756 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24914(__this, method) (( UICharInfo_t460  (*) (Enumerator_t3756 *, const MethodInfo*))Enumerator_get_Current_m24914_gshared)(__this, method)
