﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct InternalEnumerator_1_t3950;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27244_gshared (InternalEnumerator_1_t3950 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27244(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3950 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27244_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27245_gshared (InternalEnumerator_1_t3950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27245(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3950 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27245_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27246_gshared (InternalEnumerator_1_t3950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27246(__this, method) (( void (*) (InternalEnumerator_1_t3950 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27246_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27247_gshared (InternalEnumerator_1_t3950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27247(__this, method) (( bool (*) (InternalEnumerator_1_t3950 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27247_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C" X509ChainStatus_t1902  InternalEnumerator_1_get_Current_m27248_gshared (InternalEnumerator_1_t3950 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27248(__this, method) (( X509ChainStatus_t1902  (*) (InternalEnumerator_1_t3950 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27248_gshared)(__this, method)
