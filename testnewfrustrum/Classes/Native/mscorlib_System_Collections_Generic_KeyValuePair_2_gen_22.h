﻿#pragma once
#include <stdint.h>
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t73;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct  KeyValuePair_2_t3523 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::value
	SurfaceAbstractBehaviour_t73 * ___value_1;
};
