﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Byte>
struct GenericEqualityComparer_1_t3944;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m27188_gshared (GenericEqualityComparer_1_t3944 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m27188(__this, method) (( void (*) (GenericEqualityComparer_1_t3944 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m27188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m27189_gshared (GenericEqualityComparer_1_t3944 * __this, uint8_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m27189(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3944 *, uint8_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m27189_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m27190_gshared (GenericEqualityComparer_1_t3944 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m27190(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3944 *, uint8_t, uint8_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m27190_gshared)(__this, ___x, ___y, method)
