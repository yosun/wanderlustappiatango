﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t3795;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t1252;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32MethodDeclarations.h"
#define Enumerator__ctor_m25383(__this, ___dictionary, method) (( void (*) (Enumerator_t3795 *, Dictionary_2_t1252 *, const MethodInfo*))Enumerator__ctor_m25281_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25384(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25282_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25385(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25283_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25386(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25284_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25387(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m25388(__this, method) (( bool (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_MoveNext_m25286_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m25389(__this, method) (( KeyValuePair_2_t3792  (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_Current_m25287_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25390(__this, method) (( String_t* (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_CurrentKey_m25288_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25391(__this, method) (( int64_t (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_CurrentValue_m25289_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m25392(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_VerifyState_m25290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25393(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_VerifyCurrent_m25291_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m25394(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_Dispose_m25292_gshared)(__this, method)
