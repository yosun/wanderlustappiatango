﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t272;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t474  : public MulticastDelegate_t307
{
};
