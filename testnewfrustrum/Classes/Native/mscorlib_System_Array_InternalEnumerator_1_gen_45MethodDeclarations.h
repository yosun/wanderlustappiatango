﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>
struct InternalEnumerator_1_t3566;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21986_gshared (InternalEnumerator_1_t3566 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21986(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3566 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21986_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21987_gshared (InternalEnumerator_1_t3566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21987(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3566 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21988_gshared (InternalEnumerator_1_t3566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21988(__this, method) (( void (*) (InternalEnumerator_1_t3566 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21988_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21989_gshared (InternalEnumerator_1_t3566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21989(__this, method) (( bool (*) (InternalEnumerator_1_t3566 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21989_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>::get_Current()
extern "C" KeyValuePair_2_t3565  InternalEnumerator_1_get_Current_m21990_gshared (InternalEnumerator_1_t3566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21990(__this, method) (( KeyValuePair_2_t3565  (*) (InternalEnumerator_1_t3566 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21990_gshared)(__this, method)
