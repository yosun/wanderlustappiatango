﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.InAttribute
struct InAttribute_t2052;

// System.Void System.Runtime.InteropServices.InAttribute::.ctor()
extern "C" void InAttribute__ctor_m10301 (InAttribute_t2052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
