﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>
struct ShimEnumerator_t3942;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3931;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m27177_gshared (ShimEnumerator_t3942 * __this, Dictionary_2_t3931 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m27177(__this, ___host, method) (( void (*) (ShimEnumerator_t3942 *, Dictionary_2_t3931 *, const MethodInfo*))ShimEnumerator__ctor_m27177_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m27178_gshared (ShimEnumerator_t3942 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m27178(__this, method) (( bool (*) (ShimEnumerator_t3942 *, const MethodInfo*))ShimEnumerator_MoveNext_m27178_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m27179_gshared (ShimEnumerator_t3942 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m27179(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3942 *, const MethodInfo*))ShimEnumerator_get_Entry_m27179_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m27180_gshared (ShimEnumerator_t3942 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m27180(__this, method) (( Object_t * (*) (ShimEnumerator_t3942 *, const MethodInfo*))ShimEnumerator_get_Key_m27180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m27181_gshared (ShimEnumerator_t3942 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m27181(__this, method) (( Object_t * (*) (ShimEnumerator_t3942 *, const MethodInfo*))ShimEnumerator_get_Value_m27181_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m27182_gshared (ShimEnumerator_t3942 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m27182(__this, method) (( Object_t * (*) (ShimEnumerator_t3942 *, const MethodInfo*))ShimEnumerator_get_Current_m27182_gshared)(__this, method)
