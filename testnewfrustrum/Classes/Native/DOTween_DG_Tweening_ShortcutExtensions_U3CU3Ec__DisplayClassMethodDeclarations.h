﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
struct U3CU3Ec__DisplayClass92_t959;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::.ctor()
extern "C" void U3CU3Ec__DisplayClass92__ctor_m5366 (U3CU3Ec__DisplayClass92_t959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__90()
extern "C" Vector3_t15  U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5367 (U3CU3Ec__DisplayClass92_t959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__91(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5368 (U3CU3Ec__DisplayClass92_t959 * __this, Vector3_t15  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
