﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t689;
// Vuforia.WordResult
struct WordResult_t695;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
struct  Enumerator_t840 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::l
	List_1_t689 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::current
	WordResult_t695 * ___current_3;
};
