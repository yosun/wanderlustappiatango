﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t257;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t255;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t236;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t197;
// System.Collections.IEnumerator
struct IEnumerator_t410;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m1057 (Button_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t255 * Button_get_onClick_m1058 (Button_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m1059 (Button_t257 * __this, ButtonClickedEvent_t255 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m1060 (Button_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m1061 (Button_t257 * __this, PointerEventData_t236 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m1062 (Button_t257 * __this, BaseEventData_t197 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m1063 (Button_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
