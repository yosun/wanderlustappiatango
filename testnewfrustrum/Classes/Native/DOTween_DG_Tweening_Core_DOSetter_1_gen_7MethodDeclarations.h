﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t1042;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23697_gshared (DOSetter_1_t1042 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23697(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1042 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23697_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23698_gshared (DOSetter_1_t1042 * __this, int32_t ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23698(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1042 *, int32_t, const MethodInfo*))DOSetter_1_Invoke_m23698_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23699_gshared (DOSetter_1_t1042 * __this, int32_t ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23699(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1042 *, int32_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23699_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23700_gshared (DOSetter_1_t1042 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23700(__this, ___result, method) (( void (*) (DOSetter_1_t1042 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23700_gshared)(__this, ___result, method)
