﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct Predicate_1_t815;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m4421_gshared (Predicate_1_t815 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m4421(__this, ___object, ___method, method) (( void (*) (Predicate_1_t815 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m4421_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m19808_gshared (Predicate_1_t815 * __this, TrackableResultData_t642  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m19808(__this, ___obj, method) (( bool (*) (Predicate_1_t815 *, TrackableResultData_t642 , const MethodInfo*))Predicate_1_Invoke_m19808_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m19809_gshared (Predicate_1_t815 * __this, TrackableResultData_t642  ___obj, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m19809(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t815 *, TrackableResultData_t642 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m19809_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m19810_gshared (Predicate_1_t815 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m19810(__this, ___result, method) (( bool (*) (Predicate_1_t815 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m19810_gshared)(__this, ___result, method)
