﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t79;

// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m220 (UserDefinedTargetBuildingBehaviour_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
