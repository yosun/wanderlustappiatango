﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
struct  Nullable_1_t118 
{
	// T System.Nullable`1<DG.Tweening.LogBehaviour>::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::has_value
	bool ___has_value_1;
};
