﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
struct U3COnFinishSubmitU3Ec__Iterator1_t258;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::.ctor()
extern "C" void U3COnFinishSubmitU3Ec__Iterator1__ctor_m1051 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1052 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1053 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::MoveNext()
extern "C" bool U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m1054 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::Dispose()
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Dispose_m1055 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::Reset()
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Reset_m1056 (U3COnFinishSubmitU3Ec__Iterator1_t258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
