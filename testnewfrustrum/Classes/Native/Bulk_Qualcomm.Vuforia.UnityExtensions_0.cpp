﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFacMethodDeclarations.h"

// System.Type
#include "mscorlib_System_Type.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
extern "C" Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2614 (NullEyewearComponentFactory_t557 * __this, const MethodInfo* method)
{
	{
		return (Type_t *)NULL;
	}
}
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
extern "C" void NullEyewearComponentFactory__ctor_m2615 (NullEyewearComponentFactory_t557 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.EyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.EyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac_0MethodDeclarations.h"



// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::get_Instance()
extern TypeInfo* EyewearComponentFactory_t559_il2cpp_TypeInfo_var;
extern TypeInfo* NullEyewearComponentFactory_t557_il2cpp_TypeInfo_var;
extern "C" Object_t * EyewearComponentFactory_get_Instance_m2616 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearComponentFactory_t559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1010);
		NullEyewearComponentFactory_t557_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1011);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((EyewearComponentFactory_t559_StaticFields*)EyewearComponentFactory_t559_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullEyewearComponentFactory_t557 * L_1 = (NullEyewearComponentFactory_t557 *)il2cpp_codegen_object_new (NullEyewearComponentFactory_t557_il2cpp_TypeInfo_var);
		NullEyewearComponentFactory__ctor_m2615(L_1, /*hidden argument*/NULL);
		((EyewearComponentFactory_t559_StaticFields*)EyewearComponentFactory_t559_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((EyewearComponentFactory_t559_StaticFields*)EyewearComponentFactory_t559_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.EyewearComponentFactory::set_Instance(Vuforia.IEyewearComponentFactory)
extern TypeInfo* EyewearComponentFactory_t559_il2cpp_TypeInfo_var;
extern "C" void EyewearComponentFactory_set_Instance_m2617 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearComponentFactory_t559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1010);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((EyewearComponentFactory_t559_StaticFields*)EyewearComponentFactory_t559_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.EyewearComponentFactory::.ctor()
extern "C" void EyewearComponentFactory__ctor_m2618 (EyewearComponentFactory_t559 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetterMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"


// System.Void Vuforia.FactorySetter::.ctor()
extern "C" void FactorySetter__ctor_m512 (FactorySetter_t142 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m4271(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.InternalEyewearCalibrationProfileManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearCali.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewearCalibrationProfileManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearCaliMethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"
#include "mscorlib_ArrayTypes.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.String
#include "mscorlib_System_String.h"
// Vuforia.QCARWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARWrapperMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"


// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getMaxCount()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getMaxCount_m2619 (InternalEyewearCalibrationProfileManager_t560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(139 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetMaxCount() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getUsedCount()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getUsedCount_m2620 (InternalEyewearCalibrationProfileManager_t560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(140 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetUsedCount() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::isProfileUsed(System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_isProfileUsed_m2621 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(141 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMIsProfileUsed(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getActiveProfile()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getActiveProfile_m2622 (InternalEyewearCalibrationProfileManager_t560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(142 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetActiveProfile() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setActiveProfile(System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setActiveProfile_m2623 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(143 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetActiveProfile(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewearCalibrationProfileManager::getProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID)
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t156  InternalEyewearCalibrationProfileManager_getProjectionMatrix_m2624 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, int32_t ___eyeID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t156  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___profileID;
		int32_t L_6 = ___eyeID;
		IntPtr_t L_7 = V_1;
		NullCheck(L_4);
		InterfaceFuncInvoker3< int32_t, int32_t, int32_t, IntPtr_t >::Invoke(144 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7);
		IntPtr_t L_8 = V_1;
		SingleU5BU5D_t585* L_9 = V_0;
		SingleU5BU5D_t585* L_10 = V_0;
		NullCheck(L_10);
		Marshal_Copy_m4274(NULL /*static, unused*/, L_8, L_9, 0, (((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t156  L_11 = Matrix4x4_get_identity_m4275(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_11;
		V_3 = 0;
		goto IL_0053;
	}

IL_0044:
	{
		int32_t L_12 = V_3;
		SingleU5BU5D_t585* L_13 = V_0;
		int32_t L_14 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Matrix4x4_set_Item_m4276((&V_2), L_12, (*(float*)(float*)SZArrayLdElema(L_13, L_15)), /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_17 = V_3;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)16))))
		{
			goto IL_0044;
		}
	}
	{
		IntPtr_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Matrix4x4_t156  L_19 = V_2;
		return L_19;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID,UnityEngine.Matrix4x4)
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setProjectionMatrix_m2625 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, int32_t ___eyeID, Matrix4x4_t156  ___projectionMatrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	int32_t V_1 = 0;
	IntPtr_t V_2 = {0};
	bool V_3 = false;
	{
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, ((int32_t)16)));
		V_1 = 0;
		goto IL_001b;
	}

IL_000c:
	{
		SingleU5BU5D_t585* L_0 = V_0;
		int32_t L_1 = V_1;
		int32_t L_2 = V_1;
		float L_3 = Matrix4x4_get_Item_m4278((&___projectionMatrix), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		*((float*)(float*)SZArrayLdElema(L_0, L_1)) = (float)L_3;
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)16))))
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_8 = V_0;
		NullCheck(L_8);
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))), /*hidden argument*/NULL);
		V_2 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = ___profileID;
		int32_t L_12 = ___eyeID;
		IntPtr_t L_13 = V_2;
		NullCheck(L_10);
		bool L_14 = (bool)InterfaceFuncInvoker3< bool, int32_t, int32_t, IntPtr_t >::Invoke(145 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_11, L_12, L_13);
		V_3 = L_14;
		IntPtr_t L_15 = V_2;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_16 = V_3;
		return L_16;
	}
}
// System.String Vuforia.InternalEyewearCalibrationProfileManager::getProfileName(System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern "C" String_t* InternalEyewearCalibrationProfileManager_getProfileName_m2626 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		IntPtr_t L_2 = (IntPtr_t)InterfaceFuncInvoker1< IntPtr_t, int32_t >::Invoke(146 /* System.IntPtr Vuforia.IQCARWrapper::EyewearCPMGetProfileName(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		String_t* L_3 = Marshal_PtrToStringUni_m4279(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProfileName(System.Int32,System.String)
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setProfileName_m2627 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m4280(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = ___profileID;
		IntPtr_t L_4 = V_0;
		NullCheck(L_2);
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, IntPtr_t >::Invoke(147 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return L_5;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::clearProfile(System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_clearProfile_m2628 (InternalEyewearCalibrationProfileManager_t560 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(148 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMClearProfile(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.InternalEyewearCalibrationProfileManager::.ctor()
extern "C" void InternalEyewearCalibrationProfileManager__ctor_m2629 (InternalEyewearCalibrationProfileManager_t560 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Vuforia.CameraDevice
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.InternalEyewear
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensioMethodDeclarations.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// Vuforia.CameraDevice
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceMethodDeclarations.h"
// Vuforia.InternalEyewear
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearMethodDeclarations.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
struct Component_t103;
struct Camera_t3;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t103;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m262_gshared (Component_t103 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m262(__this, method) (( Object_t * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m347(__this, method) (( Camera_t3 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
struct Component_t103;
struct MeshFilter_t149;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t149_m469(__this, method) (( MeshFilter_t149 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
struct GameObject_t2;
struct MeshFilter_t149;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m437_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m437(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
#define GameObject_AddComponent_TisMeshFilter_t149_m4281(__this, method) (( MeshFilter_t149 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m437_gshared)(__this, method)


// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C" int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2630 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mNumDivisions_12);
		return L_0;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2631 (BackgroundPlaneAbstractBehaviour_t32 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mNumDivisions_12 = L_0;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_SetEditorValues_m2632 (BackgroundPlaneAbstractBehaviour_t32 * __this, int32_t ___numDivisions, const MethodInfo* method)
{
	{
		int32_t L_0 = ___numDivisions;
		BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2631(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C" bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m2633 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2630(__this, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___defaultNumDivisions_9);
		return ((((int32_t)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2634 (BackgroundPlaneAbstractBehaviour_t32 * __this, float ___depth, const MethodInfo* method)
{
	{
		float L_0 = ___depth;
		float L_1 = (__this->___mStereoDepth_11);
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		float L_2 = ___depth;
		__this->___mStereoDepth_11 = L_2;
		BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2639(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t3_m347_MethodInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_Start_m2635 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Component_GetComponent_TisCamera_t3_m347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4147(L_4, __this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		Transform_t11 * L_5 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = Transform_get_parent_m244(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_t3 * L_7 = Component_GetComponent_TisCamera_t3_m347(L_6, /*hidden argument*/Component_GetComponent_TisCamera_t3_m347_MethodInfo_var);
		__this->___mCamera_6 = L_7;
		Camera_t3 * L_8 = (__this->___mCamera_6);
		float L_9 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2654(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___mStereoDepth_11 = L_9;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_Update_m2636 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mVideoBgConfigChanged_5);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_1 = QCARRenderer_get_Instance_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Vuforia.QCARRenderer::IsVideoBackgroundInfoAvailable() */, L_1);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_3 = QCARRenderer_get_Instance_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VideoTextureInfo_t561  L_4 = (VideoTextureInfo_t561 )VirtFuncInvoker0< VideoTextureInfo_t561  >::Invoke(10 /* Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRenderer::GetVideoTextureInfo() */, L_3);
		__this->___mTextureInfo_2 = L_4;
		BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2638(__this, /*hidden argument*/NULL);
		BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2639(__this, /*hidden argument*/NULL);
		__this->___mVideoBgConfigChanged_5 = 0;
	}

IL_0037:
	{
		return;
	}
}
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C" Quaternion_t13  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2637 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = Vector3_get_right_m2341(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_1 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (270.0f), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern TypeInfo* Vector3U5BU5D_t154_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t19_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t293_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshFilter_t149_m4281_MethodInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2638 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		Int32U5BU5D_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		Vector2U5BU5D_t293_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		GameObject_AddComponent_TisMeshFilter_t149_m4281_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t149 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3U5BU5D_t154* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Int32U5BU5D_t19* V_11 = {0};
	Vector2U5BU5D_t293* V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	{
		MeshFilter_t149 * L_0 = Component_GetComponent_TisMeshFilter_t149_m469(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t149_m469_MethodInfo_var);
		V_0 = L_0;
		MeshFilter_t149 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m375(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t2 * L_3 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		MeshFilter_t149 * L_4 = GameObject_AddComponent_TisMeshFilter_t149_m4281(L_3, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t149_m4281_MethodInfo_var);
		V_0 = L_4;
		MeshFilter_t149 * L_5 = V_0;
		NullCheck(L_5);
		Mesh_t153 * L_6 = MeshFilter_get_mesh_m4283(L_5, /*hidden argument*/NULL);
		__this->___mMesh_10 = L_6;
	}

IL_0028:
	{
		Mesh_t153 * L_7 = (__this->___mMesh_10);
		NullCheck(L_7);
		Mesh_Clear_m4284(L_7, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___mNumDivisions_12);
		int32_t L_9 = (__this->___defaultNumDivisions_9);
		if ((((int32_t)L_8) >= ((int32_t)L_9)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_10 = (__this->___defaultNumDivisions_9);
		__this->___mNumDivisions_12 = L_10;
	}

IL_004d:
	{
		int32_t L_11 = (__this->___mNumDivisions_12);
		V_1 = L_11;
		int32_t L_12 = (__this->___mNumDivisions_12);
		V_2 = L_12;
		Mesh_t153 * L_13 = (__this->___mMesh_10);
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_13);
		Mesh_set_vertices_m4285(L_13, ((Vector3U5BU5D_t154*)SZArrayNew(Vector3U5BU5D_t154_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_14*(int32_t)L_15)))), /*hidden argument*/NULL);
		Mesh_t153 * L_16 = (__this->___mMesh_10);
		NullCheck(L_16);
		Vector3U5BU5D_t154* L_17 = Mesh_get_vertices_m487(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		V_4 = 0;
		goto IL_0109;
	}

IL_0082:
	{
		V_5 = 0;
		goto IL_00fe;
	}

IL_0087:
	{
		int32_t L_18 = V_5;
		int32_t L_19 = V_2;
		V_6 = ((float)((float)((float)((float)(((float)L_18))/(float)(((float)((int32_t)((int32_t)L_19-(int32_t)1))))))-(float)(0.5f)));
		int32_t L_20 = V_4;
		int32_t L_21 = V_1;
		V_7 = ((float)((float)((float)((float)(1.0f)-(float)((float)((float)(((float)L_20))/(float)(((float)((int32_t)((int32_t)L_21-(int32_t)1))))))))-(float)(0.5f)));
		Vector3U5BU5D_t154* L_22 = V_3;
		int32_t L_23 = V_4;
		int32_t L_24 = V_2;
		int32_t L_25 = V_5;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))+(int32_t)L_25)));
		float L_26 = V_6;
		((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_22, ((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))+(int32_t)L_25))))->___x_1 = ((float)((float)L_26*(float)(2.0f)));
		Vector3U5BU5D_t154* L_27 = V_3;
		int32_t L_28 = V_4;
		int32_t L_29 = V_2;
		int32_t L_30 = V_5;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)((int32_t)((int32_t)L_28*(int32_t)L_29))+(int32_t)L_30)));
		((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_27, ((int32_t)((int32_t)((int32_t)((int32_t)L_28*(int32_t)L_29))+(int32_t)L_30))))->___y_2 = (0.0f);
		Vector3U5BU5D_t154* L_31 = V_3;
		int32_t L_32 = V_4;
		int32_t L_33 = V_2;
		int32_t L_34 = V_5;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))+(int32_t)L_34)));
		float L_35 = V_7;
		((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_31, ((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))+(int32_t)L_34))))->___z_3 = ((float)((float)L_35*(float)(2.0f)));
		int32_t L_36 = V_5;
		V_5 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00fe:
	{
		int32_t L_37 = V_5;
		int32_t L_38 = V_2;
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_39 = V_4;
		V_4 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0109:
	{
		int32_t L_40 = V_4;
		int32_t L_41 = V_1;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0082;
		}
	}
	{
		Mesh_t153 * L_42 = (__this->___mMesh_10);
		Vector3U5BU5D_t154* L_43 = V_3;
		NullCheck(L_42);
		Mesh_set_vertices_m4285(L_42, L_43, /*hidden argument*/NULL);
		Mesh_t153 * L_44 = (__this->___mMesh_10);
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		NullCheck(L_44);
		Mesh_set_triangles_m4286(L_44, ((Int32U5BU5D_t19*)SZArrayNew(Int32U5BU5D_t19_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)L_46))*(int32_t)2))*(int32_t)3)))), /*hidden argument*/NULL);
		V_8 = 0;
		VideoTextureInfo_t561 * L_47 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_48 = &(L_47->___imageSize_1);
		int32_t L_49 = (L_48->___x_0);
		VideoTextureInfo_t561 * L_50 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_51 = &(L_50->___textureSize_0);
		int32_t L_52 = (L_51->___x_0);
		V_9 = ((float)((float)(((float)L_49))/(float)(((float)L_52))));
		VideoTextureInfo_t561 * L_53 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_54 = &(L_53->___imageSize_1);
		int32_t L_55 = (L_54->___y_1);
		VideoTextureInfo_t561 * L_56 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_57 = &(L_56->___textureSize_0);
		int32_t L_58 = (L_57->___y_1);
		V_10 = ((float)((float)(((float)L_55))/(float)(((float)L_58))));
		Mesh_t153 * L_59 = (__this->___mMesh_10);
		int32_t L_60 = V_1;
		int32_t L_61 = V_2;
		NullCheck(L_59);
		Mesh_set_uv_m4287(L_59, ((Vector2U5BU5D_t293*)SZArrayNew(Vector2U5BU5D_t293_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_60*(int32_t)L_61)))), /*hidden argument*/NULL);
		Mesh_t153 * L_62 = (__this->___mMesh_10);
		NullCheck(L_62);
		Int32U5BU5D_t19* L_63 = Mesh_get_triangles_m488(L_62, /*hidden argument*/NULL);
		V_11 = L_63;
		Mesh_t153 * L_64 = (__this->___mMesh_10);
		NullCheck(L_64);
		Vector2U5BU5D_t293* L_65 = Mesh_get_uv_m4288(L_64, /*hidden argument*/NULL);
		V_12 = L_65;
		V_13 = 0;
		goto IL_02f4;
	}

IL_01b6:
	{
		V_14 = 0;
		goto IL_02e4;
	}

IL_01be:
	{
		int32_t L_66 = V_13;
		int32_t L_67 = V_2;
		int32_t L_68 = V_14;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)L_67))+(int32_t)L_68));
		int32_t L_69 = V_13;
		int32_t L_70 = V_2;
		int32_t L_71 = V_14;
		int32_t L_72 = V_2;
		V_16 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)L_70))+(int32_t)L_71))+(int32_t)L_72))+(int32_t)1));
		int32_t L_73 = V_13;
		int32_t L_74 = V_2;
		int32_t L_75 = V_14;
		int32_t L_76 = V_2;
		V_17 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_73*(int32_t)L_74))+(int32_t)L_75))+(int32_t)L_76));
		int32_t L_77 = V_13;
		int32_t L_78 = V_2;
		int32_t L_79 = V_14;
		V_18 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_77*(int32_t)L_78))+(int32_t)L_79))+(int32_t)1));
		Int32U5BU5D_t19* L_80 = V_11;
		int32_t L_81 = V_8;
		int32_t L_82 = L_81;
		V_8 = ((int32_t)((int32_t)L_82+(int32_t)1));
		int32_t L_83 = V_15;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_82);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_80, L_82)) = (int32_t)L_83;
		Int32U5BU5D_t19* L_84 = V_11;
		int32_t L_85 = V_8;
		int32_t L_86 = L_85;
		V_8 = ((int32_t)((int32_t)L_86+(int32_t)1));
		int32_t L_87 = V_16;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, L_86);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_84, L_86)) = (int32_t)L_87;
		Int32U5BU5D_t19* L_88 = V_11;
		int32_t L_89 = V_8;
		int32_t L_90 = L_89;
		V_8 = ((int32_t)((int32_t)L_90+(int32_t)1));
		int32_t L_91 = V_17;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_90);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_88, L_90)) = (int32_t)L_91;
		Int32U5BU5D_t19* L_92 = V_11;
		int32_t L_93 = V_8;
		int32_t L_94 = L_93;
		V_8 = ((int32_t)((int32_t)L_94+(int32_t)1));
		int32_t L_95 = V_16;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, L_94);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_92, L_94)) = (int32_t)L_95;
		Int32U5BU5D_t19* L_96 = V_11;
		int32_t L_97 = V_8;
		int32_t L_98 = L_97;
		V_8 = ((int32_t)((int32_t)L_98+(int32_t)1));
		int32_t L_99 = V_15;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_98);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_96, L_98)) = (int32_t)L_99;
		Int32U5BU5D_t19* L_100 = V_11;
		int32_t L_101 = V_8;
		int32_t L_102 = L_101;
		V_8 = ((int32_t)((int32_t)L_102+(int32_t)1));
		int32_t L_103 = V_18;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_102);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_100, L_102)) = (int32_t)L_103;
		Vector2U5BU5D_t293* L_104 = V_12;
		int32_t L_105 = V_15;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = V_14;
		int32_t L_107 = V_2;
		float L_108 = V_9;
		int32_t L_109 = V_13;
		int32_t L_110 = V_1;
		float L_111 = V_10;
		Vector2_t10  L_112 = {0};
		Vector2__ctor_m264(&L_112, ((float)((float)((float)((float)(((float)L_106))/(float)(((float)((int32_t)((int32_t)L_107-(int32_t)1))))))*(float)L_108)), ((float)((float)((float)((float)(((float)L_109))/(float)(((float)((int32_t)((int32_t)L_110-(int32_t)1))))))*(float)L_111)), /*hidden argument*/NULL);
		*((Vector2_t10 *)(Vector2_t10 *)SZArrayLdElema(L_104, L_105)) = L_112;
		Vector2U5BU5D_t293* L_113 = V_12;
		int32_t L_114 = V_16;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, L_114);
		int32_t L_115 = V_14;
		int32_t L_116 = V_2;
		float L_117 = V_9;
		int32_t L_118 = V_13;
		int32_t L_119 = V_1;
		float L_120 = V_10;
		Vector2_t10  L_121 = {0};
		Vector2__ctor_m264(&L_121, ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_115+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_116-(int32_t)1))))))*(float)L_117)), ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_118+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_119-(int32_t)1))))))*(float)L_120)), /*hidden argument*/NULL);
		*((Vector2_t10 *)(Vector2_t10 *)SZArrayLdElema(L_113, L_114)) = L_121;
		Vector2U5BU5D_t293* L_122 = V_12;
		int32_t L_123 = V_17;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, L_123);
		int32_t L_124 = V_14;
		int32_t L_125 = V_2;
		float L_126 = V_9;
		int32_t L_127 = V_13;
		int32_t L_128 = V_1;
		float L_129 = V_10;
		Vector2_t10  L_130 = {0};
		Vector2__ctor_m264(&L_130, ((float)((float)((float)((float)(((float)L_124))/(float)(((float)((int32_t)((int32_t)L_125-(int32_t)1))))))*(float)L_126)), ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_127+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_128-(int32_t)1))))))*(float)L_129)), /*hidden argument*/NULL);
		*((Vector2_t10 *)(Vector2_t10 *)SZArrayLdElema(L_122, L_123)) = L_130;
		Vector2U5BU5D_t293* L_131 = V_12;
		int32_t L_132 = V_18;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = V_14;
		int32_t L_134 = V_2;
		float L_135 = V_9;
		int32_t L_136 = V_13;
		int32_t L_137 = V_1;
		float L_138 = V_10;
		Vector2_t10  L_139 = {0};
		Vector2__ctor_m264(&L_139, ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_133+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_134-(int32_t)1))))))*(float)L_135)), ((float)((float)((float)((float)(((float)L_136))/(float)(((float)((int32_t)((int32_t)L_137-(int32_t)1))))))*(float)L_138)), /*hidden argument*/NULL);
		*((Vector2_t10 *)(Vector2_t10 *)SZArrayLdElema(L_131, L_132)) = L_139;
		int32_t L_140 = V_14;
		V_14 = ((int32_t)((int32_t)L_140+(int32_t)1));
	}

IL_02e4:
	{
		int32_t L_141 = V_14;
		int32_t L_142 = V_2;
		if ((((int32_t)L_141) < ((int32_t)((int32_t)((int32_t)L_142-(int32_t)1)))))
		{
			goto IL_01be;
		}
	}
	{
		int32_t L_143 = V_13;
		V_13 = ((int32_t)((int32_t)L_143+(int32_t)1));
	}

IL_02f4:
	{
		int32_t L_144 = V_13;
		int32_t L_145 = V_1;
		if ((((int32_t)L_144) < ((int32_t)((int32_t)((int32_t)L_145-(int32_t)1)))))
		{
			goto IL_01b6;
		}
	}
	{
		Mesh_t153 * L_146 = (__this->___mMesh_10);
		Int32U5BU5D_t19* L_147 = V_11;
		NullCheck(L_146);
		Mesh_set_triangles_m4286(L_146, L_147, /*hidden argument*/NULL);
		Mesh_t153 * L_148 = (__this->___mMesh_10);
		Vector2U5BU5D_t293* L_149 = V_12;
		NullCheck(L_148);
		Mesh_set_uv_m4287(L_148, L_149, /*hidden argument*/NULL);
		Mesh_t153 * L_150 = (__this->___mMesh_10);
		Mesh_t153 * L_151 = (__this->___mMesh_10);
		NullCheck(L_151);
		Vector3U5BU5D_t154* L_152 = Mesh_get_vertices_m487(L_151, /*hidden argument*/NULL);
		NullCheck(L_152);
		NullCheck(L_150);
		Mesh_set_normals_m4289(L_150, ((Vector3U5BU5D_t154*)SZArrayNew(Vector3U5BU5D_t154_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_152)->max_length))))), /*hidden argument*/NULL);
		Mesh_t153 * L_153 = (__this->___mMesh_10);
		NullCheck(L_153);
		Mesh_RecalculateNormals_m4290(L_153, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* InternalEyewear_t587_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2639 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		InternalEyewear_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1018);
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	QCARAbstractBehaviour_t67 * V_1 = {0};
	Plane_t457  V_2 = {0};
	Ray_t96  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t15  V_5 = {0};
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	VideoBGCfgData_t662  V_10 = {0};
	float V_11 = 0.0f;
	Rect_t124  V_12 = {0};
	Rect_t124  V_13 = {0};
	Rect_t124  V_14 = {0};
	Rect_t124  V_15 = {0};
	Rect_t124  V_16 = {0};
	Rect_t124  V_17 = {0};
	Matrix4x4_t156  V_18 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4178(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t3 * L_1 = (__this->___mCamera_6);
		int32_t L_2 = UnityCameraExtensions_GetPixelWidthInt_m2653(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___mViewWidth_3 = L_2;
		Camera_t3 * L_3 = (__this->___mCamera_6);
		int32_t L_4 = UnityCameraExtensions_GetPixelHeightInt_m2652(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->___mViewHeight_4 = L_4;
		GameObject_t2 * L_5 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m273(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var);
		Quaternion_t13  L_7 = BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localRotation_m299(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_9 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = ((QCARAbstractBehaviour_t67 *)Castclass(L_9, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_10 = V_1;
		bool L_11 = Object_op_Inequality_m386(NULL /*static, unused*/, L_10, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t2 * L_13 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t11 * L_14 = GameObject_get_transform_m273(L_13, /*hidden argument*/NULL);
		Transform_t11 * L_15 = L_14;
		NullCheck(L_15);
		Quaternion_t13  L_16 = Transform_get_localRotation_m297(L_15, /*hidden argument*/NULL);
		Quaternion_t13  L_17 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_18 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localRotation_m299(L_15, L_18, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0087:
	{
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_00f1;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4124(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		GameObject_t2 * L_22 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t11 * L_23 = GameObject_get_transform_m273(L_22, /*hidden argument*/NULL);
		Transform_t11 * L_24 = L_23;
		NullCheck(L_24);
		Quaternion_t13  L_25 = Transform_get_localRotation_m297(L_24, /*hidden argument*/NULL);
		Vector3_t15  L_26 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_27 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (270.0f), L_26, /*hidden argument*/NULL);
		Quaternion_t13  L_28 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localRotation_m299(L_24, L_28, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_00c2:
	{
		GameObject_t2 * L_29 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t11 * L_30 = GameObject_get_transform_m273(L_29, /*hidden argument*/NULL);
		Transform_t11 * L_31 = L_30;
		NullCheck(L_31);
		Quaternion_t13  L_32 = Transform_get_localRotation_m297(L_31, /*hidden argument*/NULL);
		Vector3_t15  L_33 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_34 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (90.0f), L_33, /*hidden argument*/NULL);
		Quaternion_t13  L_35 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localRotation_m299(L_31, L_35, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_00f1:
	{
		int32_t L_36 = V_0;
		if ((!(((uint32_t)L_36) == ((uint32_t)4))))
		{
			goto IL_0121;
		}
	}
	{
		GameObject_t2 * L_37 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t11 * L_38 = GameObject_get_transform_m273(L_37, /*hidden argument*/NULL);
		Transform_t11 * L_39 = L_38;
		NullCheck(L_39);
		Quaternion_t13  L_40 = Transform_get_localRotation_m297(L_39, /*hidden argument*/NULL);
		Vector3_t15  L_41 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_42 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (180.0f), L_41, /*hidden argument*/NULL);
		Quaternion_t13  L_43 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_localRotation_m299(L_39, L_43, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0121:
	{
		int32_t L_44 = V_0;
		if ((!(((uint32_t)L_44) == ((uint32_t)2))))
		{
			goto IL_0183;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_45 = V_1;
		NullCheck(L_45);
		bool L_46 = QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4124(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0159;
		}
	}
	{
		GameObject_t2 * L_47 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_t11 * L_48 = GameObject_get_transform_m273(L_47, /*hidden argument*/NULL);
		Transform_t11 * L_49 = L_48;
		NullCheck(L_49);
		Quaternion_t13  L_50 = Transform_get_localRotation_m297(L_49, /*hidden argument*/NULL);
		Vector3_t15  L_51 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_52 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (90.0f), L_51, /*hidden argument*/NULL);
		Quaternion_t13  L_53 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_localRotation_m299(L_49, L_53, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0159:
	{
		GameObject_t2 * L_54 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t11 * L_55 = GameObject_get_transform_m273(L_54, /*hidden argument*/NULL);
		Transform_t11 * L_56 = L_55;
		NullCheck(L_56);
		Quaternion_t13  L_57 = Transform_get_localRotation_m297(L_56, /*hidden argument*/NULL);
		Vector3_t15  L_58 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_59 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (270.0f), L_58, /*hidden argument*/NULL);
		Quaternion_t13  L_60 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_57, L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localRotation_m299(L_56, L_60, /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_61 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)8))))
		{
			goto IL_01c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_62 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(16 /* Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::GetCameraDirection() */, L_62);
		if ((!(((uint32_t)L_63) == ((uint32_t)2))))
		{
			goto IL_01c2;
		}
	}
	{
		GameObject_t2 * L_64 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_t11 * L_65 = GameObject_get_transform_m273(L_64, /*hidden argument*/NULL);
		Transform_t11 * L_66 = L_65;
		NullCheck(L_66);
		Quaternion_t13  L_67 = Transform_get_localRotation_m297(L_66, /*hidden argument*/NULL);
		Vector3_t15  L_68 = Vector3_get_up_m1984(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_69 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (180.0f), L_68, /*hidden argument*/NULL);
		Quaternion_t13  L_70 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_67, L_69, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_set_localRotation_m299(L_66, L_70, /*hidden argument*/NULL);
	}

IL_01c2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
		InternalEyewear_t587 * L_71 = InternalEyewear_get_Instance_m2764(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		bool L_72 = InternalEyewear_IsSeeThru_m2766(L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_01f8;
		}
	}
	{
		GameObject_t2 * L_73 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_73);
		Transform_t11 * L_74 = GameObject_get_transform_m273(L_73, /*hidden argument*/NULL);
		float L_75 = (__this->___mStereoDepth_11);
		Vector3_t15  L_76 = {0};
		Vector3__ctor_m249(&L_76, (0.0f), (0.0f), L_75, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_localPosition_m2264(L_74, L_76, /*hidden argument*/NULL);
		goto IL_031c;
	}

IL_01f8:
	{
		Camera_t3 * L_77 = (__this->___mCamera_6);
		NullCheck(L_77);
		Transform_t11 * L_78 = Component_get_transform_m243(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t15  L_79 = Transform_get_forward_m282(L_78, /*hidden argument*/NULL);
		Camera_t3 * L_80 = (__this->___mCamera_6);
		NullCheck(L_80);
		Transform_t11 * L_81 = Component_get_transform_m243(L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t15  L_82 = Transform_get_position_m247(L_81, /*hidden argument*/NULL);
		Camera_t3 * L_83 = (__this->___mCamera_6);
		NullCheck(L_83);
		Transform_t11 * L_84 = Component_get_transform_m243(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		Vector3_t15  L_85 = Transform_get_forward_m282(L_84, /*hidden argument*/NULL);
		float L_86 = (__this->___mStereoDepth_11);
		Vector3_t15  L_87 = Vector3_op_Multiply_m2363(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
		Vector3_t15  L_88 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_82, L_87, /*hidden argument*/NULL);
		Plane__ctor_m2223((&V_2), L_79, L_88, /*hidden argument*/NULL);
		Camera_t3 * L_89 = (__this->___mCamera_6);
		Camera_t3 * L_90 = (__this->___mCamera_6);
		NullCheck(L_90);
		Rect_t124  L_91 = Camera_get_pixelRect_m4291(L_90, /*hidden argument*/NULL);
		V_12 = L_91;
		float L_92 = Rect_get_xMin_m2184((&V_12), /*hidden argument*/NULL);
		Camera_t3 * L_93 = (__this->___mCamera_6);
		NullCheck(L_93);
		Rect_t124  L_94 = Camera_get_pixelRect_m4291(L_93, /*hidden argument*/NULL);
		V_13 = L_94;
		float L_95 = Rect_get_xMax_m2175((&V_13), /*hidden argument*/NULL);
		Camera_t3 * L_96 = (__this->___mCamera_6);
		NullCheck(L_96);
		Rect_t124  L_97 = Camera_get_pixelRect_m4291(L_96, /*hidden argument*/NULL);
		V_14 = L_97;
		float L_98 = Rect_get_xMin_m2184((&V_14), /*hidden argument*/NULL);
		Camera_t3 * L_99 = (__this->___mCamera_6);
		NullCheck(L_99);
		Rect_t124  L_100 = Camera_get_pixelRect_m4291(L_99, /*hidden argument*/NULL);
		V_15 = L_100;
		float L_101 = Rect_get_yMin_m2183((&V_15), /*hidden argument*/NULL);
		Camera_t3 * L_102 = (__this->___mCamera_6);
		NullCheck(L_102);
		Rect_t124  L_103 = Camera_get_pixelRect_m4291(L_102, /*hidden argument*/NULL);
		V_16 = L_103;
		float L_104 = Rect_get_yMax_m2176((&V_16), /*hidden argument*/NULL);
		Camera_t3 * L_105 = (__this->___mCamera_6);
		NullCheck(L_105);
		Rect_t124  L_106 = Camera_get_pixelRect_m4291(L_105, /*hidden argument*/NULL);
		V_17 = L_106;
		float L_107 = Rect_get_yMin_m2183((&V_17), /*hidden argument*/NULL);
		Vector3_t15  L_108 = {0};
		Vector3__ctor_m249(&L_108, ((float)((float)L_92+(float)((float)((float)((float)((float)L_95-(float)L_98))/(float)(2.0f))))), ((float)((float)L_101+(float)((float)((float)((float)((float)L_104-(float)L_107))/(float)(2.0f))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_89);
		Ray_t96  L_109 = Camera_ScreenPointToRay_m254(L_89, L_108, /*hidden argument*/NULL);
		V_3 = L_109;
		V_4 = (0.0f);
		Ray_t96  L_110 = V_3;
		Plane_Raycast_m2224((&V_2), L_110, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_111 = (__this->___mCamera_6);
		NullCheck(L_111);
		Transform_t11 * L_112 = Component_get_transform_m243(L_111, /*hidden argument*/NULL);
		float L_113 = V_4;
		Vector3_t15  L_114 = Ray_GetPoint_m2225((&V_3), L_113, /*hidden argument*/NULL);
		NullCheck(L_112);
		Vector3_t15  L_115 = Transform_InverseTransformPoint_m2222(L_112, L_114, /*hidden argument*/NULL);
		V_5 = L_115;
		GameObject_t2 * L_116 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_116);
		Transform_t11 * L_117 = GameObject_get_transform_m273(L_116, /*hidden argument*/NULL);
		Vector3_t15  L_118 = V_5;
		NullCheck(L_117);
		Transform_set_localPosition_m2264(L_117, L_118, /*hidden argument*/NULL);
	}

IL_031c:
	{
		int32_t L_119 = (__this->___mViewWidth_3);
		int32_t L_120 = (__this->___mViewHeight_4);
		V_6 = ((float)((float)(((float)L_119))/(float)(((float)L_120))));
		Camera_t3 * L_121 = (__this->___mCamera_6);
		NullCheck(L_121);
		Matrix4x4_t156  L_122 = Camera_get_projectionMatrix_m4292(L_121, /*hidden argument*/NULL);
		V_18 = L_122;
		float L_123 = Matrix4x4_get_Item_m4278((&V_18), 5, /*hidden argument*/NULL);
		V_7 = ((float)((float)(1.0f)/(float)L_123));
		float L_124 = (__this->___mStereoDepth_11);
		float L_125 = V_7;
		V_8 = ((float)((float)L_124*(float)L_125));
		float L_126 = V_8;
		float L_127 = V_6;
		V_9 = ((float)((float)L_126*(float)L_127));
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_128 = QCARRenderer_get_Instance_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_128);
		VideoBGCfgData_t662  L_129 = (VideoBGCfgData_t662 )VirtFuncInvoker0< VideoBGCfgData_t662  >::Invoke(5 /* Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRenderer::GetVideoBackgroundConfig() */, L_128);
		V_10 = L_129;
		Vec2I_t663 * L_130 = &((&V_10)->___size_3);
		int32_t L_131 = (L_130->___y_1);
		int32_t L_132 = (__this->___mViewHeight_4);
		if ((((int32_t)L_131) == ((int32_t)L_132)))
		{
			goto IL_0396;
		}
	}
	{
		float L_133 = V_8;
		Vec2I_t663 * L_134 = &((&V_10)->___size_3);
		int32_t L_135 = (L_134->___y_1);
		int32_t L_136 = (__this->___mViewHeight_4);
		V_8 = ((float)((float)L_133*(float)((float)((float)(((float)L_135))/(float)(((float)L_136))))));
	}

IL_0396:
	{
		Vec2I_t663 * L_137 = &((&V_10)->___size_3);
		int32_t L_138 = (L_137->___x_0);
		int32_t L_139 = (__this->___mViewWidth_3);
		if ((((int32_t)L_138) == ((int32_t)L_139)))
		{
			goto IL_03c4;
		}
	}
	{
		float L_140 = V_9;
		Vec2I_t663 * L_141 = &((&V_10)->___size_3);
		int32_t L_142 = (L_141->___x_0);
		int32_t L_143 = (__this->___mViewWidth_3);
		V_9 = ((float)((float)L_140*(float)((float)((float)(((float)L_142))/(float)(((float)L_143))))));
	}

IL_03c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_144 = QCARRuntimeUtilities_get_IsPortraitOrientation_m4180(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_144)
		{
			goto IL_03f2;
		}
	}
	{
		VideoTextureInfo_t561 * L_145 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_146 = &(L_145->___imageSize_1);
		int32_t L_147 = (L_146->___y_1);
		VideoTextureInfo_t561 * L_148 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_149 = &(L_148->___imageSize_1);
		int32_t L_150 = (L_149->___x_0);
		V_11 = ((float)((float)(((float)L_147))/(float)(((float)L_150))));
		goto IL_0417;
	}

IL_03f2:
	{
		VideoTextureInfo_t561 * L_151 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_152 = &(L_151->___imageSize_1);
		int32_t L_153 = (L_152->___x_0);
		VideoTextureInfo_t561 * L_154 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_155 = &(L_154->___imageSize_1);
		int32_t L_156 = (L_155->___y_1);
		V_11 = ((float)((float)(((float)L_153))/(float)(((float)L_156))));
	}

IL_0417:
	{
		bool L_157 = BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2640(__this, /*hidden argument*/NULL);
		if (!L_157)
		{
			goto IL_046c;
		}
	}
	{
		float L_158 = V_11;
		if ((!(((float)L_158) > ((float)(1.0f)))))
		{
			goto IL_044a;
		}
	}
	{
		GameObject_t2 * L_159 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_159);
		Transform_t11 * L_160 = GameObject_get_transform_m273(L_159, /*hidden argument*/NULL);
		float L_161 = V_9;
		float L_162 = V_9;
		float L_163 = V_11;
		Vector3_t15  L_164 = {0};
		Vector3__ctor_m249(&L_164, L_161, (1.0f), ((float)((float)L_162/(float)L_163)), /*hidden argument*/NULL);
		NullCheck(L_160);
		Transform_set_localScale_m2265(L_160, L_164, /*hidden argument*/NULL);
		return;
	}

IL_044a:
	{
		GameObject_t2 * L_165 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_165);
		Transform_t11 * L_166 = GameObject_get_transform_m273(L_165, /*hidden argument*/NULL);
		float L_167 = V_9;
		float L_168 = V_11;
		float L_169 = V_9;
		Vector3_t15  L_170 = {0};
		Vector3__ctor_m249(&L_170, ((float)((float)L_167/(float)L_168)), (1.0f), L_169, /*hidden argument*/NULL);
		NullCheck(L_166);
		Transform_set_localScale_m2265(L_166, L_170, /*hidden argument*/NULL);
		return;
	}

IL_046c:
	{
		float L_171 = V_11;
		if ((!(((float)L_171) > ((float)(1.0f)))))
		{
			goto IL_0497;
		}
	}
	{
		GameObject_t2 * L_172 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_172);
		Transform_t11 * L_173 = GameObject_get_transform_m273(L_172, /*hidden argument*/NULL);
		float L_174 = V_8;
		float L_175 = V_11;
		float L_176 = V_8;
		Vector3_t15  L_177 = {0};
		Vector3__ctor_m249(&L_177, ((float)((float)L_174*(float)L_175)), (1.0f), L_176, /*hidden argument*/NULL);
		NullCheck(L_173);
		Transform_set_localScale_m2265(L_173, L_177, /*hidden argument*/NULL);
		return;
	}

IL_0497:
	{
		GameObject_t2 * L_178 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_178);
		Transform_t11 * L_179 = GameObject_get_transform_m273(L_178, /*hidden argument*/NULL);
		float L_180 = V_8;
		float L_181 = V_8;
		float L_182 = V_11;
		Vector3_t15  L_183 = {0};
		Vector3__ctor_m249(&L_183, L_180, (1.0f), ((float)((float)L_181*(float)L_182)), /*hidden argument*/NULL);
		NullCheck(L_179);
		Transform_set_localScale_m2265(L_179, L_183, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2640 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (__this->___mViewWidth_3);
		int32_t L_1 = (__this->___mViewHeight_4);
		V_0 = ((float)((float)(((float)L_0))/(float)(((float)L_1))));
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_get_IsPortraitOrientation_m4180(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		VideoTextureInfo_t561 * L_3 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_4 = &(L_3->___imageSize_1);
		int32_t L_5 = (L_4->___y_1);
		VideoTextureInfo_t561 * L_6 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_7 = &(L_6->___imageSize_1);
		int32_t L_8 = (L_7->___x_0);
		V_1 = ((float)((float)(((float)L_5))/(float)(((float)L_8))));
		goto IL_0061;
	}

IL_003d:
	{
		VideoTextureInfo_t561 * L_9 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_10 = &(L_9->___imageSize_1);
		int32_t L_11 = (L_10->___x_0);
		VideoTextureInfo_t561 * L_12 = &(__this->___mTextureInfo_2);
		Vec2I_t663 * L_13 = &(L_12->___imageSize_1);
		int32_t L_14 = (L_13->___y_1);
		V_1 = ((float)((float)(((float)L_11))/(float)(((float)L_14))));
	}

IL_0061:
	{
		float L_15 = V_0;
		float L_16 = V_1;
		return ((((int32_t)((!(((float)L_15) >= ((float)L_16)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m543 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	{
		__this->___mVideoBgConfigChanged_5 = 1;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C" void BackgroundPlaneAbstractBehaviour__ctor_m391 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method)
{
	{
		__this->___defaultNumDivisions_9 = 2;
		__this->___mNumDivisions_12 = 2;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour__cctor_m2641 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		((BackgroundPlaneAbstractBehaviour_t32_StaticFields*)BackgroundPlaneAbstractBehaviour_t32_il2cpp_TypeInfo_var->static_fields)->___maxDisplacement_8 = (3000.0f);
		return;
	}
}
// Vuforia.InternalEyewearUserCalibrator
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearUser.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewearUserCalibrator
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearUserMethodDeclarations.h"

#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"


// System.Boolean Vuforia.InternalEyewearUserCalibrator::init(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_init_m2642 (InternalEyewearUserCalibrator_t562 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___surfaceWidth;
		int32_t L_2 = ___surfaceHeight;
		int32_t L_3 = ___targetWidth;
		int32_t L_4 = ___targetHeight;
		NullCheck(L_0);
		bool L_5 = (bool)InterfaceFuncInvoker4< bool, int32_t, int32_t, int32_t, int32_t >::Invoke(149 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3, L_4);
		return L_5;
	}
}
// System.Single Vuforia.InternalEyewearUserCalibrator::getMinScaleHint()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" float InternalEyewearUserCalibrator_getMinScaleHint_m2643 (InternalEyewearUserCalibrator_t562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(150 /* System.Single Vuforia.IQCARWrapper::EyewearUserCalibratorGetMinScaleHint() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single Vuforia.InternalEyewearUserCalibrator::getMaxScaleHint()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" float InternalEyewearUserCalibrator_getMaxScaleHint_m2644 (InternalEyewearUserCalibrator_t562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(151 /* System.Single Vuforia.IQCARWrapper::EyewearUserCalibratorGetMaxScaleHint() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearUserCalibrator::isStereoStretched()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_isStereoStretched_m2645 (InternalEyewearUserCalibrator_t562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(152 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorIsStereoStretched() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearUserCalibrator::getProjectionMatrix(Vuforia.InternalEyewear/EyewearCalibrationReading[],UnityEngine.Matrix4x4)
extern const Il2CppType* EyewearCalibrationReading_t586_0_0_0_var;
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* EyewearCalibrationReading_t586_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_getProjectionMatrix_m2646 (InternalEyewearUserCalibrator_t562 * __this, EyewearCalibrationReadingU5BU5D_t760* ___readings, Matrix4x4_t156  ___projectionMatrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearCalibrationReading_t586_0_0_0_var = il2cpp_codegen_type_from_index(1019);
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		EyewearCalibrationReading_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1019);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SingleU5BU5D_t585* V_4 = {0};
	IntPtr_t V_5 = {0};
	bool V_6 = false;
	int32_t V_7 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(EyewearCalibrationReading_t586_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		EyewearCalibrationReadingU5BU5D_t760* L_2 = ___readings;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_0 = L_3;
		int64_t L_4 = IntPtr_ToInt64_m4293((&V_0), /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_005c;
	}

IL_0025:
	{
		int64_t L_5 = V_1;
		IntPtr__ctor_m4294((&V_3), L_5, /*hidden argument*/NULL);
		EyewearCalibrationReadingU5BU5D_t760* L_6 = ___readings;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		EyewearCalibrationReading_t586  L_8 = (*(EyewearCalibrationReading_t586 *)((EyewearCalibrationReading_t586 *)(EyewearCalibrationReading_t586 *)SZArrayLdElema(L_6, L_7)));
		Object_t * L_9 = Box(EyewearCalibrationReading_t586_il2cpp_TypeInfo_var, &L_8);
		IntPtr_t L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		int64_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(EyewearCalibrationReading_t586_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_13 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = ((int64_t)((int64_t)L_11+(int64_t)(((int64_t)L_13))));
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_15 = V_2;
		EyewearCalibrationReadingU5BU5D_t760* L_16 = ___readings;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_0025;
		}
	}
	{
		V_4 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_18 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_19 = V_4;
		NullCheck(L_19);
		IntPtr_t L_20 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_18*(int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))), /*hidden argument*/NULL);
		V_5 = L_20;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_21 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_22 = V_0;
		EyewearCalibrationReadingU5BU5D_t760* L_23 = ___readings;
		NullCheck(L_23);
		IntPtr_t L_24 = V_5;
		NullCheck(L_21);
		bool L_25 = (bool)InterfaceFuncInvoker3< bool, IntPtr_t, int32_t, IntPtr_t >::Invoke(153 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_21, L_22, (((int32_t)(((Array_t *)L_23)->max_length))), L_24);
		V_6 = L_25;
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_00c9;
		}
	}
	{
		IntPtr_t L_27 = V_5;
		SingleU5BU5D_t585* L_28 = V_4;
		SingleU5BU5D_t585* L_29 = V_4;
		NullCheck(L_29);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_Copy_m4274(NULL /*static, unused*/, L_27, L_28, 0, (((int32_t)(((Array_t *)L_29)->max_length))), /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_00c3;
	}

IL_00af:
	{
		int32_t L_30 = V_7;
		SingleU5BU5D_t585* L_31 = V_4;
		int32_t L_32 = V_7;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		Matrix4x4_set_Item_m4276((&___projectionMatrix), L_30, (*(float*)(float*)SZArrayLdElema(L_31, L_33)), /*hidden argument*/NULL);
		int32_t L_34 = V_7;
		V_7 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_35 = V_7;
		if ((((int32_t)L_35) < ((int32_t)((int32_t)16))))
		{
			goto IL_00af;
		}
	}

IL_00c9:
	{
		IntPtr_t L_36 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		IntPtr_t L_37 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		bool L_38 = V_6;
		return L_38;
	}
}
// System.Void Vuforia.InternalEyewearUserCalibrator::.ctor()
extern "C" void InternalEyewearUserCalibrator__ctor_m2647 (InternalEyewearUserCalibrator_t562 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.IOSCamRecoveringHelper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.IOSCamRecoveringHelper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHelMethodDeclarations.h"



// System.Void Vuforia.IOSCamRecoveringHelper::SetHasJustResumed()
extern TypeInfo* IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper_SetHasJustResumed_m2648 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 1;
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean Vuforia.IOSCamRecoveringHelper::TryToRecover()
extern TypeInfo* IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern "C" bool IOSCamRecoveringHelper_TryToRecover_m2649 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_1 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3;
		if (!L_1)
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_2 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6;
		if (L_2)
		{
			goto IL_00c0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_3 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4;
		if (L_3)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 1;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		goto IL_00e1;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		int32_t L_4 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)15))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		int32_t L_6 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = ((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)10))))
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
		return 0;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_8 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.CameraDevice::Stop() */, L_8);
		if (L_9)
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_10 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.CameraDevice::Start() */, L_10);
		if (L_11)
		{
			goto IL_00a6;
		}
	}
	{
		return 0;
	}

IL_00a6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 1;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		goto IL_00e1;
	}

IL_00c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		int32_t L_12 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7;
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)20))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
	}

IL_00e1:
	{
		return 1;
	}
}
// System.Void Vuforia.IOSCamRecoveringHelper::SetSuccessfullyRecovered()
extern TypeInfo* IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper_SetSuccessfullyRecovered_m2650 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m459(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_1 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_2 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		bool L_3 = ((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4;
		if (!L_3)
		{
			goto IL_0041;
		}
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vuforia.IOSCamRecoveringHelper::.cctor()
extern TypeInfo* IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper__cctor_m2651 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1020);
		s_Il2CppMethodIntialized = true;
	}
	{
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 1;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t563_StaticFields*)IOSCamRecoveringHelper_t563_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
		return;
	}
}
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensio.h"
#ifndef _MSC_VER
#else
#endif



// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelHeightInt_m2652 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	Rect_t124  V_0 = {0};
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		Rect_t124  L_1 = Camera_get_pixelRect_m4291(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_height_m2098((&V_0), /*hidden argument*/NULL);
		return (((int32_t)L_2));
	}
}
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelWidthInt_m2653 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	Rect_t124  V_0 = {0};
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		Rect_t124  L_1 = Camera_get_pixelRect_m4291(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_width_m2097((&V_0), /*hidden argument*/NULL);
		return (((int32_t)L_2));
	}
}
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMaxDepthForVideoBackground_m2654 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		float L_1 = Camera_get_farClipPlane_m2037(L_0, /*hidden argument*/NULL);
		return ((float)((float)L_1*(float)(0.99f)));
	}
}
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMinDepthForVideoBackground_m2655 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		float L_1 = Camera_get_nearClipPlane_m2038(L_0, /*hidden argument*/NULL);
		return ((float)((float)L_1*(float)(1.01f)));
	}
}
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_MethodDeclarations.h"



// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
struct Component_t103;
struct Renderer_t8;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t8_m261(__this, method) (( Renderer_t8 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)


// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::get_CurrentStatus()
extern "C" int32_t TrackableBehaviour_get_CurrentStatus_m2656 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStatus_6);
		return L_0;
	}
}
// Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable()
extern "C" Object_t * TrackableBehaviour_get_Trackable_m547 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mTrackable_7);
		return L_0;
	}
}
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C" String_t* TrackableBehaviour_get_TrackableName_m544 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mTrackableName_2);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern TypeInfo* ITrackableEventHandler_t178_il2cpp_TypeInfo_var;
extern "C" void TrackableBehaviour_RegisterTrackableEventHandler_m414 (TrackableBehaviour_t44 * __this, Object_t * ___trackableEventHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(187);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t566 * L_0 = (__this->___mTrackableEventHandlers_8);
		Object_t * L_1 = ___trackableEventHandler;
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Add(!0) */, L_0, L_1);
		Object_t * L_2 = ___trackableEventHandler;
		int32_t L_3 = (__this->___mStatus_6);
		NullCheck(L_2);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t178_il2cpp_TypeInfo_var, L_2, 0, L_3);
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C" bool TrackableBehaviour_UnregisterTrackableEventHandler_m2657 (TrackableBehaviour_t44 * __this, Object_t * ___trackableEventHandler, const MethodInfo* method)
{
	{
		List_1_t566 * L_0 = (__this->___mTrackableEventHandlers_8);
		Object_t * L_1 = ___trackableEventHandler;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
extern TypeInfo* ITrackableEventHandler_t178_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t793_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4296_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4297_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4298_MethodInfo_var;
extern "C" void TrackableBehaviour_OnTrackerUpdate_m617 (TrackableBehaviour_t44 * __this, int32_t ___newStatus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(187);
		Enumerator_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1021);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1_GetEnumerator_m4296_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		Enumerator_get_Current_m4297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		Enumerator_MoveNext_m4298_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t793  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___mStatus_6);
		V_0 = L_0;
		int32_t L_1 = ___newStatus;
		__this->___mStatus_6 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = ___newStatus;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		List_1_t566 * L_4 = (__this->___mTrackableEventHandlers_8);
		NullCheck(L_4);
		Enumerator_t793  L_5 = List_1_GetEnumerator_m4296(L_4, /*hidden argument*/List_1_GetEnumerator_m4296_MethodInfo_var);
		V_2 = L_5;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0020:
		{
			Object_t * L_6 = Enumerator_get_Current_m4297((&V_2), /*hidden argument*/Enumerator_get_Current_m4297_MethodInfo_var);
			V_1 = L_6;
			Object_t * L_7 = V_1;
			int32_t L_8 = V_0;
			int32_t L_9 = ___newStatus;
			NullCheck(L_7);
			InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t178_il2cpp_TypeInfo_var, L_7, L_8, L_9);
		}

IL_0030:
		{
			bool L_10 = Enumerator_MoveNext_m4298((&V_2), /*hidden argument*/Enumerator_MoveNext_m4298_MethodInfo_var);
			if (L_10)
			{
				goto IL_0020;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t793_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t793_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0049:
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnFrameIndexUpdate(System.Int32)
extern "C" void TrackableBehaviour_OnFrameIndexUpdate_m597 (TrackableBehaviour_t44 * __this, int32_t ___newFrameIndex, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable()
// System.Void Vuforia.TrackableBehaviour::Start()
extern "C" void TrackableBehaviour_Start_m2658 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour_t56 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3963(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m386(NULL /*static, unused*/, L_0, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t56 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m3963(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m3953(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t2 * L_4 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4299(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnDisable()
extern TypeInfo* ITrackableEventHandler_t178_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t793_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4296_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4297_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4298_MethodInfo_var;
extern "C" void TrackableBehaviour_OnDisable_m2659 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(187);
		Enumerator_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1021);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1_GetEnumerator_m4296_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		Enumerator_get_Current_m4297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		Enumerator_MoveNext_m4298_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t793  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___mStatus_6);
		V_0 = L_0;
		__this->___mStatus_6 = (-1);
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0049;
		}
	}
	{
		List_1_t566 * L_2 = (__this->___mTrackableEventHandlers_8);
		NullCheck(L_2);
		Enumerator_t793  L_3 = List_1_GetEnumerator_m4296(L_2, /*hidden argument*/List_1_GetEnumerator_m4296_MethodInfo_var);
		V_2 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0020:
		{
			Object_t * L_4 = Enumerator_get_Current_m4297((&V_2), /*hidden argument*/Enumerator_get_Current_m4297_MethodInfo_var);
			V_1 = L_4;
			Object_t * L_5 = V_1;
			int32_t L_6 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t178_il2cpp_TypeInfo_var, L_5, L_6, (-1));
		}

IL_0030:
		{
			bool L_7 = Enumerator_MoveNext_m4298((&V_2), /*hidden argument*/Enumerator_MoveNext_m4298_MethodInfo_var);
			if (L_7)
			{
				goto IL_0020;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t793_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t793_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0049:
	{
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.CorrectScale()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m545 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl() */, __this);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl()
extern "C" bool TrackableBehaviour_CorrectScaleImpl_m628 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetNameForTrackable(System.String)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m546 (TrackableBehaviour_t44 * __this, String_t* ___trackableName, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mTrackable_7);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___trackableName;
		__this->___mTrackableName_2 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// UnityEngine.Vector3 Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreviousScale()
extern "C" Vector3_t15  TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m548 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mPreviousScale_3);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreviousScale(UnityEngine.Vector3)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m549 (TrackableBehaviour_t44 * __this, Vector3_t15  ___previousScale, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Vector3_t15  L_1 = ___previousScale;
		__this->___mPreviousScale_3 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreserveChildSize()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m550 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mPreserveChildSize_4);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreserveChildSize(System.Boolean)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m551 (TrackableBehaviour_t44 * __this, bool ___preserveChildSize, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ___preserveChildSize;
		__this->___mPreserveChildSize_4 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_InitializedInEditor()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m552 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitializedInEditor_5);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetInitializedInEditor(System.Boolean)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m553 (TrackableBehaviour_t44 * __this, bool ___initializedInEditor, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ___initializedInEditor;
		__this->___mInitializedInEditor_5 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.UnregisterTrackable()
extern "C" void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m554 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(23 /* System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable() */, __this);
		return;
	}
}
// UnityEngine.Renderer Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.GetRenderer()
extern const MethodInfo* Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var;
extern "C" Renderer_t8 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m559 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t8 * L_0 = Component_GetComponent_TisRenderer_t8_m261(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t8_m261_MethodInfo_var);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::.ctor()
extern TypeInfo* List_1_t566_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4300_MethodInfo_var;
extern "C" void TrackableBehaviour__ctor_m2660 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		List_1__ctor_m4300_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mTrackableName_2 = (String_t*) &_stringLiteral122;
		Vector3_t15  L_0 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mPreviousScale_3 = L_0;
		List_1_t566 * L_1 = (List_1_t566 *)il2cpp_codegen_object_new (List_1_t566_il2cpp_TypeInfo_var);
		List_1__ctor_m4300(L_1, /*hidden argument*/List_1__ctor_m4300_MethodInfo_var);
		__this->___mTrackableEventHandlers_8 = L_1;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2661 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2662 (TrackableBehaviour_t44 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m240(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2663 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2664 (TrackableBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBehMethodDeclarations.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder.h"
// Vuforia.ReconstructionImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionImpl.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0MethodDeclarations.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerMethodDeclarations.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0MethodDeclarations.h"
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilderMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// Vuforia.ReconstructionImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionImplMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
struct TrackerManager_t728;
struct SmartTerrainTracker_t674;
struct TrackerManager_t728;
struct Object_t;
// Declaration T Vuforia.TrackerManager::GetTracker<System.Object>()
// T Vuforia.TrackerManager::GetTracker<System.Object>()
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.SmartTerrainTracker>()
struct Enumerable_t132;
struct IEnumerable_1_t762;
struct ReconstructionAbstractBehaviour_t68;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t132;
struct IEnumerable_1_t136;
struct Object_t;
// Declaration System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C" bool Enumerable_Contains_TisObject_t_m4304_gshared (Object_t * __this /* static, unused */, Object_t* p0, Object_t * p1, const MethodInfo* method);
#define Enumerable_Contains_TisObject_t_m4304(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, Object_t *, const MethodInfo*))Enumerable_Contains_TisObject_t_m4304_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Boolean System.Linq.Enumerable::Contains<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
// System.Boolean System.Linq.Enumerable::Contains<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, ReconstructionAbstractBehaviour_t68 *, const MethodInfo*))Enumerable_Contains_TisObject_t_m4304_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void Vuforia.DataSetTrackableBehaviour::OnDrawGizmos()
extern "C" void DataSetTrackableBehaviour_OnDrawGizmos_m588 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	Vector3_t15  V_1 = {0};
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		if (!L_0)
		{
			goto IL_01f4;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_1 = (__this->___mReconstructionToInitialize_12);
		bool L_2 = Object_op_Inequality_m386(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_01f4;
		}
	}
	{
		bool L_3 = (__this->___mAutoSetOccluderFromTargetSize_19);
		if (L_3)
		{
			goto IL_01f4;
		}
	}
	{
		bool L_4 = (__this->___mIsSmartTerrainOccluderOffset_15);
		if (!L_4)
		{
			goto IL_008d;
		}
	}
	{
		GameObject_t2 * L_5 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m273(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t13  L_7 = Transform_get_rotation_m245(L_6, /*hidden argument*/NULL);
		Vector3_t15  L_8 = (__this->___mSmartTerrainOccluderOffset_16);
		Vector3_t15  L_9 = Quaternion_op_Multiply_m378(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		GameObject_t2 * L_10 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m273(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t15  L_12 = Transform_get_position_m247(L_11, /*hidden argument*/NULL);
		Vector3_t15  L_13 = V_0;
		Vector3_t15  L_14 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		GameObject_t2 * L_15 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t11 * L_16 = GameObject_get_transform_m273(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t13  L_17 = Transform_get_rotation_m245(L_16, /*hidden argument*/NULL);
		Quaternion_t13  L_18 = (__this->___mSmartTerrainOccluderRotation_17);
		Quaternion_t13  L_19 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t15  L_20 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t156  L_21 = Matrix4x4_TRS_m499(NULL /*static, unused*/, L_14, L_19, L_20, /*hidden argument*/NULL);
		Gizmos_set_matrix_m500(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_008d:
	{
		GameObject_t2 * L_22 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t11 * L_23 = GameObject_get_transform_m273(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t15  L_24 = Transform_get_position_m247(L_23, /*hidden argument*/NULL);
		GameObject_t2 * L_25 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t11 * L_26 = GameObject_get_transform_m273(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Quaternion_t13  L_27 = Transform_get_rotation_m245(L_26, /*hidden argument*/NULL);
		Vector3_t15  L_28 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t156  L_29 = Matrix4x4_TRS_m499(NULL /*static, unused*/, L_24, L_27, L_28, /*hidden argument*/NULL);
		Gizmos_set_matrix_m500(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		Color_t90  L_30 = Color_get_white_m2085(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Vector3_t15  L_31 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t15  L_32 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t15  L_33 = Vector3_op_Subtraction_m275(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_1 = L_33;
		Vector3_t15  L_34 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t15  L_35 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t15  L_36 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t15  L_37 = Vector3_op_Division_m4306(NULL /*static, unused*/, L_36, (2.0f), /*hidden argument*/NULL);
		Vector3_t15  L_38 = V_1;
		Gizmos_DrawWireCube_m4307(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		float L_39 = ((&V_1)->___x_1);
		float L_40 = ((&V_1)->___y_2);
		float L_41 = ((&V_1)->___z_3);
		V_2 = ((float)((float)((float)((float)((float)((float)L_39+(float)L_40))+(float)L_41))/(float)(2.0f)));
		Color_t90  L_42 = Color_get_gray_m4308(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0178;
	}

IL_0125:
	{
		float L_43 = V_2;
		int32_t L_44 = V_3;
		float L_45 = V_2;
		V_4 = ((float)((float)((-L_43))+(float)((float)((float)((float)((float)(((float)L_44))*(float)L_45))/(float)(2.5f)))));
		float L_46 = V_2;
		float L_47 = V_4;
		Vector3_t15  L_48 = {0};
		Vector3__ctor_m249(&L_48, ((-L_46)), (0.0f), L_47, /*hidden argument*/NULL);
		float L_49 = V_2;
		float L_50 = V_4;
		Vector3_t15  L_51 = {0};
		Vector3__ctor_m249(&L_51, L_49, (0.0f), L_50, /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		float L_52 = V_4;
		float L_53 = V_2;
		Vector3_t15  L_54 = {0};
		Vector3__ctor_m249(&L_54, L_52, (0.0f), ((-L_53)), /*hidden argument*/NULL);
		float L_55 = V_4;
		float L_56 = V_2;
		Vector3_t15  L_57 = {0};
		Vector3__ctor_m249(&L_57, L_55, (0.0f), L_56, /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_54, L_57, /*hidden argument*/NULL);
		int32_t L_58 = V_3;
		V_3 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0178:
	{
		int32_t L_59 = V_3;
		if ((((int32_t)L_59) <= ((int32_t)5)))
		{
			goto IL_0125;
		}
	}
	{
		float L_60 = V_2;
		V_5 = ((float)((float)L_60/(float)(5.0f)));
		Color_t90  L_61 = Color_get_red_m4309(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		Vector3_t15  L_62 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = V_5;
		Vector3_t15  L_64 = {0};
		Vector3__ctor_m249(&L_64, L_63, (0.0f), (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_62, L_64, /*hidden argument*/NULL);
		Color_t90  L_65 = Color_get_green_m480(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		Vector3_t15  L_66 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_67 = V_5;
		Vector3_t15  L_68 = {0};
		Vector3__ctor_m249(&L_68, (0.0f), L_67, (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_66, L_68, /*hidden argument*/NULL);
		Color_t90  L_69 = Color_get_blue_m4310(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Vector3_t15  L_70 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = V_5;
		Vector3_t15  L_72 = {0};
		Vector3__ctor_m249(&L_72, (0.0f), (0.0f), L_71, /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
	}

IL_01f4:
	{
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
extern TypeInfo* ReconstructionFromTargetImpl_t581_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorReconstructionBehaviour_t187_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern const MethodInfo* Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var;
extern "C" void DataSetTrackableBehaviour_OnTrackerUpdate_m560 (DataSetTrackableBehaviour_t567 * __this, int32_t ___newStatus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionFromTargetImpl_t581_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1024);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IEditorReconstructionBehaviour_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(230);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	ReconstructionFromTargetImpl_t581 * V_1 = {0};
	Vector3_t15  V_2 = {0};
	Vector3_t15  V_3 = {0};
	SmartTerrainTracker_t674 * V_4 = {0};
	{
		int32_t L_0 = (((TrackableBehaviour_t44 *)__this)->___mStatus_6);
		V_0 = 0;
		bool L_1 = (__this->___mInitializeSmartTerrain_11);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_2 = (__this->___mReconstructionToInitialize_12);
		bool L_3 = Object_op_Inequality_m386(NULL /*static, unused*/, L_2, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = ___newStatus;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_5 = ___newStatus;
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_0029;
		}
	}

IL_0027:
	{
		V_0 = 1;
	}

IL_0029:
	{
		int32_t L_6 = ___newStatus;
		TrackableBehaviour_OnTrackerUpdate_m617(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_8 = (__this->___mReconstructionToInitialize_12);
		bool L_9 = Object_op_Inequality_m386(NULL /*static, unused*/, L_8, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_10 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_10);
		Object_t * L_11 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2758(L_10, /*hidden argument*/NULL);
		V_1 = ((ReconstructionFromTargetImpl_t581 *)Castclass(L_11, ReconstructionFromTargetImpl_t581_il2cpp_TypeInfo_var));
		ReconstructionFromTargetImpl_t581 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetImpl_t581 * L_13 = V_1;
		NullCheck(L_13);
		bool L_14 = ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m2755(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00eb;
		}
	}
	{
		Object_t * L_15 = (((TrackableBehaviour_t44 *)__this)->___mTrackable_7);
		if (!L_15)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetImpl_t581 * L_16 = V_1;
		NullCheck(L_16);
		Object_t * L_17 = (Object_t *)VirtFuncInvoker2< Object_t *, Vector3_t15 *, Vector3_t15 * >::Invoke(21 /* Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&) */, L_16, (&V_2), (&V_3));
		Object_t * L_18 = (((TrackableBehaviour_t44 *)__this)->___mTrackable_7);
		if ((((Object_t*)(Object_t *)L_17) == ((Object_t*)(Object_t *)L_18)))
		{
			goto IL_008b;
		}
	}
	{
		DataSetTrackableBehaviour_SetAsSmartTerrainInitializationTarget_m2665(__this, /*hidden argument*/NULL);
		return;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_19 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		SmartTerrainTracker_t674 * L_20 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_19);
		V_4 = L_20;
		SmartTerrainTracker_t674 * L_21 = V_4;
		if (!L_21)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_22 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_22);
		ReconstructionAbstractBehaviour_t68 * L_23 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2757(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.IEditorReconstructionBehaviour::get_AutomaticStart() */, IEditorReconstructionBehaviour_t187_il2cpp_TypeInfo_var, L_23);
		if (!L_24)
		{
			goto IL_00eb;
		}
	}
	{
		SmartTerrainTracker_t674 * L_25 = V_4;
		NullCheck(L_25);
		SmartTerrainBuilder_t583 * L_26 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_25);
		NullCheck(L_26);
		Object_t* L_27 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_26);
		ReconstructionFromTargetAbstractBehaviour_t70 * L_28 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_28);
		ReconstructionAbstractBehaviour_t68 * L_29 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2757(L_28, /*hidden argument*/NULL);
		bool L_30 = Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var);
		if (!L_30)
		{
			goto IL_00d3;
		}
	}
	{
		ReconstructionFromTargetImpl_t581 * L_31 = V_1;
		NullCheck(L_31);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.ReconstructionImpl::Start() */, L_31);
		return;
	}

IL_00d3:
	{
		SmartTerrainTracker_t674 * L_32 = V_4;
		NullCheck(L_32);
		SmartTerrainBuilder_t583 * L_33 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_32);
		ReconstructionFromTargetAbstractBehaviour_t70 * L_34 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_34);
		ReconstructionAbstractBehaviour_t68 * L_35 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2757(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtFuncInvoker1< bool, ReconstructionAbstractBehaviour_t68 * >::Invoke(8 /* System.Boolean Vuforia.SmartTerrainBuilder::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour) */, L_33, L_35);
	}

IL_00eb:
	{
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::SetAsSmartTerrainInitializationTarget()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSetTrackableBehaviour_SetAsSmartTerrainInitializationTarget_m2665 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t15  V_0 = {0};
	Vector3_t15  V_1 = {0};
	Object_t * V_2 = {0};
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_1 = (__this->___mReconstructionToInitialize_12);
		bool L_2 = Object_op_Inequality_m386(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005b;
		}
	}
	{
		bool L_3 = (__this->___mAutoSetOccluderFromTargetSize_19);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		VirtActionInvoker2< Vector3_t15 *, Vector3_t15 * >::Invoke(51 /* System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&) */, __this, (&V_0), (&V_1));
		Vector3_t15  L_4 = V_0;
		__this->___mSmartTerrainOccluderBoundsMin_13 = L_4;
		Vector3_t15  L_5 = V_1;
		__this->___mSmartTerrainOccluderBoundsMax_14 = L_5;
	}

IL_0036:
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_6 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_6);
		Object_t * L_7 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2758(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Object_t * L_8 = V_2;
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		Object_t * L_9 = V_2;
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void Vuforia.DataSetTrackableBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget) */, __this, L_9);
		ReconstructionFromTargetAbstractBehaviour_t70 * L_10 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_10);
		ReconstructionFromTargetAbstractBehaviour_Initialize_m2761(L_10, /*hidden argument*/NULL);
		return 1;
	}

IL_0059:
	{
		return 0;
	}

IL_005b:
	{
		String_t* L_11 = TrackableBehaviour_get_TrackableName_m544(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral123, L_11, (String_t*) &_stringLiteral124, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return 0;
	}
}
// System.String Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_DataSetName()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" String_t* DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m564 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = (__this->___mDataSetPath_9);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		String_t* L_1 = QCARRuntimeUtilities_StripFileNameFromPath_m4176(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = (__this->___mDataSetPath_9);
		String_t* L_3 = QCARRuntimeUtilities_StripExtensionFromPath_m4177(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2207(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
		String_t* L_8 = V_0;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2207(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		NullCheck(L_8);
		String_t* L_12 = String_Remove_m4311(L_8, ((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}

IL_0036:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.String Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_DataSetPath()
extern "C" String_t* DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m565 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDataSetPath_9);
		return L_0;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetDataSetPath(System.String)
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m566 (DataSetTrackableBehaviour_t567 * __this, String_t* ___dataSetPath, const MethodInfo* method)
{
	{
		Object_t * L_0 = (((TrackableBehaviour_t44 *)__this)->___mTrackable_7);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___dataSetPath;
		__this->___mDataSetPath_9 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_ExtendedTracking()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m567 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mExtendedTracking_10);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetExtendedTracking(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m568 (DataSetTrackableBehaviour_t567 * __this, bool ___extendedTracking, const MethodInfo* method)
{
	{
		bool L_0 = ___extendedTracking;
		__this->___mExtendedTracking_10 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_InitializeSmartTerrain()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m569 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetInitializeSmartTerrain(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m570 (DataSetTrackableBehaviour_t567 * __this, bool ___initializeSmartTerrain, const MethodInfo* method)
{
	{
		bool L_0 = ___initializeSmartTerrain;
		__this->___mInitializeSmartTerrain_11 = L_0;
		return;
	}
}
// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_ReconstructionToInitialize()
extern "C" ReconstructionFromTargetAbstractBehaviour_t70 * DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m571 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_0 = (__this->___mReconstructionToInitialize_12);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetReconstructionToInitialize(Vuforia.ReconstructionFromTargetAbstractBehaviour)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m572 (DataSetTrackableBehaviour_t567 * __this, ReconstructionFromTargetAbstractBehaviour_t70 * ___reconstructionToInitialize, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour_t70 * L_0 = ___reconstructionToInitialize;
		__this->___mReconstructionToInitialize_12 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderBoundsMin()
extern "C" Vector3_t15  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m573 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderBoundsMin(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m574 (DataSetTrackableBehaviour_t567 * __this, Vector3_t15  ___occluderBoundsMin, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___occluderBoundsMin;
		__this->___mSmartTerrainOccluderBoundsMin_13 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderBoundsMax()
extern "C" Vector3_t15  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m575 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderBoundsMax(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m576 (DataSetTrackableBehaviour_t567 * __this, Vector3_t15  ___occluderBoundsMax, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___occluderBoundsMax;
		__this->___mSmartTerrainOccluderBoundsMax_14 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_IsSmartTerrainOccluderOffset()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m577 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsSmartTerrainOccluderOffset_15);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetIsSmartTerrainOccluderOffset(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m578 (DataSetTrackableBehaviour_t567 * __this, bool ___isOffset, const MethodInfo* method)
{
	{
		bool L_0 = ___isOffset;
		__this->___mIsSmartTerrainOccluderOffset_15 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderOffset()
extern "C" Vector3_t15  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m579 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mSmartTerrainOccluderOffset_16);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderOffset(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m580 (DataSetTrackableBehaviour_t567 * __this, Vector3_t15  ___occluderOffset, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___occluderOffset;
		__this->___mSmartTerrainOccluderOffset_16 = L_0;
		return;
	}
}
// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderRotation()
extern "C" Quaternion_t13  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m581 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		Quaternion_t13  L_0 = (__this->___mSmartTerrainOccluderRotation_17);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderRotation(UnityEngine.Quaternion)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m582 (DataSetTrackableBehaviour_t567 * __this, Quaternion_t13  ___occluderRotation, const MethodInfo* method)
{
	{
		Quaternion_t13  L_0 = ___occluderRotation;
		__this->___mSmartTerrainOccluderRotation_17 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderLockedInPlace()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m583 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mSmartTerrainOccluderLockedInPlace_18);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetLockSmartTerrainOccluderInPlace(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m584 (DataSetTrackableBehaviour_t567 * __this, bool ___lockOccluder, const MethodInfo* method)
{
	{
		bool L_0 = ___lockOccluder;
		__this->___mSmartTerrainOccluderLockedInPlace_18 = L_0;
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.GetDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m585 (DataSetTrackableBehaviour_t567 * __this, Vector3_t15 * ___boundsMin, Vector3_t15 * ___boundsMax, const MethodInfo* method)
{
	{
		Vector3_t15 * L_0 = ___boundsMin;
		Vector3_t15 * L_1 = ___boundsMax;
		VirtActionInvoker2< Vector3_t15 *, Vector3_t15 * >::Invoke(51 /* System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&) */, __this, L_0, L_1);
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_AutoSetOccluderFromTargetSize()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m586 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoSetOccluderFromTargetSize_19);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetAutoSetOccluderFromTargetSize(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m587 (DataSetTrackableBehaviour_t567 * __this, bool ___autoset, const MethodInfo* method)
{
	{
		bool L_0 = ___autoset;
		__this->___mAutoSetOccluderFromTargetSize_19 = L_0;
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// System.Void Vuforia.DataSetTrackableBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
// System.Void Vuforia.DataSetTrackableBehaviour::.ctor()
extern "C" void DataSetTrackableBehaviour__ctor_m2666 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		__this->___mDataSetPath_9 = (String_t*) &_stringLiteral122;
		TrackableBehaviour__ctor_m2660(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2667 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2668 (DataSetTrackableBehaviour_t567 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m240(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2669 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2670 (DataSetTrackableBehaviour_t567 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"


// System.Void Vuforia.ObjectTargetAbstractBehaviour::.ctor()
extern "C" void ObjectTargetAbstractBehaviour__ctor_m454 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		DataSetTrackableBehaviour__ctor_m2666(__this, /*hidden argument*/NULL);
		__this->___mAspectRatioXY_21 = (1.0f);
		__this->___mAspectRatioXZ_22 = (1.0f);
		return;
	}
}
// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::get_ObjectTarget()
extern "C" Object_t * ObjectTargetAbstractBehaviour_get_ObjectTarget_m2671 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mObjectTarget_20);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::OnDrawGizmos()
extern "C" void ObjectTargetAbstractBehaviour_OnDrawGizmos_m2672 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		DataSetTrackableBehaviour_OnDrawGizmos_m588(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___mShowBoundingBox_23);
		if (!L_0)
		{
			goto IL_02b2;
		}
	}
	{
		GameObject_t2 * L_1 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m273(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t15  L_3 = Transform_get_position_m247(L_2, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t11 * L_5 = GameObject_get_transform_m273(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t13  L_6 = Transform_get_rotation_m245(L_5, /*hidden argument*/NULL);
		GameObject_t2 * L_7 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m273(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t15  L_9 = Transform_get_localScale_m339(L_8, /*hidden argument*/NULL);
		Matrix4x4_t156  L_10 = Matrix4x4_TRS_m499(NULL /*static, unused*/, L_3, L_6, L_9, /*hidden argument*/NULL);
		Gizmos_set_matrix_m500(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Color_t90  L_11 = {0};
		Color__ctor_m241(&L_11, (0.2f), (0.6f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector3_t15 * L_12 = &(__this->___mBBoxMax_25);
		float L_13 = (L_12->___y_2);
		Vector3_t15 * L_14 = &(__this->___mBBoxMax_25);
		float L_15 = (L_14->___x_1);
		Vector3_t15 * L_16 = &(__this->___mBBoxMax_25);
		float L_17 = (L_16->___z_3);
		Vector3_t15 * L_18 = &(__this->___mBBoxMax_25);
		float L_19 = (L_18->___x_1);
		Vector3__ctor_m249((&V_0), (1.0f), ((float)((float)L_13/(float)L_15)), ((float)((float)L_17/(float)L_19)), /*hidden argument*/NULL);
		float L_20 = ((&V_0)->___x_1);
		float L_21 = ((&V_0)->___y_2);
		float L_22 = ((&V_0)->___z_3);
		Vector3_t15  L_23 = {0};
		Vector3__ctor_m249(&L_23, ((float)((float)L_20/(float)(2.0f))), ((float)((float)L_21/(float)(2.0f))), ((float)((float)L_22/(float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t15  L_24 = V_0;
		Gizmos_DrawWireCube_m4307(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Color_t90  L_25 = Color_get_black_m2116(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_1 = (10.0f);
		Vector3_t15 * L_26 = &(__this->___mBBoxMax_25);
		float L_27 = (L_26->___x_1);
		float L_28 = V_1;
		V_2 = (((int32_t)((float)((float)L_27/(float)L_28))));
		Vector3_t15 * L_29 = &(__this->___mBBoxMax_25);
		float L_30 = (L_29->___z_3);
		float L_31 = V_1;
		V_3 = (((int32_t)((float)((float)L_30/(float)L_31))));
		float L_32 = V_1;
		float L_33 = ((&V_0)->___x_1);
		Vector3_t15 * L_34 = &(__this->___mBBoxMax_25);
		float L_35 = (L_34->___x_1);
		V_4 = ((float)((float)L_32*(float)((float)((float)L_33/(float)L_35))));
		float L_36 = V_1;
		float L_37 = ((&V_0)->___z_3);
		Vector3_t15 * L_38 = &(__this->___mBBoxMax_25);
		float L_39 = (L_38->___z_3);
		V_5 = ((float)((float)L_36*(float)((float)((float)L_37/(float)L_39))));
		V_6 = 0;
		goto IL_016d;
	}

IL_0136:
	{
		int32_t L_40 = V_6;
		float L_41 = V_4;
		Vector3_t15  L_42 = {0};
		Vector3__ctor_m249(&L_42, ((float)((float)(((float)L_40))*(float)L_41)), (0.0f), (0.0f), /*hidden argument*/NULL);
		int32_t L_43 = V_6;
		float L_44 = V_4;
		float L_45 = ((&V_0)->___z_3);
		Vector3_t15  L_46 = {0};
		Vector3__ctor_m249(&L_46, ((float)((float)(((float)L_43))*(float)L_44)), (0.0f), L_45, /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_42, L_46, /*hidden argument*/NULL);
		int32_t L_47 = V_6;
		V_6 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_48 = V_6;
		int32_t L_49 = V_2;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_0136;
		}
	}
	{
		V_7 = 0;
		goto IL_01ae;
	}

IL_0177:
	{
		int32_t L_50 = V_7;
		float L_51 = V_5;
		Vector3_t15  L_52 = {0};
		Vector3__ctor_m249(&L_52, (0.0f), (0.0f), ((float)((float)(((float)L_50))*(float)L_51)), /*hidden argument*/NULL);
		float L_53 = ((&V_0)->___x_1);
		int32_t L_54 = V_7;
		float L_55 = V_5;
		Vector3_t15  L_56 = {0};
		Vector3__ctor_m249(&L_56, L_53, (0.0f), ((float)((float)(((float)L_54))*(float)L_55)), /*hidden argument*/NULL);
		Gizmos_DrawLine_m502(NULL /*static, unused*/, L_52, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_7;
		V_7 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_01ae:
	{
		int32_t L_58 = V_7;
		int32_t L_59 = V_3;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0177;
		}
	}
	{
		Color_t90  L_60 = {0};
		Color__ctor_m241(&L_60, (1.0f), (1.0f), (1.0f), (0.8f), /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		float L_61 = ((&V_0)->___x_1);
		float L_62 = ((&V_0)->___z_3);
		Vector3_t15  L_63 = {0};
		Vector3__ctor_m249(&L_63, ((float)((float)L_61/(float)(2.0f))), (0.0f), ((float)((float)L_62/(float)(2.0f))), /*hidden argument*/NULL);
		float L_64 = ((&V_0)->___x_1);
		float L_65 = ((&V_0)->___z_3);
		Vector3_t15  L_66 = {0};
		Vector3__ctor_m249(&L_66, L_64, (0.0f), L_65, /*hidden argument*/NULL);
		Gizmos_DrawCube_m4312(NULL /*static, unused*/, L_63, L_66, /*hidden argument*/NULL);
		Color_t90  L_67 = {0};
		Color__ctor_m241(&L_67, (0.2f), (0.6f), (1.0f), (0.2f), /*hidden argument*/NULL);
		Gizmos_set_color_m501(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		float L_68 = ((&V_0)->___x_1);
		float L_69 = ((&V_0)->___y_2);
		Vector3_t15  L_70 = {0};
		Vector3__ctor_m249(&L_70, ((float)((float)L_68/(float)(2.0f))), ((float)((float)L_69/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		float L_71 = ((&V_0)->___x_1);
		float L_72 = ((&V_0)->___y_2);
		Vector3_t15  L_73 = {0};
		Vector3__ctor_m249(&L_73, L_71, L_72, (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawCube_m4312(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		float L_74 = ((&V_0)->___y_2);
		float L_75 = ((&V_0)->___z_3);
		Vector3_t15  L_76 = {0};
		Vector3__ctor_m249(&L_76, (0.0f), ((float)((float)L_74/(float)(2.0f))), ((float)((float)L_75/(float)(2.0f))), /*hidden argument*/NULL);
		float L_77 = ((&V_0)->___y_2);
		float L_78 = ((&V_0)->___z_3);
		Vector3_t15  L_79 = {0};
		Vector3__ctor_m249(&L_79, (0.0f), L_77, L_78, /*hidden argument*/NULL);
		Gizmos_DrawCube_m4312(NULL /*static, unused*/, L_76, L_79, /*hidden argument*/NULL);
	}

IL_02b2:
	{
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::CorrectScaleImpl()
extern "C" bool ObjectTargetAbstractBehaviour_CorrectScaleImpl_m637 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Vector3_t15  V_2 = {0};
	Vector3_t15  V_3 = {0};
	Vector3_t15  V_4 = {0};
	Vector3_t15  V_5 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0092;
	}

IL_0009:
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_localScale_m339(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = V_1;
		float L_3 = Vector3_get_Item_m2311((&V_2), L_2, /*hidden argument*/NULL);
		Vector3_t15 * L_4 = &(((TrackableBehaviour_t44 *)__this)->___mPreviousScale_3);
		int32_t L_5 = V_1;
		float L_6 = Vector3_get_Item_m2311(L_4, L_5, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_6)))
		{
			goto IL_008e;
		}
	}
	{
		Transform_t11 * L_7 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		Transform_t11 * L_8 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t15  L_9 = Transform_get_localScale_m339(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_1;
		float L_11 = Vector3_get_Item_m2311((&V_3), L_10, /*hidden argument*/NULL);
		Transform_t11 * L_12 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t15  L_13 = Transform_get_localScale_m339(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_1;
		float L_15 = Vector3_get_Item_m2311((&V_4), L_14, /*hidden argument*/NULL);
		Transform_t11 * L_16 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t15  L_17 = Transform_get_localScale_m339(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_1;
		float L_19 = Vector3_get_Item_m2311((&V_5), L_18, /*hidden argument*/NULL);
		Vector3_t15  L_20 = {0};
		Vector3__ctor_m249(&L_20, L_11, L_15, L_19, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2265(L_7, L_20, /*hidden argument*/NULL);
		Transform_t11 * L_21 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t15  L_22 = Transform_get_localScale_m339(L_21, /*hidden argument*/NULL);
		((TrackableBehaviour_t44 *)__this)->___mPreviousScale_3 = L_22;
		V_0 = 1;
		goto IL_0099;
	}

IL_008e:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0092:
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) < ((int32_t)3)))
		{
			goto IL_0009;
		}
	}

IL_0099:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m636 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mObjectTarget_20 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_0;
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m638 (ObjectTargetAbstractBehaviour_t64 * __this, Vector3_t15 * ___boundsMin, Vector3_t15 * ___boundsMax, const MethodInfo* method)
{
	{
		Vector3_t15 * L_0 = ___boundsMin;
		Vector3_t15  L_1 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		Vector3_t15 * L_2 = ___boundsMax;
		Vector3_t15  L_3 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_2 = L_3;
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
extern "C" void ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m639 (ObjectTargetAbstractBehaviour_t64 * __this, Object_t * ___reconstructionFromTarget, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetBoundingBox(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void ObjectTargetAbstractBehaviour_SetBoundingBox_m651 (ObjectTargetAbstractBehaviour_t64 * __this, Vector3_t15  ___minBBox, Vector3_t15  ___maxBBox, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___minBBox;
		__this->___mBBoxMin_24 = L_0;
		Vector3_t15  L_1 = ___maxBBox;
		__this->___mBBoxMax_25 = L_1;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::GetSize()
extern "C" Vector3_t15  ObjectTargetAbstractBehaviour_GetSize_m645 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((!(((float)L_0) <= ((float)(1.0f)))))
		{
			goto IL_0051;
		}
	}
	{
		Transform_t11 * L_1 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t15  L_2 = Transform_get_localScale_m339(L_1, /*hidden argument*/NULL);
		float L_3 = (L_2.___x_1);
		Transform_t11 * L_4 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t15  L_5 = Transform_get_localScale_m339(L_4, /*hidden argument*/NULL);
		float L_6 = (L_5.___x_1);
		float L_7 = (__this->___mAspectRatioXY_21);
		Transform_t11 * L_8 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t15  L_9 = Transform_get_localScale_m339(L_8, /*hidden argument*/NULL);
		float L_10 = (L_9.___x_1);
		float L_11 = (__this->___mAspectRatioXZ_22);
		Vector3_t15  L_12 = {0};
		Vector3__ctor_m249(&L_12, L_3, ((float)((float)L_6*(float)L_7)), ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}

IL_0051:
	{
		Transform_t11 * L_13 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t15  L_14 = Transform_get_localScale_m339(L_13, /*hidden argument*/NULL);
		float L_15 = (L_14.___x_1);
		float L_16 = (__this->___mAspectRatioXY_21);
		Transform_t11 * L_17 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t15  L_18 = Transform_get_localScale_m339(L_17, /*hidden argument*/NULL);
		float L_19 = (L_18.___x_1);
		Transform_t11 * L_20 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t15  L_21 = Transform_get_localScale_m339(L_20, /*hidden argument*/NULL);
		float L_22 = (L_21.___x_1);
		Vector3_t15  L_23 = {0};
		Vector3__ctor_m249(&L_23, ((float)((float)L_15/(float)L_16)), L_19, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetLength(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetLength_m646 (ObjectTargetAbstractBehaviour_t64 * __this, float ___length, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = ___length;
		float L_2 = (__this->___mAspectRatioXY_21);
		G_B3_0 = ((float)((float)L_1/(float)L_2));
		goto IL_0018;
	}

IL_0017:
	{
		float L_3 = ___length;
		G_B3_0 = L_3;
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t15  L_8 = {0};
		Vector3__ctor_m249(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2265(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetWidth(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetWidth_m647 (ObjectTargetAbstractBehaviour_t64 * __this, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___width;
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0010:
	{
		float L_2 = ___width;
		float L_3 = (__this->___mAspectRatioXY_21);
		G_B3_0 = ((float)((float)L_2/(float)L_3));
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t15  L_8 = {0};
		Vector3__ctor_m249(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2265(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetHeight(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetHeight_m648 (ObjectTargetAbstractBehaviour_t64 * __this, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXZ_22);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___height;
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0010:
	{
		float L_2 = ___height;
		float L_3 = (__this->___mAspectRatioXZ_22);
		G_B3_0 = ((float)((float)L_2/(float)L_3));
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t15  L_8 = {0};
		Vector3__ctor_m249(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2265(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXY()
extern "C" float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m642 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		return L_0;
	}
}
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXZ()
extern "C" float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m643 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXZ_22);
		return L_0;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetAspectRatio(System.Single,System.Single)
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m644 (ObjectTargetAbstractBehaviour_t64 * __this, float ___aspectRatioXY, float ___aspectRatioXZ, const MethodInfo* method)
{
	{
		Object_t * L_0 = (((TrackableBehaviour_t44 *)__this)->___mTrackable_7);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		float L_1 = ___aspectRatioXY;
		__this->___mAspectRatioXY_21 = L_1;
		float L_2 = ___aspectRatioXZ;
		__this->___mAspectRatioXZ_22 = L_2;
		return 1;
	}

IL_0018:
	{
		return 0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.InitializeObjectTarget(Vuforia.ObjectTarget)
extern TypeInfo* ObjectTarget_t568_il2cpp_TypeInfo_var;
extern TypeInfo* ExtendedTrackable_t794_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m652 (ObjectTargetAbstractBehaviour_t64 * __this, Object_t * ___objectTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectTarget_t568_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		ExtendedTrackable_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t15  V_0 = {0};
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = ___objectTarget;
		Object_t * L_1 = L_0;
		V_1 = L_1;
		__this->___mObjectTarget_20 = L_1;
		Object_t * L_2 = V_1;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_2;
		Vector3_t15  L_3 = ObjectTargetAbstractBehaviour_GetSize_m645(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		Object_t * L_4 = ___objectTarget;
		Vector3_t15  L_5 = V_0;
		NullCheck(L_4);
		InterfaceActionInvoker1< Vector3_t15  >::Invoke(1 /* System.Void Vuforia.ObjectTarget::SetSize(UnityEngine.Vector3) */, ObjectTarget_t568_il2cpp_TypeInfo_var, L_4, L_5);
		bool L_6 = (((DataSetTrackableBehaviour_t567 *)__this)->___mExtendedTracking_10);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_7 = (__this->___mObjectTarget_20);
		NullCheck(L_7);
		InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ExtendedTrackable::StartExtendedTracking() */, ExtendedTrackable_t794_il2cpp_TypeInfo_var, L_7);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetShowBoundingBox(System.Boolean)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m649 (ObjectTargetAbstractBehaviour_t64 * __this, bool ___showBoundingBox, const MethodInfo* method)
{
	{
		bool L_0 = ___showBoundingBox;
		__this->___mShowBoundingBox_23 = L_0;
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_ShowBoundingBox()
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m650 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mShowBoundingBox_23);
		return L_0;
	}
}
// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_PreviewImage()
extern "C" Texture2D_t270 * ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m640 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		Texture2D_t270 * L_0 = (__this->___mPreviewImage_26);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetPreviewImage(UnityEngine.Texture2D)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m641 (ObjectTargetAbstractBehaviour_t64 * __this, Texture2D_t270 * ___previewImage, const MethodInfo* method)
{
	{
		Texture2D_t270 * L_0 = ___previewImage;
		__this->___mPreviewImage_26 = L_0;
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m632 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m633 (ObjectTargetAbstractBehaviour_t64 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m240(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m634 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m635 (ObjectTargetAbstractBehaviour_t64 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_CameraMethodDeclarations.h"



// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusMMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0MethodDeclarations.h"



// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoMMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// Vuforia.CameraDeviceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// Vuforia.Image
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// Vuforia.CameraDeviceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceImplMethodDeclarations.h"


// Vuforia.CameraDevice Vuforia.CameraDevice::get_Instance()
extern const Il2CppType* CameraDevice_t573_0_0_0_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern "C" CameraDevice_t573 * CameraDevice_get_Instance_m2673 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t573_0_0_0_var = il2cpp_codegen_type_from_index(1017);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_0 = ((CameraDevice_t573_StaticFields*)CameraDevice_t573_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(CameraDevice_t573_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4313(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
			CameraDevice_t573 * L_3 = ((CameraDevice_t573_StaticFields*)CameraDevice_t573_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			CameraDeviceImpl_t611 * L_4 = (CameraDeviceImpl_t611 *)il2cpp_codegen_object_new (CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
			CameraDeviceImpl__ctor_m2879(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
			((CameraDevice_t573_StaticFields*)CameraDevice_t573_il2cpp_TypeInfo_var->static_fields)->___mInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_6 = ((CameraDevice_t573_StaticFields*)CameraDevice_t573_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		return L_6;
	}
}
// System.Boolean Vuforia.CameraDevice::Init(Vuforia.CameraDevice/CameraDirection)
// System.Boolean Vuforia.CameraDevice::Deinit()
// System.Boolean Vuforia.CameraDevice::Start()
// System.Boolean Vuforia.CameraDevice::Stop()
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode()
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
// System.Boolean Vuforia.CameraDevice::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
// System.Boolean Vuforia.CameraDevice::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
// System.Boolean Vuforia.CameraDevice::SetFlashTorchMode(System.Boolean)
// System.Boolean Vuforia.CameraDevice::SetFocusMode(Vuforia.CameraDevice/FocusMode)
// System.Boolean Vuforia.CameraDevice::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
// Vuforia.Image Vuforia.CameraDevice::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::GetCameraDirection()
// System.Void Vuforia.CameraDevice::.ctor()
extern "C" void CameraDevice__ctor_m2674 (CameraDevice_t573 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CameraDevice::.cctor()
extern "C" void CameraDevice__cctor_m2675 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"

// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.TargetFinder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder.h"
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_19.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// System.Action
#include "System_Core_System_Action.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerMethodDeclarations.h"
// Vuforia.TargetFinder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_19MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
struct TrackerManager_t728;
struct ObjectTracker_t574;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()


// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2676 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCloudRecoStarted_5);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C" void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2677 (CloudRecoAbstractBehaviour_t34 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		CloudRecoAbstractBehaviour_StartCloudReco_m2682(__this, /*hidden argument*/NULL);
		return;
	}

IL_000a:
	{
		CloudRecoAbstractBehaviour_StopCloudReco_m2683(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2678 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitSuccess_4);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C" void CloudRecoAbstractBehaviour_Initialize_m2679 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		ObjectTracker_t574 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t626 * L_1 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		String_t* L_2 = (__this->___AccessKey_9);
		String_t* L_3 = (__this->___SecretKey_10);
		NullCheck(L_1);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(4 /* System.Boolean Vuforia.TargetFinder::StartInit(System.String,System.String) */, L_1, L_2, L_3);
		__this->___mCurrentlyInitializing_3 = L_4;
		bool L_5 = (__this->___mCurrentlyInitializing_3);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral125, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C" void CloudRecoAbstractBehaviour_Deinitialize_m2680 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		ObjectTracker_t574 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t626 * L_1 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.TargetFinder::Deinit() */, L_1);
		__this->___mCurrentlyInitializing_3 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = (__this->___mCurrentlyInitializing_3);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral126, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		__this->___mInitSuccess_4 = 0;
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern TypeInfo* ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t795_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4318_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_CheckInitialization_m2681 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Enumerator_t795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1_GetEnumerator_m4316_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		Enumerator_get_Current_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_MoveNext_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Enumerator_t795  V_3 = {0};
	Enumerator_t795  V_4 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectTracker_t574 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t626 * L_1 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* Vuforia.TargetFinder/InitState Vuforia.TargetFinder::GetInitState() */, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_008b;
		}
	}
	{
		List_1_t575 * L_4 = (__this->___mHandlers_7);
		NullCheck(L_4);
		Enumerator_t795  L_5 = List_1_GetEnumerator_m4316(L_4, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
		V_3 = L_5;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0023:
		{
			Object_t * L_6 = Enumerator_get_Current_m4317((&V_3), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
			V_1 = L_6;
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitialized() */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_7);
		}

IL_0031:
		{
			bool L_8 = Enumerator_MoveNext_m4318((&V_3), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
			if (L_8)
			{
				goto IL_0023;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_004a:
	{
		ObjectTracker_t574 * L_9 = (__this->___mObjectTracker_2);
		NullCheck(L_9);
		TargetFinder_t626 * L_10 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_9);
		Color_t90  L_11 = (__this->___ScanlineColor_11);
		NullCheck(L_10);
		VirtActionInvoker1< Color_t90  >::Invoke(9 /* System.Void Vuforia.TargetFinder::SetUIScanlineColor(UnityEngine.Color) */, L_10, L_11);
		ObjectTracker_t574 * L_12 = (__this->___mObjectTracker_2);
		NullCheck(L_12);
		TargetFinder_t626 * L_13 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_12);
		Color_t90  L_14 = (__this->___FeaturePointColor_12);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t90  >::Invoke(10 /* System.Void Vuforia.TargetFinder::SetUIPointColor(UnityEngine.Color) */, L_13, L_14);
		__this->___mCurrentlyInitializing_3 = 0;
		__this->___mInitSuccess_4 = 1;
		CloudRecoAbstractBehaviour_StartCloudReco_m2682(__this, /*hidden argument*/NULL);
		return;
	}

IL_008b:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_00cd;
		}
	}
	{
		List_1_t575 * L_16 = (__this->___mHandlers_7);
		NullCheck(L_16);
		Enumerator_t795  L_17 = List_1_GetEnumerator_m4316(L_16, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
		V_4 = L_17;
	}

IL_009c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ad;
		}

IL_009e:
		{
			Object_t * L_18 = Enumerator_get_Current_m4317((&V_4), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
			V_2 = L_18;
			Object_t * L_19 = V_2;
			int32_t L_20 = V_0;
			NullCheck(L_19);
			InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitError(Vuforia.TargetFinder/InitState) */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_19, L_20);
		}

IL_00ad:
		{
			bool L_21 = Enumerator_MoveNext_m4318((&V_4), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
			if (L_21)
			{
				goto IL_009e;
			}
		}

IL_00b6:
		{
			IL2CPP_LEAVE(0xC6, FINALLY_00b8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_00b8;
	}

FINALLY_00b8:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_4)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_4)));
		IL2CPP_END_FINALLY(184)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(184)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_00c6:
	{
		__this->___mCurrentlyInitializing_3 = 0;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern TypeInfo* ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t795_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4318_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_StartCloudReco_m2682 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Enumerator_t795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1_GetEnumerator_m4316_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		Enumerator_get_Current_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_MoveNext_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Enumerator_t795  V_1 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectTracker_t574 * L_0 = (__this->___mObjectTracker_2);
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		bool L_1 = (__this->___mCloudRecoStarted_5);
		if (L_1)
		{
			goto IL_005c;
		}
	}
	{
		ObjectTracker_t574 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		TargetFinder_t626 * L_3 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_2);
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.TargetFinder::StartRecognition() */, L_3);
		__this->___mCloudRecoStarted_5 = L_4;
		List_1_t575 * L_5 = (__this->___mHandlers_7);
		NullCheck(L_5);
		Enumerator_t795  L_6 = List_1_GetEnumerator_m4316(L_5, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
		V_1 = L_6;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_0034:
		{
			Object_t * L_7 = Enumerator_get_Current_m4317((&V_1), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
			V_0 = L_7;
			Object_t * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker1< bool >::Invoke(3 /* System.Void Vuforia.ICloudRecoEventHandler::OnStateChanged(System.Boolean) */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_8, 1);
		}

IL_0043:
		{
			bool L_9 = Enumerator_MoveNext_m4318((&V_1), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_004c:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_1)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_1)));
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_005c:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern TypeInfo* ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t795_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4318_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_StopCloudReco_m2683 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Enumerator_t795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1_GetEnumerator_m4316_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		Enumerator_get_Current_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_MoveNext_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Enumerator_t795  V_1 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___mCloudRecoStarted_5);
		if (!L_0)
		{
			goto IL_006a;
		}
	}
	{
		ObjectTracker_t574 * L_1 = (__this->___mObjectTracker_2);
		NullCheck(L_1);
		TargetFinder_t626 * L_2 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_1);
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Vuforia.TargetFinder::Stop() */, L_2);
		__this->___mCloudRecoStarted_5 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = (__this->___mCloudRecoStarted_5);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral127, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		List_1_t575 * L_5 = (__this->___mHandlers_7);
		NullCheck(L_5);
		Enumerator_t795  L_6 = List_1_GetEnumerator_m4316(L_5, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
		V_1 = L_6;
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0051;
		}

IL_0042:
		{
			Object_t * L_7 = Enumerator_get_Current_m4317((&V_1), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
			V_0 = L_7;
			Object_t * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker1< bool >::Invoke(3 /* System.Void Vuforia.ICloudRecoEventHandler::OnStateChanged(System.Boolean) */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_8, 0);
		}

IL_0051:
		{
			bool L_9 = Enumerator_MoveNext_m4318((&V_1), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
			if (L_9)
			{
				goto IL_0042;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_1)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_1)));
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_006a:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern TypeInfo* ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var;
extern "C" void CloudRecoAbstractBehaviour_RegisterEventHandler_m2684 (CloudRecoAbstractBehaviour_t34 * __this, Object_t * ___eventHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t575 * L_0 = (__this->___mHandlers_7);
		Object_t * L_1 = ___eventHandler;
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Add(!0) */, L_0, L_1);
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Object_t * L_3 = ___eventHandler;
		NullCheck(L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitialized() */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_3);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2685 (CloudRecoAbstractBehaviour_t34 * __this, Object_t * ___eventHandler, const MethodInfo* method)
{
	{
		List_1_t575 * L_0 = (__this->___mHandlers_7);
		Object_t * L_1 = ___eventHandler;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C" void CloudRecoAbstractBehaviour_OnEnable_m2686 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mOnInitializedCalled_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (__this->___mTargetFinderStartedBeforeDisable_8);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		CloudRecoAbstractBehaviour_StartCloudReco_m2682(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnDisable_m2687 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_0 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Vuforia.QCARManager::get_Initialized() */, L_0);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		bool L_3 = (__this->___mCloudRecoStarted_5);
		__this->___mTargetFinderStartedBeforeDisable_8 = L_3;
		CloudRecoAbstractBehaviour_StopCloudReco_m2683(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_Start_m2688 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var = il2cpp_codegen_method_info_from_index(293);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		KeepAliveAbstractBehaviour_t56 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3963(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m3961(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t2 * L_2 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4299(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_4 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_4, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m397(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var };
		Action_t139 * L_9 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4138(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern TypeInfo* IEnumerable_1_t784_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t796_il2cpp_TypeInfo_var;
extern TypeInfo* ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t795_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4318_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_Update_m2689 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t784_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		IEnumerator_1_t796_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Enumerator_t795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		List_1_GetEnumerator_m4316_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		Enumerator_get_Current_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_MoveNext_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t* V_1 = {0};
	TargetSearchResult_t720  V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t* V_5 = {0};
	Enumerator_t795  V_6 = {0};
	Enumerator_t795  V_7 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___mOnInitializedCalled_6);
		if (!L_0)
		{
			goto IL_00e8;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyInitializing_3);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		CloudRecoAbstractBehaviour_CheckInitialization_m2681(__this, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		bool L_2 = (__this->___mInitSuccess_4);
		if (!L_2)
		{
			goto IL_00e8;
		}
	}
	{
		ObjectTracker_t574 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		TargetFinder_t626 * L_4 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_3);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* Vuforia.TargetFinder/UpdateState Vuforia.TargetFinder::Update() */, L_4);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00ab;
		}
	}
	{
		ObjectTracker_t574 * L_7 = (__this->___mObjectTracker_2);
		NullCheck(L_7);
		TargetFinder_t626 * L_8 = (TargetFinder_t626 *)VirtFuncInvoker0< TargetFinder_t626 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_7);
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(13 /* System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinder::GetResults() */, L_8);
		V_1 = L_9;
		Object_t* L_10 = V_1;
		NullCheck(L_10);
		Object_t* L_11 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator() */, IEnumerable_1_t784_il2cpp_TypeInfo_var, L_10);
		V_5 = L_11;
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_0055:
		{
			Object_t* L_12 = V_5;
			NullCheck(L_12);
			TargetSearchResult_t720  L_13 = (TargetSearchResult_t720 )InterfaceFuncInvoker0< TargetSearchResult_t720  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current() */, IEnumerator_1_t796_il2cpp_TypeInfo_var, L_12);
			V_2 = L_13;
			List_1_t575 * L_14 = (__this->___mHandlers_7);
			NullCheck(L_14);
			Enumerator_t795  L_15 = List_1_GetEnumerator_m4316(L_14, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
			V_6 = L_15;
		}

IL_006a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_007b;
			}

IL_006c:
			{
				Object_t * L_16 = Enumerator_get_Current_m4317((&V_6), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
				V_3 = L_16;
				Object_t * L_17 = V_3;
				TargetSearchResult_t720  L_18 = V_2;
				NullCheck(L_17);
				InterfaceActionInvoker1< TargetSearchResult_t720  >::Invoke(4 /* System.Void Vuforia.ICloudRecoEventHandler::OnNewSearchResult(Vuforia.TargetFinder/TargetSearchResult) */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_17, L_18);
			}

IL_007b:
			{
				bool L_19 = Enumerator_MoveNext_m4318((&V_6), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
				if (L_19)
				{
					goto IL_006c;
				}
			}

IL_0084:
			{
				IL2CPP_LEAVE(0x94, FINALLY_0086);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t140 *)e.ex;
			goto FINALLY_0086;
		}

FINALLY_0086:
		{ // begin finally (depth: 2)
			NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_6)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_6)));
			IL2CPP_END_FINALLY(134)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(134)
		{
			IL2CPP_JUMP_TBL(0x94, IL_0094)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
		}

IL_0094:
		{
			Object_t* L_20 = V_5;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0055;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xE8, FINALLY_009f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_009f;
	}

FINALLY_009f:
	{ // begin finally (depth: 1)
		{
			Object_t* L_22 = V_5;
			if (!L_22)
			{
				goto IL_00aa;
			}
		}

IL_00a3:
		{
			Object_t* L_23 = V_5;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_23);
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(159)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(159)
	{
		IL2CPP_JUMP_TBL(0xE8, IL_00e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_00ab:
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) >= ((int32_t)0)))
		{
			goto IL_00e8;
		}
	}
	{
		List_1_t575 * L_25 = (__this->___mHandlers_7);
		NullCheck(L_25);
		Enumerator_t795  L_26 = List_1_GetEnumerator_m4316(L_25, /*hidden argument*/List_1_GetEnumerator_m4316_MethodInfo_var);
		V_7 = L_26;
	}

IL_00bc:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cf;
		}

IL_00be:
		{
			Object_t * L_27 = Enumerator_get_Current_m4317((&V_7), /*hidden argument*/Enumerator_get_Current_m4317_MethodInfo_var);
			V_4 = L_27;
			Object_t * L_28 = V_4;
			int32_t L_29 = V_0;
			NullCheck(L_28);
			InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void Vuforia.ICloudRecoEventHandler::OnUpdateError(Vuforia.TargetFinder/UpdateState) */, ICloudRecoEventHandler_t761_il2cpp_TypeInfo_var, L_28, L_29);
		}

IL_00cf:
		{
			bool L_30 = Enumerator_MoveNext_m4318((&V_7), /*hidden argument*/Enumerator_MoveNext_m4318_MethodInfo_var);
			if (L_30)
			{
				goto IL_00be;
			}
		}

IL_00d8:
		{
			IL2CPP_LEAVE(0xE8, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_7)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t795_il2cpp_TypeInfo_var, (&V_7)));
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE8, IL_00e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_00e8:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnDestroy_m2690 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var = il2cpp_codegen_method_info_from_index(293);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_0 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Vuforia.QCARManager::get_Initialized() */, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		CloudRecoAbstractBehaviour_Deinitialize_m2680(__this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_4 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_4, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m397(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0049;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)CloudRecoAbstractBehaviour_OnQCARStarted_m2691_MethodInfo_var };
		Action_t139 * L_9 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4139(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnQCARStarted_m2691 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t574 * L_1 = (ObjectTracker_t574 *)GenericVirtFuncInvoker0< ObjectTracker_t574 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var, L_0);
		__this->___mObjectTracker_2 = L_1;
		ObjectTracker_t574 * L_2 = (__this->___mObjectTracker_2);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		CloudRecoAbstractBehaviour_Initialize_m2679(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		__this->___mOnInitializedCalled_6 = 1;
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern TypeInfo* List_1_t575_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4320_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour__ctor_m392 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t575_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1035);
		List_1__ctor_m4320_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t575 * L_0 = (List_1_t575 *)il2cpp_codegen_object_new (List_1_t575_il2cpp_TypeInfo_var);
		List_1__ctor_m4320(L_0, /*hidden argument*/List_1__ctor_m4320_MethodInfo_var);
		__this->___mHandlers_7 = L_0;
		__this->___mTargetFinderStartedBeforeDisable_8 = 1;
		__this->___AccessKey_9 = (String_t*) &_stringLiteral122;
		__this->___SecretKey_10 = (String_t*) &_stringLiteral122;
		Color_t90  L_1 = {0};
		Color__ctor_m4321(&L_1, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->___ScanlineColor_11 = L_1;
		Color_t90  L_2 = {0};
		Color__ctor_m4321(&L_2, (0.427f), (0.988f), (0.286f), /*hidden argument*/NULL);
		__this->___FeaturePointColor_12 = L_2;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"


// System.Void Vuforia.ReconstructionImpl::.ctor(System.IntPtr)
extern TypeInfo* Rect_t124_il2cpp_TypeInfo_var;
extern "C" void ReconstructionImpl__ctor_m2692 (ReconstructionImpl_t576 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t124 * L_0 = &(__this->___mMaximumArea_2);
		Initobj (Rect_t124_il2cpp_TypeInfo_var, L_0);
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___nativeReconstructionPtr;
		__this->___mNativeReconstructionPtr_0 = L_1;
		return;
	}
}
// System.IntPtr Vuforia.ReconstructionImpl::get_NativePtr()
extern "C" IntPtr_t ReconstructionImpl_get_NativePtr_m2693 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mNativeReconstructionPtr_0);
		return L_0;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::SetMaximumArea(UnityEngine.Rect)
extern const Il2CppType* RectangleData_t596_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t596_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_SetMaximumArea_m2694 (ReconstructionImpl_t576 * __this, Rect_t124  ___maximumArea, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectangleData_t596_0_0_0_var = il2cpp_codegen_type_from_index(1036);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		RectangleData_t596_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	RectangleData_t596  V_0 = {0};
	IntPtr_t V_1 = {0};
	bool V_2 = false;
	{
		float L_0 = Rect_get_xMin_m2184((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___leftTopX_0 = L_0;
		float L_1 = Rect_get_yMin_m2183((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___leftTopY_1 = L_1;
		float L_2 = Rect_get_xMax_m2175((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___rightBottomX_2 = L_2;
		float L_3 = Rect_get_yMax_m2176((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___rightBottomY_3 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(RectangleData_t596_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_6 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		RectangleData_t596  L_7 = V_0;
		RectangleData_t596  L_8 = L_7;
		Object_t * L_9 = Box(RectangleData_t596_il2cpp_TypeInfo_var, &L_8);
		IntPtr_t L_10 = V_1;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_11 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_12 = (__this->___mNativeReconstructionPtr_0);
		IntPtr_t L_13 = V_1;
		NullCheck(L_11);
		bool L_14 = (bool)InterfaceFuncInvoker2< bool, IntPtr_t, IntPtr_t >::Invoke(82 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_11, L_12, L_13);
		V_2 = L_14;
		IntPtr_t L_15 = V_1;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_16 = V_2;
		if (!L_16)
		{
			goto IL_0083;
		}
	}
	{
		__this->___mMaximumAreaIsSet_1 = 1;
		Rect_t124  L_17 = ___maximumArea;
		__this->___mMaximumArea_2 = L_17;
	}

IL_0083:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::GetMaximumArea(UnityEngine.Rect&)
extern "C" bool ReconstructionImpl_GetMaximumArea_m2695 (ReconstructionImpl_t576 * __this, Rect_t124 * ___rect, const MethodInfo* method)
{
	{
		Rect_t124 * L_0 = ___rect;
		Rect_t124  L_1 = (__this->___mMaximumArea_2);
		*L_0 = L_1;
		bool L_2 = (__this->___mMaximumAreaIsSet_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Stop()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_Stop_m2696 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(84 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionStop(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Start()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_Start_m2697 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(83 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionStart(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::IsReconstructing()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_IsReconstructing_m2698 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(85 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionIsReconstructing(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.ReconstructionImpl::SetNavMeshPadding(System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void ReconstructionImpl_SetNavMeshPadding_m2699 (ReconstructionImpl_t576 * __this, float ___padding, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		float L_2 = ___padding;
		NullCheck(L_0);
		InterfaceActionInvoker2< IntPtr_t, float >::Invoke(86 /* System.Void Vuforia.IQCARWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		float L_3 = ___padding;
		__this->___mNavMeshPadding_3 = L_3;
		return;
	}
}
// System.Single Vuforia.ReconstructionImpl::get_NavMeshPadding()
extern "C" float ReconstructionImpl_get_NavMeshPadding_m2700 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mNavMeshPadding_3);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionImpl::StartNavMeshUpdates()
extern "C" void ReconstructionImpl_StartNavMeshUpdates_m2701 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	{
		__this->___mNavMeshUpdatesEnabled_4 = 1;
		return;
	}
}
// System.Void Vuforia.ReconstructionImpl::StopNavMeshUpdates()
extern "C" void ReconstructionImpl_StopNavMeshUpdates_m2702 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	{
		__this->___mNavMeshUpdatesEnabled_4 = 0;
		return;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::IsNavMeshUpdating()
extern "C" bool ReconstructionImpl_IsNavMeshUpdating_m2703 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mNavMeshUpdatesEnabled_4);
		return L_0;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Reset()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t762_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t797_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" bool ReconstructionImpl_Reset_m2704 (ReconstructionImpl_t576 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		IEnumerable_1_t762_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1037);
		IEnumerator_1_t797_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	bool V_1 = false;
	SmartTerrainTracker_t674 * V_2 = {0};
	ReconstructionAbstractBehaviour_t68 * V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		SmartTerrainTracker_t674 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_3);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral128, /*hidden argument*/NULL);
		return 0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_5);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(87 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionReset(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5, L_6);
		V_1 = L_7;
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_9 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		SmartTerrainTracker_t674 * L_10 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_9);
		V_2 = L_10;
		SmartTerrainTracker_t674 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_0086;
		}
	}
	{
		SmartTerrainTracker_t674 * L_12 = V_2;
		NullCheck(L_12);
		SmartTerrainBuilder_t583 * L_13 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_12);
		NullCheck(L_13);
		Object_t* L_14 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_13);
		NullCheck(L_14);
		Object_t* L_15 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator() */, IEnumerable_1_t762_il2cpp_TypeInfo_var, L_14);
		V_4 = L_15;
	}

IL_0056:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_0058:
		{
			Object_t* L_16 = V_4;
			NullCheck(L_16);
			ReconstructionAbstractBehaviour_t68 * L_17 = (ReconstructionAbstractBehaviour_t68 *)InterfaceFuncInvoker0< ReconstructionAbstractBehaviour_t68 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current() */, IEnumerator_1_t797_il2cpp_TypeInfo_var, L_16);
			V_3 = L_17;
			ReconstructionAbstractBehaviour_t68 * L_18 = V_3;
			NullCheck(L_18);
			Object_t * L_19 = ReconstructionAbstractBehaviour_get_Reconstruction_m3967(L_18, /*hidden argument*/NULL);
			if ((!(((Object_t*)(Object_t *)L_19) == ((Object_t*)(ReconstructionImpl_t576 *)__this))))
			{
				goto IL_006f;
			}
		}

IL_0069:
		{
			ReconstructionAbstractBehaviour_t68 * L_20 = V_3;
			NullCheck(L_20);
			ReconstructionAbstractBehaviour_ClearOnReset_m3990(L_20, /*hidden argument*/NULL);
		}

IL_006f:
		{
			Object_t* L_21 = V_4;
			NullCheck(L_21);
			bool L_22 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_21);
			if (L_22)
			{
				goto IL_0058;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_007a;
	}

FINALLY_007a:
	{ // begin finally (depth: 1)
		{
			Object_t* L_23 = V_4;
			if (!L_23)
			{
				goto IL_0085;
			}
		}

IL_007e:
		{
			Object_t* L_24 = V_4;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_24);
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(122)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(122)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0086:
	{
		bool L_25 = V_1;
		return L_25;
	}
}
// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
struct GameObject_t2;
struct Renderer_t8;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m2036_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m2036(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2036_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t8_m4322(__this, method) (( Renderer_t8 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2036_gshared)(__this, method)
struct GameObject_t2;
struct Collider_t157;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t157_m4323(__this, method) (( Collider_t157 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2036_gshared)(__this, method)
struct GameObject_t2;
struct Camera_t3;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t3_m4324(__this, method) (( Camera_t3 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2036_gshared)(__this, method)
struct Component_t103;
struct BackgroundPlaneAbstractBehaviour_t32;
struct Component_t103;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m4326_gshared (Component_t103 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m4326(__this, method) (( Object_t * (*) (Component_t103 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4326_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t32_m4325(__this, method) (( BackgroundPlaneAbstractBehaviour_t32 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4326_gshared)(__this, method)


// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern TypeInfo* Material_t4_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t8_m4322_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t157_m4323_MethodInfo_var;
extern "C" GameObject_t2 * HideExcessAreaAbstractBehaviour_CreateQuad_m2705 (HideExcessAreaAbstractBehaviour_t48 * __this, GameObject_t2 * ___parent, String_t* ___name, Vector3_t15  ___position, Quaternion_t13  ___rotation, Vector3_t15  ___scale, int32_t ___layer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		GameObject_GetComponent_TisRenderer_t8_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483944);
		GameObject_GetComponent_TisCollider_t157_m4323_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	Material_t4 * V_1 = {0};
	Collider_t157 * V_2 = {0};
	{
		GameObject_t2 * L_0 = GameObject_CreatePrimitive_m4327(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t2 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m273(L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = ___parent;
		NullCheck(L_3);
		Transform_t11 * L_4 = GameObject_get_transform_m273(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_parent_m2285(L_2, L_4, /*hidden argument*/NULL);
		GameObject_t2 * L_5 = V_0;
		String_t* L_6 = ___name;
		NullCheck(L_5);
		Object_set_name_m2352(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t2 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m273(L_7, /*hidden argument*/NULL);
		Vector3_t15  L_9 = ___position;
		NullCheck(L_8);
		Transform_set_localPosition_m2264(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t2 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m273(L_10, /*hidden argument*/NULL);
		Quaternion_t13  L_12 = ___rotation;
		NullCheck(L_11);
		Transform_set_localRotation_m299(L_11, L_12, /*hidden argument*/NULL);
		GameObject_t2 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t11 * L_14 = GameObject_get_transform_m273(L_13, /*hidden argument*/NULL);
		Vector3_t15  L_15 = ___scale;
		NullCheck(L_14);
		Transform_set_localScale_m2265(L_14, L_15, /*hidden argument*/NULL);
		Shader_t152 * L_16 = (__this->___matteShader_2);
		Material_t4 * L_17 = (Material_t4 *)il2cpp_codegen_object_new (Material_t4_il2cpp_TypeInfo_var);
		Material__ctor_m4328(L_17, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		GameObject_t2 * L_18 = V_0;
		NullCheck(L_18);
		Renderer_t8 * L_19 = GameObject_GetComponent_TisRenderer_t8_m4322(L_18, /*hidden argument*/GameObject_GetComponent_TisRenderer_t8_m4322_MethodInfo_var);
		Material_t4 * L_20 = V_1;
		NullCheck(L_19);
		Renderer_set_material_m4329(L_19, L_20, /*hidden argument*/NULL);
		GameObject_t2 * L_21 = V_0;
		NullCheck(L_21);
		Collider_t157 * L_22 = GameObject_GetComponent_TisCollider_t157_m4323(L_21, /*hidden argument*/GameObject_GetComponent_TisCollider_t157_m4323_MethodInfo_var);
		V_2 = L_22;
		Collider_t157 * L_23 = V_2;
		bool L_24 = Object_op_Implicit_m397(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0072;
		}
	}
	{
		Collider_t157 * L_25 = V_2;
		Object_Destroy_m471(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_0072:
	{
		GameObject_t2 * L_26 = V_0;
		int32_t L_27 = ___layer;
		NullCheck(L_26);
		GameObject_set_layer_m2255(L_26, L_27, /*hidden argument*/NULL);
		GameObject_t2 * L_28 = V_0;
		return L_28;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2706 (HideExcessAreaAbstractBehaviour_t48 * __this, bool ___activeflag, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = (__this->___mLeftPlane_5);
		bool L_1 = ___activeflag;
		NullCheck(L_0);
		GameObject_SetActive_m238(L_0, L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_2 = (__this->___mRightPlane_6);
		bool L_3 = ___activeflag;
		NullCheck(L_2);
		GameObject_SetActive_m238(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = (__this->___mTopPlane_7);
		bool L_5 = ___activeflag;
		NullCheck(L_4);
		GameObject_SetActive_m238(L_4, L_5, /*hidden argument*/NULL);
		GameObject_t2 * L_6 = (__this->___mBottomPlane_8);
		bool L_7 = ___activeflag;
		NullCheck(L_6);
		GameObject_SetActive_m238(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnQCARStarted()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t3_m4324_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t32_m4325_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		GameObject_GetComponent_TisCamera_t3_m4324_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t32_m4325_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483947);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m386(NULL /*static, unused*/, L_2, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0125;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = QCARAbstractBehaviour_get_SceneScaleFactor_m4118(L_4, /*hidden argument*/NULL);
		__this->___mSceneIsScaledDown_10 = ((((float)L_5) < ((float)(1.0f)))? 1 : 0);
		bool L_6 = (__this->___mSceneIsScaledDown_10);
		if (!L_6)
		{
			goto IL_0125;
		}
	}
	{
		GameObject_t2 * L_7 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Camera_t3 * L_8 = GameObject_GetComponent_TisCamera_t3_m4324(L_7, /*hidden argument*/GameObject_GetComponent_TisCamera_t3_m4324_MethodInfo_var);
		__this->___mCamera_9 = L_8;
		QCARAbstractBehaviour_t67 * L_9 = V_0;
		NullCheck(L_9);
		BackgroundPlaneAbstractBehaviour_t32 * L_10 = Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t32_m4325(L_9, /*hidden argument*/Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t32_m4325_MethodInfo_var);
		NullCheck(L_10);
		GameObject_t2 * L_11 = Component_get_gameObject_m272(L_10, /*hidden argument*/NULL);
		__this->___mBgPlane_4 = L_11;
		GameObject_t2 * L_12 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Vector3_t15  L_13 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_14 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t15  L_15 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_16 = (__this->___mBgPlane_4);
		NullCheck(L_16);
		int32_t L_17 = GameObject_get_layer_m2254(L_16, /*hidden argument*/NULL);
		GameObject_t2 * L_18 = HideExcessAreaAbstractBehaviour_CreateQuad_m2705(__this, L_12, (String_t*) &_stringLiteral129, L_13, L_14, L_15, L_17, /*hidden argument*/NULL);
		__this->___mLeftPlane_5 = L_18;
		GameObject_t2 * L_19 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Vector3_t15  L_20 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_21 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t15  L_22 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_23 = (__this->___mBgPlane_4);
		NullCheck(L_23);
		int32_t L_24 = GameObject_get_layer_m2254(L_23, /*hidden argument*/NULL);
		GameObject_t2 * L_25 = HideExcessAreaAbstractBehaviour_CreateQuad_m2705(__this, L_19, (String_t*) &_stringLiteral130, L_20, L_21, L_22, L_24, /*hidden argument*/NULL);
		__this->___mRightPlane_6 = L_25;
		GameObject_t2 * L_26 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Vector3_t15  L_27 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_28 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t15  L_29 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_30 = (__this->___mBgPlane_4);
		NullCheck(L_30);
		int32_t L_31 = GameObject_get_layer_m2254(L_30, /*hidden argument*/NULL);
		GameObject_t2 * L_32 = HideExcessAreaAbstractBehaviour_CreateQuad_m2705(__this, L_26, (String_t*) &_stringLiteral131, L_27, L_28, L_29, L_31, /*hidden argument*/NULL);
		__this->___mTopPlane_7 = L_32;
		GameObject_t2 * L_33 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		Vector3_t15  L_34 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_35 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t15  L_36 = Vector3_get_one_m4305(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_37 = (__this->___mBgPlane_4);
		NullCheck(L_37);
		int32_t L_38 = GameObject_get_layer_m2254(L_37, /*hidden argument*/NULL);
		GameObject_t2 * L_39 = HideExcessAreaAbstractBehaviour_CreateQuad_m2705(__this, L_33, (String_t*) &_stringLiteral132, L_34, L_35, L_36, L_38, /*hidden argument*/NULL);
		__this->___mBottomPlane_8 = L_39;
	}

IL_0125:
	{
		return;
	}
}
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2708 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		Vector3_t15  L_0 = (__this->___mBgPlaneLocalPos_11);
		GameObject_t2 * L_1 = (__this->___mBgPlane_4);
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m273(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t15  L_3 = Transform_get_localPosition_m2257(L_2, /*hidden argument*/NULL);
		bool L_4 = Vector3_op_Inequality_m2258(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		Vector3_t15  L_5 = (__this->___mBgPlaneLocalScale_12);
		GameObject_t2 * L_6 = (__this->___mBgPlane_4);
		NullCheck(L_6);
		Transform_t11 * L_7 = GameObject_get_transform_m273(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t15  L_8 = Transform_get_localScale_m339(L_7, /*hidden argument*/NULL);
		bool L_9 = Vector3_op_Inequality_m2258(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0065;
		}
	}
	{
		float L_10 = (__this->___mCameraNearPlane_13);
		Camera_t3 * L_11 = (__this->___mCamera_9);
		NullCheck(L_11);
		float L_12 = Camera_get_nearClipPlane_m2038(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) == ((float)L_12))))
		{
			goto IL_0065;
		}
	}
	{
		Rect_t124  L_13 = (__this->___mCameraPixelRect_14);
		Camera_t3 * L_14 = (__this->___mCamera_9);
		NullCheck(L_14);
		Rect_t124  L_15 = Camera_get_pixelRect_m4291(L_14, /*hidden argument*/NULL);
		bool L_16 = Rect_op_Inequality_m4330(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_16));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 1;
	}

IL_0066:
	{
		V_0 = G_B5_0;
		bool L_17 = V_0;
		if (!L_17)
		{
			goto IL_00b8;
		}
	}
	{
		GameObject_t2 * L_18 = (__this->___mBgPlane_4);
		NullCheck(L_18);
		Transform_t11 * L_19 = GameObject_get_transform_m273(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t15  L_20 = Transform_get_localPosition_m2257(L_19, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalPos_11 = L_20;
		GameObject_t2 * L_21 = (__this->___mBgPlane_4);
		NullCheck(L_21);
		Transform_t11 * L_22 = GameObject_get_transform_m273(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t15  L_23 = Transform_get_localScale_m339(L_22, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalScale_12 = L_23;
		Camera_t3 * L_24 = (__this->___mCamera_9);
		NullCheck(L_24);
		float L_25 = Camera_get_nearClipPlane_m2038(L_24, /*hidden argument*/NULL);
		__this->___mCameraNearPlane_13 = L_25;
		Camera_t3 * L_26 = (__this->___mCamera_9);
		NullCheck(L_26);
		Rect_t124  L_27 = Camera_get_pixelRect_m4291(L_26, /*hidden argument*/NULL);
		__this->___mCameraPixelRect_14 = L_27;
	}

IL_00b8:
	{
		bool L_28 = V_0;
		return L_28;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_Start_m2709 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var = il2cpp_codegen_method_info_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		__this->___mSceneIsScaledDown_10 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m386(NULL /*static, unused*/, L_2, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var };
		Action_t139 * L_6 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4138(L_4, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral133, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m2710 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var = il2cpp_codegen_method_info_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707_MethodInfo_var };
		Action_t139 * L_6 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4139(L_4, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m2711 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Plane_t457  V_1 = {0};
	Ray_t96  V_2 = {0};
	Ray_t96  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t15  V_5 = {0};
	Vector3_t15  V_6 = {0};
	Quaternion_t13  V_7 = {0};
	Vector3_t15  V_8 = {0};
	Vector3_t15  V_9 = {0};
	Ray_t96  V_10 = {0};
	Vector3_t15  V_11 = {0};
	Vector3_t15  V_12 = {0};
	Ray_t96  V_13 = {0};
	Vector3_t15  V_14 = {0};
	Vector3_t15  V_15 = {0};
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	{
		bool L_0 = (__this->___mSceneIsScaledDown_10);
		if (!L_0)
		{
			goto IL_040d;
		}
	}
	{
		bool L_1 = HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2708(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		bool L_2 = (__this->___disableMattes_3);
		HideExcessAreaAbstractBehaviour_SetPlanesActive_m2706(__this, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_3 = (__this->___disableMattes_3);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		return;
	}

IL_002c:
	{
		float L_4 = (__this->___mCameraNearPlane_13);
		V_0 = ((float)((float)L_4+(float)(0.01f)));
		Camera_t3 * L_5 = (__this->___mCamera_9);
		NullCheck(L_5);
		Transform_t11 * L_6 = Component_get_transform_m243(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t15  L_7 = Transform_get_forward_m282(L_6, /*hidden argument*/NULL);
		Camera_t3 * L_8 = (__this->___mCamera_9);
		NullCheck(L_8);
		Transform_t11 * L_9 = Component_get_transform_m243(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t15  L_10 = Transform_get_position_m247(L_9, /*hidden argument*/NULL);
		Camera_t3 * L_11 = (__this->___mCamera_9);
		NullCheck(L_11);
		Transform_t11 * L_12 = Component_get_transform_m243(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t15  L_13 = Transform_get_forward_m282(L_12, /*hidden argument*/NULL);
		float L_14 = V_0;
		Vector3_t15  L_15 = Vector3_op_Multiply_m2363(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t15  L_16 = Vector3_op_Addition_m350(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		Plane__ctor_m2223((&V_1), L_7, L_16, /*hidden argument*/NULL);
		Camera_t3 * L_17 = (__this->___mCamera_9);
		Rect_t124 * L_18 = &(__this->___mCameraPixelRect_14);
		float L_19 = Rect_get_xMin_m2184(L_18, /*hidden argument*/NULL);
		Rect_t124 * L_20 = &(__this->___mCameraPixelRect_14);
		float L_21 = Rect_get_yMin_m2183(L_20, /*hidden argument*/NULL);
		Vector3_t15  L_22 = {0};
		Vector3__ctor_m249(&L_22, L_19, L_21, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Ray_t96  L_23 = Camera_ScreenPointToRay_m254(L_17, L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		Camera_t3 * L_24 = (__this->___mCamera_9);
		Rect_t124 * L_25 = &(__this->___mCameraPixelRect_14);
		float L_26 = Rect_get_xMax_m2175(L_25, /*hidden argument*/NULL);
		Rect_t124 * L_27 = &(__this->___mCameraPixelRect_14);
		float L_28 = Rect_get_yMax_m2176(L_27, /*hidden argument*/NULL);
		Vector3_t15  L_29 = {0};
		Vector3__ctor_m249(&L_29, L_26, L_28, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Ray_t96  L_30 = Camera_ScreenPointToRay_m254(L_24, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		V_4 = (0.0f);
		Ray_t96  L_31 = V_2;
		Plane_Raycast_m2224((&V_1), L_31, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_32 = (__this->___mCamera_9);
		NullCheck(L_32);
		Transform_t11 * L_33 = Component_get_transform_m243(L_32, /*hidden argument*/NULL);
		float L_34 = V_4;
		Vector3_t15  L_35 = Ray_GetPoint_m2225((&V_2), L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t15  L_36 = Transform_InverseTransformPoint_m2222(L_33, L_35, /*hidden argument*/NULL);
		V_5 = L_36;
		Ray_t96  L_37 = V_3;
		Plane_Raycast_m2224((&V_1), L_37, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_38 = (__this->___mCamera_9);
		NullCheck(L_38);
		Transform_t11 * L_39 = Component_get_transform_m243(L_38, /*hidden argument*/NULL);
		float L_40 = V_4;
		Vector3_t15  L_41 = Ray_GetPoint_m2225((&V_3), L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t15  L_42 = Transform_InverseTransformPoint_m2222(L_39, L_41, /*hidden argument*/NULL);
		V_6 = L_42;
		GameObject_t2 * L_43 = (__this->___mBgPlane_4);
		NullCheck(L_43);
		Transform_t11 * L_44 = GameObject_get_transform_m273(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Quaternion_t13  L_45 = Transform_get_localRotation_m297(L_44, /*hidden argument*/NULL);
		Quaternion_t13  L_46 = Quaternion_Inverse_m285(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t15  L_47 = Vector3_get_right_m2341(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_48 = Quaternion_AngleAxis_m4282(NULL /*static, unused*/, (270.0f), L_47, /*hidden argument*/NULL);
		Quaternion_t13  L_49 = Quaternion_op_Multiply_m279(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		GameObject_t2 * L_50 = (__this->___mBgPlane_4);
		NullCheck(L_50);
		Transform_t11 * L_51 = GameObject_get_transform_m273(L_50, /*hidden argument*/NULL);
		Quaternion_t13  L_52 = V_7;
		Vector3_t15  L_53 = {0};
		Vector3__ctor_m249(&L_53, (-1.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		Vector3_t15  L_54 = Quaternion_op_Multiply_m378(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t15  L_55 = Transform_TransformPoint_m2337(L_51, L_54, /*hidden argument*/NULL);
		V_8 = L_55;
		Camera_t3 * L_56 = (__this->___mCamera_9);
		Vector3_t15  L_57 = V_8;
		NullCheck(L_56);
		Vector3_t15  L_58 = Camera_WorldToScreenPoint_m4331(L_56, L_57, /*hidden argument*/NULL);
		V_9 = L_58;
		Camera_t3 * L_59 = (__this->___mCamera_9);
		Vector3_t15  L_60 = V_9;
		NullCheck(L_59);
		Ray_t96  L_61 = Camera_ScreenPointToRay_m254(L_59, L_60, /*hidden argument*/NULL);
		V_10 = L_61;
		GameObject_t2 * L_62 = (__this->___mBgPlane_4);
		NullCheck(L_62);
		Transform_t11 * L_63 = GameObject_get_transform_m273(L_62, /*hidden argument*/NULL);
		Quaternion_t13  L_64 = V_7;
		Vector3_t15  L_65 = {0};
		Vector3__ctor_m249(&L_65, (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t15  L_66 = Quaternion_op_Multiply_m378(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		Vector3_t15  L_67 = Transform_TransformPoint_m2337(L_63, L_66, /*hidden argument*/NULL);
		V_11 = L_67;
		Camera_t3 * L_68 = (__this->___mCamera_9);
		Vector3_t15  L_69 = V_11;
		NullCheck(L_68);
		Vector3_t15  L_70 = Camera_WorldToScreenPoint_m4331(L_68, L_69, /*hidden argument*/NULL);
		V_12 = L_70;
		Camera_t3 * L_71 = (__this->___mCamera_9);
		Vector3_t15  L_72 = V_12;
		NullCheck(L_71);
		Ray_t96  L_73 = Camera_ScreenPointToRay_m254(L_71, L_72, /*hidden argument*/NULL);
		V_13 = L_73;
		Ray_t96  L_74 = V_10;
		Plane_Raycast_m2224((&V_1), L_74, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_75 = (__this->___mCamera_9);
		NullCheck(L_75);
		Transform_t11 * L_76 = Component_get_transform_m243(L_75, /*hidden argument*/NULL);
		float L_77 = V_4;
		Vector3_t15  L_78 = Ray_GetPoint_m2225((&V_10), L_77, /*hidden argument*/NULL);
		NullCheck(L_76);
		Vector3_t15  L_79 = Transform_InverseTransformPoint_m2222(L_76, L_78, /*hidden argument*/NULL);
		V_14 = L_79;
		Ray_t96  L_80 = V_13;
		Plane_Raycast_m2224((&V_1), L_80, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_81 = (__this->___mCamera_9);
		NullCheck(L_81);
		Transform_t11 * L_82 = Component_get_transform_m243(L_81, /*hidden argument*/NULL);
		float L_83 = V_4;
		Vector3_t15  L_84 = Ray_GetPoint_m2225((&V_13), L_83, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t15  L_85 = Transform_InverseTransformPoint_m2222(L_82, L_84, /*hidden argument*/NULL);
		V_15 = L_85;
		float L_86 = ((&V_14)->___x_1);
		float L_87 = ((&V_5)->___x_1);
		V_16 = ((float)((float)L_86-(float)L_87));
		float L_88 = ((&V_6)->___y_2);
		float L_89 = ((&V_5)->___y_2);
		V_17 = ((float)((float)L_88-(float)L_89));
		GameObject_t2 * L_90 = (__this->___mLeftPlane_5);
		NullCheck(L_90);
		Transform_t11 * L_91 = GameObject_get_transform_m273(L_90, /*hidden argument*/NULL);
		float L_92 = ((&V_5)->___x_1);
		float L_93 = V_16;
		float L_94 = ((&V_5)->___y_2);
		float L_95 = V_17;
		float L_96 = V_0;
		Vector3_t15  L_97 = {0};
		Vector3__ctor_m249(&L_97, ((float)((float)L_92+(float)((float)((float)L_93*(float)(0.5f))))), ((float)((float)L_94+(float)((float)((float)L_95*(float)(0.5f))))), L_96, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_localPosition_m2264(L_91, L_97, /*hidden argument*/NULL);
		GameObject_t2 * L_98 = (__this->___mLeftPlane_5);
		NullCheck(L_98);
		Transform_t11 * L_99 = GameObject_get_transform_m273(L_98, /*hidden argument*/NULL);
		float L_100 = V_16;
		float L_101 = V_17;
		Vector3_t15  L_102 = {0};
		Vector3__ctor_m249(&L_102, L_100, L_101, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_99);
		Transform_set_localScale_m2265(L_99, L_102, /*hidden argument*/NULL);
		float L_103 = ((&V_6)->___x_1);
		float L_104 = ((&V_15)->___x_1);
		V_18 = ((float)((float)L_103-(float)L_104));
		float L_105 = ((&V_6)->___y_2);
		float L_106 = ((&V_5)->___y_2);
		V_19 = ((float)((float)L_105-(float)L_106));
		GameObject_t2 * L_107 = (__this->___mRightPlane_6);
		NullCheck(L_107);
		Transform_t11 * L_108 = GameObject_get_transform_m273(L_107, /*hidden argument*/NULL);
		float L_109 = ((&V_15)->___x_1);
		float L_110 = V_18;
		float L_111 = ((&V_5)->___y_2);
		float L_112 = V_19;
		float L_113 = V_0;
		Vector3_t15  L_114 = {0};
		Vector3__ctor_m249(&L_114, ((float)((float)L_109+(float)((float)((float)L_110*(float)(0.5f))))), ((float)((float)L_111+(float)((float)((float)L_112*(float)(0.5f))))), L_113, /*hidden argument*/NULL);
		NullCheck(L_108);
		Transform_set_localPosition_m2264(L_108, L_114, /*hidden argument*/NULL);
		GameObject_t2 * L_115 = (__this->___mRightPlane_6);
		NullCheck(L_115);
		Transform_t11 * L_116 = GameObject_get_transform_m273(L_115, /*hidden argument*/NULL);
		float L_117 = V_18;
		float L_118 = V_19;
		Vector3_t15  L_119 = {0};
		Vector3__ctor_m249(&L_119, L_117, L_118, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_116);
		Transform_set_localScale_m2265(L_116, L_119, /*hidden argument*/NULL);
		float L_120 = ((&V_15)->___x_1);
		float L_121 = ((&V_14)->___x_1);
		V_20 = ((float)((float)L_120-(float)L_121));
		float L_122 = ((&V_6)->___y_2);
		float L_123 = ((&V_15)->___y_2);
		V_21 = ((float)((float)L_122-(float)L_123));
		GameObject_t2 * L_124 = (__this->___mTopPlane_7);
		NullCheck(L_124);
		Transform_t11 * L_125 = GameObject_get_transform_m273(L_124, /*hidden argument*/NULL);
		float L_126 = ((&V_14)->___x_1);
		float L_127 = V_20;
		float L_128 = ((&V_15)->___y_2);
		float L_129 = V_21;
		float L_130 = V_0;
		Vector3_t15  L_131 = {0};
		Vector3__ctor_m249(&L_131, ((float)((float)L_126+(float)((float)((float)L_127*(float)(0.5f))))), ((float)((float)L_128+(float)((float)((float)L_129*(float)(0.5f))))), L_130, /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_set_localPosition_m2264(L_125, L_131, /*hidden argument*/NULL);
		GameObject_t2 * L_132 = (__this->___mTopPlane_7);
		NullCheck(L_132);
		Transform_t11 * L_133 = GameObject_get_transform_m273(L_132, /*hidden argument*/NULL);
		float L_134 = V_20;
		float L_135 = V_21;
		Vector3_t15  L_136 = {0};
		Vector3__ctor_m249(&L_136, L_134, L_135, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_133);
		Transform_set_localScale_m2265(L_133, L_136, /*hidden argument*/NULL);
		float L_137 = ((&V_15)->___x_1);
		float L_138 = ((&V_14)->___x_1);
		V_22 = ((float)((float)L_137-(float)L_138));
		float L_139 = ((&V_14)->___y_2);
		float L_140 = ((&V_5)->___y_2);
		V_23 = ((float)((float)L_139-(float)L_140));
		GameObject_t2 * L_141 = (__this->___mBottomPlane_8);
		NullCheck(L_141);
		Transform_t11 * L_142 = GameObject_get_transform_m273(L_141, /*hidden argument*/NULL);
		float L_143 = ((&V_14)->___x_1);
		float L_144 = V_22;
		float L_145 = ((&V_5)->___y_2);
		float L_146 = V_23;
		float L_147 = V_0;
		Vector3_t15  L_148 = {0};
		Vector3__ctor_m249(&L_148, ((float)((float)L_143+(float)((float)((float)L_144*(float)(0.5f))))), ((float)((float)L_145+(float)((float)((float)L_146*(float)(0.5f))))), L_147, /*hidden argument*/NULL);
		NullCheck(L_142);
		Transform_set_localPosition_m2264(L_142, L_148, /*hidden argument*/NULL);
		GameObject_t2 * L_149 = (__this->___mBottomPlane_8);
		NullCheck(L_149);
		Transform_t11 * L_150 = GameObject_get_transform_m273(L_149, /*hidden argument*/NULL);
		float L_151 = V_22;
		float L_152 = V_23;
		Vector3_t15  L_153 = {0};
		Vector3__ctor_m249(&L_153, L_151, L_152, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_150);
		Transform_set_localScale_m2265(L_150, L_153, /*hidden argument*/NULL);
	}

IL_040d:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m418 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalPos_11 = L_0;
		Vector3_t15  L_1 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalScale_12 = L_1;
		Rect_t124  L_2 = {0};
		Rect__ctor_m383(&L_2, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___mCameraPixelRect_14 = L_2;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImplMethodDeclarations.h"



// System.Void Vuforia.TrackableImpl::.ctor(System.String,System.Int32)
extern "C" void TrackableImpl__ctor_m2712 (TrackableImpl_t577 * __this, String_t* ___name, int32_t ___id, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		TrackableImpl_set_Name_m2714(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___id;
		TrackableImpl_set_ID_m2716(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Vuforia.TrackableImpl::get_Name()
extern "C" String_t* TrackableImpl_get_Name_m2713 (TrackableImpl_t577 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.TrackableImpl::set_Name(System.String)
extern "C" void TrackableImpl_set_Name_m2714 (TrackableImpl_t577 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Int32 Vuforia.TrackableImpl::get_ID()
extern "C" int32_t TrackableImpl_get_ID_m2715 (TrackableImpl_t577 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CIDU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.TrackableImpl::set_ID(System.Int32)
extern "C" void TrackableImpl_set_ID_m2716 (TrackableImpl_t577 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CIDU3Ek__BackingField_1 = L_0;
		return;
	}
}
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImplMethodDeclarations.h"

// Vuforia.DataSetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetImpl.h"
// Vuforia.DataSet
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet.h"
// Vuforia.DataSetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetImplMethodDeclarations.h"


// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern const Il2CppType* Vector3_t15_0_0_0_var;
extern TypeInfo* DataSetImpl_t578_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t15_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetImpl__ctor_m2717 (ObjectTargetImpl_t579 * __this, String_t* ___name, int32_t ___id, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t15_0_0_0_var = il2cpp_codegen_type_from_index(27);
		DataSetImpl_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Vector3_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2712(__this, L_0, L_1, /*hidden argument*/NULL);
		DataSet_t594 * L_2 = ___dataSet;
		__this->___mDataSet_3 = ((DataSetImpl_t578 *)Castclass(L_2, DataSetImpl_t578_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_7 = (__this->___mDataSet_3);
		NullCheck(L_7);
		IntPtr_t L_8 = DataSetImpl_get_DataSetPtr_m2886(L_7, /*hidden argument*/NULL);
		String_t* L_9 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_0;
		NullCheck(L_6);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(37 /* System.Int32 Vuforia.IQCARWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_6, L_8, L_9, L_10);
		IntPtr_t L_11 = V_0;
		Type_t * L_12 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_13 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		__this->___mSize_2 = ((*(Vector3_t15 *)((Vector3_t15 *)UnBox (L_13, Vector3_t15_il2cpp_TypeInfo_var))));
		IntPtr_t L_14 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
extern "C" Vector3_t15  ObjectTargetImpl_GetSize_m2718 (ObjectTargetImpl_t579 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
extern const Il2CppType* Vector3_t15_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t15_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetImpl_SetSize_m2719 (ObjectTargetImpl_t579 * __this, Vector3_t15  ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t15_0_0_0_var = il2cpp_codegen_type_from_index(27);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		Vector3_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Vector3_t15  L_0 = ___size;
		__this->___mSize_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t15  L_4 = (__this->___mSize_2);
		Vector3_t15  L_5 = L_4;
		Object_t * L_6 = Box(Vector3_t15_il2cpp_TypeInfo_var, &L_5);
		IntPtr_t L_7 = V_0;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_6, L_7, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_9 = (__this->___mDataSet_3);
		NullCheck(L_9);
		IntPtr_t L_10 = DataSetImpl_get_DataSetPtr_m2886(L_9, /*hidden argument*/NULL);
		String_t* L_11 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		IntPtr_t L_12 = V_0;
		NullCheck(L_8);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(36 /* System.Int32 Vuforia.IQCARWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_8, L_10, L_11, L_12);
		IntPtr_t L_13 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTargetImpl_StartExtendedTracking_m2720 (ObjectTargetImpl_t579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_1 = (__this->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		return ((((int32_t)L_4) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.ObjectTargetImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTargetImpl_StopExtendedTracking_m2721 (ObjectTargetImpl_t579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_1 = (__this->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		return ((((int32_t)L_4) > ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
extern "C" DataSetImpl_t578 * ObjectTargetImpl_get_DataSet_m2722 (ObjectTargetImpl_t579 * __this, const MethodInfo* method)
{
	{
		DataSetImpl_t578 * L_0 = (__this->___mDataSet_3);
		return L_0;
	}
}
// Vuforia.UnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.UnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlayerMethodDeclarations.h"

// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"


// Vuforia.IUnityPlayer Vuforia.UnityPlayer::get_Instance()
extern TypeInfo* UnityPlayer_t580_il2cpp_TypeInfo_var;
extern TypeInfo* NullUnityPlayer_t146_il2cpp_TypeInfo_var;
extern "C" Object_t * UnityPlayer_get_Instance_m2723 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityPlayer_t580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1040);
		NullUnityPlayer_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t580_il2cpp_TypeInfo_var);
		Object_t * L_0 = ((UnityPlayer_t580_StaticFields*)UnityPlayer_t580_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullUnityPlayer_t146 * L_1 = (NullUnityPlayer_t146 *)il2cpp_codegen_object_new (NullUnityPlayer_t146_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m458(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t580_il2cpp_TypeInfo_var);
		((UnityPlayer_t580_StaticFields*)UnityPlayer_t580_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0 = L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t580_il2cpp_TypeInfo_var);
		Object_t * L_2 = ((UnityPlayer_t580_StaticFields*)UnityPlayer_t580_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0;
		return L_2;
	}
}
// System.Void Vuforia.UnityPlayer::SetImplementation(Vuforia.IUnityPlayer)
extern TypeInfo* UnityPlayer_t580_il2cpp_TypeInfo_var;
extern "C" void UnityPlayer_SetImplementation_m2724 (Object_t * __this /* static, unused */, Object_t * ___implementation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityPlayer_t580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1040);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___implementation;
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t580_il2cpp_TypeInfo_var);
		((UnityPlayer_t580_StaticFields*)UnityPlayer_t580_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0 = L_0;
		return;
	}
}
// System.Void Vuforia.UnityPlayer::.cctor()
extern "C" void UnityPlayer__cctor_m2725 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"


// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C" void NullUnityPlayer_LoadNativeLibraries_m2726 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C" void NullUnityPlayer_InitializePlatform_m2727 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C" int32_t NullUnityPlayer_Start_m2728 (NullUnityPlayer_t146 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C" void NullUnityPlayer_Update_m2729 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C" void NullUnityPlayer_Dispose_m2730 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C" void NullUnityPlayer_OnPause_m2731 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C" void NullUnityPlayer_OnResume_m2732 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C" void NullUnityPlayer_OnDestroy_m2733 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C" void NullUnityPlayer__ctor_m458 (NullUnityPlayer_t146 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"

// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// Vuforia.QCARUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityMethodDeclarations.h"


// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C" void PlayModeUnityPlayer_LoadNativeLibraries_m2734 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_InitializePlatform_m2735 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(55 /* System.Void Vuforia.IQCARWrapper::InitPlatformNative() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m426(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_InitializeSurface_m2736 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m425(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t PlayModeUnityPlayer_Start_m2737 (PlayModeUnityPlayer_t147 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, String_t* >::Invoke(126 /* System.Int32 Vuforia.IQCARWrapper::QcarInit(System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return (int32_t)(L_2);
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t131_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_Update_m2738 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t131_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m420(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		PlayModeUnityPlayer_InitializeSurface_m2736(__this, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C" void PlayModeUnityPlayer_Dispose_m2739 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C" void PlayModeUnityPlayer_OnPause_m2740 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C" void PlayModeUnityPlayer_OnResume_m2741 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C" void PlayModeUnityPlayer_OnDestroy_m2742 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m424(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C" void PlayModeUnityPlayer__ctor_m461 (PlayModeUnityPlayer_t147 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.CylinderTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl.h"
// Vuforia.ImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetImpl.h"
// Vuforia.MultiTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetImpl.h"


// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
extern "C" void ReconstructionFromTargetImpl__ctor_m2743 (ReconstructionFromTargetImpl_t581 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderMin_5 = L_0;
		Vector3_t15  L_1 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderMax_6 = L_1;
		Vector3_t15  L_2 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderOffset_7 = L_2;
		Quaternion_t13  L_3 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderRotation_8 = L_3;
		__this->___mCanAutoSetInitializationTarget_10 = 1;
		IntPtr_t L_4 = ___nativeReconstructionPtr;
		ReconstructionImpl__ctor_m2692(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2744 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___cylinderTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___cylinderTarget;
		Vector3_t15  L_1 = ___occluderMin;
		Vector3_t15  L_2 = ___occluderMax;
		Vector3_t15  L_3 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_4 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2745(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* CylinderTargetImpl_t603_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2745 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___cylinderTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, Vector3_t15  ___offsetToOccluderOrigin, Quaternion_t13  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTargetImpl_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___cylinderTarget;
		NullCheck(((CylinderTargetImpl_t603 *)Castclass(L_0, CylinderTargetImpl_t603_il2cpp_TypeInfo_var)));
		DataSetImpl_t578 * L_1 = ObjectTargetImpl_get_DataSet_m2722(((CylinderTargetImpl_t603 *)Castclass(L_0, CylinderTargetImpl_t603_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		Object_t * L_3 = ___cylinderTarget;
		Vector3_t15  L_4 = ___occluderMin;
		Vector3_t15  L_5 = ___occluderMax;
		Vector3_t15  L_6 = ___offsetToOccluderOrigin;
		Quaternion_t13  L_7 = ___rotationToOccluderOrigin;
		bool L_8 = ReconstructionFromTargetImpl_SetInitializationTarget_m2754(__this, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2746 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___imageTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___imageTarget;
		Vector3_t15  L_1 = ___occluderMin;
		Vector3_t15  L_2 = ___occluderMax;
		Vector3_t15  L_3 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_4 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2747(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t622_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2747 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___imageTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, Vector3_t15  ___offsetToOccluderOrigin, Quaternion_t13  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		ImageTargetImpl_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		V_0 = L_0;
		Object_t * L_1 = ___imageTarget;
		if (!((ImageTargetImpl_t622 *)IsInst(L_1, ImageTargetImpl_t622_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		Object_t * L_2 = ___imageTarget;
		NullCheck(((ImageTargetImpl_t622 *)Castclass(L_2, ImageTargetImpl_t622_il2cpp_TypeInfo_var)));
		DataSetImpl_t578 * L_3 = ObjectTargetImpl_get_DataSet_m2722(((ImageTargetImpl_t622 *)Castclass(L_2, ImageTargetImpl_t622_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_001f:
	{
		IntPtr_t L_5 = V_0;
		Object_t * L_6 = ___imageTarget;
		Vector3_t15  L_7 = ___occluderMin;
		Vector3_t15  L_8 = ___occluderMax;
		Vector3_t15  L_9 = ___offsetToOccluderOrigin;
		Quaternion_t13  L_10 = ___rotationToOccluderOrigin;
		bool L_11 = ReconstructionFromTargetImpl_SetInitializationTarget_m2754(__this, L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2748 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___multiTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___multiTarget;
		Vector3_t15  L_1 = ___occluderMin;
		Vector3_t15  L_2 = ___occluderMax;
		Vector3_t15  L_3 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t13  L_4 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2749(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* MultiTargetImpl_t632_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2749 (ReconstructionFromTargetImpl_t581 * __this, Object_t * ___multiTarget, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, Vector3_t15  ___offsetToOccluderOrigin, Quaternion_t13  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiTargetImpl_t632_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___multiTarget;
		NullCheck(((MultiTargetImpl_t632 *)Castclass(L_0, MultiTargetImpl_t632_il2cpp_TypeInfo_var)));
		DataSetImpl_t578 * L_1 = ObjectTargetImpl_get_DataSet_m2722(((MultiTargetImpl_t632 *)Castclass(L_0, MultiTargetImpl_t632_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		Object_t * L_3 = ___multiTarget;
		Vector3_t15  L_4 = ___occluderMin;
		Vector3_t15  L_5 = ___occluderMax;
		Vector3_t15  L_6 = ___offsetToOccluderOrigin;
		Quaternion_t13  L_7 = ___rotationToOccluderOrigin;
		bool L_8 = ReconstructionFromTargetImpl_SetInitializationTarget_m2754(__this, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2750 (ReconstructionFromTargetImpl_t581 * __this, Vector3_t15 * ___occluderMin, Vector3_t15 * ___occluderMax, const MethodInfo* method)
{
	{
		Vector3_t15 * L_0 = ___occluderMin;
		Vector3_t15  L_1 = (__this->___mOccluderMin_5);
		*L_0 = L_1;
		Vector3_t15 * L_2 = ___occluderMax;
		Vector3_t15  L_3 = (__this->___mOccluderMax_6);
		*L_2 = L_3;
		Object_t * L_4 = (__this->___mInitializationTarget_9);
		return L_4;
	}
}
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2751 (ReconstructionFromTargetImpl_t581 * __this, Vector3_t15 * ___occluderMin, Vector3_t15 * ___occluderMax, Vector3_t15 * ___offsetToOccluderOrigin, Quaternion_t13 * ___rotationToOccluderOrigin, const MethodInfo* method)
{
	{
		Vector3_t15 * L_0 = ___offsetToOccluderOrigin;
		Vector3_t15  L_1 = (__this->___mOccluderOffset_7);
		*L_0 = L_1;
		Quaternion_t13 * L_2 = ___rotationToOccluderOrigin;
		Quaternion_t13  L_3 = (__this->___mOccluderRotation_8);
		*L_2 = L_3;
		Vector3_t15 * L_4 = ___occluderMin;
		Vector3_t15 * L_5 = ___occluderMax;
		Object_t * L_6 = ReconstructionFromTargetImpl_GetInitializationTarget_m2750(__this, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Reset()
extern "C" bool ReconstructionFromTargetImpl_Reset_m2752 (ReconstructionFromTargetImpl_t581 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ReconstructionImpl_Reset_m2704(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		bool L_2 = ReconstructionImpl_IsReconstructing_m2698(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		__this->___mCanAutoSetInitializationTarget_10 = 1;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Start()
extern "C" bool ReconstructionFromTargetImpl_Start_m2753 (ReconstructionFromTargetImpl_t581 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ReconstructionImpl_Start_m2697(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mCanAutoSetInitializationTarget_10 = 0;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(System.IntPtr,Vuforia.Trackable,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern const Il2CppType* Vector3_t15_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t15_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2754 (ReconstructionFromTargetImpl_t581 * __this, IntPtr_t ___datasetPtr, Object_t * ___trackable, Vector3_t15  ___occluderMin, Vector3_t15  ___occluderMax, Vector3_t15  ___offsetToOccluderOrigin, Quaternion_t13  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t15_0_0_0_var = il2cpp_codegen_type_from_index(27);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		Vector3_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t15  V_0 = {0};
	float V_1 = 0.0f;
	IntPtr_t V_2 = {0};
	IntPtr_t V_3 = {0};
	IntPtr_t V_4 = {0};
	IntPtr_t V_5 = {0};
	bool V_6 = false;
	{
		Object_t * L_0 = ___trackable;
		__this->___mInitializationTarget_9 = L_0;
		Vector3_t15  L_1 = ___occluderMin;
		__this->___mOccluderMin_5 = L_1;
		Vector3_t15  L_2 = ___occluderMax;
		__this->___mOccluderMax_6 = L_2;
		Vector3_t15  L_3 = ___offsetToOccluderOrigin;
		__this->___mOccluderOffset_7 = L_3;
		Quaternion_t13  L_4 = ___rotationToOccluderOrigin;
		__this->___mOccluderRotation_8 = L_4;
		Quaternion_ToAngleAxis_m4333((&___rotationToOccluderOrigin), (&V_1), (&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_6 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_7 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t15  L_8 = ___occluderMin;
		Vector3_t15  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t15_il2cpp_TypeInfo_var, &L_9);
		IntPtr_t L_11 = V_2;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_10, L_11, 0, /*hidden argument*/NULL);
		Type_t * L_12 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_13 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IntPtr_t L_14 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector3_t15  L_15 = ___occluderMax;
		Vector3_t15  L_16 = L_15;
		Object_t * L_17 = Box(Vector3_t15_il2cpp_TypeInfo_var, &L_16);
		IntPtr_t L_18 = V_3;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_17, L_18, 0, /*hidden argument*/NULL);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_20 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr_t L_21 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		Vector3_t15  L_22 = ___offsetToOccluderOrigin;
		Vector3_t15  L_23 = L_22;
		Object_t * L_24 = Box(Vector3_t15_il2cpp_TypeInfo_var, &L_23);
		IntPtr_t L_25 = V_4;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_24, L_25, 0, /*hidden argument*/NULL);
		Type_t * L_26 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Vector3_t15_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_27 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IntPtr_t L_28 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		Vector3_t15  L_29 = V_0;
		Vector3_t15  L_30 = L_29;
		Object_t * L_31 = Box(Vector3_t15_il2cpp_TypeInfo_var, &L_30);
		IntPtr_t L_32 = V_5;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_31, L_32, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_33 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_34 = (((ReconstructionImpl_t576 *)__this)->___mNativeReconstructionPtr_0);
		IntPtr_t L_35 = ___datasetPtr;
		Object_t * L_36 = ___trackable;
		NullCheck(L_36);
		int32_t L_37 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_36);
		IntPtr_t L_38 = V_2;
		IntPtr_t L_39 = V_3;
		IntPtr_t L_40 = V_4;
		IntPtr_t L_41 = V_5;
		float L_42 = V_1;
		NullCheck(L_33);
		bool L_43 = (bool)InterfaceFuncInvoker8< bool, IntPtr_t, IntPtr_t, int32_t, IntPtr_t, IntPtr_t, IntPtr_t, IntPtr_t, float >::Invoke(81 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_33, L_34, L_35, L_37, L_38, L_39, L_40, L_41, ((float)((float)(360.0f)-(float)L_42)));
		V_6 = L_43;
		bool L_44 = V_6;
		if (!L_44)
		{
			goto IL_0100;
		}
	}
	{
		Object_t * L_45 = ___trackable;
		NullCheck(L_45);
		String_t* L_46 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t565_il2cpp_TypeInfo_var, L_45);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m323(NULL /*static, unused*/, L_46, (String_t*) &_stringLiteral134, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		goto IL_0115;
	}

IL_0100:
	{
		Object_t * L_48 = ___trackable;
		NullCheck(L_48);
		String_t* L_49 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t565_il2cpp_TypeInfo_var, L_48);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m323(NULL /*static, unused*/, L_49, (String_t*) &_stringLiteral135, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
	}

IL_0115:
	{
		IntPtr_t L_51 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		IntPtr_t L_52 = V_3;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		IntPtr_t L_53 = V_4;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		IntPtr_t L_54 = V_5;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		bool L_55 = V_6;
		return L_55;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::get_CanAutoSetInitializationTarget()
extern "C" bool ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m2755 (ReconstructionFromTargetImpl_t581 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCanAutoSetInitializationTarget_10);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionFromTargetImpl::set_CanAutoSetInitializationTarget(System.Boolean)
extern "C" void ReconstructionFromTargetImpl_set_CanAutoSetInitializationTarget_m2756 (ReconstructionFromTargetImpl_t581 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mCanAutoSetInitializationTarget_10 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"
// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"
struct Component_t103;
struct ReconstructionAbstractBehaviour_t68;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionAbstractBehaviour>()
#define Component_GetComponent_TisReconstructionAbstractBehaviour_t68_m4334(__this, method) (( ReconstructionAbstractBehaviour_t68 * (*) (Component_t103 *, const MethodInfo*))Component_GetComponent_TisObject_t_m262_gshared)(__this, method)
struct SmartTerrainBuilder_t583;
struct ReconstructionFromTarget_t582;
struct SmartTerrainBuilder_t583;
struct Object_t;
// Declaration T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
// Declaration T Vuforia.SmartTerrainBuilder::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<Vuforia.ReconstructionFromTarget>()


// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t68 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2757 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour_t68 * L_0 = (__this->___mReconstructionBehaviour_3);
		return L_0;
	}
}
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2758 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mReconstructionFromTarget_2);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern const Il2CppType* SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionAbstractBehaviour_t68_m4334_MethodInfo_var;
extern const MethodInfo* ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m2759 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var = il2cpp_codegen_type_from_index(237);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(237);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		Component_GetComponent_TisReconstructionAbstractBehaviour_t68_m4334_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483949);
		ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var = il2cpp_codegen_method_info_from_index(302);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTrackerAbstractBehaviour_t72 * V_0 = {0};
	{
		ReconstructionAbstractBehaviour_t68 * L_0 = Component_GetComponent_TisReconstructionAbstractBehaviour_t68_m4334(__this, /*hidden argument*/Component_GetComponent_TisReconstructionAbstractBehaviour_t68_m4334_MethodInfo_var);
		__this->___mReconstructionBehaviour_3 = L_0;
		ReconstructionAbstractBehaviour_t68 * L_1 = (__this->___mReconstructionBehaviour_3);
		bool L_2 = Object_op_Equality_m375(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral136, /*hidden argument*/NULL);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_4 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((SmartTerrainTrackerAbstractBehaviour_t72 *)Castclass(L_4, SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var));
		SmartTerrainTrackerAbstractBehaviour_t72 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m397(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_t72 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var };
		Action_t139 * L_9 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2787(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern const Il2CppType* SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern const MethodInfo* Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var;
extern const MethodInfo* ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2760 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var = il2cpp_codegen_type_from_index(237);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(237);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var = il2cpp_codegen_method_info_from_index(302);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	SmartTerrainTrackerAbstractBehaviour_t72 * V_1 = {0};
	{
		ReconstructionAbstractBehaviour_t68 * L_0 = (__this->___mReconstructionBehaviour_3);
		bool L_1 = Object_op_Inequality_m386(NULL /*static, unused*/, L_0, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_2 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SmartTerrainTracker_t674 * L_3 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_2);
		V_0 = L_3;
		SmartTerrainTracker_t674 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		SmartTerrainTracker_t674 * L_5 = V_0;
		NullCheck(L_5);
		SmartTerrainBuilder_t583 * L_6 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_5);
		NullCheck(L_6);
		Object_t* L_7 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_6);
		ReconstructionAbstractBehaviour_t68 * L_8 = (__this->___mReconstructionBehaviour_3);
		bool L_9 = Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/Enumerable_Contains_TisReconstructionAbstractBehaviour_t68_m4303_MethodInfo_var);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		SmartTerrainTracker_t674 * L_10 = V_0;
		NullCheck(L_10);
		SmartTerrainBuilder_t583 * L_11 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_10);
		ReconstructionAbstractBehaviour_t68 * L_12 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_11);
		VirtFuncInvoker1< bool, ReconstructionAbstractBehaviour_t68 * >::Invoke(9 /* System.Boolean Vuforia.SmartTerrainBuilder::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour) */, L_11, L_12);
	}

IL_0046:
	{
		ReconstructionAbstractBehaviour_t68 * L_13 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_13);
		Object_t * L_14 = ReconstructionAbstractBehaviour_get_Reconstruction_m3967(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		SmartTerrainTracker_t674 * L_15 = V_0;
		NullCheck(L_15);
		SmartTerrainBuilder_t583 * L_16 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_15);
		Object_t * L_17 = (__this->___mReconstructionFromTarget_2);
		NullCheck(L_16);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(10 /* System.Boolean Vuforia.SmartTerrainBuilder::DestroyReconstruction(Vuforia.Reconstruction) */, L_16, L_17);
	}

IL_0065:
	{
		ReconstructionAbstractBehaviour_t68 * L_18 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_18);
		ReconstructionAbstractBehaviour_Deinitialize_m3987(L_18, /*hidden argument*/NULL);
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SmartTerrainTrackerAbstractBehaviour_t72_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_20 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_1 = ((SmartTerrainTrackerAbstractBehaviour_t72 *)Castclass(L_20, SmartTerrainTrackerAbstractBehaviour_t72_il2cpp_TypeInfo_var));
		SmartTerrainTrackerAbstractBehaviour_t72 * L_21 = V_1;
		bool L_22 = Object_op_Implicit_m397(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009f;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_t72 * L_23 = V_1;
		IntPtr_t L_24 = { (void*)ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762_MethodInfo_var };
		Action_t139 * L_25 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_25, __this, L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2788(L_23, L_25, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m2761 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour_t68 * L_0 = (__this->___mReconstructionBehaviour_3);
		bool L_1 = Object_op_Inequality_m386(NULL /*static, unused*/, L_0, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		ReconstructionAbstractBehaviour_t68 * L_2 = (__this->___mReconstructionBehaviour_3);
		Object_t * L_3 = (__this->___mReconstructionFromTarget_2);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_Initialize_m3986(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0020:
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral137, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern const MethodInfo* SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t582_m4335_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t582_m4335_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483951);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		Object_t * L_0 = (__this->___mReconstructionFromTarget_2);
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_1 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SmartTerrainTracker_t674 * L_2 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_1);
		V_0 = L_2;
		SmartTerrainTracker_t674 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		SmartTerrainTracker_t674 * L_4 = V_0;
		NullCheck(L_4);
		SmartTerrainBuilder_t583 * L_5 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_4);
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)GenericVirtFuncInvoker0< Object_t * >::Invoke(SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t582_m4335_MethodInfo_var, L_5);
		__this->___mReconstructionFromTarget_2 = L_6;
	}

IL_0027:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m464 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean Vuforia.SmartTerrainBuilder::Init()
// System.Boolean Vuforia.SmartTerrainBuilder::Deinit()
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions()
// System.Boolean Vuforia.SmartTerrainBuilder::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::DestroyReconstruction(Vuforia.Reconstruction)
// System.Void Vuforia.SmartTerrainBuilder::.ctor()
extern "C" void SmartTerrainBuilder__ctor_m2763 (SmartTerrainBuilder_t583 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_EyeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0MethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.InternalEyewear/EyewearCalibrationReading
void EyewearCalibrationReading_t586_marshal(const EyewearCalibrationReading_t586& unmarshaled, EyewearCalibrationReading_t586_marshaled& marshaled)
{
	marshaled.___pose_0 = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)unmarshaled.___pose_0);
	marshaled.___scale_1 = unmarshaled.___scale_1;
	marshaled.___centerX_2 = unmarshaled.___centerX_2;
	marshaled.___centerY_3 = unmarshaled.___centerY_3;
	marshaled.___unused_4 = unmarshaled.___unused_4;
}
extern TypeInfo* Single_t151_il2cpp_TypeInfo_var;
void EyewearCalibrationReading_t586_marshal_back(const EyewearCalibrationReading_t586_marshaled& marshaled, EyewearCalibrationReading_t586& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___pose_0 = (SingleU5BU5D_t585*)il2cpp_codegen_marshal_array_result(Single_t151_il2cpp_TypeInfo_var, marshaled.___pose_0, 1);
	unmarshaled.___scale_1 = marshaled.___scale_1;
	unmarshaled.___centerX_2 = marshaled.___centerX_2;
	unmarshaled.___centerY_3 = marshaled.___centerY_3;
	unmarshaled.___unused_4 = marshaled.___unused_4;
}
// Conversion method for clean up from marshalling of: Vuforia.InternalEyewear/EyewearCalibrationReading
void EyewearCalibrationReading_t586_marshal_cleanup(EyewearCalibrationReading_t586_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif



// Vuforia.InternalEyewear Vuforia.InternalEyewear::get_Instance()
extern const Il2CppType* InternalEyewear_t587_0_0_0_var;
extern TypeInfo* InternalEyewear_t587_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" InternalEyewear_t587 * InternalEyewear_get_Instance_m2764 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewear_t587_0_0_0_var = il2cpp_codegen_type_from_index(1018);
		InternalEyewear_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1018);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
		InternalEyewear_t587 * L_0 = ((InternalEyewear_t587_StaticFields*)InternalEyewear_t587_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(InternalEyewear_t587_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4313(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
			InternalEyewear_t587 * L_3 = ((InternalEyewear_t587_StaticFields*)InternalEyewear_t587_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			InternalEyewear_t587 * L_4 = (InternalEyewear_t587 *)il2cpp_codegen_object_new (InternalEyewear_t587_il2cpp_TypeInfo_var);
			InternalEyewear__ctor_m2777(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
			((InternalEyewear_t587_StaticFields*)InternalEyewear_t587_il2cpp_TypeInfo_var->static_fields)->___mInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
		InternalEyewear_t587 * L_6 = ((InternalEyewear_t587_StaticFields*)InternalEyewear_t587_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		return L_6;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsSupportedDeviceDetected()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsSupportedDeviceDetected_m2765 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(130 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsSupportedDeviceDetected() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsSeeThru()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsSeeThru_m2766 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(131 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsSeeThru() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.ScreenOrientation Vuforia.InternalEyewear::GetScreenOrientation()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewear_GetScreenOrientation_m2767 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(132 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetScreenOrientation() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 0)
		{
			goto IL_0021;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 1)
		{
			goto IL_0023;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0027;
	}

IL_0021:
	{
		return (int32_t)(1);
	}

IL_0023:
	{
		return (int32_t)(3);
	}

IL_0025:
	{
		return (int32_t)(4);
	}

IL_0027:
	{
		return (int32_t)(0);
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoCapable()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoCapable_m2768 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(133 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoCapable() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoEnabled()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoEnabled_m2769 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(134 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoEnabled() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoGLOnly()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoGLOnly_m2770 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(135 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoGLOnly() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::SetStereo(System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_SetStereo_m2771 (InternalEyewear_t587 * __this, bool ___enable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___enable;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, bool >::Invoke(136 /* System.Boolean Vuforia.IQCARWrapper::EyewearSetStereo(System.Boolean) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Single Vuforia.InternalEyewear::GetDefaultSceneScale()
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" float InternalEyewear_GetDefaultSceneScale_m2772 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	IntPtr_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (1.0f);
	}

IL_000d:
	{
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_5 = V_1;
		NullCheck(L_4);
		InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(137 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetDefaultSceneScale(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5);
		IntPtr_t L_6 = V_1;
		SingleU5BU5D_t585* L_7 = V_0;
		Marshal_Copy_m4274(NULL /*static, unused*/, L_6, L_7, 0, 1, /*hidden argument*/NULL);
		IntPtr_t L_8 = V_1;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		return (*(float*)(float*)SZArrayLdElema(L_9, L_10));
	}
}
// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::getProfileManager()
extern TypeInfo* InternalEyewearCalibrationProfileManager_t560_il2cpp_TypeInfo_var;
extern "C" InternalEyewearCalibrationProfileManager_t560 * InternalEyewear_getProfileManager_m2773 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewearCalibrationProfileManager_t560_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1046);
		s_Il2CppMethodIntialized = true;
	}
	InternalEyewear_t587 * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		InternalEyewearCalibrationProfileManager_t560 * L_0 = (__this->___mProfileManager_1);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = __this;
		Monitor_Enter_m4313(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			InternalEyewearCalibrationProfileManager_t560 * L_1 = (__this->___mProfileManager_1);
			if (L_1)
			{
				goto IL_0023;
			}
		}

IL_0018:
		{
			InternalEyewearCalibrationProfileManager_t560 * L_2 = (InternalEyewearCalibrationProfileManager_t560 *)il2cpp_codegen_object_new (InternalEyewearCalibrationProfileManager_t560_il2cpp_TypeInfo_var);
			InternalEyewearCalibrationProfileManager__ctor_m2629(L_2, /*hidden argument*/NULL);
			__this->___mProfileManager_1 = L_2;
		}

IL_0023:
		{
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		InternalEyewear_t587 * L_3 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_002c:
	{
		InternalEyewearCalibrationProfileManager_t560 * L_4 = (__this->___mProfileManager_1);
		return L_4;
	}
}
// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::getCalibrator()
extern TypeInfo* InternalEyewearUserCalibrator_t562_il2cpp_TypeInfo_var;
extern "C" InternalEyewearUserCalibrator_t562 * InternalEyewear_getCalibrator_m2774 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewearUserCalibrator_t562_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1047);
		s_Il2CppMethodIntialized = true;
	}
	InternalEyewear_t587 * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		InternalEyewearUserCalibrator_t562 * L_0 = (__this->___mCalibrator_2);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = __this;
		Monitor_Enter_m4313(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			InternalEyewearUserCalibrator_t562 * L_1 = (__this->___mCalibrator_2);
			if (L_1)
			{
				goto IL_0023;
			}
		}

IL_0018:
		{
			InternalEyewearUserCalibrator_t562 * L_2 = (InternalEyewearUserCalibrator_t562 *)il2cpp_codegen_object_new (InternalEyewearUserCalibrator_t562_il2cpp_TypeInfo_var);
			InternalEyewearUserCalibrator__ctor_m2647(L_2, /*hidden argument*/NULL);
			__this->___mCalibrator_2 = L_2;
		}

IL_0023:
		{
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		InternalEyewear_t587 * L_3 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_002c:
	{
		InternalEyewearUserCalibrator_t562 * L_4 = (__this->___mCalibrator_2);
		return L_4;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,UnityEngine.ScreenOrientation)
extern "C" Matrix4x4_t156  InternalEyewear_GetProjectionMatrix_m2775 (InternalEyewear_t587 * __this, int32_t ___eyeID, int32_t ___screenOrientation, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eyeID;
		int32_t L_1 = ___screenOrientation;
		Matrix4x4_t156  L_2 = InternalEyewear_GetProjectionMatrix_m2776(__this, L_0, (-1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,System.Int32,UnityEngine.ScreenOrientation)
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t156  InternalEyewear_GetProjectionMatrix_m2776 (InternalEyewear_t587 * __this, int32_t ___eyeID, int32_t ___profileID, int32_t ___screenOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t156  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___eyeID;
		int32_t L_6 = ___profileID;
		IntPtr_t L_7 = V_1;
		int32_t L_8 = ___screenOrientation;
		NullCheck(L_4);
		InterfaceFuncInvoker4< int32_t, int32_t, int32_t, IntPtr_t, int32_t >::Invoke(138 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8);
		IntPtr_t L_9 = V_1;
		SingleU5BU5D_t585* L_10 = V_0;
		SingleU5BU5D_t585* L_11 = V_0;
		NullCheck(L_11);
		Marshal_Copy_m4274(NULL /*static, unused*/, L_9, L_10, 0, (((int32_t)(((Array_t *)L_11)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t156  L_12 = Matrix4x4_get_identity_m4275(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_12;
		V_3 = 0;
		goto IL_0054;
	}

IL_0045:
	{
		int32_t L_13 = V_3;
		SingleU5BU5D_t585* L_14 = V_0;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Matrix4x4_set_Item_m4276((&V_2), L_13, (*(float*)(float*)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)16))))
		{
			goto IL_0045;
		}
	}
	{
		IntPtr_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Matrix4x4_t156  L_20 = V_2;
		return L_20;
	}
}
// System.Void Vuforia.InternalEyewear::.ctor()
extern "C" void InternalEyewear__ctor_m2777 (InternalEyewear_t587 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.InternalEyewear::.cctor()
extern "C" void InternalEyewear__cctor_m2778 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitialMethodDeclarations.h"



// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackabMethodDeclarations.h"

// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshCollider.h"
// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshColliderMethodDeclarations.h"


// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
extern "C" Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2779 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mSmartTerrainTrackable_9);
		return L_0;
	}
}
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
extern "C" bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2780 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mDisableAutomaticUpdates_10);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
extern TypeInfo* SmartTerrainTrackable_t589_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m701 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackable_t589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1048);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t153 * V_0 = {0};
	{
		Object_t * L_0 = (__this->___mSmartTerrainTrackable_9);
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		bool L_1 = (__this->___mDisableAutomaticUpdates_10);
		if (L_1)
		{
			goto IL_008c;
		}
	}
	{
		Object_t * L_2 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_2);
		Mesh_t153 * L_3 = (Mesh_t153 *)InterfaceFuncInvoker0< Mesh_t153 * >::Invoke(1 /* UnityEngine.Mesh Vuforia.SmartTerrainTrackable::GetMesh() */, SmartTerrainTrackable_t589_il2cpp_TypeInfo_var, L_2);
		V_0 = L_3;
		Mesh_t153 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m375(NULL /*static, unused*/, L_4, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		MeshFilter_t149 * L_6 = (__this->___mMeshFilterToUpdate_11);
		bool L_7 = Object_op_Inequality_m386(NULL /*static, unused*/, L_6, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		MeshFilter_t149 * L_8 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_8);
		Mesh_t153 * L_9 = MeshFilter_get_sharedMesh_m486(L_8, /*hidden argument*/NULL);
		Mesh_t153 * L_10 = V_0;
		bool L_11 = Object_op_Inequality_m386(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0066;
		}
	}
	{
		MeshFilter_t149 * L_12 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_12);
		Mesh_t153 * L_13 = MeshFilter_get_mesh_m4283(L_12, /*hidden argument*/NULL);
		Object_Destroy_m471(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		MeshFilter_t149 * L_14 = (__this->___mMeshFilterToUpdate_11);
		Mesh_t153 * L_15 = V_0;
		NullCheck(L_14);
		MeshFilter_set_sharedMesh_m4337(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		MeshCollider_t590 * L_16 = (__this->___mMeshColliderToUpdate_12);
		bool L_17 = Object_op_Inequality_m386(NULL /*static, unused*/, L_16, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008c;
		}
	}
	{
		MeshCollider_t590 * L_18 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_18);
		MeshCollider_set_sharedMesh_m4338(L_18, (Mesh_t153 *)NULL, /*hidden argument*/NULL);
		MeshCollider_t590 * L_19 = (__this->___mMeshColliderToUpdate_12);
		Mesh_t153 * L_20 = V_0;
		NullCheck(L_19);
		MeshCollider_set_sharedMesh_m4338(L_19, L_20, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
extern "C" void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2781 (SmartTerrainTrackableBehaviour_t591 * __this, bool ___disabled, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = (__this->___mDisableAutomaticUpdates_10);
		V_0 = L_0;
		bool L_1 = ___disabled;
		__this->___mDisableAutomaticUpdates_10 = L_1;
		bool L_2 = ___disabled;
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		VirtActionInvoker0::Invoke(25 /* System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders() */, __this);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackableBehaviour_Start_m702 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		MeshFilter_t149 * L_0 = (__this->___mMeshFilterToUpdate_11);
		bool L_1 = Object_op_Inequality_m386(NULL /*static, unused*/, L_0, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d6;
		}
	}
	{
		MeshFilter_t149 * L_2 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_2);
		GameObject_t2 * L_3 = Component_get_gameObject_m272(L_2, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		bool L_5 = Object_op_Equality_m375(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00d6;
		}
	}
	{
		MeshFilter_t149 * L_6 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_6);
		GameObject_t2 * L_7 = Component_get_gameObject_m272(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m273(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = Transform_get_parent_m244(L_8, /*hidden argument*/NULL);
		GameObject_t2 * L_10 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m273(L_10, /*hidden argument*/NULL);
		bool L_12 = Object_op_Equality_m375(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		MeshFilter_t149 * L_13 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_13);
		Transform_t11 * L_14 = Component_get_transform_m243(L_13, /*hidden argument*/NULL);
		Vector3_t15  L_15 = {0};
		Vector3__ctor_m249(&L_15, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localPosition_m2264(L_14, L_15, /*hidden argument*/NULL);
		MeshFilter_t149 * L_16 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_16);
		Transform_t11 * L_17 = Component_get_transform_m243(L_16, /*hidden argument*/NULL);
		Vector3_t15  L_18 = {0};
		Vector3__ctor_m249(&L_18, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localScale_m2265(L_17, L_18, /*hidden argument*/NULL);
		MeshFilter_t149 * L_19 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_19);
		Transform_t11 * L_20 = Component_get_transform_m243(L_19, /*hidden argument*/NULL);
		Quaternion_t13  L_21 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localRotation_m299(L_20, L_21, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b2:
	{
		Object_t * L_22 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_22);
		int32_t L_24 = L_23;
		Object_t * L_25 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1948(NULL /*static, unused*/, (String_t*) &_stringLiteral138, L_25, (String_t*) &_stringLiteral139, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		MeshCollider_t590 * L_27 = (__this->___mMeshColliderToUpdate_12);
		bool L_28 = Object_op_Inequality_m386(NULL /*static, unused*/, L_27, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_01a9;
		}
	}
	{
		MeshCollider_t590 * L_29 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_29);
		GameObject_t2 * L_30 = Component_get_gameObject_m272(L_29, /*hidden argument*/NULL);
		GameObject_t2 * L_31 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		bool L_32 = Object_op_Equality_m375(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0100;
		}
	}
	{
		return;
	}

IL_0100:
	{
		MeshCollider_t590 * L_33 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_33);
		GameObject_t2 * L_34 = Component_get_gameObject_m272(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_t11 * L_35 = GameObject_get_transform_m273(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t11 * L_36 = Transform_get_parent_m244(L_35, /*hidden argument*/NULL);
		GameObject_t2 * L_37 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t11 * L_38 = GameObject_get_transform_m273(L_37, /*hidden argument*/NULL);
		bool L_39 = Object_op_Equality_m375(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0185;
		}
	}
	{
		MeshCollider_t590 * L_40 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_40);
		Transform_t11 * L_41 = Component_get_transform_m243(L_40, /*hidden argument*/NULL);
		Vector3_t15  L_42 = {0};
		Vector3__ctor_m249(&L_42, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localPosition_m2264(L_41, L_42, /*hidden argument*/NULL);
		MeshCollider_t590 * L_43 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_43);
		Transform_t11 * L_44 = Component_get_transform_m243(L_43, /*hidden argument*/NULL);
		Vector3_t15  L_45 = {0};
		Vector3__ctor_m249(&L_45, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_localScale_m2265(L_44, L_45, /*hidden argument*/NULL);
		MeshCollider_t590 * L_46 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_46);
		Transform_t11 * L_47 = Component_get_transform_m243(L_46, /*hidden argument*/NULL);
		Quaternion_t13  L_48 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_localRotation_m299(L_47, L_48, /*hidden argument*/NULL);
		return;
	}

IL_0185:
	{
		Object_t * L_49 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_49);
		int32_t L_50 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_49);
		int32_t L_51 = L_50;
		Object_t * L_52 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m1948(NULL /*static, unused*/, (String_t*) &_stringLiteral138, L_52, (String_t*) &_stringLiteral140, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
	}

IL_01a9:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
extern "C" void SmartTerrainTrackableBehaviour__ctor_m2782 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method)
{
	{
		TrackableBehaviour__ctor_m2660(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
struct QCARAbstractBehaviour_t67;
struct QCARAbstractBehaviour_t67;
// Declaration System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<System.Object>()
// System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<System.Object>()
extern "C" void QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340_gshared (QCARAbstractBehaviour_t67 * __this, const MethodInfo* method);
#define QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340(__this, method) (( void (*) (QCARAbstractBehaviour_t67 *, const MethodInfo*))QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340_gshared)(__this, method)
// Declaration System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<Vuforia.SmartTerrainTracker>()
// System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<Vuforia.SmartTerrainTracker>()
#define QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t674_m4339(__this, method) (( void (*) (QCARAbstractBehaviour_t67 *, const MethodInfo*))QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4340_gshared)(__this, method)
struct TrackerManager_t728;
struct SmartTerrainTracker_t674;
struct TrackerManager_t728;
struct Object_t;
// Declaration T Vuforia.TrackerManager::InitTracker<System.Object>()
// T Vuforia.TrackerManager::InitTracker<System.Object>()
// Declaration T Vuforia.TrackerManager::InitTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManager::InitTracker<Vuforia.SmartTerrainTracker>()


// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t747_il2cpp_TypeInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4343_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_Awake_m2783 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		Action_1_t747_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(304);
		SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		Action_1__ctor_m4343_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483955);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_2, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m397(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005b;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_5 = V_0;
		IntPtr_t L_6 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var };
		Action_t139 * L_7 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4136(L_5, L_7, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_8 = V_0;
		IntPtr_t L_9 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var };
		Action_t139 * L_10 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_10, __this, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4138(L_8, L_10, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_11 = V_0;
		IntPtr_t L_12 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var };
		Action_1_t747 * L_13 = (Action_1_t747 *)il2cpp_codegen_object_new (Action_1_t747_il2cpp_TypeInfo_var);
		Action_1__ctor_m4343(L_13, __this, L_12, /*hidden argument*/Action_1__ctor_m4343_MethodInfo_var);
		NullCheck(L_11);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4142(L_11, L_13, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2784 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mTrackerWasActiveBeforeDisabling_8);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2789(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDisable_m2785 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		SmartTerrainTracker_t674 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_3);
		__this->___mTrackerWasActiveBeforeDisabling_8 = L_4;
		SmartTerrainTracker_t674 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_5);
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2790(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t747_il2cpp_TypeInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4343_MethodInfo_var;
extern const MethodInfo* QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t674_m4339_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2786 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		Action_1_t747_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(304);
		SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		Action_1__ctor_m4343_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483955);
		QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t674_m4339_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_1, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792_MethodInfo_var };
		Action_t139 * L_6 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4137(L_4, L_6, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793_MethodInfo_var };
		Action_t139 * L_9 = (Action_t139 *)il2cpp_codegen_object_new (Action_t139_il2cpp_TypeInfo_var);
		Action__ctor_m4319(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4139(L_7, L_9, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_10 = V_0;
		IntPtr_t L_11 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnPause_m2794_MethodInfo_var };
		Action_1_t747 * L_12 = (Action_1_t747 *)il2cpp_codegen_object_new (Action_1_t747_il2cpp_TypeInfo_var);
		Action_1__ctor_m4343(L_12, __this, L_11, /*hidden argument*/Action_1__ctor_m4343_MethodInfo_var);
		NullCheck(L_10);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4143(L_10, L_12, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_13 = V_0;
		NullCheck(L_13);
		QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t674_m4339(L_13, /*hidden argument*/QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t674_m4339_MethodInfo_var);
	}

IL_0059:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2787 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, Action_t139 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		Action_t139 * L_0 = (__this->___mTrackerStarted_6);
		Action_t139 * L_1 = ___callback;
		Delegate_t143 * L_2 = Delegate_Combine_m2117(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTrackerStarted_6 = ((Action_t139 *)Castclass(L_2, Action_t139_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_3 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SmartTerrainTracker_t674 * L_4 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_3);
		V_0 = L_4;
		SmartTerrainTracker_t674 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		SmartTerrainTracker_t674 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_6);
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		Action_t139 * L_8 = ___callback;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_8);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern TypeInfo* Action_t139_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2788 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, Action_t139 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t139 * L_0 = (__this->___mTrackerStarted_6);
		Action_t139 * L_1 = ___callback;
		Delegate_t143 * L_2 = Delegate_Remove_m2118(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTrackerStarted_6 = ((Action_t139 *)Castclass(L_2, Action_t139_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2789 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral141, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		SmartTerrainTracker_t674 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.Tracker::Start() */, L_3);
		Action_t139 * L_4 = (__this->___mTrackerStarted_6);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		Action_t139 * L_5 = (__this->___mTrackerStarted_6);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_5);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2790 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		SmartTerrainTracker_t674 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(5 /* System.Void Vuforia.Tracker::Stop() */, L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern const MethodInfo* TrackerManager_InitTracker_TisSmartTerrainTracker_t674_m4341_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2791 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		TrackerManager_InitTracker_TisSmartTerrainTracker_t674_m4341_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483957);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		if (L_1)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_2 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SmartTerrainTracker_t674 * L_3 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_InitTracker_TisSmartTerrainTracker_t674_m4341_MethodInfo_var, L_2);
		V_0 = L_3;
		SmartTerrainTracker_t674 * L_4 = V_0;
		float L_5 = (__this->___mSceneUnitsToMillimeter_5);
		NullCheck(L_4);
		VirtFuncInvoker1< bool, float >::Invoke(9 /* System.Boolean Vuforia.SmartTerrainTracker::SetScaleToMillimeter(System.Single) */, L_4, L_5);
		bool L_6 = (__this->___mAutoInitBuilder_4);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		SmartTerrainTracker_t674 * L_7 = V_0;
		NullCheck(L_7);
		SmartTerrainBuilder_t583 * L_8 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_7);
		NullCheck(L_8);
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.SmartTerrainBuilder::Init() */, L_8);
	}

IL_0038:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARInitialized()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2792 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	QCARAbstractBehaviour_t67 * V_1 = {0};
	{
		bool L_0 = (__this->___mAutoInitTracker_2);
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = ((QCARAbstractBehaviour_t67 *)Castclass(L_2, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_3 = V_1;
		bool L_4 = Object_op_Implicit_m397(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = QCARAbstractBehaviour_get_HasStarted_m4127(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_7 = V_1;
		NullCheck(L_7);
		Behaviour_set_enabled_m240(L_7, 0, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_0038:
	{
		SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2791(__this, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_9 = V_1;
		NullCheck(L_9);
		Behaviour_set_enabled_m240(L_9, 1, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARStarted()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2793 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoStartTracker_3);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2789(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnPause_m2794 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		bool L_3 = ___pause;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		SmartTerrainTracker_t674 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_4);
		__this->___mTrackerWasActiveBeforePause_7 = L_5;
		SmartTerrainTracker_t674 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_6);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2790(__this, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		bool L_8 = (__this->___mTrackerWasActiveBeforePause_7);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2789(__this, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m692 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, bool ___autoStart, const MethodInfo* method)
{
	{
		bool L_0 = ___autoStart;
		__this->___mAutoInitTracker_2 = L_0;
		bool L_1 = ___autoStart;
		__this->___mAutoStartTracker_3 = L_1;
		bool L_2 = ___autoStart;
		__this->___mAutoInitBuilder_4 = L_2;
		return;
	}
}
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C" bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m693 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoInitTracker_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = (__this->___mAutoStartTracker_3);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		bool L_2 = (__this->___mAutoInitBuilder_4);
		return L_2;
	}

IL_0017:
	{
		return 0;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m694 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, float ___scaleToMM, const MethodInfo* method)
{
	{
		float L_0 = ___scaleToMM;
		__this->___mSceneUnitsToMillimeter_5 = L_0;
		return;
	}
}
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C" float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m695 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSceneUnitsToMillimeter_5);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C" void SmartTerrainTrackerAbstractBehaviour__ctor_m465 (SmartTerrainTrackerAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"



// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
extern "C" Object_t * SurfaceAbstractBehaviour_get_Surface_m2795 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mSurface_13);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m700 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mSurface_13 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		Object_t * L_1 = L_0;
		V_1 = L_1;
		((SmartTerrainTrackableBehaviour_t591 *)__this)->___mSmartTerrainTrackable_9 = L_1;
		Object_t * L_2 = V_1;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_2;
		return;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m703 (SurfaceAbstractBehaviour_t73 * __this, Object_t * ___surface, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = ___surface;
		Object_t * L_1 = L_0;
		V_0 = L_1;
		__this->___mSurface_13 = L_1;
		Object_t * L_2 = V_0;
		Object_t * L_3 = L_2;
		V_1 = L_3;
		((SmartTerrainTrackableBehaviour_t591 *)__this)->___mSmartTerrainTrackable_9 = L_3;
		Object_t * L_4 = V_1;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_4;
		return;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m704 (SurfaceAbstractBehaviour_t73 * __this, MeshFilter_t149 * ___meshFilterToUpdate, const MethodInfo* method)
{
	{
		MeshFilter_t149 * L_0 = ___meshFilterToUpdate;
		((SmartTerrainTrackableBehaviour_t591 *)__this)->___mMeshFilterToUpdate_11 = L_0;
		return;
	}
}
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
extern "C" MeshFilter_t149 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m705 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		MeshFilter_t149 * L_0 = (((SmartTerrainTrackableBehaviour_t591 *)__this)->___mMeshFilterToUpdate_11);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m706 (SurfaceAbstractBehaviour_t73 * __this, MeshCollider_t590 * ___meshColliderToUpdate, const MethodInfo* method)
{
	{
		MeshCollider_t590 * L_0 = ___meshColliderToUpdate;
		((SmartTerrainTrackableBehaviour_t591 *)__this)->___mMeshColliderToUpdate_12 = L_0;
		return;
	}
}
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
extern "C" MeshCollider_t590 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m707 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		MeshCollider_t590 * L_0 = (((SmartTerrainTrackableBehaviour_t591 *)__this)->___mMeshColliderToUpdate_12);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
extern "C" void SurfaceAbstractBehaviour__ctor_m466 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackableBehaviour__ctor_m2782(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m696 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m697 (SurfaceAbstractBehaviour_t73 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m240(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m698 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m699 (SurfaceAbstractBehaviour_t73 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"

// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"


// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::get_CylinderTarget()
extern "C" Object_t * CylinderTargetAbstractBehaviour_get_CylinderTarget_m2796 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mCylinderTarget_20);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_SideLength()
extern "C" float CylinderTargetAbstractBehaviour_get_SideLength_m2797 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		float L_0 = CylinderTargetAbstractBehaviour_GetScale_m2803(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_TopDiameter()
extern "C" float CylinderTargetAbstractBehaviour_get_TopDiameter_m2798 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameterRatio_21);
		float L_1 = CylinderTargetAbstractBehaviour_GetScale_m2803(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0*(float)L_1));
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_BottomDiameter()
extern "C" float CylinderTargetAbstractBehaviour_get_BottomDiameter_m2799 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameterRatio_22);
		float L_1 = CylinderTargetAbstractBehaviour_GetScale_m2803(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0*(float)L_1));
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetSideLength(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetSideLength_m2800 (CylinderTargetAbstractBehaviour_t36 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		bool L_1 = CylinderTargetAbstractBehaviour_SetScale_m2804(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetTopDiameter(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetTopDiameter_m2801 (CylinderTargetAbstractBehaviour_t36 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameterRatio_21);
		float L_1 = fabsf(L_0);
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = (__this->___mTopDiameterRatio_21);
		bool L_4 = CylinderTargetAbstractBehaviour_SetScale_m2804(__this, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 0;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetBottomDiameter(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetBottomDiameter_m2802 (CylinderTargetAbstractBehaviour_t36 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameterRatio_22);
		float L_1 = fabsf(L_0);
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = (__this->___mBottomDiameterRatio_22);
		bool L_4 = CylinderTargetAbstractBehaviour_SetScale_m2804(__this, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 0;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::OnFrameIndexUpdate(System.Int32)
extern "C" void CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m561 (CylinderTargetAbstractBehaviour_t36 * __this, int32_t ___newFrameIndex, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mUpdateFrameIndex_24);
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = (__this->___mUpdateFrameIndex_24);
		int32_t L_2 = ___newFrameIndex;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		float L_3 = (__this->___mFutureScale_25);
		CylinderTargetAbstractBehaviour_ApplyScale_m2805(__this, L_3, /*hidden argument*/NULL);
		__this->___mUpdateFrameIndex_24 = (-1);
	}

IL_0025:
	{
		int32_t L_4 = ___newFrameIndex;
		__this->___mFrameIndex_23 = L_4;
		return;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::CorrectScaleImpl()
extern "C" bool CylinderTargetAbstractBehaviour_CorrectScaleImpl_m563 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Vector3_t15  V_2 = {0};
	Vector3_t15  V_3 = {0};
	Vector3_t15  V_4 = {0};
	Vector3_t15  V_5 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0092;
	}

IL_0009:
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_localScale_m339(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = V_1;
		float L_3 = Vector3_get_Item_m2311((&V_2), L_2, /*hidden argument*/NULL);
		Vector3_t15 * L_4 = &(((TrackableBehaviour_t44 *)__this)->___mPreviousScale_3);
		int32_t L_5 = V_1;
		float L_6 = Vector3_get_Item_m2311(L_4, L_5, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_6)))
		{
			goto IL_008e;
		}
	}
	{
		Transform_t11 * L_7 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		Transform_t11 * L_8 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t15  L_9 = Transform_get_localScale_m339(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_1;
		float L_11 = Vector3_get_Item_m2311((&V_3), L_10, /*hidden argument*/NULL);
		Transform_t11 * L_12 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t15  L_13 = Transform_get_localScale_m339(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_1;
		float L_15 = Vector3_get_Item_m2311((&V_4), L_14, /*hidden argument*/NULL);
		Transform_t11 * L_16 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t15  L_17 = Transform_get_localScale_m339(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_1;
		float L_19 = Vector3_get_Item_m2311((&V_5), L_18, /*hidden argument*/NULL);
		Vector3_t15  L_20 = {0};
		Vector3__ctor_m249(&L_20, L_11, L_15, L_19, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2265(L_7, L_20, /*hidden argument*/NULL);
		Transform_t11 * L_21 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t15  L_22 = Transform_get_localScale_m339(L_21, /*hidden argument*/NULL);
		((TrackableBehaviour_t44 *)__this)->___mPreviousScale_3 = L_22;
		V_0 = 1;
		goto IL_0099;
	}

IL_008e:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0092:
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) < ((int32_t)3)))
		{
			goto IL_0009;
		}
	}

IL_0099:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m562 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mCylinderTarget_20 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_0;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m589 (CylinderTargetAbstractBehaviour_t36 * __this, Vector3_t15 * ___boundsMin, Vector3_t15 * ___boundsMax, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = CylinderTargetAbstractBehaviour_get_BottomDiameter_m2799(__this, /*hidden argument*/NULL);
		float L_1 = CylinderTargetAbstractBehaviour_get_TopDiameter_m2798(__this, /*hidden argument*/NULL);
		float L_2 = Math_Max_m4344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		V_0 = ((float)((float)L_3*(float)(1.1f)));
		Vector3_t15 * L_4 = ___boundsMin;
		float L_5 = V_0;
		float L_6 = V_0;
		Vector3_t15  L_7 = {0};
		Vector3__ctor_m249(&L_7, ((float)((float)L_5*(float)(-0.5f))), (0.0f), ((float)((float)L_6*(float)(-0.5f))), /*hidden argument*/NULL);
		*L_4 = L_7;
		Vector3_t15 * L_8 = ___boundsMax;
		float L_9 = V_0;
		float L_10 = CylinderTargetAbstractBehaviour_get_SideLength_m2797(__this, /*hidden argument*/NULL);
		float L_11 = V_0;
		Vector3_t15  L_12 = {0};
		Vector3__ctor_m249(&L_12, ((float)((float)L_9*(float)(0.5f))), L_10, ((float)((float)L_11*(float)(0.5f))), /*hidden argument*/NULL);
		*L_8 = L_12;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
extern TypeInfo* ReconstructionFromTarget_t582_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m590 (CylinderTargetAbstractBehaviour_t36 * __this, Object_t * ___reconstructionFromTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionFromTarget_t582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1045);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mCylinderTarget_20);
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		bool L_1 = (((DataSetTrackableBehaviour_t567 *)__this)->___mIsSmartTerrainOccluderOffset_15);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Object_t * L_2 = ___reconstructionFromTarget;
		Object_t * L_3 = (__this->___mCylinderTarget_20);
		Vector3_t15  L_4 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t15  L_5 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t15  L_6 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderOffset_16);
		Quaternion_t13  L_7 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderRotation_17);
		NullCheck(L_2);
		InterfaceFuncInvoker5< bool, Object_t *, Vector3_t15 , Vector3_t15 , Vector3_t15 , Quaternion_t13  >::Invoke(1 /* System.Boolean Vuforia.ReconstructionFromTarget::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion) */, ReconstructionFromTarget_t582_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6, L_7);
		return;
	}

IL_0036:
	{
		Object_t * L_8 = ___reconstructionFromTarget;
		Object_t * L_9 = (__this->___mCylinderTarget_20);
		Vector3_t15  L_10 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t15  L_11 = (((DataSetTrackableBehaviour_t567 *)__this)->___mSmartTerrainOccluderBoundsMax_14);
		NullCheck(L_8);
		InterfaceFuncInvoker3< bool, Object_t *, Vector3_t15 , Vector3_t15  >::Invoke(0 /* System.Boolean Vuforia.ReconstructionFromTarget::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3) */, ReconstructionFromTarget_t582_il2cpp_TypeInfo_var, L_8, L_9, L_10, L_11);
	}

IL_004f:
	{
		return;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::GetScale()
extern "C" float CylinderTargetAbstractBehaviour_GetScale_m2803 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_localScale_m339(L_0, /*hidden argument*/NULL);
		float L_2 = (L_1.___x_1);
		return L_2;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetScale(System.Single)
extern TypeInfo* CylinderTarget_t592_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetAbstractBehaviour_SetScale_m2804 (CylinderTargetAbstractBehaviour_t36 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1050);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t15  L_1 = Transform_get_localScale_m339(L_0, /*hidden argument*/NULL);
		float L_2 = (L_1.___x_1);
		float L_3 = ___value;
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0015;
		}
	}
	{
		return 1;
	}

IL_0015:
	{
		Object_t * L_4 = (__this->___mCylinderTarget_20);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		Object_t * L_5 = (__this->___mCylinderTarget_20);
		float L_6 = ___value;
		NullCheck(L_5);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, float >::Invoke(3 /* System.Boolean Vuforia.CylinderTarget::SetSideLength(System.Single) */, CylinderTarget_t592_il2cpp_TypeInfo_var, L_5, L_6);
		if (L_7)
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		int32_t L_8 = (__this->___mFrameIndex_23);
		__this->___mUpdateFrameIndex_24 = L_8;
		float L_9 = ___value;
		__this->___mFutureScale_25 = L_9;
		goto IL_0049;
	}

IL_0042:
	{
		float L_10 = ___value;
		CylinderTargetAbstractBehaviour_ApplyScale_m2805(__this, L_10, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return 1;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ApplyScale(System.Single)
extern "C" void CylinderTargetAbstractBehaviour_ApplyScale_m2805 (CylinderTargetAbstractBehaviour_t36 * __this, float ___value, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		float L_1 = ___value;
		float L_2 = ___value;
		float L_3 = ___value;
		Vector3_t15  L_4 = {0};
		Vector3__ctor_m249(&L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2265(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.InitializeCylinderTarget(Vuforia.CylinderTarget)
extern TypeInfo* CylinderTarget_t592_il2cpp_TypeInfo_var;
extern TypeInfo* ExtendedTrackable_t794_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m591 (CylinderTargetAbstractBehaviour_t36 * __this, Object_t * ___cylinderTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1050);
		ExtendedTrackable_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___cylinderTarget;
		Object_t * L_1 = L_0;
		V_0 = L_1;
		__this->___mCylinderTarget_20 = L_1;
		Object_t * L_2 = V_0;
		((TrackableBehaviour_t44 *)__this)->___mTrackable_7 = L_2;
		Object_t * L_3 = ___cylinderTarget;
		float L_4 = CylinderTargetAbstractBehaviour_get_SideLength_m2797(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker1< bool, float >::Invoke(3 /* System.Boolean Vuforia.CylinderTarget::SetSideLength(System.Single) */, CylinderTarget_t592_il2cpp_TypeInfo_var, L_3, L_4);
		bool L_5 = (((DataSetTrackableBehaviour_t567 *)__this)->___mExtendedTracking_10);
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		Object_t * L_6 = (__this->___mCylinderTarget_20);
		NullCheck(L_6);
		InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ExtendedTrackable::StartExtendedTracking() */, ExtendedTrackable_t794_il2cpp_TypeInfo_var, L_6);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.SetAspectRatio(System.Single,System.Single)
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m592 (CylinderTargetAbstractBehaviour_t36 * __this, float ___topRatio, float ___bottomRatio, const MethodInfo* method)
{
	{
		float L_0 = ___topRatio;
		__this->___mTopDiameterRatio_21 = L_0;
		float L_1 = ___bottomRatio;
		__this->___mBottomDiameterRatio_22 = L_1;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::.ctor()
extern "C" void CylinderTargetAbstractBehaviour__ctor_m393 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		__this->___mFrameIndex_23 = (-1);
		__this->___mUpdateFrameIndex_24 = (-1);
		DataSetTrackableBehaviour__ctor_m2666(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m555 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m497(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m556 (CylinderTargetAbstractBehaviour_t36 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m240(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m557 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m243(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m558 (CylinderTargetAbstractBehaviour_t36 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m272(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSet
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetMethodDeclarations.h"

// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"


// System.String Vuforia.DataSet::get_Path()
// Vuforia.QCARUnity/StorageType Vuforia.DataSet::get_FileStorageType()
// System.Boolean Vuforia.DataSet::Exists(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSet_Exists_m2806 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral143, L_0, (String_t*) &_stringLiteral144, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = DataSet_Exists_m2808(NULL /*static, unused*/, L_2, 1, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Vuforia.DataSet::Exists(System.String,Vuforia.DataSet/StorageType)
extern "C" bool DataSet_Exists_m2807 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = DataSet_Exists_m2808(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSet::Exists(System.String,Vuforia.QCARUnity/StorageType)
extern "C" bool DataSet_Exists_m2808 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = DataSetImpl_ExistsImpl_m2900(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSet::Load(System.String)
// System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.DataSet/StorageType)
// System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType)
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables()
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,System.String)
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject)
// System.Boolean Vuforia.DataSet::Destroy(Vuforia.Trackable,System.Boolean)
// System.Boolean Vuforia.DataSet::HasReachedTrackableLimit()
// System.Boolean Vuforia.DataSet::Contains(Vuforia.Trackable)
// System.Void Vuforia.DataSet::DestroyAllTrackables(System.Boolean)
// System.Void Vuforia.DataSet::.ctor()
extern "C" void DataSet__ctor_m2809 (DataSet_t594 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstract.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstractMethodDeclarations.h"

// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_20.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"


// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t798_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4345_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4346_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4347_MethodInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_LoadDatasets_m2810 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Enumerator_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1051);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		List_1_GetEnumerator_m4345_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483958);
		Enumerator_get_Current_m4346_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		Enumerator_MoveNext_m4347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483960);
		s_Il2CppMethodIntialized = true;
	}
	ObjectTracker_t574 * V_0 = {0};
	String_t* V_1 = {0};
	DataSet_t594 * V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	Enumerator_t798  V_5 = {0};
	Enumerator_t798  V_6 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_1 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectTracker_t574 * L_2 = (ObjectTracker_t574 *)GenericVirtFuncInvoker0< ObjectTracker_t574 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var, L_1);
		V_0 = L_2;
		List_1_t595 * L_3 = (__this->___mDataSetsToLoad_3);
		NullCheck(L_3);
		Enumerator_t798  L_4 = List_1_GetEnumerator_m4345(L_3, /*hidden argument*/List_1_GetEnumerator_m4345_MethodInfo_var);
		V_5 = L_4;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0155;
		}

IL_0025:
		{
			String_t* L_5 = Enumerator_get_Current_m4346((&V_5), /*hidden argument*/Enumerator_get_Current_m4346_MethodInfo_var);
			V_1 = L_5;
			V_2 = (DataSet_t594 *)NULL;
			String_t* L_6 = V_1;
			bool L_7 = DataSet_Exists_m2806(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0064;
			}
		}

IL_0037:
		{
			ObjectTracker_t574 * L_8 = V_0;
			NullCheck(L_8);
			DataSet_t594 * L_9 = (DataSet_t594 *)VirtFuncInvoker0< DataSet_t594 * >::Invoke(10 /* Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet() */, L_8);
			V_2 = L_9;
			DataSet_t594 * L_10 = V_2;
			String_t* L_11 = V_1;
			NullCheck(L_10);
			bool L_12 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(6 /* System.Boolean Vuforia.DataSet::Load(System.String) */, L_10, L_11);
			if (L_12)
			{
				goto IL_0125;
			}
		}

IL_004a:
		{
			String_t* L_13 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral145, L_13, (String_t*) &_stringLiteral107, /*hidden argument*/NULL);
			Debug_LogError_m403(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			goto IL_0155;
		}

IL_0064:
		{
			List_1_t595 * L_15 = (__this->___mExternalDatasetRoots_5);
			NullCheck(L_15);
			int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_15);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_0125;
			}
		}

IL_0075:
		{
			String_t* L_17 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_18 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral146, L_17, (String_t*) &_stringLiteral147, /*hidden argument*/NULL);
			Debug_Log_m417(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			List_1_t595 * L_19 = (__this->___mExternalDatasetRoots_5);
			NullCheck(L_19);
			Enumerator_t798  L_20 = List_1_GetEnumerator_m4345(L_19, /*hidden argument*/List_1_GetEnumerator_m4345_MethodInfo_var);
			V_6 = L_20;
		}

IL_0097:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00f4;
			}

IL_0099:
			{
				String_t* L_21 = Enumerator_get_Current_m4346((&V_6), /*hidden argument*/Enumerator_get_Current_m4346_MethodInfo_var);
				V_3 = L_21;
				String_t* L_22 = V_3;
				String_t* L_23 = V_1;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_24 = String_Concat_m416(NULL /*static, unused*/, L_22, L_23, (String_t*) &_stringLiteral144, /*hidden argument*/NULL);
				V_4 = L_24;
				String_t* L_25 = V_4;
				bool L_26 = DataSet_Exists_m2808(NULL /*static, unused*/, L_25, 2, /*hidden argument*/NULL);
				if (!L_26)
				{
					goto IL_00f4;
				}
			}

IL_00b9:
			{
				ObjectTracker_t574 * L_27 = V_0;
				NullCheck(L_27);
				DataSet_t594 * L_28 = (DataSet_t594 *)VirtFuncInvoker0< DataSet_t594 * >::Invoke(10 /* Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet() */, L_27);
				V_2 = L_28;
				DataSet_t594 * L_29 = V_2;
				String_t* L_30 = V_4;
				NullCheck(L_29);
				bool L_31 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, L_29, L_30, 2);
				if (L_31)
				{
					goto IL_00e3;
				}
			}

IL_00cb:
			{
				String_t* L_32 = V_4;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_33 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral145, L_32, (String_t*) &_stringLiteral107, /*hidden argument*/NULL);
				Debug_LogError_m403(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
				goto IL_00f4;
			}

IL_00e3:
			{
				String_t* L_34 = V_4;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_35 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral148, L_34, /*hidden argument*/NULL);
				Debug_Log_m417(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			}

IL_00f4:
			{
				bool L_36 = Enumerator_MoveNext_m4347((&V_6), /*hidden argument*/Enumerator_MoveNext_m4347_MethodInfo_var);
				if (L_36)
				{
					goto IL_0099;
				}
			}

IL_00fd:
			{
				IL2CPP_LEAVE(0x10D, FINALLY_00ff);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t140 *)e.ex;
			goto FINALLY_00ff;
		}

FINALLY_00ff:
		{ // begin finally (depth: 2)
			NullCheck(Box(Enumerator_t798_il2cpp_TypeInfo_var, (&V_6)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t798_il2cpp_TypeInfo_var, (&V_6)));
			IL2CPP_END_FINALLY(255)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(255)
		{
			IL2CPP_JUMP_TBL(0x10D, IL_010d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
		}

IL_010d:
		{
			DataSet_t594 * L_37 = V_2;
			if (L_37)
			{
				goto IL_0125;
			}
		}

IL_0110:
		{
			String_t* L_38 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_39 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral149, L_38, (String_t*) &_stringLiteral150, /*hidden argument*/NULL);
			Debug_LogError_m403(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		}

IL_0125:
		{
			List_1_t595 * L_40 = (__this->___mDataSetsToActivate_4);
			String_t* L_41 = V_1;
			NullCheck(L_40);
			bool L_42 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0) */, L_40, L_41);
			if (!L_42)
			{
				goto IL_0155;
			}
		}

IL_0133:
		{
			DataSet_t594 * L_43 = V_2;
			if (!L_43)
			{
				goto IL_0140;
			}
		}

IL_0136:
		{
			ObjectTracker_t574 * L_44 = V_0;
			DataSet_t594 * L_45 = V_2;
			NullCheck(L_44);
			VirtFuncInvoker1< bool, DataSet_t594 * >::Invoke(12 /* System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet) */, L_44, L_45);
			goto IL_0155;
		}

IL_0140:
		{
			String_t* L_46 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_47 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral151, L_46, (String_t*) &_stringLiteral152, /*hidden argument*/NULL);
			Debug_LogError_m403(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		}

IL_0155:
		{
			bool L_48 = Enumerator_MoveNext_m4347((&V_5), /*hidden argument*/Enumerator_MoveNext_m4347_MethodInfo_var);
			if (L_48)
			{
				goto IL_0025;
			}
		}

IL_0161:
		{
			IL2CPP_LEAVE(0x171, FINALLY_0163);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0163;
	}

FINALLY_0163:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t798_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t798_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(355)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(355)
	{
		IL2CPP_JUMP_TBL(0x171, IL_0171)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0171:
	{
		__this->___mDatasetsLoaded_2 = 1;
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2811 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2812 (DataSetLoadAbstractBehaviour_t38 * __this, String_t* ___searchDir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___searchDir;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_1 = ___searchDir;
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_1, (String_t*) &_stringLiteral122);
		if (!L_2)
		{
			goto IL_0011;
		}
	}

IL_0010:
	{
		return;
	}

IL_0011:
	{
		String_t* L_3 = ___searchDir;
		NullCheck(L_3);
		bool L_4 = String_EndsWith_m4348(L_3, (String_t*) &_stringLiteral33, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_5 = ___searchDir;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m323(NULL /*static, unused*/, L_5, (String_t*) &_stringLiteral33, /*hidden argument*/NULL);
		___searchDir = L_6;
	}

IL_002b:
	{
		List_1_t595 * L_7 = (__this->___mExternalDatasetRoots_5);
		String_t* L_8 = ___searchDir;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_8);
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_Start_m2813 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		bool L_0 = (__this->___mDatasetsLoaded_2);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_2, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m397(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = QCARAbstractBehaviour_get_HasStarted_m4127(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		DataSetLoadAbstractBehaviour_LoadDatasets_m2810(__this, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
extern TypeInfo* List_1_t595_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4349_MethodInfo_var;
extern "C" void DataSetLoadAbstractBehaviour__ctor_m394 (DataSetLoadAbstractBehaviour_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		List_1__ctor_m4349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t595 * L_0 = (List_1_t595 *)il2cpp_codegen_object_new (List_1_t595_il2cpp_TypeInfo_var);
		List_1__ctor_m4349(L_0, /*hidden argument*/List_1__ctor_m4349_MethodInfo_var);
		__this->___mDataSetsToLoad_3 = L_0;
		List_1_t595 * L_1 = (List_1_t595 *)il2cpp_codegen_object_new (List_1_t595_il2cpp_TypeInfo_var);
		List_1__ctor_m4349(L_1, /*hidden argument*/List_1__ctor_m4349_MethodInfo_var);
		__this->___mDataSetsToActivate_4 = L_1;
		List_1_t595 * L_2 = (List_1_t595 *)il2cpp_codegen_object_new (List_1_t595_il2cpp_TypeInfo_var);
		List_1__ctor_m4349(L_2, /*hidden argument*/List_1__ctor_m4349_MethodInfo_var);
		__this->___mExternalDatasetRoots_5 = L_2;
		MonoBehaviour__ctor_m237(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleDataMethodDeclarations.h"



// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntDataMethodDeclarations.h"



// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBoxMethodDeclarations.h"



// System.Void Vuforia.OrientedBoundingBox::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern TypeInfo* OrientedBoundingBox_t598_il2cpp_TypeInfo_var;
extern "C" void OrientedBoundingBox__ctor_m2814 (OrientedBoundingBox_t598 * __this, Vector2_t10  ___center, Vector2_t10  ___halfExtents, float ___rotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OrientedBoundingBox_t598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	{
		Initobj (OrientedBoundingBox_t598_il2cpp_TypeInfo_var, __this);
		Vector2_t10  L_0 = ___center;
		OrientedBoundingBox_set_Center_m2816(__this, L_0, /*hidden argument*/NULL);
		Vector2_t10  L_1 = ___halfExtents;
		OrientedBoundingBox_set_HalfExtents_m2818(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___rotation;
		OrientedBoundingBox_set_Rotation_m2820(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_Center()
extern "C" Vector2_t10  OrientedBoundingBox_get_Center_m2815 (OrientedBoundingBox_t598 * __this, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = (__this->___U3CCenterU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_Center(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_Center_m2816 (OrientedBoundingBox_t598 * __this, Vector2_t10  ___value, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = ___value;
		__this->___U3CCenterU3Ek__BackingField_0 = L_0;
		return;
	}
}
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_HalfExtents()
extern "C" Vector2_t10  OrientedBoundingBox_get_HalfExtents_m2817 (OrientedBoundingBox_t598 * __this, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = (__this->___U3CHalfExtentsU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_HalfExtents(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_HalfExtents_m2818 (OrientedBoundingBox_t598 * __this, Vector2_t10  ___value, const MethodInfo* method)
{
	{
		Vector2_t10  L_0 = ___value;
		__this->___U3CHalfExtentsU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Single Vuforia.OrientedBoundingBox::get_Rotation()
extern "C" float OrientedBoundingBox_get_Rotation_m2819 (OrientedBoundingBox_t598 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CRotationU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_Rotation(System.Single)
extern "C" void OrientedBoundingBox_set_Rotation_m2820 (OrientedBoundingBox_t598 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CRotationU3Ek__BackingField_2 = L_0;
		return;
	}
}
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0MethodDeclarations.h"



// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern TypeInfo* OrientedBoundingBox3D_t599_il2cpp_TypeInfo_var;
extern "C" void OrientedBoundingBox3D__ctor_m2821 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___center, Vector3_t15  ___halfExtents, float ___rotationY, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OrientedBoundingBox3D_t599_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1054);
		s_Il2CppMethodIntialized = true;
	}
	{
		Initobj (OrientedBoundingBox3D_t599_il2cpp_TypeInfo_var, __this);
		Vector3_t15  L_0 = ___center;
		OrientedBoundingBox3D_set_Center_m2823(__this, L_0, /*hidden argument*/NULL);
		Vector3_t15  L_1 = ___halfExtents;
		OrientedBoundingBox3D_set_HalfExtents_m2825(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___rotationY;
		OrientedBoundingBox3D_set_RotationY_m2827(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t15  OrientedBoundingBox3D_get_Center_m2822 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___U3CCenterU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m2823 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___value, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___value;
		__this->___U3CCenterU3Ek__BackingField_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t15  OrientedBoundingBox3D_get_HalfExtents_m2824 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___U3CHalfExtentsU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m2825 (OrientedBoundingBox3D_t599 * __this, Vector3_t15  ___value, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = ___value;
		__this->___U3CHalfExtentsU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m2826 (OrientedBoundingBox3D_t599 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CRotationYU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m2827 (OrientedBoundingBox3D_t599 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CRotationYU3Ek__BackingField_2 = L_0;
		return;
	}
}
// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF_0MethodDeclarations.h"

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"


// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t60 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2828 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MaskOutAbstractBehaviour_t60 *)NULL;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t86 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2829 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (VirtualButtonAbstractBehaviour_t86 *)NULL;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t77 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2830 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (TurnOffAbstractBehaviour_t77 *)NULL;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t50 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2831 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (ImageTargetAbstractBehaviour_t50 *)NULL;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t58 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2832 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MarkerAbstractBehaviour_t58 *)NULL;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t62 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2833 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MultiTargetAbstractBehaviour_t62 *)NULL;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t36 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2834 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (CylinderTargetAbstractBehaviour_t36 *)NULL;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t93 * NullBehaviourComponentFactory_AddWordBehaviour_m2835 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (WordAbstractBehaviour_t93 *)NULL;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t75 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2836 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (TextRecoAbstractBehaviour_t75 *)NULL;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t64 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2837 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (ObjectTargetAbstractBehaviour_t64 *)NULL;
	}
}
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m2838 (NullBehaviourComponentFactory_t600 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"



// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
extern TypeInfo* BehaviourComponentFactory_t601_il2cpp_TypeInfo_var;
extern TypeInfo* NullBehaviourComponentFactory_t600_il2cpp_TypeInfo_var;
extern "C" Object_t * BehaviourComponentFactory_get_Instance_m2839 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BehaviourComponentFactory_t601_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		NullBehaviourComponentFactory_t600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1056);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((BehaviourComponentFactory_t601_StaticFields*)BehaviourComponentFactory_t601_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullBehaviourComponentFactory_t600 * L_1 = (NullBehaviourComponentFactory_t600 *)il2cpp_codegen_object_new (NullBehaviourComponentFactory_t600_il2cpp_TypeInfo_var);
		NullBehaviourComponentFactory__ctor_m2838(L_1, /*hidden argument*/NULL);
		((BehaviourComponentFactory_t601_StaticFields*)BehaviourComponentFactory_t601_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((BehaviourComponentFactory_t601_StaticFields*)BehaviourComponentFactory_t601_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
extern TypeInfo* BehaviourComponentFactory_t601_il2cpp_TypeInfo_var;
extern "C" void BehaviourComponentFactory_set_Instance_m435 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BehaviourComponentFactory_t601_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((BehaviourComponentFactory_t601_StaticFields*)BehaviourComponentFactory_t601_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
extern "C" void BehaviourComponentFactory__ctor_m2840 (BehaviourComponentFactory_t601 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CloudRecoImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarge.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoImageTargeMethodDeclarations.h"

// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_gen_21MethodDeclarations.h"


// System.Void Vuforia.CloudRecoImageTargetImpl::.ctor(System.String,System.Int32,UnityEngine.Vector3)
extern "C" void CloudRecoImageTargetImpl__ctor_m2841 (CloudRecoImageTargetImpl_t602 * __this, String_t* ___name, int32_t ___id, Vector3_t15  ___size, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2712(__this, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t15  L_2 = ___size;
		__this->___mSize_2 = L_2;
		return;
	}
}
// Vuforia.ImageTargetType Vuforia.CloudRecoImageTargetImpl::get_ImageTargetType()
extern "C" int32_t CloudRecoImageTargetImpl_get_ImageTargetType_m2842 (CloudRecoImageTargetImpl_t602 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::GetSize()
extern "C" Vector3_t15  CloudRecoImageTargetImpl_GetSize_m2843 (CloudRecoImageTargetImpl_t602 * __this, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoImageTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void CloudRecoImageTargetImpl_SetSize_m2844 (CloudRecoImageTargetImpl_t602 * __this, Vector3_t15  ___size, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral153, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
extern "C" VirtualButton_t731 * CloudRecoImageTargetImpl_CreateVirtualButton_m2845 (CloudRecoImageTargetImpl_t602 * __this, String_t* ___name, RectangleData_t596  ___area, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral154, /*hidden argument*/NULL);
		return (VirtualButton_t731 *)NULL;
	}
}
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::GetVirtualButtonByName(System.String)
extern "C" VirtualButton_t731 * CloudRecoImageTargetImpl_GetVirtualButtonByName_m2846 (CloudRecoImageTargetImpl_t602 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral154, /*hidden argument*/NULL);
		return (VirtualButton_t731 *)NULL;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.CloudRecoImageTargetImpl::GetVirtualButtons()
extern TypeInfo* List_1_t799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4350_MethodInfo_var;
extern "C" Object_t* CloudRecoImageTargetImpl_GetVirtualButtons_m2847 (CloudRecoImageTargetImpl_t602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		List_1__ctor_m4350_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483962);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral154, /*hidden argument*/NULL);
		List_1_t799 * L_0 = (List_1_t799 *)il2cpp_codegen_object_new (List_1_t799_il2cpp_TypeInfo_var);
		List_1__ctor_m4350(L_0, /*hidden argument*/List_1__ctor_m4350_MethodInfo_var);
		return L_0;
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
extern "C" bool CloudRecoImageTargetImpl_DestroyVirtualButton_m2848 (CloudRecoImageTargetImpl_t602 * __this, VirtualButton_t731 * ___vb, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral154, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CloudRecoImageTargetImpl_StartExtendedTracking_m2849 (CloudRecoImageTargetImpl_t602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CloudRecoImageTargetImpl_StopExtendedTracking_m2850 (CloudRecoImageTargetImpl_t602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetImplMethodDeclarations.h"



// System.Void Vuforia.CylinderTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetImpl__ctor_m2851 (CylinderTargetImpl_t603 * __this, String_t* ___name, int32_t ___id, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	IntPtr_t V_1 = {0};
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t594 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2717(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, 3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)3*(int32_t)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_7 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_7);
		IntPtr_t L_8 = DataSetImpl_get_DataSetPtr_m2886(L_7, /*hidden argument*/NULL);
		String_t* L_9 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_1;
		NullCheck(L_6);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(32 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_6, L_8, L_9, L_10);
		IntPtr_t L_11 = V_1;
		SingleU5BU5D_t585* L_12 = V_0;
		Marshal_Copy_m4274(NULL /*static, unused*/, L_11, L_12, 0, 3, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		__this->___mSideLength_4 = (*(float*)(float*)SZArrayLdElema(L_14, L_15));
		SingleU5BU5D_t585* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		int32_t L_17 = 1;
		__this->___mTopDiameter_5 = (*(float*)(float*)SZArrayLdElema(L_16, L_17));
		SingleU5BU5D_t585* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		int32_t L_19 = 2;
		__this->___mBottomDiameter_6 = (*(float*)(float*)SZArrayLdElema(L_18, L_19));
		return;
	}
}
// System.Void Vuforia.CylinderTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void CylinderTargetImpl_SetSize_m2852 (CylinderTargetImpl_t603 * __this, Vector3_t15  ___size, const MethodInfo* method)
{
	Vector3_t15  V_0 = {0};
	{
		float L_0 = Vector3_get_Item_m2311((&___size), 0, /*hidden argument*/NULL);
		Vector3_t15  L_1 = (Vector3_t15 )VirtFuncInvoker0< Vector3_t15  >::Invoke(10 /* UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize() */, __this);
		V_0 = L_1;
		float L_2 = Vector3_get_Item_m2311((&V_0), 0, /*hidden argument*/NULL);
		CylinderTargetImpl_ScaleCylinder_m2859(__this, ((float)((float)L_0/(float)L_2)), /*hidden argument*/NULL);
		Vector3_t15  L_3 = ___size;
		ObjectTargetImpl_SetSize_m2719(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetSideLength()
extern "C" float CylinderTargetImpl_GetSideLength_m2853 (CylinderTargetImpl_t603 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSideLength_4);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetTopDiameter()
extern "C" float CylinderTargetImpl_GetTopDiameter_m2854 (CylinderTargetImpl_t603 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameter_5);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetBottomDiameter()
extern "C" float CylinderTargetImpl_GetBottomDiameter_m2855 (CylinderTargetImpl_t603 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameter_6);
		return L_0;
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetSideLength(System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetSideLength_m2856 (CylinderTargetImpl_t603 * __this, float ___sideLength, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___sideLength;
		float L_1 = (__this->___mSideLength_4);
		CylinderTargetImpl_ScaleCylinder_m2859(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_3 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		float L_6 = ___sideLength;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(33 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetTopDiameter(System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetTopDiameter_m2857 (CylinderTargetImpl_t603 * __this, float ___topDiameter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___topDiameter;
		float L_1 = (__this->___mTopDiameter_5);
		CylinderTargetImpl_ScaleCylinder_m2859(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_3 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		float L_6 = ___topDiameter;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(34 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetBottomDiameter(System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetBottomDiameter_m2858 (CylinderTargetImpl_t603 * __this, float ___bottomDiameter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___bottomDiameter;
		float L_1 = (__this->___mBottomDiameter_6);
		CylinderTargetImpl_ScaleCylinder_m2859(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_3 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		float L_6 = ___bottomDiameter;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(35 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.CylinderTargetImpl::ScaleCylinder(System.Single)
extern "C" void CylinderTargetImpl_ScaleCylinder_m2859 (CylinderTargetImpl_t603 * __this, float ___scale, const MethodInfo* method)
{
	{
		Vector3_t15  L_0 = (((ObjectTargetImpl_t579 *)__this)->___mSize_2);
		float L_1 = ___scale;
		Vector3_t15  L_2 = Vector3_op_Multiply_m2363(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((ObjectTargetImpl_t579 *)__this)->___mSize_2 = L_2;
		float L_3 = (__this->___mSideLength_4);
		float L_4 = ___scale;
		__this->___mSideLength_4 = ((float)((float)L_3*(float)L_4));
		float L_5 = (__this->___mTopDiameter_5);
		float L_6 = ___scale;
		__this->___mTopDiameter_5 = ((float)((float)L_5*(float)L_6));
		float L_7 = (__this->___mBottomDiameter_6);
		float L_8 = ___scale;
		__this->___mBottomDiameter_6 = ((float)((float)L_7*(float)L_8));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetTypeMethodDeclarations.h"



// Vuforia.ImageTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetData.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetDataMethodDeclarations.h"



// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_MethodDeclarations.h"



// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderMethodDeclarations.h"



// System.Boolean Vuforia.ImageTargetBuilder::Build(System.String,System.Single)
// System.Void Vuforia.ImageTargetBuilder::StartScan()
// System.Void Vuforia.ImageTargetBuilder::StopScan()
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilder::GetFrameQuality()
// Vuforia.TrackableSource Vuforia.ImageTargetBuilder::GetTrackableSource()
// System.Void Vuforia.ImageTargetBuilder::.ctor()
extern "C" void ImageTargetBuilder__ctor_m2860 (ImageTargetBuilder_t607 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImpl.h"
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4.h"
// Vuforia.ImageImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImpl.h"
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_gen_22.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl.h"
// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImplMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4MethodDeclarations.h"
// Vuforia.ImageImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImplMethodDeclarations.h"
// Vuforia.Image
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_gen_22MethodDeclarations.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImplMethodDeclarations.h"


// Vuforia.WebCamImpl Vuforia.CameraDeviceImpl::get_WebCam()
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern "C" WebCamImpl_t610 * CameraDeviceImpl_get_WebCam_m2861 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_0 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		return L_0;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::get_CameraReady()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_get_CameraReady_m2862 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_1 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_2 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		bool L_3 = WebCamImpl_get_IsTextureSizeAvailable_m4082(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0019:
	{
		return 0;
	}

IL_001b:
	{
		bool L_4 = (__this->___mCameraReady_4);
		return L_4;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Init(Vuforia.CameraDevice/CameraDirection)
extern const Il2CppType* QCARAbstractBehaviour_t67_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Init_m2863 (CameraDeviceImpl_t611 * __this, int32_t ___cameraDirection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t67_0_0_0_var = il2cpp_codegen_type_from_index(38);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t67 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		___cameraDirection = 1;
	}

IL_000a:
	{
		int32_t L_1 = ___cameraDirection;
		int32_t L_2 = CameraDeviceImpl_InitCameraDevice_m2881(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		int32_t L_3 = ___cameraDirection;
		__this->___mCameraDirection_6 = L_3;
		__this->___mCameraReady_4 = 1;
		bool L_4 = CameraDeviceImpl_get_CameraReady_m2862(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t67_0_0_0_var), /*hidden argument*/NULL);
		Object_t123 * L_6 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t67 *)Castclass(L_6, QCARAbstractBehaviour_t67_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t67 * L_7 = V_0;
		bool L_8 = Object_op_Implicit_m397(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		QCARAbstractBehaviour_t67 * L_9 = V_0;
		NullCheck(L_9);
		QCARAbstractBehaviour_ResetBackgroundPlane_m4155(L_9, 1, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t67 * L_10 = V_0;
		NullCheck(L_10);
		QCARAbstractBehaviour_ConfigureVideoBackground_m4154(L_10, 1, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Deinit()
extern "C" bool CameraDeviceImpl_Deinit_m2864 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = CameraDeviceImpl_DeinitCameraDevice_m2882(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		__this->___mCameraReady_4 = 0;
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Start()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Start_m2865 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mIsDirty_5 = 1;
		Color_t90  L_0 = {0};
		Color__ctor_m241(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		GL_Clear_m4351(NULL /*static, unused*/, 0, 1, L_0, /*hidden argument*/NULL);
		int32_t L_1 = CameraDeviceImpl_StartCameraDevice_m2883(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		return 0;
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_3 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		CameraDeviceImpl_ForceFrameFormat_m2880(__this, ((int32_t)16), 1, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Stop()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Stop_m2866 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		CameraDeviceImpl_ForceFrameFormat_m2880(__this, ((int32_t)16), 0, /*hidden argument*/NULL);
	}

IL_0010:
	{
		int32_t L_1 = CameraDeviceImpl_StopCameraDevice_m2884(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		return 0;
	}

IL_001a:
	{
		return 1;
	}
}
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode()
extern "C" VideoModeData_t572  CameraDeviceImpl_GetVideoMode_m2867 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mVideoModeDataNeedsUpdate_9);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = (__this->___mCameraDeviceMode_7);
		VideoModeData_t572  L_2 = (VideoModeData_t572 )VirtFuncInvoker1< VideoModeData_t572 , int32_t >::Invoke(9 /* Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode) */, __this, L_1);
		__this->___mVideoModeData_8 = L_2;
		__this->___mVideoModeDataNeedsUpdate_9 = 0;
	}

IL_0021:
	{
		VideoModeData_t572  L_3 = (__this->___mVideoModeData_8);
		return L_3;
	}
}
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
extern const Il2CppType* VideoModeData_t572_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* VideoModeData_t572_il2cpp_TypeInfo_var;
extern "C" VideoModeData_t572  CameraDeviceImpl_GetVideoMode_m2868 (CameraDeviceImpl_t611 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoModeData_t572_0_0_0_var = il2cpp_codegen_type_from_index(1059);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		VideoModeData_t572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1059);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	VideoModeData_t572  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		WebCamImpl_t610 * L_1 = CameraDeviceImpl_get_WebCam_m2861(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VideoModeData_t572  L_2 = WebCamImpl_GetVideoMode_m4093(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoModeData_t572_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = ___mode;
		IntPtr_t L_8 = V_0;
		NullCheck(L_6);
		InterfaceActionInvoker2< int32_t, IntPtr_t >::Invoke(5 /* System.Void Vuforia.IQCARWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_6, L_7, L_8);
		IntPtr_t L_9 = V_0;
		Type_t * L_10 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoModeData_t572_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_11 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_1 = ((*(VideoModeData_t572 *)((VideoModeData_t572 *)UnBox (L_11, VideoModeData_t572_il2cpp_TypeInfo_var))));
		IntPtr_t L_12 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		VideoModeData_t572  L_13 = V_1;
		return L_13;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SelectVideoMode_m2869 (CameraDeviceImpl_t611 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___mode;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(6 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSelectVideoMode(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		int32_t L_3 = ___mode;
		__this->___mCameraDeviceMode_7 = L_3;
		__this->___mHasCameraDeviceModeBeenSet_10 = 1;
		__this->___mVideoModeDataNeedsUpdate_9 = 1;
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
extern "C" bool CameraDeviceImpl_GetSelectedVideoMode_m2870 (CameraDeviceImpl_t611 * __this, int32_t* ___mode, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___mode;
		int32_t L_1 = (__this->___mCameraDeviceMode_7);
		*((int32_t*)(L_0)) = (int32_t)L_1;
		bool L_2 = (__this->___mHasCameraDeviceModeBeenSet_10);
		return L_2;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFlashTorchMode(System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFlashTorchMode_m2871 (CameraDeviceImpl_t611 * __this, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Object_t * G_B3_1 = {0};
	String_t* G_B5_0 = {0};
	String_t* G_B4_0 = {0};
	String_t* G_B6_0 = {0};
	String_t* G_B6_1 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B8_1 = {0};
	String_t* G_B8_2 = {0};
	String_t* G_B7_0 = {0};
	String_t* G_B7_1 = {0};
	String_t* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	String_t* G_B9_1 = {0};
	String_t* G_B9_2 = {0};
	String_t* G_B9_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___on;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		NullCheck(G_B3_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(7 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetFlashTorchMode(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, G_B3_1, G_B3_0);
		V_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = ___on;
		G_B4_0 = (String_t*) &_stringLiteral155;
		if (L_3)
		{
			G_B5_0 = (String_t*) &_stringLiteral155;
			goto IL_0027;
		}
	}
	{
		G_B6_0 = (String_t*) &_stringLiteral156;
		G_B6_1 = G_B4_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B6_0 = (String_t*) &_stringLiteral157;
		G_B6_1 = G_B5_0;
	}

IL_002c:
	{
		bool L_4 = V_0;
		G_B7_0 = (String_t*) &_stringLiteral158;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		if (L_4)
		{
			G_B8_0 = (String_t*) &_stringLiteral158;
			G_B8_1 = G_B6_0;
			G_B8_2 = G_B6_1;
			goto IL_003b;
		}
	}
	{
		G_B9_0 = (String_t*) &_stringLiteral159;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0040;
	}

IL_003b:
	{
		G_B9_0 = (String_t*) &_stringLiteral160;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m4352(NULL /*static, unused*/, G_B9_3, G_B9_2, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFocusMode(Vuforia.CameraDevice/FocusMode)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* FocusMode_t570_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFocusMode_m2872 (CameraDeviceImpl_t611 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		FocusMode_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1060);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Object_t * G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	String_t* G_B3_0 = {0};
	Object_t * G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___mode;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetFocusMode(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_3 = ___mode;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(FocusMode_t570_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = V_0;
		G_B1_0 = L_5;
		G_B1_1 = (String_t*) &_stringLiteral161;
		if (L_6)
		{
			G_B2_0 = L_5;
			G_B2_1 = (String_t*) &_stringLiteral161;
			goto IL_0027;
		}
	}
	{
		G_B3_0 = (String_t*) &_stringLiteral162;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (String_t*) &_stringLiteral163;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1948(NULL /*static, unused*/, G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t618_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFrameFormat_m2873 (CameraDeviceImpl_t611 * __this, int32_t ___format, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		ImageImpl_t618_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		s_Il2CppMethodIntialized = true;
	}
	Image_t615 * V_0 = {0};
	{
		bool L_0 = ___enabled;
		if (!L_0)
		{
			goto IL_0047;
		}
	}
	{
		Dictionary_2_t608 * L_1 = (__this->___mCameraImages_1);
		int32_t L_2 = ___format;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_1, L_2);
		if (L_3)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___format;
		NullCheck(L_4);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(10 /* System.Int32 Vuforia.IQCARWrapper::QcarSetFrameFormat(System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5, 1);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral164, /*hidden argument*/NULL);
		return 0;
	}

IL_002b:
	{
		ImageImpl_t618 * L_7 = (ImageImpl_t618 *)il2cpp_codegen_object_new (ImageImpl_t618_il2cpp_TypeInfo_var);
		ImageImpl__ctor_m2922(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		Image_t615 * L_8 = V_0;
		int32_t L_9 = ___format;
		NullCheck(L_8);
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT) */, L_8, L_9);
		Dictionary_2_t608 * L_10 = (__this->___mCameraImages_1);
		int32_t L_11 = ___format;
		Image_t615 * L_12 = V_0;
		NullCheck(L_10);
		VirtActionInvoker2< int32_t, Image_t615 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Add(!0,!1) */, L_10, L_11, L_12);
		return 1;
	}

IL_0047:
	{
		Dictionary_2_t608 * L_13 = (__this->___mCameraImages_1);
		int32_t L_14 = ___format;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_13, L_14);
		if (!L_15)
		{
			goto IL_008a;
		}
	}
	{
		List_1_t609 * L_16 = (__this->___mForcedCameraFormats_2);
		int32_t L_17 = ___format;
		NullCheck(L_16);
		bool L_18 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_16, L_17);
		if (L_18)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_19 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = ___format;
		NullCheck(L_19);
		int32_t L_21 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(10 /* System.Int32 Vuforia.IQCARWrapper::QcarSetFrameFormat(System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_19, L_20, 0);
		if (L_21)
		{
			goto IL_007d;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral164, /*hidden argument*/NULL);
		return 0;
	}

IL_007d:
	{
		Dictionary_2_t608 * L_22 = (__this->___mCameraImages_1);
		int32_t L_23 = ___format;
		NullCheck(L_22);
		bool L_24 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Remove(!0) */, L_22, L_23);
		return L_24;
	}

IL_008a:
	{
		return 1;
	}
}
// Vuforia.Image Vuforia.CameraDeviceImpl::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
extern "C" Image_t615 * CameraDeviceImpl_GetCameraImage_m2874 (CameraDeviceImpl_t611 * __this, int32_t ___format, const MethodInfo* method)
{
	Image_t615 * V_0 = {0};
	{
		Dictionary_2_t608 * L_0 = (__this->___mCameraImages_1);
		int32_t L_1 = ___format;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Dictionary_2_t608 * L_3 = (__this->___mCameraImages_1);
		int32_t L_4 = ___format;
		NullCheck(L_3);
		Image_t615 * L_5 = (Image_t615 *)VirtFuncInvoker1< Image_t615 *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Item(!0) */, L_3, L_4);
		V_0 = L_5;
		Image_t615 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean Vuforia.Image::IsValid() */, L_6);
		if (!L_7)
		{
			goto IL_0025;
		}
	}
	{
		Image_t615 * L_8 = V_0;
		return L_8;
	}

IL_0025:
	{
		return (Image_t615 *)NULL;
	}
}
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::GetCameraDirection()
extern "C" int32_t CameraDeviceImpl_GetCameraDirection_m2875 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mCameraDirection_6);
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::GetAllImages()
extern "C" Dictionary_2_t608 * CameraDeviceImpl_GetAllImages_m2876 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t608 * L_0 = (__this->___mCameraImages_1);
		return L_0;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::IsDirty()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_IsDirty_m2877 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool L_1 = (__this->___mIsDirty_5);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		WebCamImpl_t610 * L_2 = CameraDeviceImpl_get_WebCam_m2861(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = WebCamImpl_IsRendererDirty_m4095(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001b:
	{
		return 1;
	}

IL_001d:
	{
		bool L_4 = (__this->___mIsDirty_5);
		return L_4;
	}
}
// System.Void Vuforia.CameraDeviceImpl::ResetDirtyFlag()
extern "C" void CameraDeviceImpl_ResetDirtyFlag_m2878 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	{
		__this->___mIsDirty_5 = 0;
		return;
	}
}
// System.Void Vuforia.CameraDeviceImpl::.ctor()
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t608_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t609_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4353_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4354_MethodInfo_var;
extern "C" void CameraDeviceImpl__ctor_m2879 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		Dictionary_2_t608_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1064);
		List_1_t609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		Dictionary_2__ctor_m4353_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483963);
		List_1__ctor_m4354_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mCameraDeviceMode_7 = (-1);
		__this->___mVideoModeDataNeedsUpdate_9 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice__ctor_m2674(__this, /*hidden argument*/NULL);
		Dictionary_2_t608 * L_0 = (Dictionary_2_t608 *)il2cpp_codegen_object_new (Dictionary_2_t608_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4353(L_0, /*hidden argument*/Dictionary_2__ctor_m4353_MethodInfo_var);
		__this->___mCameraImages_1 = L_0;
		List_1_t609 * L_1 = (List_1_t609 *)il2cpp_codegen_object_new (List_1_t609_il2cpp_TypeInfo_var);
		List_1__ctor_m4354(L_1, /*hidden argument*/List_1__ctor_m4354_MethodInfo_var);
		__this->___mForcedCameraFormats_2 = L_1;
		return;
	}
}
// System.Void Vuforia.CameraDeviceImpl::ForceFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
extern "C" void CameraDeviceImpl_ForceFrameFormat_m2880 (CameraDeviceImpl_t611 * __this, int32_t ___format, bool ___enabled, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ___enabled;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t609 * L_1 = (__this->___mForcedCameraFormats_2);
		int32_t L_2 = ___format;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_1, L_2);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t609 * L_4 = (__this->___mForcedCameraFormats_2);
		int32_t L_5 = ___format;
		NullCheck(L_4);
		VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(!0) */, L_4, L_5);
	}

IL_001e:
	{
		int32_t L_6 = ___format;
		bool L_7 = ___enabled;
		bool L_8 = (bool)VirtFuncInvoker2< bool, int32_t, bool >::Invoke(14 /* System.Boolean Vuforia.CameraDevice::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean) */, __this, L_6, L_7);
		V_0 = L_8;
		bool L_9 = ___enabled;
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t609 * L_11 = (__this->___mForcedCameraFormats_2);
		int32_t L_12 = ___format;
		NullCheck(L_11);
		bool L_13 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t609 * L_14 = (__this->___mForcedCameraFormats_2);
		int32_t L_15 = ___format;
		NullCheck(L_14);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(!0) */, L_14, L_15);
	}

IL_0047:
	{
		return;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::InitCameraDevice(System.Int32)
extern const Il2CppType* WebCamAbstractBehaviour_t88_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* WebCamAbstractBehaviour_t88_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* NullReferenceException_t800_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_InitCameraDevice_m2881 (CameraDeviceImpl_t611 * __this, int32_t ___camera, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WebCamAbstractBehaviour_t88_0_0_0_var = il2cpp_codegen_type_from_index(260);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		WebCamAbstractBehaviour_t88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		NullReferenceException_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	WebCamAbstractBehaviour_t88 * V_1 = {0};
	Vec2I_t663  V_2 = {0};
	NullReferenceException_t800 * V_3 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		V_0 = 0;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(WebCamAbstractBehaviour_t88_0_0_0_var), /*hidden argument*/NULL);
			Object_t123 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			V_1 = ((WebCamAbstractBehaviour_t88 *)Castclass(L_2, WebCamAbstractBehaviour_t88_il2cpp_TypeInfo_var));
			WebCamAbstractBehaviour_t88 * L_3 = V_1;
			NullCheck(L_3);
			WebCamAbstractBehaviour_InitCamera_m4264(L_3, /*hidden argument*/NULL);
			WebCamAbstractBehaviour_t88 * L_4 = V_1;
			NullCheck(L_4);
			WebCamImpl_t610 * L_5 = WebCamAbstractBehaviour_get_ImplementationClass_m4263(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
			((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3 = L_5;
			WebCamImpl_t610 * L_6 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
			NullCheck(L_6);
			Vec2I_t663  L_7 = WebCamImpl_get_ResampledTextureSize_m4085(L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			int32_t L_8 = ((&V_2)->___y_1);
			if (!L_8)
			{
				goto IL_005f;
			}
		}

IL_0046:
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
			Object_t * L_9 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
			int32_t L_10 = ((&V_2)->___x_0);
			int32_t L_11 = ((&V_2)->___y_1);
			NullCheck(L_9);
			InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(9 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		}

IL_005f:
		{
			V_0 = 1;
			goto IL_0071;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t140 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t800_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0063;
		throw e;
	}

CATCH_0063:
	{ // begin catch(System.NullReferenceException)
		V_3 = ((NullReferenceException_t800 *)__exception_local);
		NullReferenceException_t800 * L_12 = V_3;
		NullCheck(L_12);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_12);
		Debug_LogError_m403(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_0071;
	} // end catch (depth: 1)

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_14 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = ___camera;
		NullCheck(L_14);
		InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceInitCamera(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_14, L_15);
		Dictionary_2_t608 * L_16 = (__this->___mCameraImages_1);
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Clear() */, L_16);
		int32_t L_17 = V_0;
		return L_17;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_18 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = ___camera;
		NullCheck(L_18);
		int32_t L_20 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceInitCamera(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_18, L_19);
		return L_20;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::DeinitCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t660_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_DeinitCameraDevice_m2882 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		QCARManagerImpl_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_1 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_2 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StopCamera_m4089(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceDeinitCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_5 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t660 *)Castclass(L_5, QCARManagerImpl_t660_il2cpp_TypeInfo_var)));
		QCARManagerImpl_SetStatesToDiscard_m3021(((QCARManagerImpl_t660 *)Castclass(L_5, QCARManagerImpl_t660_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceDeinitCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_6);
		return L_7;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::StartCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_StartCameraDevice_m2883 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_1 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_2 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StartCamera_m4088(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStartCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStartCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5);
		return L_6;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::StopCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_StopCameraDevice_m2884 (CameraDeviceImpl_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_1 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t611_il2cpp_TypeInfo_var);
		WebCamImpl_t610 * L_2 = ((CameraDeviceImpl_t611_StaticFields*)CameraDeviceImpl_t611_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StopCamera_m4089(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStopCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStopCamera() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5);
		return L_6;
	}
}
// System.Void Vuforia.CameraDeviceImpl::.cctor()
extern "C" void CameraDeviceImpl__cctor_m2885 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
// Vuforia.StateManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerImpl.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManager.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
// Vuforia.TrackableSourceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSourceImpl.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Collections.Generic.List`1<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"
// Vuforia.StateManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerImplMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// Vuforia.TrackableSourceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSourceImplMethodDeclarations.h"
// Vuforia.TypeMapping
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMappingMethodDeclarations.h"
// Vuforia.ImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetImplMethodDeclarations.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
// Vuforia.MultiTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetImplMethodDeclarations.h"


// System.IntPtr Vuforia.DataSetImpl::get_DataSetPtr()
extern "C" IntPtr_t DataSetImpl_get_DataSetPtr_m2886 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mDataSetPtr_0);
		return L_0;
	}
}
// System.String Vuforia.DataSetImpl::get_Path()
extern "C" String_t* DataSetImpl_get_Path_m2887 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mPath_1);
		return L_0;
	}
}
// Vuforia.QCARUnity/StorageType Vuforia.DataSetImpl::get_FileStorageType()
extern "C" int32_t DataSetImpl_get_FileStorageType_m2888 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStorageType_2);
		return L_0;
	}
}
// System.Void Vuforia.DataSetImpl::.ctor(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t612_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4355_MethodInfo_var;
extern "C" void DataSetImpl__ctor_m2889 (DataSetImpl_t578 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		Dictionary_2_t612_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1068);
		Dictionary_2__ctor_m4355_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mDataSetPtr_0 = L_0;
		__this->___mPath_1 = (String_t*) &_stringLiteral122;
		__this->___mStorageType_2 = 1;
		Dictionary_2_t612 * L_1 = (Dictionary_2_t612 *)il2cpp_codegen_object_new (Dictionary_2_t612_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4355(L_1, /*hidden argument*/Dictionary_2__ctor_m4355_MethodInfo_var);
		__this->___mTrackablesDict_3 = L_1;
		DataSet__ctor_m2809(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = ___dataSetPtr;
		__this->___mDataSetPtr_0 = L_2;
		return;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Load_m2890 (DataSetImpl_t578 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral143, L_0, (String_t*) &_stringLiteral144, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, __this, L_2, 1);
		return L_3;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.DataSet/StorageType)
extern "C" bool DataSetImpl_Load_m2891 (DataSetImpl_t578 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, __this, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.QCARUnity/StorageType)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Load_m2892 (DataSetImpl_t578 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	StateManagerImpl_t717 * V_4 = {0};
	{
		IntPtr_t L_0 = (__this->___mDataSetPtr_0);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m4356(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral165, /*hidden argument*/NULL);
		return 0;
	}

IL_001e:
	{
		String_t* L_3 = ___path;
		V_0 = L_3;
		int32_t L_4 = ___storageType;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_5 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral166, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		int32_t L_10 = ___storageType;
		IntPtr_t L_11 = (__this->___mDataSetPtr_0);
		NullCheck(L_8);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker3< int32_t, String_t*, int32_t, IntPtr_t >::Invoke(12 /* System.Int32 Vuforia.IQCARWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_8, L_9, L_10, L_11);
		if (L_12)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_13 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral167, L_13, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return 0;
	}

IL_005d:
	{
		String_t* L_15 = ___path;
		__this->___mPath_1 = L_15;
		int32_t L_16 = ___storageType;
		__this->___mStorageType_2 = L_16;
		bool L_17 = DataSetImpl_CreateImageTargets_m2901(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		bool L_18 = DataSetImpl_CreateMultiTargets_m2902(__this, /*hidden argument*/NULL);
		V_2 = L_18;
		bool L_19 = DataSetImpl_CreateCylinderTargets_m2903(__this, /*hidden argument*/NULL);
		V_3 = L_19;
		bool L_20 = V_1;
		if (L_20)
		{
			goto IL_0090;
		}
	}
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0090;
		}
	}
	{
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_0090;
		}
	}
	{
		DataSetImpl_CreateObjectTargets_m2904(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_23 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		StateManager_t713 * L_24 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_23);
		V_4 = ((StateManagerImpl_t717 *)Castclass(L_24, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		StateManagerImpl_t717 * L_25 = V_4;
		NullCheck(L_25);
		StateManagerImpl_AssociateTrackableBehavioursForDataSet_m4012(L_25, __this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSetImpl::GetTrackables()
extern const MethodInfo* Dictionary_2_get_Values_m4357_MethodInfo_var;
extern "C" Object_t* DataSetImpl_GetTrackables_m2893 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4357_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t612 * L_0 = (__this->___mTrackablesDict_3);
		NullCheck(L_0);
		ValueCollection_t801 * L_1 = Dictionary_2_get_Values_m4357(L_0, /*hidden argument*/Dictionary_2_get_Values_m4357_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,System.String)
extern TypeInfo* GameObject_t2_il2cpp_TypeInfo_var;
extern "C" DataSetTrackableBehaviour_t567 * DataSetImpl_CreateTrackable_m2894 (DataSetImpl_t578 * __this, TrackableSource_t619 * ___trackableSource, String_t* ___gameObjectName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	{
		String_t* L_0 = ___gameObjectName;
		GameObject_t2 * L_1 = (GameObject_t2 *)il2cpp_codegen_object_new (GameObject_t2_il2cpp_TypeInfo_var);
		GameObject__ctor_m2251(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TrackableSource_t619 * L_2 = ___trackableSource;
		GameObject_t2 * L_3 = V_0;
		DataSetTrackableBehaviour_t567 * L_4 = (DataSetTrackableBehaviour_t567 *)VirtFuncInvoker2< DataSetTrackableBehaviour_t567 *, TrackableSource_t619 *, GameObject_t2 * >::Invoke(11 /* Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject) */, __this, L_2, L_3);
		return L_4;
	}
}
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject)
extern const Il2CppType* SimpleTargetData_t753_0_0_0_var;
extern const Il2CppType* ImageTarget_t732_0_0_0_var;
extern TypeInfo* TrackableSourceImpl_t726_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t753_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t681_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t622_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern "C" DataSetTrackableBehaviour_t567 * DataSetImpl_CreateTrackable_m2895 (DataSetImpl_t578 * __this, TrackableSource_t619 * ___trackableSource, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleTargetData_t753_0_0_0_var = il2cpp_codegen_type_from_index(1070);
		ImageTarget_t732_0_0_0_var = il2cpp_codegen_type_from_index(1071);
		TrackableSourceImpl_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1072);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		SimpleTargetData_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		TypeMapping_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		ImageTargetImpl_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		s_Il2CppMethodIntialized = true;
	}
	TrackableSourceImpl_t726 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t423 * V_2 = {0};
	IntPtr_t V_3 = {0};
	int32_t V_4 = 0;
	SimpleTargetData_t753  V_5 = {0};
	Object_t * V_6 = {0};
	StateManagerImpl_t717 * V_7 = {0};
	{
		TrackableSource_t619 * L_0 = ___trackableSource;
		V_0 = ((TrackableSourceImpl_t726 *)Castclass(L_0, TrackableSourceImpl_t726_il2cpp_TypeInfo_var));
		V_1 = ((int32_t)128);
		int32_t L_1 = V_1;
		StringBuilder_t423 * L_2 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_2, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_7 = (__this->___mDataSetPtr_0);
		TrackableSourceImpl_t726 * L_8 = V_0;
		NullCheck(L_8);
		IntPtr_t L_9 = TrackableSourceImpl_get_TrackableSourcePtr_m4056(L_8, /*hidden argument*/NULL);
		StringBuilder_t423 * L_10 = V_2;
		int32_t L_11 = V_1;
		IntPtr_t L_12 = V_3;
		NullCheck(L_6);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker5< int32_t, IntPtr_t, IntPtr_t, StringBuilder_t423 *, int32_t, IntPtr_t >::Invoke(16 /* System.Int32 Vuforia.IQCARWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_6, L_7, L_9, L_10, L_11, L_12);
		V_4 = L_13;
		IntPtr_t L_14 = V_3;
		Type_t * L_15 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_16 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_5 = ((*(SimpleTargetData_t753 *)((SimpleTargetData_t753 *)UnBox (L_16, SimpleTargetData_t753_il2cpp_TypeInfo_var))));
		IntPtr_t L_17 = V_3;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_4;
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t732_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_20 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_00d2;
		}
	}
	{
		StringBuilder_t423 * L_21 = V_2;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		int32_t L_23 = ((&V_5)->___id_0);
		ImageTargetImpl_t622 * L_24 = (ImageTargetImpl_t622 *)il2cpp_codegen_object_new (ImageTargetImpl_t622_il2cpp_TypeInfo_var);
		ImageTargetImpl__ctor_m2935(L_24, L_22, L_23, 1, __this, /*hidden argument*/NULL);
		V_6 = L_24;
		Dictionary_2_t612 * L_25 = (__this->___mTrackablesDict_3);
		int32_t L_26 = ((&V_5)->___id_0);
		Object_t * L_27 = V_6;
		NullCheck(L_25);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_25, L_26, L_27);
		int32_t L_28 = V_4;
		int32_t L_29 = L_28;
		Object_t * L_30 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_29);
		StringBuilder_t423 * L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Format_m4359(NULL /*static, unused*/, (String_t*) &_stringLiteral168, L_30, L_31, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_33 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		StateManager_t713 * L_34 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_33);
		V_7 = ((StateManagerImpl_t717 *)Castclass(L_34, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		StateManagerImpl_t717 * L_35 = V_7;
		Object_t * L_36 = V_6;
		GameObject_t2 * L_37 = ___gameObject;
		NullCheck(L_35);
		ImageTargetAbstractBehaviour_t50 * L_38 = StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m4018(L_35, L_36, L_37, __this, /*hidden argument*/NULL);
		return L_38;
	}

IL_00d2:
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral169, /*hidden argument*/NULL);
		return (DataSetTrackableBehaviour_t567 *)NULL;
	}
}
// System.Boolean Vuforia.DataSetImpl::Destroy(Vuforia.Trackable,System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Destroy_m2896 (DataSetImpl_t578 * __this, Object_t * ___trackable, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		s_Il2CppMethodIntialized = true;
	}
	StateManager_t713 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mDataSetPtr_0);
		Object_t * L_2 = ___trackable;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_2);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(17 /* System.Int32 Vuforia.IQCARWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_3);
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		Object_t * L_5 = ___trackable;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_5);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1948(NULL /*static, unused*/, (String_t*) &_stringLiteral170, L_8, (String_t*) &_stringLiteral107, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return 0;
	}

IL_0039:
	{
		Dictionary_2_t612 * L_10 = (__this->___mTrackablesDict_3);
		Object_t * L_11 = ___trackable;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_10);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::Remove(!0) */, L_10, L_12);
		bool L_13 = ___destroyGameObject;
		if (!L_13)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_14 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		StateManager_t713 * L_15 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_14);
		V_0 = L_15;
		StateManager_t713 * L_16 = V_0;
		Object_t * L_17 = ___trackable;
		NullCheck(L_16);
		VirtActionInvoker2< Object_t *, bool >::Invoke(6 /* System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean) */, L_16, L_17, 1);
	}

IL_0061:
	{
		return 1;
	}
}
// System.Boolean Vuforia.DataSetImpl::HasReachedTrackableLimit()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_HasReachedTrackableLimit_m2897 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(18 /* System.Int32 Vuforia.IQCARWrapper::DataSetHasReachedTrackableLimit(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.DataSetImpl::Contains(Vuforia.Trackable)
extern const MethodInfo* Dictionary_2_ContainsValue_m4360_MethodInfo_var;
extern "C" bool DataSetImpl_Contains_m2898 (DataSetImpl_t578 * __this, Object_t * ___trackable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_ContainsValue_m4360_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t612 * L_0 = (__this->___mTrackablesDict_3);
		Object_t * L_1 = ___trackable;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsValue_m4360(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsValue_m4360_MethodInfo_var);
		return L_2;
	}
}
// System.Void Vuforia.DataSetImpl::DestroyAllTrackables(System.Boolean)
extern TypeInfo* List_1_t802_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t803_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4357_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4361_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4362_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4363_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4364_MethodInfo_var;
extern "C" void DataSetImpl_DestroyAllTrackables_m2899 (DataSetImpl_t578 * __this, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t802_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1074);
		Enumerator_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1075);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4357_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		List_1__ctor_m4361_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		List_1_GetEnumerator_m4362_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483969);
		Enumerator_get_Current_m4363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		Enumerator_MoveNext_m4364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483971);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t802 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t803  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t612 * L_0 = (__this->___mTrackablesDict_3);
		NullCheck(L_0);
		ValueCollection_t801 * L_1 = Dictionary_2_get_Values_m4357(L_0, /*hidden argument*/Dictionary_2_get_Values_m4357_MethodInfo_var);
		List_1_t802 * L_2 = (List_1_t802 *)il2cpp_codegen_object_new (List_1_t802_il2cpp_TypeInfo_var);
		List_1__ctor_m4361(L_2, L_1, /*hidden argument*/List_1__ctor_m4361_MethodInfo_var);
		V_0 = L_2;
		List_1_t802 * L_3 = V_0;
		NullCheck(L_3);
		Enumerator_t803  L_4 = List_1_GetEnumerator_m4362(L_3, /*hidden argument*/List_1_GetEnumerator_m4362_MethodInfo_var);
		V_2 = L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001a:
		{
			Object_t * L_5 = Enumerator_get_Current_m4363((&V_2), /*hidden argument*/Enumerator_get_Current_m4363_MethodInfo_var);
			V_1 = L_5;
			Object_t * L_6 = V_1;
			bool L_7 = ___destroyGameObject;
			VirtFuncInvoker2< bool, Object_t *, bool >::Invoke(12 /* System.Boolean Vuforia.DataSet::Destroy(Vuforia.Trackable,System.Boolean) */, __this, L_6, L_7);
		}

IL_002b:
		{
			bool L_8 = Enumerator_MoveNext_m4364((&V_2), /*hidden argument*/Enumerator_MoveNext_m4364_MethodInfo_var);
			if (L_8)
			{
				goto IL_001a;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t803_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t803_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Boolean Vuforia.DataSetImpl::ExistsImpl(System.String,Vuforia.QCARUnity/StorageType)
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_ExistsImpl_m2900 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___storageType;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m323(NULL /*static, unused*/, (String_t*) &_stringLiteral166, L_2, /*hidden argument*/NULL);
		___path = L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___path;
		int32_t L_6 = ___storageType;
		NullCheck(L_4);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(11 /* System.Int32 Vuforia.IQCARWrapper::DataSetExists(System.String,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateImageTargets()
extern const Il2CppType* ImageTarget_t732_0_0_0_var;
extern const Il2CppType* ImageTargetData_t605_0_0_0_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t681_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetData_t605_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t622_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateImageTargets_m2901 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageTarget_t732_0_0_0_var = il2cpp_codegen_type_from_index(1071);
		ImageTargetData_t605_0_0_0_var = il2cpp_codegen_type_from_index(1076);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		TypeMapping_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		ImageTargetData_t605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1076);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		ImageTargetImpl_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	ImageTargetData_t605  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t423 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t732_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_012a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t605_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t732_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral171, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011b;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4293((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t605_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t605_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(ImageTargetData_t605 *)((ImageTargetData_t605 *)UnBox (L_23, ImageTargetData_t605_il2cpp_TypeInfo_var))));
		Dictionary_2_t612 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0117;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t423 * L_28 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t423 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t423 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t423 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		ImageTargetImpl_t622 * L_37 = (ImageTargetImpl_t622 *)il2cpp_codegen_object_new (ImageTargetImpl_t622_il2cpp_TypeInfo_var);
		ImageTargetImpl__ctor_m2935(L_37, L_35, L_36, 0, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t612 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0117:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011b:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_012a:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateMultiTargets()
extern const Il2CppType* MultiTarget_t740_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t753_0_0_0_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t681_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t753_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* MultiTargetImpl_t632_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateMultiTargets_m2902 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiTarget_t740_0_0_0_var = il2cpp_codegen_type_from_index(1077);
		SimpleTargetData_t753_0_0_0_var = il2cpp_codegen_type_from_index(1070);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		TypeMapping_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		SimpleTargetData_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		MultiTargetImpl_t632_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t753  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t423 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(MultiTarget_t740_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(MultiTarget_t740_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral172, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4293((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t753 *)((SimpleTargetData_t753 *)UnBox (L_23, SimpleTargetData_t753_il2cpp_TypeInfo_var))));
		Dictionary_2_t612 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t423 * L_28 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t423 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t423 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t423 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		MultiTargetImpl_t632 * L_37 = (MultiTargetImpl_t632 *)il2cpp_codegen_object_new (MultiTargetImpl_t632_il2cpp_TypeInfo_var);
		MultiTargetImpl__ctor_m2980(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t612 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateCylinderTargets()
extern const Il2CppType* CylinderTarget_t592_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t753_0_0_0_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t681_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t753_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* CylinderTargetImpl_t603_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateCylinderTargets_m2903 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t592_0_0_0_var = il2cpp_codegen_type_from_index(1050);
		SimpleTargetData_t753_0_0_0_var = il2cpp_codegen_type_from_index(1070);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		TypeMapping_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		SimpleTargetData_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		CylinderTargetImpl_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t753  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t423 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(CylinderTarget_t592_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(CylinderTarget_t592_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral173, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4293((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t753 *)((SimpleTargetData_t753 *)UnBox (L_23, SimpleTargetData_t753_il2cpp_TypeInfo_var))));
		Dictionary_2_t612 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t423 * L_28 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t423 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t423 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t423 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		CylinderTargetImpl_t603 * L_37 = (CylinderTargetImpl_t603 *)il2cpp_codegen_object_new (CylinderTargetImpl_t603_il2cpp_TypeInfo_var);
		CylinderTargetImpl__ctor_m2851(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t612 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateObjectTargets()
extern const Il2CppType* ObjectTarget_t568_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t753_0_0_0_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t681_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t753_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectTargetImpl_t579_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateObjectTargets_m2904 (DataSetImpl_t578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectTarget_t568_0_0_0_var = il2cpp_codegen_type_from_index(1026);
		SimpleTargetData_t753_0_0_0_var = il2cpp_codegen_type_from_index(1070);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		TypeMapping_t681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		SimpleTargetData_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		ObjectTargetImpl_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1078);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t753  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t423 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ObjectTarget_t568_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ObjectTarget_t568_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t681_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3103(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral174, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4293((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t753_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t753 *)((SimpleTargetData_t753 *)UnBox (L_23, SimpleTargetData_t753_il2cpp_TypeInfo_var))));
		Dictionary_2_t612 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t423 * L_28 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t423 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t423 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t423 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		ObjectTargetImpl_t579 * L_37 = (ObjectTargetImpl_t579 *)il2cpp_codegen_object_new (ObjectTargetImpl_t579_il2cpp_TypeInfo_var);
		ObjectTargetImpl__ctor_m2717(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t612 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateModeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMATMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Byte
#include "mscorlib_System_Byte.h"


// System.Int32 Vuforia.Image::get_Width()
// System.Void Vuforia.Image::set_Width(System.Int32)
// System.Int32 Vuforia.Image::get_Height()
// System.Void Vuforia.Image::set_Height(System.Int32)
// System.Int32 Vuforia.Image::get_Stride()
// System.Void Vuforia.Image::set_Stride(System.Int32)
// System.Int32 Vuforia.Image::get_BufferWidth()
// System.Void Vuforia.Image::set_BufferWidth(System.Int32)
// System.Int32 Vuforia.Image::get_BufferHeight()
// System.Void Vuforia.Image::set_BufferHeight(System.Int32)
// Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat()
// System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
// System.Byte[] Vuforia.Image::get_Pixels()
// System.Void Vuforia.Image::set_Pixels(System.Byte[])
// System.Boolean Vuforia.Image::IsValid()
// System.Void Vuforia.Image::CopyToTexture(UnityEngine.Texture2D)
// System.Void Vuforia.Image::.ctor()
extern "C" void Image__ctor_m2905 (Image_t615 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"


// System.Int32 Vuforia.ImageImpl::get_Width()
extern "C" int32_t ImageImpl_get_Width_m2906 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWidth_0);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Width(System.Int32)
extern "C" void ImageImpl_set_Width_m2907 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mWidth_0 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_Height()
extern "C" int32_t ImageImpl_get_Height_m2908 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mHeight_1);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Height(System.Int32)
extern "C" void ImageImpl_set_Height_m2909 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mHeight_1 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_Stride()
extern "C" int32_t ImageImpl_get_Stride_m2910 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStride_2);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Stride(System.Int32)
extern "C" void ImageImpl_set_Stride_m2911 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mStride_2 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_BufferWidth()
extern "C" int32_t ImageImpl_get_BufferWidth_m2912 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mBufferWidth_3);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_BufferWidth(System.Int32)
extern "C" void ImageImpl_set_BufferWidth_m2913 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mBufferWidth_3 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_BufferHeight()
extern "C" int32_t ImageImpl_get_BufferHeight_m2914 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mBufferHeight_4);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_BufferHeight(System.Int32)
extern "C" void ImageImpl_set_BufferHeight_m2915 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mBufferHeight_4 = L_0;
		return;
	}
}
// Vuforia.Image/PIXEL_FORMAT Vuforia.ImageImpl::get_PixelFormat()
extern "C" int32_t ImageImpl_get_PixelFormat_m2916 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
extern "C" void ImageImpl_set_PixelFormat_m2917 (ImageImpl_t618 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mPixelFormat_5 = L_0;
		return;
	}
}
// System.Byte[] Vuforia.ImageImpl::get_Pixels()
extern "C" ByteU5BU5D_t616* ImageImpl_get_Pixels_m2918 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t616* L_0 = (__this->___mData_6);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Pixels(System.Byte[])
extern "C" void ImageImpl_set_Pixels_m2919 (ImageImpl_t618 * __this, ByteU5BU5D_t616* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t616* L_0 = ___value;
		__this->___mData_6 = L_0;
		return;
	}
}
// System.IntPtr Vuforia.ImageImpl::get_UnmanagedData()
extern "C" IntPtr_t ImageImpl_get_UnmanagedData_m2920 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mUnmanagedData_7);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_UnmanagedData(System.IntPtr)
extern "C" void ImageImpl_set_UnmanagedData_m2921 (ImageImpl_t618 * __this, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		__this->___mUnmanagedData_7 = L_0;
		return;
	}
}
// System.Void Vuforia.ImageImpl::.ctor()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Color32U5BU5D_t617_il2cpp_TypeInfo_var;
extern "C" void ImageImpl__ctor_m2922 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		Color32U5BU5D_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image__ctor_m2905(__this, /*hidden argument*/NULL);
		__this->___mWidth_0 = 0;
		__this->___mHeight_1 = 0;
		__this->___mStride_2 = 0;
		__this->___mBufferWidth_3 = 0;
		__this->___mBufferHeight_4 = 0;
		__this->___mPixelFormat_5 = 0;
		__this->___mData_6 = (ByteU5BU5D_t616*)NULL;
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mUnmanagedData_7 = L_0;
		__this->___mDataSet_8 = 0;
		__this->___mPixel32_9 = ((Color32U5BU5D_t617*)SZArrayNew(Color32U5BU5D_t617_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.Void Vuforia.ImageImpl::Finalize()
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void ImageImpl_Finalize_m2923 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = (__this->___mUnmanagedData_7);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mUnmanagedData_7 = L_1;
		IL2CPP_LEAVE(0x1F, FINALLY_0018);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		Object_Finalize_m515(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(24)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean Vuforia.ImageImpl::IsValid()
extern "C" bool ImageImpl_IsValid_m2924 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWidth_0);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = (__this->___mHeight_1);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = (__this->___mStride_2);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = (__this->___mBufferWidth_3);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_4 = (__this->___mBufferHeight_4);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ByteU5BU5D_t616* L_5 = (__this->___mData_6);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		bool L_6 = (__this->___mDataSet_8);
		return L_6;
	}

IL_003c:
	{
		return 0;
	}
}
// System.Void Vuforia.ImageImpl::CopyToTexture(UnityEngine.Texture2D)
extern "C" void ImageImpl_CopyToTexture_m2925 (ImageImpl_t618 * __this, Texture2D_t270 * ___texture2D, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	ColorU5BU5D_t804* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = {0};
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		int32_t L_1 = ImageImpl_ConvertPixelFormat_m2928(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Texture2D_t270 * L_2 = ___texture2D;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		int32_t L_4 = (__this->___mWidth_0);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0032;
		}
	}
	{
		Texture2D_t270 * L_5 = ___texture2D;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_5);
		int32_t L_7 = (__this->___mHeight_1);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_8 = V_0;
		Texture2D_t270 * L_9 = ___texture2D;
		NullCheck(L_9);
		int32_t L_10 = Texture2D_get_format_m4365(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)L_10)))
		{
			goto IL_0047;
		}
	}

IL_0032:
	{
		Texture2D_t270 * L_11 = ___texture2D;
		int32_t L_12 = (__this->___mWidth_0);
		int32_t L_13 = (__this->___mHeight_1);
		int32_t L_14 = V_0;
		NullCheck(L_11);
		Texture2D_Resize_m4366(L_11, L_12, L_13, L_14, 0, /*hidden argument*/NULL);
	}

IL_0047:
	{
		V_1 = 1;
		int32_t L_15 = (__this->___mPixelFormat_5);
		V_7 = L_15;
		int32_t L_16 = V_7;
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 0)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 1)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_17 = V_7;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_006e;
		}
	}
	{
		V_1 = 4;
		goto IL_006e;
	}

IL_006c:
	{
		V_1 = 3;
	}

IL_006e:
	{
		Texture2D_t270 * L_18 = ___texture2D;
		NullCheck(L_18);
		ColorU5BU5D_t804* L_19 = Texture2D_GetPixels_m4367(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		V_3 = 0;
		V_4 = 0;
		goto IL_00e4;
	}

IL_007c:
	{
		V_5 = 0;
		goto IL_00a9;
	}

IL_0081:
	{
		ColorU5BU5D_t804* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = V_5;
		ByteU5BU5D_t616* L_23 = (__this->___mData_6);
		int32_t L_24 = V_3;
		int32_t L_25 = L_24;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)1));
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		int32_t L_26 = L_25;
		Color_set_Item_m4368(((Color_t90 *)(Color_t90 *)SZArrayLdElema(L_20, L_21)), L_22, ((float)((float)(((float)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_23, L_26))))/(float)(255.0f))), /*hidden argument*/NULL);
		int32_t L_27 = V_5;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_28 = V_5;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_30 = V_1;
		V_6 = L_30;
		goto IL_00d9;
	}

IL_00b3:
	{
		ColorU5BU5D_t804* L_31 = V_2;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = V_6;
		ColorU5BU5D_t804* L_34 = V_2;
		int32_t L_35 = V_4;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = V_6;
		float L_37 = Color_get_Item_m4369(((Color_t90 *)(Color_t90 *)SZArrayLdElema(L_34, L_35)), ((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/NULL);
		Color_set_Item_m4368(((Color_t90 *)(Color_t90 *)SZArrayLdElema(L_31, L_32)), L_33, L_37, /*hidden argument*/NULL);
		int32_t L_38 = V_6;
		V_6 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00d9:
	{
		int32_t L_39 = V_6;
		if ((((int32_t)L_39) < ((int32_t)4)))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00e4:
	{
		int32_t L_41 = V_4;
		ColorU5BU5D_t804* L_42 = V_2;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_007c;
		}
	}
	{
		Texture2D_t270 * L_43 = ___texture2D;
		ColorU5BU5D_t804* L_44 = V_2;
		NullCheck(L_43);
		Texture2D_SetPixels_m4370(L_43, L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageImpl::CopyPixelsFromUnmanagedBuffer()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern "C" void ImageImpl_CopyPixelsFromUnmanagedBuffer_m2926 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		ByteU5BU5D_t616* L_0 = (__this->___mData_6);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_1 = (__this->___mUnmanagedData_7);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Equality_m4356(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}

IL_001a:
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral175, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		int32_t L_4 = (__this->___mPixelFormat_5);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
		{
			goto IL_0065;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 1)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_6 = V_1;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_7 = (__this->___mBufferWidth_3);
		int32_t L_8 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)L_8))*(int32_t)4));
		goto IL_0085;
	}

IL_0053:
	{
		int32_t L_9 = (__this->___mBufferWidth_3);
		int32_t L_10 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)L_10))*(int32_t)3));
		goto IL_0085;
	}

IL_0065:
	{
		int32_t L_11 = (__this->___mBufferWidth_3);
		int32_t L_12 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_11*(int32_t)L_12))*(int32_t)2));
		goto IL_0085;
	}

IL_0077:
	{
		int32_t L_13 = (__this->___mBufferWidth_3);
		int32_t L_14 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)L_13*(int32_t)L_14));
	}

IL_0085:
	{
		IntPtr_t L_15 = (__this->___mUnmanagedData_7);
		ByteU5BU5D_t616* L_16 = (__this->___mData_6);
		int32_t L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_Copy_m4371(NULL /*static, unused*/, L_15, L_16, 0, L_17, /*hidden argument*/NULL);
		__this->___mDataSet_8 = 1;
		return;
	}
}
// UnityEngine.Color32[] Vuforia.ImageImpl::GetPixels32()
extern TypeInfo* Color32U5BU5D_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern "C" Color32U5BU5D_t617* ImageImpl_GetPixels32_m2927 (ImageImpl_t618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color32U5BU5D_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GCHandle_t805  V_1 = {0};
	IntPtr_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mBufferWidth_3);
		int32_t L_1 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)L_0*(int32_t)L_1));
		Color32U5BU5D_t617* L_2 = (__this->___mPixel32_9);
		NullCheck(L_2);
		int32_t L_3 = V_0;
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((int32_t)L_3)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_4 = V_0;
		__this->___mPixel32_9 = ((Color32U5BU5D_t617*)SZArrayNew(Color32U5BU5D_t617_il2cpp_TypeInfo_var, L_4));
	}

IL_0025:
	{
		Color32U5BU5D_t617* L_5 = (__this->___mPixel32_9);
		GCHandle_t805  L_6 = GCHandle_Alloc_m4372(NULL /*static, unused*/, (Object_t *)(Object_t *)L_5, 3, /*hidden argument*/NULL);
		V_1 = L_6;
		IntPtr_t L_7 = GCHandle_AddrOfPinnedObject_m4373((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		ByteU5BU5D_t616* L_8 = (__this->___mData_6);
		IntPtr_t L_9 = V_2;
		ByteU5BU5D_t616* L_10 = (__this->___mData_6);
		NullCheck(L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_Copy_m4374(NULL /*static, unused*/, L_8, 0, L_9, (((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		GCHandle_Free_m4375((&V_1), /*hidden argument*/NULL);
		Color32U5BU5D_t617* L_11 = (__this->___mPixel32_9);
		return L_11;
	}
}
// UnityEngine.TextureFormat Vuforia.ImageImpl::ConvertPixelFormat(Vuforia.Image/PIXEL_FORMAT)
extern "C" int32_t ImageImpl_ConvertPixelFormat_m2928 (ImageImpl_t618 * __this, int32_t ___input, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0020;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0022;
		}
	}
	{
		return (int32_t)(4);
	}

IL_001e:
	{
		return (int32_t)(3);
	}

IL_0020:
	{
		return (int32_t)(7);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// Vuforia.ImageTargetBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderI.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderIMethodDeclarations.h"



// System.Boolean Vuforia.ImageTargetBuilderImpl::Build(System.String,System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ImageTargetBuilderImpl_Build_m2929 (ImageTargetBuilderImpl_t620 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___targetName;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2207(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)64))))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral176, /*hidden argument*/NULL);
		return 0;
	}

IL_0016:
	{
		__this->___mTrackableSource_0 = (TrackableSource_t619 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___targetName;
		float L_4 = ___sceenSizeWidth;
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker2< int32_t, String_t*, float >::Invoke(20 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetBuilderBuild(System.String,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return ((((int32_t)L_5) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::StartScan()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void ImageTargetBuilderImpl_StartScan_m2930 (ImageTargetBuilderImpl_t620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(22 /* System.Void Vuforia.IQCARWrapper::ImageTargetBuilderStartScan() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::StopScan()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void ImageTargetBuilderImpl_StopScan_m2931 (ImageTargetBuilderImpl_t620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(23 /* System.Void Vuforia.IQCARWrapper::ImageTargetBuilderStopScan() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilderImpl::GetFrameQuality()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t ImageTargetBuilderImpl_GetFrameQuality_m2932 (ImageTargetBuilderImpl_t620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(24 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetBuilderGetFrameQuality() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return (int32_t)(L_1);
	}
}
// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::GetTrackableSource()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableSourceImpl_t726_il2cpp_TypeInfo_var;
extern "C" TrackableSource_t619 * ImageTargetBuilderImpl_GetTrackableSource_m2933 (ImageTargetBuilderImpl_t620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		TrackableSourceImpl_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1072);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IntPtr_t L_1 = (IntPtr_t)InterfaceFuncInvoker0< IntPtr_t >::Invoke(25 /* System.IntPtr Vuforia.IQCARWrapper::ImageTargetBuilderGetTrackableSource() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		TrackableSource_t619 * L_2 = (__this->___mTrackableSource_0);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_3 = V_0;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_5 = IntPtr_op_Inequality_m4376(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_6 = V_0;
		TrackableSourceImpl_t726 * L_7 = (TrackableSourceImpl_t726 *)il2cpp_codegen_object_new (TrackableSourceImpl_t726_il2cpp_TypeInfo_var);
		TrackableSourceImpl__ctor_m4058(L_7, L_6, /*hidden argument*/NULL);
		__this->___mTrackableSource_0 = L_7;
	}

IL_002c:
	{
		TrackableSource_t619 * L_8 = (__this->___mTrackableSource_0);
		return L_8;
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::.ctor()
extern "C" void ImageTargetBuilderImpl__ctor_m2934 (ImageTargetBuilderImpl_t620 * __this, const MethodInfo* method)
{
	{
		ImageTargetBuilder__ctor_m2860(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
// Vuforia.VirtualButtonImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2MethodDeclarations.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonMethodDeclarations.h"
// Vuforia.VirtualButtonImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonImplMethodDeclarations.h"


// System.Void Vuforia.ImageTargetImpl::.ctor(System.String,System.Int32,Vuforia.ImageTargetType,Vuforia.DataSet)
extern TypeInfo* Dictionary_2_t621_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4377_MethodInfo_var;
extern "C" void ImageTargetImpl__ctor_m2935 (ImageTargetImpl_t622 * __this, String_t* ___name, int32_t ___id, int32_t ___imageTargetType, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t621_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		Dictionary_2__ctor_m4377_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483972);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t594 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2717(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___imageTargetType;
		__this->___mImageTargetType_4 = L_3;
		Dictionary_2_t621 * L_4 = (Dictionary_2_t621 *)il2cpp_codegen_object_new (Dictionary_2_t621_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4377(L_4, /*hidden argument*/Dictionary_2__ctor_m4377_MethodInfo_var);
		__this->___mVirtualButtons_5 = L_4;
		ImageTargetImpl_CreateVirtualButtonsFromNative_m2943(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::get_ImageTargetType()
extern "C" int32_t ImageTargetImpl_get_ImageTargetType_m2936 (ImageTargetImpl_t622 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mImageTargetType_4);
		return L_0;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
extern "C" VirtualButton_t731 * ImageTargetImpl_CreateVirtualButton_m2937 (ImageTargetImpl_t622 * __this, String_t* ___name, RectangleData_t596  ___area, const MethodInfo* method)
{
	VirtualButton_t731 * V_0 = {0};
	{
		String_t* L_0 = ___name;
		RectangleData_t596  L_1 = ___area;
		VirtualButton_t731 * L_2 = ImageTargetImpl_CreateNewVirtualButtonInNative_m2941(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		VirtualButton_t731 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral177, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0018:
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral178, /*hidden argument*/NULL);
	}

IL_0022:
	{
		VirtualButton_t731 * L_4 = V_0;
		return L_4;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::GetVirtualButtonByName(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t806_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4378_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4379_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4380_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4381_MethodInfo_var;
extern "C" VirtualButton_t731 * ImageTargetImpl_GetVirtualButtonByName_m2938 (ImageTargetImpl_t622 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Enumerator_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1081);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4378_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483973);
		ValueCollection_GetEnumerator_m4379_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483974);
		Enumerator_get_Current_m4380_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483975);
		Enumerator_MoveNext_m4381_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483976);
		s_Il2CppMethodIntialized = true;
	}
	VirtualButton_t731 * V_0 = {0};
	VirtualButton_t731 * V_1 = {0};
	Enumerator_t806  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t621 * L_0 = (__this->___mVirtualButtons_5);
		NullCheck(L_0);
		ValueCollection_t807 * L_1 = Dictionary_2_get_Values_m4378(L_0, /*hidden argument*/Dictionary_2_get_Values_m4378_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t806  L_2 = ValueCollection_GetEnumerator_m4379(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4379_MethodInfo_var);
		V_2 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0013:
		{
			VirtualButton_t731 * L_3 = Enumerator_get_Current_m4380((&V_2), /*hidden argument*/Enumerator_get_Current_m4380_MethodInfo_var);
			V_0 = L_3;
			VirtualButton_t731 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_4);
			String_t* L_6 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_op_Equality_m260(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_002d;
			}
		}

IL_0029:
		{
			VirtualButton_t731 * L_8 = V_0;
			V_1 = L_8;
			IL2CPP_LEAVE(0x48, FINALLY_0038);
		}

IL_002d:
		{
			bool L_9 = Enumerator_MoveNext_m4381((&V_2), /*hidden argument*/Enumerator_MoveNext_m4381_MethodInfo_var);
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t806_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t806_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0046:
	{
		return (VirtualButton_t731 *)NULL;
	}

IL_0048:
	{
		VirtualButton_t731 * L_10 = V_1;
		return L_10;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.ImageTargetImpl::GetVirtualButtons()
extern const MethodInfo* Dictionary_2_get_Values_m4378_MethodInfo_var;
extern "C" Object_t* ImageTargetImpl_GetVirtualButtons_m2939 (ImageTargetImpl_t622 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4378_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483973);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t621 * L_0 = (__this->___mVirtualButtons_5);
		NullCheck(L_0);
		ValueCollection_t807 * L_1 = Dictionary_2_get_Values_m4378(L_0, /*hidden argument*/Dictionary_2_get_Values_m4378_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean Vuforia.ImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t765_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t808_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var;
extern "C" bool ImageTargetImpl_DestroyVirtualButton_m2940 (ImageTargetImpl_t622 * __this, VirtualButton_t731 * ___vb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IEnumerable_1_t765_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		IEnumerator_1_t808_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	ObjectTracker_t574 * V_1 = {0};
	bool V_2 = false;
	DataSet_t594 * V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t574 * L_1 = (ObjectTracker_t574 *)GenericVirtFuncInvoker0< ObjectTracker_t574 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t574_m4315_MethodInfo_var, L_0);
		V_1 = L_1;
		ObjectTracker_t574 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_00a1;
		}
	}
	{
		V_2 = 0;
		ObjectTracker_t574 * L_3 = V_1;
		NullCheck(L_3);
		Object_t* L_4 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(14 /* System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetActiveDataSets() */, L_3);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>::GetEnumerator() */, IEnumerable_1_t765_il2cpp_TypeInfo_var, L_4);
		V_4 = L_5;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_0024:
		{
			Object_t* L_6 = V_4;
			NullCheck(L_6);
			DataSet_t594 * L_7 = (DataSet_t594 *)InterfaceFuncInvoker0< DataSet_t594 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>::get_Current() */, IEnumerator_1_t808_il2cpp_TypeInfo_var, L_6);
			V_3 = L_7;
			DataSetImpl_t578 * L_8 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
			DataSet_t594 * L_9 = V_3;
			if ((!(((Object_t*)(DataSetImpl_t578 *)L_8) == ((Object_t*)(DataSet_t594 *)L_9))))
			{
				goto IL_0037;
			}
		}

IL_0035:
		{
			V_2 = 1;
		}

IL_0037:
		{
			Object_t* L_10 = V_4;
			NullCheck(L_10);
			bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0024;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		{
			Object_t* L_12 = V_4;
			if (!L_12)
			{
				goto IL_004d;
			}
		}

IL_0046:
		{
			Object_t* L_13 = V_4;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_13);
		}

IL_004d:
		{
			IL2CPP_END_FINALLY(66)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_004e:
	{
		bool L_14 = V_2;
		if (!L_14)
		{
			goto IL_005e;
		}
	}
	{
		ObjectTracker_t574 * L_15 = V_1;
		DataSetImpl_t578 * L_16 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_15);
		VirtFuncInvoker1< bool, DataSet_t594 * >::Invoke(13 /* System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet) */, L_15, L_16);
	}

IL_005e:
	{
		VirtualButton_t731 * L_17 = ___vb;
		bool L_18 = ImageTargetImpl_UnregisterVirtualButtonInNative_m2942(__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0087;
		}
	}
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral179, /*hidden argument*/NULL);
		V_0 = 1;
		Dictionary_2_t621 * L_19 = (__this->___mVirtualButtons_5);
		VirtualButton_t731 * L_20 = ___vb;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 Vuforia.VirtualButton::get_ID() */, L_20);
		NullCheck(L_19);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Remove(!0) */, L_19, L_21);
		goto IL_0091;
	}

IL_0087:
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral180, /*hidden argument*/NULL);
	}

IL_0091:
	{
		bool L_22 = V_2;
		if (!L_22)
		{
			goto IL_00a1;
		}
	}
	{
		ObjectTracker_t574 * L_23 = V_1;
		DataSetImpl_t578 * L_24 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_23);
		VirtFuncInvoker1< bool, DataSet_t594 * >::Invoke(12 /* System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet) */, L_23, L_24);
	}

IL_00a1:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateNewVirtualButtonInNative(System.String,Vuforia.RectangleData)
extern const Il2CppType* RectangleData_t596_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t596_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonImpl_t733_il2cpp_TypeInfo_var;
extern "C" VirtualButton_t731 * ImageTargetImpl_CreateNewVirtualButtonInNative_m2941 (ImageTargetImpl_t622 * __this, String_t* ___name, RectangleData_t596  ___rectangleData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectangleData_t596_0_0_0_var = il2cpp_codegen_type_from_index(1036);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		RectangleData_t596_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		VirtualButtonImpl_t733_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	bool V_1 = false;
	VirtualButton_t731 * V_2 = {0};
	int32_t V_3 = 0;
	{
		int32_t L_0 = ImageTargetImpl_get_ImageTargetType_m2936(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral181, L_1, (String_t*) &_stringLiteral182, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (VirtualButton_t731 *)NULL;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(RectangleData_t596_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectangleData_t596  L_6 = ___rectangleData;
		RectangleData_t596  L_7 = L_6;
		Object_t * L_8 = Box(RectangleData_t596_il2cpp_TypeInfo_var, &L_7);
		IntPtr_t L_9 = V_0;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_8, L_9, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_11 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_11);
		IntPtr_t L_12 = DataSetImpl_get_DataSetPtr_m2886(L_11, /*hidden argument*/NULL);
		String_t* L_13 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		String_t* L_14 = ___name;
		IntPtr_t L_15 = V_0;
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, IntPtr_t, String_t*, String_t*, IntPtr_t >::Invoke(26 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		V_1 = ((((int32_t)((((int32_t)L_16) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_2 = (VirtualButton_t731 *)NULL;
		bool L_17 = V_1;
		if (!L_17)
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_18 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_19 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_19);
		IntPtr_t L_20 = DataSetImpl_get_DataSetPtr_m2886(L_19, /*hidden argument*/NULL);
		String_t* L_21 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		String_t* L_22 = ___name;
		NullCheck(L_18);
		int32_t L_23 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(28 /* System.Int32 Vuforia.IQCARWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_18, L_20, L_21, L_22);
		V_3 = L_23;
		Dictionary_2_t621 * L_24 = (__this->___mVirtualButtons_5);
		int32_t L_25 = V_3;
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_27 = ___name;
		int32_t L_28 = V_3;
		RectangleData_t596  L_29 = ___rectangleData;
		DataSetImpl_t578 * L_30 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		VirtualButtonImpl_t733 * L_31 = (VirtualButtonImpl_t733 *)il2cpp_codegen_object_new (VirtualButtonImpl_t733_il2cpp_TypeInfo_var);
		VirtualButtonImpl__ctor_m4074(L_31, L_27, L_28, L_29, __this, L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		Dictionary_2_t621 * L_32 = (__this->___mVirtualButtons_5);
		int32_t L_33 = V_3;
		VirtualButton_t731 * L_34 = V_2;
		NullCheck(L_32);
		VirtActionInvoker2< int32_t, VirtualButton_t731 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Add(!0,!1) */, L_32, L_33, L_34);
		goto IL_00c1;
	}

IL_00b4:
	{
		Dictionary_2_t621 * L_35 = (__this->___mVirtualButtons_5);
		int32_t L_36 = V_3;
		NullCheck(L_35);
		VirtualButton_t731 * L_37 = (VirtualButton_t731 *)VirtFuncInvoker1< VirtualButton_t731 *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::get_Item(!0) */, L_35, L_36);
		V_2 = L_37;
	}

IL_00c1:
	{
		VirtualButton_t731 * L_38 = V_2;
		return L_38;
	}
}
// System.Boolean Vuforia.ImageTargetImpl::UnregisterVirtualButtonInNative(Vuforia.VirtualButton)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ImageTargetImpl_UnregisterVirtualButtonInNative_m2942 (ImageTargetImpl_t622 * __this, VirtualButton_t731 * ___vb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_1 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		String_t* L_3 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		VirtualButton_t731 * L_4 = ___vb;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_4);
		NullCheck(L_0);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(28 /* System.Int32 Vuforia.IQCARWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3, L_5);
		V_0 = L_6;
		V_1 = 0;
		Object_t * L_7 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_8 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_8);
		IntPtr_t L_9 = DataSetImpl_get_DataSetPtr_m2886(L_8, /*hidden argument*/NULL);
		String_t* L_10 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		VirtualButton_t731 * L_11 = ___vb;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_11);
		NullCheck(L_7);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(27 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_7, L_9, L_10, L_12);
		if (!L_13)
		{
			goto IL_0057;
		}
	}
	{
		Dictionary_2_t621 * L_14 = (__this->___mVirtualButtons_5);
		int32_t L_15 = V_0;
		NullCheck(L_14);
		bool L_16 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Remove(!0) */, L_14, L_15);
		if (!L_16)
		{
			goto IL_0057;
		}
	}
	{
		V_1 = 1;
	}

IL_0057:
	{
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_0064;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral183, /*hidden argument*/NULL);
	}

IL_0064:
	{
		bool L_18 = V_1;
		return L_18;
	}
}
// System.Void Vuforia.ImageTargetImpl::CreateVirtualButtonsFromNative()
extern const Il2CppType* VirtualButtonData_t643_0_0_0_var;
extern const Il2CppType* RectangleData_t596_0_0_0_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonData_t643_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t596_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t423_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonImpl_t733_il2cpp_TypeInfo_var;
extern "C" void ImageTargetImpl_CreateVirtualButtonsFromNative_m2943 (ImageTargetImpl_t622 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualButtonData_t643_0_0_0_var = il2cpp_codegen_type_from_index(1086);
		RectangleData_t596_0_0_0_var = il2cpp_codegen_type_from_index(1036);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		VirtualButtonData_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		RectangleData_t596_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		StringBuilder_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		VirtualButtonImpl_t733_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	IntPtr_t V_2 = {0};
	int32_t V_3 = 0;
	IntPtr_t V_4 = {0};
	VirtualButtonData_t643  V_5 = {0};
	IntPtr_t V_6 = {0};
	RectangleData_t596  V_7 = {0};
	int32_t V_8 = 0;
	StringBuilder_t423 * V_9 = {0};
	VirtualButton_t731 * V_10 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_1 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2886(L_1, /*hidden argument*/NULL);
		String_t* L_3 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, String_t* >::Invoke(29 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0187;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t643_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(RectangleData_t596_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_11 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		IntPtr_t L_13 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_11*(int32_t)L_12)), /*hidden argument*/NULL);
		V_2 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_14 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_15 = V_1;
		IntPtr_t L_16 = V_2;
		int32_t L_17 = V_0;
		DataSetImpl_t578 * L_18 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_18);
		IntPtr_t L_19 = DataSetImpl_get_DataSetPtr_m2886(L_18, /*hidden argument*/NULL);
		String_t* L_20 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, IntPtr_t, IntPtr_t, int32_t, IntPtr_t, String_t* >::Invoke(30 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17, L_19, L_20);
		V_3 = 0;
		goto IL_0174;
	}

IL_0077:
	{
		int64_t L_21 = IntPtr_ToInt64_m4293((&V_1), /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t643_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_24 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_4), ((int64_t)((int64_t)L_21+(int64_t)(((int64_t)((int32_t)((int32_t)L_22*(int32_t)L_24)))))), /*hidden argument*/NULL);
		IntPtr_t L_25 = V_4;
		Type_t * L_26 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t643_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_27 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_5 = ((*(VirtualButtonData_t643 *)((VirtualButtonData_t643 *)UnBox (L_27, VirtualButtonData_t643_il2cpp_TypeInfo_var))));
		Dictionary_2_t621 * L_28 = (__this->___mVirtualButtons_5);
		int32_t L_29 = ((&V_5)->___id_0);
		NullCheck(L_28);
		bool L_30 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::ContainsKey(!0) */, L_28, L_29);
		if (L_30)
		{
			goto IL_0170;
		}
	}
	{
		int64_t L_31 = IntPtr_ToInt64_m4293((&V_2), /*hidden argument*/NULL);
		int32_t L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(RectangleData_t596_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_34 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_6), ((int64_t)((int64_t)L_31+(int64_t)(((int64_t)((int32_t)((int32_t)L_32*(int32_t)L_34)))))), /*hidden argument*/NULL);
		IntPtr_t L_35 = V_6;
		Type_t * L_36 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(RectangleData_t596_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_37 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_7 = ((*(RectangleData_t596 *)((RectangleData_t596 *)UnBox (L_37, RectangleData_t596_il2cpp_TypeInfo_var))));
		V_8 = ((int32_t)128);
		int32_t L_38 = V_8;
		StringBuilder_t423 * L_39 = (StringBuilder_t423 *)il2cpp_codegen_object_new (StringBuilder_t423_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4358(L_39, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_40 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_41 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		NullCheck(L_41);
		IntPtr_t L_42 = DataSetImpl_get_DataSetPtr_m2886(L_41, /*hidden argument*/NULL);
		String_t* L_43 = TrackableImpl_get_Name_m2713(__this, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		StringBuilder_t423 * L_45 = V_9;
		int32_t L_46 = V_8;
		NullCheck(L_40);
		int32_t L_47 = (int32_t)InterfaceFuncInvoker5< int32_t, IntPtr_t, String_t*, int32_t, StringBuilder_t423 *, int32_t >::Invoke(31 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_40, L_42, L_43, L_44, L_45, L_46);
		if (L_47)
		{
			goto IL_013e;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral184, /*hidden argument*/NULL);
		goto IL_0170;
	}

IL_013e:
	{
		StringBuilder_t423 * L_48 = V_9;
		NullCheck(L_48);
		String_t* L_49 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_48);
		int32_t L_50 = ((&V_5)->___id_0);
		RectangleData_t596  L_51 = V_7;
		DataSetImpl_t578 * L_52 = (((ObjectTargetImpl_t579 *)__this)->___mDataSet_3);
		VirtualButtonImpl_t733 * L_53 = (VirtualButtonImpl_t733 *)il2cpp_codegen_object_new (VirtualButtonImpl_t733_il2cpp_TypeInfo_var);
		VirtualButtonImpl__ctor_m4074(L_53, L_49, L_50, L_51, __this, L_52, /*hidden argument*/NULL);
		V_10 = L_53;
		Dictionary_2_t621 * L_54 = (__this->___mVirtualButtons_5);
		int32_t L_55 = ((&V_5)->___id_0);
		VirtualButton_t731 * L_56 = V_10;
		NullCheck(L_54);
		VirtActionInvoker2< int32_t, VirtualButton_t731 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Add(!0,!1) */, L_54, L_55, L_56);
	}

IL_0170:
	{
		int32_t L_57 = V_3;
		V_3 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0174:
	{
		int32_t L_58 = V_3;
		int32_t L_59 = V_0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0077;
		}
	}
	{
		IntPtr_t L_60 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		IntPtr_t L_61 = V_2;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
	}

IL_0187:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean Vuforia.Tracker::Start()
// System.Void Vuforia.Tracker::Stop()
// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C" bool Tracker_get_IsActive_m2944 (Tracker_t623 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsActiveU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C" void Tracker_set_IsActive_m2945 (Tracker_t623 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsActiveU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Void Vuforia.Tracker::.ctor()
extern "C" void Tracker__ctor_m2946 (Tracker_t623 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::get_ImageTargetBuilder()
// Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder()
// Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet()
// System.Boolean Vuforia.ObjectTracker::DestroyDataSet(Vuforia.DataSet,System.Boolean)
// System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet)
// System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet)
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetActiveDataSets()
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetDataSets()
// System.Void Vuforia.ObjectTracker::DestroyAllDataSets(System.Boolean)
// System.Boolean Vuforia.ObjectTracker::PersistExtendedTracking(System.Boolean)
// System.Boolean Vuforia.ObjectTracker::ResetExtendedTracking()
// System.Void Vuforia.ObjectTracker::.ctor()
extern "C" void ObjectTracker__ctor_m2947 (ObjectTracker_t574 * __this, const MethodInfo* method)
{
	{
		Tracker__ctor_m2946(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImplMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_gen_24.h"
// System.Collections.Generic.List`1<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_gen_25.h"
// Vuforia.TargetFinderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImpl.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_gen_24MethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
// Vuforia.TargetFinderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImplMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
struct Enumerable_t132;
struct IEnumerable_1_t765;
struct IEnumerable_t550;
struct Enumerable_t132;
struct IEnumerable_1_t136;
struct IEnumerable_t550;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C" Object_t* Enumerable_Cast_TisObject_t_m4383_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Enumerable_Cast_TisObject_t_m4383(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m4383_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<Vuforia.DataSet>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<Vuforia.DataSet>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisDataSet_t594_m4382(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m4383_gshared)(__this /* static, unused */, p0, method)


// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::get_ImageTargetBuilder()
extern "C" ImageTargetBuilder_t607 * ObjectTrackerImpl_get_ImageTargetBuilder_m2948 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	{
		ImageTargetBuilder_t607 * L_0 = (__this->___mImageTargetBuilder_3);
		return L_0;
	}
}
// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::get_TargetFinder()
extern "C" TargetFinder_t626 * ObjectTrackerImpl_get_TargetFinder_m2949 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	{
		TargetFinder_t626 * L_0 = (__this->___mTargetFinder_4);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
extern TypeInfo* List_1_t624_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t625_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetBuilderImpl_t620_il2cpp_TypeInfo_var;
extern TypeInfo* TargetFinderImpl_t725_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4384_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4385_MethodInfo_var;
extern "C" void ObjectTrackerImpl__ctor_m2950 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		List_1_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		ImageTargetBuilderImpl_t620_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		TargetFinderImpl_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		List_1__ctor_m4384_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483977);
		List_1__ctor_m4385_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483978);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t624 * L_0 = (List_1_t624 *)il2cpp_codegen_object_new (List_1_t624_il2cpp_TypeInfo_var);
		List_1__ctor_m4384(L_0, /*hidden argument*/List_1__ctor_m4384_MethodInfo_var);
		__this->___mActiveDataSets_1 = L_0;
		List_1_t625 * L_1 = (List_1_t625 *)il2cpp_codegen_object_new (List_1_t625_il2cpp_TypeInfo_var);
		List_1__ctor_m4385(L_1, /*hidden argument*/List_1__ctor_m4385_MethodInfo_var);
		__this->___mDataSets_2 = L_1;
		ObjectTracker__ctor_m2947(__this, /*hidden argument*/NULL);
		ImageTargetBuilderImpl_t620 * L_2 = (ImageTargetBuilderImpl_t620 *)il2cpp_codegen_object_new (ImageTargetBuilderImpl_t620_il2cpp_TypeInfo_var);
		ImageTargetBuilderImpl__ctor_m2934(L_2, /*hidden argument*/NULL);
		__this->___mImageTargetBuilder_3 = L_2;
		TargetFinderImpl_t725 * L_3 = (TargetFinderImpl_t725 *)il2cpp_codegen_object_new (TargetFinderImpl_t725_il2cpp_TypeInfo_var);
		TargetFinderImpl__ctor_m4039(L_3, /*hidden argument*/NULL);
		__this->___mTargetFinder_4 = L_3;
		return;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::Start()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_Start_m2951 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(38 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerStart() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral185, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 1);
		return 1;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::Stop()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t763_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t810_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t809_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4386_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4387_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4388_MethodInfo_var;
extern "C" void ObjectTrackerImpl_Stop_m2952 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		IEnumerable_1_t763_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		IEnumerator_1_t810_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Enumerator_t809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		List_1_GetEnumerator_m4386_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483979);
		Enumerator_get_Current_m4387_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483980);
		Enumerator_MoveNext_m4388_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483981);
		s_Il2CppMethodIntialized = true;
	}
	StateManagerImpl_t717 * V_0 = {0};
	DataSetImpl_t578 * V_1 = {0};
	Object_t * V_2 = {0};
	Enumerator_t809  V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(39 /* System.Void Vuforia.IQCARWrapper::ObjectTrackerStop() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_1 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		StateManager_t713 * L_2 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_1);
		V_0 = ((StateManagerImpl_t717 *)Castclass(L_2, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		List_1_t624 * L_3 = (__this->___mActiveDataSets_1);
		NullCheck(L_3);
		Enumerator_t809  L_4 = List_1_GetEnumerator_m4386(L_3, /*hidden argument*/List_1_GetEnumerator_m4386_MethodInfo_var);
		V_3 = L_4;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_002f:
		{
			DataSetImpl_t578 * L_5 = Enumerator_get_Current_m4387((&V_3), /*hidden argument*/Enumerator_get_Current_m4387_MethodInfo_var);
			V_1 = L_5;
			DataSetImpl_t578 * L_6 = V_1;
			NullCheck(L_6);
			Object_t* L_7 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_6);
			NullCheck(L_7);
			Object_t* L_8 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t763_il2cpp_TypeInfo_var, L_7);
			V_4 = L_8;
		}

IL_0044:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0055;
			}

IL_0046:
			{
				Object_t* L_9 = V_4;
				NullCheck(L_9);
				Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t810_il2cpp_TypeInfo_var, L_9);
				V_2 = L_10;
				StateManagerImpl_t717 * L_11 = V_0;
				Object_t * L_12 = V_2;
				NullCheck(L_11);
				StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m4021(L_11, L_12, /*hidden argument*/NULL);
			}

IL_0055:
			{
				Object_t* L_13 = V_4;
				NullCheck(L_13);
				bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_13);
				if (L_14)
				{
					goto IL_0046;
				}
			}

IL_005e:
			{
				IL2CPP_LEAVE(0x6C, FINALLY_0060);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t140 *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			{
				Object_t* L_15 = V_4;
				if (!L_15)
				{
					goto IL_006b;
				}
			}

IL_0064:
			{
				Object_t* L_16 = V_4;
				NullCheck(L_16);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_16);
			}

IL_006b:
			{
				IL2CPP_END_FINALLY(96)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_JUMP_TBL(0x6C, IL_006c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
		}

IL_006c:
		{
			bool L_17 = Enumerator_MoveNext_m4388((&V_3), /*hidden argument*/Enumerator_MoveNext_m4388_MethodInfo_var);
			if (L_17)
			{
				goto IL_002f;
			}
		}

IL_0075:
		{
			IL2CPP_LEAVE(0x85, FINALLY_0077);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0077;
	}

FINALLY_0077:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(119)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(119)
	{
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0085:
	{
		return;
	}
}
// Vuforia.DataSet Vuforia.ObjectTrackerImpl::CreateDataSet()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* DataSetImpl_t578_il2cpp_TypeInfo_var;
extern "C" DataSet_t594 * ObjectTrackerImpl_CreateDataSet_m2953 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		DataSetImpl_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	DataSet_t594 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IntPtr_t L_1 = (IntPtr_t)InterfaceFuncInvoker0< IntPtr_t >::Invoke(40 /* System.IntPtr Vuforia.IQCARWrapper::ObjectTrackerCreateDataSet() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_4 = IntPtr_op_Equality_m4356(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral186, /*hidden argument*/NULL);
		return (DataSet_t594 *)NULL;
	}

IL_0024:
	{
		IntPtr_t L_5 = V_0;
		DataSetImpl_t578 * L_6 = (DataSetImpl_t578 *)il2cpp_codegen_object_new (DataSetImpl_t578_il2cpp_TypeInfo_var);
		DataSetImpl__ctor_m2889(L_6, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		List_1_t625 * L_7 = (__this->___mDataSets_2);
		DataSet_t594 * L_8 = V_1;
		NullCheck(L_7);
		VirtActionInvoker1< DataSet_t594 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Add(!0) */, L_7, L_8);
		DataSet_t594 * L_9 = V_1;
		return L_9;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::DestroyDataSet(Vuforia.DataSet,System.Boolean)
extern TypeInfo* DataSetImpl_t578_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_DestroyDataSet_m2954 (ObjectTrackerImpl_t627 * __this, DataSet_t594 * ___dataSet, bool ___destroyTrackables, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t578 * V_0 = {0};
	{
		DataSet_t594 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral187, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		bool L_1 = ___destroyTrackables;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		DataSet_t594 * L_2 = ___dataSet;
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(15 /* System.Void Vuforia.DataSet::DestroyAllTrackables(System.Boolean) */, L_2, 1);
	}

IL_0019:
	{
		DataSet_t594 * L_3 = ___dataSet;
		V_0 = ((DataSetImpl_t578 *)Castclass(L_3, DataSetImpl_t578_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_5 = V_0;
		NullCheck(L_5);
		IntPtr_t L_6 = DataSetImpl_get_DataSetPtr_m2886(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(41 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerDestroyDataSet(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_6);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral188, /*hidden argument*/NULL);
		return 0;
	}

IL_003e:
	{
		List_1_t625 * L_8 = (__this->___mDataSets_2);
		DataSet_t594 * L_9 = ___dataSet;
		NullCheck(L_8);
		VirtFuncInvoker1< bool, DataSet_t594 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Remove(!0) */, L_8, L_9);
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::ActivateDataSet(Vuforia.DataSet)
extern TypeInfo* DataSetImpl_t578_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t763_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t810_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_ActivateDataSet_m2955 (ObjectTrackerImpl_t627 * __this, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		IEnumerable_1_t763_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		IEnumerator_1_t810_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t578 * V_0 = {0};
	StateManagerImpl_t717 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataSet_t594 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral187, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		DataSet_t594 * L_1 = ___dataSet;
		V_0 = ((DataSetImpl_t578 *)Castclass(L_1, DataSetImpl_t578_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_3 = V_0;
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(42 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerActivateDataSet(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_4);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral189, /*hidden argument*/NULL);
		return 0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_6 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		StateManager_t713 * L_7 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_6);
		V_1 = ((StateManagerImpl_t717 *)Castclass(L_7, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		DataSetImpl_t578 * L_8 = V_0;
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_8);
		NullCheck(L_9);
		Object_t* L_10 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t763_il2cpp_TypeInfo_var, L_9);
		V_3 = L_10;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Object_t* L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t810_il2cpp_TypeInfo_var, L_11);
			V_2 = L_12;
			StateManagerImpl_t717 * L_13 = V_1;
			Object_t * L_14 = V_2;
			NullCheck(L_13);
			StateManagerImpl_EnableTrackableBehavioursForTrackable_m4022(L_13, L_14, 1, /*hidden argument*/NULL);
		}

IL_0061:
		{
			Object_t* L_15 = V_3;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		{
			Object_t* L_17 = V_3;
			if (!L_17)
			{
				goto IL_0074;
			}
		}

IL_006e:
		{
			Object_t* L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_18);
		}

IL_0074:
		{
			IL2CPP_END_FINALLY(107)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0075:
	{
		List_1_t624 * L_19 = (__this->___mActiveDataSets_1);
		DataSetImpl_t578 * L_20 = V_0;
		NullCheck(L_19);
		VirtActionInvoker1< DataSetImpl_t578 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Add(!0) */, L_19, L_20);
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::DeactivateDataSet(Vuforia.DataSet)
extern TypeInfo* DataSetImpl_t578_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t763_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t810_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t410_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_DeactivateDataSet_m2956 (ObjectTrackerImpl_t627 * __this, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		IEnumerable_1_t763_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		IEnumerator_1_t810_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		IEnumerator_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t578 * V_0 = {0};
	StateManagerImpl_t717 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataSet_t594 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral187, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		DataSet_t594 * L_1 = ___dataSet;
		V_0 = ((DataSetImpl_t578 *)Castclass(L_1, DataSetImpl_t578_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t578 * L_3 = V_0;
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2886(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(43 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_4);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral190, /*hidden argument*/NULL);
		return 0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_6 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		StateManager_t713 * L_7 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_6);
		V_1 = ((StateManagerImpl_t717 *)Castclass(L_7, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		DataSet_t594 * L_8 = ___dataSet;
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_8);
		NullCheck(L_9);
		Object_t* L_10 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t763_il2cpp_TypeInfo_var, L_9);
		V_3 = L_10;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Object_t* L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t810_il2cpp_TypeInfo_var, L_11);
			V_2 = L_12;
			StateManagerImpl_t717 * L_13 = V_1;
			Object_t * L_14 = V_2;
			NullCheck(L_13);
			StateManagerImpl_EnableTrackableBehavioursForTrackable_m4022(L_13, L_14, 0, /*hidden argument*/NULL);
		}

IL_0061:
		{
			Object_t* L_15 = V_3;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t410_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		{
			Object_t* L_17 = V_3;
			if (!L_17)
			{
				goto IL_0074;
			}
		}

IL_006e:
		{
			Object_t* L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, L_18);
		}

IL_0074:
		{
			IL2CPP_END_FINALLY(107)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0075:
	{
		List_1_t624 * L_19 = (__this->___mActiveDataSets_1);
		DataSetImpl_t578 * L_20 = V_0;
		NullCheck(L_19);
		VirtFuncInvoker1< bool, DataSetImpl_t578 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Remove(!0) */, L_19, L_20);
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetActiveDataSets()
extern const MethodInfo* Enumerable_Cast_TisDataSet_t594_m4382_MethodInfo_var;
extern "C" Object_t* ObjectTrackerImpl_GetActiveDataSets_m2957 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerable_Cast_TisDataSet_t594_m4382_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483982);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t624 * L_0 = (__this->___mActiveDataSets_1);
		Object_t* L_1 = Enumerable_Cast_TisDataSet_t594_m4382(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Cast_TisDataSet_t594_m4382_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetDataSets()
extern "C" Object_t* ObjectTrackerImpl_GetDataSets_m2958 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	{
		List_1_t625 * L_0 = (__this->___mDataSets_2);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::DestroyAllDataSets(System.Boolean)
extern TypeInfo* List_1_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t809_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4389_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4386_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4387_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4388_MethodInfo_var;
extern "C" void ObjectTrackerImpl_DestroyAllDataSets_m2959 (ObjectTrackerImpl_t627 * __this, bool ___destroyTrackables, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		Enumerator_t809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		List_1__ctor_m4389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483983);
		List_1_GetEnumerator_m4386_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483979);
		Enumerator_get_Current_m4387_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483980);
		Enumerator_MoveNext_m4388_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483981);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t624 * V_0 = {0};
	DataSetImpl_t578 * V_1 = {0};
	int32_t V_2 = 0;
	Enumerator_t809  V_3 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t624 * L_0 = (__this->___mActiveDataSets_1);
		List_1_t624 * L_1 = (List_1_t624 *)il2cpp_codegen_object_new (List_1_t624_il2cpp_TypeInfo_var);
		List_1__ctor_m4389(L_1, L_0, /*hidden argument*/List_1__ctor_m4389_MethodInfo_var);
		V_0 = L_1;
		List_1_t624 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t809  L_3 = List_1_GetEnumerator_m4386(L_2, /*hidden argument*/List_1_GetEnumerator_m4386_MethodInfo_var);
		V_3 = L_3;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0015:
		{
			DataSetImpl_t578 * L_4 = Enumerator_get_Current_m4387((&V_3), /*hidden argument*/Enumerator_get_Current_m4387_MethodInfo_var);
			V_1 = L_4;
			DataSetImpl_t578 * L_5 = V_1;
			VirtFuncInvoker1< bool, DataSet_t594 * >::Invoke(13 /* System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet) */, __this, L_5);
		}

IL_0025:
		{
			bool L_6 = Enumerator_MoveNext_m4388((&V_3), /*hidden argument*/Enumerator_MoveNext_m4388_MethodInfo_var);
			if (L_6)
			{
				goto IL_0015;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_003e:
	{
		List_1_t625 * L_7 = (__this->___mDataSets_2);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Count() */, L_7);
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)1));
		goto IL_0066;
	}

IL_004e:
	{
		List_1_t625 * L_9 = (__this->___mDataSets_2);
		int32_t L_10 = V_2;
		NullCheck(L_9);
		DataSet_t594 * L_11 = (DataSet_t594 *)VirtFuncInvoker1< DataSet_t594 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Item(System.Int32) */, L_9, L_10);
		bool L_12 = ___destroyTrackables;
		VirtFuncInvoker2< bool, DataSet_t594 *, bool >::Invoke(11 /* System.Boolean Vuforia.ObjectTracker::DestroyDataSet(Vuforia.DataSet,System.Boolean) */, __this, L_11, L_12);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0066:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t625 * L_15 = (__this->___mDataSets_2);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Clear() */, L_15);
		return;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::PersistExtendedTracking(System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_PersistExtendedTracking_m2960 (ObjectTrackerImpl_t627 * __this, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Object_t * G_B3_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___on;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		NullCheck(G_B3_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(44 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerPersistExtendedTracking(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, G_B3_1, G_B3_0);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral191, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::ResetExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_ResetExtendedTracking_m2961 (ObjectTrackerImpl_t627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral192, /*hidden argument*/NULL);
		return 0;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_1 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(45 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerResetExtendedTracking() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_1);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral193, /*hidden argument*/NULL);
		return 0;
	}

IL_002c:
	{
		return 1;
	}
}
// Vuforia.MarkerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImplMethodDeclarations.h"



// System.Int32 Vuforia.MarkerImpl::get_MarkerID()
extern "C" int32_t MarkerImpl_get_MarkerID_m2962 (MarkerImpl_t628 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CMarkerIDU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void Vuforia.MarkerImpl::set_MarkerID(System.Int32)
extern "C" void MarkerImpl_set_MarkerID_m2963 (MarkerImpl_t628 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CMarkerIDU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void Vuforia.MarkerImpl::.ctor(System.String,System.Int32,System.Single,System.Int32)
extern "C" void MarkerImpl__ctor_m2964 (MarkerImpl_t628 * __this, String_t* ___name, int32_t ___id, float ___size, int32_t ___markerID, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2712(__this, L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ___size;
		__this->___mSize_2 = L_2;
		int32_t L_3 = ___markerID;
		MarkerImpl_set_MarkerID_m2963(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.MarkerImpl::GetSize()
extern "C" float MarkerImpl_GetSize_m2965 (MarkerImpl_t628 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.MarkerImpl::SetSize(System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void MarkerImpl_SetSize_m2966 (MarkerImpl_t628 * __this, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___size;
		__this->___mSize_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_1 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		float L_3 = ___size;
		NullCheck(L_1);
		InterfaceFuncInvoker2< int32_t, int32_t, float >::Invoke(46 /* System.Int32 Vuforia.IQCARWrapper::MarkerSetSize(System.Int32,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		return;
	}
}
// System.Boolean Vuforia.MarkerImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool MarkerImpl_StartExtendedTracking_m2967 (MarkerImpl_t628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.MarkerImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool MarkerImpl_StopExtendedTracking_m2968 (MarkerImpl_t628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2715(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerMethodDeclarations.h"



// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTracker::CreateMarker(System.Int32,System.String,System.Single)
// System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean)
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTracker::GetMarkers()
// Vuforia.Marker Vuforia.MarkerTracker::GetMarkerByMarkerID(System.Int32)
// System.Void Vuforia.MarkerTracker::DestroyAllMarkers(System.Boolean)
// System.Void Vuforia.MarkerTracker::.ctor()
extern "C" void MarkerTracker__ctor_m2969 (MarkerTracker_t629 * __this, const MethodInfo* method)
{
	{
		Tracker__ctor_m2946(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImplMethodDeclarations.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4MethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"


// System.Boolean Vuforia.MarkerTrackerImpl::Start()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool MarkerTrackerImpl_Start_m2970 (MarkerTrackerImpl_t631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(47 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerStart() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral185, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 1);
		return 1;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::Stop()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t811_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4390_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4391_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4392_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4393_MethodInfo_var;
extern "C" void MarkerTrackerImpl_Stop_m2971 (MarkerTrackerImpl_t631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		Enumerator_t811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1095);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483984);
		ValueCollection_GetEnumerator_m4391_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		Enumerator_get_Current_m4392_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483986);
		Enumerator_MoveNext_m4393_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483987);
		s_Il2CppMethodIntialized = true;
	}
	StateManagerImpl_t717 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t811  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(48 /* System.Void Vuforia.IQCARWrapper::MarkerTrackerStop() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_1 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		StateManager_t713 * L_2 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_1);
		V_0 = ((StateManagerImpl_t717 *)Castclass(L_2, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		Dictionary_2_t630 * L_3 = (__this->___mMarkerDict_1);
		NullCheck(L_3);
		ValueCollection_t812 * L_4 = Dictionary_2_get_Values_m4390(L_3, /*hidden argument*/Dictionary_2_get_Values_m4390_MethodInfo_var);
		NullCheck(L_4);
		Enumerator_t811  L_5 = ValueCollection_GetEnumerator_m4391(L_4, /*hidden argument*/ValueCollection_GetEnumerator_m4391_MethodInfo_var);
		V_2 = L_5;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_0034:
		{
			Object_t * L_6 = Enumerator_get_Current_m4392((&V_2), /*hidden argument*/Enumerator_get_Current_m4392_MethodInfo_var);
			V_1 = L_6;
			StateManagerImpl_t717 * L_7 = V_0;
			Object_t * L_8 = V_1;
			NullCheck(L_7);
			StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m4021(L_7, L_8, /*hidden argument*/NULL);
		}

IL_0043:
		{
			bool L_9 = Enumerator_MoveNext_m4393((&V_2), /*hidden argument*/Enumerator_MoveNext_m4393_MethodInfo_var);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_004c:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t811_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t811_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_005c:
	{
		return;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTrackerImpl::CreateMarker(System.Int32,System.String,System.Single)
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MarkerImpl_t628_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern "C" MarkerAbstractBehaviour_t58 * MarkerTrackerImpl_CreateMarker_m2972 (MarkerTrackerImpl_t631 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		MarkerImpl_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1096);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	StateManagerImpl_t717 * V_2 = {0};
	MarkerAbstractBehaviour_t58 * V_3 = {0};
	{
		int32_t L_0 = ___markerID;
		String_t* L_1 = ___trackableName;
		float L_2 = ___size;
		int32_t L_3 = MarkerTrackerImpl_RegisterMarker_m2978(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_5 = ___markerID;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1948(NULL /*static, unused*/, (String_t*) &_stringLiteral194, L_7, (String_t*) &_stringLiteral107, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (MarkerAbstractBehaviour_t58 *)NULL;
	}

IL_002a:
	{
		String_t* L_9 = ___trackableName;
		int32_t L_10 = V_0;
		float L_11 = ___size;
		int32_t L_12 = ___markerID;
		MarkerImpl_t628 * L_13 = (MarkerImpl_t628 *)il2cpp_codegen_object_new (MarkerImpl_t628_il2cpp_TypeInfo_var);
		MarkerImpl__ctor_m2964(L_13, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Dictionary_2_t630 * L_14 = (__this->___mMarkerDict_1);
		int32_t L_15 = V_0;
		Object_t * L_16 = V_1;
		NullCheck(L_14);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::set_Item(!0,!1) */, L_14, L_15, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_17 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		StateManager_t713 * L_18 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_17);
		V_2 = ((StateManagerImpl_t717 *)Castclass(L_18, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		StateManagerImpl_t717 * L_19 = V_2;
		Object_t * L_20 = V_1;
		String_t* L_21 = ___trackableName;
		NullCheck(L_19);
		MarkerAbstractBehaviour_t58 * L_22 = StateManagerImpl_CreateNewMarkerBehaviourForMarker_m4019(L_19, L_20, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		MarkerAbstractBehaviour_t58 * L_23 = V_3;
		return L_23;
	}
}
// System.Boolean Vuforia.MarkerTrackerImpl::DestroyMarker(Vuforia.Marker,System.Boolean)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Marker_t739_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern "C" bool MarkerTrackerImpl_DestroyMarker_m2973 (MarkerTrackerImpl_t631 * __this, Object_t * ___marker, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Marker_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1094);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		s_Il2CppMethodIntialized = true;
	}
	StateManager_t713 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t * L_1 = ___marker;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(54 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerDestroyMarker(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_2);
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_4 = ___marker;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.Marker::get_MarkerID() */, Marker_t739_il2cpp_TypeInfo_var, L_4);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1948(NULL /*static, unused*/, (String_t*) &_stringLiteral195, L_7, (String_t*) &_stringLiteral107, /*hidden argument*/NULL);
		Debug_LogError_m403(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return 0;
	}

IL_0033:
	{
		Dictionary_2_t630 * L_9 = (__this->___mMarkerDict_1);
		Object_t * L_10 = ___marker;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_10);
		NullCheck(L_9);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Remove(!0) */, L_9, L_11);
		bool L_12 = ___destroyGameObject;
		if (!L_12)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_13 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		StateManager_t713 * L_14 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_13);
		V_0 = L_14;
		StateManager_t713 * L_15 = V_0;
		Object_t * L_16 = ___marker;
		NullCheck(L_15);
		VirtActionInvoker2< Object_t *, bool >::Invoke(6 /* System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean) */, L_15, L_16, 1);
	}

IL_005b:
	{
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTrackerImpl::GetMarkers()
extern const MethodInfo* Dictionary_2_get_Values_m4390_MethodInfo_var;
extern "C" Object_t* MarkerTrackerImpl_GetMarkers_m2974 (MarkerTrackerImpl_t631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483984);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t630 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t812 * L_1 = Dictionary_2_get_Values_m4390(L_0, /*hidden argument*/Dictionary_2_get_Values_m4390_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.Marker Vuforia.MarkerTrackerImpl::GetMarkerByMarkerID(System.Int32)
extern TypeInfo* Marker_t739_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t811_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4390_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4391_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4392_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4393_MethodInfo_var;
extern "C" Object_t * MarkerTrackerImpl_GetMarkerByMarkerID_m2975 (MarkerTrackerImpl_t631 * __this, int32_t ___markerID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marker_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1094);
		Enumerator_t811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1095);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483984);
		ValueCollection_GetEnumerator_m4391_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		Enumerator_get_Current_m4392_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483986);
		Enumerator_MoveNext_m4393_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483987);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t811  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t630 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t812 * L_1 = Dictionary_2_get_Values_m4390(L_0, /*hidden argument*/Dictionary_2_get_Values_m4390_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t811  L_2 = ValueCollection_GetEnumerator_m4391(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4391_MethodInfo_var);
		V_2 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0013:
		{
			Object_t * L_3 = Enumerator_get_Current_m4392((&V_2), /*hidden argument*/Enumerator_get_Current_m4392_MethodInfo_var);
			V_0 = L_3;
			Object_t * L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.Marker::get_MarkerID() */, Marker_t739_il2cpp_TypeInfo_var, L_4);
			int32_t L_6 = ___markerID;
			if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
			{
				goto IL_0028;
			}
		}

IL_0024:
		{
			Object_t * L_7 = V_0;
			V_1 = L_7;
			IL2CPP_LEAVE(0x43, FINALLY_0033);
		}

IL_0028:
		{
			bool L_8 = Enumerator_MoveNext_m4393((&V_2), /*hidden argument*/Enumerator_MoveNext_m4393_MethodInfo_var);
			if (L_8)
			{
				goto IL_0013;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t811_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t811_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0041:
	{
		return (Object_t *)NULL;
	}

IL_0043:
	{
		Object_t * L_9 = V_1;
		return L_9;
	}
}
// Vuforia.Marker Vuforia.MarkerTrackerImpl::InternalCreateMarker(System.Int32,System.String,System.Single)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MarkerImpl_t628_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t115_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t127_il2cpp_TypeInfo_var;
extern "C" Object_t * MarkerTrackerImpl_InternalCreateMarker_m2976 (MarkerTrackerImpl_t631 * __this, int32_t ___markerID, String_t* ___name, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		MarkerImpl_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1096);
		ObjectU5BU5D_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		Int32_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	ObjectU5BU5D_t115* V_2 = {0};
	{
		int32_t L_0 = ___markerID;
		String_t* L_1 = ___name;
		float L_2 = ___size;
		int32_t L_3 = MarkerTrackerImpl_RegisterMarker_m2978(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m416(NULL /*static, unused*/, (String_t*) &_stringLiteral196, L_5, (String_t*) &_stringLiteral197, /*hidden argument*/NULL);
		Debug_LogWarning_m4394(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}

IL_0025:
	{
		Dictionary_2_t630 * L_7 = (__this->___mMarkerDict_1);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		bool L_9 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ContainsKey(!0) */, L_7, L_8);
		if (L_9)
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_10 = ___name;
		int32_t L_11 = V_0;
		float L_12 = ___size;
		int32_t L_13 = ___markerID;
		MarkerImpl_t628 * L_14 = (MarkerImpl_t628 *)il2cpp_codegen_object_new (MarkerImpl_t628_il2cpp_TypeInfo_var);
		MarkerImpl__ctor_m2964(L_14, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Dictionary_2_t630 * L_15 = (__this->___mMarkerDict_1);
		int32_t L_16 = V_0;
		Object_t * L_17 = V_1;
		NullCheck(L_15);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::set_Item(!0,!1) */, L_15, L_16, L_17);
		V_2 = ((ObjectU5BU5D_t115*)SZArrayNew(ObjectU5BU5D_t115_il2cpp_TypeInfo_var, 4));
		ObjectU5BU5D_t115* L_18 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, (String_t*) &_stringLiteral198);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 0)) = (Object_t *)(String_t*) &_stringLiteral198;
		ObjectU5BU5D_t115* L_19 = V_2;
		Object_t * L_20 = V_1;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t565_il2cpp_TypeInfo_var, L_20);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 1)) = (Object_t *)L_21;
		ObjectU5BU5D_t115* L_22 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		ArrayElementTypeCheck (L_22, (String_t*) &_stringLiteral199);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 2)) = (Object_t *)(String_t*) &_stringLiteral199;
		ObjectU5BU5D_t115* L_23 = V_2;
		Object_t * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_24);
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(Int32_t127_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 3);
		ArrayElementTypeCheck (L_23, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 3)) = (Object_t *)L_27;
		ObjectU5BU5D_t115* L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m388(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Debug_Log_m417(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_0083:
	{
		Dictionary_2_t630 * L_30 = (__this->___mMarkerDict_1);
		int32_t L_31 = V_0;
		NullCheck(L_30);
		Object_t * L_32 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Item(!0) */, L_30, L_31);
		return L_32;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::DestroyAllMarkers(System.Boolean)
extern TypeInfo* List_1_t813_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t814_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4390_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4395_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4396_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4397_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4398_MethodInfo_var;
extern "C" void MarkerTrackerImpl_DestroyAllMarkers_m2977 (MarkerTrackerImpl_t631 * __this, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1097);
		Enumerator_t814_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1098);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483984);
		List_1__ctor_m4395_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483988);
		List_1_GetEnumerator_m4396_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483989);
		Enumerator_get_Current_m4397_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483990);
		Enumerator_MoveNext_m4398_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483991);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t813 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t814  V_2 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t630 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t812 * L_1 = Dictionary_2_get_Values_m4390(L_0, /*hidden argument*/Dictionary_2_get_Values_m4390_MethodInfo_var);
		List_1_t813 * L_2 = (List_1_t813 *)il2cpp_codegen_object_new (List_1_t813_il2cpp_TypeInfo_var);
		List_1__ctor_m4395(L_2, L_1, /*hidden argument*/List_1__ctor_m4395_MethodInfo_var);
		V_0 = L_2;
		List_1_t813 * L_3 = V_0;
		NullCheck(L_3);
		Enumerator_t814  L_4 = List_1_GetEnumerator_m4396(L_3, /*hidden argument*/List_1_GetEnumerator_m4396_MethodInfo_var);
		V_2 = L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001a:
		{
			Object_t * L_5 = Enumerator_get_Current_m4397((&V_2), /*hidden argument*/Enumerator_get_Current_m4397_MethodInfo_var);
			V_1 = L_5;
			Object_t * L_6 = V_1;
			bool L_7 = ___destroyGameObject;
			VirtFuncInvoker2< bool, Object_t *, bool >::Invoke(9 /* System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean) */, __this, L_6, L_7);
		}

IL_002b:
		{
			bool L_8 = Enumerator_MoveNext_m4398((&V_2), /*hidden argument*/Enumerator_MoveNext_m4398_MethodInfo_var);
			if (L_8)
			{
				goto IL_001a;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t814_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t814_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Int32 Vuforia.MarkerTrackerImpl::RegisterMarker(System.Int32,System.String,System.Single)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" int32_t MarkerTrackerImpl_RegisterMarker_m2978 (MarkerTrackerImpl_t631 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___markerID;
		String_t* L_2 = ___trackableName;
		float L_3 = ___size;
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, String_t*, float >::Invoke(53 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::.ctor()
extern TypeInfo* Dictionary_2_t630_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4399_MethodInfo_var;
extern "C" void MarkerTrackerImpl__ctor_m2979 (MarkerTrackerImpl_t631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t630_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1099);
		Dictionary_2__ctor_m4399_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483992);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t630 * L_0 = (Dictionary_2_t630 *)il2cpp_codegen_object_new (Dictionary_2_t630_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4399(L_0, /*hidden argument*/Dictionary_2__ctor_m4399_MethodInfo_var);
		__this->___mMarkerDict_1 = L_0;
		MarkerTracker__ctor_m2969(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void Vuforia.MultiTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern "C" void MultiTargetImpl__ctor_m2980 (MultiTargetImpl_t632 * __this, String_t* ___name, int32_t ___id, DataSet_t594 * ___dataSet, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t594 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2717(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.MultiTargetImpl::GetSize()
extern "C" Vector3_t15  MultiTargetImpl_GetSize_m2981 (MultiTargetImpl_t632 * __this, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral200, /*hidden argument*/NULL);
		Vector3_t15  L_0 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.MultiTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void MultiTargetImpl_SetSize_m2982 (MultiTargetImpl_t632 * __this, Vector3_t15  ___size, const MethodInfo* method)
{
	{
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral201, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorMethodDeclarations.h"



// System.Boolean Vuforia.WebCamTexAdaptor::get_DidUpdateThisFrame()
// System.Boolean Vuforia.WebCamTexAdaptor::get_IsPlaying()
// UnityEngine.Texture Vuforia.WebCamTexAdaptor::get_Texture()
// System.Void Vuforia.WebCamTexAdaptor::Play()
// System.Void Vuforia.WebCamTexAdaptor::Stop()
// System.Void Vuforia.WebCamTexAdaptor::.ctor()
extern "C" void WebCamTexAdaptor__ctor_m2983 (WebCamTexAdaptor_t633 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.NullWebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapto.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.NullWebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdaptoMethodDeclarations.h"

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// Vuforia.PlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili_0MethodDeclarations.h"


// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
extern TypeInfo* DateTime_t111_il2cpp_TypeInfo_var;
extern "C" bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m2984 (NullWebCamTexAdaptor_t634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t112  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t111_il2cpp_TypeInfo_var);
		DateTime_t111  L_0 = DateTime_get_Now_m4400(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t111  L_1 = (__this->___mLastFrame_4);
		TimeSpan_t112  L_2 = DateTime_op_Subtraction_m334(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		double L_3 = TimeSpan_get_TotalMilliseconds_m4401((&V_0), /*hidden argument*/NULL);
		double L_4 = (__this->___mMsBetweenFrames_3);
		if ((!(((double)L_3) > ((double)L_4))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t111_il2cpp_TypeInfo_var);
		DateTime_t111  L_5 = DateTime_get_Now_m4400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mLastFrame_4 = L_5;
		return 1;
	}

IL_002d:
	{
		return 0;
	}
}
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
extern "C" bool NullWebCamTexAdaptor_get_IsPlaying_m2985 (NullWebCamTexAdaptor_t634 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mPseudoPlaying_2);
		return L_0;
	}
}
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
extern "C" Texture_t321 * NullWebCamTexAdaptor_get_Texture_m2986 (NullWebCamTexAdaptor_t634 * __this, const MethodInfo* method)
{
	{
		Texture2D_t270 * L_0 = (__this->___mTexture_1);
		return L_0;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.QCARRenderer/Vec2I)
extern TypeInfo* Texture2D_t270_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t111_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t112_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayModeEditorUtility_t636_il2cpp_TypeInfo_var;
extern "C" void NullWebCamTexAdaptor__ctor_m2987 (NullWebCamTexAdaptor_t634 * __this, int32_t ___requestedFPS, Vec2I_t663  ___requestedTextureSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1100);
		DateTime_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		TimeSpan_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1101);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		IPlayModeEditorUtility_t636_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1102);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mPseudoPlaying_2 = 1;
		WebCamTexAdaptor__ctor_m2983(__this, /*hidden argument*/NULL);
		int32_t L_0 = ((&___requestedTextureSize)->___x_0);
		int32_t L_1 = ((&___requestedTextureSize)->___y_1);
		Texture2D_t270 * L_2 = (Texture2D_t270 *)il2cpp_codegen_object_new (Texture2D_t270_il2cpp_TypeInfo_var);
		Texture2D__ctor_m4402(L_2, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTexture_1 = L_2;
		int32_t L_3 = ___requestedFPS;
		__this->___mMsBetweenFrames_3 = ((double)((double)(1000.0)/(double)(((double)L_3))));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t111_il2cpp_TypeInfo_var);
		DateTime_t111  L_4 = DateTime_get_Now_m4400(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t112_il2cpp_TypeInfo_var);
		TimeSpan_t112  L_5 = TimeSpan_FromDays_m4403(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		DateTime_t111  L_6 = DateTime_op_Subtraction_m4404(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->___mLastFrame_4 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_7 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0080;
		}
	}
	{
		Object_t * L_8 = PlayModeEditorUtility_get_Instance_m2994(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(0 /* System.Void Vuforia.IPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String) */, IPlayModeEditorUtility_t636_il2cpp_TypeInfo_var, L_8, (String_t*) &_stringLiteral202, (String_t*) &_stringLiteral203, (String_t*) &_stringLiteral204);
		Debug_LogError_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral203, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
extern "C" void NullWebCamTexAdaptor_Play_m2988 (NullWebCamTexAdaptor_t634 * __this, const MethodInfo* method)
{
	{
		__this->___mPseudoPlaying_2 = 1;
		return;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
extern "C" void NullWebCamTexAdaptor_Stop_m2989 (NullWebCamTexAdaptor_t634 * __this, const MethodInfo* method)
{
	{
		__this->___mPseudoPlaying_2 = 0;
		return;
	}
}
// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtiliMethodDeclarations.h"

// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"


// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String)
extern "C" void NullPlayModeEditorUtility_DisplayDialog_m2990 (NullPlayModeEditorUtility_t635 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.WebCamProfile/ProfileCollection Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::LoadAndParseWebcamProfiles(System.String)
extern TypeInfo* ProfileCollection_t736_il2cpp_TypeInfo_var;
extern "C" ProfileCollection_t736  NullPlayModeEditorUtility_LoadAndParseWebcamProfiles_m2991 (NullPlayModeEditorUtility_t635 * __this, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileCollection_t736_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1103);
		s_Il2CppMethodIntialized = true;
	}
	ProfileCollection_t736  V_0 = {0};
	{
		Initobj (ProfileCollection_t736_il2cpp_TypeInfo_var, (&V_0));
		ProfileCollection_t736  L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::RestartPlayMode()
extern "C" void NullPlayModeEditorUtility_RestartPlayMode_m2992 (NullPlayModeEditorUtility_t635 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::.ctor()
extern "C" void NullPlayModeEditorUtility__ctor_m2993 (NullPlayModeEditorUtility_t635 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili_0.h"
#ifndef _MSC_VER
#else
#endif



// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
extern TypeInfo* PlayModeEditorUtility_t637_il2cpp_TypeInfo_var;
extern TypeInfo* NullPlayModeEditorUtility_t635_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayModeEditorUtility_get_Instance_m2994 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayModeEditorUtility_t637_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		NullPlayModeEditorUtility_t635_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1105);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((PlayModeEditorUtility_t637_StaticFields*)PlayModeEditorUtility_t637_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullPlayModeEditorUtility_t635 * L_1 = (NullPlayModeEditorUtility_t635 *)il2cpp_codegen_object_new (NullPlayModeEditorUtility_t635_il2cpp_TypeInfo_var);
		NullPlayModeEditorUtility__ctor_m2993(L_1, /*hidden argument*/NULL);
		((PlayModeEditorUtility_t637_StaticFields*)PlayModeEditorUtility_t637_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((PlayModeEditorUtility_t637_StaticFields*)PlayModeEditorUtility_t637_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
extern TypeInfo* PlayModeEditorUtility_t637_il2cpp_TypeInfo_var;
extern "C" void PlayModeEditorUtility_set_Instance_m2995 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayModeEditorUtility_t637_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((PlayModeEditorUtility_t637_StaticFields*)PlayModeEditorUtility_t637_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
extern "C" void PlayModeEditorUtility__ctor_m2996 (PlayModeEditorUtility_t637 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactorMethodDeclarations.h"



// System.Void Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::.ctor()
extern "C" void NullPremiumObjectFactory__ctor_m2997 (NullPremiumObjectFactory_t638 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0MethodDeclarations.h"



// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern TypeInfo* PremiumObjectFactory_t640_il2cpp_TypeInfo_var;
extern TypeInfo* NullPremiumObjectFactory_t638_il2cpp_TypeInfo_var;
extern "C" Object_t * PremiumObjectFactory_get_Instance_m2998 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PremiumObjectFactory_t640_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1106);
		NullPremiumObjectFactory_t638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1107);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((PremiumObjectFactory_t640_StaticFields*)PremiumObjectFactory_t640_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullPremiumObjectFactory_t638 * L_1 = (NullPremiumObjectFactory_t638 *)il2cpp_codegen_object_new (NullPremiumObjectFactory_t638_il2cpp_TypeInfo_var);
		NullPremiumObjectFactory__ctor_m2997(L_1, /*hidden argument*/NULL);
		((PremiumObjectFactory_t640_StaticFields*)PremiumObjectFactory_t640_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((PremiumObjectFactory_t640_StaticFields*)PremiumObjectFactory_t640_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
extern TypeInfo* PremiumObjectFactory_t640_il2cpp_TypeInfo_var;
extern "C" void PremiumObjectFactory_set_Instance_m2999 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PremiumObjectFactory_t640_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1106);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((PremiumObjectFactory_t640_StaticFields*)PremiumObjectFactory_t640_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.PremiumObjectFactory::.ctor()
extern "C" void PremiumObjectFactory__ctor_m3000 (PremiumObjectFactory_t640 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"


// Vuforia.QCARManager Vuforia.QCARManager::get_Instance()
extern const Il2CppType* QCARManager_t155_0_0_0_var;
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t660_il2cpp_TypeInfo_var;
extern "C" QCARManager_t155 * QCARManager_get_Instance_m484 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t155_0_0_0_var = il2cpp_codegen_type_from_index(78);
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARManagerImpl_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_0 = ((QCARManager_t155_StaticFields*)QCARManager_t155_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARManager_t155_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4313(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
			QCARManager_t155 * L_3 = ((QCARManager_t155_StaticFields*)QCARManager_t155_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			QCARManagerImpl_t660 * L_4 = (QCARManagerImpl_t660 *)il2cpp_codegen_object_new (QCARManagerImpl_t660_il2cpp_TypeInfo_var);
			QCARManagerImpl__ctor_m3030(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
			((QCARManager_t155_StaticFields*)QCARManager_t155_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_6 = ((QCARManager_t155_StaticFields*)QCARManager_t155_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_6;
	}
}
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManager::get_WorldCenterMode()
// System.Void Vuforia.QCARManager::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManager::get_WorldCenter()
// System.Void Vuforia.QCARManager::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
// UnityEngine.Transform Vuforia.QCARManager::get_ARCameraTransform()
// System.Void Vuforia.QCARManager::set_ARCameraTransform(UnityEngine.Transform)
// System.Boolean Vuforia.QCARManager::get_Initialized()
// System.Int32 Vuforia.QCARManager::get_QCARFrameIndex()
// System.Boolean Vuforia.QCARManager::Init()
// System.Void Vuforia.QCARManager::Deinit()
// System.Void Vuforia.QCARManager::.ctor()
extern "C" void QCARManager__ctor_m3001 (QCARManager_t155 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManager::.cctor()
extern "C" void QCARManager__cctor_m3002 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pos.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_PosMethodDeclarations.h"



// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_TraMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/TrackableResultData
void TrackableResultData_t642_marshal(const TrackableResultData_t642& unmarshaled, TrackableResultData_t642_marshaled& marshaled)
{
	marshaled.___pose_0 = unmarshaled.___pose_0;
	marshaled.___status_1 = unmarshaled.___status_1;
	marshaled.___id_2 = unmarshaled.___id_2;
}
void TrackableResultData_t642_marshal_back(const TrackableResultData_t642_marshaled& marshaled, TrackableResultData_t642& unmarshaled)
{
	unmarshaled.___pose_0 = marshaled.___pose_0;
	unmarshaled.___status_1 = marshaled.___status_1;
	unmarshaled.___id_2 = marshaled.___id_2;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/TrackableResultData
void TrackableResultData_t642_marshal_cleanup(TrackableResultData_t642_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_VirMethodDeclarations.h"



// Vuforia.QCARManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ObbMethodDeclarations.h"



// Vuforia.QCARManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb_0MethodDeclarations.h"



// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_WorMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/WordResultData
void WordResultData_t646_marshal(const WordResultData_t646& unmarshaled, WordResultData_t646_marshaled& marshaled)
{
	marshaled.___pose_0 = unmarshaled.___pose_0;
	marshaled.___status_1 = unmarshaled.___status_1;
	marshaled.___id_2 = unmarshaled.___id_2;
	marshaled.___orientedBoundingBox_3 = unmarshaled.___orientedBoundingBox_3;
}
void WordResultData_t646_marshal_back(const WordResultData_t646_marshaled& marshaled, WordResultData_t646& unmarshaled)
{
	unmarshaled.___pose_0 = marshaled.___pose_0;
	unmarshaled.___status_1 = marshaled.___status_1;
	unmarshaled.___id_2 = marshaled.___id_2;
	unmarshaled.___orientedBoundingBox_3 = marshaled.___orientedBoundingBox_3;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/WordResultData
void WordResultData_t646_marshal_cleanup(WordResultData_t646_marshaled& marshaled)
{
}
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0MethodDeclarations.h"



// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Ima.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ImaMethodDeclarations.h"



// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Mes.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_MesMethodDeclarations.h"



// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_SmaMethodDeclarations.h"



// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_SurMethodDeclarations.h"



// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ProMethodDeclarations.h"



// Vuforia.QCARManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Fra.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_FraMethodDeclarations.h"



// Vuforia.QCARManagerImpl/AutoRotationState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Aut.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/AutoRotationState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_AutMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/AutoRotationState
void AutoRotationState_t654_marshal(const AutoRotationState_t654& unmarshaled, AutoRotationState_t654_marshaled& marshaled)
{
	marshaled.___setOnPause_0 = unmarshaled.___setOnPause_0;
	marshaled.___autorotateToPortrait_1 = unmarshaled.___autorotateToPortrait_1;
	marshaled.___autorotateToPortraitUpsideDown_2 = unmarshaled.___autorotateToPortraitUpsideDown_2;
	marshaled.___autorotateToLandscapeLeft_3 = unmarshaled.___autorotateToLandscapeLeft_3;
	marshaled.___autorotateToLandscapeRight_4 = unmarshaled.___autorotateToLandscapeRight_4;
}
void AutoRotationState_t654_marshal_back(const AutoRotationState_t654_marshaled& marshaled, AutoRotationState_t654& unmarshaled)
{
	unmarshaled.___setOnPause_0 = marshaled.___setOnPause_0;
	unmarshaled.___autorotateToPortrait_1 = marshaled.___autorotateToPortrait_1;
	unmarshaled.___autorotateToPortraitUpsideDown_2 = marshaled.___autorotateToPortraitUpsideDown_2;
	unmarshaled.___autorotateToLandscapeLeft_3 = marshaled.___autorotateToLandscapeLeft_3;
	unmarshaled.___autorotateToLandscapeRight_4 = marshaled.___autorotateToLandscapeRight_4;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/AutoRotationState
void AutoRotationState_t654_marshal_cleanup(AutoRotationState_t654_marshaled& marshaled)
{
}
// Vuforia.QCARManagerImpl/<>c__DisplayClass3
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_U3C.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/<>c__DisplayClass3
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_U3CMethodDeclarations.h"



// System.Void Vuforia.QCARManagerImpl/<>c__DisplayClass3::.ctor()
extern "C" void U3CU3Ec__DisplayClass3__ctor_m3003 (U3CU3Ec__DisplayClass3_t655 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.QCARManagerImpl/TrackableResultData)
extern "C" bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3004 (U3CU3Ec__DisplayClass3_t655 * __this, TrackableResultData_t642  ___tr, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___tr)->___id_2);
		int32_t L_1 = (__this->___id_0);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.LinkedList`1<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_gen.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Predicate_1_gen_2.h"
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"
// System.Collections.Generic.LinkedListNode`1<System.Int32>
#include "System_System_Collections_Generic_LinkedListNode_1_gen.h"
// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0.h"
// Vuforia.WordImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImpl.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
// System.Collections.Generic.LinkedList`1<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_genMethodDeclarations.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImplMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"
// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Predicate_1_gen_2MethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<System.Int32>
#include "System_System_Collections_Generic_LinkedListNode_1_genMethodDeclarations.h"
// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0MethodDeclarations.h"
// Vuforia.WordImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImplMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6MethodDeclarations.h"
struct Array_t;
struct TrackableResultDataU5BU5D_t656;
struct Predicate_1_t815;
// Declaration System.Boolean System.Array::Exists<Vuforia.QCARManagerImpl/TrackableResultData>(!!0[],System.Predicate`1<!!0>)
// System.Boolean System.Array::Exists<Vuforia.QCARManagerImpl/TrackableResultData>(!!0[],System.Predicate`1<!!0>)
extern "C" bool Array_Exists_TisTrackableResultData_t642_m4405_gshared (Object_t * __this /* static, unused */, TrackableResultDataU5BU5D_t656* p0, Predicate_1_t815 * p1, const MethodInfo* method);
#define Array_Exists_TisTrackableResultData_t642_m4405(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, TrackableResultDataU5BU5D_t656*, Predicate_1_t815 *, const MethodInfo*))Array_Exists_TisTrackableResultData_t642_m4405_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t132;
struct IEnumerable_1_t762;
struct Enumerable_t132;
struct IEnumerable_1_t136;
// Declaration System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" bool Enumerable_Any_TisObject_t_m4407_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_Any_TisObject_t_m4407(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m4407_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Boolean System.Linq.Enumerable::Any<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisReconstructionAbstractBehaviour_t68_m4406(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m4407_gshared)(__this /* static, unused */, p0, method)


// System.Void Vuforia.QCARManagerImpl::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
extern "C" void QCARManagerImpl_set_WorldCenterMode_m3005 (QCARManagerImpl_t660 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mWorldCenterMode_1 = L_0;
		return;
	}
}
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManagerImpl::get_WorldCenterMode()
extern "C" int32_t QCARManagerImpl_get_WorldCenterMode_m3006 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWorldCenterMode_1);
		return L_0;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
extern "C" void QCARManagerImpl_set_WorldCenter_m3007 (QCARManagerImpl_t660 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___mWorldCenter_2 = L_0;
		return;
	}
}
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManagerImpl::get_WorldCenter()
extern "C" Object_t * QCARManagerImpl_get_WorldCenter_m3008 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mWorldCenter_2);
		return L_0;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_ARCameraTransform(UnityEngine.Transform)
extern "C" void QCARManagerImpl_set_ARCameraTransform_m3009 (QCARManagerImpl_t660 * __this, Transform_t11 * ___value, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = ___value;
		__this->___mARCameraTransform_3 = L_0;
		return;
	}
}
// UnityEngine.Transform Vuforia.QCARManagerImpl::get_ARCameraTransform()
extern "C" Transform_t11 * QCARManagerImpl_get_ARCameraTransform_m3010 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___mARCameraTransform_3);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::get_Initialized()
extern "C" bool QCARManagerImpl_get_Initialized_m3011 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitialized_12);
		return L_0;
	}
}
// System.Int32 Vuforia.QCARManagerImpl::get_QCARFrameIndex()
extern "C" int32_t QCARManagerImpl_get_QCARFrameIndex_m3012 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		FrameState_t653 * L_0 = &(__this->___mFrameState_14);
		int32_t L_1 = (L_0->___frameIndex_10);
		return L_1;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_VideoBackgroundTextureSet(System.Boolean)
extern "C" void QCARManagerImpl_set_VideoBackgroundTextureSet_m3013 (QCARManagerImpl_t660 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CVideoBackgroundTextureSetU3Ek__BackingField_18 = L_0;
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::get_VideoBackgroundTextureSet()
extern "C" bool QCARManagerImpl_get_VideoBackgroundTextureSet_m3014 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CVideoBackgroundTextureSetU3Ek__BackingField_18);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::Init()
extern const Il2CppType* FrameState_t653_0_0_0_var;
extern TypeInfo* TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var;
extern TypeInfo* WordDataU5BU5D_t657_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var;
extern TypeInfo* LinkedList_1_t659_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m4408_MethodInfo_var;
extern "C" bool QCARManagerImpl_Init_m3015 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FrameState_t653_0_0_0_var = il2cpp_codegen_type_from_index(1108);
		TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		WordDataU5BU5D_t657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1113);
		LinkedList_1_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1115);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		LinkedList_1__ctor_m4408_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mTrackableResultDataArray_4 = ((TrackableResultDataU5BU5D_t656*)SZArrayNew(TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var, 0));
		__this->___mWordDataArray_5 = ((WordDataU5BU5D_t657*)SZArrayNew(WordDataU5BU5D_t657_il2cpp_TypeInfo_var, 0));
		__this->___mWordResultDataArray_6 = ((WordResultDataU5BU5D_t658*)SZArrayNew(WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var, 0));
		LinkedList_1_t659 * L_0 = (LinkedList_1_t659 *)il2cpp_codegen_object_new (LinkedList_1_t659_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m4408(L_0, /*hidden argument*/LinkedList_1__ctor_m4408_MethodInfo_var);
		__this->___mTrackableFoundQueue_7 = L_0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mImageHeaderData_8 = L_1;
		__this->___mNumImageHeaders_9 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(FrameState_t653_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->___mLastProcessedFrameStatePtr_11 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = (__this->___mLastProcessedFrameStatePtr_11);
		NullCheck(L_5);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(56 /* System.Void Vuforia.IQCARWrapper::InitFrameState(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5, L_6);
		QCARManagerImpl_InitializeTrackableContainer_m3022(__this, 0, /*hidden argument*/NULL);
		__this->___mInitialized_12 = 1;
		return 1;
	}
}
// System.Void Vuforia.QCARManagerImpl::Deinit()
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_Deinit_m3016 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mInitialized_12);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_1 = (__this->___mImageHeaderData_8);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mLastProcessedFrameStatePtr_11);
		NullCheck(L_2);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(57 /* System.Void Vuforia.IQCARWrapper::DeinitFrameState(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2, L_3);
		IntPtr_t L_4 = (__this->___mLastProcessedFrameStatePtr_11);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->___mInitialized_12 = 0;
		__this->___mPaused_13 = 0;
	}

IL_003c:
	{
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::Update(UnityEngine.ScreenOrientation)
extern const Il2CppType* FrameState_t653_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* FrameState_t653_il2cpp_TypeInfo_var;
extern "C" bool QCARManagerImpl_Update_m3017 (QCARManagerImpl_t660 * __this, int32_t ___counterRotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FrameState_t653_0_0_0_var = il2cpp_codegen_type_from_index(1108);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		FrameState_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1108);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	CameraDeviceImpl_t611 * V_1 = {0};
	CameraDeviceImpl_t611 * V_2 = {0};
	{
		V_0 = 1;
		bool L_0 = (__this->___mPaused_13);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		V_0 = 0;
		goto IL_00b5;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsQCAREnabled_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		QCARManagerImpl_UpdateTrackablesEditor_m3025(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0020:
	{
		QCARManagerImpl_UpdateImageContainer_m3027(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_3 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((CameraDeviceImpl_t611 *)Castclass(L_3, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t611 * L_4 = V_1;
		NullCheck(L_4);
		WebCamImpl_t610 * L_5 = CameraDeviceImpl_get_WebCam_m2861(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = WebCamImpl_get_DidUpdateThisFrame_m4078(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		QCARManagerImpl_InjectCameraFrame_m3029(__this, /*hidden argument*/NULL);
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_7 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_8 = (__this->___mImageHeaderData_8);
		int32_t L_9 = (__this->___mNumImageHeaders_9);
		IntPtr_t L_10 = (__this->___mLastProcessedFrameStatePtr_11);
		int32_t L_11 = ___counterRotation;
		NullCheck(L_7);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, IntPtr_t, int32_t >::Invoke(58 /* System.Int32 Vuforia.IQCARWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_7, L_8, L_9, L_10, L_11);
		if (L_12)
		{
			goto IL_006e;
		}
	}
	{
		V_0 = 0;
		goto IL_00b5;
	}

IL_006e:
	{
		int32_t L_13 = (__this->___mDiscardStatesForRendering_17);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_14 = (__this->___mDiscardStatesForRendering_17);
		__this->___mDiscardStatesForRendering_17 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_0085:
	{
		IntPtr_t L_15 = (__this->___mLastProcessedFrameStatePtr_11);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(FrameState_t653_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Object_t * L_17 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		__this->___mFrameState_14 = ((*(FrameState_t653 *)((FrameState_t653 *)UnBox (L_17, FrameState_t653_il2cpp_TypeInfo_var))));
		FrameState_t653 * L_18 = &(__this->___mFrameState_14);
		int32_t L_19 = (L_18->___frameIndex_10);
		if ((((int32_t)L_19) >= ((int32_t)0)))
		{
			goto IL_00b5;
		}
	}
	{
		V_0 = 0;
	}

IL_00b5:
	{
		bool L_20 = V_0;
		if (!L_20)
		{
			goto IL_0116;
		}
	}
	{
		FrameState_t653 * L_21 = &(__this->___mFrameState_14);
		int32_t L_22 = (L_21->___numTrackableResults_8);
		FrameState_t653 * L_23 = &(__this->___mFrameState_14);
		int32_t L_24 = (L_23->___numPropTrackableResults_13);
		QCARManagerImpl_InitializeTrackableContainer_m3022(__this, ((int32_t)((int32_t)L_22+(int32_t)L_24)), /*hidden argument*/NULL);
		QCARManagerImpl_UpdateCameraFrame_m3028(__this, /*hidden argument*/NULL);
		FrameState_t653  L_25 = (__this->___mFrameState_14);
		QCARManagerImpl_UpdateTrackers_m3023(__this, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_26 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_27 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((CameraDeviceImpl_t611 *)Castclass(L_27, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t611 * L_28 = V_2;
		NullCheck(L_28);
		WebCamImpl_t610 * L_29 = CameraDeviceImpl_get_WebCam_m2861(L_28, /*hidden argument*/NULL);
		FrameState_t653 * L_30 = &(__this->___mFrameState_14);
		int32_t L_31 = (L_30->___frameIndex_10);
		NullCheck(L_29);
		WebCamImpl_RenderFrame_m4092(L_29, L_31, /*hidden argument*/NULL);
	}

IL_010f:
	{
		__this->___mVideoBackgroundNeedsRedrawing_16 = 1;
	}

IL_0116:
	{
		bool L_32 = V_0;
		return L_32;
	}
}
// System.Void Vuforia.QCARManagerImpl::StartRendering()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* InternalEyewear_t587_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_StartRendering_m3018 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		InternalEyewear_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1018);
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_1 = (__this->___mDiscardStatesForRendering_17);
		if (L_1)
		{
			goto IL_004b;
		}
	}
	{
		bool L_2 = QCARManagerImpl_get_VideoBackgroundTextureSet_m3014(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		bool L_3 = (__this->___mVideoBackgroundNeedsRedrawing_16);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t587_il2cpp_TypeInfo_var);
		InternalEyewear_t587 * L_4 = InternalEyewear_get_Instance_m2764(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = InternalEyewear_IsSeeThru_m2766(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRendererImpl_t666 * L_6 = QCARRenderer_get_InternalInstance_m3033(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		QCARRendererImpl_UnityRenderEvent_m3044(L_6, ((int32_t)103), /*hidden argument*/NULL);
		__this->___mVideoBackgroundNeedsRedrawing_16 = 0;
		return;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRendererImpl_t666 * L_7 = QCARRenderer_get_InternalInstance_m3033(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARRendererImpl_UnityRenderEvent_m3044(L_7, ((int32_t)102), /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::FinishRendering()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_FinishRendering_m3019 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = (__this->___mDiscardStatesForRendering_17);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRendererImpl_t666 * L_2 = QCARRenderer_get_InternalInstance_m3033(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		QCARRendererImpl_UnityRenderEvent_m3044(L_2, ((int32_t)104), /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::Pause(System.Boolean)
extern TypeInfo* AutoRotationState_t654_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_Pause_m3020 (QCARManagerImpl_t660 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AutoRotationState_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1116);
		s_Il2CppMethodIntialized = true;
	}
	AutoRotationState_t654  V_0 = {0};
	{
		bool L_0 = ___pause;
		if (!L_0)
		{
			goto IL_0064;
		}
	}
	{
		Initobj (AutoRotationState_t654_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = Screen_get_autorotateToLandscapeLeft_m4409(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToLandscapeLeft_3 = L_1;
		bool L_2 = Screen_get_autorotateToLandscapeRight_m4410(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToLandscapeRight_4 = L_2;
		bool L_3 = Screen_get_autorotateToPortrait_m4411(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToPortrait_1 = L_3;
		bool L_4 = Screen_get_autorotateToPortraitUpsideDown_m4412(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToPortraitUpsideDown_2 = L_4;
		(&V_0)->___setOnPause_0 = 1;
		AutoRotationState_t654  L_5 = V_0;
		__this->___mAutoRotationState_15 = L_5;
		Screen_set_autorotateToLandscapeLeft_m4413(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToLandscapeRight_m4414(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToPortrait_m4415(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToPortraitUpsideDown_m4416(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_0064:
	{
		AutoRotationState_t654 * L_6 = &(__this->___mAutoRotationState_15);
		bool L_7 = (L_6->___setOnPause_0);
		if (!L_7)
		{
			goto IL_00b1;
		}
	}
	{
		AutoRotationState_t654 * L_8 = &(__this->___mAutoRotationState_15);
		bool L_9 = (L_8->___autorotateToLandscapeLeft_3);
		Screen_set_autorotateToLandscapeLeft_m4413(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		AutoRotationState_t654 * L_10 = &(__this->___mAutoRotationState_15);
		bool L_11 = (L_10->___autorotateToLandscapeRight_4);
		Screen_set_autorotateToLandscapeRight_m4414(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		AutoRotationState_t654 * L_12 = &(__this->___mAutoRotationState_15);
		bool L_13 = (L_12->___autorotateToPortrait_1);
		Screen_set_autorotateToPortrait_m4415(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		AutoRotationState_t654 * L_14 = &(__this->___mAutoRotationState_15);
		bool L_15 = (L_14->___autorotateToPortraitUpsideDown_2);
		Screen_set_autorotateToPortraitUpsideDown_m4416(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		bool L_16 = ___pause;
		__this->___mPaused_13 = L_16;
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::SetStatesToDiscard()
extern "C" void QCARManagerImpl_SetStatesToDiscard_m3021 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	{
		__this->___mDiscardStatesForRendering_17 = 3;
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::InitializeTrackableContainer(System.Int32)
extern TypeInfo* TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_InitializeTrackableContainer_m3022 (QCARManagerImpl_t660 * __this, int32_t ___numTrackableResults, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableResultDataU5BU5D_t656* L_0 = (__this->___mTrackableResultDataArray_4);
		NullCheck(L_0);
		int32_t L_1 = ___numTrackableResults;
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = ___numTrackableResults;
		__this->___mTrackableResultDataArray_4 = ((TrackableResultDataU5BU5D_t656*)SZArrayNew(TrackableResultDataU5BU5D_t656_il2cpp_TypeInfo_var, L_2));
	}

IL_0017:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateTrackers(Vuforia.QCARManagerImpl/FrameState)
extern const Il2CppType* TrackableResultData_t642_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableResultData_t642_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t715_il2cpp_TypeInfo_var;
extern TypeInfo* U3CU3Ec__DisplayClass3_t655_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t815_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t816_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t717_il2cpp_TypeInfo_var;
extern TypeInfo* WorldCenterTrackableBehaviour_t175_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t565_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m4417_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4418_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4419_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4420_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3004_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m4421_MethodInfo_var;
extern const MethodInfo* Array_Exists_TisTrackableResultData_t642_m4405_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4422_MethodInfo_var;
extern const MethodInfo* LinkedList_1_get_First_m4423_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m4424_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateTrackers_m3023 (QCARManagerImpl_t660 * __this, FrameState_t653  ___frameState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableResultData_t642_0_0_0_var = il2cpp_codegen_type_from_index(1110);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		TrackableResultData_t642_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1110);
		List_1_t715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		U3CU3Ec__DisplayClass3_t655_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1118);
		Predicate_1_t815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1119);
		Enumerator_t816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		StateManagerImpl_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		WorldCenterTrackableBehaviour_t175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		Trackable_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		LinkedList_1_AddLast_m4417_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483994);
		List_1__ctor_m4418_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483995);
		List_1_GetEnumerator_m4419_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483996);
		Enumerator_get_Current_m4420_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483997);
		U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3004_MethodInfo_var = il2cpp_codegen_method_info_from_index(350);
		Predicate_1__ctor_m4421_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483999);
		Array_Exists_TisTrackableResultData_t642_m4405_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484000);
		Enumerator_MoveNext_m4422_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484001);
		LinkedList_1_get_First_m4423_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484002);
		LinkedListNode_1_get_Value_m4424_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484003);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	TrackableResultData_t642  V_2 = {0};
	int32_t V_3 = 0;
	IntPtr_t V_4 = {0};
	TrackableResultData_t642  V_5 = {0};
	TrackableResultData_t642  V_6 = {0};
	List_1_t715 * V_7 = {0};
	Predicate_1_t815 * V_8 = {0};
	U3CU3Ec__DisplayClass3_t655 * V_9 = {0};
	StateManagerImpl_t717 * V_10 = {0};
	int32_t V_11 = 0;
	TrackableResultDataU5BU5D_t656* V_12 = {0};
	int32_t V_13 = 0;
	Enumerator_t816  V_14 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	TrackableResultDataU5BU5D_t656* G_B20_0 = {0};
	TrackableResultDataU5BU5D_t656* G_B19_0 = {0};
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_0004:
	{
		IntPtr_t* L_0 = &((&___frameState)->___trackableDataArray_0);
		int64_t L_1 = IntPtr_ToInt64_m4293(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t642_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_1), ((int64_t)((int64_t)L_1+(int64_t)(((int64_t)((int32_t)((int32_t)L_2*(int32_t)L_4)))))), /*hidden argument*/NULL);
		IntPtr_t L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t642_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_7 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_2 = ((*(TrackableResultData_t642 *)((TrackableResultData_t642 *)UnBox (L_7, TrackableResultData_t642_il2cpp_TypeInfo_var))));
		TrackableResultDataU5BU5D_t656* L_8 = (__this->___mTrackableResultDataArray_4);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		TrackableResultData_t642  L_10 = V_2;
		*((TrackableResultData_t642 *)(TrackableResultData_t642 *)SZArrayLdElema(L_8, L_9)) = L_10;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ((&___frameState)->___numTrackableResults_8);
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0004;
		}
	}
	{
		V_3 = 0;
		goto IL_00c1;
	}

IL_0064:
	{
		IntPtr_t* L_14 = &((&___frameState)->___propTrackableDataArray_4);
		int64_t L_15 = IntPtr_ToInt64_m4293(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t642_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_18 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_4), ((int64_t)((int64_t)L_15+(int64_t)(((int64_t)((int32_t)((int32_t)L_16*(int32_t)L_18)))))), /*hidden argument*/NULL);
		IntPtr_t L_19 = V_4;
		Type_t * L_20 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t642_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_21 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_5 = ((*(TrackableResultData_t642 *)((TrackableResultData_t642 *)UnBox (L_21, TrackableResultData_t642_il2cpp_TypeInfo_var))));
		TrackableResultDataU5BU5D_t656* L_22 = (__this->___mTrackableResultDataArray_4);
		int32_t L_23 = ((&___frameState)->___numTrackableResults_8);
		int32_t L_24 = V_3;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)L_24)));
		TrackableResultData_t642  L_25 = V_5;
		*((TrackableResultData_t642 *)(TrackableResultData_t642 *)SZArrayLdElema(L_22, ((int32_t)((int32_t)L_23+(int32_t)L_24)))) = L_25;
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00c1:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = ((&___frameState)->___numPropTrackableResults_13);
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0064;
		}
	}
	{
		TrackableResultDataU5BU5D_t656* L_29 = (__this->___mTrackableResultDataArray_4);
		V_12 = L_29;
		V_13 = 0;
		goto IL_015f;
	}

IL_00db:
	{
		TrackableResultDataU5BU5D_t656* L_30 = V_12;
		int32_t L_31 = V_13;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		V_6 = (*(TrackableResultData_t642 *)((TrackableResultData_t642 *)(TrackableResultData_t642 *)SZArrayLdElema(L_30, L_31)));
		int32_t L_32 = ((&V_6)->___status_1);
		if ((((int32_t)L_32) == ((int32_t)2)))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_33 = ((&V_6)->___status_1);
		if ((((int32_t)L_33) == ((int32_t)3)))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_34 = ((&V_6)->___status_1);
		if ((!(((uint32_t)L_34) == ((uint32_t)4))))
		{
			goto IL_0132;
		}
	}

IL_0109:
	{
		LinkedList_1_t659 * L_35 = (__this->___mTrackableFoundQueue_7);
		int32_t L_36 = ((&V_6)->___id_2);
		NullCheck(L_35);
		bool L_37 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(15 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(!0) */, L_35, L_36);
		if (L_37)
		{
			goto IL_0159;
		}
	}
	{
		LinkedList_1_t659 * L_38 = (__this->___mTrackableFoundQueue_7);
		int32_t L_39 = ((&V_6)->___id_2);
		NullCheck(L_38);
		LinkedList_1_AddLast_m4417(L_38, L_39, /*hidden argument*/LinkedList_1_AddLast_m4417_MethodInfo_var);
		goto IL_0159;
	}

IL_0132:
	{
		LinkedList_1_t659 * L_40 = (__this->___mTrackableFoundQueue_7);
		int32_t L_41 = ((&V_6)->___id_2);
		NullCheck(L_40);
		bool L_42 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(15 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(!0) */, L_40, L_41);
		if (!L_42)
		{
			goto IL_0159;
		}
	}
	{
		LinkedList_1_t659 * L_43 = (__this->___mTrackableFoundQueue_7);
		int32_t L_44 = ((&V_6)->___id_2);
		NullCheck(L_43);
		VirtFuncInvoker1< bool, int32_t >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(!0) */, L_43, L_44);
	}

IL_0159:
	{
		int32_t L_45 = V_13;
		V_13 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_46 = V_13;
		TrackableResultDataU5BU5D_t656* L_47 = V_12;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)(((Array_t *)L_47)->max_length))))))
		{
			goto IL_00db;
		}
	}
	{
		LinkedList_1_t659 * L_48 = (__this->___mTrackableFoundQueue_7);
		List_1_t715 * L_49 = (List_1_t715 *)il2cpp_codegen_object_new (List_1_t715_il2cpp_TypeInfo_var);
		List_1__ctor_m4418(L_49, L_48, /*hidden argument*/List_1__ctor_m4418_MethodInfo_var);
		V_7 = L_49;
		List_1_t715 * L_50 = V_7;
		NullCheck(L_50);
		Enumerator_t816  L_51 = List_1_GetEnumerator_m4419(L_50, /*hidden argument*/List_1_GetEnumerator_m4419_MethodInfo_var);
		V_14 = L_51;
	}

IL_0180:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01cf;
		}

IL_0182:
		{
			V_8 = (Predicate_1_t815 *)NULL;
			U3CU3Ec__DisplayClass3_t655 * L_52 = (U3CU3Ec__DisplayClass3_t655 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClass3_t655_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass3__ctor_m3003(L_52, /*hidden argument*/NULL);
			V_9 = L_52;
			U3CU3Ec__DisplayClass3_t655 * L_53 = V_9;
			int32_t L_54 = Enumerator_get_Current_m4420((&V_14), /*hidden argument*/Enumerator_get_Current_m4420_MethodInfo_var);
			NullCheck(L_53);
			L_53->___id_0 = L_54;
			TrackableResultDataU5BU5D_t656* L_55 = (__this->___mTrackableResultDataArray_4);
			Predicate_1_t815 * L_56 = V_8;
			G_B19_0 = L_55;
			if (L_56)
			{
				G_B20_0 = L_55;
				goto IL_01b3;
			}
		}

IL_01a4:
		{
			U3CU3Ec__DisplayClass3_t655 * L_57 = V_9;
			IntPtr_t L_58 = { (void*)U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3004_MethodInfo_var };
			Predicate_1_t815 * L_59 = (Predicate_1_t815 *)il2cpp_codegen_object_new (Predicate_1_t815_il2cpp_TypeInfo_var);
			Predicate_1__ctor_m4421(L_59, L_57, L_58, /*hidden argument*/Predicate_1__ctor_m4421_MethodInfo_var);
			V_8 = L_59;
			G_B20_0 = G_B19_0;
		}

IL_01b3:
		{
			Predicate_1_t815 * L_60 = V_8;
			bool L_61 = Array_Exists_TisTrackableResultData_t642_m4405(NULL /*static, unused*/, G_B20_0, L_60, /*hidden argument*/Array_Exists_TisTrackableResultData_t642_m4405_MethodInfo_var);
			if (L_61)
			{
				goto IL_01d8;
			}
		}

IL_01bc:
		{
			LinkedList_1_t659 * L_62 = (__this->___mTrackableFoundQueue_7);
			U3CU3Ec__DisplayClass3_t655 * L_63 = V_9;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___id_0);
			NullCheck(L_62);
			VirtFuncInvoker1< bool, int32_t >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(!0) */, L_62, L_64);
		}

IL_01cf:
		{
			bool L_65 = Enumerator_MoveNext_m4422((&V_14), /*hidden argument*/Enumerator_MoveNext_m4422_MethodInfo_var);
			if (L_65)
			{
				goto IL_0182;
			}
		}

IL_01d8:
		{
			IL2CPP_LEAVE(0x1E8, FINALLY_01da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_01da;
	}

FINALLY_01da:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t816_il2cpp_TypeInfo_var, (&V_14)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t816_il2cpp_TypeInfo_var, (&V_14)));
		IL2CPP_END_FINALLY(474)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(474)
	{
		IL2CPP_JUMP_TBL(0x1E8, IL_01e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_01e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_66 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		StateManager_t713 * L_67 = (StateManager_t713 *)VirtFuncInvoker0< StateManager_t713 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_66);
		V_10 = ((StateManagerImpl_t717 *)Castclass(L_67, StateManagerImpl_t717_il2cpp_TypeInfo_var));
		FrameState_t653  L_68 = ___frameState;
		StateManagerImpl_t717 * L_69 = V_10;
		QCARManagerImpl_UpdateSmartTerrain_m3024(__this, L_68, L_69, /*hidden argument*/NULL);
		V_11 = (-1);
		int32_t L_70 = (__this->___mWorldCenterMode_1);
		if (L_70)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_71 = (__this->___mWorldCenter_2);
		if (!L_71)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_72 = (__this->___mWorldCenter_2);
		NullCheck(L_72);
		Object_t * L_73 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* Vuforia.Trackable Vuforia.WorldCenterTrackableBehaviour::get_Trackable() */, WorldCenterTrackableBehaviour_t175_il2cpp_TypeInfo_var, L_72);
		if (!L_73)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_74 = (__this->___mWorldCenter_2);
		NullCheck(L_74);
		Object_t * L_75 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* Vuforia.Trackable Vuforia.WorldCenterTrackableBehaviour::get_Trackable() */, WorldCenterTrackableBehaviour_t175_il2cpp_TypeInfo_var, L_74);
		NullCheck(L_75);
		int32_t L_76 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t565_il2cpp_TypeInfo_var, L_75);
		V_11 = L_76;
		goto IL_026c;
	}

IL_0236:
	{
		int32_t L_77 = (__this->___mWorldCenterMode_1);
		if ((!(((uint32_t)L_77) == ((uint32_t)1))))
		{
			goto IL_026c;
		}
	}
	{
		StateManagerImpl_t717 * L_78 = V_10;
		LinkedList_1_t659 ** L_79 = &(__this->___mTrackableFoundQueue_7);
		NullCheck(L_78);
		StateManagerImpl_RemoveDisabledTrackablesFromQueue_m4023(L_78, L_79, /*hidden argument*/NULL);
		LinkedList_1_t659 * L_80 = (__this->___mTrackableFoundQueue_7);
		NullCheck(L_80);
		int32_t L_81 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count() */, L_80);
		if ((((int32_t)L_81) <= ((int32_t)0)))
		{
			goto IL_026c;
		}
	}
	{
		LinkedList_1_t659 * L_82 = (__this->___mTrackableFoundQueue_7);
		NullCheck(L_82);
		LinkedListNode_1_t817 * L_83 = LinkedList_1_get_First_m4423(L_82, /*hidden argument*/LinkedList_1_get_First_m4423_MethodInfo_var);
		NullCheck(L_83);
		int32_t L_84 = LinkedListNode_1_get_Value_m4424(L_83, /*hidden argument*/LinkedListNode_1_get_Value_m4424_MethodInfo_var);
		V_11 = L_84;
	}

IL_026c:
	{
		FrameState_t653  L_85 = ___frameState;
		QCARManagerImpl_UpdateWordTrackables_m3026(__this, L_85, /*hidden argument*/NULL);
		StateManagerImpl_t717 * L_86 = V_10;
		Transform_t11 * L_87 = (__this->___mARCameraTransform_3);
		TrackableResultDataU5BU5D_t656* L_88 = (__this->___mTrackableResultDataArray_4);
		int32_t L_89 = V_11;
		NullCheck(L_86);
		StateManagerImpl_UpdateCameraPose_m4024(L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		StateManagerImpl_t717 * L_90 = V_10;
		Transform_t11 * L_91 = (__this->___mARCameraTransform_3);
		TrackableResultDataU5BU5D_t656* L_92 = (__this->___mTrackableResultDataArray_4);
		int32_t L_93 = V_11;
		int32_t L_94 = ((&___frameState)->___frameIndex_10);
		NullCheck(L_90);
		StateManagerImpl_UpdateTrackablePoses_m4025(L_90, L_91, L_92, L_93, L_94, /*hidden argument*/NULL);
		StateManagerImpl_t717 * L_95 = V_10;
		Transform_t11 * L_96 = (__this->___mARCameraTransform_3);
		WordDataU5BU5D_t657* L_97 = (__this->___mWordDataArray_5);
		WordResultDataU5BU5D_t658* L_98 = (__this->___mWordResultDataArray_6);
		NullCheck(L_95);
		StateManagerImpl_UpdateWords_m4027(L_95, L_96, L_97, L_98, /*hidden argument*/NULL);
		StateManagerImpl_t717 * L_99 = V_10;
		int32_t L_100 = ((&___frameState)->___numVirtualButtonResults_9);
		IntPtr_t L_101 = ((&___frameState)->___vbDataArray_1);
		NullCheck(L_99);
		StateManagerImpl_UpdateVirtualButtons_m4026(L_99, L_100, L_101, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateSmartTerrain(Vuforia.QCARManagerImpl/FrameState,Vuforia.StateManagerImpl)
extern const Il2CppType* SmartTerrainRevisionData_t650_0_0_0_var;
extern const Il2CppType* SurfaceData_t651_0_0_0_var;
extern const Il2CppType* PropData_t652_0_0_0_var;
extern TypeInfo* TrackerManager_t728_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainRevisionDataU5BU5D_t768_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceDataU5BU5D_t769_il2cpp_TypeInfo_var;
extern TypeInfo* PropDataU5BU5D_t770_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainRevisionData_t650_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceData_t651_il2cpp_TypeInfo_var;
extern TypeInfo* PropData_t652_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainBuilderImpl_t672_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisReconstructionAbstractBehaviour_t68_m4406_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateSmartTerrain_m3024 (QCARManagerImpl_t660 * __this, FrameState_t653  ___frameState, StateManagerImpl_t717 * ___stateManager, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainRevisionData_t650_0_0_0_var = il2cpp_codegen_type_from_index(1121);
		SurfaceData_t651_0_0_0_var = il2cpp_codegen_type_from_index(1122);
		PropData_t652_0_0_0_var = il2cpp_codegen_type_from_index(1123);
		TrackerManager_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		SmartTerrainRevisionDataU5BU5D_t768_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1124);
		SurfaceDataU5BU5D_t769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1125);
		PropDataU5BU5D_t770_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1126);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		SmartTerrainRevisionData_t650_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		SurfaceData_t651_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1122);
		PropData_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		SmartTerrainBuilderImpl_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1127);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		Enumerable_Any_TisReconstructionAbstractBehaviour_t68_m4406_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484004);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t674 * V_0 = {0};
	SmartTerrainRevisionDataU5BU5D_t768* V_1 = {0};
	SurfaceDataU5BU5D_t769* V_2 = {0};
	PropDataU5BU5D_t770* V_3 = {0};
	int32_t V_4 = 0;
	IntPtr_t V_5 = {0};
	SmartTerrainRevisionData_t650  V_6 = {0};
	int32_t V_7 = 0;
	IntPtr_t V_8 = {0};
	SurfaceData_t651  V_9 = {0};
	int32_t V_10 = 0;
	IntPtr_t V_11 = {0};
	PropData_t652  V_12 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t728_il2cpp_TypeInfo_var);
		TrackerManager_t728 * L_0 = TrackerManager_get_Instance_m4064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t674 * L_1 = (SmartTerrainTracker_t674 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t674 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t674_m4301_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t674 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_018c;
		}
	}
	{
		SmartTerrainTracker_t674 * L_3 = V_0;
		NullCheck(L_3);
		SmartTerrainBuilder_t583 * L_4 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_3);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_4);
		bool L_6 = Enumerable_Any_TisReconstructionAbstractBehaviour_t68_m4406(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_Any_TisReconstructionAbstractBehaviour_t68_m4406_MethodInfo_var);
		if (!L_6)
		{
			goto IL_018c;
		}
	}
	{
		int32_t L_7 = ((&___frameState)->___numSmartTerrainRevisions_14);
		V_1 = ((SmartTerrainRevisionDataU5BU5D_t768*)SZArrayNew(SmartTerrainRevisionDataU5BU5D_t768_il2cpp_TypeInfo_var, L_7));
		int32_t L_8 = ((&___frameState)->___numUpdatedSurfaces_15);
		V_2 = ((SurfaceDataU5BU5D_t769*)SZArrayNew(SurfaceDataU5BU5D_t769_il2cpp_TypeInfo_var, L_8));
		int32_t L_9 = ((&___frameState)->___numUpdatedProps_16);
		V_3 = ((PropDataU5BU5D_t770*)SZArrayNew(PropDataU5BU5D_t770_il2cpp_TypeInfo_var, L_9));
		V_4 = 0;
		goto IL_00a6;
	}

IL_0052:
	{
		IntPtr_t* L_10 = &((&___frameState)->___smartTerrainRevisionsArray_5);
		int64_t L_11 = IntPtr_ToInt64_m4293(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SmartTerrainRevisionData_t650_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_14 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_5), ((int64_t)((int64_t)L_11+(int64_t)(((int64_t)((int32_t)((int32_t)L_12*(int32_t)L_14)))))), /*hidden argument*/NULL);
		IntPtr_t L_15 = V_5;
		Type_t * L_16 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SmartTerrainRevisionData_t650_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_17 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_6 = ((*(SmartTerrainRevisionData_t650 *)((SmartTerrainRevisionData_t650 *)UnBox (L_17, SmartTerrainRevisionData_t650_il2cpp_TypeInfo_var))));
		SmartTerrainRevisionDataU5BU5D_t768* L_18 = V_1;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		SmartTerrainRevisionData_t650  L_20 = V_6;
		*((SmartTerrainRevisionData_t650 *)(SmartTerrainRevisionData_t650 *)SZArrayLdElema(L_18, L_19)) = L_20;
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00a6:
	{
		int32_t L_22 = V_4;
		int32_t L_23 = ((&___frameState)->___numSmartTerrainRevisions_14);
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0052;
		}
	}
	{
		V_7 = 0;
		goto IL_010a;
	}

IL_00b6:
	{
		IntPtr_t* L_24 = &((&___frameState)->___updatedSurfacesArray_6);
		int64_t L_25 = IntPtr_ToInt64_m4293(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SurfaceData_t651_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_28 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_8), ((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int32_t)((int32_t)L_26*(int32_t)L_28)))))), /*hidden argument*/NULL);
		IntPtr_t L_29 = V_8;
		Type_t * L_30 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(SurfaceData_t651_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_31 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_9 = ((*(SurfaceData_t651 *)((SurfaceData_t651 *)UnBox (L_31, SurfaceData_t651_il2cpp_TypeInfo_var))));
		SurfaceDataU5BU5D_t769* L_32 = V_2;
		int32_t L_33 = V_7;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		SurfaceData_t651  L_34 = V_9;
		*((SurfaceData_t651 *)(SurfaceData_t651 *)SZArrayLdElema(L_32, L_33)) = L_34;
		int32_t L_35 = V_7;
		V_7 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_010a:
	{
		int32_t L_36 = V_7;
		int32_t L_37 = ((&___frameState)->___numUpdatedSurfaces_15);
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_00b6;
		}
	}
	{
		V_10 = 0;
		goto IL_016e;
	}

IL_011a:
	{
		IntPtr_t* L_38 = &((&___frameState)->___updatedPropsArray_7);
		int64_t L_39 = IntPtr_ToInt64_m4293(L_38, /*hidden argument*/NULL);
		int32_t L_40 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(PropData_t652_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_42 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_11), ((int64_t)((int64_t)L_39+(int64_t)(((int64_t)((int32_t)((int32_t)L_40*(int32_t)L_42)))))), /*hidden argument*/NULL);
		IntPtr_t L_43 = V_11;
		Type_t * L_44 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(PropData_t652_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_45 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		V_12 = ((*(PropData_t652 *)((PropData_t652 *)UnBox (L_45, PropData_t652_il2cpp_TypeInfo_var))));
		PropDataU5BU5D_t770* L_46 = V_3;
		int32_t L_47 = V_10;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		PropData_t652  L_48 = V_12;
		*((PropData_t652 *)(PropData_t652 *)SZArrayLdElema(L_46, L_47)) = L_48;
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_016e:
	{
		int32_t L_50 = V_10;
		int32_t L_51 = ((&___frameState)->___numUpdatedProps_16);
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_011a;
		}
	}
	{
		SmartTerrainTracker_t674 * L_52 = V_0;
		NullCheck(L_52);
		SmartTerrainBuilder_t583 * L_53 = (SmartTerrainBuilder_t583 *)VirtFuncInvoker0< SmartTerrainBuilder_t583 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_52);
		SmartTerrainRevisionDataU5BU5D_t768* L_54 = V_1;
		SurfaceDataU5BU5D_t769* L_55 = V_2;
		PropDataU5BU5D_t770* L_56 = V_3;
		NullCheck(((SmartTerrainBuilderImpl_t672 *)Castclass(L_53, SmartTerrainBuilderImpl_t672_il2cpp_TypeInfo_var)));
		SmartTerrainBuilderImpl_UpdateSmartTerrainData_m3081(((SmartTerrainBuilderImpl_t672 *)Castclass(L_53, SmartTerrainBuilderImpl_t672_il2cpp_TypeInfo_var)), L_54, L_55, L_56, /*hidden argument*/NULL);
	}

IL_018c:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateTrackablesEditor()
extern const Il2CppType* TrackableBehaviour_t44_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableBehaviourU5BU5D_t818_il2cpp_TypeInfo_var;
extern TypeInfo* WordAbstractBehaviour_t93_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorWordBehaviour_t192_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorTrackableBehaviour_t174_il2cpp_TypeInfo_var;
extern TypeInfo* WordImpl_t685_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_UpdateTrackablesEditor_m3025 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableBehaviour_t44_0_0_0_var = il2cpp_codegen_type_from_index(47);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		TrackableBehaviourU5BU5D_t818_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1128);
		WordAbstractBehaviour_t93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(209);
		IEditorWordBehaviour_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(269);
		IEditorTrackableBehaviour_t174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(168);
		WordImpl_t685_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1129);
		s_Il2CppMethodIntialized = true;
	}
	TrackableBehaviourU5BU5D_t818* V_0 = {0};
	TrackableBehaviour_t44 * V_1 = {0};
	Object_t * V_2 = {0};
	TrackableBehaviourU5BU5D_t818* V_3 = {0};
	int32_t V_4 = 0;
	Object_t * G_B5_0 = {0};
	Object_t * G_B4_0 = {0};
	String_t* G_B6_0 = {0};
	Object_t * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(TrackableBehaviour_t44_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t819* L_1 = Object_FindObjectsOfType_m4425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((TrackableBehaviourU5BU5D_t818*)Castclass(L_1, TrackableBehaviourU5BU5D_t818_il2cpp_TypeInfo_var));
		TrackableBehaviourU5BU5D_t818* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_0082;
	}

IL_001c:
	{
		TrackableBehaviourU5BU5D_t818* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(TrackableBehaviour_t44 **)(TrackableBehaviour_t44 **)SZArrayLdElema(L_3, L_5));
		TrackableBehaviour_t44 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = Behaviour_get_enabled_m497(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007c;
		}
	}
	{
		TrackableBehaviour_t44 * L_8 = V_1;
		if (!((WordAbstractBehaviour_t93 *)IsInst(L_8, WordAbstractBehaviour_t93_il2cpp_TypeInfo_var)))
		{
			goto IL_0075;
		}
	}
	{
		TrackableBehaviour_t44 * L_9 = V_1;
		V_2 = ((Object_t *)Castclass(L_9, IEditorWordBehaviour_t192_il2cpp_TypeInfo_var));
		Object_t * L_10 = V_2;
		Object_t * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.IEditorWordBehaviour::get_IsSpecificWordMode() */, IEditorWordBehaviour_t192_il2cpp_TypeInfo_var, L_11);
		G_B4_0 = L_10;
		if (L_12)
		{
			G_B5_0 = L_10;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = (String_t*) &_stringLiteral205;
		G_B6_1 = G_B4_0;
		goto IL_004e;
	}

IL_0048:
	{
		Object_t * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.IEditorWordBehaviour::get_SpecificWord() */, IEditorWordBehaviour_t192_il2cpp_TypeInfo_var, L_13);
		G_B6_0 = L_14;
		G_B6_1 = G_B5_0;
	}

IL_004e:
	{
		NullCheck(G_B6_1);
		InterfaceFuncInvoker1< bool, String_t* >::Invoke(2 /* System.Boolean Vuforia.IEditorTrackableBehaviour::SetNameForTrackable(System.String) */, IEditorTrackableBehaviour_t174_il2cpp_TypeInfo_var, G_B6_1, G_B6_0);
		Object_t * L_15 = V_2;
		Object_t * L_16 = V_2;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.IEditorTrackableBehaviour::get_TrackableName() */, IEditorTrackableBehaviour_t174_il2cpp_TypeInfo_var, L_16);
		Vector2_t10  L_18 = {0};
		Vector2__ctor_m264(&L_18, (500.0f), (100.0f), /*hidden argument*/NULL);
		WordImpl_t685 * L_19 = (WordImpl_t685 *)il2cpp_codegen_object_new (WordImpl_t685_il2cpp_TypeInfo_var);
		WordImpl__ctor_m3111(L_19, 0, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker1< Object_t * >::Invoke(6 /* System.Void Vuforia.IEditorWordBehaviour::InitializeWord(Vuforia.Word) */, IEditorWordBehaviour_t192_il2cpp_TypeInfo_var, L_15, L_19);
	}

IL_0075:
	{
		TrackableBehaviour_t44 * L_20 = V_1;
		NullCheck(L_20);
		VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status) */, L_20, 3);
	}

IL_007c:
	{
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_22 = V_4;
		TrackableBehaviourU5BU5D_t818* L_23 = V_3;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)(((Array_t *)L_23)->max_length))))))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateWordTrackables(Vuforia.QCARManagerImpl/FrameState)
extern const Il2CppType* WordData_t647_0_0_0_var;
extern const Il2CppType* WordResultData_t646_0_0_0_var;
extern TypeInfo* WordDataU5BU5D_t657_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* WordData_t647_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultData_t646_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_UpdateWordTrackables_m3026 (QCARManagerImpl_t660 * __this, FrameState_t653  ___frameState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WordData_t647_0_0_0_var = il2cpp_codegen_type_from_index(1112);
		WordResultData_t646_0_0_0_var = il2cpp_codegen_type_from_index(1114);
		WordDataU5BU5D_t657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		WordData_t647_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1112);
		WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1113);
		WordResultData_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1114);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	{
		int32_t L_0 = ((&___frameState)->___numNewWords_12);
		__this->___mWordDataArray_5 = ((WordDataU5BU5D_t657*)SZArrayNew(WordDataU5BU5D_t657_il2cpp_TypeInfo_var, L_0));
		V_0 = 0;
		goto IL_0066;
	}

IL_0016:
	{
		IntPtr_t* L_1 = &((&___frameState)->___newWordDataArray_3);
		int64_t L_2 = IntPtr_ToInt64_m4293(L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(WordData_t647_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_1), ((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int32_t)((int32_t)L_3*(int32_t)L_5)))))), /*hidden argument*/NULL);
		WordDataU5BU5D_t657* L_6 = (__this->___mWordDataArray_5);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		IntPtr_t L_8 = V_1;
		Type_t * L_9 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(WordData_t647_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_10 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		*((WordData_t647 *)(WordData_t647 *)SZArrayLdElema(L_6, L_7)) = ((*(WordData_t647 *)((WordData_t647 *)UnBox (L_10, WordData_t647_il2cpp_TypeInfo_var))));
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ((&___frameState)->___numNewWords_12);
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_14 = ((&___frameState)->___numWordResults_11);
		__this->___mWordResultDataArray_6 = ((WordResultDataU5BU5D_t658*)SZArrayNew(WordResultDataU5BU5D_t658_il2cpp_TypeInfo_var, L_14));
		V_2 = 0;
		goto IL_00d6;
	}

IL_0086:
	{
		IntPtr_t* L_15 = &((&___frameState)->___wordResultArray_2);
		int64_t L_16 = IntPtr_ToInt64_m4293(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(WordResultData_t646_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_19 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_16+(int64_t)(((int64_t)((int32_t)((int32_t)L_17*(int32_t)L_19)))))), /*hidden argument*/NULL);
		WordResultDataU5BU5D_t658* L_20 = (__this->___mWordResultDataArray_6);
		int32_t L_21 = V_2;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		IntPtr_t L_22 = V_3;
		Type_t * L_23 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(WordResultData_t646_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_24 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		*((WordResultData_t646 *)(WordResultData_t646 *)SZArrayLdElema(L_20, L_21)) = ((*(WordResultData_t646 *)((WordResultData_t646 *)UnBox (L_24, WordResultData_t646_il2cpp_TypeInfo_var))));
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00d6:
	{
		int32_t L_26 = V_2;
		int32_t L_27 = ((&___frameState)->___numWordResults_11);
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0086;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateImageContainer()
extern const Il2CppType* ImageHeaderData_t648_0_0_0_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t618_il2cpp_TypeInfo_var;
extern TypeInfo* ImageHeaderData_t648_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t820_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4426_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4427_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4428_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4429_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateImageContainer_m3027 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageHeaderData_t648_0_0_0_var = il2cpp_codegen_type_from_index(1130);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		ImageImpl_t618_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		ImageHeaderData_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1130);
		Enumerator_t820_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4426_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484005);
		ValueCollection_GetEnumerator_m4427_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484006);
		Enumerator_get_Current_m4428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484007);
		Enumerator_MoveNext_m4429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484008);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t611 * V_0 = {0};
	int32_t V_1 = 0;
	ImageImpl_t618 * V_2 = {0};
	IntPtr_t V_3 = {0};
	ImageHeaderData_t648  V_4 = {0};
	Enumerator_t820  V_5 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_0 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t611 *)Castclass(L_0, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		int32_t L_1 = (__this->___mNumImageHeaders_9);
		CameraDeviceImpl_t611 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_t608 * L_3 = CameraDeviceImpl_GetAllImages_m2876(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_3);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_4))))
		{
			goto IL_003e;
		}
	}
	{
		CameraDeviceImpl_t611 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_t608 * L_6 = CameraDeviceImpl_GetAllImages_m2876(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_6);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_007b;
		}
	}
	{
		IntPtr_t L_8 = (__this->___mImageHeaderData_8);
		IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_10 = IntPtr_op_Equality_m4356(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007b;
		}
	}

IL_003e:
	{
		CameraDeviceImpl_t611 * L_11 = V_0;
		NullCheck(L_11);
		Dictionary_2_t608 * L_12 = CameraDeviceImpl_GetAllImages_m2876(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_12);
		__this->___mNumImageHeaders_9 = L_13;
		IntPtr_t L_14 = (__this->___mImageHeaderData_8);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t648_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_16 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_17 = (__this->___mNumImageHeaders_9);
		IntPtr_t L_18 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_16*(int32_t)L_17)), /*hidden argument*/NULL);
		__this->___mImageHeaderData_8 = L_18;
	}

IL_007b:
	{
		V_1 = 0;
		CameraDeviceImpl_t611 * L_19 = V_0;
		NullCheck(L_19);
		Dictionary_2_t608 * L_20 = CameraDeviceImpl_GetAllImages_m2876(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		ValueCollection_t821 * L_21 = Dictionary_2_get_Values_m4426(L_20, /*hidden argument*/Dictionary_2_get_Values_m4426_MethodInfo_var);
		NullCheck(L_21);
		Enumerator_t820  L_22 = ValueCollection_GetEnumerator_m4427(L_21, /*hidden argument*/ValueCollection_GetEnumerator_m4427_MethodInfo_var);
		V_5 = L_22;
	}

IL_008f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014b;
		}

IL_0094:
		{
			Image_t615 * L_23 = Enumerator_get_Current_m4428((&V_5), /*hidden argument*/Enumerator_get_Current_m4428_MethodInfo_var);
			V_2 = ((ImageImpl_t618 *)Castclass(L_23, ImageImpl_t618_il2cpp_TypeInfo_var));
			IntPtr_t* L_24 = &(__this->___mImageHeaderData_8);
			int64_t L_25 = IntPtr_ToInt64_m4293(L_24, /*hidden argument*/NULL);
			int32_t L_26 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_27 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t648_0_0_0_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
			int32_t L_28 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int32_t)((int32_t)L_26*(int32_t)L_28)))))), /*hidden argument*/NULL);
			Initobj (ImageHeaderData_t648_il2cpp_TypeInfo_var, (&V_4));
			ImageImpl_t618 * L_29 = V_2;
			NullCheck(L_29);
			int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Vuforia.Image::get_Width() */, L_29);
			(&V_4)->___width_1 = L_30;
			ImageImpl_t618 * L_31 = V_2;
			NullCheck(L_31);
			int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Vuforia.Image::get_Height() */, L_31);
			(&V_4)->___height_2 = L_32;
			ImageImpl_t618 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Vuforia.Image::get_Stride() */, L_33);
			(&V_4)->___stride_3 = L_34;
			ImageImpl_t618 * L_35 = V_2;
			NullCheck(L_35);
			int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_35);
			(&V_4)->___bufferWidth_4 = L_36;
			ImageImpl_t618 * L_37 = V_2;
			NullCheck(L_37);
			int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_37);
			(&V_4)->___bufferHeight_5 = L_38;
			ImageImpl_t618 * L_39 = V_2;
			NullCheck(L_39);
			int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_39);
			(&V_4)->___format_6 = L_40;
			(&V_4)->___reallocate_7 = 0;
			(&V_4)->___updated_8 = 0;
			ImageImpl_t618 * L_41 = V_2;
			NullCheck(L_41);
			IntPtr_t L_42 = ImageImpl_get_UnmanagedData_m2920(L_41, /*hidden argument*/NULL);
			(&V_4)->___data_0 = L_42;
			ImageHeaderData_t648  L_43 = V_4;
			ImageHeaderData_t648  L_44 = L_43;
			Object_t * L_45 = Box(ImageHeaderData_t648_il2cpp_TypeInfo_var, &L_44);
			IntPtr_t L_46 = V_3;
			Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_45, L_46, 0, /*hidden argument*/NULL);
			int32_t L_47 = V_1;
			V_1 = ((int32_t)((int32_t)L_47+(int32_t)1));
		}

IL_014b:
		{
			bool L_48 = Enumerator_MoveNext_m4429((&V_5), /*hidden argument*/Enumerator_MoveNext_m4429_MethodInfo_var);
			if (L_48)
			{
				goto IL_0094;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x167, FINALLY_0159);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0159;
	}

FINALLY_0159:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(345)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(345)
	{
		IL2CPP_JUMP_TBL(0x167, IL_0167)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0167:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateCameraFrame()
extern const Il2CppType* ImageHeaderData_t648_0_0_0_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t618_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* ImageHeaderData_t648_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t616_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t820_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4426_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4427_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4428_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4429_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateCameraFrame_m3028 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageHeaderData_t648_0_0_0_var = il2cpp_codegen_type_from_index(1130);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		ImageImpl_t618_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		ImageHeaderData_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1130);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		ByteU5BU5D_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1132);
		Enumerator_t820_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		IDisposable_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Dictionary_2_get_Values_m4426_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484005);
		ValueCollection_GetEnumerator_m4427_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484006);
		Enumerator_get_Current_m4428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484007);
		Enumerator_MoveNext_m4429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484008);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CameraDeviceImpl_t611 * V_1 = {0};
	ImageImpl_t618 * V_2 = {0};
	IntPtr_t V_3 = {0};
	ImageHeaderData_t648  V_4 = {0};
	Enumerator_t820  V_5 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_0 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((CameraDeviceImpl_t611 *)Castclass(L_0, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t611 * L_1 = V_1;
		NullCheck(L_1);
		Dictionary_2_t608 * L_2 = CameraDeviceImpl_GetAllImages_m2876(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ValueCollection_t821 * L_3 = Dictionary_2_get_Values_m4426(L_2, /*hidden argument*/Dictionary_2_get_Values_m4426_MethodInfo_var);
		NullCheck(L_3);
		Enumerator_t820  L_4 = ValueCollection_GetEnumerator_m4427(L_3, /*hidden argument*/ValueCollection_GetEnumerator_m4427_MethodInfo_var);
		V_5 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0134;
		}

IL_0024:
		{
			Image_t615 * L_5 = Enumerator_get_Current_m4428((&V_5), /*hidden argument*/Enumerator_get_Current_m4428_MethodInfo_var);
			V_2 = ((ImageImpl_t618 *)Castclass(L_5, ImageImpl_t618_il2cpp_TypeInfo_var));
			IntPtr_t* L_6 = &(__this->___mImageHeaderData_8);
			int64_t L_7 = IntPtr_ToInt64_m4293(L_6, /*hidden argument*/NULL);
			int32_t L_8 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_9 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t648_0_0_0_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
			int32_t L_10 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			IntPtr__ctor_m4294((&V_3), ((int64_t)((int64_t)L_7+(int64_t)(((int64_t)((int32_t)((int32_t)L_8*(int32_t)L_10)))))), /*hidden argument*/NULL);
			IntPtr_t L_11 = V_3;
			Type_t * L_12 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t648_0_0_0_var), /*hidden argument*/NULL);
			Object_t * L_13 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
			V_4 = ((*(ImageHeaderData_t648 *)((ImageHeaderData_t648 *)UnBox (L_13, ImageHeaderData_t648_il2cpp_TypeInfo_var))));
			ImageImpl_t618 * L_14 = V_2;
			int32_t L_15 = ((&V_4)->___width_1);
			NullCheck(L_14);
			VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void Vuforia.Image::set_Width(System.Int32) */, L_14, L_15);
			ImageImpl_t618 * L_16 = V_2;
			int32_t L_17 = ((&V_4)->___height_2);
			NullCheck(L_16);
			VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void Vuforia.Image::set_Height(System.Int32) */, L_16, L_17);
			ImageImpl_t618 * L_18 = V_2;
			int32_t L_19 = ((&V_4)->___stride_3);
			NullCheck(L_18);
			VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void Vuforia.Image::set_Stride(System.Int32) */, L_18, L_19);
			ImageImpl_t618 * L_20 = V_2;
			int32_t L_21 = ((&V_4)->___bufferWidth_4);
			NullCheck(L_20);
			VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void Vuforia.Image::set_BufferWidth(System.Int32) */, L_20, L_21);
			ImageImpl_t618 * L_22 = V_2;
			int32_t L_23 = ((&V_4)->___bufferHeight_5);
			NullCheck(L_22);
			VirtActionInvoker1< int32_t >::Invoke(13 /* System.Void Vuforia.Image::set_BufferHeight(System.Int32) */, L_22, L_23);
			ImageImpl_t618 * L_24 = V_2;
			int32_t L_25 = ((&V_4)->___format_6);
			NullCheck(L_24);
			VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT) */, L_24, L_25);
			int32_t L_26 = ((&V_4)->___reallocate_7);
			if ((!(((uint32_t)L_26) == ((uint32_t)1))))
			{
				goto IL_0120;
			}
		}

IL_00c5:
		{
			ImageImpl_t618 * L_27 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
			Object_t * L_28 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
			ImageImpl_t618 * L_29 = V_2;
			NullCheck(L_29);
			int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_29);
			ImageImpl_t618 * L_31 = V_2;
			NullCheck(L_31);
			int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_31);
			ImageImpl_t618 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_33);
			NullCheck(L_28);
			int32_t L_35 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(60 /* System.Int32 Vuforia.IQCARWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_28, L_30, L_32, L_34);
			NullCheck(L_27);
			VirtActionInvoker1< ByteU5BU5D_t616* >::Invoke(17 /* System.Void Vuforia.Image::set_Pixels(System.Byte[]) */, L_27, ((ByteU5BU5D_t616*)SZArrayNew(ByteU5BU5D_t616_il2cpp_TypeInfo_var, L_35)));
			ImageImpl_t618 * L_36 = V_2;
			NullCheck(L_36);
			IntPtr_t L_37 = ImageImpl_get_UnmanagedData_m2920(L_36, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
			Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			ImageImpl_t618 * L_38 = V_2;
			Object_t * L_39 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
			ImageImpl_t618 * L_40 = V_2;
			NullCheck(L_40);
			int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_40);
			ImageImpl_t618 * L_42 = V_2;
			NullCheck(L_42);
			int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_42);
			ImageImpl_t618 * L_44 = V_2;
			NullCheck(L_44);
			int32_t L_45 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_44);
			NullCheck(L_39);
			int32_t L_46 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(60 /* System.Int32 Vuforia.IQCARWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_39, L_41, L_43, L_45);
			IntPtr_t L_47 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
			NullCheck(L_38);
			ImageImpl_set_UnmanagedData_m2921(L_38, L_47, /*hidden argument*/NULL);
			goto IL_0130;
		}

IL_0120:
		{
			int32_t L_48 = ((&V_4)->___updated_8);
			if ((!(((uint32_t)L_48) == ((uint32_t)1))))
			{
				goto IL_0130;
			}
		}

IL_012a:
		{
			ImageImpl_t618 * L_49 = V_2;
			NullCheck(L_49);
			ImageImpl_CopyPixelsFromUnmanagedBuffer_m2926(L_49, /*hidden argument*/NULL);
		}

IL_0130:
		{
			int32_t L_50 = V_0;
			V_0 = ((int32_t)((int32_t)L_50+(int32_t)1));
		}

IL_0134:
		{
			bool L_51 = Enumerator_MoveNext_m4429((&V_5), /*hidden argument*/Enumerator_MoveNext_m4429_MethodInfo_var);
			if (L_51)
			{
				goto IL_0024;
			}
		}

IL_0140:
		{
			IL2CPP_LEAVE(0x150, FINALLY_0142);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_0142;
	}

FINALLY_0142:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t144_il2cpp_TypeInfo_var, Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(322)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(322)
	{
		IL2CPP_JUMP_TBL(0x150, IL_0150)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0150:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::InjectCameraFrame()
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_InjectCameraFrame_m3029 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t611 * V_0 = {0};
	Color32U5BU5D_t617* V_1 = {0};
	GCHandle_t805  V_2 = {0};
	IntPtr_t V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	int32_t G_B2_2 = 0;
	int32_t G_B2_3 = 0;
	int32_t G_B2_4 = 0;
	IntPtr_t G_B2_5 = {0};
	Object_t * G_B2_6 = {0};
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	int32_t G_B1_2 = 0;
	int32_t G_B1_3 = 0;
	int32_t G_B1_4 = 0;
	IntPtr_t G_B1_5 = {0};
	Object_t * G_B1_6 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B3_2 = 0;
	int32_t G_B3_3 = 0;
	int32_t G_B3_4 = 0;
	int32_t G_B3_5 = 0;
	IntPtr_t G_B3_6 = {0};
	Object_t * G_B3_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_0 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t611 *)Castclass(L_0, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t611 * L_1 = V_0;
		NullCheck(L_1);
		WebCamImpl_t610 * L_2 = CameraDeviceImpl_get_WebCam_m2861(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Color32U5BU5D_t617* L_3 = WebCamImpl_GetPixels32AndBufferFrame_m4091(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Color32U5BU5D_t617* L_4 = V_1;
		GCHandle_t805  L_5 = GCHandle_Alloc_m4372(NULL /*static, unused*/, (Object_t *)(Object_t *)L_4, 3, /*hidden argument*/NULL);
		V_2 = L_5;
		IntPtr_t L_6 = GCHandle_AddrOfPinnedObject_m4373((&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		CameraDeviceImpl_t611 * L_7 = V_0;
		NullCheck(L_7);
		WebCamImpl_t610 * L_8 = CameraDeviceImpl_get_WebCam_m2861(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = WebCamImpl_get_ActualWidth_m4080(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		CameraDeviceImpl_t611 * L_10 = V_0;
		NullCheck(L_10);
		WebCamImpl_t610 * L_11 = CameraDeviceImpl_get_WebCam_m2861(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = WebCamImpl_get_ActualHeight_m4081(L_11, /*hidden argument*/NULL);
		V_5 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_13 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_14 = V_3;
		int32_t L_15 = V_4;
		int32_t L_16 = V_5;
		int32_t L_17 = V_4;
		int32_t L_18 = (__this->___mInjectedFrameIdx_10);
		CameraDeviceImpl_t611 * L_19 = V_0;
		NullCheck(L_19);
		WebCamImpl_t610 * L_20 = CameraDeviceImpl_get_WebCam_m2861(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		bool L_21 = WebCamImpl_get_FlipHorizontally_m4084(L_20, /*hidden argument*/NULL);
		G_B1_0 = L_18;
		G_B1_1 = ((int32_t)((int32_t)4*(int32_t)L_17));
		G_B1_2 = ((int32_t)16);
		G_B1_3 = L_16;
		G_B1_4 = L_15;
		G_B1_5 = L_14;
		G_B1_6 = L_13;
		if (L_21)
		{
			G_B2_0 = L_18;
			G_B2_1 = ((int32_t)((int32_t)4*(int32_t)L_17));
			G_B2_2 = ((int32_t)16);
			G_B2_3 = L_16;
			G_B2_4 = L_15;
			G_B2_5 = L_14;
			G_B2_6 = L_13;
			goto IL_0067;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		G_B3_6 = G_B1_5;
		G_B3_7 = G_B1_6;
		goto IL_0068;
	}

IL_0067:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
		G_B3_6 = G_B2_5;
		G_B3_7 = G_B2_6;
	}

IL_0068:
	{
		NullCheck(G_B3_7);
		InterfaceActionInvoker7< IntPtr_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t >::Invoke(61 /* System.Void Vuforia.IQCARWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, G_B3_7, G_B3_6, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0);
		int32_t L_22 = (__this->___mInjectedFrameIdx_10);
		__this->___mInjectedFrameIdx_10 = ((int32_t)((int32_t)L_22+(int32_t)1));
		IntPtr_t L_23 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		V_3 = L_23;
		GCHandle_Free_m4375((&V_2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::.ctor()
extern TypeInfo* LinkedList_1_t659_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m4408_MethodInfo_var;
extern "C" void QCARManagerImpl__ctor_m3030 (QCARManagerImpl_t660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1115);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(545);
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		LinkedList_1__ctor_m4408_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t659 * L_0 = (LinkedList_1_t659 *)il2cpp_codegen_object_new (LinkedList_1_t659_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m4408(L_0, /*hidden argument*/LinkedList_1__ctor_m4408_MethodInfo_var);
		__this->___mTrackableFoundQueue_7 = L_0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mImageHeaderData_8 = L_1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mLastProcessedFrameStatePtr_11 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager__ctor_m3001(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoBMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0MethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARRenderer/VideoBGCfgData
void VideoBGCfgData_t662_marshal(const VideoBGCfgData_t662& unmarshaled, VideoBGCfgData_t662_marshaled& marshaled)
{
	marshaled.___enabled_0 = unmarshaled.___enabled_0;
	marshaled.___synchronous_1 = unmarshaled.___synchronous_1;
	marshaled.___position_2 = unmarshaled.___position_2;
	marshaled.___size_3 = unmarshaled.___size_3;
	marshaled.___reflection_4 = unmarshaled.___reflection_4;
	marshaled.___unused_5 = unmarshaled.___unused_5;
}
void VideoBGCfgData_t662_marshal_back(const VideoBGCfgData_t662_marshaled& marshaled, VideoBGCfgData_t662& unmarshaled)
{
	unmarshaled.___enabled_0 = marshaled.___enabled_0;
	unmarshaled.___synchronous_1 = marshaled.___synchronous_1;
	unmarshaled.___position_2 = marshaled.___position_2;
	unmarshaled.___size_3 = marshaled.___size_3;
	unmarshaled.___reflection_4 = marshaled.___reflection_4;
	unmarshaled.___unused_5 = marshaled.___unused_5;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARRenderer/VideoBGCfgData
void VideoBGCfgData_t662_marshal_cleanup(VideoBGCfgData_t662_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2IMethodDeclarations.h"



// System.Void Vuforia.QCARRenderer/Vec2I::.ctor(System.Int32,System.Int32)
extern "C" void Vec2I__ctor_m3031 (Vec2I_t663 * __this, int32_t ___v1, int32_t ___v2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___v1;
		__this->___x_0 = L_0;
		int32_t L_1 = ___v2;
		__this->___y_1 = L_1;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoTMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// Vuforia.QCARRenderer Vuforia.QCARRenderer::get_Instance()
extern const Il2CppType* QCARRenderer_t664_0_0_0_var;
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRendererImpl_t666_il2cpp_TypeInfo_var;
extern "C" QCARRenderer_t664 * QCARRenderer_get_Instance_m3032 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t664_0_0_0_var = il2cpp_codegen_type_from_index(1016);
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		QCARRendererImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1134);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t140 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t140 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_0 = ((QCARRenderer_t664_StaticFields*)QCARRenderer_t664_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(QCARRenderer_t664_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4313(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
			QCARRenderer_t664 * L_3 = ((QCARRenderer_t664_StaticFields*)QCARRenderer_t664_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			QCARRendererImpl_t666 * L_4 = (QCARRendererImpl_t666 *)il2cpp_codegen_object_new (QCARRendererImpl_t666_il2cpp_TypeInfo_var);
			QCARRendererImpl__ctor_m3045(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
			((QCARRenderer_t664_StaticFields*)QCARRenderer_t664_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t140 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4314(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t140 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_6 = ((QCARRenderer_t664_StaticFields*)QCARRenderer_t664_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_6;
	}
}
// Vuforia.QCARRendererImpl Vuforia.QCARRenderer::get_InternalInstance()
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRendererImpl_t666_il2cpp_TypeInfo_var;
extern "C" QCARRendererImpl_t666 * QCARRenderer_get_InternalInstance_m3033 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		QCARRendererImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1134);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer_t664 * L_0 = QCARRenderer_get_Instance_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((QCARRendererImpl_t666 *)Castclass(L_0, QCARRendererImpl_t666_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Texture2D Vuforia.QCARRenderer::get_VideoBackgroundTexture()
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRenderer::GetVideoBackgroundConfig()
// System.Void Vuforia.QCARRenderer::ClearVideoBackgroundConfig()
// System.Void Vuforia.QCARRenderer::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
// System.Boolean Vuforia.QCARRenderer::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
// System.Boolean Vuforia.QCARRenderer::IsVideoBackgroundInfoAvailable()
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRenderer::GetVideoTextureInfo()
// System.Void Vuforia.QCARRenderer::Pause(System.Boolean)
// System.Void Vuforia.QCARRenderer::.ctor()
extern "C" void QCARRenderer__ctor_m3034 (QCARRenderer_t664 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRenderer::.cctor()
extern "C" void QCARRenderer__cctor_m3035 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_ReMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// UnityEngine.Texture2D Vuforia.QCARRendererImpl::get_VideoBackgroundTexture()
extern "C" Texture2D_t270 * QCARRendererImpl_get_VideoBackgroundTexture_m3036 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	{
		Texture2D_t270 * L_0 = (__this->___mVideoBackgroundTexture_3);
		return L_0;
	}
}
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRendererImpl::GetVideoBackgroundConfig()
extern const Il2CppType* VideoBGCfgData_t662_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* VideoBGCfgData_t662_il2cpp_TypeInfo_var;
extern "C" VideoBGCfgData_t662  QCARRendererImpl_GetVideoBackgroundConfig_m3037 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoBGCfgData_t662_0_0_0_var = il2cpp_codegen_type_from_index(1135);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		VideoBGCfgData_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1135);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	VideoBGCfgData_t662  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		VideoBGCfgData_t662  L_1 = (__this->___mVideoBGConfig_1);
		return L_1;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t662_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = V_0;
		NullCheck(L_5);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(63 /* System.Void Vuforia.IQCARWrapper::RendererGetVideoBackgroundCfg(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5, L_6);
		IntPtr_t L_7 = V_0;
		Type_t * L_8 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t662_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_9 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = ((*(VideoBGCfgData_t662 *)((VideoBGCfgData_t662 *)UnBox (L_9, VideoBGCfgData_t662_il2cpp_TypeInfo_var))));
		IntPtr_t L_10 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		VideoBGCfgData_t662  L_11 = V_1;
		return L_11;
	}
}
// System.Void Vuforia.QCARRendererImpl::ClearVideoBackgroundConfig()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_ClearVideoBackgroundConfig_m3038 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		__this->___mVideoBGConfigSet_2 = 0;
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
extern const Il2CppType* VideoBGCfgData_t662_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* VideoBGCfgData_t662_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_SetVideoBackgroundConfig_m3039 (QCARRendererImpl_t666 * __this, VideoBGCfgData_t662  ___config, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoBGCfgData_t662_0_0_0_var = il2cpp_codegen_type_from_index(1135);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		VideoBGCfgData_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1135);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		VideoBGCfgData_t662  L_1 = ___config;
		__this->___mVideoBGConfig_1 = L_1;
		__this->___mVideoBGConfigSet_2 = 1;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t662_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		VideoBGCfgData_t662  L_5 = ___config;
		VideoBGCfgData_t662  L_6 = L_5;
		Object_t * L_7 = Box(VideoBGCfgData_t662_il2cpp_TypeInfo_var, &L_6);
		IntPtr_t L_8 = V_0;
		Marshal_StructureToPtr_m4295(NULL /*static, unused*/, L_7, L_8, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_9 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_0;
		NullCheck(L_9);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(62 /* System.Void Vuforia.IQCARWrapper::RendererSetVideoBackgroundCfg(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_9, L_10);
		IntPtr_t L_11 = V_0;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.QCARRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t660_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool QCARRendererImpl_SetVideoBackgroundTexture_m3040 (QCARRendererImpl_t666 * __this, Texture2D_t270 * ___texture, int32_t ___nativeTextureID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		QCARManagerImpl_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t270 * L_0 = ___texture;
		__this->___mVideoBackgroundTexture_3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		Texture2D_t270 * L_2 = ___texture;
		bool L_3 = Object_op_Inequality_m386(NULL /*static, unused*/, L_2, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_4 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t660 *)Castclass(L_4, QCARManagerImpl_t660_il2cpp_TypeInfo_var)));
		QCARManagerImpl_set_VideoBackgroundTextureSet_m3013(((QCARManagerImpl_t660 *)Castclass(L_4, QCARManagerImpl_t660_il2cpp_TypeInfo_var)), 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = ___nativeTextureID;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(65 /* System.Int32 Vuforia.IQCARWrapper::RendererSetVideoBackgroundTextureID(System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_5, L_6);
		return ((((int32_t)((((int32_t)L_7) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_8 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t660 *)Castclass(L_8, QCARManagerImpl_t660_il2cpp_TypeInfo_var)));
		QCARManagerImpl_set_VideoBackgroundTextureSet_m3013(((QCARManagerImpl_t660 *)Castclass(L_8, QCARManagerImpl_t660_il2cpp_TypeInfo_var)), 0, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARRendererImpl::IsVideoBackgroundInfoAvailable()
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool QCARRendererImpl_IsVideoBackgroundInfoAvailable_m3041 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		bool L_1 = (__this->___mVideoBGConfigSet_2);
		return L_1;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(66 /* System.Int32 Vuforia.IQCARWrapper::RendererIsVideoBackgroundTextureInfoAvailable() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_2);
		return ((((int32_t)((((int32_t)L_3) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRendererImpl::GetVideoTextureInfo()
extern const Il2CppType* VideoTextureInfo_t561_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern TypeInfo* VideoTextureInfo_t561_il2cpp_TypeInfo_var;
extern "C" VideoTextureInfo_t561  QCARRendererImpl_GetVideoTextureInfo_m3042 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoTextureInfo_t561_0_0_0_var = il2cpp_codegen_type_from_index(1136);
		QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		VideoTextureInfo_t561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1136);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t611 * V_0 = {0};
	IntPtr_t V_1 = {0};
	VideoTextureInfo_t561  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t145_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m460(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_1 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t611 *)Castclass(L_1, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t611 * L_2 = V_0;
		NullCheck(L_2);
		WebCamImpl_t610 * L_3 = CameraDeviceImpl_get_WebCam_m2861(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		VideoTextureInfo_t561  L_4 = WebCamImpl_GetVideoTextureInfo_m4094(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoTextureInfo_t561_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_6 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_7 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_9 = V_1;
		NullCheck(L_8);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(64 /* System.Void Vuforia.IQCARWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_8, L_9);
		IntPtr_t L_10 = V_1;
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(VideoTextureInfo_t561_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_12 = Marshal_PtrToStructure_m4332(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = ((*(VideoTextureInfo_t561 *)((VideoTextureInfo_t561 *)UnBox (L_12, VideoTextureInfo_t561_il2cpp_TypeInfo_var))));
		IntPtr_t L_13 = V_1;
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		VideoTextureInfo_t561  L_14 = V_2;
		return L_14;
	}
}
// System.Void Vuforia.QCARRendererImpl::Pause(System.Boolean)
extern TypeInfo* QCARManager_t155_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t660_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_Pause_m3043 (QCARRendererImpl_t666 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		QCARManagerImpl_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t155_il2cpp_TypeInfo_var);
		QCARManager_t155 * L_0 = QCARManager_get_Instance_m484(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___pause;
		NullCheck(((QCARManagerImpl_t660 *)Castclass(L_0, QCARManagerImpl_t660_il2cpp_TypeInfo_var)));
		QCARManagerImpl_Pause_m3020(((QCARManagerImpl_t660 *)Castclass(L_0, QCARManagerImpl_t660_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::UnityRenderEvent(Vuforia.QCARRendererImpl/RenderEvent)
extern "C" void QCARRendererImpl_UnityRenderEvent_m3044 (QCARRendererImpl_t666 * __this, int32_t ___renderEvent, const MethodInfo* method)
{
	{
		int32_t L_0 = ___renderEvent;
		GL_IssuePluginEvent_m4430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::.ctor()
extern TypeInfo* QCARRenderer_t664_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl__ctor_m3045 (QCARRendererImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1016);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t664_il2cpp_TypeInfo_var);
		QCARRenderer__ctor_m3034(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImplMethodDeclarations.h"

// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"


// System.Void Vuforia.QCARUnityImpl::Deinit()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_Deinit_m3046 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceFuncInvoker0< int32_t >::Invoke(127 /* System.Int32 Vuforia.IQCARWrapper::QcarDeinit() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Boolean Vuforia.QCARUnityImpl::IsRendererDirty()
extern TypeInfo* CameraDevice_t573_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t611_il2cpp_TypeInfo_var;
extern TypeInfo* QCARUnityImpl_t667_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_IsRendererDirty_m3047 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1017);
		CameraDeviceImpl_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARUnityImpl_t667_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1137);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t611 * V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t573_il2cpp_TypeInfo_var);
		CameraDevice_t573 * L_0 = CameraDevice_get_Instance_m2673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t611 *)Castclass(L_0, CameraDeviceImpl_t611_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARUnityImpl_t667_il2cpp_TypeInfo_var);
		bool L_1 = ((QCARUnityImpl_t667_StaticFields*)QCARUnityImpl_t667_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0;
		V_1 = L_1;
		((QCARUnityImpl_t667_StaticFields*)QCARUnityImpl_t667_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0 = 0;
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		CameraDeviceImpl_t611 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = CameraDeviceImpl_IsDirty_m2877(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARUnityImpl::SetHint(Vuforia.QCARUnity/QCARHint,System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_SetHint_m3048 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral206, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___hint;
		int32_t L_2 = ___value;
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(67 /* System.Int32 Vuforia.IQCARWrapper::QcarSetHint(System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.QCARUnityImpl::SetHint(System.Int32,System.Int32)
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_SetHint_m3049 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m417(NULL /*static, unused*/, (String_t*) &_stringLiteral206, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___hint;
		int32_t L_2 = ___value;
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(67 /* System.Int32 Vuforia.IQCARWrapper::QcarSetHint(System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
	}
}
// UnityEngine.Matrix4x4 Vuforia.QCARUnityImpl::GetProjectionGL(System.Single,System.Single,UnityEngine.ScreenOrientation)
extern const Il2CppType* Single_t151_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t585_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t792_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t156  QCARUnityImpl_GetProjectionGL_m3050 (Object_t * __this /* static, unused */, float ___nearPlane, float ___farPlane, int32_t ___screenOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t151_0_0_0_var = il2cpp_codegen_type_from_index(77);
		SingleU5BU5D_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1014);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Marshal_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1015);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t585* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t156  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t585*)SZArrayNew(SingleU5BU5D_t585_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(Single_t151_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4272(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t585* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4273(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = ___nearPlane;
		float L_6 = ___farPlane;
		IntPtr_t L_7 = V_1;
		int32_t L_8 = ___screenOrientation;
		NullCheck(L_4);
		InterfaceFuncInvoker4< int32_t, float, float, IntPtr_t, int32_t >::Invoke(68 /* System.Int32 Vuforia.IQCARWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8);
		IntPtr_t L_9 = V_1;
		SingleU5BU5D_t585* L_10 = V_0;
		SingleU5BU5D_t585* L_11 = V_0;
		NullCheck(L_11);
		Marshal_Copy_m4274(NULL /*static, unused*/, L_9, L_10, 0, (((int32_t)(((Array_t *)L_11)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t156  L_12 = Matrix4x4_get_identity_m4275(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_12;
		V_3 = 0;
		goto IL_0054;
	}

IL_0045:
	{
		int32_t L_13 = V_3;
		SingleU5BU5D_t585* L_14 = V_0;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Matrix4x4_set_Item_m4276((&V_2), L_13, (*(float*)(float*)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)16))))
		{
			goto IL_0045;
		}
	}
	{
		IntPtr_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t792_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4277(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Matrix4x4_t156  L_20 = V_2;
		return L_20;
	}
}
// System.Void Vuforia.QCARUnityImpl::SetApplicationEnvironment()
extern TypeInfo* Regex_t822_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_SetApplicationEnvironment_m3051 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1138);
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringU5BU5D_t109* V_3 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		String_t* L_0 = Application_get_unityVersion_m4431(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t822_il2cpp_TypeInfo_var);
		StringU5BU5D_t109* L_1 = Regex_Split_m4432(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral207, /*hidden argument*/NULL);
		V_3 = L_1;
		StringU5BU5D_t109* L_2 = V_3;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) < ((int32_t)3)))
		{
			goto IL_0037;
		}
	}
	{
		StringU5BU5D_t109* L_3 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		int32_t L_5 = Int32_Parse_m4433(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_3, L_4)), /*hidden argument*/NULL);
		V_0 = L_5;
		StringU5BU5D_t109* L_6 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		int32_t L_8 = Int32_Parse_m4433(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7)), /*hidden argument*/NULL);
		V_1 = L_8;
		StringU5BU5D_t109* L_9 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		int32_t L_10 = 2;
		int32_t L_11 = Int32_Parse_m4433(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_10)), /*hidden argument*/NULL);
		V_2 = L_11;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_12 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_12);
		InterfaceActionInvoker3< int32_t, int32_t, int32_t >::Invoke(69 /* System.Void Vuforia.IQCARWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_12, L_13, L_14, L_15);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::OnPause()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_OnPause_m3052 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(50 /* System.Void Vuforia.IQCARWrapper::OnPause() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::OnResume()
extern TypeInfo* QCARWrapper_t703_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t702_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_OnResume_m3053 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1012);
		IQCARWrapper_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1013);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t703_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3947(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(51 /* System.Void Vuforia.IQCARWrapper::OnResume() */, IQCARWrapper_t702_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::SetRendererDirty()
extern TypeInfo* QCARUnityImpl_t667_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_SetRendererDirty_m3054 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARUnityImpl_t667_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1137);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARUnityImpl_t667_il2cpp_TypeInfo_var);
		((QCARUnityImpl_t667_StaticFields*)QCARUnityImpl_t667_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0 = 1;
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::.cctor()
extern "C" void QCARUnityImpl__cctor_m3055 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0MethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_28.h"
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_28MethodDeclarations.h"


// System.Void Vuforia.SmartTerrainTrackableImpl::.ctor(System.String,System.Int32,Vuforia.SmartTerrainTrackable)
extern TypeInfo* List_1_t668_il2cpp_TypeInfo_var;
extern TypeInfo* PoseData_t641_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4434_MethodInfo_var;
extern "C" void SmartTerrainTrackableImpl__ctor_m3056 (SmartTerrainTrackableImpl_t669 * __this, String_t* ___name, int32_t ___id, Object_t * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1139);
		PoseData_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1140);
		List_1__ctor_m4434_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484009);
		s_Il2CppMethodIntialized = true;
	}
	PoseData_t641  V_0 = {0};
	{
		List_1_t668 * L_0 = (List_1_t668 *)il2cpp_codegen_object_new (List_1_t668_il2cpp_TypeInfo_var);
		List_1__ctor_m4434(L_0, /*hidden argument*/List_1__ctor_m4434_MethodInfo_var);
		__this->___mChildren_2 = L_0;
		String_t* L_1 = ___name;
		int32_t L_2 = ___id;
		TrackableImpl__ctor_m2712(__this, L_1, L_2, /*hidden argument*/NULL);
		__this->___mMeshRevision_4 = 0;
		Object_t * L_3 = ___parent;
		SmartTerrainTrackableImpl_set_Parent_m3060(__this, L_3, /*hidden argument*/NULL);
		Initobj (PoseData_t641_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t13  L_4 = Quaternion_get_identity_m271(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___orientation_1 = L_4;
		Vector3_t15  L_5 = Vector3_get_zero_m373(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_5;
		PoseData_t641  L_6 = V_0;
		__this->___mLocalPose_5 = L_6;
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable> Vuforia.SmartTerrainTrackableImpl::get_Children()
extern "C" Object_t* SmartTerrainTrackableImpl_get_Children_m3057 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		List_1_t668 * L_0 = (__this->___mChildren_2);
		return L_0;
	}
}
// System.Int32 Vuforia.SmartTerrainTrackableImpl::get_MeshRevision()
extern "C" int32_t SmartTerrainTrackableImpl_get_MeshRevision_m3058 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mMeshRevision_4);
		return L_0;
	}
}
// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableImpl::get_Parent()
extern "C" Object_t * SmartTerrainTrackableImpl_get_Parent_m3059 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CParentU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::set_Parent(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_set_Parent_m3060 (SmartTerrainTrackableImpl_t669 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.Mesh Vuforia.SmartTerrainTrackableImpl::GetMesh()
extern "C" Mesh_t153 * SmartTerrainTrackableImpl_GetMesh_m3061 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		Mesh_t153 * L_0 = (__this->___mMesh_3);
		return L_0;
	}
}
// UnityEngine.Vector3 Vuforia.SmartTerrainTrackableImpl::get_LocalPosition()
extern "C" Vector3_t15  SmartTerrainTrackableImpl_get_LocalPosition_m3062 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		PoseData_t641 * L_0 = &(__this->___mLocalPose_5);
		Vector3_t15 * L_1 = &(L_0->___position_0);
		float L_2 = (L_1->___x_1);
		PoseData_t641 * L_3 = &(__this->___mLocalPose_5);
		Vector3_t15 * L_4 = &(L_3->___position_0);
		float L_5 = (L_4->___z_3);
		PoseData_t641 * L_6 = &(__this->___mLocalPose_5);
		Vector3_t15 * L_7 = &(L_6->___position_0);
		float L_8 = (L_7->___y_2);
		Vector3_t15  L_9 = {0};
		Vector3__ctor_m249(&L_9, L_2, L_5, ((-L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::SetLocalPose(Vuforia.QCARManagerImpl/PoseData)
extern "C" void SmartTerrainTrackableImpl_SetLocalPose_m3063 (SmartTerrainTrackableImpl_t669 * __this, PoseData_t641  ___localPose, const MethodInfo* method)
{
	{
		PoseData_t641  L_0 = ___localPose;
		__this->___mLocalPose_5 = L_0;
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::DestroyMesh()
extern "C" void SmartTerrainTrackableImpl_DestroyMesh_m3064 (SmartTerrainTrackableImpl_t669 * __this, const MethodInfo* method)
{
	{
		Mesh_t153 * L_0 = (__this->___mMesh_3);
		Object_Destroy_m471(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::AddChild(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_AddChild_m3065 (SmartTerrainTrackableImpl_t669 * __this, Object_t * ___newChild, const MethodInfo* method)
{
	{
		List_1_t668 * L_0 = (__this->___mChildren_2);
		Object_t * L_1 = ___newChild;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		List_1_t668 * L_3 = (__this->___mChildren_2);
		Object_t * L_4 = ___newChild;
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Add(!0) */, L_3, L_4);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::RemoveChild(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_RemoveChild_m3066 (SmartTerrainTrackableImpl_t669 * __this, Object_t * ___removedChild, const MethodInfo* method)
{
	{
		List_1_t668 * L_0 = (__this->___mChildren_2);
		Object_t * L_1 = ___removedChild;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		List_1_t668 * L_3 = (__this->___mChildren_2);
		Object_t * L_4 = ___removedChild;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Remove(!0) */, L_3, L_4);
	}

IL_001b:
	{
		return;
	}
}
// Vuforia.SurfaceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceImplMethodDeclarations.h"

// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"


// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
extern TypeInfo* Int32U5BU5D_t19_il2cpp_TypeInfo_var;
extern "C" void SurfaceImpl__ctor_m3067 (SurfaceImpl_t670 * __this, int32_t ___id, Object_t * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mMeshBoundaries_8 = ((Int32U5BU5D_t19*)SZArrayNew(Int32U5BU5D_t19_il2cpp_TypeInfo_var, 0));
		int32_t L_0 = ___id;
		Object_t * L_1 = ___parent;
		SmartTerrainTrackableImpl__ctor_m3056(__this, (String_t*) &_stringLiteral208, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
extern "C" void SurfaceImpl_SetID_m3068 (SurfaceImpl_t670 * __this, int32_t ___trackableID, const MethodInfo* method)
{
	{
		int32_t L_0 = ___trackableID;
		TrackableImpl_set_ID_m2716(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
extern "C" void SurfaceImpl_SetMesh_m3069 (SurfaceImpl_t670 * __this, int32_t ___meshRev, Mesh_t153 * ___mesh, Mesh_t153 * ___navMesh, Int32U5BU5D_t19* ___meshBoundaries, const MethodInfo* method)
{
	{
		Mesh_t153 * L_0 = ___mesh;
		((SmartTerrainTrackableImpl_t669 *)__this)->___mMesh_3 = L_0;
		Mesh_t153 * L_1 = ___navMesh;
		__this->___mNavMesh_7 = L_1;
		int32_t L_2 = ___meshRev;
		((SmartTerrainTrackableImpl_t669 *)__this)->___mMeshRevision_4 = L_2;
		Int32U5BU5D_t19* L_3 = ___meshBoundaries;
		__this->___mMeshBoundaries_8 = L_3;
		__this->___mAreaNeedsUpdate_11 = 1;
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
extern TypeInfo* Mathf_t104_il2cpp_TypeInfo_var;
extern "C" void SurfaceImpl_SetBoundingBox_m3070 (SurfaceImpl_t670 * __this, Rect_t124  ___boundingBox, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Vector3_t15  V_5 = {0};
	Int32U5BU5D_t19* V_6 = {0};
	int32_t V_7 = 0;
	{
		Rect_t124  L_0 = ___boundingBox;
		__this->___mBoundingBox_9 = L_0;
		Mesh_t153 * L_1 = (((SmartTerrainTrackableImpl_t669 *)__this)->___mMesh_3);
		bool L_2 = Object_op_Inequality_m386(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b6;
		}
	}
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (std::numeric_limits<float>::infinity());
		V_2 = (-std::numeric_limits<float>::infinity());
		V_3 = (-std::numeric_limits<float>::infinity());
		Int32U5BU5D_t19* L_3 = (__this->___mMeshBoundaries_8);
		V_6 = L_3;
		V_7 = 0;
		goto IL_009b;
	}

IL_003d:
	{
		Int32U5BU5D_t19* L_4 = V_6;
		int32_t L_5 = V_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6));
		Mesh_t153 * L_7 = (((SmartTerrainTrackableImpl_t669 *)__this)->___mMesh_3);
		NullCheck(L_7);
		Vector3U5BU5D_t154* L_8 = Mesh_get_vertices_m487(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		V_5 = (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_8, L_9)));
		float L_10 = ((&V_5)->___x_1);
		float L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t104_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m2380(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = ((&V_5)->___z_3);
		float L_14 = V_1;
		float L_15 = Mathf_Min_m2380(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = ((&V_5)->___x_1);
		float L_17 = V_2;
		float L_18 = Mathf_Max_m2339(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = ((&V_5)->___z_3);
		float L_20 = V_3;
		float L_21 = Mathf_Max_m2339(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		int32_t L_22 = V_7;
		V_7 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_23 = V_7;
		Int32U5BU5D_t19* L_24 = V_6;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)(((Array_t *)L_24)->max_length))))))
		{
			goto IL_003d;
		}
	}
	{
		float L_25 = V_0;
		float L_26 = V_1;
		float L_27 = V_2;
		float L_28 = V_0;
		float L_29 = V_3;
		float L_30 = V_1;
		Rect_t124  L_31 = {0};
		Rect__ctor_m383(&L_31, L_25, L_26, ((float)((float)L_27-(float)L_28)), ((float)((float)L_29-(float)L_30)), /*hidden argument*/NULL);
		__this->___mBoundingBox_9 = L_31;
	}

IL_00b6:
	{
		return;
	}
}
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
extern "C" Mesh_t153 * SurfaceImpl_GetNavMesh_m3071 (SurfaceImpl_t670 * __this, const MethodInfo* method)
{
	{
		Mesh_t153 * L_0 = (__this->___mNavMesh_7);
		return L_0;
	}
}
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
extern "C" Int32U5BU5D_t19* SurfaceImpl_GetMeshBoundaries_m3072 (SurfaceImpl_t670 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t19* L_0 = (__this->___mMeshBoundaries_8);
		return L_0;
	}
}
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
extern "C" Rect_t124  SurfaceImpl_get_BoundingBox_m3073 (SurfaceImpl_t670 * __this, const MethodInfo* method)
{
	{
		Rect_t124  L_0 = (__this->___mBoundingBox_9);
		return L_0;
	}
}
// System.Single Vuforia.SurfaceImpl::GetArea()
extern "C" float SurfaceImpl_GetArea_m3074 (SurfaceImpl_t670 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t154* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t15  V_3 = {0};
	{
		bool L_0 = (__this->___mAreaNeedsUpdate_11);
		if (!L_0)
		{
			goto IL_00a9;
		}
	}
	{
		__this->___mSurfaceArea_10 = (0.0f);
		Mesh_t153 * L_1 = (((SmartTerrainTrackableImpl_t669 *)__this)->___mMesh_3);
		bool L_2 = Object_op_Inequality_m386(NULL /*static, unused*/, L_1, (Object_t123 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a2;
		}
	}
	{
		Mesh_t153 * L_3 = (((SmartTerrainTrackableImpl_t669 *)__this)->___mMesh_3);
		NullCheck(L_3);
		Vector3U5BU5D_t154* L_4 = Mesh_get_vertices_m487(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Int32U5BU5D_t19* L_5 = (__this->___mMeshBoundaries_8);
		NullCheck(L_5);
		V_1 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))-(int32_t)1));
		V_2 = 0;
		goto IL_0085;
	}

IL_003f:
	{
		float L_6 = (__this->___mSurfaceArea_10);
		Vector3U5BU5D_t154* L_7 = V_0;
		Int32U5BU5D_t19* L_8 = (__this->___mMeshBoundaries_8);
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, (*(int32_t*)(int32_t*)SZArrayLdElema(L_8, L_10)));
		Vector3U5BU5D_t154* L_11 = V_0;
		Int32U5BU5D_t19* L_12 = (__this->___mMeshBoundaries_8);
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, (*(int32_t*)(int32_t*)SZArrayLdElema(L_12, L_14)));
		Vector3_t15  L_15 = Vector3_Cross_m4435(NULL /*static, unused*/, (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_7, (*(int32_t*)(int32_t*)SZArrayLdElema(L_8, L_10))))), (*(Vector3_t15 *)((Vector3_t15 *)(Vector3_t15 *)SZArrayLdElema(L_11, (*(int32_t*)(int32_t*)SZArrayLdElema(L_12, L_14))))), /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = Vector3_get_magnitude_m4436((&V_3), /*hidden argument*/NULL);
		__this->___mSurfaceArea_10 = ((float)((float)L_6+(float)L_16));
		int32_t L_17 = V_2;
		int32_t L_18 = L_17;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
		V_1 = L_18;
	}

IL_0085:
	{
		int32_t L_19 = V_2;
		Int32U5BU5D_t19* L_20 = (__this->___mMeshBoundaries_8);
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)(((Array_t *)L_20)->max_length))))))
		{
			goto IL_003f;
		}
	}
	{
		float L_21 = (__this->___mSurfaceArea_10);
		__this->___mSurfaceArea_10 = ((float)((float)L_21*(float)(0.5f)));
	}

IL_00a2:
	{
		__this->___mAreaNeedsUpdate_11 = 0;
	}

IL_00a9:
	{
		float L_22 = (__this->___mSurfaceArea_10);
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
