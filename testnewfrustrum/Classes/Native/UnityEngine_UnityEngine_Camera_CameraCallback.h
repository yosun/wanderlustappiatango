﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t3;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t1209  : public MulticastDelegate_t307
{
};
