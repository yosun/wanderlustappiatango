﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IDeselectHandler
struct IDeselectHandler_t403;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t197;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct  EventFunction_1_t219  : public MulticastDelegate_t307
{
};
