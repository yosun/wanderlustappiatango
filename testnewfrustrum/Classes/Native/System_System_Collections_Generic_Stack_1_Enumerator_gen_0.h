﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t979;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>
struct  Enumerator_t3684 
{
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::parent
	Stack_1_t979 * ___parent_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>::_version
	int32_t ____version_2;
};
