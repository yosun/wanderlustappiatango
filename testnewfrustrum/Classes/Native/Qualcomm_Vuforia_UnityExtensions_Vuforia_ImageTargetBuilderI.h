﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
struct TrackableSource_t619;
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilderImpl
struct  ImageTargetBuilderImpl_t620  : public ImageTargetBuilder_t607
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t619 * ___mTrackableSource_0;
};
