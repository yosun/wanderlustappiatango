﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t143;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t2507  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t143 * ____delegate_0;
};
