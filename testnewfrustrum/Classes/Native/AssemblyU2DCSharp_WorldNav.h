﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WorldNav
struct  WorldNav_t26  : public MonoBehaviour_t7
{
	// UnityEngine.GameObject WorldNav::mainCharacter
	GameObject_t2 * ___mainCharacter_3;
	// System.Int32 WorldNav::dirwalking
	int32_t ___dirwalking_7;
	// UnityEngine.GameObject WorldNav::goUI_Directions_TextWalkButton
	GameObject_t2 * ___goUI_Directions_TextWalkButton_8;
};
struct WorldNav_t26_StaticFields{
	// System.Single WorldNav::coeff
	float ___coeff_2;
	// UnityEngine.GameObject WorldNav::goMainCharacter
	GameObject_t2 * ___goMainCharacter_4;
	// UnityEngine.Camera WorldNav::camCharacter
	Camera_t3 * ___camCharacter_5;
	// System.Boolean WorldNav::walking
	bool ___walking_6;
};
