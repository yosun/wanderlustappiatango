﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3261;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3253;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16963_gshared (Enumerator_t3261 * __this, Dictionary_2_t3253 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16963(__this, ___host, method) (( void (*) (Enumerator_t3261 *, Dictionary_2_t3253 *, const MethodInfo*))Enumerator__ctor_m16963_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16964_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16964(__this, method) (( Object_t * (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16965_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16965(__this, method) (( void (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_Dispose_m16965_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16966_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16966(__this, method) (( bool (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_MoveNext_m16966_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16967_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16967(__this, method) (( Object_t * (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_get_Current_m16967_gshared)(__this, method)
