﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3814;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt64,System.Object>
struct  KeyCollection_t3818  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt64,System.Object>::dictionary
	Dictionary_2_t3814 * ___dictionary_0;
};
