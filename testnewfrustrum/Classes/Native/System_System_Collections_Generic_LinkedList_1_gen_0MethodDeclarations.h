﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3927;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3926;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t528;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m26967_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m26967(__this, method) (( void (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1__ctor_m26967_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m26968_gshared (LinkedList_1_t3927 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m26968(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3927 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))LinkedList_1__ctor_m26968_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26969_gshared (LinkedList_1_t3927 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26969(__this, ___value, method) (( void (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26969_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m26970_gshared (LinkedList_1_t3927 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m26970(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3927 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m26970_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26971_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26971(__this, method) (( Object_t* (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26971_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26972_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26972(__this, method) (( Object_t * (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26973_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26973(__this, method) (( bool (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26973_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26974_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26974(__this, method) (( bool (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26974_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26975_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26975(__this, method) (( Object_t * (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26975_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m26976_gshared (LinkedList_1_t3927 * __this, LinkedListNode_1_t3926 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m26976(__this, ___node, method) (( void (*) (LinkedList_1_t3927 *, LinkedListNode_1_t3926 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m26976_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t3926 * LinkedList_1_AddLast_m26977_gshared (LinkedList_1_t3927 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m26977(__this, ___value, method) (( LinkedListNode_1_t3926 * (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m26977_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m26978_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m26978(__this, method) (( void (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_Clear_m26978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m26979_gshared (LinkedList_1_t3927 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m26979(__this, ___value, method) (( bool (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m26979_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m26980_gshared (LinkedList_1_t3927 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m26980(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3927 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m26980_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t3926 * LinkedList_1_Find_m26981_gshared (LinkedList_1_t3927 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m26981(__this, ___value, method) (( LinkedListNode_1_t3926 * (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m26981_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3928  LinkedList_1_GetEnumerator_m26982_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m26982(__this, method) (( Enumerator_t3928  (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_GetEnumerator_m26982_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m26983_gshared (LinkedList_1_t3927 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m26983(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3927 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))LinkedList_1_GetObjectData_m26983_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m26984_gshared (LinkedList_1_t3927 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m26984(__this, ___sender, method) (( void (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m26984_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m26985_gshared (LinkedList_1_t3927 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m26985(__this, ___value, method) (( bool (*) (LinkedList_1_t3927 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m26985_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m26986_gshared (LinkedList_1_t3927 * __this, LinkedListNode_1_t3926 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m26986(__this, ___node, method) (( void (*) (LinkedList_1_t3927 *, LinkedListNode_1_t3926 *, const MethodInfo*))LinkedList_1_Remove_m26986_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m26987_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m26987(__this, method) (( void (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_RemoveLast_m26987_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m26988_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m26988(__this, method) (( int32_t (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_get_Count_m26988_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t3926 * LinkedList_1_get_First_m26989_gshared (LinkedList_1_t3927 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m26989(__this, method) (( LinkedListNode_1_t3926 * (*) (LinkedList_1_t3927 *, const MethodInfo*))LinkedList_1_get_First_m26989_gshared)(__this, method)
