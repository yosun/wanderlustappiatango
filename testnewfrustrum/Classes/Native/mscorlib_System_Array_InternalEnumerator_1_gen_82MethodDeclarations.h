﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t3886;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26599_gshared (InternalEnumerator_1_t3886 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26599(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3886 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26599_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26600_gshared (InternalEnumerator_1_t3886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26600(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3886 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26600_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26601_gshared (InternalEnumerator_1_t3886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26601(__this, method) (( void (*) (InternalEnumerator_1_t3886 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26601_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26602_gshared (InternalEnumerator_1_t3886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26602(__this, method) (( bool (*) (InternalEnumerator_1_t3886 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26602_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t1323  InternalEnumerator_1_get_Current_m26603_gshared (InternalEnumerator_1_t3886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26603(__this, method) (( HitInfo_t1323  (*) (InternalEnumerator_1_t3886 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26603_gshared)(__this, method)
