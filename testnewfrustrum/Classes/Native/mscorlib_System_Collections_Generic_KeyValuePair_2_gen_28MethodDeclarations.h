﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t3604;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22532_gshared (KeyValuePair_2_t3604 * __this, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22532(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3604 *, Object_t *, ProfileData_t734 , const MethodInfo*))KeyValuePair_2__ctor_m22532_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m22533_gshared (KeyValuePair_2_t3604 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22533(__this, method) (( Object_t * (*) (KeyValuePair_2_t3604 *, const MethodInfo*))KeyValuePair_2_get_Key_m22533_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22534_gshared (KeyValuePair_2_t3604 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22534(__this, ___value, method) (( void (*) (KeyValuePair_2_t3604 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m22534_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" ProfileData_t734  KeyValuePair_2_get_Value_m22535_gshared (KeyValuePair_2_t3604 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22535(__this, method) (( ProfileData_t734  (*) (KeyValuePair_2_t3604 *, const MethodInfo*))KeyValuePair_2_get_Value_m22535_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22536_gshared (KeyValuePair_2_t3604 * __this, ProfileData_t734  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22536(__this, ___value, method) (( void (*) (KeyValuePair_2_t3604 *, ProfileData_t734 , const MethodInfo*))KeyValuePair_2_set_Value_m22536_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22537_gshared (KeyValuePair_2_t3604 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22537(__this, method) (( String_t* (*) (KeyValuePair_2_t3604 *, const MethodInfo*))KeyValuePair_2_ToString_m22537_gshared)(__this, method)
