﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.FloatPlugin
struct FloatPlugin_t1007;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t1052;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t1053;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t1054;
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.FloatPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_Reset_m5493 (FloatPlugin_t1007 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.FloatPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Single)
extern "C" float FloatPlugin_ConvertToStartValue_m5494 (FloatPlugin_t1007 * __this, TweenerCore_3_t1052 * ___t, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.FloatPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_SetRelativeEndValue_m5495 (FloatPlugin_t1007 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.FloatPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_SetChangeValue_m5496 (FloatPlugin_t1007 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.FloatPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.FloatOptions,System.Single,System.Single)
extern "C" float FloatPlugin_GetSpeedBasedDuration_m5497 (FloatPlugin_t1007 * __this, FloatOptions_t996  ___options, float ___unitsXSecond, float ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.FloatPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.FloatOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void FloatPlugin_EvaluateAndApply_m5498 (FloatPlugin_t1007 * __this, FloatOptions_t996  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1053 * ___getter, DOSetter_1_t1054 * ___setter, float ___elapsed, float ___startValue, float ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.FloatPlugin::.ctor()
extern "C" void FloatPlugin__ctor_m5499 (FloatPlugin_t1007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
