﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t674;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t583;

// System.Single Vuforia.SmartTerrainTracker::get_ScaleToMillimeter()
// System.Boolean Vuforia.SmartTerrainTracker::SetScaleToMillimeter(System.Single)
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder()
// System.Void Vuforia.SmartTerrainTracker::.ctor()
extern "C" void SmartTerrainTracker__ctor_m3087 (SmartTerrainTracker_t674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
