﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct Transform_1_t3869;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m26478_gshared (Transform_1_t3869 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m26478(__this, ___object, ___method, method) (( void (*) (Transform_1_t3869 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m26478_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3836  Transform_1_Invoke_m26479_gshared (Transform_1_t3869 * __this, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m26479(__this, ___key, ___value, method) (( KeyValuePair_2_t3836  (*) (Transform_1_t3869 *, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Transform_1_Invoke_m26479_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m26480_gshared (Transform_1_t3869 * __this, Object_t * ___key, KeyValuePair_2_t3254  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m26480(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3869 *, Object_t *, KeyValuePair_2_t3254 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m26480_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3836  Transform_1_EndInvoke_m26481_gshared (Transform_1_t3869 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m26481(__this, ___result, method) (( KeyValuePair_2_t3836  (*) (Transform_1_t3869 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m26481_gshared)(__this, ___result, method)
