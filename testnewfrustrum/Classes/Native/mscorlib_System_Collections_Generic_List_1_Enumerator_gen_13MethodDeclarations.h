﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>
struct Enumerator_t861;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t97;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t780;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21573(__this, ___l, method) (( void (*) (Enumerator_t861 *, List_1_t780 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21574(__this, method) (( Object_t * (*) (Enumerator_t861 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m21575(__this, method) (( void (*) (Enumerator_t861 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::VerifyState()
#define Enumerator_VerifyState_m21576(__this, method) (( void (*) (Enumerator_t861 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m4538(__this, method) (( bool (*) (Enumerator_t861 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m4537(__this, method) (( Object_t * (*) (Enumerator_t861 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
