﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t324;

// System.Void UnityEngine.UI.Scrollbar/ScrollEvent::.ctor()
extern "C" void ScrollEvent__ctor_m1419 (ScrollEvent_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
