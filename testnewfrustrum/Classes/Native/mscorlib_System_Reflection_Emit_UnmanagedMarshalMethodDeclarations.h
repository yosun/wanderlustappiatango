﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t2220;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t2051;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t2051 * UnmanagedMarshal_ToMarshalAsAttribute_m11772 (UnmanagedMarshal_t2220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
