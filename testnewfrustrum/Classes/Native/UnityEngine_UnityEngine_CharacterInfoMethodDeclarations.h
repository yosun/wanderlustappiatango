﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CharacterInfo
struct CharacterInfo_t1243;
struct CharacterInfo_t1243_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m6434 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m6435 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m6436 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m6437 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m6438 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m6439 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m6440 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m6441 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t10  CharacterInfo_get_uvBottomLeftUnFlipped_m6442 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t10  CharacterInfo_get_uvBottomRightUnFlipped_m6443 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t10  CharacterInfo_get_uvTopRightUnFlipped_m6444 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t10  CharacterInfo_get_uvTopLeftUnFlipped_m6445 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t10  CharacterInfo_get_uvBottomLeft_m6446 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t10  CharacterInfo_get_uvBottomRight_m6447 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t10  CharacterInfo_get_uvTopRight_m6448 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t10  CharacterInfo_get_uvTopLeft_m6449 (CharacterInfo_t1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void CharacterInfo_t1243_marshal(const CharacterInfo_t1243& unmarshaled, CharacterInfo_t1243_marshaled& marshaled);
void CharacterInfo_t1243_marshal_back(const CharacterInfo_t1243_marshaled& marshaled, CharacterInfo_t1243& unmarshaled);
void CharacterInfo_t1243_marshal_cleanup(CharacterInfo_t1243_marshaled& marshaled);
