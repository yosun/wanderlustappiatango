﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t1791;
struct U24ArrayTypeU24256_t1791_marshaled;

void U24ArrayTypeU24256_t1791_marshal(const U24ArrayTypeU24256_t1791& unmarshaled, U24ArrayTypeU24256_t1791_marshaled& marshaled);
void U24ArrayTypeU24256_t1791_marshal_back(const U24ArrayTypeU24256_t1791_marshaled& marshaled, U24ArrayTypeU24256_t1791& unmarshaled);
void U24ArrayTypeU24256_t1791_marshal_cleanup(U24ArrayTypeU24256_t1791_marshaled& marshaled);
