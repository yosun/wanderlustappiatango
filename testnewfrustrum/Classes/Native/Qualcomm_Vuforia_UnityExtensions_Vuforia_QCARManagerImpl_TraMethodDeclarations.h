﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/TrackableResultData
struct TrackableResultData_t642;
struct TrackableResultData_t642_marshaled;

void TrackableResultData_t642_marshal(const TrackableResultData_t642& unmarshaled, TrackableResultData_t642_marshaled& marshaled);
void TrackableResultData_t642_marshal_back(const TrackableResultData_t642_marshaled& marshaled, TrackableResultData_t642& unmarshaled);
void TrackableResultData_t642_marshal_cleanup(TrackableResultData_t642_marshaled& marshaled);
