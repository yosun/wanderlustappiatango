﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t1736;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Hashtable/HashKeys
struct  HashKeys_t2150  : public Object_t
{
	// System.Collections.Hashtable System.Collections.Hashtable/HashKeys::host
	Hashtable_t1736 * ___host_0;
};
