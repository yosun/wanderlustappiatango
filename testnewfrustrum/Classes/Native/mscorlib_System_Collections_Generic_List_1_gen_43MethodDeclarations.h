﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t752;
// System.Object
struct Object_t;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t789;
// System.Collections.Generic.IEnumerable`1<Vuforia.ITextRecoEventHandler>
struct IEnumerable_1_t4273;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITextRecoEventHandler>
struct IEnumerator_1_t4274;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ITextRecoEventHandler>
struct ICollection_1_t4275;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>
struct ReadOnlyCollection_1_t3640;
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t3638;
// System.Predicate`1<Vuforia.ITextRecoEventHandler>
struct Predicate_1_t3641;
// System.Comparison`1<Vuforia.ITextRecoEventHandler>
struct Comparison_1_t3642;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4651(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22961(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m22962(__this, ___capacity, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.cctor()
#define List_1__cctor_m22963(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22964(__this, method) (( Object_t* (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22965(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t752 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22966(__this, method) (( Object_t * (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22967(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22968(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22969(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22970(__this, ___index, ___item, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22971(__this, ___item, method) (( void (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22972(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22973(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22974(__this, method) (( Object_t * (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22975(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22976(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22977(__this, ___index, method) (( Object_t * (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22978(__this, ___index, ___value, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Add(T)
#define List_1_Add_m22979(__this, ___item, method) (( void (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22980(__this, ___newCount, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22981(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22982(__this, ___enumerable, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22983(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m22984(__this, method) (( ReadOnlyCollection_1_t3640 * (*) (List_1_t752 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Clear()
#define List_1_Clear_m22985(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Contains(T)
#define List_1_Contains_m22986(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22987(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t752 *, ITextRecoEventHandlerU5BU5D_t3638*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m22988(__this, ___match, method) (( Object_t * (*) (List_1_t752 *, Predicate_1_t3641 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22989(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3641 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22990(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t752 *, int32_t, int32_t, Predicate_1_t3641 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4648(__this, method) (( Enumerator_t890  (*) (List_1_t752 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::IndexOf(T)
#define List_1_IndexOf_m22991(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22992(__this, ___start, ___delta, method) (( void (*) (List_1_t752 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22993(__this, ___index, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m22994(__this, ___index, ___item, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22995(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Remove(T)
#define List_1_Remove_m22996(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22997(__this, ___match, method) (( int32_t (*) (List_1_t752 *, Predicate_1_t3641 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22998(__this, ___index, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Reverse()
#define List_1_Reverse_m22999(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Sort()
#define List_1_Sort_m23000(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23001(__this, ___comparison, method) (( void (*) (List_1_t752 *, Comparison_1_t3642 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::ToArray()
#define List_1_ToArray_m23002(__this, method) (( ITextRecoEventHandlerU5BU5D_t3638* (*) (List_1_t752 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23003(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23004(__this, method) (( int32_t (*) (List_1_t752 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23005(__this, ___value, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Count()
#define List_1_get_Count_m23006(__this, method) (( int32_t (*) (List_1_t752 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23007(__this, ___index, method) (( Object_t * (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23008(__this, ___index, ___value, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
