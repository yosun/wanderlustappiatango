﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t40;

// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m211 (ReconstructionBehaviour_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
