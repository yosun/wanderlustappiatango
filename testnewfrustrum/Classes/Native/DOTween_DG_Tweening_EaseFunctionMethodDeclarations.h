﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.EaseFunction
struct EaseFunction_t945;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern "C" void EaseFunction__ctor_m5362 (EaseFunction_t945 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern "C" float EaseFunction_Invoke_m5363 (EaseFunction_t945 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" float pinvoke_delegate_wrapper_EaseFunction_t945(Il2CppObject* delegate, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period);
// System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C" Object_t * EaseFunction_BeginInvoke_m5364 (EaseFunction_t945 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern "C" float EaseFunction_EndInvoke_m5365 (EaseFunction_t945 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
