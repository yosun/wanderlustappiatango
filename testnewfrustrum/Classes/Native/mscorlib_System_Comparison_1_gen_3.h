﻿#pragma once
#include <stdint.h>
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<DG.Tweening.Core.ABSSequentiable>
struct  Comparison_1_t1058  : public MulticastDelegate_t307
{
};
