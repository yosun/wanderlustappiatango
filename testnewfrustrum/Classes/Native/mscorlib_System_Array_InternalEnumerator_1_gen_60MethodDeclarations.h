﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
struct InternalEnumerator_1_t3713;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24145_gshared (InternalEnumerator_1_t3713 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24145(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3713 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24145_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24146_gshared (InternalEnumerator_1_t3713 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24146(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3713 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24146_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24147_gshared (InternalEnumerator_1_t3713 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24147(__this, method) (( void (*) (InternalEnumerator_1_t3713 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24147_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24148_gshared (InternalEnumerator_1_t3713 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24148(__this, method) (( bool (*) (InternalEnumerator_1_t3713 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24148_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t1305  InternalEnumerator_1_get_Current_m24149_gshared (InternalEnumerator_1_t3713 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24149(__this, method) (( GcAchievementData_t1305  (*) (InternalEnumerator_1_t3713 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24149_gshared)(__this, method)
