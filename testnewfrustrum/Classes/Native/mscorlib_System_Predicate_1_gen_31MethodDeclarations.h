﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.DataSet>
struct Predicate_1_t3426;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t594;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.DataSet>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m19554(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3426 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.DataSet>::Invoke(T)
#define Predicate_1_Invoke_m19555(__this, ___obj, method) (( bool (*) (Predicate_1_t3426 *, DataSet_t594 *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.DataSet>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m19556(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3426 *, DataSet_t594 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.DataSet>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m19557(__this, ___result, method) (( bool (*) (Predicate_1_t3426 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
