﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>
struct ReadOnlyCollection_1_t3323;
// UnityEngine.CanvasGroup
struct CanvasGroup_t442;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.CanvasGroup>
struct IList_1_t3322;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t3321;
// System.Collections.Generic.IEnumerator`1<UnityEngine.CanvasGroup>
struct IEnumerator_1_t4116;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m17886(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3323 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15055_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17887(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3323 *, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15056_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17888(__this, method) (( void (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15057_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17889(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15058_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17890(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15059_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17891(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15060_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17892(__this, ___index, method) (( CanvasGroup_t442 * (*) (ReadOnlyCollection_1_t3323 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15061_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17893(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15062_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17894(__this, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15063_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17895(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3323 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15064_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17896(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15065_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m17897(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3323 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15066_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m17898(__this, method) (( void (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15067_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m17899(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15068_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17900(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3323 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15069_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m17901(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15070_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m17902(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3323 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15071_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17903(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15072_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17904(__this, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15073_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17905(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15074_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17906(__this, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15075_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17907(__this, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15076_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m17908(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3323 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15077_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m17909(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3323 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15078_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::Contains(T)
#define ReadOnlyCollection_1_Contains_m17910(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3323 *, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15079_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m17911(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3323 *, CanvasGroupU5BU5D_t3321*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15080_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m17912(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15081_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m17913(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3323 *, CanvasGroup_t442 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15082_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::get_Count()
#define ReadOnlyCollection_1_get_Count_m17914(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3323 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15083_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m17915(__this, ___index, method) (( CanvasGroup_t442 * (*) (ReadOnlyCollection_1_t3323 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15084_gshared)(__this, ___index, method)
