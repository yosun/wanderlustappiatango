﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2556;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t2491  : public MulticastDelegate_t307
{
};
