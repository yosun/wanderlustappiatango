﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t1057;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24005_gshared (DOSetter_1_t1057 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m24005(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1057 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m24005_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24006_gshared (DOSetter_1_t1057 * __this, int64_t ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m24006(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1057 *, int64_t, const MethodInfo*))DOSetter_1_Invoke_m24006_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m24007_gshared (DOSetter_1_t1057 * __this, int64_t ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m24007(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1057 *, int64_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m24007_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24008_gshared (DOSetter_1_t1057 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m24008(__this, ___result, method) (( void (*) (DOSetter_1_t1057 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m24008_gshared)(__this, ___result, method)
