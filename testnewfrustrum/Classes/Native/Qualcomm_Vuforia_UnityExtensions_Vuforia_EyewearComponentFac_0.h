﻿#pragma once
#include <stdint.h>
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t558;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.EyewearComponentFactory
struct  EyewearComponentFactory_t559  : public Object_t
{
};
struct EyewearComponentFactory_t559_StaticFields{
	// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
