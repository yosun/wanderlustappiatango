﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct  DOSetter_1_t1019  : public MulticastDelegate_t307
{
};
