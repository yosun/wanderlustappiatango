﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Transform_1_t3574;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22056_gshared (Transform_1_t3574 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22056(__this, ___object, ___method, method) (( void (*) (Transform_1_t3574 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22056_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C" VirtualButtonData_t643  Transform_1_Invoke_m22057_gshared (Transform_1_t3574 * __this, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22057(__this, ___key, ___value, method) (( VirtualButtonData_t643  (*) (Transform_1_t3574 *, int32_t, VirtualButtonData_t643 , const MethodInfo*))Transform_1_Invoke_m22057_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22058_gshared (Transform_1_t3574 * __this, int32_t ___key, VirtualButtonData_t643  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22058(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3574 *, int32_t, VirtualButtonData_t643 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22058_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C" VirtualButtonData_t643  Transform_1_EndInvoke_m22059_gshared (Transform_1_t3574 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22059(__this, ___result, method) (( VirtualButtonData_t643  (*) (Transform_1_t3574 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22059_gshared)(__this, ___result, method)
