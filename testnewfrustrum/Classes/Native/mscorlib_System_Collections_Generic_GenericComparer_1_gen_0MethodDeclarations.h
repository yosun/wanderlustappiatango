﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2620;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m14007_gshared (GenericComparer_1_t2620 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m14007(__this, method) (( void (*) (GenericComparer_1_t2620 *, const MethodInfo*))GenericComparer_1__ctor_m14007_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m27681_gshared (GenericComparer_1_t2620 * __this, DateTimeOffset_t1420  ___x, DateTimeOffset_t1420  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m27681(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2620 *, DateTimeOffset_t1420 , DateTimeOffset_t1420 , const MethodInfo*))GenericComparer_1_Compare_m27681_gshared)(__this, ___x, ___y, method)
