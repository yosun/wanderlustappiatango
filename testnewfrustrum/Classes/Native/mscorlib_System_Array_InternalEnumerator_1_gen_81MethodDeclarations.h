﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t3885;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26594_gshared (InternalEnumerator_1_t3885 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26594(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3885 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26594_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26595_gshared (InternalEnumerator_1_t3885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26595(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3885 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26595_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26596_gshared (InternalEnumerator_1_t3885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26596(__this, method) (( void (*) (InternalEnumerator_1_t3885 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26596_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26597_gshared (InternalEnumerator_1_t3885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26597(__this, method) (( bool (*) (InternalEnumerator_1_t3885 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26597_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t2260  InternalEnumerator_1_get_Current_m26598_gshared (InternalEnumerator_1_t3885 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26598(__this, method) (( ParameterModifier_t2260  (*) (InternalEnumerator_1_t3885 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26598_gshared)(__this, method)
