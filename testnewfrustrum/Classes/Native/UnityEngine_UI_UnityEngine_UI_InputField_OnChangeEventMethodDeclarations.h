﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t302;

// System.Void UnityEngine.UI.InputField/OnChangeEvent::.ctor()
extern "C" void OnChangeEvent__ctor_m1243 (OnChangeEvent_t302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
