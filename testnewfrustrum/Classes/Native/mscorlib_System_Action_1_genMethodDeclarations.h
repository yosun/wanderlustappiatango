﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t128;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Int32>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"
#define Action_1__ctor_m398(__this, ___object, ___method, method) (( void (*) (Action_1_t128 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14961_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
#define Action_1_Invoke_m14962(__this, ___obj, method) (( void (*) (Action_1_t128 *, int32_t, const MethodInfo*))Action_1_Invoke_m14963_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m14964(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t128 *, int32_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14965_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m14966(__this, ___result, method) (( void (*) (Action_1_t128 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14967_gshared)(__this, ___result, method)
