﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t706;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"

// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m21101_gshared (Action_1_t706 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m21101(__this, ___object, ___method, method) (( void (*) (Action_1_t706 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m21101_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C" void Action_1_Invoke_m21102_gshared (Action_1_t706 * __this, SmartTerrainInitializationInfo_t588  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m21102(__this, ___obj, method) (( void (*) (Action_1_t706 *, SmartTerrainInitializationInfo_t588 , const MethodInfo*))Action_1_Invoke_m21102_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m21103_gshared (Action_1_t706 * __this, SmartTerrainInitializationInfo_t588  ___obj, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m21103(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t706 *, SmartTerrainInitializationInfo_t588 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m21103_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m21104_gshared (Action_1_t706 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m21104(__this, ___result, method) (( void (*) (Action_1_t706 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m21104_gshared)(__this, ___result, method)
