﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<DG.Tweening.Tween>
struct IList_1_t3669;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>
struct  ReadOnlyCollection_1_t3670  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::list
	Object_t* ___list_0;
};
