﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ResolveEventHandler
struct ResolveEventHandler_t2489;
// System.Object
struct Object_t;
// System.Reflection.Assembly
struct Assembly_t2003;
// System.ResolveEventArgs
struct ResolveEventArgs_t2544;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ResolveEventHandler__ctor_m13991 (ResolveEventHandler_t2489 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern "C" Assembly_t2003 * ResolveEventHandler_Invoke_m13992 (ResolveEventHandler_t2489 * __this, Object_t * ___sender, ResolveEventArgs_t2544 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Assembly_t2003 * pinvoke_delegate_wrapper_ResolveEventHandler_t2489(Il2CppObject* delegate, Object_t * ___sender, ResolveEventArgs_t2544 * ___args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ResolveEventHandler_BeginInvoke_m13993 (ResolveEventHandler_t2489 * __this, Object_t * ___sender, ResolveEventArgs_t2544 * ___args, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern "C" Assembly_t2003 * ResolveEventHandler_EndInvoke_m13994 (ResolveEventHandler_t2489 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
