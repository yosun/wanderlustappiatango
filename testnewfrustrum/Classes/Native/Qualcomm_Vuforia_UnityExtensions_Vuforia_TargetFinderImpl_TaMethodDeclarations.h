﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl/TargetFinderState
struct TargetFinderState_t721;
struct TargetFinderState_t721_marshaled;

void TargetFinderState_t721_marshal(const TargetFinderState_t721& unmarshaled, TargetFinderState_t721_marshaled& marshaled);
void TargetFinderState_t721_marshal_back(const TargetFinderState_t721_marshaled& marshaled, TargetFinderState_t721& unmarshaled);
void TargetFinderState_t721_marshal_cleanup(TargetFinderState_t721_marshaled& marshaled);
