﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t3321;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct  List_1_t338  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::_items
	CanvasGroupU5BU5D_t3321* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::_version
	int32_t ____version_3;
};
struct List_1_t338_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::EmptyArray
	CanvasGroupU5BU5D_t3321* ___EmptyArray_4;
};
