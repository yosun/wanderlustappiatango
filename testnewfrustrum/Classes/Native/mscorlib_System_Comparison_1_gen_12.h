﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.GameObject>
struct  Comparison_1_t3190  : public MulticastDelegate_t307
{
};
