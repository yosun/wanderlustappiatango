﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t1240;
struct SkeletonBone_t1240_marshaled;

void SkeletonBone_t1240_marshal(const SkeletonBone_t1240& unmarshaled, SkeletonBone_t1240_marshaled& marshaled);
void SkeletonBone_t1240_marshal_back(const SkeletonBone_t1240_marshaled& marshaled, SkeletonBone_t1240& unmarshaled);
void SkeletonBone_t1240_marshal_cleanup(SkeletonBone_t1240_marshaled& marshaled);
