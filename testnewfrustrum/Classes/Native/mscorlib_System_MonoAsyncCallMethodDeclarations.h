﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoAsyncCall
struct MonoAsyncCall_t2531;

// System.Void System.MonoAsyncCall::.ctor()
extern "C" void MonoAsyncCall__ctor_m13650 (MonoAsyncCall_t2531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
