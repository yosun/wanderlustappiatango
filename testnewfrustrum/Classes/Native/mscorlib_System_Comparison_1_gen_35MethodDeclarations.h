﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t3488;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t695;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Vuforia.WordResult>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m20452(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3488 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15149_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.WordResult>::Invoke(T,T)
#define Comparison_1_Invoke_m20453(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3488 *, WordResult_t695 *, WordResult_t695 *, const MethodInfo*))Comparison_1_Invoke_m15150_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.WordResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20454(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3488 *, WordResult_t695 *, WordResult_t695 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15151_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.WordResult>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20455(__this, ___result, method) (( int32_t (*) (Comparison_1_t3488 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15152_gshared)(__this, ___result, method)
