﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
struct U3CU3Ec__DisplayClass9a_t961;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::.ctor()
extern "C" void U3CU3Ec__DisplayClass9a__ctor_m5372 (U3CU3Ec__DisplayClass9a_t961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__98()
extern "C" Vector3_t15  U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5373 (U3CU3Ec__DisplayClass9a_t961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__99(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5374 (U3CU3Ec__DisplayClass9a_t961 * __this, Vector3_t15  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
