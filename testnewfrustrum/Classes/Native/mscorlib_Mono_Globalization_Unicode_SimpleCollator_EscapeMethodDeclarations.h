﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t2082;
struct Escape_t2082_marshaled;

void Escape_t2082_marshal(const Escape_t2082& unmarshaled, Escape_t2082_marshaled& marshaled);
void Escape_t2082_marshal_back(const Escape_t2082_marshaled& marshaled, Escape_t2082& unmarshaled);
void Escape_t2082_marshal_cleanup(Escape_t2082_marshaled& marshaled);
