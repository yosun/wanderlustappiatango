﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t675;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t583;

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
extern "C" float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m3088 (SmartTerrainTrackerImpl_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C" bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m3089 (SmartTerrainTrackerImpl_t675 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C" SmartTerrainBuilder_t583 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m3090 (SmartTerrainTrackerImpl_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C" bool SmartTerrainTrackerImpl_Start_m3091 (SmartTerrainTrackerImpl_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C" void SmartTerrainTrackerImpl_Stop_m3092 (SmartTerrainTrackerImpl_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C" void SmartTerrainTrackerImpl__ctor_m3093 (SmartTerrainTrackerImpl_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
