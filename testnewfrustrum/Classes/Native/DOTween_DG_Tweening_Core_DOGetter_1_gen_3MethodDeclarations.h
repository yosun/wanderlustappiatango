﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>
struct DOGetter_1_t1026;
// System.Object
struct Object_t;
// UnityEngine.RectOffset
struct RectOffset_t371;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>::.ctor(System.Object,System.IntPtr)
// DG.Tweening.Core.DOGetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_13MethodDeclarations.h"
#define DOGetter_1__ctor_m23624(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1026 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23603_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>::Invoke()
#define DOGetter_1_Invoke_m23625(__this, method) (( RectOffset_t371 * (*) (DOGetter_1_t1026 *, const MethodInfo*))DOGetter_1_Invoke_m23604_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>::BeginInvoke(System.AsyncCallback,System.Object)
#define DOGetter_1_BeginInvoke_m23626(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1026 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23605_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>::EndInvoke(System.IAsyncResult)
#define DOGetter_1_EndInvoke_m23627(__this, ___result, method) (( RectOffset_t371 * (*) (DOGetter_1_t1026 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23606_gshared)(__this, ___result, method)
