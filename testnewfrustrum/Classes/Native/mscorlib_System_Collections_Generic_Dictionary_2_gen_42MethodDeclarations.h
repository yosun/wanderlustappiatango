﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3861;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t4391;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t3862;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ValueCollection_t3866;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IDictionary_2_t4392;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t4393;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct IEnumerator_1_t4394;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__37.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Dictionary_2__ctor_m26317_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m26317(__this, method) (( void (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2__ctor_m26317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26319_gshared (Dictionary_2_t3861 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26319(__this, ___comparer, method) (( void (*) (Dictionary_2_t3861 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26319_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m26321_gshared (Dictionary_2_t3861 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m26321(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3861 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26321_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m26323_gshared (Dictionary_2_t3861 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m26323(__this, ___capacity, method) (( void (*) (Dictionary_2_t3861 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m26323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26325_gshared (Dictionary_2_t3861 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26325(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3861 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26325_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m26327_gshared (Dictionary_2_t3861 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m26327(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3861 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m26327_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26329_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26329(__this, method) (( Object_t* (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26329_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26331_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26331(__this, method) (( Object_t* (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26331_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m26333_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26333(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m26333_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26335_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26335(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m26335_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26337_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m26337(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m26337_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m26339_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m26339(__this, ___key, method) (( bool (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m26339_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26341_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m26341(__this, ___key, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m26341_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26343_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26343(__this, method) (( bool (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26343_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26345_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26345(__this, method) (( Object_t * (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26345_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26347_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26347(__this, method) (( bool (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26347_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26349_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2_t3836  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26349(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3861 *, KeyValuePair_2_t3836 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26349_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26351_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2_t3836  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26351(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3861 *, KeyValuePair_2_t3836 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26351_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26353_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2U5BU5D_t4393* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26353(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3861 *, KeyValuePair_2U5BU5D_t4393*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26353_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26355_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2_t3836  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26355(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3861 *, KeyValuePair_2_t3836 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26355_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26357_gshared (Dictionary_2_t3861 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26357(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3861 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m26357_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26359_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26359(__this, method) (( Object_t * (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26359_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26361_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26361(__this, method) (( Object_t* (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26361_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26363_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26363(__this, method) (( Object_t * (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26363_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m26365_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m26365(__this, method) (( int32_t (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_get_Count_m26365_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(TKey)
extern "C" KeyValuePair_2_t3254  Dictionary_2_get_Item_m26367_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m26367(__this, ___key, method) (( KeyValuePair_2_t3254  (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m26367_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m26369_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m26369(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_set_Item_m26369_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m26371_gshared (Dictionary_2_t3861 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m26371(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3861 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m26371_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m26373_gshared (Dictionary_2_t3861 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m26373(__this, ___size, method) (( void (*) (Dictionary_2_t3861 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m26373_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m26375_gshared (Dictionary_2_t3861 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m26375(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3861 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m26375_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3836  Dictionary_2_make_pair_m26377_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m26377(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3836  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_make_pair_m26377_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m26379_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m26379(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_pick_key_m26379_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_value(TKey,TValue)
extern "C" KeyValuePair_2_t3254  Dictionary_2_pick_value_m26381_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m26381(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3254  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_pick_value_m26381_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m26383_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2U5BU5D_t4393* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m26383(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3861 *, KeyValuePair_2U5BU5D_t4393*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m26383_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Resize()
extern "C" void Dictionary_2_Resize_m26385_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m26385(__this, method) (( void (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_Resize_m26385_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m26387_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m26387(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_Add_m26387_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Dictionary_2_Clear_m26389_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m26389(__this, method) (( void (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_Clear_m26389_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m26391_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m26391(__this, ___key, method) (( bool (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m26391_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m26393_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m26393(__this, ___value, method) (( bool (*) (Dictionary_2_t3861 *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_ContainsValue_m26393_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m26395_gshared (Dictionary_2_t3861 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m26395(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3861 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m26395_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m26397_gshared (Dictionary_2_t3861 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m26397(__this, ___sender, method) (( void (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m26397_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m26399_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m26399(__this, ___key, method) (( bool (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m26399_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m26401_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, KeyValuePair_2_t3254 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m26401(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3861 *, Object_t *, KeyValuePair_2_t3254 *, const MethodInfo*))Dictionary_2_TryGetValue_m26401_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Keys()
extern "C" KeyCollection_t3862 * Dictionary_2_get_Keys_m26403_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m26403(__this, method) (( KeyCollection_t3862 * (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_get_Keys_m26403_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Values()
extern "C" ValueCollection_t3866 * Dictionary_2_get_Values_m26405_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m26405(__this, method) (( ValueCollection_t3866 * (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_get_Values_m26405_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m26407_gshared (Dictionary_2_t3861 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m26407(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m26407_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTValue(System.Object)
extern "C" KeyValuePair_2_t3254  Dictionary_2_ToTValue_m26409_gshared (Dictionary_2_t3861 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m26409(__this, ___value, method) (( KeyValuePair_2_t3254  (*) (Dictionary_2_t3861 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m26409_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m26411_gshared (Dictionary_2_t3861 * __this, KeyValuePair_2_t3836  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m26411(__this, ___pair, method) (( bool (*) (Dictionary_2_t3861 *, KeyValuePair_2_t3836 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m26411_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t3864  Dictionary_2_GetEnumerator_m26413_gshared (Dictionary_2_t3861 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m26413(__this, method) (( Enumerator_t3864  (*) (Dictionary_2_t3861 *, const MethodInfo*))Dictionary_2_GetEnumerator_m26413_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m26415_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3254  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m26415(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3254 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m26415_gshared)(__this /* static, unused */, ___key, ___value, method)
