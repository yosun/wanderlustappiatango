﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t1361;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t2460  : public Decoder_t2181
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t1361 * ___encoding_2;
};
