﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t1048;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23963_gshared (DOSetter_1_t1048 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23963(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1048 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23963_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23964_gshared (DOSetter_1_t1048 * __this, Vector4_t413  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23964(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1048 *, Vector4_t413 , const MethodInfo*))DOSetter_1_Invoke_m23964_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23965_gshared (DOSetter_1_t1048 * __this, Vector4_t413  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23965(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1048 *, Vector4_t413 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23965_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23966_gshared (DOSetter_1_t1048 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23966(__this, ___result, method) (( void (*) (DOSetter_1_t1048 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23966_gshared)(__this, ___result, method)
