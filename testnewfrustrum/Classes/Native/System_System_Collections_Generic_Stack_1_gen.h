﻿#pragma once
#include <stdint.h>
// DG.Tweening.Tween[]
struct TweenU5BU5D_t978;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct  Stack_1_t979  : public Object_t
{
	// T[] System.Collections.Generic.Stack`1<DG.Tweening.Tween>::_array
	TweenU5BU5D_t978* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1<DG.Tweening.Tween>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1<DG.Tweening.Tween>::_version
	int32_t ____version_3;
};
