﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct KeyValuePair_2_t3847;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t1376;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26137(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3847 *, Type_t *, Object_t*, const MethodInfo*))KeyValuePair_2__ctor_m16908_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m26138(__this, method) (( Type_t * (*) (KeyValuePair_2_t3847 *, const MethodInfo*))KeyValuePair_2_get_Key_m16909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26139(__this, ___value, method) (( void (*) (KeyValuePair_2_t3847 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m16910_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m26140(__this, method) (( Object_t* (*) (KeyValuePair_2_t3847 *, const MethodInfo*))KeyValuePair_2_get_Value_m16911_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26141(__this, ___value, method) (( void (*) (KeyValuePair_2_t3847 *, Object_t*, const MethodInfo*))KeyValuePair_2_set_Value_m16912_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m26142(__this, method) (( String_t* (*) (KeyValuePair_2_t3847 *, const MethodInfo*))KeyValuePair_2_ToString_m16913_gshared)(__this, method)
