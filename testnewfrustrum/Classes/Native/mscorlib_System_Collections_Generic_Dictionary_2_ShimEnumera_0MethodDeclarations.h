﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t3238;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3224;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16648_gshared (ShimEnumerator_t3238 * __this, Dictionary_2_t3224 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16648(__this, ___host, method) (( void (*) (ShimEnumerator_t3238 *, Dictionary_2_t3224 *, const MethodInfo*))ShimEnumerator__ctor_m16648_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16649_gshared (ShimEnumerator_t3238 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16649(__this, method) (( bool (*) (ShimEnumerator_t3238 *, const MethodInfo*))ShimEnumerator_MoveNext_m16649_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m16650_gshared (ShimEnumerator_t3238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16650(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3238 *, const MethodInfo*))ShimEnumerator_get_Entry_m16650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16651_gshared (ShimEnumerator_t3238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16651(__this, method) (( Object_t * (*) (ShimEnumerator_t3238 *, const MethodInfo*))ShimEnumerator_get_Key_m16651_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16652_gshared (ShimEnumerator_t3238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16652(__this, method) (( Object_t * (*) (ShimEnumerator_t3238 *, const MethodInfo*))ShimEnumerator_get_Value_m16652_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16653_gshared (ShimEnumerator_t3238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16653(__this, method) (( Object_t * (*) (ShimEnumerator_t3238 *, const MethodInfo*))ShimEnumerator_get_Current_m16653_gshared)(__this, method)
