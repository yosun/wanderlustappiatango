﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t3679;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback_1__ctor_m23599_gshared (TweenCallback_1_t3679 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define TweenCallback_1__ctor_m23599(__this, ___object, ___method, method) (( void (*) (TweenCallback_1_t3679 *, Object_t *, IntPtr_t, const MethodInfo*))TweenCallback_1__ctor_m23599_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C" void TweenCallback_1_Invoke_m23600_gshared (TweenCallback_1_t3679 * __this, Object_t * ___value, const MethodInfo* method);
#define TweenCallback_1_Invoke_m23600(__this, ___value, method) (( void (*) (TweenCallback_1_t3679 *, Object_t *, const MethodInfo*))TweenCallback_1_Invoke_m23600_gshared)(__this, ___value, method)
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_1_BeginInvoke_m23601_gshared (TweenCallback_1_t3679 * __this, Object_t * ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define TweenCallback_1_BeginInvoke_m23601(__this, ___value, ___callback, ___object, method) (( Object_t * (*) (TweenCallback_1_t3679 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))TweenCallback_1_BeginInvoke_m23601_gshared)(__this, ___value, ___callback, ___object, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_1_EndInvoke_m23602_gshared (TweenCallback_1_t3679 * __this, Object_t * ___result, const MethodInfo* method);
#define TweenCallback_1_EndInvoke_m23602(__this, ___result, method) (( void (*) (TweenCallback_1_t3679 *, Object_t *, const MethodInfo*))TweenCallback_1_EndInvoke_m23602_gshared)(__this, ___result, method)
