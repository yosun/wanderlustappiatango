﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldLoop
struct WorldLoop_t25;

// System.Void WorldLoop::.ctor()
extern "C" void WorldLoop__ctor_m95 (WorldLoop_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Awake()
extern "C" void WorldLoop_Awake_m96 (WorldLoop_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Start()
extern "C" void WorldLoop_Start_m97 (WorldLoop_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Update()
extern "C" void WorldLoop_Update_m98 (WorldLoop_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
