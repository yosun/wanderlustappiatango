﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EventArgs
struct EventArgs_t1688;

// System.Void System.EventArgs::.ctor()
extern "C" void EventArgs__ctor_m13581 (EventArgs_t1688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.cctor()
extern "C" void EventArgs__cctor_m13582 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
