﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t348;

// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern "C" void ToggleEvent__ctor_m1684 (ToggleEvent_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
