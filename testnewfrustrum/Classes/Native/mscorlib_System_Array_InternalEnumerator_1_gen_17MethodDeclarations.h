﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t3220;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16468_gshared (InternalEnumerator_1_t3220 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16468(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3220 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16468_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16469_gshared (InternalEnumerator_1_t3220 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16469(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3220 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16469_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16470_gshared (InternalEnumerator_1_t3220 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16470(__this, method) (( void (*) (InternalEnumerator_1_t3220 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16470_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16471_gshared (InternalEnumerator_1_t3220 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16471(__this, method) (( bool (*) (InternalEnumerator_1_t3220 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16471_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t432  InternalEnumerator_1_get_Current_m16472_gshared (InternalEnumerator_1_t3220 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16472(__this, method) (( RaycastHit2D_t432  (*) (InternalEnumerator_1_t3220 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16472_gshared)(__this, method)
