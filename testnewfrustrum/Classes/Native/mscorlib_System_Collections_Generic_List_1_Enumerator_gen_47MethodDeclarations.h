﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
struct Enumerator_t3451;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t589;
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t668;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19917(__this, ___l, method) (( void (*) (Enumerator_t3451 *, List_1_t668 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19918(__this, method) (( Object_t * (*) (Enumerator_t3451 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::Dispose()
#define Enumerator_Dispose_m19919(__this, method) (( void (*) (Enumerator_t3451 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::VerifyState()
#define Enumerator_VerifyState_m19920(__this, method) (( void (*) (Enumerator_t3451 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::MoveNext()
#define Enumerator_MoveNext_m19921(__this, method) (( bool (*) (Enumerator_t3451 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::get_Current()
#define Enumerator_get_Current_m19922(__this, method) (( Object_t * (*) (Enumerator_t3451 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
