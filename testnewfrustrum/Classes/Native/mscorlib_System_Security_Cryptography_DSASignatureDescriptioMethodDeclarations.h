﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDescription
struct DSASignatureDescription_t2428;

// System.Void System.Security.Cryptography.DSASignatureDescription::.ctor()
extern "C" void DSASignatureDescription__ctor_m12777 (DSASignatureDescription_t2428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
