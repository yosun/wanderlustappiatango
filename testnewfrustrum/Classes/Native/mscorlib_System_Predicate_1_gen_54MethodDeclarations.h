﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<System.Char>
struct Predicate_1_t3699;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.UInt16>
#include "mscorlib_System_Predicate_1_gen_53MethodDeclarations.h"
#define Predicate_1__ctor_m23939(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3699 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m23893_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
#define Predicate_1_Invoke_m23940(__this, ___obj, method) (( bool (*) (Predicate_1_t3699 *, uint16_t, const MethodInfo*))Predicate_1_Invoke_m23894_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23941(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3699 *, uint16_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m23895_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23942(__this, ___result, method) (( bool (*) (Predicate_1_t3699 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m23896_gshared)(__this, ___result, method)
