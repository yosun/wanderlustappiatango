﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int64>
struct InternalEnumerator_1_t3779;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25257_gshared (InternalEnumerator_1_t3779 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25257(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3779 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25257_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25258_gshared (InternalEnumerator_1_t3779 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25258(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3779 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25258_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25259_gshared (InternalEnumerator_1_t3779 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25259(__this, method) (( void (*) (InternalEnumerator_1_t3779 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25259_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25260_gshared (InternalEnumerator_1_t3779 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25260(__this, method) (( bool (*) (InternalEnumerator_1_t3779 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25260_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m25261_gshared (InternalEnumerator_1_t3779 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25261(__this, method) (( int64_t (*) (InternalEnumerator_1_t3779 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25261_gshared)(__this, method)
