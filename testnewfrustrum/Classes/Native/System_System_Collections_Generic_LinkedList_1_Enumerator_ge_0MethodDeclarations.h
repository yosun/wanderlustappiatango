﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
struct Enumerator_t3928;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3927;

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m26996_gshared (Enumerator_t3928 * __this, LinkedList_1_t3927 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m26996(__this, ___parent, method) (( void (*) (Enumerator_t3928 *, LinkedList_1_t3927 *, const MethodInfo*))Enumerator__ctor_m26996_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26997_gshared (Enumerator_t3928 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26997(__this, method) (( Object_t * (*) (Enumerator_t3928 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26997_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m26998_gshared (Enumerator_t3928 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26998(__this, method) (( Object_t * (*) (Enumerator_t3928 *, const MethodInfo*))Enumerator_get_Current_m26998_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26999_gshared (Enumerator_t3928 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26999(__this, method) (( bool (*) (Enumerator_t3928 *, const MethodInfo*))Enumerator_MoveNext_m26999_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m27000_gshared (Enumerator_t3928 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27000(__this, method) (( void (*) (Enumerator_t3928 *, const MethodInfo*))Enumerator_Dispose_m27000_gshared)(__this, method)
