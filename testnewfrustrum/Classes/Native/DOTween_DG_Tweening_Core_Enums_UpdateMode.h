﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateMode
struct  UpdateMode_t929 
{
	// System.Int32 DG.Tweening.Core.Enums.UpdateMode::value__
	int32_t ___value___1;
};
