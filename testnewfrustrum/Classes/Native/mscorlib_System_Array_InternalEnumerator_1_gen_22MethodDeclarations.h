﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t3308;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17703(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3308 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17704(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3308 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14883_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
#define InternalEnumerator_1_Dispose_m17705(__this, method) (( void (*) (InternalEnumerator_1_t3308 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14884_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17706(__this, method) (( bool (*) (InternalEnumerator_1_t3308 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14885_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
#define InternalEnumerator_1_get_Current_m17707(__this, method) (( int32_t (*) (InternalEnumerator_1_t3308 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14886_gshared)(__this, method)
