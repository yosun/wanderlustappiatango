﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t110;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleMode.h"
// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t1003 
{
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t110* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t1003_marshaled
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	char* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
