﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t528;

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18147_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18147(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18147_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18148_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18148(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18148_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18149_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18149(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18149_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18150_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18150(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18150_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18151_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18151(__this, method) (( Object_t* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18151_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern "C" bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18152_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18152(__this, method) (( bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18152_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18153_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18153(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3343 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18153_gshared)(__this, method)
