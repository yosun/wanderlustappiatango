﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t356;
// UnityEngine.RectTransform
struct RectTransform_t272;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern "C" void AspectRatioFitter__ctor_m1715 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern "C" int32_t AspectRatioFitter_get_aspectMode_m1716 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern "C" void AspectRatioFitter_set_aspectMode_m1717 (AspectRatioFitter_t356 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern "C" float AspectRatioFitter_get_aspectRatio_m1718 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern "C" void AspectRatioFitter_set_aspectRatio_m1719 (AspectRatioFitter_t356 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern "C" RectTransform_t272 * AspectRatioFitter_get_rectTransform_m1720 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern "C" void AspectRatioFitter_OnEnable_m1721 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern "C" void AspectRatioFitter_OnDisable_m1722 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m1723 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern "C" void AspectRatioFitter_UpdateRect_m1724 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern "C" float AspectRatioFitter_GetSizeDeltaToProduceSize_m1725 (AspectRatioFitter_t356 * __this, float ___size, int32_t ___axis, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern "C" Vector2_t10  AspectRatioFitter_GetParentSize_m1726 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m1727 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern "C" void AspectRatioFitter_SetLayoutVertical_m1728 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern "C" void AspectRatioFitter_SetDirty_m1729 (AspectRatioFitter_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
