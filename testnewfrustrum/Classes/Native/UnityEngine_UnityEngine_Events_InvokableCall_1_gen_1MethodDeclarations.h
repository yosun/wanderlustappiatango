﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t3312;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t465;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m17729_gshared (InvokableCall_1_t3312 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m17729(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3312 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m17729_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m17730_gshared (InvokableCall_1_t3312 * __this, UnityAction_1_t465 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m17730(__this, ___callback, method) (( void (*) (InvokableCall_1_t3312 *, UnityAction_1_t465 *, const MethodInfo*))InvokableCall_1__ctor_m17730_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m17731_gshared (InvokableCall_1_t3312 * __this, ObjectU5BU5D_t115* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m17731(__this, ___args, method) (( void (*) (InvokableCall_1_t3312 *, ObjectU5BU5D_t115*, const MethodInfo*))InvokableCall_1_Invoke_m17731_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m17732_gshared (InvokableCall_1_t3312 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m17732(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3312 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m17732_gshared)(__this, ___targetObj, ___method, method)
