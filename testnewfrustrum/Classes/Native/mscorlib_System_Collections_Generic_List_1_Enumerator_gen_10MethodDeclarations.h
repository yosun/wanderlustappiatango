﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
struct Enumerator_t840;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t695;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t689;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20448(__this, ___l, method) (( void (*) (Enumerator_t840 *, List_1_t689 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20449(__this, method) (( Object_t * (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::Dispose()
#define Enumerator_Dispose_m20450(__this, method) (( void (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::VerifyState()
#define Enumerator_VerifyState_m20451(__this, method) (( void (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::MoveNext()
#define Enumerator_MoveNext_m4485(__this, method) (( bool (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::get_Current()
#define Enumerator_get_Current_m4484(__this, method) (( WordResult_t695 * (*) (Enumerator_t840 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
