﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$4
struct U24ArrayTypeU244_t1798;
struct U24ArrayTypeU244_t1798_marshaled;

void U24ArrayTypeU244_t1798_marshal(const U24ArrayTypeU244_t1798& unmarshaled, U24ArrayTypeU244_t1798_marshaled& marshaled);
void U24ArrayTypeU244_t1798_marshal_back(const U24ArrayTypeU244_t1798_marshaled& marshaled, U24ArrayTypeU244_t1798& unmarshaled);
void U24ArrayTypeU244_t1798_marshal_cleanup(U24ArrayTypeU244_t1798_marshaled& marshaled);
