﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>
struct ResponseDelegate_1_t1400;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void ResponseDelegate_1__ctor_m25832_gshared (ResponseDelegate_1_t1400 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define ResponseDelegate_1__ctor_m25832(__this, ___object, ___method, method) (( void (*) (ResponseDelegate_1_t1400 *, Object_t *, IntPtr_t, const MethodInfo*))ResponseDelegate_1__ctor_m25832_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>::Invoke(T)
extern "C" void ResponseDelegate_1_Invoke_m25834_gshared (ResponseDelegate_1_t1400 * __this, Object_t * ___response, const MethodInfo* method);
#define ResponseDelegate_1_Invoke_m25834(__this, ___response, method) (( void (*) (ResponseDelegate_1_t1400 *, Object_t *, const MethodInfo*))ResponseDelegate_1_Invoke_m25834_gshared)(__this, ___response, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * ResponseDelegate_1_BeginInvoke_m25836_gshared (ResponseDelegate_1_t1400 * __this, Object_t * ___response, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define ResponseDelegate_1_BeginInvoke_m25836(__this, ___response, ___callback, ___object, method) (( Object_t * (*) (ResponseDelegate_1_t1400 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))ResponseDelegate_1_BeginInvoke_m25836_gshared)(__this, ___response, ___callback, ___object, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void ResponseDelegate_1_EndInvoke_m25838_gshared (ResponseDelegate_1_t1400 * __this, Object_t * ___result, const MethodInfo* method);
#define ResponseDelegate_1_EndInvoke_m25838(__this, ___result, method) (( void (*) (ResponseDelegate_1_t1400 *, Object_t *, const MethodInfo*))ResponseDelegate_1_EndInvoke_m25838_gshared)(__this, ___result, method)
