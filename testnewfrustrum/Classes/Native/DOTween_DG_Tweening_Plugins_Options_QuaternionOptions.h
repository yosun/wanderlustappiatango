﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// DG.Tweening.Plugins.Options.QuaternionOptions
struct  QuaternionOptions_t971 
{
	// DG.Tweening.RotateMode DG.Tweening.Plugins.Options.QuaternionOptions::rotateMode
	int32_t ___rotateMode_0;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.QuaternionOptions::axisConstraint
	int32_t ___axisConstraint_1;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::up
	Vector3_t15  ___up_2;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t971_marshaled
{
	int32_t ___rotateMode_0;
	int32_t ___axisConstraint_1;
	Vector3_t15  ___up_2;
};
