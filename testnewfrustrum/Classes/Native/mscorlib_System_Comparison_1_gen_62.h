﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1342;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.PersistentCall>
struct  Comparison_1_t3911  : public MulticastDelegate_t307
{
};
