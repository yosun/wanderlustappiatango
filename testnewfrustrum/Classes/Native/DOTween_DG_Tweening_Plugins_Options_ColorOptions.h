﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t1011 
{
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1011_marshaled
{
	int32_t ___alphaOnly_0;
};
