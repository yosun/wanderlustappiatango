﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t996;
struct FloatOptions_t996_marshaled;

void FloatOptions_t996_marshal(const FloatOptions_t996& unmarshaled, FloatOptions_t996_marshaled& marshaled);
void FloatOptions_t996_marshal_back(const FloatOptions_t996_marshaled& marshaled, FloatOptions_t996& unmarshaled);
void FloatOptions_t996_marshal_cleanup(FloatOptions_t996_marshaled& marshaled);
