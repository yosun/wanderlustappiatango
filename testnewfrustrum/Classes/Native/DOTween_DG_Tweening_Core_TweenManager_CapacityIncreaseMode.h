﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
struct  CapacityIncreaseMode_t977 
{
	// System.Int32 DG.Tweening.Core.TweenManager/CapacityIncreaseMode::value__
	int32_t ___value___1;
};
