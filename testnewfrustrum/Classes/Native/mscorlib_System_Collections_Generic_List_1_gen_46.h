﻿#pragma once
#include <stdint.h>
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t3663;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct  List_1_t988  : public Object_t
{
	// T[] System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::_items
	TweenCallbackU5BU5D_t3663* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::_version
	int32_t ____version_3;
};
struct List_1_t988_StaticFields{
	// T[] System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::EmptyArray
	TweenCallbackU5BU5D_t3663* ___EmptyArray_4;
};
