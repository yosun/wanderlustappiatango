﻿#pragma once
#include <stdint.h>
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t3632;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct  List_1_t746  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_items
	IVideoBackgroundEventHandlerU5BU5D_t3632* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t746_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::EmptyArray
	IVideoBackgroundEventHandlerU5BU5D_t3632* ___EmptyArray_4;
};
