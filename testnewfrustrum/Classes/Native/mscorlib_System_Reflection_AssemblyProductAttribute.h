﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t483  : public Attribute_t138
{
	// System.String System.Reflection.AssemblyProductAttribute::name
	String_t* ___name_0;
};
