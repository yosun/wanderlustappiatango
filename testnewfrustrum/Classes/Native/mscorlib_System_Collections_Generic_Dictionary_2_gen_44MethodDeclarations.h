﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3931;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t4417;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Byte>
struct KeyCollection_t3934;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Byte>
struct ValueCollection_t3938;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,System.Byte>
struct IDictionary_2_t4418;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct KeyValuePair_2U5BU5D_t4419;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>
struct IEnumerator_1_t4420;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_44.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__40.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor()
extern "C" void Dictionary_2__ctor_m27002_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m27002(__this, method) (( void (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2__ctor_m27002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27003_gshared (Dictionary_2_t3931 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27003(__this, ___comparer, method) (( void (*) (Dictionary_2_t3931 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27003_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m27005_gshared (Dictionary_2_t3931 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m27005(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3931 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27005_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m27007_gshared (Dictionary_2_t3931 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m27007(__this, ___capacity, method) (( void (*) (Dictionary_2_t3931 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m27007_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27009_gshared (Dictionary_2_t3931 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27009(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3931 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27009_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m27011_gshared (Dictionary_2_t3931 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m27011(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3931 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m27011_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27013_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27013(__this, method) (( Object_t* (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27013_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27015_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27015(__this, method) (( Object_t* (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m27017_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m27017(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m27017_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27019_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m27019(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m27019_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27021_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m27021(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m27021_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m27023_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m27023(__this, ___key, method) (( bool (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m27023_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27025_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m27025(__this, ___key, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m27025_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27027_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27027(__this, method) (( bool (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27029_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27029(__this, method) (( Object_t * (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27031_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27031(__this, method) (( bool (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27033_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2_t3932  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27033(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3931 *, KeyValuePair_2_t3932 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27033_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27035_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2_t3932  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27035(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3931 *, KeyValuePair_2_t3932 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27035_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27037_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2U5BU5D_t4419* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27037(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3931 *, KeyValuePair_2U5BU5D_t4419*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27037_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27039_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2_t3932  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27039(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3931 *, KeyValuePair_2_t3932 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27039_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27041_gshared (Dictionary_2_t3931 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m27041(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3931 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m27041_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27043_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27043(__this, method) (( Object_t * (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27043_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27045_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27045(__this, method) (( Object_t* (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27045_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27047_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27047(__this, method) (( Object_t * (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m27049_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m27049(__this, method) (( int32_t (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_get_Count_m27049_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Item(TKey)
extern "C" uint8_t Dictionary_2_get_Item_m27051_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m27051(__this, ___key, method) (( uint8_t (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m27051_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m27053_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m27053(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_set_Item_m27053_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m27055_gshared (Dictionary_2_t3931 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m27055(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3931 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m27055_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m27057_gshared (Dictionary_2_t3931 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m27057(__this, ___size, method) (( void (*) (Dictionary_2_t3931 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m27057_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m27059_gshared (Dictionary_2_t3931 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m27059(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3931 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m27059_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3932  Dictionary_2_make_pair_m27061_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m27061(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3932  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_make_pair_m27061_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m27063_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m27063(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_pick_key_m27063_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_value(TKey,TValue)
extern "C" uint8_t Dictionary_2_pick_value_m27065_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m27065(__this /* static, unused */, ___key, ___value, method) (( uint8_t (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_pick_value_m27065_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m27067_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2U5BU5D_t4419* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m27067(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3931 *, KeyValuePair_2U5BU5D_t4419*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m27067_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Resize()
extern "C" void Dictionary_2_Resize_m27069_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m27069(__this, method) (( void (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_Resize_m27069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m27071_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m27071(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_Add_m27071_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Clear()
extern "C" void Dictionary_2_Clear_m27073_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m27073(__this, method) (( void (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_Clear_m27073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m27075_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m27075(__this, ___key, method) (( bool (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m27075_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m27077_gshared (Dictionary_2_t3931 * __this, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m27077(__this, ___value, method) (( bool (*) (Dictionary_2_t3931 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsValue_m27077_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m27079_gshared (Dictionary_2_t3931 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m27079(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3931 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m27079_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m27081_gshared (Dictionary_2_t3931 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m27081(__this, ___sender, method) (( void (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m27081_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m27083_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m27083(__this, ___key, method) (( bool (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m27083_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m27085_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, uint8_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m27085(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3931 *, Object_t *, uint8_t*, const MethodInfo*))Dictionary_2_TryGetValue_m27085_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Keys()
extern "C" KeyCollection_t3934 * Dictionary_2_get_Keys_m27087_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m27087(__this, method) (( KeyCollection_t3934 * (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_get_Keys_m27087_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Values()
extern "C" ValueCollection_t3938 * Dictionary_2_get_Values_m27089_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m27089(__this, method) (( ValueCollection_t3938 * (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_get_Values_m27089_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m27091_gshared (Dictionary_2_t3931 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m27091(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m27091_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTValue(System.Object)
extern "C" uint8_t Dictionary_2_ToTValue_m27093_gshared (Dictionary_2_t3931 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m27093(__this, ___value, method) (( uint8_t (*) (Dictionary_2_t3931 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m27093_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m27095_gshared (Dictionary_2_t3931 * __this, KeyValuePair_2_t3932  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m27095(__this, ___pair, method) (( bool (*) (Dictionary_2_t3931 *, KeyValuePair_2_t3932 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m27095_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetEnumerator()
extern "C" Enumerator_t3936  Dictionary_2_GetEnumerator_m27097_gshared (Dictionary_2_t3931 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m27097(__this, method) (( Enumerator_t3936  (*) (Dictionary_2_t3931 *, const MethodInfo*))Dictionary_2_GetEnumerator_m27097_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m27099_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m27099(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m27099_gshared)(__this /* static, unused */, ___key, ___value, method)
