﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t600;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t60;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t86;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t77;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t62;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t36;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t93;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t75;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t64;

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t60 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2828 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t86 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2829 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t77 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2830 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t50 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2831 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t58 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2832 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t62 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2833 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t36 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2834 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t93 * NullBehaviourComponentFactory_AddWordBehaviour_m2835 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t75 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2836 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t64 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2837 (NullBehaviourComponentFactory_t600 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m2838 (NullBehaviourComponentFactory_t600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
