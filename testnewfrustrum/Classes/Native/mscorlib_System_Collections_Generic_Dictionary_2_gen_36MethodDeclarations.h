﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3603;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<Vuforia.WebCamProfile/ProfileData>
struct ICollection_1_t4252;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3607;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3611;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct IDictionary_2_t4256;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4257;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t4258;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__26.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void Dictionary_2__ctor_m22428_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m22428(__this, method) (( void (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2__ctor_m22428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22430_gshared (Dictionary_2_t3603 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22430(__this, ___comparer, method) (( void (*) (Dictionary_2_t3603 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22430_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m22432_gshared (Dictionary_2_t3603 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m22432(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3603 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22432_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22434_gshared (Dictionary_2_t3603 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22434(__this, ___capacity, method) (( void (*) (Dictionary_2_t3603 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22434_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22436_gshared (Dictionary_2_t3603 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22436(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3603 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22436_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22438_gshared (Dictionary_2_t3603 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22438(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3603 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m22438_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440(__this, method) (( Object_t* (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22440_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442(__this, method) (( Object_t* (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22442_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22444_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22444(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22444_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22446_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22446(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22446_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22448_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22448(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22448_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22450_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22450(__this, ___key, method) (( bool (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22450_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22452_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22452(__this, ___key, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22452_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454(__this, method) (( bool (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22454_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456(__this, method) (( Object_t * (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458(__this, method) (( bool (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2_t3604  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3603 *, KeyValuePair_2_t3604 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22460_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2_t3604  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3603 *, KeyValuePair_2_t3604 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22462_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2U5BU5D_t4257* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3603 *, KeyValuePair_2U5BU5D_t4257*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22464_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2_t3604  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3603 *, KeyValuePair_2_t3604 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22466_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22468_gshared (Dictionary_2_t3603 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22468(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3603 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22468_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470(__this, method) (( Object_t * (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22470_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472(__this, method) (( Object_t* (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22472_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474(__this, method) (( Object_t * (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22474_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22476_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22476(__this, method) (( int32_t (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_get_Count_m22476_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
extern "C" ProfileData_t734  Dictionary_2_get_Item_m22478_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22478(__this, ___key, method) (( ProfileData_t734  (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m22478_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22480_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22480(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_set_Item_m22480_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22482_gshared (Dictionary_2_t3603 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22482(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3603 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22482_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22484_gshared (Dictionary_2_t3603 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22484(__this, ___size, method) (( void (*) (Dictionary_2_t3603 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22484_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22486_gshared (Dictionary_2_t3603 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22486(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3603 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22486_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3604  Dictionary_2_make_pair_m22488_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22488(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3604  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_make_pair_m22488_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m22490_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22490(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_pick_key_m22490_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
extern "C" ProfileData_t734  Dictionary_2_pick_value_m22492_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22492(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t734  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_pick_value_m22492_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22494_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2U5BU5D_t4257* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22494(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3603 *, KeyValuePair_2U5BU5D_t4257*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22494_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Resize()
extern "C" void Dictionary_2_Resize_m22496_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22496(__this, method) (( void (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_Resize_m22496_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22498_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22498(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_Add_m22498_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Clear()
extern "C" void Dictionary_2_Clear_m22500_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22500(__this, method) (( void (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_Clear_m22500_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22502_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22502(__this, ___key, method) (( bool (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m22502_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22504_gshared (Dictionary_2_t3603 * __this, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22504(__this, ___value, method) (( bool (*) (Dictionary_2_t3603 *, ProfileData_t734 , const MethodInfo*))Dictionary_2_ContainsValue_m22504_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22506_gshared (Dictionary_2_t3603 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22506(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3603 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m22506_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22508_gshared (Dictionary_2_t3603 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22508(__this, ___sender, method) (( void (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22508_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22510_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22510(__this, ___key, method) (( bool (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m22510_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22512_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, ProfileData_t734 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22512(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3603 *, Object_t *, ProfileData_t734 *, const MethodInfo*))Dictionary_2_TryGetValue_m22512_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Keys()
extern "C" KeyCollection_t3607 * Dictionary_2_get_Keys_m22514_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22514(__this, method) (( KeyCollection_t3607 * (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_get_Keys_m22514_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Values()
extern "C" ValueCollection_t3611 * Dictionary_2_get_Values_m22516_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22516(__this, method) (( ValueCollection_t3611 * (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_get_Values_m22516_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m22518_gshared (Dictionary_2_t3603 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22518(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22518_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
extern "C" ProfileData_t734  Dictionary_2_ToTValue_m22520_gshared (Dictionary_2_t3603 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22520(__this, ___value, method) (( ProfileData_t734  (*) (Dictionary_2_t3603 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22520_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22522_gshared (Dictionary_2_t3603 * __this, KeyValuePair_2_t3604  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22522(__this, ___pair, method) (( bool (*) (Dictionary_2_t3603 *, KeyValuePair_2_t3604 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22522_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3609  Dictionary_2_GetEnumerator_m22524_gshared (Dictionary_2_t3603 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22524(__this, method) (( Enumerator_t3609  (*) (Dictionary_2_t3603 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22524_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m22526_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t734  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22526(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t734 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22526_gshared)(__this /* static, unused */, ___key, ___value, method)
