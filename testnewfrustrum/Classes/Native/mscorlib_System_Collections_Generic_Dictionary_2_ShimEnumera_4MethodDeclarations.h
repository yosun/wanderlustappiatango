﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ShimEnumerator_t3576;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22068_gshared (ShimEnumerator_t3576 * __this, Dictionary_2_t870 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22068(__this, ___host, method) (( void (*) (ShimEnumerator_t3576 *, Dictionary_2_t870 *, const MethodInfo*))ShimEnumerator__ctor_m22068_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22069_gshared (ShimEnumerator_t3576 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22069(__this, method) (( bool (*) (ShimEnumerator_t3576 *, const MethodInfo*))ShimEnumerator_MoveNext_m22069_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m22070_gshared (ShimEnumerator_t3576 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22070(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3576 *, const MethodInfo*))ShimEnumerator_get_Entry_m22070_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22071_gshared (ShimEnumerator_t3576 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22071(__this, method) (( Object_t * (*) (ShimEnumerator_t3576 *, const MethodInfo*))ShimEnumerator_get_Key_m22071_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22072_gshared (ShimEnumerator_t3576 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22072(__this, method) (( Object_t * (*) (ShimEnumerator_t3576 *, const MethodInfo*))ShimEnumerator_get_Value_m22072_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22073_gshared (ShimEnumerator_t3576 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22073(__this, method) (( Object_t * (*) (ShimEnumerator_t3576 *, const MethodInfo*))ShimEnumerator_get_Current_m22073_gshared)(__this, method)
