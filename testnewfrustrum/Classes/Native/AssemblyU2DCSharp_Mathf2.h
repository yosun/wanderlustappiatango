﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Mathf2
struct  Mathf2_t17  : public Object_t
{
};
struct Mathf2_t17_StaticFields{
	// UnityEngine.Vector3 Mathf2::FarFarAway
	Vector3_t15  ___FarFarAway_0;
};
