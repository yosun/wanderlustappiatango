﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Check
struct Check_t1579;
// System.Object
struct Object_t;

// System.Void System.Linq.Check::Source(System.Object)
extern "C" void Check_Source_m7259 (Object_t * __this /* static, unused */, Object_t * ___source, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern "C" void Check_SourceAndPredicate_m7260 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___predicate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
