﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct Enumerator_t3202;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3196;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16259_gshared (Enumerator_t3202 * __this, Dictionary_2_t3196 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16259(__this, ___dictionary, method) (( void (*) (Enumerator_t3202 *, Dictionary_2_t3196 *, const MethodInfo*))Enumerator__ctor_m16259_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16260(__this, method) (( Object_t * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262(__this, method) (( Object_t * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263(__this, method) (( Object_t * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16264_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16264(__this, method) (( bool (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_MoveNext_m16264_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3197  Enumerator_get_Current_m16265_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16265(__this, method) (( KeyValuePair_2_t3197  (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_get_Current_m16265_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m16266_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16266(__this, method) (( int32_t (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_get_CurrentKey_m16266_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m16267_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16267(__this, method) (( Object_t * (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_get_CurrentValue_m16267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m16268_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16268(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_VerifyState_m16268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16269_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16269(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_VerifyCurrent_m16269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16270_gshared (Enumerator_t3202 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16270(__this, method) (( void (*) (Enumerator_t3202 *, const MethodInfo*))Enumerator_Dispose_m16270_gshared)(__this, method)
