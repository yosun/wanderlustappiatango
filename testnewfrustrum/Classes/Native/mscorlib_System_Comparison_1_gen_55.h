﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1174;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.GUILayoutEntry>
struct  Comparison_1_t3728  : public MulticastDelegate_t307
{
};
