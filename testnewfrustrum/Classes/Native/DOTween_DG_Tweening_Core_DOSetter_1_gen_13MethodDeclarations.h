﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t3681;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23607_gshared (DOSetter_1_t3681 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23607(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t3681 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23607_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23608_gshared (DOSetter_1_t3681 * __this, Object_t * ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23608(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t3681 *, Object_t *, const MethodInfo*))DOSetter_1_Invoke_m23608_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23609_gshared (DOSetter_1_t3681 * __this, Object_t * ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23609(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t3681 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23609_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23610_gshared (DOSetter_1_t3681 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23610(__this, ___result, method) (( void (*) (DOSetter_1_t3681 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23610_gshared)(__this, ___result, method)
