﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t56;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t776;

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m3951 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m3952 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m3953 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m3954 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m3955 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m3956 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3957 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m3958 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m3959 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m3960 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
extern "C" bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m3961 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
extern "C" void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m3962 (KeepAliveAbstractBehaviour_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
extern "C" KeepAliveAbstractBehaviour_t56 * KeepAliveAbstractBehaviour_get_Instance_m3963 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" void KeepAliveAbstractBehaviour_RegisterEventHandler_m3964 (KeepAliveAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
extern "C" bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m3965 (KeepAliveAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
extern "C" void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m3966 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
extern "C" void KeepAliveAbstractBehaviour__ctor_m447 (KeepAliveAbstractBehaviour_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
