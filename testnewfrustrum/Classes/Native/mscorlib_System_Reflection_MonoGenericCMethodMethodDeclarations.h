﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoGenericCMethod
struct MonoGenericCMethod_t2253;
// System.Type
struct Type_t;

// System.Void System.Reflection.MonoGenericCMethod::.ctor()
extern "C" void MonoGenericCMethod__ctor_m11932 (MonoGenericCMethod_t2253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericCMethod::get_ReflectedType()
extern "C" Type_t * MonoGenericCMethod_get_ReflectedType_m11933 (MonoGenericCMethod_t2253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
