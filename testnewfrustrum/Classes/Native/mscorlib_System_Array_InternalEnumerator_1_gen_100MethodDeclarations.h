﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>
struct InternalEnumerator_1_t3974;
// System.Object
struct Object_t;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t2073;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m27448(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3974 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27449(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>::Dispose()
#define InternalEnumerator_1_Dispose_m27450(__this, method) (( void (*) (InternalEnumerator_1_t3974 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>::MoveNext()
#define InternalEnumerator_1_MoveNext_m27451(__this, method) (( bool (*) (InternalEnumerator_1_t3974 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>::get_Current()
#define InternalEnumerator_1_get_Current_m27452(__this, method) (( Contraction_t2073 * (*) (InternalEnumerator_1_t3974 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
