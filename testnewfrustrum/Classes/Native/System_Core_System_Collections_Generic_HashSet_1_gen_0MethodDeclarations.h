﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3654;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t528;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m23141_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m23141(__this, method) (( void (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1__ctor_m23141_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m23143_gshared (HashSet_1_t3654 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m23143(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3654 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))HashSet_1__ctor_m23143_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23145_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23145(__this, method) (( Object_t* (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23145_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23147_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23147(__this, method) (( bool (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23147_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23149_gshared (HashSet_1_t3654 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23149(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3654 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23149_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23151_gshared (HashSet_1_t3654 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23151(__this, ___item, method) (( void (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23151_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23153_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23153(__this, method) (( Object_t * (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23153_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m23155_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m23155(__this, method) (( int32_t (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_get_Count_m23155_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m23157_gshared (HashSet_1_t3654 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m23157(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t3654 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m23157_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m23159_gshared (HashSet_1_t3654 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m23159(__this, ___size, method) (( void (*) (HashSet_1_t3654 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m23159_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m23161_gshared (HashSet_1_t3654 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m23161(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t3654 *, int32_t, int32_t, Object_t *, const MethodInfo*))HashSet_1_SlotsContainsAt_m23161_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m23163_gshared (HashSet_1_t3654 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m23163(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3654 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m23163_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m23165_gshared (HashSet_1_t3654 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m23165(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t3654 *, ObjectU5BU5D_t115*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m23165_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m23167_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m23167(__this, method) (( void (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_Resize_m23167_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m23169_gshared (HashSet_1_t3654 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m23169(__this, ___index, method) (( int32_t (*) (HashSet_1_t3654 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m23169_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m23171_gshared (HashSet_1_t3654 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m23171(__this, ___item, method) (( int32_t (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_GetItemHashCode_m23171_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m23172_gshared (HashSet_1_t3654 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Add_m23172(__this, ___item, method) (( bool (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_Add_m23172_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m23174_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m23174(__this, method) (( void (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_Clear_m23174_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m23176_gshared (HashSet_1_t3654 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Contains_m23176(__this, ___item, method) (( bool (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_Contains_m23176_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m23178_gshared (HashSet_1_t3654 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Remove_m23178(__this, ___item, method) (( bool (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_Remove_m23178_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m23180_gshared (HashSet_1_t3654 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m23180(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3654 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))HashSet_1_GetObjectData_m23180_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m23182_gshared (HashSet_1_t3654 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m23182(__this, ___sender, method) (( void (*) (HashSet_1_t3654 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m23182_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3656  HashSet_1_GetEnumerator_m23183_gshared (HashSet_1_t3654 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m23183(__this, method) (( Enumerator_t3656  (*) (HashSet_1_t3654 *, const MethodInfo*))HashSet_1_GetEnumerator_m23183_gshared)(__this, method)
