﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.IntPlugin
struct IntPlugin_t986;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1040;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t1041;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t1042;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_Reset_m5442 (IntPlugin_t986 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern "C" int32_t IntPlugin_ConvertToStartValue_m5443 (IntPlugin_t986 * __this, TweenerCore_3_t1040 * ___t, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetRelativeEndValue_m5444 (IntPlugin_t986 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetChangeValue_m5445 (IntPlugin_t986 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern "C" float IntPlugin_GetSpeedBasedDuration_m5446 (IntPlugin_t986 * __this, NoOptions_t933  ___options, float ___unitsXSecond, int32_t ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void IntPlugin_EvaluateAndApply_m5447 (IntPlugin_t986 * __this, NoOptions_t933  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1041 * ___getter, DOSetter_1_t1042 * ___setter, float ___elapsed, int32_t ___startValue, int32_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern "C" void IntPlugin__ctor_m5448 (IntPlugin_t986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
