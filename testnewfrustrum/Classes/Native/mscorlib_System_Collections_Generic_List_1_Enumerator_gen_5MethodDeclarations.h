﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
struct Enumerator_t814;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t739;
// System.Collections.Generic.List`1<Vuforia.Marker>
struct List_1_t813;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19757(__this, ___l, method) (( void (*) (Enumerator_t814 *, List_1_t813 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19758(__this, method) (( Object_t * (*) (Enumerator_t814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m19759(__this, method) (( void (*) (Enumerator_t814 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::VerifyState()
#define Enumerator_VerifyState_m19760(__this, method) (( void (*) (Enumerator_t814 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m4398(__this, method) (( bool (*) (Enumerator_t814 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m4397(__this, method) (( Object_t * (*) (Enumerator_t814 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
