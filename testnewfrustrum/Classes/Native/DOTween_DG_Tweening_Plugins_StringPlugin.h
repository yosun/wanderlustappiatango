﻿#pragma once
#include <stdint.h>
// System.Text.StringBuilder
struct StringBuilder_t423;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t989;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_9.h"
// DG.Tweening.Plugins.StringPlugin
struct  StringPlugin_t990  : public ABSTweenPlugin_3_t991
{
};
struct StringPlugin_t990_StaticFields{
	// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::_Buffer
	StringBuilder_t423 * ____Buffer_0;
	// System.Collections.Generic.List`1<System.Char> DG.Tweening.Plugins.StringPlugin::_OpenedTags
	List_1_t989 * ____OpenedTags_1;
};
