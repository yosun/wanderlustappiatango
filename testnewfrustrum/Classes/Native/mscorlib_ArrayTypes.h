﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Object[]
// System.Object[]
struct  ObjectU5BU5D_t115  : public Array_t
{
};
// System.String[]
// System.String[]
struct  StringU5BU5D_t109  : public Array_t
{
};
struct StringU5BU5D_t109_StaticFields{
};
// System.IConvertible[]
// System.IConvertible[]
struct  IConvertibleU5BU5D_t4433  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct  IComparableU5BU5D_t4434  : public Array_t
{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct  IEnumerableU5BU5D_t4435  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct  ICloneableU5BU5D_t4436  : public Array_t
{
};
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct  IComparable_1U5BU5D_t4437  : public Array_t
{
};
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct  IEquatable_1U5BU5D_t4438  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct  CharU5BU5D_t110  : public Array_t
{
};
struct CharU5BU5D_t110_StaticFields{
};
// System.UInt16[]
// System.UInt16[]
struct  UInt16U5BU5D_t1060  : public Array_t
{
};
// System.IFormattable[]
// System.IFormattable[]
struct  IFormattableU5BU5D_t4439  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct  IComparable_1U5BU5D_t4440  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct  IEquatable_1U5BU5D_t4441  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct  ValueTypeU5BU5D_t4442  : public Array_t
{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct  IComparable_1U5BU5D_t4443  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct  IEquatable_1U5BU5D_t4444  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct  Int32U5BU5D_t19  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct  IComparable_1U5BU5D_t4445  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct  IEquatable_1U5BU5D_t4446  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct  TypeU5BU5D_t878  : public Array_t
{
};
struct TypeU5BU5D_t878_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct  IReflectU5BU5D_t4447  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct  _TypeU5BU5D_t4448  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct  MemberInfoU5BU5D_t2365  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct  ICustomAttributeProviderU5BU5D_t4449  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct  _MemberInfoU5BU5D_t4450  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct  DoubleU5BU5D_t2587  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct  IComparable_1U5BU5D_t4451  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct  IEquatable_1U5BU5D_t4452  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct  MethodInfoU5BU5D_t141  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct  List_1U5BU5D_t3149  : public Array_t
{
};
struct List_1U5BU5D_t3149_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4056  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct  LinkU5BU5D_t3191  : public Array_t
{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct  DictionaryEntryU5BU5D_t4453  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct  KeyValuePair_2U5BU5D_t4053  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4066  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3859  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct  List_1U5BU5D_t3243  : public Array_t
{
};
struct List_1U5BU5D_t3243_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct  KeyValuePair_2U5BU5D_t4078  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct  List_1U5BU5D_t3273  : public Array_t
{
};
struct List_1U5BU5D_t3273_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct  KeyValuePair_2U5BU5D_t4100  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4103  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4073  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct  EnumU5BU5D_t4454  : public Array_t
{
};
struct EnumU5BU5D_t4454_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct  List_1U5BU5D_t3352  : public Array_t
{
};
struct List_1U5BU5D_t3352_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct  List_1U5BU5D_t3354  : public Array_t
{
};
struct List_1U5BU5D_t3354_StaticFields{
};
// System.Single[]
// System.Single[]
struct  SingleU5BU5D_t585  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct  IComparable_1U5BU5D_t4455  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct  IEquatable_1U5BU5D_t4456  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct  KeyValuePair_2U5BU5D_t4144  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct  KeyValuePair_2U5BU5D_t4152  : public Array_t
{
};
// System.Byte[]
// System.Byte[]
struct  ByteU5BU5D_t616  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct  IComparable_1U5BU5D_t4457  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct  IEquatable_1U5BU5D_t4458  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct  KeyValuePair_2U5BU5D_t4157  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct  KeyValuePair_2U5BU5D_t4166  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t4178  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t4175  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct  KeyValuePair_2U5BU5D_t4185  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4192  : public Array_t
{
};
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct  List_1U5BU5D_t3500  : public Array_t
{
};
struct List_1U5BU5D_t3500_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct  KeyValuePair_2U5BU5D_t4197  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct  KeyValuePair_2U5BU5D_t4210  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4215  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct  KeyValuePair_2U5BU5D_t4221  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4226  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4231  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
struct  KeyValuePair_2U5BU5D_t4236  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
struct  KeyValuePair_2U5BU5D_t4241  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct  KeyValuePair_2U5BU5D_t4248  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t4257  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t4254  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4263  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct  KeyValuePair_2U5BU5D_t4299  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct  KeyValuePair_2U5BU5D_t4309  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4314  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct  KeyValuePair_2U5BU5D_t4318  : public Array_t
{
};
// System.Byte[][]
// System.Byte[][]
struct  ByteU5BU5DU5BU5D_t1822  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct  IntPtrU5BU5D_t1362  : public Array_t
{
};
struct IntPtrU5BU5D_t1362_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct  ISerializableU5BU5D_t4459  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t4338  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct  Int64U5BU5D_t2586  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct  IComparable_1U5BU5D_t4460  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct  IEquatable_1U5BU5D_t4461  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t4335  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4356  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct  UInt64U5BU5D_t2422  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct  IComparable_1U5BU5D_t4462  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct  IEquatable_1U5BU5D_t4463  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
struct  KeyValuePair_2U5BU5D_t4352  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct  KeyValuePair_2U5BU5D_t1373  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct  KeyValuePair_2U5BU5D_t4393  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct  KeyValuePair_2U5BU5D_t4366  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  IDictionary_2U5BU5D_t3845  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t4372  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  IDictionary_2U5BU5D_t3849  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
struct  KeyValuePair_2U5BU5D_t4378  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  KeyValuePair_2U5BU5D_t4384  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
struct  KeyValuePair_2U5BU5D_t3857  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t4389  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct  ConstructorInfoU5BU5D_t1424  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t1424_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct  _ConstructorInfoU5BU5D_t4464  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct  MethodBaseU5BU5D_t2591  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct  _MethodBaseU5BU5D_t4465  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct  ParameterInfoU5BU5D_t1425  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct  _ParameterInfoU5BU5D_t4466  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct  PropertyInfoU5BU5D_t1428  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct  _PropertyInfoU5BU5D_t4467  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct  FieldInfoU5BU5D_t1429  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct  _FieldInfoU5BU5D_t4468  : public Array_t
{
};
// System.Attribute[]
// System.Attribute[]
struct  AttributeU5BU5D_t4469  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct  _AttributeU5BU5D_t4470  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct  ParameterModifierU5BU5D_t1432  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t4401  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct  UInt32U5BU5D_t1662  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct  IComparable_1U5BU5D_t4471  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct  IEquatable_1U5BU5D_t4472  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct  KeySizesU5BU5D_t1686  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct  X509CertificateU5BU5D_t1835  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct  IDeserializationCallbackU5BU5D_t4473  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct  KeyValuePair_2U5BU5D_t4419  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct  BooleanU5BU5D_t1882  : public Array_t
{
};
struct BooleanU5BU5D_t1882_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t4415  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4426  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct  SingleU5BU2CU5D_t4474  : public Array_t
{
};
// System.Delegate[]
// System.Delegate[]
struct  DelegateU5BU5D_t2585  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct  Int16U5BU5D_t2601  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct  IComparable_1U5BU5D_t4475  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct  IEquatable_1U5BU5D_t4476  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct  SByteU5BU5D_t2465  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct  IComparable_1U5BU5D_t4477  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct  IEquatable_1U5BU5D_t4478  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct  TableRangeU5BU5D_t2070  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct  TailoringInfoU5BU5D_t2077  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct  ContractionU5BU5D_t2086  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct  Level2MapU5BU5D_t2087  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t2588  : public Array_t
{
};
struct BigIntegerU5BU5D_t2588_StaticFields{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct  SlotU5BU5D_t2152  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct  SlotU5BU5D_t2156  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct  StackFrameU5BU5D_t2164  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct  CalendarU5BU5D_t2172  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct  ModuleBuilderU5BU5D_t2214  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t2214_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct  _ModuleBuilderU5BU5D_t4479  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct  ModuleU5BU5D_t2590  : public Array_t
{
};
struct ModuleU5BU5D_t2590_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct  _ModuleU5BU5D_t4480  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct  ParameterBuilderU5BU5D_t2217  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct  _ParameterBuilderU5BU5D_t4481  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct  GenericTypeParameterBuilderU5BU5D_t2224  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct  MethodBuilderU5BU5D_t2229  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct  _MethodBuilderU5BU5D_t4482  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct  _MethodInfoU5BU5D_t4483  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct  ConstructorBuilderU5BU5D_t2230  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct  _ConstructorBuilderU5BU5D_t4484  : public Array_t
{
};
// System.Reflection.Emit.PropertyBuilder[]
// System.Reflection.Emit.PropertyBuilder[]
struct  PropertyBuilderU5BU5D_t2231  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyBuilder[]
// System.Runtime.InteropServices._PropertyBuilder[]
struct  _PropertyBuilderU5BU5D_t4485  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct  FieldBuilderU5BU5D_t2232  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct  _FieldBuilderU5BU5D_t4486  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct  HeaderU5BU5D_t2560  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct  ITrackingHandlerU5BU5D_t2609  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct  IContextAttributeU5BU5D_t2597  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct  DateTimeU5BU5D_t2611  : public Array_t
{
};
struct DateTimeU5BU5D_t2611_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct  IComparable_1U5BU5D_t4487  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct  IEquatable_1U5BU5D_t4488  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct  DecimalU5BU5D_t2612  : public Array_t
{
};
struct DecimalU5BU5D_t2612_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct  IComparable_1U5BU5D_t4489  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct  IEquatable_1U5BU5D_t4490  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct  TimeSpanU5BU5D_t2613  : public Array_t
{
};
struct TimeSpanU5BU5D_t2613_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct  IComparable_1U5BU5D_t4491  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct  IEquatable_1U5BU5D_t4492  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct  TypeTagU5BU5D_t2614  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct  MonoTypeU5BU5D_t2616  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct  ByteU5BU2CU5D_t2396  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct  StrongNameU5BU5D_t4002  : public Array_t
{
};
