﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t693;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1371;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ICollection_1_t4195;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t692;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyCollection_t831;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ValueCollection_t830;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3087;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct IDictionary_2_t4196;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t4197;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>>
struct IEnumerator_1_t4198;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_34MethodDeclarations.h"
#define Dictionary_2__ctor_m4496(__this, method) (( void (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2__ctor_m16760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20726(__this, ___comparer, method) (( void (*) (Dictionary_2_t693 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16762_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m20727(__this, ___dictionary, method) (( void (*) (Dictionary_2_t693 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16764_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20728(__this, ___capacity, method) (( void (*) (Dictionary_2_t693 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16766_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20729(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t693 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16768_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20730(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t693 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m16770_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20731(__this, method) (( Object_t* (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16772_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20732(__this, method) (( Object_t* (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16774_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20733(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16776_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20734(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t693 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16778_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20735(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t693 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16780_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m20736(__this, ___key, method) (( bool (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16782_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20737(__this, ___key, method) (( void (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16784_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20738(__this, method) (( bool (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16786_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20739(__this, method) (( Object_t * (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16788_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20740(__this, method) (( bool (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16790_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20741(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t693 *, KeyValuePair_2_t3502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16792_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20742(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t693 *, KeyValuePair_2_t3502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16794_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20743(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t693 *, KeyValuePair_2U5BU5D_t4197*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16796_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20744(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t693 *, KeyValuePair_2_t3502 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16798_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20745(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t693 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16800_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20746(__this, method) (( Object_t * (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16802_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20747(__this, method) (( Object_t* (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16804_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20748(__this, method) (( Object_t * (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16806_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Count()
#define Dictionary_2_get_Count_m20749(__this, method) (( int32_t (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_get_Count_m16808_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Item(TKey)
#define Dictionary_2_get_Item_m20750(__this, ___key, method) (( List_1_t692 * (*) (Dictionary_2_t693 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m16810_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20751(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t693 *, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_set_Item_m16812_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20752(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t693 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16814_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20753(__this, ___size, method) (( void (*) (Dictionary_2_t693 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16816_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20754(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t693 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16818_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20755(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3502  (*) (Object_t * /* static, unused */, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_make_pair_m16820_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20756(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_pick_key_m16822_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20757(__this /* static, unused */, ___key, ___value, method) (( List_1_t692 * (*) (Object_t * /* static, unused */, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_pick_value_m16824_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20758(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t693 *, KeyValuePair_2U5BU5D_t4197*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16826_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Resize()
#define Dictionary_2_Resize_m20759(__this, method) (( void (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_Resize_m16828_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Add(TKey,TValue)
#define Dictionary_2_Add_m20760(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t693 *, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_Add_m16830_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Clear()
#define Dictionary_2_Clear_m20761(__this, method) (( void (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_Clear_m16832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20762(__this, ___key, method) (( bool (*) (Dictionary_2_t693 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m16834_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20763(__this, ___value, method) (( bool (*) (Dictionary_2_t693 *, List_1_t692 *, const MethodInfo*))Dictionary_2_ContainsValue_m16836_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20764(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t693 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m16838_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20765(__this, ___sender, method) (( void (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16840_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Remove(TKey)
#define Dictionary_2_Remove_m20766(__this, ___key, method) (( bool (*) (Dictionary_2_t693 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m16842_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20767(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t693 *, String_t*, List_1_t692 **, const MethodInfo*))Dictionary_2_TryGetValue_m16844_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Keys()
#define Dictionary_2_get_Keys_m4468(__this, method) (( KeyCollection_t831 * (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_get_Keys_m16846_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Values()
#define Dictionary_2_get_Values_m4463(__this, method) (( ValueCollection_t830 * (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_get_Values_m16848_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20768(__this, ___key, method) (( String_t* (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16850_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20769(__this, ___value, method) (( List_1_t692 * (*) (Dictionary_2_t693 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16852_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20770(__this, ___pair, method) (( bool (*) (Dictionary_2_t693 *, KeyValuePair_2_t3502 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16854_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20771(__this, method) (( Enumerator_t3503  (*) (Dictionary_2_t693 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16856_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20772(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, String_t*, List_1_t692 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16858_gshared)(__this /* static, unused */, ___key, ___value, method)
