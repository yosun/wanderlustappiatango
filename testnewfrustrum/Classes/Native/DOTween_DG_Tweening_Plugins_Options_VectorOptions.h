﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t1002 
{
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1002_marshaled
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
