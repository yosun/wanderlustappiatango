﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t708;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Collections.Generic.ICollection`1<Vuforia.Surface>
struct ICollection_1_t4208;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t98;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.Surface>
struct KeyCollection_t863;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Surface>
struct ValueCollection_t848;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3193;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.Surface>
struct IDictionary_2_t4209;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct KeyValuePair_2U5BU5D_t4210;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>
struct IEnumerator_1_t4211;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_32MethodDeclarations.h"
#define Dictionary_2__ctor_m4548(__this, method) (( void (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2__ctor_m16127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21105(__this, ___comparer, method) (( void (*) (Dictionary_2_t708 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16129_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m21106(__this, ___dictionary, method) (( void (*) (Dictionary_2_t708 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16131_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor(System.Int32)
#define Dictionary_2__ctor_m21107(__this, ___capacity, method) (( void (*) (Dictionary_2_t708 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16133_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21108(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t708 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16135_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m21109(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t708 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m16137_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21110(__this, method) (( Object_t* (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16139_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21111(__this, method) (( Object_t* (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16141_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21112(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16143_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21113(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t708 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16145_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m21114(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t708 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16147_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m21115(__this, ___key, method) (( bool (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16149_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m21116(__this, ___key, method) (( void (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16151_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21117(__this, method) (( bool (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21118(__this, method) (( Object_t * (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16155_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21119(__this, method) (( bool (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21120(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t708 *, KeyValuePair_2_t855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16159_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21121(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t708 *, KeyValuePair_2_t855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16161_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21122(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t708 *, KeyValuePair_2U5BU5D_t4210*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16163_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21123(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t708 *, KeyValuePair_2_t855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16165_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21124(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t708 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16167_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21125(__this, method) (( Object_t * (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16169_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21126(__this, method) (( Object_t* (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16171_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21127(__this, method) (( Object_t * (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16173_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::get_Count()
#define Dictionary_2_get_Count_m21128(__this, method) (( int32_t (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_get_Count_m16175_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::get_Item(TKey)
#define Dictionary_2_get_Item_m21129(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t708 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16177_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m21130(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t708 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m16179_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m21131(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t708 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16181_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m21132(__this, ___size, method) (( void (*) (Dictionary_2_t708 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16183_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m21133(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t708 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16185_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m21134(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t855  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m16187_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m21135(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m16189_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m21136(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m16191_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m21137(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t708 *, KeyValuePair_2U5BU5D_t4210*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16193_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::Resize()
#define Dictionary_2_Resize_m21138(__this, method) (( void (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_Resize_m16195_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::Add(TKey,TValue)
#define Dictionary_2_Add_m21139(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t708 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m16197_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::Clear()
#define Dictionary_2_Clear_m21140(__this, method) (( void (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_Clear_m16199_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m21141(__this, ___key, method) (( bool (*) (Dictionary_2_t708 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16201_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m21142(__this, ___value, method) (( bool (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m16203_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m21143(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t708 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m16205_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m21144(__this, ___sender, method) (( void (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16207_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::Remove(TKey)
#define Dictionary_2_Remove_m21145(__this, ___key, method) (( bool (*) (Dictionary_2_t708 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16209_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m21146(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t708 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m16211_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::get_Keys()
#define Dictionary_2_get_Keys_m4543(__this, method) (( KeyCollection_t863 * (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_get_Keys_m16213_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::get_Values()
#define Dictionary_2_get_Values_m4514(__this, method) (( ValueCollection_t848 * (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_get_Values_m16214_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m21147(__this, ___key, method) (( int32_t (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16216_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m21148(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t708 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16218_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m21149(__this, ___pair, method) (( bool (*) (Dictionary_2_t708 *, KeyValuePair_2_t855 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16220_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m4530(__this, method) (( Enumerator_t857  (*) (Dictionary_2_t708 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16221_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m21150(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16223_gshared)(__this /* static, unused */, ___key, ___value, method)
