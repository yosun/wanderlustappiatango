﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Gyro2
struct  Gyro2_t12  : public MonoBehaviour_t7
{
	// UnityEngine.GameObject Gyro2::goUI_Directions_FiveFingers
	GameObject_t2 * ___goUI_Directions_FiveFingers_8;
	// UnityEngine.GameObject Gyro2::goUI_Directions_Swipe
	GameObject_t2 * ___goUI_Directions_Swipe_9;
	// UnityEngine.GameObject Gyro2::goUI_SettingsButtonToggle
	GameObject_t2 * ___goUI_SettingsButtonToggle_10;
};
struct Gyro2_t12_StaticFields{
	// UnityEngine.Transform Gyro2::transform
	Transform_t11 * ___transform_2;
	// System.Boolean Gyro2::gyroEnabled
	bool ___gyroEnabled_3;
	// UnityEngine.Quaternion Gyro2::camBase
	Quaternion_t13  ___camBase_4;
	// UnityEngine.Quaternion Gyro2::calibration
	Quaternion_t13  ___calibration_5;
	// UnityEngine.Quaternion Gyro2::refRot
	Quaternion_t13  ___refRot_6;
	// UnityEngine.Quaternion Gyro2::baseOrientation
	Quaternion_t13  ___baseOrientation_7;
	// System.Single Gyro2::countdown
	float ___countdown_11;
};
