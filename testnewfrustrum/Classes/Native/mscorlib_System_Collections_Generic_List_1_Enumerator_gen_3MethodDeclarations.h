﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
struct Enumerator_t803;
// System.Object
struct Object_t;
// Vuforia.Trackable
struct Trackable_t565;
// System.Collections.Generic.List`1<Vuforia.Trackable>
struct List_1_t802;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19263(__this, ___l, method) (( void (*) (Enumerator_t803 *, List_1_t802 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19264(__this, method) (( Object_t * (*) (Enumerator_t803 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::Dispose()
#define Enumerator_Dispose_m19265(__this, method) (( void (*) (Enumerator_t803 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::VerifyState()
#define Enumerator_VerifyState_m19266(__this, method) (( void (*) (Enumerator_t803 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::MoveNext()
#define Enumerator_MoveNext_m4364(__this, method) (( bool (*) (Enumerator_t803 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::get_Current()
#define Enumerator_get_Current_m4363(__this, method) (( Object_t * (*) (Enumerator_t803 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
