﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ARVRModes
struct ARVRModes_t6;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;

// System.Void ARVRModes::.ctor()
extern "C" void ARVRModes__ctor_m0 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::.cctor()
extern "C" void ARVRModes__cctor_m1 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::Start()
extern "C" void ARVRModes_Start_m2 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::FlipGameObjectArray(UnityEngine.GameObject[],System.Boolean)
extern "C" void ARVRModes_FlipGameObjectArray_m3 (ARVRModes_t6 * __this, GameObjectU5BU5D_t5* ___g, bool ___flip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SeeEntire()
extern "C" void ARVRModes_SeeEntire_m4 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SeeDoor()
extern "C" void ARVRModes_SeeDoor_m5 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchARToVR()
extern "C" void ARVRModes_SwitchARToVR_m6 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchVRToAR()
extern "C" void ARVRModes_SwitchVRToAR_m7 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchModes()
extern "C" void ARVRModes_SwitchModes_m8 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::Update()
extern "C" void ARVRModes_Update_m9 (ARVRModes_t6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
