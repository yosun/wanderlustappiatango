﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t3618;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22627(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, String_t*, ProfileData_t734 , const MethodInfo*))KeyValuePair_2__ctor_m22532_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m22628(__this, method) (( String_t* (*) (KeyValuePair_2_t3618 *, const MethodInfo*))KeyValuePair_2_get_Key_m22533_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22629(__this, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m22534_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m22630(__this, method) (( ProfileData_t734  (*) (KeyValuePair_2_t3618 *, const MethodInfo*))KeyValuePair_2_get_Value_m22535_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22631(__this, ___value, method) (( void (*) (KeyValuePair_2_t3618 *, ProfileData_t734 , const MethodInfo*))KeyValuePair_2_set_Value_m22536_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m22632(__this, method) (( String_t* (*) (KeyValuePair_2_t3618 *, const MethodInfo*))KeyValuePair_2_ToString_m22537_gshared)(__this, method)
