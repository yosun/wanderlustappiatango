﻿#pragma once
#include <stdint.h>
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t2215;
// System.Char[]
struct CharU5BU5D_t110;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// System.Reflection.Emit.ModuleBuilder
struct  ModuleBuilder_t2225  : public Module_t2226
{
	// System.Reflection.Emit.AssemblyBuilder System.Reflection.Emit.ModuleBuilder::assemblyb
	AssemblyBuilder_t2215 * ___assemblyb_10;
};
struct ModuleBuilder_t2225_StaticFields{
	// System.Char[] System.Reflection.Emit.ModuleBuilder::type_modifiers
	CharU5BU5D_t110* ___type_modifiers_11;
};
