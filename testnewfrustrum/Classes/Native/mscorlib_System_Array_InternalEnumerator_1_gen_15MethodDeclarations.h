﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.Link>
struct InternalEnumerator_1_t3199;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16235_gshared (InternalEnumerator_1_t3199 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16235(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3199 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16235_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16236_gshared (InternalEnumerator_1_t3199 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16236(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3199 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16236_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16237_gshared (InternalEnumerator_1_t3199 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16237(__this, method) (( void (*) (InternalEnumerator_1_t3199 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16237_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16238_gshared (InternalEnumerator_1_t3199 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16238(__this, method) (( bool (*) (InternalEnumerator_1_t3199 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16238_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern "C" Link_t2136  InternalEnumerator_1_get_Current_m16239_gshared (InternalEnumerator_1_t3199 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16239(__this, method) (( Link_t2136  (*) (InternalEnumerator_1_t3199 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16239_gshared)(__this, method)
