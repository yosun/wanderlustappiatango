﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1697;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t2399  : public AsymmetricSignatureFormatter_t1758
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t1697 * ___dsa_0;
};
