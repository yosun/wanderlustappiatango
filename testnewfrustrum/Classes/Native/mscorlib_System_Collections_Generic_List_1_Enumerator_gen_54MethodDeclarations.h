﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Char>
struct Enumerator_t3700;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t989;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53MethodDeclarations.h"
#define Enumerator__ctor_m23943(__this, ___l, method) (( void (*) (Enumerator_t3700 *, List_1_t989 *, const MethodInfo*))Enumerator__ctor_m23821_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23944(__this, method) (( Object_t * (*) (Enumerator_t3700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23822_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
#define Enumerator_Dispose_m23945(__this, method) (( void (*) (Enumerator_t3700 *, const MethodInfo*))Enumerator_Dispose_m23823_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
#define Enumerator_VerifyState_m23946(__this, method) (( void (*) (Enumerator_t3700 *, const MethodInfo*))Enumerator_VerifyState_m23824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
#define Enumerator_MoveNext_m23947(__this, method) (( bool (*) (Enumerator_t3700 *, const MethodInfo*))Enumerator_MoveNext_m23825_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
#define Enumerator_get_Current_m23948(__this, method) (( uint16_t (*) (Enumerator_t3700 *, const MethodInfo*))Enumerator_get_Current_m23826_gshared)(__this, method)
