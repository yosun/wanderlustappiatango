﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t3894;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t317;
struct Event_t317_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1336;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_43.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m26692(__this, ___dictionary, method) (( void (*) (Enumerator_t3894 *, Dictionary_2_t1336 *, const MethodInfo*))Enumerator__ctor_m16601_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26693(__this, method) (( Object_t * (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16602_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26694(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16603_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26695(__this, method) (( Object_t * (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16604_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26696(__this, method) (( Object_t * (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16605_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m26697(__this, method) (( bool (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_MoveNext_m16606_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m26698(__this, method) (( KeyValuePair_2_t3891  (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_get_Current_m16607_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m26699(__this, method) (( Event_t317 * (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_get_CurrentKey_m16608_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m26700(__this, method) (( int32_t (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_get_CurrentValue_m16609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m26701(__this, method) (( void (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_VerifyState_m16610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m26702(__this, method) (( void (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_VerifyCurrent_m16611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m26703(__this, method) (( void (*) (Enumerator_t3894 *, const MethodInfo*))Enumerator_Dispose_m16612_gshared)(__this, method)
