﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1029;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m5521_gshared (DOGetter_1_t1029 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m5521(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1029 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m5521_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C" Quaternion_t13  DOGetter_1_Invoke_m14937_gshared (DOGetter_1_t1029 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m14937(__this, method) (( Quaternion_t13  (*) (DOGetter_1_t1029 *, const MethodInfo*))DOGetter_1_Invoke_m14937_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m14938_gshared (DOGetter_1_t1029 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m14938(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1029 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m14938_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C" Quaternion_t13  DOGetter_1_EndInvoke_m14939_gshared (DOGetter_1_t1029 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m14939(__this, ___result, method) (( Quaternion_t13  (*) (DOGetter_1_t1029 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m14939_gshared)(__this, ___result, method)
