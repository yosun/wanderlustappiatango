﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallback
struct DecoderFallback_t2446;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2447;

// System.Void System.Text.DecoderFallback::.ctor()
extern "C" void DecoderFallback__ctor_m12894 (DecoderFallback_t2446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallback::.cctor()
extern "C" void DecoderFallback__cctor_m12895 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ExceptionFallback()
extern "C" DecoderFallback_t2446 * DecoderFallback_get_ExceptionFallback_m12896 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ReplacementFallback()
extern "C" DecoderFallback_t2446 * DecoderFallback_get_ReplacementFallback_m12897 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_StandardSafeFallback()
extern "C" DecoderFallback_t2446 * DecoderFallback_get_StandardSafeFallback_m12898 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderFallback::CreateFallbackBuffer()
