﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3239;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct  List_1_t3223  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_items
	ICanvasElementU5BU5D_t3239* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_version
	int32_t ____version_3;
};
struct List_1_t3223_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::EmptyArray
	ICanvasElementU5BU5D_t3239* ___EmptyArray_4;
};
