﻿#pragma once
#include <stdint.h>
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t1296;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1297;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1298;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t1299  : public Object_t
{
};
struct AttributeHelperEngine_t1299_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t1296* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t1297* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t1298* ____requireComponentArray_2;
};
