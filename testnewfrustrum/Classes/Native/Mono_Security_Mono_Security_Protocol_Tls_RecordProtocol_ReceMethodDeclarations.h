﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1752;
// System.IO.Stream
struct Stream_t1751;
// System.Byte[]
struct ByteU5BU5D_t616;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t140;
// System.Threading.WaitHandle
struct WaitHandle_t1802;
// System.AsyncCallback
struct AsyncCallback_t305;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m7903 (ReceiveRecordAsyncResult_t1752 * __this, AsyncCallback_t305 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t616* ___initialBuffer, Stream_t1751 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1751 * ReceiveRecordAsyncResult_get_Record_m7904 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t616* ReceiveRecordAsyncResult_get_ResultingBuffer_m7905 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t616* ReceiveRecordAsyncResult_get_InitialBuffer_m7906 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m7907 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t140 * ReceiveRecordAsyncResult_get_AsyncException_m7908 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m7909 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1802 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m7910 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m7911 (ReceiveRecordAsyncResult_t1752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7912 (ReceiveRecordAsyncResult_t1752 * __this, Exception_t140 * ___ex, ByteU5BU5D_t616* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7913 (ReceiveRecordAsyncResult_t1752 * __this, Exception_t140 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7914 (ReceiveRecordAsyncResult_t1752 * __this, ByteU5BU5D_t616* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
