﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t3115;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t135;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15049_gshared (Enumerator_t3115 * __this, List_1_t135 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15049(__this, ___l, method) (( void (*) (Enumerator_t3115 *, List_1_t135 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared (Enumerator_t3115 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15050(__this, method) (( Object_t * (*) (Enumerator_t3115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15051_gshared (Enumerator_t3115 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15051(__this, method) (( void (*) (Enumerator_t3115 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15052_gshared (Enumerator_t3115 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15052(__this, method) (( void (*) (Enumerator_t3115 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15053_gshared (Enumerator_t3115 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15053(__this, method) (( bool (*) (Enumerator_t3115 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15054_gshared (Enumerator_t3115 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15054(__this, method) (( Object_t * (*) (Enumerator_t3115 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
