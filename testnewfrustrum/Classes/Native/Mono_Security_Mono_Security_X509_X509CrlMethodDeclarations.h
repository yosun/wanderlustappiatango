﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Crl
struct X509Crl_t1705;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1698;
// System.Byte[]
struct ByteU5BU5D_t616;
// System.String
struct String_t;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1704;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1699;
// System.Security.Cryptography.DSA
struct DSA_t1697;
// System.Security.Cryptography.RSA
struct RSA_t1691;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1789;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C" void X509Crl__ctor_m7649 (X509Crl_t1705 * __this, ByteU5BU5D_t616* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C" void X509Crl_Parse_m7650 (X509Crl_t1705 * __this, ByteU5BU5D_t616* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C" X509ExtensionCollection_t1698 * X509Crl_get_Extensions_m7651 (X509Crl_t1705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C" ByteU5BU5D_t616* X509Crl_get_Hash_m7652 (X509Crl_t1705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
extern "C" String_t* X509Crl_get_IssuerName_m7653 (X509Crl_t1705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
extern "C" DateTime_t111  X509Crl_get_NextUpdate_m7654 (X509Crl_t1705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C" bool X509Crl_Compare_m7655 (X509Crl_t1705 * __this, ByteU5BU5D_t616* ___array1, ByteU5BU5D_t616* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
extern "C" X509CrlEntry_t1704 * X509Crl_GetCrlEntry_m7656 (X509Crl_t1705 * __this, X509Certificate_t1699 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C" X509CrlEntry_t1704 * X509Crl_GetCrlEntry_m7657 (X509Crl_t1705 * __this, ByteU5BU5D_t616* ___serialNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C" String_t* X509Crl_GetHashName_m7658 (X509Crl_t1705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C" bool X509Crl_VerifySignature_m7659 (X509Crl_t1705 * __this, DSA_t1697 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C" bool X509Crl_VerifySignature_m7660 (X509Crl_t1705 * __this, RSA_t1691 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool X509Crl_VerifySignature_m7661 (X509Crl_t1705 * __this, AsymmetricAlgorithm_t1789 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
