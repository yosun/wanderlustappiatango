﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_t485  : public Attribute_t138
{
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::Visible
	bool ___Visible_0;
};
