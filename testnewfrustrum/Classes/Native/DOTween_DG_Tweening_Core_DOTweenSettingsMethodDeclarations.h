﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenSettings
struct DOTweenSettings_t954;

// System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern "C" void DOTweenSettings__ctor_m5356 (DOTweenSettings_t954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
