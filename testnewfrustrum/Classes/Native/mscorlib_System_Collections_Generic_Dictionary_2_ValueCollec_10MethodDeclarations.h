﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ValueCollection_t830;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t693;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t692;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct IEnumerator_1_t4200;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct List_1U5BU5D_t3500;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define ValueCollection__ctor_m20797(__this, ___dictionary, method) (( void (*) (ValueCollection_t830 *, Dictionary_2_t693 *, const MethodInfo*))ValueCollection__ctor_m16949_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20798(__this, ___item, method) (( void (*) (ValueCollection_t830 *, List_1_t692 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16950_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20799(__this, method) (( void (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20800(__this, ___item, method) (( bool (*) (ValueCollection_t830 *, List_1_t692 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16952_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20801(__this, ___item, method) (( bool (*) (ValueCollection_t830 *, List_1_t692 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16953_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20802(__this, method) (( Object_t* (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16954_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m20803(__this, ___array, ___index, method) (( void (*) (ValueCollection_t830 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16955_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20804(__this, method) (( Object_t * (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20805(__this, method) (( bool (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16957_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20806(__this, method) (( bool (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16958_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m20807(__this, method) (( Object_t * (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16959_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m20808(__this, ___array, ___index, method) (( void (*) (ValueCollection_t830 *, List_1U5BU5D_t3500*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m16960_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4464(__this, method) (( Enumerator_t829  (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_GetEnumerator_m16961_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Count()
#define ValueCollection_get_Count_m20809(__this, method) (( int32_t (*) (ValueCollection_t830 *, const MethodInfo*))ValueCollection_get_Count_m16962_gshared)(__this, method)
