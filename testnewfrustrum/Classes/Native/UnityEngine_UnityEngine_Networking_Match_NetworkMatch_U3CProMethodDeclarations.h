﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t3834;
// System.Object
struct Object_t;

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25839_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25839(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25839_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25840_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25840(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25840_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25841_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25841(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25841_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25842_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25842(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25842_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25843_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25843(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3834 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25843_gshared)(__this, method)
