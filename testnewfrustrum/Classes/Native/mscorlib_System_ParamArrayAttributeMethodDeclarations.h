﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ParamArrayAttribute
struct ParamArrayAttribute_t502;

// System.Void System.ParamArrayAttribute::.ctor()
extern "C" void ParamArrayAttribute__ctor_m2498 (ParamArrayAttribute_t502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
