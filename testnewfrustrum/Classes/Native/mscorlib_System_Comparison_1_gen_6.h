﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t196;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
struct  Comparison_1_t3138  : public MulticastDelegate_t307
{
};
