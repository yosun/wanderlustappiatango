﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t70;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t68;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t582;

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t68 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2757 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2758 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m2759 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2760 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m2761 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2762 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m464 (ReconstructionFromTargetAbstractBehaviour_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
