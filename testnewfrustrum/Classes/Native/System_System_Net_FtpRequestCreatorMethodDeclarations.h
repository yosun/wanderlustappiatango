﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1871;
// System.Net.WebRequest
struct WebRequest_t1833;
// System.Uri
struct Uri_t1273;

// System.Void System.Net.FtpRequestCreator::.ctor()
extern "C" void FtpRequestCreator__ctor_m8529 (FtpRequestCreator_t1871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1833 * FtpRequestCreator_Create_m8530 (FtpRequestCreator_t1871 * __this, Uri_t1273 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
