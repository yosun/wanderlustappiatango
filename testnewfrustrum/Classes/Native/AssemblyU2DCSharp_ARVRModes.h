﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.Material
struct Material_t4;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
// ARVRModes
struct  ARVRModes_t6  : public MonoBehaviour_t7
{
	// UnityEngine.GameObject ARVRModes::goMaskThese
	GameObject_t2 * ___goMaskThese_2;
	// UnityEngine.GameObject ARVRModes::goARModeOnlyStuff
	GameObject_t2 * ___goARModeOnlyStuff_3;
	// UnityEngine.Camera ARVRModes::camAR
	Camera_t3 * ___camAR_4;
	// UnityEngine.Camera ARVRModes::camVR
	Camera_t3 * ___camVR_5;
	// UnityEngine.GameObject ARVRModes::goVRSet
	GameObject_t2 * ___goVRSet_6;
	// UnityEngine.Material ARVRModes::matTouchMeLipstick
	Material_t4 * ___matTouchMeLipstick_7;
	// UnityEngine.GameObject ARVRModes::goUI_PanelNav
	GameObject_t2 * ___goUI_PanelNav_8;
	// UnityEngine.GameObject[] ARVRModes::goDisableForEntire
	GameObjectU5BU5D_t5* ___goDisableForEntire_9;
	// UnityEngine.GameObject[] ARVRModes::goEnableForEntire
	GameObjectU5BU5D_t5* ___goEnableForEntire_10;
	// UnityEngine.GameObject[] ARVRModes::goDisableForDoor
	GameObjectU5BU5D_t5* ___goDisableForDoor_11;
	// UnityEngine.GameObject[] ARVRModes::goEnableForDoor
	GameObjectU5BU5D_t5* ___goEnableForDoor_12;
};
struct ARVRModes_t6_StaticFields{
	// ARVRModes/TheCurrentMode ARVRModes::tcm
	int32_t ___tcm_13;
};
