﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct InternalEnumerator_1_t3439;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19765_gshared (InternalEnumerator_1_t3439 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19765(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3439 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19765_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19766_gshared (InternalEnumerator_1_t3439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19766(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19766_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19767_gshared (InternalEnumerator_1_t3439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19767(__this, method) (( void (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19767_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19768_gshared (InternalEnumerator_1_t3439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19768(__this, method) (( bool (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19768_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t642  InternalEnumerator_1_get_Current_m19769_gshared (InternalEnumerator_1_t3439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19769(__this, method) (( TrackableResultData_t642  (*) (InternalEnumerator_1_t3439 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19769_gshared)(__this, method)
