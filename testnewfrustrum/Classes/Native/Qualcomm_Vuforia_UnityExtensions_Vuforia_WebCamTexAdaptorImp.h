﻿#pragma once
#include <stdint.h>
// UnityEngine.WebCamTexture
struct WebCamTexture_t682;
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.WebCamTexAdaptorImpl
struct  WebCamTexAdaptorImpl_t683  : public WebCamTexAdaptor_t633
{
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t682 * ___mWebCamTexture_0;
};
