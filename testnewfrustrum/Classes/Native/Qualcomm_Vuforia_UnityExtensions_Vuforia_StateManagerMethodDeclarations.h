﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager
struct StateManager_t713;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t782;
// Vuforia.Trackable
struct Trackable_t565;
// Vuforia.WordManager
struct WordManager_t687;

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetActiveTrackableBehaviours()
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetTrackableBehaviours()
// System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
// Vuforia.WordManager Vuforia.StateManager::GetWordManager()
// System.Void Vuforia.StateManager::.ctor()
extern "C" void StateManager__ctor_m4006 (StateManager_t713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
