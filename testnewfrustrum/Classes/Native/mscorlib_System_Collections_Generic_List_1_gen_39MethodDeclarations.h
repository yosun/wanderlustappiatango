﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t723;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t784;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t796;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t4245;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t3587;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3583;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3591;
// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3594;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_49.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void List_1__ctor_m4576_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1__ctor_m4576(__this, method) (( void (*) (List_1_t723 *, const MethodInfo*))List_1__ctor_m4576_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22174_gshared (List_1_t723 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22174(__this, ___collection, method) (( void (*) (List_1_t723 *, Object_t*, const MethodInfo*))List_1__ctor_m22174_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22175_gshared (List_1_t723 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22175(__this, ___capacity, method) (( void (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1__ctor_m22175_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void List_1__cctor_m22176_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22176(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22176_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22177_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22177(__this, method) (( Object_t* (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22178_gshared (List_1_t723 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22178(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t723 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22178_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22179_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22179(__this, method) (( Object_t * (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22179_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22180_gshared (List_1_t723 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22180(__this, ___item, method) (( int32_t (*) (List_1_t723 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22180_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22181_gshared (List_1_t723 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22181(__this, ___item, method) (( bool (*) (List_1_t723 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22181_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22182_gshared (List_1_t723 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22182(__this, ___item, method) (( int32_t (*) (List_1_t723 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22182_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22183_gshared (List_1_t723 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22183(__this, ___index, ___item, method) (( void (*) (List_1_t723 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22183_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22184_gshared (List_1_t723 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22184(__this, ___item, method) (( void (*) (List_1_t723 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22184_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22185_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22185(__this, method) (( bool (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22185_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22186_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22186(__this, method) (( bool (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22186_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22187_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22187(__this, method) (( Object_t * (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22187_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22188_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22188(__this, method) (( bool (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22189_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22189(__this, method) (( bool (*) (List_1_t723 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22189_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22190_gshared (List_1_t723 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22190(__this, ___index, method) (( Object_t * (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22190_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22191_gshared (List_1_t723 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22191(__this, ___index, ___value, method) (( void (*) (List_1_t723 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22191_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void List_1_Add_m22192_gshared (List_1_t723 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define List_1_Add_m22192(__this, ___item, method) (( void (*) (List_1_t723 *, TargetSearchResult_t720 , const MethodInfo*))List_1_Add_m22192_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22193_gshared (List_1_t723 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22193(__this, ___newCount, method) (( void (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22193_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22194_gshared (List_1_t723 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22194(__this, ___collection, method) (( void (*) (List_1_t723 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22194_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22195_gshared (List_1_t723 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22195(__this, ___enumerable, method) (( void (*) (List_1_t723 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22195_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m22196_gshared (List_1_t723 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m22196(__this, ___collection, method) (( void (*) (List_1_t723 *, Object_t*, const MethodInfo*))List_1_AddRange_m22196_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3587 * List_1_AsReadOnly_m22197_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22197(__this, method) (( ReadOnlyCollection_1_t3587 * (*) (List_1_t723 *, const MethodInfo*))List_1_AsReadOnly_m22197_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void List_1_Clear_m22198_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_Clear_m22198(__this, method) (( void (*) (List_1_t723 *, const MethodInfo*))List_1_Clear_m22198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool List_1_Contains_m22199_gshared (List_1_t723 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define List_1_Contains_m22199(__this, ___item, method) (( bool (*) (List_1_t723 *, TargetSearchResult_t720 , const MethodInfo*))List_1_Contains_m22199_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22200_gshared (List_1_t723 * __this, TargetSearchResultU5BU5D_t3583* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22200(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t723 *, TargetSearchResultU5BU5D_t3583*, int32_t, const MethodInfo*))List_1_CopyTo_m22200_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Find(System.Predicate`1<T>)
extern "C" TargetSearchResult_t720  List_1_Find_m22201_gshared (List_1_t723 * __this, Predicate_1_t3591 * ___match, const MethodInfo* method);
#define List_1_Find_m22201(__this, ___match, method) (( TargetSearchResult_t720  (*) (List_1_t723 *, Predicate_1_t3591 *, const MethodInfo*))List_1_Find_m22201_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22202_gshared (Object_t * __this /* static, unused */, Predicate_1_t3591 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22202(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3591 *, const MethodInfo*))List_1_CheckMatch_m22202_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22203_gshared (List_1_t723 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3591 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22203(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t723 *, int32_t, int32_t, Predicate_1_t3591 *, const MethodInfo*))List_1_GetIndex_m22203_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Enumerator_t3585  List_1_GetEnumerator_m22204_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22204(__this, method) (( Enumerator_t3585  (*) (List_1_t723 *, const MethodInfo*))List_1_GetEnumerator_m22204_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22205_gshared (List_1_t723 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22205(__this, ___item, method) (( int32_t (*) (List_1_t723 *, TargetSearchResult_t720 , const MethodInfo*))List_1_IndexOf_m22205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22206_gshared (List_1_t723 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22206(__this, ___start, ___delta, method) (( void (*) (List_1_t723 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22206_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22207_gshared (List_1_t723 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22207(__this, ___index, method) (( void (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22208_gshared (List_1_t723 * __this, int32_t ___index, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define List_1_Insert_m22208(__this, ___index, ___item, method) (( void (*) (List_1_t723 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))List_1_Insert_m22208_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22209_gshared (List_1_t723 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22209(__this, ___collection, method) (( void (*) (List_1_t723 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22209_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool List_1_Remove_m22210_gshared (List_1_t723 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define List_1_Remove_m22210(__this, ___item, method) (( bool (*) (List_1_t723 *, TargetSearchResult_t720 , const MethodInfo*))List_1_Remove_m22210_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22211_gshared (List_1_t723 * __this, Predicate_1_t3591 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22211(__this, ___match, method) (( int32_t (*) (List_1_t723 *, Predicate_1_t3591 *, const MethodInfo*))List_1_RemoveAll_m22211_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22212_gshared (List_1_t723 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22212(__this, ___index, method) (( void (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Reverse()
extern "C" void List_1_Reverse_m22213_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_Reverse_m22213(__this, method) (( void (*) (List_1_t723 *, const MethodInfo*))List_1_Reverse_m22213_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort()
extern "C" void List_1_Sort_m22214_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_Sort_m22214(__this, method) (( void (*) (List_1_t723 *, const MethodInfo*))List_1_Sort_m22214_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22215_gshared (List_1_t723 * __this, Comparison_1_t3594 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22215(__this, ___comparison, method) (( void (*) (List_1_t723 *, Comparison_1_t3594 *, const MethodInfo*))List_1_Sort_m22215_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C" TargetSearchResultU5BU5D_t3583* List_1_ToArray_m22216_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_ToArray_m22216(__this, method) (( TargetSearchResultU5BU5D_t3583* (*) (List_1_t723 *, const MethodInfo*))List_1_ToArray_m22216_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m22217_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22217(__this, method) (( void (*) (List_1_t723 *, const MethodInfo*))List_1_TrimExcess_m22217_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22218_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22218(__this, method) (( int32_t (*) (List_1_t723 *, const MethodInfo*))List_1_get_Capacity_m22218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22219_gshared (List_1_t723 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22219(__this, ___value, method) (( void (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22219_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t List_1_get_Count_m22220_gshared (List_1_t723 * __this, const MethodInfo* method);
#define List_1_get_Count_m22220(__this, method) (( int32_t (*) (List_1_t723 *, const MethodInfo*))List_1_get_Count_m22220_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t720  List_1_get_Item_m22221_gshared (List_1_t723 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22221(__this, ___index, method) (( TargetSearchResult_t720  (*) (List_1_t723 *, int32_t, const MethodInfo*))List_1_get_Item_m22221_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22222_gshared (List_1_t723 * __this, int32_t ___index, TargetSearchResult_t720  ___value, const MethodInfo* method);
#define List_1_set_Item_m22222(__this, ___index, ___value, method) (( void (*) (List_1_t723 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))List_1_set_Item_m22222_gshared)(__this, ___index, ___value, method)
