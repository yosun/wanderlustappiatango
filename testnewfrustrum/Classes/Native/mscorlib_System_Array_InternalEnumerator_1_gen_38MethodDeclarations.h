﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct InternalEnumerator_1_t3463;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20120_gshared (InternalEnumerator_1_t3463 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20120(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3463 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20120_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_gshared (InternalEnumerator_1_t3463 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3463 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20122_gshared (InternalEnumerator_1_t3463 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20122(__this, method) (( void (*) (InternalEnumerator_1_t3463 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20122_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20123_gshared (InternalEnumerator_1_t3463 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20123(__this, method) (( bool (*) (InternalEnumerator_1_t3463 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20123_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern "C" KeyValuePair_2_t3462  InternalEnumerator_1_get_Current_m20124_gshared (InternalEnumerator_1_t3463 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20124(__this, method) (( KeyValuePair_2_t3462  (*) (InternalEnumerator_1_t3463 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20124_gshared)(__this, method)
