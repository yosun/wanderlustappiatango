﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t3100;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14858_gshared (InternalEnumerator_1_t3100 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14858(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3100 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared (InternalEnumerator_1_t3100 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3100 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14862_gshared (InternalEnumerator_1_t3100 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14862(__this, method) (( void (*) (InternalEnumerator_1_t3100 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14864_gshared (InternalEnumerator_1_t3100 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14864(__this, method) (( bool (*) (InternalEnumerator_1_t3100 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m14866_gshared (InternalEnumerator_1_t3100 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14866(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3100 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
