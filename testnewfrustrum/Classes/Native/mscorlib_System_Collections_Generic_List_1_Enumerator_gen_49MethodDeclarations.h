﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
struct Enumerator_t3585;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t723;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22228_gshared (Enumerator_t3585 * __this, List_1_t723 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22228(__this, ___l, method) (( void (*) (Enumerator_t3585 *, List_1_t723 *, const MethodInfo*))Enumerator__ctor_m22228_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22229_gshared (Enumerator_t3585 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22229(__this, method) (( Object_t * (*) (Enumerator_t3585 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22229_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void Enumerator_Dispose_m22230_gshared (Enumerator_t3585 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22230(__this, method) (( void (*) (Enumerator_t3585 *, const MethodInfo*))Enumerator_Dispose_m22230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m22231_gshared (Enumerator_t3585 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22231(__this, method) (( void (*) (Enumerator_t3585 *, const MethodInfo*))Enumerator_VerifyState_m22231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22232_gshared (Enumerator_t3585 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22232(__this, method) (( bool (*) (Enumerator_t3585 *, const MethodInfo*))Enumerator_MoveNext_m22232_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t720  Enumerator_get_Current_m22233_gshared (Enumerator_t3585 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22233(__this, method) (( TargetSearchResult_t720  (*) (Enumerator_t3585 *, const MethodInfo*))Enumerator_get_Current_m22233_gshared)(__this, method)
