﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.DataSetImpl>
struct Predicate_1_t3421;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t578;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.DataSetImpl>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m19463(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3421 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.DataSetImpl>::Invoke(T)
#define Predicate_1_Invoke_m19464(__this, ___obj, method) (( bool (*) (Predicate_1_t3421 *, DataSetImpl_t578 *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.DataSetImpl>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m19465(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3421 *, DataSetImpl_t578 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.DataSetImpl>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m19466(__this, ___result, method) (( bool (*) (Predicate_1_t3421 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
