﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.Byte>
struct Nullable_1_t3108;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<System.Byte>
#include "mscorlib_System_Nullable_1_gen_2.h"

// System.Void System.Nullable`1<System.Byte>::.ctor(T)
extern "C" void Nullable_1__ctor_m14897_gshared (Nullable_1_t3108 * __this, uint8_t ___value, const MethodInfo* method);
#define Nullable_1__ctor_m14897(__this, ___value, method) (( void (*) (Nullable_1_t3108 *, uint8_t, const MethodInfo*))Nullable_1__ctor_m14897_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Byte>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m14898_gshared (Nullable_1_t3108 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m14898(__this, method) (( bool (*) (Nullable_1_t3108 *, const MethodInfo*))Nullable_1_get_HasValue_m14898_gshared)(__this, method)
// T System.Nullable`1<System.Byte>::get_Value()
extern "C" uint8_t Nullable_1_get_Value_m14899_gshared (Nullable_1_t3108 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m14899(__this, method) (( uint8_t (*) (Nullable_1_t3108 *, const MethodInfo*))Nullable_1_get_Value_m14899_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m14901_gshared (Nullable_1_t3108 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m14901(__this, ___other, method) (( bool (*) (Nullable_1_t3108 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m14901_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m14903_gshared (Nullable_1_t3108 * __this, Nullable_1_t3108  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m14903(__this, ___other, method) (( bool (*) (Nullable_1_t3108 *, Nullable_1_t3108 , const MethodInfo*))Nullable_1_Equals_m14903_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Byte>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m14905_gshared (Nullable_1_t3108 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m14905(__this, method) (( int32_t (*) (Nullable_1_t3108 *, const MethodInfo*))Nullable_1_GetHashCode_m14905_gshared)(__this, method)
// System.String System.Nullable`1<System.Byte>::ToString()
extern "C" String_t* Nullable_1_ToString_m14907_gshared (Nullable_1_t3108 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m14907(__this, method) (( String_t* (*) (Nullable_1_t3108 *, const MethodInfo*))Nullable_1_ToString_m14907_gshared)(__this, method)
