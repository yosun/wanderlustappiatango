﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.UInt16>
struct List_1_t3687;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
struct  Enumerator_t3688 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.UInt16>::l
	List_1_t3687 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.UInt16>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.UInt16>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.UInt16>::current
	uint16_t ___current_3;
};
