﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t84;

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern "C" void VideoTextureRendererAbstractBehaviour_Awake_m4235 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern "C" void VideoTextureRendererAbstractBehaviour_Start_m4236 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern "C" void VideoTextureRendererAbstractBehaviour_Update_m4237 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern "C" void VideoTextureRendererAbstractBehaviour_OnDestroy_m4238 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m724 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern "C" void VideoTextureRendererAbstractBehaviour__ctor_m475 (VideoTextureRendererAbstractBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
