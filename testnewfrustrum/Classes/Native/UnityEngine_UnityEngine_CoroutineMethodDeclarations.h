﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t316;
struct Coroutine_t316_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m5662 (Coroutine_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m5663 (Coroutine_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m5664 (Coroutine_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t316_marshal(const Coroutine_t316& unmarshaled, Coroutine_t316_marshaled& marshaled);
void Coroutine_t316_marshal_back(const Coroutine_t316_marshaled& marshaled, Coroutine_t316& unmarshaled);
void Coroutine_t316_marshal_cleanup(Coroutine_t316_marshaled& marshaled);
