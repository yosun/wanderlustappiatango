﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Basics
struct Basics_t27;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Basics::.ctor()
extern "C" void Basics__ctor_m106 (Basics_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::Start()
extern "C" void Basics_Start_m107 (Basics_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Basics::<Start>m__0()
extern "C" Vector3_t15  Basics_U3CStartU3Em__0_m108 (Basics_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern "C" void Basics_U3CStartU3Em__1_m109 (Basics_t27 * __this, Vector3_t15  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
