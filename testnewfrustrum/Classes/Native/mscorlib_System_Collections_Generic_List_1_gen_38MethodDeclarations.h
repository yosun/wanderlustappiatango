﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>
struct List_1_t871;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t86;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerable_1_t787;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerator_1_t874;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ICollection_1_t4244;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ReadOnlyCollection_1_t3580;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t783;
// System.Predicate`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Predicate_1_t3581;
// System.Comparison`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparison_1_t3582;
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4563(__this, method) (( void (*) (List_1_t871 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4617(__this, ___collection, method) (( void (*) (List_1_t871 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m22082(__this, ___capacity, method) (( void (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.cctor()
#define List_1__cctor_m22083(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22084(__this, method) (( Object_t* (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22085(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t871 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22086(__this, method) (( Object_t * (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22087(__this, ___item, method) (( int32_t (*) (List_1_t871 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22088(__this, ___item, method) (( bool (*) (List_1_t871 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22089(__this, ___item, method) (( int32_t (*) (List_1_t871 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22090(__this, ___index, ___item, method) (( void (*) (List_1_t871 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22091(__this, ___item, method) (( void (*) (List_1_t871 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22092(__this, method) (( bool (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22093(__this, method) (( bool (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22094(__this, method) (( Object_t * (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22095(__this, method) (( bool (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22096(__this, method) (( bool (*) (List_1_t871 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22097(__this, ___index, method) (( Object_t * (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22098(__this, ___index, ___value, method) (( void (*) (List_1_t871 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
#define List_1_Add_m22099(__this, ___item, method) (( void (*) (List_1_t871 *, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22100(__this, ___newCount, method) (( void (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22101(__this, ___collection, method) (( void (*) (List_1_t871 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22102(__this, ___enumerable, method) (( void (*) (List_1_t871 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22103(__this, ___collection, method) (( void (*) (List_1_t871 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m22104(__this, method) (( ReadOnlyCollection_1_t3580 * (*) (List_1_t871 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
#define List_1_Clear_m22105(__this, method) (( void (*) (List_1_t871 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
#define List_1_Contains_m22106(__this, ___item, method) (( bool (*) (List_1_t871 *, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22107(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t871 *, VirtualButtonAbstractBehaviourU5BU5D_t783*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m22108(__this, ___match, method) (( VirtualButtonAbstractBehaviour_t86 * (*) (List_1_t871 *, Predicate_1_t3581 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22109(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3581 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22110(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t871 *, int32_t, int32_t, Predicate_1_t3581 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m4567(__this, method) (( Enumerator_t873  (*) (List_1_t871 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m22111(__this, ___item, method) (( int32_t (*) (List_1_t871 *, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22112(__this, ___start, ___delta, method) (( void (*) (List_1_t871 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22113(__this, ___index, method) (( void (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m22114(__this, ___index, ___item, method) (( void (*) (List_1_t871 *, int32_t, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22115(__this, ___collection, method) (( void (*) (List_1_t871 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
#define List_1_Remove_m22116(__this, ___item, method) (( bool (*) (List_1_t871 *, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22117(__this, ___match, method) (( int32_t (*) (List_1_t871 *, Predicate_1_t3581 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22118(__this, ___index, method) (( void (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Reverse()
#define List_1_Reverse_m22119(__this, method) (( void (*) (List_1_t871 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort()
#define List_1_Sort_m22120(__this, method) (( void (*) (List_1_t871 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22121(__this, ___comparison, method) (( void (*) (List_1_t871 *, Comparison_1_t3582 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::ToArray()
#define List_1_ToArray_m22122(__this, method) (( VirtualButtonAbstractBehaviourU5BU5D_t783* (*) (List_1_t871 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m22123(__this, method) (( void (*) (List_1_t871 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m22124(__this, method) (( int32_t (*) (List_1_t871 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22125(__this, ___value, method) (( void (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define List_1_get_Count_m22126(__this, method) (( int32_t (*) (List_1_t871 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m22127(__this, ___index, method) (( VirtualButtonAbstractBehaviour_t86 * (*) (List_1_t871 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m22128(__this, ___index, ___value, method) (( void (*) (List_1_t871 *, int32_t, VirtualButtonAbstractBehaviour_t86 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
