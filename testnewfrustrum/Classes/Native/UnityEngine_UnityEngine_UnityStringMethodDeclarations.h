﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityString
struct UnityString_t1206;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C" String_t* UnityString_Format_m6196 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t115* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
