﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>
struct Enumerator_t806;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t731;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t621;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m19383(__this, ___host, method) (( void (*) (Enumerator_t806 *, Dictionary_2_t621 *, const MethodInfo*))Enumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19384(__this, method) (( Object_t * (*) (Enumerator_t806 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::Dispose()
#define Enumerator_Dispose_m19385(__this, method) (( void (*) (Enumerator_t806 *, const MethodInfo*))Enumerator_Dispose_m16291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m4381(__this, method) (( bool (*) (Enumerator_t806 *, const MethodInfo*))Enumerator_MoveNext_m16292_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::get_Current()
#define Enumerator_get_Current_m4380(__this, method) (( VirtualButton_t731 * (*) (Enumerator_t806 *, const MethodInfo*))Enumerator_get_Current_m16293_gshared)(__this, method)
