﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>
struct InternalEnumerator_1_t3445;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19821_gshared (InternalEnumerator_1_t3445 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19821(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3445 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19821_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19822_gshared (InternalEnumerator_1_t3445 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19822(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3445 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19822_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19823_gshared (InternalEnumerator_1_t3445 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19823(__this, method) (( void (*) (InternalEnumerator_1_t3445 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19823_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19824_gshared (InternalEnumerator_1_t3445 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19824(__this, method) (( bool (*) (InternalEnumerator_1_t3445 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19824_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::get_Current()
extern "C" PropData_t652  InternalEnumerator_1_get_Current_m19825_gshared (InternalEnumerator_1_t3445 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19825(__this, method) (( PropData_t652  (*) (InternalEnumerator_1_t3445 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19825_gshared)(__this, method)
