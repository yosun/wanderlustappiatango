﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Byte>
struct InternalEnumerator_1_t3411;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19271_gshared (InternalEnumerator_1_t3411 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19271(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3411 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19271_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272_gshared (InternalEnumerator_1_t3411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3411 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19272_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19273_gshared (InternalEnumerator_1_t3411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19273(__this, method) (( void (*) (InternalEnumerator_1_t3411 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19273_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19274_gshared (InternalEnumerator_1_t3411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19274(__this, method) (( bool (*) (InternalEnumerator_1_t3411 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19274_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m19275_gshared (InternalEnumerator_1_t3411 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19275(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3411 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19275_gshared)(__this, method)
