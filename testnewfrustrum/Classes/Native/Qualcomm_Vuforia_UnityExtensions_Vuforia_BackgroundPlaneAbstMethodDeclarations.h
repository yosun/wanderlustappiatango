﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t32;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C" int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2630 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2631 (BackgroundPlaneAbstractBehaviour_t32 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_SetEditorValues_m2632 (BackgroundPlaneAbstractBehaviour_t32 * __this, int32_t ___numDivisions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C" bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m2633 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2634 (BackgroundPlaneAbstractBehaviour_t32 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern "C" void BackgroundPlaneAbstractBehaviour_Start_m2635 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern "C" void BackgroundPlaneAbstractBehaviour_Update_m2636 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C" Quaternion_t13  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2637 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2638 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2639 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern "C" bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2640 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m543 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C" void BackgroundPlaneAbstractBehaviour__ctor_m391 (BackgroundPlaneAbstractBehaviour_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern "C" void BackgroundPlaneAbstractBehaviour__cctor_m2641 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
