﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t1056;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24001_gshared (DOGetter_1_t1056 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m24001(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1056 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m24001_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C" int64_t DOGetter_1_Invoke_m24002_gshared (DOGetter_1_t1056 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m24002(__this, method) (( int64_t (*) (DOGetter_1_t1056 *, const MethodInfo*))DOGetter_1_Invoke_m24002_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24003_gshared (DOGetter_1_t1056 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m24003(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1056 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m24003_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" int64_t DOGetter_1_EndInvoke_m24004_gshared (DOGetter_1_t1056 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m24004(__this, ___result, method) (( int64_t (*) (DOGetter_1_t1056 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m24004_gshared)(__this, ___result, method)
