﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct U24ArrayTypeU2448_t1794;
struct U24ArrayTypeU2448_t1794_marshaled;

void U24ArrayTypeU2448_t1794_marshal(const U24ArrayTypeU2448_t1794& unmarshaled, U24ArrayTypeU2448_t1794_marshaled& marshaled);
void U24ArrayTypeU2448_t1794_marshal_back(const U24ArrayTypeU2448_t1794_marshaled& marshaled, U24ArrayTypeU2448_t1794& unmarshaled);
void U24ArrayTypeU2448_t1794_marshal_cleanup(U24ArrayTypeU2448_t1794_marshaled& marshaled);
