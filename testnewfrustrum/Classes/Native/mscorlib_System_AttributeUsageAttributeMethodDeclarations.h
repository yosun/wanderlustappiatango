﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1452;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C" void AttributeUsageAttribute__ctor_m7108 (AttributeUsageAttribute_t1452 * __this, int32_t ___validOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
extern "C" bool AttributeUsageAttribute_get_AllowMultiple_m9573 (AttributeUsageAttribute_t1452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m7110 (AttributeUsageAttribute_t1452 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
extern "C" bool AttributeUsageAttribute_get_Inherited_m9574 (AttributeUsageAttribute_t1452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C" void AttributeUsageAttribute_set_Inherited_m7109 (AttributeUsageAttribute_t1452 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
