﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t3733;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1172;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1184;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m24449(__this, ___dictionary, method) (( void (*) (Enumerator_t3733 *, Dictionary_2_t1184 *, const MethodInfo*))Enumerator__ctor_m16933_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24450(__this, method) (( Object_t * (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24451(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24452(__this, method) (( Object_t * (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24453(__this, method) (( Object_t * (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m24454(__this, method) (( bool (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_MoveNext_m16938_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m24455(__this, method) (( KeyValuePair_2_t3731  (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_get_Current_m16939_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24456(__this, method) (( String_t* (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_get_CurrentKey_m16940_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24457(__this, method) (( GUIStyle_t1172 * (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_get_CurrentValue_m16941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m24458(__this, method) (( void (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_VerifyState_m16942_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24459(__this, method) (( void (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_VerifyCurrent_m16943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m24460(__this, method) (( void (*) (Enumerator_t3733 *, const MethodInfo*))Enumerator_Dispose_m16944_gshared)(__this, method)
