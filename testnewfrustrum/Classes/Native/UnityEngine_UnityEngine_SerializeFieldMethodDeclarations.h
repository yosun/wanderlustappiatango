﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t161;

// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m510 (SerializeField_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
