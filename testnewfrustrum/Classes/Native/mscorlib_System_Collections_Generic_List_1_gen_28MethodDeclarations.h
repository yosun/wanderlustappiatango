﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t668;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t589;
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable>
struct IEnumerable_1_t767;
// System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackable>
struct IEnumerator_1_t4170;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackable>
struct ICollection_1_t4171;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>
struct ReadOnlyCollection_1_t3449;
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3447;
// System.Predicate`1<Vuforia.SmartTerrainTrackable>
struct Predicate_1_t3450;
// System.Comparison`1<Vuforia.SmartTerrainTrackable>
struct Comparison_1_t3452;
// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_47.h"

// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4434(__this, method) (( void (*) (List_1_t668 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19834(__this, ___collection, method) (( void (*) (List_1_t668 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Int32)
#define List_1__ctor_m19835(__this, ___capacity, method) (( void (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.cctor()
#define List_1__cctor_m19836(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19837(__this, method) (( Object_t* (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19838(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t668 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19839(__this, method) (( Object_t * (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19840(__this, ___item, method) (( int32_t (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19841(__this, ___item, method) (( bool (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19842(__this, ___item, method) (( int32_t (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19843(__this, ___index, ___item, method) (( void (*) (List_1_t668 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19844(__this, ___item, method) (( void (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19845(__this, method) (( bool (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19846(__this, method) (( bool (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19847(__this, method) (( Object_t * (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19848(__this, method) (( bool (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19849(__this, method) (( bool (*) (List_1_t668 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19850(__this, ___index, method) (( Object_t * (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19851(__this, ___index, ___value, method) (( void (*) (List_1_t668 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Add(T)
#define List_1_Add_m19852(__this, ___item, method) (( void (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19853(__this, ___newCount, method) (( void (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19854(__this, ___collection, method) (( void (*) (List_1_t668 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19855(__this, ___enumerable, method) (( void (*) (List_1_t668 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19856(__this, ___collection, method) (( void (*) (List_1_t668 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AsReadOnly()
#define List_1_AsReadOnly_m19857(__this, method) (( ReadOnlyCollection_1_t3449 * (*) (List_1_t668 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Clear()
#define List_1_Clear_m19858(__this, method) (( void (*) (List_1_t668 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(T)
#define List_1_Contains_m19859(__this, ___item, method) (( bool (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19860(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t668 *, SmartTerrainTrackableU5BU5D_t3447*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Find(System.Predicate`1<T>)
#define List_1_Find_m19861(__this, ___match, method) (( Object_t * (*) (List_1_t668 *, Predicate_1_t3450 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19862(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3450 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19863(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t668 *, int32_t, int32_t, Predicate_1_t3450 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetEnumerator()
#define List_1_GetEnumerator_m19864(__this, method) (( Enumerator_t3451  (*) (List_1_t668 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::IndexOf(T)
#define List_1_IndexOf_m19865(__this, ___item, method) (( int32_t (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19866(__this, ___start, ___delta, method) (( void (*) (List_1_t668 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19867(__this, ___index, method) (( void (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Insert(System.Int32,T)
#define List_1_Insert_m19868(__this, ___index, ___item, method) (( void (*) (List_1_t668 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19869(__this, ___collection, method) (( void (*) (List_1_t668 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Remove(T)
#define List_1_Remove_m19870(__this, ___item, method) (( bool (*) (List_1_t668 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19871(__this, ___match, method) (( int32_t (*) (List_1_t668 *, Predicate_1_t3450 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19872(__this, ___index, method) (( void (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Reverse()
#define List_1_Reverse_m19873(__this, method) (( void (*) (List_1_t668 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort()
#define List_1_Sort_m19874(__this, method) (( void (*) (List_1_t668 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19875(__this, ___comparison, method) (( void (*) (List_1_t668 *, Comparison_1_t3452 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::ToArray()
#define List_1_ToArray_m19876(__this, method) (( SmartTerrainTrackableU5BU5D_t3447* (*) (List_1_t668 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::TrimExcess()
#define List_1_TrimExcess_m19877(__this, method) (( void (*) (List_1_t668 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Capacity()
#define List_1_get_Capacity_m19878(__this, method) (( int32_t (*) (List_1_t668 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19879(__this, ___value, method) (( void (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Count()
#define List_1_get_Count_m19880(__this, method) (( int32_t (*) (List_1_t668 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Item(System.Int32)
#define List_1_get_Item_m19881(__this, ___index, method) (( Object_t * (*) (List_1_t668 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Item(System.Int32,T)
#define List_1_set_Item_m19882(__this, ___index, ___value, method) (( void (*) (List_1_t668 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
