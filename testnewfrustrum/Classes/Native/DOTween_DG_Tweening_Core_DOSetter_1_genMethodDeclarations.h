﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t121;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m362_gshared (DOSetter_1_t121 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m362(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t121 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m362_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m14922_gshared (DOSetter_1_t121 * __this, Vector3_t15  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m14922(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t121 *, Vector3_t15 , const MethodInfo*))DOSetter_1_Invoke_m14922_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m14923_gshared (DOSetter_1_t121 * __this, Vector3_t15  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m14923(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t121 *, Vector3_t15 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m14923_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m14924_gshared (DOSetter_1_t121 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m14924(__this, ___result, method) (( void (*) (DOSetter_1_t121 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m14924_gshared)(__this, ___result, method)
