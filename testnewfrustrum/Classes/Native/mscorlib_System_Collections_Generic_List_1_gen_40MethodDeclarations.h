﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t745;
// System.Object
struct Object_t;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t788;
// System.Collections.Generic.IEnumerable`1<Vuforia.ITrackerEventHandler>
struct IEnumerable_1_t4267;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITrackerEventHandler>
struct IEnumerator_1_t4268;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ITrackerEventHandler>
struct ICollection_1_t4269;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackerEventHandler>
struct ReadOnlyCollection_1_t3629;
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t3627;
// System.Predicate`1<Vuforia.ITrackerEventHandler>
struct Predicate_1_t3630;
// System.Comparison`1<Vuforia.ITrackerEventHandler>
struct Comparison_1_t3631;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4644(__this, method) (( void (*) (List_1_t745 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22776(__this, ___collection, method) (( void (*) (List_1_t745 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m22777(__this, ___capacity, method) (( void (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.cctor()
#define List_1__cctor_m22778(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22779(__this, method) (( Object_t* (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22780(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t745 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22781(__this, method) (( Object_t * (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22782(__this, ___item, method) (( int32_t (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22783(__this, ___item, method) (( bool (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22784(__this, ___item, method) (( int32_t (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22785(__this, ___index, ___item, method) (( void (*) (List_1_t745 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22786(__this, ___item, method) (( void (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22787(__this, method) (( bool (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22788(__this, method) (( bool (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22789(__this, method) (( Object_t * (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22790(__this, method) (( bool (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22791(__this, method) (( bool (*) (List_1_t745 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22792(__this, ___index, method) (( Object_t * (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22793(__this, ___index, ___value, method) (( void (*) (List_1_t745 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Add(T)
#define List_1_Add_m22794(__this, ___item, method) (( void (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22795(__this, ___newCount, method) (( void (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22796(__this, ___collection, method) (( void (*) (List_1_t745 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22797(__this, ___enumerable, method) (( void (*) (List_1_t745 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22798(__this, ___collection, method) (( void (*) (List_1_t745 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m22799(__this, method) (( ReadOnlyCollection_1_t3629 * (*) (List_1_t745 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Clear()
#define List_1_Clear_m22800(__this, method) (( void (*) (List_1_t745 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Contains(T)
#define List_1_Contains_m22801(__this, ___item, method) (( bool (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22802(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t745 *, ITrackerEventHandlerU5BU5D_t3627*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m22803(__this, ___match, method) (( Object_t * (*) (List_1_t745 *, Predicate_1_t3630 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22804(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3630 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22805(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t745 *, int32_t, int32_t, Predicate_1_t3630 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4637(__this, method) (( Enumerator_t886  (*) (List_1_t745 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::IndexOf(T)
#define List_1_IndexOf_m22806(__this, ___item, method) (( int32_t (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22807(__this, ___start, ___delta, method) (( void (*) (List_1_t745 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22808(__this, ___index, method) (( void (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m22809(__this, ___index, ___item, method) (( void (*) (List_1_t745 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22810(__this, ___collection, method) (( void (*) (List_1_t745 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Remove(T)
#define List_1_Remove_m22811(__this, ___item, method) (( bool (*) (List_1_t745 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22812(__this, ___match, method) (( int32_t (*) (List_1_t745 *, Predicate_1_t3630 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22813(__this, ___index, method) (( void (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Reverse()
#define List_1_Reverse_m22814(__this, method) (( void (*) (List_1_t745 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Sort()
#define List_1_Sort_m22815(__this, method) (( void (*) (List_1_t745 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22816(__this, ___comparison, method) (( void (*) (List_1_t745 *, Comparison_1_t3631 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::ToArray()
#define List_1_ToArray_m22817(__this, method) (( ITrackerEventHandlerU5BU5D_t3627* (*) (List_1_t745 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::TrimExcess()
#define List_1_TrimExcess_m22818(__this, method) (( void (*) (List_1_t745 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Capacity()
#define List_1_get_Capacity_m22819(__this, method) (( int32_t (*) (List_1_t745 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22820(__this, ___value, method) (( void (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Count()
#define List_1_get_Count_m22821(__this, method) (( int32_t (*) (List_1_t745 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m22822(__this, ___index, method) (( Object_t * (*) (List_1_t745 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m22823(__this, ___index, ___value, method) (( void (*) (List_1_t745 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
