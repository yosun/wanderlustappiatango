﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1002;
struct VectorOptions_t1002_marshaled;

void VectorOptions_t1002_marshal(const VectorOptions_t1002& unmarshaled, VectorOptions_t1002_marshaled& marshaled);
void VectorOptions_t1002_marshal_back(const VectorOptions_t1002_marshaled& marshaled, VectorOptions_t1002& unmarshaled);
void VectorOptions_t1002_marshal_cleanup(VectorOptions_t1002_marshaled& marshaled);
