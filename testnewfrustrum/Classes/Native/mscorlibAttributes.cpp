﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern TypeInfo* AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t483_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t897_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibVersionAttribute_t2290_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultDependencyAttribute_t2269_il2cpp_TypeInfo_var;
extern TypeInfo* StringFreezingAttribute_t2272_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var;
void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3098);
		AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3102);
		AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(484);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(478);
		AssemblyProductAttribute_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(481);
		NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3104);
		SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3099);
		AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3101);
		AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3100);
		AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(482);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(480);
		DebuggableAttribute_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1270);
		TypeLibVersionAttribute_t2290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4502);
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		DefaultDependencyAttribute_t2269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4503);
		StringFreezingAttribute_t2272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4504);
		AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(485);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1271);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 21;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyInformationalVersionAttribute_t1582 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1582 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7264(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1586 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1586 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7269(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t486 * tmp;
		tmp = (AssemblyTitleAttribute_t486 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2434(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t480 * tmp;
		tmp = (AssemblyDescriptionAttribute_t480 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2428(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t483 * tmp;
		tmp = (AssemblyProductAttribute_t483 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t483_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2431(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1588 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1588 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7271(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1583 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1583 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7265(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1585 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1585 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7268(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1584 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1584 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7266(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t484 * tmp;
		tmp = (AssemblyCopyrightAttribute_t484 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2432(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, true, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t482 * tmp;
		tmp = (AssemblyCompanyAttribute_t482 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2430(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t897 * tmp;
		tmp = (DebuggableAttribute_t897 *)il2cpp_codegen_object_new (DebuggableAttribute_t897_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4679(tmp, 2, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		TypeLibVersionAttribute_t2290 * tmp;
		tmp = (TypeLibVersionAttribute_t2290 *)il2cpp_codegen_object_new (TypeLibVersionAttribute_t2290_il2cpp_TypeInfo_var);
		TypeLibVersionAttribute__ctor_m12088(tmp, 2, 0, NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		DefaultDependencyAttribute_t2269 * tmp;
		tmp = (DefaultDependencyAttribute_t2269 *)il2cpp_codegen_object_new (DefaultDependencyAttribute_t2269_il2cpp_TypeInfo_var);
		DefaultDependencyAttribute__ctor_m12051(tmp, 1, NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		StringFreezingAttribute_t2272 * tmp;
		tmp = (StringFreezingAttribute_t2272 *)il2cpp_codegen_object_new (StringFreezingAttribute_t2272_il2cpp_TypeInfo_var);
		StringFreezingAttribute__ctor_m12052(tmp, NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t487 * tmp;
		tmp = (AssemblyFileVersionAttribute_t487 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2435(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[19] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t898 * tmp;
		tmp = (CompilationRelaxationsAttribute_t898 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7267(tmp, 8, NULL);
		cache->attributes[20] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object__ctor_m314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_Finalize_m515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m6924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ValueType_t524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern const Il2CppType* _Attribute_t901_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Attribute_t138_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Attribute_t901_0_0_0_var = il2cpp_codegen_type_from_index(1281);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Attribute_t901_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
extern const Il2CppType* Attribute_t138_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _Attribute_t901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Attribute_t138_0_0_0_var = il2cpp_codegen_type_from_index(52);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Attribute_t138_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("917B14D0-2D9E-38B8-92A9-381ACF52F7C0"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Int32_t127_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IFormattable_t164_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void IConvertible_t165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IComparable_t166_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void SerializableAttribute_t2042_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4124, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AttributeUsageAttribute_t1452_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ComVisibleAttribute_t485_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 5597, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Int64_t1092_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UInt32_t1075_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt32_t1075_CustomAttributesCacheGenerator_UInt32_Parse_m9625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt32_t1075_CustomAttributesCacheGenerator_UInt32_Parse_m9626(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt32_t1075_CustomAttributesCacheGenerator_UInt32_TryParse_m9387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt32_t1075_CustomAttributesCacheGenerator_UInt32_TryParse_m6985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CLSCompliantAttribute_t1587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UInt64_t1091_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt64_t1091_CustomAttributesCacheGenerator_UInt64_Parse_m9649(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt64_t1091_CustomAttributesCacheGenerator_UInt64_Parse_m9651(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt64_t1091_CustomAttributesCacheGenerator_UInt64_TryParse_m6970(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Byte_t449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SByte_t170_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SByte_t170_CustomAttributesCacheGenerator_SByte_Parse_m9701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SByte_t170_CustomAttributesCacheGenerator_SByte_Parse_m9702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SByte_t170_CustomAttributesCacheGenerator_SByte_TryParse_m9703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Int16_t534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt16_t454_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt16_t454_CustomAttributesCacheGenerator_UInt16_Parse_m9756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt16_t454_CustomAttributesCacheGenerator_UInt16_Parse_m9757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt16_t454_CustomAttributesCacheGenerator_UInt16_TryParse_m9758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UInt16_t454_CustomAttributesCacheGenerator_UInt16_TryParse_m9759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void IEnumerator_t410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("496B0ABF-CDEE-11D3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void IEnumerable_t550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("496B0ABE-CDEE-11d3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern TypeInfo* DispIdAttribute_t2284_il2cpp_TypeInfo_var;
void IEnumerable_t550_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DispIdAttribute_t2284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4510);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DispIdAttribute_t2284 * tmp;
		tmp = (DispIdAttribute_t2284 *)il2cpp_codegen_object_new (DispIdAttribute_t2284_il2cpp_TypeInfo_var);
		DispIdAttribute__ctor_m12058(tmp, -4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IDisposable_t144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Char_t451_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String__ctor_m9793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m9816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m9817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Split_m331_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoDocumentationNoteAttribute_t2065_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoDocumentationNoteAttribute_t2065_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoDocumentationNoteAttribute_t2065 * tmp;
		tmp = (MonoDocumentationNoteAttribute_t2065 *)il2cpp_codegen_object_new (MonoDocumentationNoteAttribute_t2065_il2cpp_TypeInfo_var);
		MonoDocumentationNoteAttribute__ctor_m10321(tmp, il2cpp_codegen_string_new_wrapper("code should be moved to managed"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9352(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m6932_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m9389_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m9350_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m2371_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m8315_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m9851_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m388_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m6946_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_GetHashCode_m9860(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICloneable_t512_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Single_t151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Single_t151_CustomAttributesCacheGenerator_Single_IsNaN_m9896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Double_t1407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Double_t1407_CustomAttributesCacheGenerator_Double_IsNaN_m9924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
extern TypeInfo* DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4512);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2056 * tmp;
		tmp = (DecimalConstantAttribute_t2056 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10306(tmp, 0, 255, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4512);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2056 * tmp;
		tmp = (DecimalConstantAttribute_t2056 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10306(tmp, 0, 0, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_MinusOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4512);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2056 * tmp;
		tmp = (DecimalConstantAttribute_t2056 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10306(tmp, 0, 255, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_One(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4512);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2056 * tmp;
		tmp = (DecimalConstantAttribute_t2056 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2056_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10306(tmp, 0, 0, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal__ctor_m9935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal__ctor_m9937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_Compare_m9968(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9996(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m5548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10005(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m5544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Boolean_t169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m4293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m4356(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m4376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10048(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ISerializable_t513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10067(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MulticastDelegate_t307_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void Delegate_t143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Delegate_t143_CustomAttributesCacheGenerator_Delegate_Combine_m10088(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Delegate_t143_CustomAttributesCacheGenerator_Delegate_t143_Delegate_Combine_m10088_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_GetName_m10096(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_IsDefined_m8335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10098(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_Parse_m9372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToString_m530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToString_m518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10102(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10103(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10105(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10106(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10109(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Enum_t167_CustomAttributesCacheGenerator_Enum_Format_m10114(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Length_m9317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m10137(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m9320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10142_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10143_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10153(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10157(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10158(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10159(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10160(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10161(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10162(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10168_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10171_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10172_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10173_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Clear_m8250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m9379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10182(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10184(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10185(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10188(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10189(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10190(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8244(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10192(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10194(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14057(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Resize_m14070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10213(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2631_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2631_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14111(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14118(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICollection_t1513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IList_t1514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void IList_1_t2633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Void_t168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Type_t2635_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Type_t2635_0_0_0_var = il2cpp_codegen_type_from_index(4513);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Type_t2635_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10266(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10279_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MemberInfo_t2636_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MemberInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MemberInfo_t2636_0_0_0_var = il2cpp_codegen_type_from_index(4514);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_MemberInfo_t2636_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICustomAttributeProvider_t2599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
extern const Il2CppType* MemberInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _MemberInfo_t2636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemberInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4386);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("f7102fa9-cabb-3a74-a6da-b4567ef1b079"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(MemberInfo_t_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void IReflect_t2637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Type
#include "mscorlib_System_Type.h"
extern const Il2CppType* Type_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void _Type_t2635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_0_0_0_var = il2cpp_codegen_type_from_index(40);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Type_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Exception_t1473_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void Exception_t140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Exception_t1473_0_0_0_var = il2cpp_codegen_type_from_index(2455);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Exception_t1473_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _Exception_t1473_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("b36b5c63-42ef-38bc-a07e-0b34c98f164a"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2048_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2048_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2047_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2047_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ParamArrayAttribute_t502_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2048, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void OutAttribute_t2049_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ObsoleteAttribute_t493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 6140, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DllImportAttribute_t2050_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2051_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 10496, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2051_CustomAttributesCacheGenerator_MarshalType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2051_CustomAttributesCacheGenerator_MarshalTypeRef(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void InAttribute_t2052_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ConditionalAttribute_t2053_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 68, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void GuidAttribute_t479_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 5149, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ComImportAttribute_t2054_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1028, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void OptionalAttribute_t2055_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void CompilerGeneratedAttribute_t162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void InternalsVisibleToAttribute_t896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void RuntimeCompatibilityAttribute_t160_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DebuggerHiddenAttribute_t497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 224, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DefaultMemberAttribute_t508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1036, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2056_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2304, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2056_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void FieldOffsetAttribute_t2057_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RuntimeArgumentHandle_t2058_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsyncCallback_t305_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IAsyncResult_t304_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void TypedReference_t2059_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MarshalByRefObject_t1885_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Locale_t2063_CustomAttributesCacheGenerator_Locale_t2063_Locale_GetText_m10318_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t2064_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void MonoDocumentationNoteAttribute_t2065_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandleZeroOrMinusOneIsInvalid_t2066_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeWaitHandle_t2068_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10324(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SortKey_t2088_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void X509CertificateCollection_t2115_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t2118_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void ASN1_t2112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void SmallXmlParser_t2130_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
extern const Il2CppType* CollectionDebuggerView_2_t2639_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Dictionary_2_t2643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2639_0_0_0_var = il2cpp_codegen_type_from_index(4516);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2639_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Dictionary_2_t2643_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Dictionary_2_t2643_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2639_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
void KeyCollection_t2646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2639_0_0_0_var = il2cpp_codegen_type_from_index(4516);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2639_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2639_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
void ValueCollection_t2648_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2639_0_0_0_var = il2cpp_codegen_type_from_index(4516);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2639_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void IDictionary_2_t2655_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void KeyNotFoundException_t2137_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
void KeyValuePair_2_t2657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("{value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11044(tmp, il2cpp_codegen_string_new_wrapper("[{key}]"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_1_t2638_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void List_1_t2658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_1_t2638_0_0_0_var = il2cpp_codegen_type_from_index(4519);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_1_t2638_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void Collection_1_t2660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ReadOnlyCollection_1_t2661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
extern const Il2CppType* CollectionDebuggerView_t2144_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArrayList_t1668_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2144_0_0_0_var = il2cpp_codegen_type_from_index(4520);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2144_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void ArrayListWrapper_t2139_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void SynchronizedArrayListWrapper_t2140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void ReadOnlyArrayListWrapper_t2142_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void BitArray_t1978_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CaseInsensitiveComparer_t1998_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CaseInsensitiveHashCodeProvider_t1999_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use StringComparer instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CollectionBase_t1701_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Comparer_t2145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
void DictionaryEntry_t1996_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("{_value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11044(tmp, il2cpp_codegen_string_new_wrapper("[{_key}]"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2144_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2144_0_0_0_var = il2cpp_codegen_type_from_index(4520);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2144_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m10967(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m10970(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_Clear_m10986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_Remove_m10989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m10993(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialize equalityComparer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_t1736____comparer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_t1736____hcp_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2144_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
void HashKeys_t2150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2144_0_0_0_var = il2cpp_codegen_type_from_index(4520);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2144_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2144_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
void HashValues_t2151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2144_0_0_0_var = il2cpp_codegen_type_from_index(4520);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2144_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IComparer_t1852_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void IDictionary_t1931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IDictionaryEnumerator_t1995_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IEqualityComparer_t1858_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void IHashCodeProvider_t1857_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Please use IEqualityComparer instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SortedList_t2002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2144_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Stack_t1353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2144_0_0_0_var = il2cpp_codegen_type_from_index(4520);
		DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2161 * tmp;
		tmp = (DebuggerDisplayAttribute_t2161 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2161_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11043(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2163 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2163 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2163_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11046(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2144_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyHashAlgorithm_t2158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyVersionCompatibility_t2159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ConditionalAttribute_t2053_il2cpp_TypeInfo_var;
void SuppressMessageAttribute_t1449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ConditionalAttribute_t2053_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t2053 * tmp;
		tmp = (ConditionalAttribute_t2053 *)il2cpp_codegen_object_new (ConditionalAttribute_t2053_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m10302(tmp, il2cpp_codegen_string_new_wrapper("CODE_ANALYSIS"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DebuggableAttribute_t897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DebuggingModes_t2160_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DebuggerDisplayAttribute_t2161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4509, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DebuggerStepThroughAttribute_t2162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 108, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DebuggerTypeProxyAttribute_t2163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 13, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StackFrame_t1433_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with MS.NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StackTrace_t1381_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Calendar_t2165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CompareInfo_t1827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CompareOptions_t2169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CultureInfo_t1405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CultureInfo_t1405_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void CultureInfo_t1405_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void DateTimeFormatFlags_t2173_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DateTimeFormatInfo_t2171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DateTimeStyles_t2174_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DaylightTime_t2175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GregorianCalendar_t2176_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GregorianCalendarTypes_t2177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void NumberFormatInfo_t2170_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void NumberStyles_t2178_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void TextInfo_t2085_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("IDeserializationCallback isn't implemented."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void TextInfo_t2085_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11239(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TextInfo_t2085_CustomAttributesCacheGenerator_TextInfo_t2085____CultureName_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnicodeCategory_t2037_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IsolatedStorageException_t2180_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void BinaryReader_t2182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Directory_t2183_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DirectoryInfo_t2184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DirectoryNotFoundException_t2186_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EndOfStreamException_t2187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void File_t2188_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void FileAccess_t2001_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileAttributes_t2189_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileMode_t2190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileNotFoundException_t2191_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void FileOptions_t2192_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileShare_t2193_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileStream_t1820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileSystemInfo_t2185_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FileSystemInfo_t2185_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IOException_t1836_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MemoryStream_t1398_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Path_t881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void Path_t881_CustomAttributesCacheGenerator_InvalidPathChars(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("see GetInvalidPathChars and GetInvalidFileNameChars methods."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PathTooLongException_t2201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SeekOrigin_t1844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Stream_t1751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StreamReader_t2206_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StreamWriter_t2207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StringReader_t1397_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TextReader_t2133_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TextWriter_t2006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyBuilder_t2662_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void AssemblyBuilder_t2215_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyBuilder_t2662_0_0_0_var = il2cpp_codegen_type_from_index(4522);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_AssemblyBuilder_t2662_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorBuilder_t2663_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorBuilder_t2663_0_0_0_var = il2cpp_codegen_type_from_index(4523);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_ConstructorBuilder_t2663_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2218_CustomAttributesCacheGenerator_ConstructorBuilder_t2218____CallingConvention_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EnumBuilder_t2664_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EnumBuilder_t2219_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EnumBuilder_t2664_0_0_0_var = il2cpp_codegen_type_from_index(4524);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_EnumBuilder_t2664_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EnumBuilder_t2219_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m11592(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldBuilder_t2665_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FieldBuilder_t2221_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldBuilder_t2665_0_0_0_var = il2cpp_codegen_type_from_index(4525);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_FieldBuilder_t2665_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m11628(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m11631(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m11672(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m11673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m11674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2223_GenericTypeParameterBuilder_MakeGenericType_m11674_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBuilder_t2666_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void MethodBuilder_t2222_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBuilder_t2666_0_0_0_var = il2cpp_codegen_type_from_index(4526);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_MethodBuilder_t2666_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void MethodBuilder_t2222_CustomAttributesCacheGenerator_MethodBuilder_Equals_m11690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void MethodBuilder_t2222_CustomAttributesCacheGenerator_MethodBuilder_t2222_MethodBuilder_MakeGenericMethod_m11693_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ModuleBuilder_t2667_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ModuleBuilder_t2225_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ModuleBuilder_t2667_0_0_0_var = il2cpp_codegen_type_from_index(4527);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_ModuleBuilder_t2667_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterBuilder_t2668_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void ParameterBuilder_t2227_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterBuilder_t2668_0_0_0_var = il2cpp_codegen_type_from_index(4528);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_ParameterBuilder_t2668_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyBuilder_t2669_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PropertyBuilder_t2228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyBuilder_t2669_0_0_0_var = il2cpp_codegen_type_from_index(4529);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_PropertyBuilder_t2669_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _TypeBuilder_t2670_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_TypeBuilder_t2670_0_0_0_var = il2cpp_codegen_type_from_index(4530);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_TypeBuilder_t2670_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m11737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m11756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_t2216_TypeBuilder_MakeGenericType_m11756_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m11763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m11764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m11765(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("arrays"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void UnmanagedMarshal_t2220_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("An alternate API is available: Emit the MarshalAs custom attribute instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AmbiguousMatchException_t2233_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Assembly_t2671_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Assembly_t2003_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Assembly_t2671_0_0_0_var = il2cpp_codegen_type_from_index(4531);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Assembly_t2671_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void AssemblyCompanyAttribute_t482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyConfigurationAttribute_t481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyCopyrightAttribute_t484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyDefaultAliasAttribute_t1584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void AssemblyDelaySignAttribute_t1586_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void AssemblyDescriptionAttribute_t480_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyFileVersionAttribute_t487_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void AssemblyInformationalVersionAttribute_t1582_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyKeyFileAttribute_t1585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyName_t2672_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void AssemblyName_t2238_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyName_t2672_0_0_0_var = il2cpp_codegen_type_from_index(4532);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_AssemblyName_t2672_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void AssemblyNameFlags_t2239_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyProductAttribute_t483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyTitleAttribute_t486_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyTrademarkAttribute_t488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void Binder_t1431_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void Default_t2240_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m11817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void BindingFlags_t2241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CallingConventions_t2242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorInfo_t2673_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void ConstructorInfo_t1287_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorInfo_t2673_0_0_0_var = il2cpp_codegen_type_from_index(4533);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_ConstructorInfo_t2673_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ConstructorInfo_t1287_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var;
void ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4534);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2162 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2162 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11045(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorInfo_t1287____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EventAttributes_t2243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EventInfo_t2674_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void EventInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EventInfo_t2674_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_EventInfo_t2674_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void FieldAttributes_t2245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldInfo_t2675_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldInfo_t2675_0_0_0_var = il2cpp_codegen_type_from_index(4536);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_FieldInfo_t2675_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m11850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4534);
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t2162 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2162 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MemberTypes_t2247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void MethodAttributes_t2248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBase_t2676_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void MethodBase_t1434_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBase_t2676_0_0_0_var = il2cpp_codegen_type_from_index(4537);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_MethodBase_t2676_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var;
void MethodBase_t1434_CustomAttributesCacheGenerator_MethodBase_Invoke_m11867(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4534);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2162 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2162 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11045(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MethodBase_t1434_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m11872(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MethodImplAttributes_t2249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodInfo_t2677_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodInfo_t2677_0_0_0_var = il2cpp_codegen_type_from_index(4538);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_MethodInfo_t2677_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m11879_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m11880(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Missing_t2250_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void Missing_t2250_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11886(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Module_t2678_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void Module_t2226_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Module_t2678_0_0_0_var = il2cpp_codegen_type_from_index(4539);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Module_t2678_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void PInfo_t2257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void ParameterAttributes_t2259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterInfo_t2679_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void ParameterInfo_t1426_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterInfo_t2679_0_0_0_var = il2cpp_codegen_type_from_index(4540);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_ParameterInfo_t2679_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ParameterModifier_t2260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Pointer_t2261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ProcessorArchitecture_t2262_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PropertyAttributes_t2263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyInfo_t2680_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyInfo_t2680_0_0_0_var = il2cpp_codegen_type_from_index(4541);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_PropertyInfo_t2680_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4534);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2162 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2162 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11045(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4534);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2162 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2162 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2162_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11045(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StrongNameKeyPair_t2237_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TargetException_t2264_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TargetInvocationException_t2265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TargetParameterCountException_t2266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void TypeAttributes_t2267_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void NeutralResourcesLanguageAttribute_t1588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void SatelliteContractVersionAttribute_t1583_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void CompilationRelaxations_t2268_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CompilationRelaxationsAttribute_t898_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 71, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void DefaultDependencyAttribute_t2269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IsVolatile_t2270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void StringFreezingAttribute_t2272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12053(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ReliabilityContractAttribute_t2276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1133, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ActivationArguments_t2277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CallingConvention_t2278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CharSet_t2279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ClassInterfaceAttribute_t2280_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 5, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ClassInterfaceType_t2281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ComDefaultInterfaceAttribute_t2282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ComInterfaceType_t2283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DispIdAttribute_t2284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 960, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GCHandle_t805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void GCHandleType_t2285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void InterfaceTypeAttribute_t2286_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
extern TypeInfo* SuppressUnmanagedCodeSecurityAttribute_t2443_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressUnmanagedCodeSecurityAttribute_t2443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4542);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressUnmanagedCodeSecurityAttribute_t2443 * tmp;
		tmp = (SuppressUnmanagedCodeSecurityAttribute_t2443 *)il2cpp_codegen_object_new (SuppressUnmanagedCodeSecurityAttribute_t2443_il2cpp_TypeInfo_var);
		SuppressUnmanagedCodeSecurityAttribute__ctor_m12865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m12070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m4273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m4277(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_PtrToStructure_m4332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_ReadInt32_m4578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_ReadInt32_m12074(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Marshal_t792_CustomAttributesCacheGenerator_Marshal_StructureToPtr_m4295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MarshalDirectiveException_t2287_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PreserveSigAttribute_t2288_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle__ctor_m12078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Close_m12079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12085(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeLibImportClassAttribute_t2289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void TypeLibVersionAttribute_t2290_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnmanagedType_t2291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Activator
#include "mscorlib_System_Activator.h"
extern const Il2CppType* Activator_t2484_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void _Activator_t2681_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Activator_t2484_0_0_0_var = il2cpp_codegen_type_from_index(4543);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Activator_t2484_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
extern const Il2CppType* Assembly_t2003_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _Assembly_t2671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Assembly_t2003_0_0_0_var = il2cpp_codegen_type_from_index(4544);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Assembly_t2003_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
extern const Il2CppType* AssemblyBuilder_t2215_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
void _AssemblyBuilder_t2662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyBuilder_t2215_0_0_0_var = il2cpp_codegen_type_from_index(4265);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(AssemblyBuilder_t2215_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
extern const Il2CppType* AssemblyName_t2238_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
void _AssemblyName_t2672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyName_t2238_0_0_0_var = il2cpp_codegen_type_from_index(4276);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(AssemblyName_t2238_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
extern const Il2CppType* ConstructorBuilder_t2218_0_0_0_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
void _ConstructorBuilder_t2663_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorBuilder_t2218_0_0_0_var = il2cpp_codegen_type_from_index(4545);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(ConstructorBuilder_t2218_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern const Il2CppType* ConstructorInfo_t1287_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _ConstructorInfo_t2673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorInfo_t1287_0_0_0_var = il2cpp_codegen_type_from_index(2270);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(ConstructorInfo_t1287_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
extern const Il2CppType* EnumBuilder_t2219_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
void _EnumBuilder_t2664_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EnumBuilder_t2219_0_0_0_var = il2cpp_codegen_type_from_index(4154);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(EnumBuilder_t2219_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
extern const Il2CppType* EventInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _EventInfo_t2674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4149);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(EventInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
extern const Il2CppType* FieldBuilder_t2221_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _FieldBuilder_t2665_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldBuilder_t2221_0_0_0_var = il2cpp_codegen_type_from_index(4546);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(FieldBuilder_t2221_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
extern const Il2CppType* FieldInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
void _FieldInfo_t2675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2261);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(FieldInfo_t_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
extern const Il2CppType* MethodBase_t1434_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _MethodBase_t2676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBase_t1434_0_0_0_var = il2cpp_codegen_type_from_index(4268);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(MethodBase_t1434_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
extern const Il2CppType* MethodBuilder_t2222_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _MethodBuilder_t2666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBuilder_t2222_0_0_0_var = il2cpp_codegen_type_from_index(4547);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(MethodBuilder_t2222_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
void _MethodInfo_t2677_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(50);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(MethodInfo_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
extern const Il2CppType* Module_t2226_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _Module_t2678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Module_t2226_0_0_0_var = il2cpp_codegen_type_from_index(4261);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Module_t2226_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
extern const Il2CppType* ModuleBuilder_t2225_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _ModuleBuilder_t2667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ModuleBuilder_t2225_0_0_0_var = il2cpp_codegen_type_from_index(4264);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(ModuleBuilder_t2225_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
extern const Il2CppType* ParameterBuilder_t2227_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
void _ParameterBuilder_t2668_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterBuilder_t2227_0_0_0_var = il2cpp_codegen_type_from_index(4548);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(ParameterBuilder_t2227_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern const Il2CppType* ParameterInfo_t1426_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void _ParameterInfo_t2679_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterInfo_t1426_0_0_0_var = il2cpp_codegen_type_from_index(4263);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(ParameterInfo_t1426_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilder.h"
extern const Il2CppType* PropertyBuilder_t2228_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void _PropertyBuilder_t2669_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyBuilder_t2228_0_0_0_var = il2cpp_codegen_type_from_index(4549);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("15F9A479-9397-3A63-ACBD-F51977FB0F02"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(PropertyBuilder_t2228_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
extern const Il2CppType* PropertyInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void _PropertyInfo_t2680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2258);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(PropertyInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
extern const Il2CppType* Thread_t2308_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void _Thread_t2682_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t2308_0_0_0_var = il2cpp_codegen_type_from_index(4120);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(Thread_t2308_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
extern const Il2CppType* TypeBuilder_t2216_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
void _TypeBuilder_t2670_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeBuilder_t2216_0_0_0_var = il2cpp_codegen_type_from_index(4152);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4508);
		InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4509);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2289 * tmp;
		tmp = (TypeLibImportClassAttribute_t2289 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2289_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12087(tmp, il2cpp_codegen_type_get_object(TypeBuilder_t2216_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2286 * tmp;
		tmp = (InterfaceTypeAttribute_t2286 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2286_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12068(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IActivator_t2292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IConstructionCallMessage_t2592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UrlAttribute_t2298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UrlAttribute_t2298_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12099(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UrlAttribute_t2298_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12100(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ChannelServices_t2302_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void ChannelServices_t2302_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void CrossAppDomainSink_t2305_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IChannel_t2593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IChannelReceiver_t2607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IChannelSender_t2683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Context_t2306_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ContextAttribute_t2299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IContextAttribute_t2605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IContextProperty_t2594_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IContributeClientContextSink_t2684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IContributeServerContextSink_t2685_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2309_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2309_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsyncResult_t2316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ConstructionCall_t2317_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ConstructionCall_t2317_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2319_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2319_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Header_t2322_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMessage_t2315_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMessageCtrl_t2314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMessageSink_t1130_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMethodCallMessage_t2596_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMethodMessage_t2327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IMethodReturnMessage_t2595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IRemotingFormatter_t2686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void LogicalCallContext_t2324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void MethodCall_t2318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MethodCall_t2318_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void MethodDictionary_t2320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MethodDictionary_t2320_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void MethodDictionary_t2320_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RemotingSurrogateSelector_t2332_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ReturnMessage_t2333_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ProxyAttribute_t2334_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ProxyAttribute_t2334_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ProxyAttribute_t2334_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RealProxy_t2335_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ITrackingHandler_t2610_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TrackingServices_t2339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ActivatedClientTypeEntry_t2340_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IChannelInfo_t2346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IEnvoyInfo_t2348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IRemotingTypeInfo_t2347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ObjRef_t2343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void ObjRef_t2343_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void ObjRef_t2343_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RemotingConfiguration_t2349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RemotingException_t2350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RemotingServices_t2352_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void RemotingServices_t2352_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void RemotingServices_t2352_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeEntry_t2341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void WellKnownObjectMode_t2357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void BinaryFormatter_t2351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void BinaryFormatter_t2351_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void BinaryFormatter_t2351_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FormatterAssemblyStyle_t2370_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FormatterTypeStyle_t2371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeFilterLevel_t2372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FormatterConverter_t2373_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void FormatterConverter_t2373_CustomAttributesCacheGenerator_FormatterConverter_ToUInt32_m12412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FormatterServices_t2374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IDeserializationCallback_t1609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IFormatter_t2688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IFormatterConverter_t2390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IObjectReference_t2615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ISerializationSurrogate_t2382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ISurrogateSelector_t2331_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ObjectManager_t2368_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void OnDeserializedAttribute_t2383_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void OnDeserializingAttribute_t2384_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void OnSerializedAttribute_t2385_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void OnSerializingAttribute_t2386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SerializationBinder_t2363_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SerializationEntry_t2389_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SerializationException_t2000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SerializationInfo_t1382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_GetUInt32_m12479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SerializationInfoEnumerator_t2391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StreamingContext_t1383_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StreamingContextStates_t2392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("X509ContentType.SerializedCert isn't supported (anywhere in the class)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m9523(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use the Issuer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_GetName_m9524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("Use the Subject property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Equals_m9515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Import_m9357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Reset_m9358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void X509KeyStorageFlags_t2036_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsymmetricAlgorithm_t1789_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsymmetricKeyExchangeFormatter_t2393_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsymmetricSignatureDeformatter_t1756_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AsymmetricSignatureFormatter_t1758_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CipherMode_t1843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CryptoConfig_t1804_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void CryptoConfig_t1804_CustomAttributesCacheGenerator_CryptoConfig_t1804_CryptoConfig_CreateFromName_m9366_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CryptographicException_t1805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CryptographicUnexpectedOperationException_t1821_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CspParameters_t1806_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void CspProviderFlags_t2395_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DES_t1823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DESCryptoServiceProvider_t2398_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSA_t1697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1813____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSAParameters_t1801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSASignatureDeformatter_t1817_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DSASignatureFormatter_t2399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMAC_t1812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACMD5_t2400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACRIPEMD160_t2401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACSHA1_t1811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACSHA256_t2402_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACSHA384_t2403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HMACSHA512_t2404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HashAlgorithm_t1680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICryptoTransform_t1727_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICspAsymmetricAlgorithm_t2689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void KeyedHashAlgorithm_t1720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MACTripleDES_t2405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MD5_t1814_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MD5CryptoServiceProvider_t2406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PaddingMode_t2407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RC2_t1824_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RC2CryptoServiceProvider_t2408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RIPEMD160_t2410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RIPEMD160Managed_t2411_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSA_t1691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1807_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1807____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSAPKCS1KeyExchangeFormatter_t1838_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureDeformatter_t1818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureFormatter_t2413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RSAParameters_t1774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Rijndael_t1826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RijndaelManaged_t2414_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RijndaelManagedTransform_t2416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA1_t1815_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA1CryptoServiceProvider_t2418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA1Managed_t2419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA256_t1816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA256Managed_t2420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA384_t2421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA384Managed_t2423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA512_t2424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SHA512Managed_t2425_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SignatureDescription_t2427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SymmetricAlgorithm_t1687_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ToBase64Transform_t2430_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TripleDES_t1825_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TripleDESCryptoServiceProvider_t2431_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StrongNamePublicKeyBlob_t2433_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ApplicationTrust_t2435_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Evidence_t2235_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Evidence_t2235_CustomAttributesCacheGenerator_Evidence_Equals_m12818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Evidence_t2235_CustomAttributesCacheGenerator_Evidence_GetHashCode_m12820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Hash_t2437_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IIdentityPermissionFactory_t2691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StrongName_t2438_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IPrincipal_t2477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PrincipalPolicy_t2439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IPermission_t2441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ISecurityEncodable_t2692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SecurityElement_t2128_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SecurityException_t2442_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SecurityException_t2442_CustomAttributesCacheGenerator_SecurityException_t2442____Demanded_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void SecuritySafeCriticalAttribute_t1443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SuppressUnmanagedCodeSecurityAttribute_t2443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 5188, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void UnverifiableCodeAttribute_t2444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 2, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2445_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m12880(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m12881(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m12882(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("we have simple override to match method signature."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Decoder_t2181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Decoder_t2181_CustomAttributesCacheGenerator_Decoder_t2181____Fallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Decoder_t2181_CustomAttributesCacheGenerator_Decoder_t2181____FallbackBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void DecoderReplacementFallback_t2451_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m12905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void EncoderReplacementFallback_t2458_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m12935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361_Encoding_InvokeI18N_m12966_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_GetByteCount_m12982(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_GetBytes_m12983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____IsReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____DecoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____EncoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t508_il2cpp_TypeInfo_var;
void StringBuilder_t423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		DefaultMemberAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t508 * tmp;
		tmp = (DefaultMemberAttribute_t508 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t508_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1956(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_t423_StringBuilder_AppendFormat_m8243_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_t423_StringBuilder_AppendFormat_m13013_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13023(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13032(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13043(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13057(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF8Encoding_t2468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("EncoderFallback is not handled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13089(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2470_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13097(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13100(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EventResetMode_t2471_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EventWaitHandle_t2472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void ExecutionContext_t2312_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13116(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void ExecutionContext_t2312_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13117(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Interlocked_t2473_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m13118(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ManualResetEvent_t1750_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Monitor_t2474_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Monitor_t2474_CustomAttributesCacheGenerator_Monitor_Exit_m4314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Mutex_t2307_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Mutex_t2307_CustomAttributesCacheGenerator_Mutex__ctor_m13119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Mutex_t2307_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SynchronizationLockException_t2476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Thread_t2682_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Thread_t2682_0_0_0_var = il2cpp_codegen_type_from_index(4550);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Thread_t2682_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator_local_slots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator__ec(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13132(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator_Thread_Finalize_m13143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Thread_t2308_CustomAttributesCacheGenerator_Thread_GetHashCode_m13147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ThreadAbortException_t2478_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ThreadInterruptedException_t2479_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ThreadState_t2480_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ThreadStateException_t2481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void WaitHandle_t1802_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void WaitHandle_t1802_CustomAttributesCacheGenerator_WaitHandle_t1802____Handle_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper("In the profiles > 2.x, use SafeHandle instead of Handle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AccessViolationException_t2482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ActivationContext_t2483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void ActivationContext_t2483_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13167(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization support"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Activator_t2681_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var;
void Activator_t2484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Activator_t2681_0_0_0_var = il2cpp_codegen_type_from_index(4552);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2282 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2282 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2282_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12057(tmp, il2cpp_codegen_type_get_object(_Activator_t2681_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t502_il2cpp_TypeInfo_var;
void Activator_t2484_CustomAttributesCacheGenerator_Activator_t2484_Activator_CreateInstance_m13172_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t502 * tmp;
		tmp = (ParamArrayAttribute_t502 *)il2cpp_codegen_object_new (ParamArrayAttribute_t502_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2498(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
void AppDomain_t2485_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void AppDomain_t2485_CustomAttributesCacheGenerator_type_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void AppDomain_t2485_CustomAttributesCacheGenerator_assembly_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void AppDomain_t2485_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void AppDomain_t2485_CustomAttributesCacheGenerator__principal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AppDomainManager_t2486_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AppDomainSetup_t2493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4505);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2280 * tmp;
		tmp = (ClassInterfaceAttribute_t2280 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2280_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12056(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ApplicationException_t2494_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2487_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2487_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArgumentException_t470_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArgumentNullException_t1404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArgumentOutOfRangeException_t1406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArithmeticException_t1803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ArrayTypeMismatchException_t2495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyLoadEventArgs_t2496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void AttributeTargets_t2497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void BitConverter_t860_CustomAttributesCacheGenerator_BitConverter_ToUInt16_m4534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Buffer_t2498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void CharEnumerator_t2499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ContextBoundObject_t2500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13248(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13249(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13259(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13263(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13264(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13321(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13323(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13324(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13325(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13355(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13360(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13361(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13362(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13363(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13364(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13367(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13389(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13390(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13395(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m6954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m6923(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13410(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m6922(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m6955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
void Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DBNull_t2501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DateTimeKind_t2503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void DateTimeOffset_t1420_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10319(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DayOfWeek_t2505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DivideByZeroException_t2508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void DllNotFoundException_t2509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EntryPointNotFoundException_t2511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void MonoEnumInfo_t2516_CustomAttributesCacheGenerator_cache(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Environment_t2519_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SpecialFolder_t2517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EventArgs_t1688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ExecutionEngineException_t2520_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FieldAccessException_t2521_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FlagsAttribute_t489_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 16, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void FormatException_t1399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void GC_t2523_CustomAttributesCacheGenerator_GC_SuppressFinalize_m8251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Guid_t108_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ICustomFormatter_t2600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IFormatProvider_t2583_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void IndexOutOfRangeException_t1394_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void InvalidCastException_t2524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void InvalidOperationException_t1828_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void LoaderOptimization_t2525_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void LoaderOptimization_t2525_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m9494(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t493_il2cpp_TypeInfo_var;
void LoaderOptimization_t2525_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t493 * tmp;
		tmp = (ObsoleteAttribute_t493 *)il2cpp_codegen_object_new (ObsoleteAttribute_t493_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m9494(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Math_t2526_CustomAttributesCacheGenerator_Math_Max_m4344(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Math_t2526_CustomAttributesCacheGenerator_Math_Max_m8257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Math_t2526_CustomAttributesCacheGenerator_Math_Min_m13620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void Math_t2526_CustomAttributesCacheGenerator_Math_Sqrt_m13629(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MemberAccessException_t2522_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MethodAccessException_t2527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MissingFieldException_t2528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MissingMemberException_t2529_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MissingMethodException_t2530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MulticastNotSupportedException_t2536_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void NonSerializedAttribute_t2537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void NotImplementedException_t1810_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void NotSupportedException_t435_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void NullReferenceException_t800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var;
void NumberFormatter_t2539_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4551);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2550 * tmp;
		tmp = (ThreadStaticAttribute_t2550 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2550_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13865(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ObjectDisposedException_t1809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void OperatingSystem_t2518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void OutOfMemoryException_t2540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void OverflowException_t2541_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void PlatformID_t2542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Random_t1270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void RankException_t2543_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ResolveEventArgs_t2544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2064_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2545_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		MonoTODOAttribute_t2064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4515);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2064 * tmp;
		tmp = (MonoTODOAttribute_t2064 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2064_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10320(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2545_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m13847(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StringComparer_t1390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void StringComparison_t2548_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void StringSplitOptions_t2549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void SystemException_t2004_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ThreadStaticAttribute_t2550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7109(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TimeSpan_t112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TimeZone_t2551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeCode_t2553_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeInitializationException_t2554_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeLoadException_t2510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnauthorizedAccessException_t2555_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m13929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m13930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2276 * tmp;
		tmp = (ReliabilityContractAttribute_t2276 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2276_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12055(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void Version_t1875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void WeakReference_t2344_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void MemberFilter_t2046_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void TypeFilter_t2251_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void HeaderHandler_t2561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AppDomainInitializer_t2492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void AssemblyLoadEventHandler_t2488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void EventHandler_t2490_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void ResolveEventHandler_t2489_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
void UnhandledExceptionEventHandler_t2491_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t2582_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_mscorlib_Assembly_AttributeGenerators[951] = 
{
	NULL,
	g_mscorlib_Assembly_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator_Object__ctor_m314,
	Object_t_CustomAttributesCacheGenerator_Object_Finalize_m515,
	Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m6924,
	ValueType_t524_CustomAttributesCacheGenerator,
	Attribute_t138_CustomAttributesCacheGenerator,
	_Attribute_t901_CustomAttributesCacheGenerator,
	Int32_t127_CustomAttributesCacheGenerator,
	IFormattable_t164_CustomAttributesCacheGenerator,
	IConvertible_t165_CustomAttributesCacheGenerator,
	IComparable_t166_CustomAttributesCacheGenerator,
	SerializableAttribute_t2042_CustomAttributesCacheGenerator,
	AttributeUsageAttribute_t1452_CustomAttributesCacheGenerator,
	ComVisibleAttribute_t485_CustomAttributesCacheGenerator,
	Int64_t1092_CustomAttributesCacheGenerator,
	UInt32_t1075_CustomAttributesCacheGenerator,
	UInt32_t1075_CustomAttributesCacheGenerator_UInt32_Parse_m9625,
	UInt32_t1075_CustomAttributesCacheGenerator_UInt32_Parse_m9626,
	UInt32_t1075_CustomAttributesCacheGenerator_UInt32_TryParse_m9387,
	UInt32_t1075_CustomAttributesCacheGenerator_UInt32_TryParse_m6985,
	CLSCompliantAttribute_t1587_CustomAttributesCacheGenerator,
	UInt64_t1091_CustomAttributesCacheGenerator,
	UInt64_t1091_CustomAttributesCacheGenerator_UInt64_Parse_m9649,
	UInt64_t1091_CustomAttributesCacheGenerator_UInt64_Parse_m9651,
	UInt64_t1091_CustomAttributesCacheGenerator_UInt64_TryParse_m6970,
	Byte_t449_CustomAttributesCacheGenerator,
	SByte_t170_CustomAttributesCacheGenerator,
	SByte_t170_CustomAttributesCacheGenerator_SByte_Parse_m9701,
	SByte_t170_CustomAttributesCacheGenerator_SByte_Parse_m9702,
	SByte_t170_CustomAttributesCacheGenerator_SByte_TryParse_m9703,
	Int16_t534_CustomAttributesCacheGenerator,
	UInt16_t454_CustomAttributesCacheGenerator,
	UInt16_t454_CustomAttributesCacheGenerator_UInt16_Parse_m9756,
	UInt16_t454_CustomAttributesCacheGenerator_UInt16_Parse_m9757,
	UInt16_t454_CustomAttributesCacheGenerator_UInt16_TryParse_m9758,
	UInt16_t454_CustomAttributesCacheGenerator_UInt16_TryParse_m9759,
	IEnumerator_t410_CustomAttributesCacheGenerator,
	IEnumerable_t550_CustomAttributesCacheGenerator,
	IEnumerable_t550_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14038,
	IDisposable_t144_CustomAttributesCacheGenerator,
	Char_t451_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator_String__ctor_m9793,
	String_t_CustomAttributesCacheGenerator_String_Equals_m9816,
	String_t_CustomAttributesCacheGenerator_String_Equals_m9817,
	String_t_CustomAttributesCacheGenerator_String_t_String_Split_m331_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_Split_m9821,
	String_t_CustomAttributesCacheGenerator_String_Split_m9822,
	String_t_CustomAttributesCacheGenerator_String_Split_m9352,
	String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m6932_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m9389_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m9350_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m2371_Arg1_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m8315_Arg2_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m9851_Arg3_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m388_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m6946_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_GetHashCode_m9860,
	ICloneable_t512_CustomAttributesCacheGenerator,
	Single_t151_CustomAttributesCacheGenerator,
	Single_t151_CustomAttributesCacheGenerator_Single_IsNaN_m9896,
	Double_t1407_CustomAttributesCacheGenerator,
	Double_t1407_CustomAttributesCacheGenerator_Double_IsNaN_m9924,
	Decimal_t1059_CustomAttributesCacheGenerator,
	Decimal_t1059_CustomAttributesCacheGenerator_MinValue,
	Decimal_t1059_CustomAttributesCacheGenerator_MaxValue,
	Decimal_t1059_CustomAttributesCacheGenerator_MinusOne,
	Decimal_t1059_CustomAttributesCacheGenerator_One,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal__ctor_m9935,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal__ctor_m9937,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_Compare_m9968,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9994,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9996,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m9998,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Explicit_m5548,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10001,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10003,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10005,
	Decimal_t1059_CustomAttributesCacheGenerator_Decimal_op_Implicit_m5544,
	Boolean_t169_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4497,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4294,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10038,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10041,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m4293,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m4356,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m4376,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10046,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10047,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10048,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10049,
	ISerializable_t513_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10052,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10059,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10067,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10068,
	MulticastDelegate_t307_CustomAttributesCacheGenerator,
	Delegate_t143_CustomAttributesCacheGenerator,
	Delegate_t143_CustomAttributesCacheGenerator_Delegate_Combine_m10088,
	Delegate_t143_CustomAttributesCacheGenerator_Delegate_t143_Delegate_Combine_m10088_Arg0_ParameterInfo,
	Enum_t167_CustomAttributesCacheGenerator,
	Enum_t167_CustomAttributesCacheGenerator_Enum_GetName_m10096,
	Enum_t167_CustomAttributesCacheGenerator_Enum_IsDefined_m8335,
	Enum_t167_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10098,
	Enum_t167_CustomAttributesCacheGenerator_Enum_Parse_m9372,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToString_m530,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToString_m518,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10102,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10103,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10104,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10105,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10106,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10107,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10108,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10109,
	Enum_t167_CustomAttributesCacheGenerator_Enum_ToObject_m10110,
	Enum_t167_CustomAttributesCacheGenerator_Enum_Format_m10114,
	Array_t_CustomAttributesCacheGenerator,
	Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10128,
	Array_t_CustomAttributesCacheGenerator_Array_get_Length_m9317,
	Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m10137,
	Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m9320,
	Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10140,
	Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10141,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10142_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10143_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10153,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10157,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10158,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10159,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10160,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10161,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10162,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10168_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10171_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10172,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10172_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10173,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10173_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10174,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10175,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10176,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10177,
	Array_t_CustomAttributesCacheGenerator_Array_Clear_m8250,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m9379,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10181,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10182,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10183,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10184,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10185,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10186,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10188,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10189,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10190,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8244,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8281,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10192,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10193,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10194,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10195,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10196,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10197,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10198,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10199,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14055,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14056,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14057,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14058,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14059,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14060,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14061,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14062,
	Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10212,
	Array_t_CustomAttributesCacheGenerator_Array_Resize_m14070,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14081,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14082,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14083,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14084,
	Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10213,
	Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo,
	ArrayReadOnlyList_1_t2631_CustomAttributesCacheGenerator,
	ArrayReadOnlyList_1_t2631_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14111,
	U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14118,
	U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14119,
	U3CGetEnumeratorU3Ec__Iterator0_t2632_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14121,
	ICollection_t1513_CustomAttributesCacheGenerator,
	IList_t1514_CustomAttributesCacheGenerator,
	IList_1_t2633_CustomAttributesCacheGenerator,
	Void_t168_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10250,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10266,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10267,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10268,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10269,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14172,
	Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10279_Arg0_ParameterInfo,
	MemberInfo_t_CustomAttributesCacheGenerator,
	ICustomAttributeProvider_t2599_CustomAttributesCacheGenerator,
	_MemberInfo_t2636_CustomAttributesCacheGenerator,
	IReflect_t2637_CustomAttributesCacheGenerator,
	_Type_t2635_CustomAttributesCacheGenerator,
	Exception_t140_CustomAttributesCacheGenerator,
	_Exception_t1473_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2048_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2048_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10290,
	RuntimeTypeHandle_t2047_CustomAttributesCacheGenerator,
	RuntimeTypeHandle_t2047_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10295,
	ParamArrayAttribute_t502_CustomAttributesCacheGenerator,
	OutAttribute_t2049_CustomAttributesCacheGenerator,
	ObsoleteAttribute_t493_CustomAttributesCacheGenerator,
	DllImportAttribute_t2050_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2051_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2051_CustomAttributesCacheGenerator_MarshalType,
	MarshalAsAttribute_t2051_CustomAttributesCacheGenerator_MarshalTypeRef,
	InAttribute_t2052_CustomAttributesCacheGenerator,
	ConditionalAttribute_t2053_CustomAttributesCacheGenerator,
	GuidAttribute_t479_CustomAttributesCacheGenerator,
	ComImportAttribute_t2054_CustomAttributesCacheGenerator,
	OptionalAttribute_t2055_CustomAttributesCacheGenerator,
	CompilerGeneratedAttribute_t162_CustomAttributesCacheGenerator,
	InternalsVisibleToAttribute_t896_CustomAttributesCacheGenerator,
	RuntimeCompatibilityAttribute_t160_CustomAttributesCacheGenerator,
	DebuggerHiddenAttribute_t497_CustomAttributesCacheGenerator,
	DefaultMemberAttribute_t508_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2056_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2056_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10306,
	FieldOffsetAttribute_t2057_CustomAttributesCacheGenerator,
	RuntimeArgumentHandle_t2058_CustomAttributesCacheGenerator,
	AsyncCallback_t305_CustomAttributesCacheGenerator,
	IAsyncResult_t304_CustomAttributesCacheGenerator,
	TypedReference_t2059_CustomAttributesCacheGenerator,
	MarshalByRefObject_t1885_CustomAttributesCacheGenerator,
	Locale_t2063_CustomAttributesCacheGenerator_Locale_t2063_Locale_GetText_m10318_Arg1_ParameterInfo,
	MonoTODOAttribute_t2064_CustomAttributesCacheGenerator,
	MonoDocumentationNoteAttribute_t2065_CustomAttributesCacheGenerator,
	SafeHandleZeroOrMinusOneIsInvalid_t2066_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10322,
	SafeWaitHandle_t2068_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10324,
	MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3,
	MSCompatUnicodeTable_t2078_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4,
	SortKey_t2088_CustomAttributesCacheGenerator,
	PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	PKCS12_t2116_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509CertificateCollection_t2115_CustomAttributesCacheGenerator,
	X509ExtensionCollection_t2118_CustomAttributesCacheGenerator,
	ASN1_t2112_CustomAttributesCacheGenerator,
	SmallXmlParser_t2130_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18,
	Dictionary_2_t2643_CustomAttributesCacheGenerator,
	Dictionary_2_t2643_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	Dictionary_2_t2643_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14265,
	KeyCollection_t2646_CustomAttributesCacheGenerator,
	ValueCollection_t2648_CustomAttributesCacheGenerator,
	IDictionary_2_t2655_CustomAttributesCacheGenerator,
	KeyNotFoundException_t2137_CustomAttributesCacheGenerator,
	KeyValuePair_2_t2657_CustomAttributesCacheGenerator,
	List_1_t2658_CustomAttributesCacheGenerator,
	Collection_1_t2660_CustomAttributesCacheGenerator,
	ReadOnlyCollection_1_t2661_CustomAttributesCacheGenerator,
	ArrayList_t1668_CustomAttributesCacheGenerator,
	ArrayListWrapper_t2139_CustomAttributesCacheGenerator,
	SynchronizedArrayListWrapper_t2140_CustomAttributesCacheGenerator,
	ReadOnlyArrayListWrapper_t2142_CustomAttributesCacheGenerator,
	BitArray_t1978_CustomAttributesCacheGenerator,
	CaseInsensitiveComparer_t1998_CustomAttributesCacheGenerator,
	CaseInsensitiveHashCodeProvider_t1999_CustomAttributesCacheGenerator,
	CollectionBase_t1701_CustomAttributesCacheGenerator,
	Comparer_t2145_CustomAttributesCacheGenerator,
	DictionaryEntry_t1996_CustomAttributesCacheGenerator,
	Hashtable_t1736_CustomAttributesCacheGenerator,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m10967,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9313,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m10970,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9314,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable__ctor_m9341,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_Clear_m10986,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_Remove_m10989,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m10993,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_t1736____comparer_PropertyInfo,
	Hashtable_t1736_CustomAttributesCacheGenerator_Hashtable_t1736____hcp_PropertyInfo,
	HashKeys_t2150_CustomAttributesCacheGenerator,
	HashValues_t2151_CustomAttributesCacheGenerator,
	IComparer_t1852_CustomAttributesCacheGenerator,
	IDictionary_t1931_CustomAttributesCacheGenerator,
	IDictionaryEnumerator_t1995_CustomAttributesCacheGenerator,
	IEqualityComparer_t1858_CustomAttributesCacheGenerator,
	IHashCodeProvider_t1857_CustomAttributesCacheGenerator,
	SortedList_t2002_CustomAttributesCacheGenerator,
	Stack_t1353_CustomAttributesCacheGenerator,
	AssemblyHashAlgorithm_t2158_CustomAttributesCacheGenerator,
	AssemblyVersionCompatibility_t2159_CustomAttributesCacheGenerator,
	SuppressMessageAttribute_t1449_CustomAttributesCacheGenerator,
	DebuggableAttribute_t897_CustomAttributesCacheGenerator,
	DebuggingModes_t2160_CustomAttributesCacheGenerator,
	DebuggerDisplayAttribute_t2161_CustomAttributesCacheGenerator,
	DebuggerStepThroughAttribute_t2162_CustomAttributesCacheGenerator,
	DebuggerTypeProxyAttribute_t2163_CustomAttributesCacheGenerator,
	StackFrame_t1433_CustomAttributesCacheGenerator,
	StackTrace_t1381_CustomAttributesCacheGenerator,
	Calendar_t2165_CustomAttributesCacheGenerator,
	CompareInfo_t1827_CustomAttributesCacheGenerator,
	CompareOptions_t2169_CustomAttributesCacheGenerator,
	CultureInfo_t1405_CustomAttributesCacheGenerator,
	CultureInfo_t1405_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19,
	CultureInfo_t1405_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A,
	DateTimeFormatFlags_t2173_CustomAttributesCacheGenerator,
	DateTimeFormatInfo_t2171_CustomAttributesCacheGenerator,
	DateTimeStyles_t2174_CustomAttributesCacheGenerator,
	DaylightTime_t2175_CustomAttributesCacheGenerator,
	GregorianCalendar_t2176_CustomAttributesCacheGenerator,
	GregorianCalendarTypes_t2177_CustomAttributesCacheGenerator,
	NumberFormatInfo_t2170_CustomAttributesCacheGenerator,
	NumberStyles_t2178_CustomAttributesCacheGenerator,
	TextInfo_t2085_CustomAttributesCacheGenerator,
	TextInfo_t2085_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11239,
	TextInfo_t2085_CustomAttributesCacheGenerator_TextInfo_t2085____CultureName_PropertyInfo,
	UnicodeCategory_t2037_CustomAttributesCacheGenerator,
	IsolatedStorageException_t2180_CustomAttributesCacheGenerator,
	BinaryReader_t2182_CustomAttributesCacheGenerator,
	BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11269,
	BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11272,
	BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11273,
	BinaryReader_t2182_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11274,
	Directory_t2183_CustomAttributesCacheGenerator,
	DirectoryInfo_t2184_CustomAttributesCacheGenerator,
	DirectoryNotFoundException_t2186_CustomAttributesCacheGenerator,
	EndOfStreamException_t2187_CustomAttributesCacheGenerator,
	File_t2188_CustomAttributesCacheGenerator,
	FileAccess_t2001_CustomAttributesCacheGenerator,
	FileAttributes_t2189_CustomAttributesCacheGenerator,
	FileMode_t2190_CustomAttributesCacheGenerator,
	FileNotFoundException_t2191_CustomAttributesCacheGenerator,
	FileOptions_t2192_CustomAttributesCacheGenerator,
	FileShare_t2193_CustomAttributesCacheGenerator,
	FileStream_t1820_CustomAttributesCacheGenerator,
	FileSystemInfo_t2185_CustomAttributesCacheGenerator,
	FileSystemInfo_t2185_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11353,
	IOException_t1836_CustomAttributesCacheGenerator,
	MemoryStream_t1398_CustomAttributesCacheGenerator,
	Path_t881_CustomAttributesCacheGenerator,
	Path_t881_CustomAttributesCacheGenerator_InvalidPathChars,
	PathTooLongException_t2201_CustomAttributesCacheGenerator,
	SeekOrigin_t1844_CustomAttributesCacheGenerator,
	Stream_t1751_CustomAttributesCacheGenerator,
	StreamReader_t2206_CustomAttributesCacheGenerator,
	StreamWriter_t2207_CustomAttributesCacheGenerator,
	StringReader_t1397_CustomAttributesCacheGenerator,
	TextReader_t2133_CustomAttributesCacheGenerator,
	TextWriter_t2006_CustomAttributesCacheGenerator,
	AssemblyBuilder_t2215_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2218_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2218_CustomAttributesCacheGenerator_ConstructorBuilder_t2218____CallingConvention_PropertyInfo,
	EnumBuilder_t2219_CustomAttributesCacheGenerator,
	EnumBuilder_t2219_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m11592,
	FieldBuilder_t2221_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m11628,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m11631,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m11672,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m11673,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m11674,
	GenericTypeParameterBuilder_t2223_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2223_GenericTypeParameterBuilder_MakeGenericType_m11674_Arg0_ParameterInfo,
	MethodBuilder_t2222_CustomAttributesCacheGenerator,
	MethodBuilder_t2222_CustomAttributesCacheGenerator_MethodBuilder_Equals_m11690,
	MethodBuilder_t2222_CustomAttributesCacheGenerator_MethodBuilder_t2222_MethodBuilder_MakeGenericMethod_m11693_Arg0_ParameterInfo,
	ModuleBuilder_t2225_CustomAttributesCacheGenerator,
	ParameterBuilder_t2227_CustomAttributesCacheGenerator,
	PropertyBuilder_t2228_CustomAttributesCacheGenerator,
	TypeBuilder_t2216_CustomAttributesCacheGenerator,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m11737,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m11756,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_t2216_TypeBuilder_MakeGenericType_m11756_Arg0_ParameterInfo,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m11763,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m11764,
	TypeBuilder_t2216_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m11765,
	UnmanagedMarshal_t2220_CustomAttributesCacheGenerator,
	AmbiguousMatchException_t2233_CustomAttributesCacheGenerator,
	Assembly_t2003_CustomAttributesCacheGenerator,
	AssemblyCompanyAttribute_t482_CustomAttributesCacheGenerator,
	AssemblyConfigurationAttribute_t481_CustomAttributesCacheGenerator,
	AssemblyCopyrightAttribute_t484_CustomAttributesCacheGenerator,
	AssemblyDefaultAliasAttribute_t1584_CustomAttributesCacheGenerator,
	AssemblyDelaySignAttribute_t1586_CustomAttributesCacheGenerator,
	AssemblyDescriptionAttribute_t480_CustomAttributesCacheGenerator,
	AssemblyFileVersionAttribute_t487_CustomAttributesCacheGenerator,
	AssemblyInformationalVersionAttribute_t1582_CustomAttributesCacheGenerator,
	AssemblyKeyFileAttribute_t1585_CustomAttributesCacheGenerator,
	AssemblyName_t2238_CustomAttributesCacheGenerator,
	AssemblyNameFlags_t2239_CustomAttributesCacheGenerator,
	AssemblyProductAttribute_t483_CustomAttributesCacheGenerator,
	AssemblyTitleAttribute_t486_CustomAttributesCacheGenerator,
	AssemblyTrademarkAttribute_t488_CustomAttributesCacheGenerator,
	Binder_t1431_CustomAttributesCacheGenerator,
	Default_t2240_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m11817,
	BindingFlags_t2241_CustomAttributesCacheGenerator,
	CallingConventions_t2242_CustomAttributesCacheGenerator,
	ConstructorInfo_t1287_CustomAttributesCacheGenerator,
	ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorName,
	ConstructorInfo_t1287_CustomAttributesCacheGenerator_TypeConstructorName,
	ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7013,
	ConstructorInfo_t1287_CustomAttributesCacheGenerator_ConstructorInfo_t1287____MemberType_PropertyInfo,
	EventAttributes_t2243_CustomAttributesCacheGenerator,
	EventInfo_t_CustomAttributesCacheGenerator,
	FieldAttributes_t2245_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m11850,
	MemberTypes_t2247_CustomAttributesCacheGenerator,
	MethodAttributes_t2248_CustomAttributesCacheGenerator,
	MethodBase_t1434_CustomAttributesCacheGenerator,
	MethodBase_t1434_CustomAttributesCacheGenerator_MethodBase_Invoke_m11867,
	MethodBase_t1434_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m11872,
	MethodImplAttributes_t2249_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m11879_Arg0_ParameterInfo,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m11880,
	Missing_t2250_CustomAttributesCacheGenerator,
	Missing_t2250_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11886,
	Module_t2226_CustomAttributesCacheGenerator,
	PInfo_t2257_CustomAttributesCacheGenerator,
	ParameterAttributes_t2259_CustomAttributesCacheGenerator,
	ParameterInfo_t1426_CustomAttributesCacheGenerator,
	ParameterModifier_t2260_CustomAttributesCacheGenerator,
	Pointer_t2261_CustomAttributesCacheGenerator,
	ProcessorArchitecture_t2262_CustomAttributesCacheGenerator,
	PropertyAttributes_t2263_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12036,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12037,
	StrongNameKeyPair_t2237_CustomAttributesCacheGenerator,
	TargetException_t2264_CustomAttributesCacheGenerator,
	TargetInvocationException_t2265_CustomAttributesCacheGenerator,
	TargetParameterCountException_t2266_CustomAttributesCacheGenerator,
	TypeAttributes_t2267_CustomAttributesCacheGenerator,
	NeutralResourcesLanguageAttribute_t1588_CustomAttributesCacheGenerator,
	SatelliteContractVersionAttribute_t1583_CustomAttributesCacheGenerator,
	CompilationRelaxations_t2268_CustomAttributesCacheGenerator,
	CompilationRelaxationsAttribute_t898_CustomAttributesCacheGenerator,
	DefaultDependencyAttribute_t2269_CustomAttributesCacheGenerator,
	IsVolatile_t2270_CustomAttributesCacheGenerator,
	StringFreezingAttribute_t2272_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12053,
	CriticalFinalizerObject_t2275_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12054,
	ReliabilityContractAttribute_t2276_CustomAttributesCacheGenerator,
	ActivationArguments_t2277_CustomAttributesCacheGenerator,
	CallingConvention_t2278_CustomAttributesCacheGenerator,
	CharSet_t2279_CustomAttributesCacheGenerator,
	ClassInterfaceAttribute_t2280_CustomAttributesCacheGenerator,
	ClassInterfaceType_t2281_CustomAttributesCacheGenerator,
	ComDefaultInterfaceAttribute_t2282_CustomAttributesCacheGenerator,
	ComInterfaceType_t2283_CustomAttributesCacheGenerator,
	DispIdAttribute_t2284_CustomAttributesCacheGenerator,
	GCHandle_t805_CustomAttributesCacheGenerator,
	GCHandleType_t2285_CustomAttributesCacheGenerator,
	InterfaceTypeAttribute_t2286_CustomAttributesCacheGenerator,
	Marshal_t792_CustomAttributesCacheGenerator,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m12070,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m4273,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m4277,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_PtrToStructure_m4332,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_ReadInt32_m4578,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_ReadInt32_m12074,
	Marshal_t792_CustomAttributesCacheGenerator_Marshal_StructureToPtr_m4295,
	MarshalDirectiveException_t2287_CustomAttributesCacheGenerator,
	PreserveSigAttribute_t2288_CustomAttributesCacheGenerator,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle__ctor_m12078,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Close_m12079,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12080,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12081,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12082,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12083,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12084,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14546,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12085,
	SafeHandle_t2067_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14547,
	TypeLibImportClassAttribute_t2289_CustomAttributesCacheGenerator,
	TypeLibVersionAttribute_t2290_CustomAttributesCacheGenerator,
	UnmanagedType_t2291_CustomAttributesCacheGenerator,
	_Activator_t2681_CustomAttributesCacheGenerator,
	_Assembly_t2671_CustomAttributesCacheGenerator,
	_AssemblyBuilder_t2662_CustomAttributesCacheGenerator,
	_AssemblyName_t2672_CustomAttributesCacheGenerator,
	_ConstructorBuilder_t2663_CustomAttributesCacheGenerator,
	_ConstructorInfo_t2673_CustomAttributesCacheGenerator,
	_EnumBuilder_t2664_CustomAttributesCacheGenerator,
	_EventInfo_t2674_CustomAttributesCacheGenerator,
	_FieldBuilder_t2665_CustomAttributesCacheGenerator,
	_FieldInfo_t2675_CustomAttributesCacheGenerator,
	_MethodBase_t2676_CustomAttributesCacheGenerator,
	_MethodBuilder_t2666_CustomAttributesCacheGenerator,
	_MethodInfo_t2677_CustomAttributesCacheGenerator,
	_Module_t2678_CustomAttributesCacheGenerator,
	_ModuleBuilder_t2667_CustomAttributesCacheGenerator,
	_ParameterBuilder_t2668_CustomAttributesCacheGenerator,
	_ParameterInfo_t2679_CustomAttributesCacheGenerator,
	_PropertyBuilder_t2669_CustomAttributesCacheGenerator,
	_PropertyInfo_t2680_CustomAttributesCacheGenerator,
	_Thread_t2682_CustomAttributesCacheGenerator,
	_TypeBuilder_t2670_CustomAttributesCacheGenerator,
	IActivator_t2292_CustomAttributesCacheGenerator,
	IConstructionCallMessage_t2592_CustomAttributesCacheGenerator,
	UrlAttribute_t2298_CustomAttributesCacheGenerator,
	UrlAttribute_t2298_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12099,
	UrlAttribute_t2298_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12100,
	ChannelServices_t2302_CustomAttributesCacheGenerator,
	ChannelServices_t2302_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12104,
	CrossAppDomainSink_t2305_CustomAttributesCacheGenerator,
	IChannel_t2593_CustomAttributesCacheGenerator,
	IChannelReceiver_t2607_CustomAttributesCacheGenerator,
	IChannelSender_t2683_CustomAttributesCacheGenerator,
	Context_t2306_CustomAttributesCacheGenerator,
	ContextAttribute_t2299_CustomAttributesCacheGenerator,
	IContextAttribute_t2605_CustomAttributesCacheGenerator,
	IContextProperty_t2594_CustomAttributesCacheGenerator,
	IContributeClientContextSink_t2684_CustomAttributesCacheGenerator,
	IContributeServerContextSink_t2685_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2309_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2309_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12134,
	SynchronizationAttribute_t2309_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12135,
	AsyncResult_t2316_CustomAttributesCacheGenerator,
	ConstructionCall_t2317_CustomAttributesCacheGenerator,
	ConstructionCall_t2317_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20,
	ConstructionCallDictionary_t2319_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23,
	ConstructionCallDictionary_t2319_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24,
	Header_t2322_CustomAttributesCacheGenerator,
	IMessage_t2315_CustomAttributesCacheGenerator,
	IMessageCtrl_t2314_CustomAttributesCacheGenerator,
	IMessageSink_t1130_CustomAttributesCacheGenerator,
	IMethodCallMessage_t2596_CustomAttributesCacheGenerator,
	IMethodMessage_t2327_CustomAttributesCacheGenerator,
	IMethodReturnMessage_t2595_CustomAttributesCacheGenerator,
	IRemotingFormatter_t2686_CustomAttributesCacheGenerator,
	LogicalCallContext_t2324_CustomAttributesCacheGenerator,
	MethodCall_t2318_CustomAttributesCacheGenerator,
	MethodCall_t2318_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F,
	MethodDictionary_t2320_CustomAttributesCacheGenerator,
	MethodDictionary_t2320_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21,
	MethodDictionary_t2320_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22,
	RemotingSurrogateSelector_t2332_CustomAttributesCacheGenerator,
	ReturnMessage_t2333_CustomAttributesCacheGenerator,
	ProxyAttribute_t2334_CustomAttributesCacheGenerator,
	ProxyAttribute_t2334_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12271,
	ProxyAttribute_t2334_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12272,
	RealProxy_t2335_CustomAttributesCacheGenerator,
	ITrackingHandler_t2610_CustomAttributesCacheGenerator,
	TrackingServices_t2339_CustomAttributesCacheGenerator,
	ActivatedClientTypeEntry_t2340_CustomAttributesCacheGenerator,
	IChannelInfo_t2346_CustomAttributesCacheGenerator,
	IEnvoyInfo_t2348_CustomAttributesCacheGenerator,
	IRemotingTypeInfo_t2347_CustomAttributesCacheGenerator,
	ObjRef_t2343_CustomAttributesCacheGenerator,
	ObjRef_t2343_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26,
	ObjRef_t2343_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12309,
	RemotingConfiguration_t2349_CustomAttributesCacheGenerator,
	RemotingException_t2350_CustomAttributesCacheGenerator,
	RemotingServices_t2352_CustomAttributesCacheGenerator,
	RemotingServices_t2352_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12329,
	RemotingServices_t2352_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12333,
	TypeEntry_t2341_CustomAttributesCacheGenerator,
	WellKnownObjectMode_t2357_CustomAttributesCacheGenerator,
	BinaryFormatter_t2351_CustomAttributesCacheGenerator,
	BinaryFormatter_t2351_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField,
	BinaryFormatter_t2351_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12365,
	FormatterAssemblyStyle_t2370_CustomAttributesCacheGenerator,
	FormatterTypeStyle_t2371_CustomAttributesCacheGenerator,
	TypeFilterLevel_t2372_CustomAttributesCacheGenerator,
	FormatterConverter_t2373_CustomAttributesCacheGenerator,
	FormatterConverter_t2373_CustomAttributesCacheGenerator_FormatterConverter_ToUInt32_m12412,
	FormatterServices_t2374_CustomAttributesCacheGenerator,
	IDeserializationCallback_t1609_CustomAttributesCacheGenerator,
	IFormatter_t2688_CustomAttributesCacheGenerator,
	IFormatterConverter_t2390_CustomAttributesCacheGenerator,
	IObjectReference_t2615_CustomAttributesCacheGenerator,
	ISerializationSurrogate_t2382_CustomAttributesCacheGenerator,
	ISurrogateSelector_t2331_CustomAttributesCacheGenerator,
	ObjectManager_t2368_CustomAttributesCacheGenerator,
	OnDeserializedAttribute_t2383_CustomAttributesCacheGenerator,
	OnDeserializingAttribute_t2384_CustomAttributesCacheGenerator,
	OnSerializedAttribute_t2385_CustomAttributesCacheGenerator,
	OnSerializingAttribute_t2386_CustomAttributesCacheGenerator,
	SerializationBinder_t2363_CustomAttributesCacheGenerator,
	SerializationEntry_t2389_CustomAttributesCacheGenerator,
	SerializationException_t2000_CustomAttributesCacheGenerator,
	SerializationInfo_t1382_CustomAttributesCacheGenerator,
	SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12470,
	SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12476,
	SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12477,
	SerializationInfo_t1382_CustomAttributesCacheGenerator_SerializationInfo_GetUInt32_m12479,
	SerializationInfoEnumerator_t2391_CustomAttributesCacheGenerator,
	StreamingContext_t1383_CustomAttributesCacheGenerator,
	StreamingContextStates_t2392_CustomAttributesCacheGenerator,
	X509Certificate_t1771_CustomAttributesCacheGenerator,
	X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m9523,
	X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_GetName_m9524,
	X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Equals_m9515,
	X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Import_m9357,
	X509Certificate_t1771_CustomAttributesCacheGenerator_X509Certificate_Reset_m9358,
	X509KeyStorageFlags_t2036_CustomAttributesCacheGenerator,
	AsymmetricAlgorithm_t1789_CustomAttributesCacheGenerator,
	AsymmetricKeyExchangeFormatter_t2393_CustomAttributesCacheGenerator,
	AsymmetricSignatureDeformatter_t1756_CustomAttributesCacheGenerator,
	AsymmetricSignatureFormatter_t1758_CustomAttributesCacheGenerator,
	CipherMode_t1843_CustomAttributesCacheGenerator,
	CryptoConfig_t1804_CustomAttributesCacheGenerator,
	CryptoConfig_t1804_CustomAttributesCacheGenerator_CryptoConfig_t1804_CryptoConfig_CreateFromName_m9366_Arg1_ParameterInfo,
	CryptographicException_t1805_CustomAttributesCacheGenerator,
	CryptographicUnexpectedOperationException_t1821_CustomAttributesCacheGenerator,
	CspParameters_t1806_CustomAttributesCacheGenerator,
	CspProviderFlags_t2395_CustomAttributesCacheGenerator,
	DES_t1823_CustomAttributesCacheGenerator,
	DESCryptoServiceProvider_t2398_CustomAttributesCacheGenerator,
	DSA_t1697_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1813____PublicOnly_PropertyInfo,
	DSAParameters_t1801_CustomAttributesCacheGenerator,
	DSASignatureDeformatter_t1817_CustomAttributesCacheGenerator,
	DSASignatureFormatter_t2399_CustomAttributesCacheGenerator,
	HMAC_t1812_CustomAttributesCacheGenerator,
	HMACMD5_t2400_CustomAttributesCacheGenerator,
	HMACRIPEMD160_t2401_CustomAttributesCacheGenerator,
	HMACSHA1_t1811_CustomAttributesCacheGenerator,
	HMACSHA256_t2402_CustomAttributesCacheGenerator,
	HMACSHA384_t2403_CustomAttributesCacheGenerator,
	HMACSHA512_t2404_CustomAttributesCacheGenerator,
	HashAlgorithm_t1680_CustomAttributesCacheGenerator,
	ICryptoTransform_t1727_CustomAttributesCacheGenerator,
	ICspAsymmetricAlgorithm_t2689_CustomAttributesCacheGenerator,
	KeyedHashAlgorithm_t1720_CustomAttributesCacheGenerator,
	MACTripleDES_t2405_CustomAttributesCacheGenerator,
	MD5_t1814_CustomAttributesCacheGenerator,
	MD5CryptoServiceProvider_t2406_CustomAttributesCacheGenerator,
	PaddingMode_t2407_CustomAttributesCacheGenerator,
	RC2_t1824_CustomAttributesCacheGenerator,
	RC2CryptoServiceProvider_t2408_CustomAttributesCacheGenerator,
	RIPEMD160_t2410_CustomAttributesCacheGenerator,
	RIPEMD160Managed_t2411_CustomAttributesCacheGenerator,
	RSA_t1691_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1807_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1807_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1807____PublicOnly_PropertyInfo,
	RSAPKCS1KeyExchangeFormatter_t1838_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureDeformatter_t1818_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureFormatter_t2413_CustomAttributesCacheGenerator,
	RSAParameters_t1774_CustomAttributesCacheGenerator,
	Rijndael_t1826_CustomAttributesCacheGenerator,
	RijndaelManaged_t2414_CustomAttributesCacheGenerator,
	RijndaelManagedTransform_t2416_CustomAttributesCacheGenerator,
	SHA1_t1815_CustomAttributesCacheGenerator,
	SHA1CryptoServiceProvider_t2418_CustomAttributesCacheGenerator,
	SHA1Managed_t2419_CustomAttributesCacheGenerator,
	SHA256_t1816_CustomAttributesCacheGenerator,
	SHA256Managed_t2420_CustomAttributesCacheGenerator,
	SHA384_t2421_CustomAttributesCacheGenerator,
	SHA384Managed_t2423_CustomAttributesCacheGenerator,
	SHA512_t2424_CustomAttributesCacheGenerator,
	SHA512Managed_t2425_CustomAttributesCacheGenerator,
	SignatureDescription_t2427_CustomAttributesCacheGenerator,
	SymmetricAlgorithm_t1687_CustomAttributesCacheGenerator,
	ToBase64Transform_t2430_CustomAttributesCacheGenerator,
	TripleDES_t1825_CustomAttributesCacheGenerator,
	TripleDESCryptoServiceProvider_t2431_CustomAttributesCacheGenerator,
	StrongNamePublicKeyBlob_t2433_CustomAttributesCacheGenerator,
	ApplicationTrust_t2435_CustomAttributesCacheGenerator,
	Evidence_t2235_CustomAttributesCacheGenerator,
	Evidence_t2235_CustomAttributesCacheGenerator_Evidence_Equals_m12818,
	Evidence_t2235_CustomAttributesCacheGenerator_Evidence_GetHashCode_m12820,
	Hash_t2437_CustomAttributesCacheGenerator,
	IIdentityPermissionFactory_t2691_CustomAttributesCacheGenerator,
	StrongName_t2438_CustomAttributesCacheGenerator,
	IPrincipal_t2477_CustomAttributesCacheGenerator,
	PrincipalPolicy_t2439_CustomAttributesCacheGenerator,
	IPermission_t2441_CustomAttributesCacheGenerator,
	ISecurityEncodable_t2692_CustomAttributesCacheGenerator,
	SecurityElement_t2128_CustomAttributesCacheGenerator,
	SecurityException_t2442_CustomAttributesCacheGenerator,
	SecurityException_t2442_CustomAttributesCacheGenerator_SecurityException_t2442____Demanded_PropertyInfo,
	SecuritySafeCriticalAttribute_t1443_CustomAttributesCacheGenerator,
	SuppressUnmanagedCodeSecurityAttribute_t2443_CustomAttributesCacheGenerator,
	UnverifiableCodeAttribute_t2444_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2445_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m12880,
	ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m12881,
	ASCIIEncoding_t2445_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m12882,
	Decoder_t2181_CustomAttributesCacheGenerator,
	Decoder_t2181_CustomAttributesCacheGenerator_Decoder_t2181____Fallback_PropertyInfo,
	Decoder_t2181_CustomAttributesCacheGenerator_Decoder_t2181____FallbackBuffer_PropertyInfo,
	DecoderReplacementFallback_t2451_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m12905,
	EncoderReplacementFallback_t2458_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m12935,
	Encoding_t1361_CustomAttributesCacheGenerator,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361_Encoding_InvokeI18N_m12966_Arg1_ParameterInfo,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_GetByteCount_m12982,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_GetBytes_m12983,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____IsReadOnly_PropertyInfo,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____DecoderFallback_PropertyInfo,
	Encoding_t1361_CustomAttributesCacheGenerator_Encoding_t1361____EncoderFallback_PropertyInfo,
	StringBuilder_t423_CustomAttributesCacheGenerator,
	StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1956,
	StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1955,
	StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_t423_StringBuilder_AppendFormat_m8243_Arg1_ParameterInfo,
	StringBuilder_t423_CustomAttributesCacheGenerator_StringBuilder_t423_StringBuilder_AppendFormat_m13013_Arg2_ParameterInfo,
	UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13022,
	UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13023,
	UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13032,
	UTF32Encoding_t2463_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13034,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13042,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13043,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13055,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13056,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13057,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13058,
	UTF7Encoding_t2466_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13059,
	UTF8Encoding_t2468_CustomAttributesCacheGenerator,
	UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13068,
	UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13073,
	UTF8Encoding_t2468_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13089,
	UnicodeEncoding_t2470_CustomAttributesCacheGenerator,
	UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13097,
	UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13100,
	UnicodeEncoding_t2470_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13104,
	EventResetMode_t2471_CustomAttributesCacheGenerator,
	EventWaitHandle_t2472_CustomAttributesCacheGenerator,
	ExecutionContext_t2312_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13116,
	ExecutionContext_t2312_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13117,
	Interlocked_t2473_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m13118,
	ManualResetEvent_t1750_CustomAttributesCacheGenerator,
	Monitor_t2474_CustomAttributesCacheGenerator,
	Monitor_t2474_CustomAttributesCacheGenerator_Monitor_Exit_m4314,
	Mutex_t2307_CustomAttributesCacheGenerator,
	Mutex_t2307_CustomAttributesCacheGenerator_Mutex__ctor_m13119,
	Mutex_t2307_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13122,
	SynchronizationLockException_t2476_CustomAttributesCacheGenerator,
	Thread_t2308_CustomAttributesCacheGenerator,
	Thread_t2308_CustomAttributesCacheGenerator_local_slots,
	Thread_t2308_CustomAttributesCacheGenerator__ec,
	Thread_t2308_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13132,
	Thread_t2308_CustomAttributesCacheGenerator_Thread_Finalize_m13143,
	Thread_t2308_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13146,
	Thread_t2308_CustomAttributesCacheGenerator_Thread_GetHashCode_m13147,
	ThreadAbortException_t2478_CustomAttributesCacheGenerator,
	ThreadInterruptedException_t2479_CustomAttributesCacheGenerator,
	ThreadState_t2480_CustomAttributesCacheGenerator,
	ThreadStateException_t2481_CustomAttributesCacheGenerator,
	WaitHandle_t1802_CustomAttributesCacheGenerator,
	WaitHandle_t1802_CustomAttributesCacheGenerator_WaitHandle_t1802____Handle_PropertyInfo,
	AccessViolationException_t2482_CustomAttributesCacheGenerator,
	ActivationContext_t2483_CustomAttributesCacheGenerator,
	ActivationContext_t2483_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13167,
	Activator_t2484_CustomAttributesCacheGenerator,
	Activator_t2484_CustomAttributesCacheGenerator_Activator_t2484_Activator_CreateInstance_m13172_Arg1_ParameterInfo,
	AppDomain_t2485_CustomAttributesCacheGenerator,
	AppDomain_t2485_CustomAttributesCacheGenerator_type_resolve_in_progress,
	AppDomain_t2485_CustomAttributesCacheGenerator_assembly_resolve_in_progress,
	AppDomain_t2485_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly,
	AppDomain_t2485_CustomAttributesCacheGenerator__principal,
	AppDomainManager_t2486_CustomAttributesCacheGenerator,
	AppDomainSetup_t2493_CustomAttributesCacheGenerator,
	ApplicationException_t2494_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2487_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2487_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13193,
	ArgumentException_t470_CustomAttributesCacheGenerator,
	ArgumentNullException_t1404_CustomAttributesCacheGenerator,
	ArgumentOutOfRangeException_t1406_CustomAttributesCacheGenerator,
	ArithmeticException_t1803_CustomAttributesCacheGenerator,
	ArrayTypeMismatchException_t2495_CustomAttributesCacheGenerator,
	AssemblyLoadEventArgs_t2496_CustomAttributesCacheGenerator,
	AttributeTargets_t2497_CustomAttributesCacheGenerator,
	BitConverter_t860_CustomAttributesCacheGenerator_BitConverter_ToUInt16_m4534,
	Buffer_t2498_CustomAttributesCacheGenerator,
	CharEnumerator_t2499_CustomAttributesCacheGenerator,
	ContextBoundObject_t2500_CustomAttributesCacheGenerator,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13245,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13248,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13249,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToBoolean_m13250,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13259,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13263,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13264,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToByte_m13265,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13270,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13273,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13274,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToChar_m13275,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13283,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13284,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13285,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDateTime_m13286,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13293,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13296,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13297,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDecimal_m13298,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13307,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13310,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13311,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToDouble_m13312,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13321,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13323,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13324,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt16_m13325,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13335,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13338,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13339,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt32_m13340,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13349,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13353,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13354,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToInt64_m13355,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13358,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13359,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13360,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13361,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13362,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13363,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13364,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13365,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13366,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13367,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13368,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13369,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13370,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSByte_m13371,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13379,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13382,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13383,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToSingle_m13384,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13388,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13389,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13390,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13391,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13392,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13393,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13394,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13395,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13396,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13397,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13398,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13399,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13400,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m6954,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt16_m13401,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m6923,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13402,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13403,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13404,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13405,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13406,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13407,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13408,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13409,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13410,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13411,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13412,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13413,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m6922,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt32_m13414,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13415,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13416,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13417,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13418,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13419,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13420,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13421,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13422,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13423,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13424,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13425,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13426,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13427,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m6955,
	Convert_t1393_CustomAttributesCacheGenerator_Convert_ToUInt64_m13428,
	DBNull_t2501_CustomAttributesCacheGenerator,
	DateTimeKind_t2503_CustomAttributesCacheGenerator,
	DateTimeOffset_t1420_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13526,
	DayOfWeek_t2505_CustomAttributesCacheGenerator,
	DivideByZeroException_t2508_CustomAttributesCacheGenerator,
	DllNotFoundException_t2509_CustomAttributesCacheGenerator,
	EntryPointNotFoundException_t2511_CustomAttributesCacheGenerator,
	MonoEnumInfo_t2516_CustomAttributesCacheGenerator_cache,
	Environment_t2519_CustomAttributesCacheGenerator,
	SpecialFolder_t2517_CustomAttributesCacheGenerator,
	EventArgs_t1688_CustomAttributesCacheGenerator,
	ExecutionEngineException_t2520_CustomAttributesCacheGenerator,
	FieldAccessException_t2521_CustomAttributesCacheGenerator,
	FlagsAttribute_t489_CustomAttributesCacheGenerator,
	FormatException_t1399_CustomAttributesCacheGenerator,
	GC_t2523_CustomAttributesCacheGenerator_GC_SuppressFinalize_m8251,
	Guid_t108_CustomAttributesCacheGenerator,
	ICustomFormatter_t2600_CustomAttributesCacheGenerator,
	IFormatProvider_t2583_CustomAttributesCacheGenerator,
	IndexOutOfRangeException_t1394_CustomAttributesCacheGenerator,
	InvalidCastException_t2524_CustomAttributesCacheGenerator,
	InvalidOperationException_t1828_CustomAttributesCacheGenerator,
	LoaderOptimization_t2525_CustomAttributesCacheGenerator,
	LoaderOptimization_t2525_CustomAttributesCacheGenerator_DomainMask,
	LoaderOptimization_t2525_CustomAttributesCacheGenerator_DisallowBindings,
	Math_t2526_CustomAttributesCacheGenerator_Math_Max_m4344,
	Math_t2526_CustomAttributesCacheGenerator_Math_Max_m8257,
	Math_t2526_CustomAttributesCacheGenerator_Math_Min_m13620,
	Math_t2526_CustomAttributesCacheGenerator_Math_Sqrt_m13629,
	MemberAccessException_t2522_CustomAttributesCacheGenerator,
	MethodAccessException_t2527_CustomAttributesCacheGenerator,
	MissingFieldException_t2528_CustomAttributesCacheGenerator,
	MissingMemberException_t2529_CustomAttributesCacheGenerator,
	MissingMethodException_t2530_CustomAttributesCacheGenerator,
	MulticastNotSupportedException_t2536_CustomAttributesCacheGenerator,
	NonSerializedAttribute_t2537_CustomAttributesCacheGenerator,
	NotImplementedException_t1810_CustomAttributesCacheGenerator,
	NotSupportedException_t435_CustomAttributesCacheGenerator,
	NullReferenceException_t800_CustomAttributesCacheGenerator,
	NumberFormatter_t2539_CustomAttributesCacheGenerator_threadNumberFormatter,
	ObjectDisposedException_t1809_CustomAttributesCacheGenerator,
	OperatingSystem_t2518_CustomAttributesCacheGenerator,
	OutOfMemoryException_t2540_CustomAttributesCacheGenerator,
	OverflowException_t2541_CustomAttributesCacheGenerator,
	PlatformID_t2542_CustomAttributesCacheGenerator,
	Random_t1270_CustomAttributesCacheGenerator,
	RankException_t2543_CustomAttributesCacheGenerator,
	ResolveEventArgs_t2544_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2545_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2545_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m13847,
	StringComparer_t1390_CustomAttributesCacheGenerator,
	StringComparison_t2548_CustomAttributesCacheGenerator,
	StringSplitOptions_t2549_CustomAttributesCacheGenerator,
	SystemException_t2004_CustomAttributesCacheGenerator,
	ThreadStaticAttribute_t2550_CustomAttributesCacheGenerator,
	TimeSpan_t112_CustomAttributesCacheGenerator,
	TimeZone_t2551_CustomAttributesCacheGenerator,
	TypeCode_t2553_CustomAttributesCacheGenerator,
	TypeInitializationException_t2554_CustomAttributesCacheGenerator,
	TypeLoadException_t2510_CustomAttributesCacheGenerator,
	UnauthorizedAccessException_t2555_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m13929,
	UnhandledExceptionEventArgs_t2556_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m13930,
	Version_t1875_CustomAttributesCacheGenerator,
	WeakReference_t2344_CustomAttributesCacheGenerator,
	MemberFilter_t2046_CustomAttributesCacheGenerator,
	TypeFilter_t2251_CustomAttributesCacheGenerator,
	HeaderHandler_t2561_CustomAttributesCacheGenerator,
	AppDomainInitializer_t2492_CustomAttributesCacheGenerator,
	AssemblyLoadEventHandler_t2488_CustomAttributesCacheGenerator,
	EventHandler_t2490_CustomAttributesCacheGenerator,
	ResolveEventHandler_t2489_CustomAttributesCacheGenerator,
	UnhandledExceptionEventHandler_t2491_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2582_CustomAttributesCacheGenerator,
};
