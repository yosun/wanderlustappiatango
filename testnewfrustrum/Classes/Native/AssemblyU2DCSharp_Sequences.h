﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Sequences
struct  Sequences_t28  : public MonoBehaviour_t7
{
	// UnityEngine.Transform Sequences::target
	Transform_t11 * ___target_2;
};
