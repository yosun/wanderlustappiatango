﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t31;

// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern "C" void BackgroundPlaneBehaviour__ctor_m129 (BackgroundPlaneBehaviour_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
