﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.Word>
struct Predicate_1_t3492;
// System.Object
struct Object_t;
// Vuforia.Word
struct Word_t696;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.Word>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m20533(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3492 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Word>::Invoke(T)
#define Predicate_1_Invoke_m20534(__this, ___obj, method) (( bool (*) (Predicate_1_t3492 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Word>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m20535(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3492 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Word>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m20536(__this, ___result, method) (( bool (*) (Predicate_1_t3492 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
