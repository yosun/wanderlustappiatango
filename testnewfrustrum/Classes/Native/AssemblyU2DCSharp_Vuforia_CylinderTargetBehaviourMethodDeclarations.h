﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t35;

// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m131 (CylinderTargetBehaviour_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
