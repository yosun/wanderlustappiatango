﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Surface>
struct IList_1_t3536;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>
struct  ReadOnlyCollection_1_t3537  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>::list
	Object_t* ___list_0;
};
