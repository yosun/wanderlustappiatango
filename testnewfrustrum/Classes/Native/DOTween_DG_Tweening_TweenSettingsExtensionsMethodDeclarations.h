﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenSettingsExtensions
struct TweenSettingsExtensions_t100;
// DG.Tweening.Sequence
struct Sequence_t122;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Tweener
struct Tweener_t99;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t116;
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"

// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t122 * TweenSettingsExtensions_Append_m367 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t122 * TweenSettingsExtensions_Join_m368 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern "C" Sequence_t122 * TweenSettingsExtensions_Insert_m372 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, float ___atPosition, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C" Tweener_t99 * TweenSettingsExtensions_SetOptions_m5357 (Object_t * __this /* static, unused */, TweenerCore_3_t116 * ___t, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern "C" Tweener_t99 * TweenSettingsExtensions_SetOptions_m5358 (Object_t * __this /* static, unused */, TweenerCore_3_t116 * ___t, int32_t ___axisConstraint, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
