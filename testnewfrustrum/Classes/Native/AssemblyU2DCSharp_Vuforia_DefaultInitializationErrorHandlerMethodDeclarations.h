﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t39;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern "C" void DefaultInitializationErrorHandler__ctor_m134 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern "C" void DefaultInitializationErrorHandler_Awake_m135 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern "C" void DefaultInitializationErrorHandler_OnGUI_m136 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m137 (DefaultInitializationErrorHandler_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m138 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m139 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m140 (DefaultInitializationErrorHandler_t39 * __this, bool ___errorOccurred, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnQCARInitializationError_m141 (DefaultInitializationErrorHandler_t39 * __this, int32_t ___initError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
