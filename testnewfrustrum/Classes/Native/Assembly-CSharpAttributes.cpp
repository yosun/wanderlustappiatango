﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void GyroCameraYo_t14_CustomAttributesCacheGenerator_parentTransform(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void SetRenderQueue_t20_CustomAttributesCacheGenerator_m_queues(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t161_il2cpp_TypeInfo_var;
void SetRenderQueueChildren_t21_CustomAttributesCacheGenerator_m_queues(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t161 * tmp;
		tmp = (SerializeField_t161 *)il2cpp_codegen_object_new (SerializeField_t161_il2cpp_TypeInfo_var);
		SerializeField__ctor_m510(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Basics_t27_CustomAttributesCacheGenerator_Basics_U3CStartU3Em__0_m108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void Basics_t27_CustomAttributesCacheGenerator_Basics_U3CStartU3Em__1_m109(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetterMethodDeclarations.h"
extern TypeInfo* FactorySetter_t142_il2cpp_TypeInfo_var;
void ComponentFactoryStarterBehaviour_t52_CustomAttributesCacheGenerator_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FactorySetter_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FactorySetter_t142 * tmp;
		tmp = (FactorySetter_t142 *)il2cpp_codegen_object_new (FactorySetter_t142_il2cpp_TypeInfo_var);
		FactorySetter__ctor_m512(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
extern const Il2CppType* QCARBehaviour_t66_0_0_0_var;
extern TypeInfo* RequireComponent_t163_il2cpp_TypeInfo_var;
void KeepAliveBehaviour_t55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARBehaviour_t66_0_0_0_var = il2cpp_codegen_type_from_index(84);
		RequireComponent_t163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t163 * tmp;
		tmp = (RequireComponent_t163 *)il2cpp_codegen_object_new (RequireComponent_t163_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m513(tmp, il2cpp_codegen_type_get_object(QCARBehaviour_t66_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t3_0_0_0_var;
extern TypeInfo* RequireComponent_t163_il2cpp_TypeInfo_var;
void VideoBackgroundBehaviour_t81_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_0_0_0_var = il2cpp_codegen_type_from_index(24);
		RequireComponent_t163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t163 * tmp;
		tmp = (RequireComponent_t163 *)il2cpp_codegen_object_new (RequireComponent_t163_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m513(tmp, il2cpp_codegen_type_get_object(Camera_t3_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[10] = 
{
	NULL,
	g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator,
	GyroCameraYo_t14_CustomAttributesCacheGenerator_parentTransform,
	SetRenderQueue_t20_CustomAttributesCacheGenerator_m_queues,
	SetRenderQueueChildren_t21_CustomAttributesCacheGenerator_m_queues,
	Basics_t27_CustomAttributesCacheGenerator_Basics_U3CStartU3Em__0_m108,
	Basics_t27_CustomAttributesCacheGenerator_Basics_U3CStartU3Em__1_m109,
	ComponentFactoryStarterBehaviour_t52_CustomAttributesCacheGenerator_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m176,
	KeepAliveBehaviour_t55_CustomAttributesCacheGenerator,
	VideoBackgroundBehaviour_t81_CustomAttributesCacheGenerator,
};
