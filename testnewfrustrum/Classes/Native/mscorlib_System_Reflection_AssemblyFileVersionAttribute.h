﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_t487  : public Attribute_t138
{
	// System.String System.Reflection.AssemblyFileVersionAttribute::name
	String_t* ___name_0;
};
