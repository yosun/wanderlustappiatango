﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t34;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t761;

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2676 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C" void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2677 (CloudRecoAbstractBehaviour_t34 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2678 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C" void CloudRecoAbstractBehaviour_Initialize_m2679 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C" void CloudRecoAbstractBehaviour_Deinitialize_m2680 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern "C" void CloudRecoAbstractBehaviour_CheckInitialization_m2681 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StartCloudReco_m2682 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StopCloudReco_m2683 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" void CloudRecoAbstractBehaviour_RegisterEventHandler_m2684 (CloudRecoAbstractBehaviour_t34 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2685 (CloudRecoAbstractBehaviour_t34 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C" void CloudRecoAbstractBehaviour_OnEnable_m2686 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern "C" void CloudRecoAbstractBehaviour_OnDisable_m2687 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern "C" void CloudRecoAbstractBehaviour_Start_m2688 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern "C" void CloudRecoAbstractBehaviour_Update_m2689 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern "C" void CloudRecoAbstractBehaviour_OnDestroy_m2690 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
extern "C" void CloudRecoAbstractBehaviour_OnQCARStarted_m2691 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern "C" void CloudRecoAbstractBehaviour__ctor_m392 (CloudRecoAbstractBehaviour_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
