﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Char>
struct List_1_t989;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t2735;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t2584;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<System.Char>
struct ICollection_1_t4292;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>
struct ReadOnlyCollection_1_t3698;
// System.Char[]
struct CharU5BU5D_t110;
// System.Predicate`1<System.Char>
struct Predicate_1_t3699;
// System.Comparison`1<System.Char>
struct Comparison_1_t3701;
// System.Collections.Generic.List`1/Enumerator<System.Char>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_54.h"

// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
// System.Collections.Generic.List`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"
#define List_1__ctor_m5574(__this, method) (( void (*) (List_1_t989 *, const MethodInfo*))List_1__ctor_m23722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23723(__this, ___collection, method) (( void (*) (List_1_t989 *, Object_t*, const MethodInfo*))List_1__ctor_m23724_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor(System.Int32)
#define List_1__ctor_m23725(__this, ___capacity, method) (( void (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1__ctor_m23726_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.cctor()
#define List_1__cctor_m23727(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23728_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Char>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23729(__this, method) (( Object_t* (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23731(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t989 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23732_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23733(__this, method) (( Object_t * (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23734_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23735(__this, ___item, method) (( int32_t (*) (List_1_t989 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23736_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23737(__this, ___item, method) (( bool (*) (List_1_t989 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23738_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23739(__this, ___item, method) (( int32_t (*) (List_1_t989 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23740_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23741(__this, ___index, ___item, method) (( void (*) (List_1_t989 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23742_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23743(__this, ___item, method) (( void (*) (List_1_t989 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23744_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23745(__this, method) (( bool (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23747(__this, method) (( bool (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23748_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23749(__this, method) (( Object_t * (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23750_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23751(__this, method) (( bool (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23753(__this, method) (( bool (*) (List_1_t989 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23754_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23755(__this, ___index, method) (( Object_t * (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23756_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23757(__this, ___index, ___value, method) (( void (*) (List_1_t989 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23758_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Add(T)
#define List_1_Add_m23759(__this, ___item, method) (( void (*) (List_1_t989 *, uint16_t, const MethodInfo*))List_1_Add_m23760_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23761(__this, ___newCount, method) (( void (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23762_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23763(__this, ___collection, method) (( void (*) (List_1_t989 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23764_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23765(__this, ___enumerable, method) (( void (*) (List_1_t989 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23766_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23767(__this, ___collection, method) (( void (*) (List_1_t989 *, Object_t*, const MethodInfo*))List_1_AddRange_m23768_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Char>::AsReadOnly()
#define List_1_AsReadOnly_m23769(__this, method) (( ReadOnlyCollection_1_t3698 * (*) (List_1_t989 *, const MethodInfo*))List_1_AsReadOnly_m23770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
#define List_1_Clear_m23771(__this, method) (( void (*) (List_1_t989 *, const MethodInfo*))List_1_Clear_m23772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::Contains(T)
#define List_1_Contains_m23773(__this, ___item, method) (( bool (*) (List_1_t989 *, uint16_t, const MethodInfo*))List_1_Contains_m23774_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23775(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t989 *, CharU5BU5D_t110*, int32_t, const MethodInfo*))List_1_CopyTo_m23776_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Char>::Find(System.Predicate`1<T>)
#define List_1_Find_m23777(__this, ___match, method) (( uint16_t (*) (List_1_t989 *, Predicate_1_t3699 *, const MethodInfo*))List_1_Find_m23778_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23779(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3699 *, const MethodInfo*))List_1_CheckMatch_m23780_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23781(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t989 *, int32_t, int32_t, Predicate_1_t3699 *, const MethodInfo*))List_1_GetIndex_m23782_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Char>::GetEnumerator()
#define List_1_GetEnumerator_m23783(__this, method) (( Enumerator_t3700  (*) (List_1_t989 *, const MethodInfo*))List_1_GetEnumerator_m23784_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::IndexOf(T)
#define List_1_IndexOf_m23785(__this, ___item, method) (( int32_t (*) (List_1_t989 *, uint16_t, const MethodInfo*))List_1_IndexOf_m23786_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23787(__this, ___start, ___delta, method) (( void (*) (List_1_t989 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23788_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23789(__this, ___index, method) (( void (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23790_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Insert(System.Int32,T)
#define List_1_Insert_m23791(__this, ___index, ___item, method) (( void (*) (List_1_t989 *, int32_t, uint16_t, const MethodInfo*))List_1_Insert_m23792_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23793(__this, ___collection, method) (( void (*) (List_1_t989 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23794_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::Remove(T)
#define List_1_Remove_m23795(__this, ___item, method) (( bool (*) (List_1_t989 *, uint16_t, const MethodInfo*))List_1_Remove_m23796_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23797(__this, ___match, method) (( int32_t (*) (List_1_t989 *, Predicate_1_t3699 *, const MethodInfo*))List_1_RemoveAll_m23798_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Char>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23799(__this, ___index, method) (( void (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23800_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Reverse()
#define List_1_Reverse_m23801(__this, method) (( void (*) (List_1_t989 *, const MethodInfo*))List_1_Reverse_m23802_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Sort()
#define List_1_Sort_m23803(__this, method) (( void (*) (List_1_t989 *, const MethodInfo*))List_1_Sort_m23804_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23805(__this, ___comparison, method) (( void (*) (List_1_t989 *, Comparison_1_t3701 *, const MethodInfo*))List_1_Sort_m23806_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Char>::ToArray()
#define List_1_ToArray_m23807(__this, method) (( CharU5BU5D_t110* (*) (List_1_t989 *, const MethodInfo*))List_1_ToArray_m23808_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::TrimExcess()
#define List_1_TrimExcess_m23809(__this, method) (( void (*) (List_1_t989 *, const MethodInfo*))List_1_TrimExcess_m23810_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Capacity()
#define List_1_get_Capacity_m23811(__this, method) (( int32_t (*) (List_1_t989 *, const MethodInfo*))List_1_get_Capacity_m23812_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23813(__this, ___value, method) (( void (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23814_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count()
#define List_1_get_Count_m23815(__this, method) (( int32_t (*) (List_1_t989 *, const MethodInfo*))List_1_get_Count_m23816_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32)
#define List_1_get_Item_m23817(__this, ___index, method) (( uint16_t (*) (List_1_t989 *, int32_t, const MethodInfo*))List_1_get_Item_m23818_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::set_Item(System.Int32,T)
#define List_1_set_Item_m23819(__this, ___index, ___value, method) (( void (*) (List_1_t989 *, int32_t, uint16_t, const MethodInfo*))List_1_set_Item_m23820_gshared)(__this, ___index, ___value, method)
