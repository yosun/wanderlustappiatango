﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct List_1_t704;
// System.Object
struct Object_t;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t776;
// System.Collections.Generic.IEnumerable`1<Vuforia.ILoadLevelEventHandler>
struct IEnumerable_1_t4201;
// System.Collections.Generic.IEnumerator`1<Vuforia.ILoadLevelEventHandler>
struct IEnumerator_1_t4202;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.ILoadLevelEventHandler>
struct ICollection_1_t4203;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ILoadLevelEventHandler>
struct ReadOnlyCollection_1_t3506;
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t3504;
// System.Predicate`1<Vuforia.ILoadLevelEventHandler>
struct Predicate_1_t3507;
// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct Comparison_1_t3508;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4506(__this, method) (( void (*) (List_1_t704 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20828(__this, ___collection, method) (( void (*) (List_1_t704 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m20829(__this, ___capacity, method) (( void (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.cctor()
#define List_1__cctor_m20830(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20831(__this, method) (( Object_t* (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20832(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t704 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20833(__this, method) (( Object_t * (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20834(__this, ___item, method) (( int32_t (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20835(__this, ___item, method) (( bool (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20836(__this, ___item, method) (( int32_t (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20837(__this, ___index, ___item, method) (( void (*) (List_1_t704 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20838(__this, ___item, method) (( void (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20839(__this, method) (( bool (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20840(__this, method) (( bool (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20841(__this, method) (( Object_t * (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20842(__this, method) (( bool (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20843(__this, method) (( bool (*) (List_1_t704 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20844(__this, ___index, method) (( Object_t * (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20845(__this, ___index, ___value, method) (( void (*) (List_1_t704 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Add(T)
#define List_1_Add_m20846(__this, ___item, method) (( void (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20847(__this, ___newCount, method) (( void (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20848(__this, ___collection, method) (( void (*) (List_1_t704 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20849(__this, ___enumerable, method) (( void (*) (List_1_t704 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20850(__this, ___collection, method) (( void (*) (List_1_t704 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m20851(__this, method) (( ReadOnlyCollection_1_t3506 * (*) (List_1_t704 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Clear()
#define List_1_Clear_m20852(__this, method) (( void (*) (List_1_t704 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Contains(T)
#define List_1_Contains_m20853(__this, ___item, method) (( bool (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20854(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t704 *, ILoadLevelEventHandlerU5BU5D_t3504*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m20855(__this, ___match, method) (( Object_t * (*) (List_1_t704 *, Predicate_1_t3507 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20856(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3507 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20857(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t704 *, int32_t, int32_t, Predicate_1_t3507 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4503(__this, method) (( Enumerator_t843  (*) (List_1_t704 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::IndexOf(T)
#define List_1_IndexOf_m20858(__this, ___item, method) (( int32_t (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20859(__this, ___start, ___delta, method) (( void (*) (List_1_t704 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20860(__this, ___index, method) (( void (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m20861(__this, ___index, ___item, method) (( void (*) (List_1_t704 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20862(__this, ___collection, method) (( void (*) (List_1_t704 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Remove(T)
#define List_1_Remove_m20863(__this, ___item, method) (( bool (*) (List_1_t704 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20864(__this, ___match, method) (( int32_t (*) (List_1_t704 *, Predicate_1_t3507 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20865(__this, ___index, method) (( void (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Reverse()
#define List_1_Reverse_m20866(__this, method) (( void (*) (List_1_t704 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort()
#define List_1_Sort_m20867(__this, method) (( void (*) (List_1_t704 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20868(__this, ___comparison, method) (( void (*) (List_1_t704 *, Comparison_1_t3508 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::ToArray()
#define List_1_ToArray_m20869(__this, method) (( ILoadLevelEventHandlerU5BU5D_t3504* (*) (List_1_t704 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::TrimExcess()
#define List_1_TrimExcess_m20870(__this, method) (( void (*) (List_1_t704 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Capacity()
#define List_1_get_Capacity_m20871(__this, method) (( int32_t (*) (List_1_t704 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20872(__this, ___value, method) (( void (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Count()
#define List_1_get_Count_m20873(__this, method) (( int32_t (*) (List_1_t704 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m20874(__this, ___index, method) (( Object_t * (*) (List_1_t704 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m20875(__this, ___index, ___value, method) (( void (*) (List_1_t704 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
