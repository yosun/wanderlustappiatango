﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t3225;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t528;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Predicate`1<System.Object>
struct Predicate_1_t3121;
// System.Comparison`1<System.Object>
struct Comparison_1_t3125;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m16490_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m16490(__this, method) (( void (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1__ctor_m16490_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492(__this, method) (( Object_t * (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m16494_gshared (IndexedSet_1_t3225 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m16494(__this, ___item, method) (( void (*) (IndexedSet_1_t3225 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m16494_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m16496_gshared (IndexedSet_1_t3225 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m16496(__this, ___item, method) (( bool (*) (IndexedSet_1_t3225 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m16496_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m16498_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m16498(__this, method) (( Object_t* (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m16498_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m16500_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m16500(__this, method) (( void (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1_Clear_m16500_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m16502_gshared (IndexedSet_1_t3225 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m16502(__this, ___item, method) (( bool (*) (IndexedSet_1_t3225 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m16502_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m16504_gshared (IndexedSet_1_t3225 * __this, ObjectU5BU5D_t115* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m16504(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t3225 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m16504_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m16506_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m16506(__this, method) (( int32_t (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1_get_Count_m16506_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m16508_gshared (IndexedSet_1_t3225 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m16508(__this, method) (( bool (*) (IndexedSet_1_t3225 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m16508_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m16510_gshared (IndexedSet_1_t3225 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m16510(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t3225 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m16510_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m16512_gshared (IndexedSet_1_t3225 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m16512(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t3225 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m16512_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m16514_gshared (IndexedSet_1_t3225 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m16514(__this, ___index, method) (( void (*) (IndexedSet_1_t3225 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m16514_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m16516_gshared (IndexedSet_1_t3225 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m16516(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t3225 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m16516_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m16518_gshared (IndexedSet_1_t3225 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m16518(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t3225 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m16518_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m16519_gshared (IndexedSet_1_t3225 * __this, Predicate_1_t3121 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m16519(__this, ___match, method) (( void (*) (IndexedSet_1_t3225 *, Predicate_1_t3121 *, const MethodInfo*))IndexedSet_1_RemoveAll_m16519_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m16520_gshared (IndexedSet_1_t3225 * __this, Comparison_1_t3125 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m16520(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t3225 *, Comparison_1_t3125 *, const MethodInfo*))IndexedSet_1_Sort_m16520_gshared)(__this, ___sortLayoutFunction, method)
