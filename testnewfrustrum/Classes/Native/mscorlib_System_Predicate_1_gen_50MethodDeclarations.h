﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<DG.Tweening.TweenCallback>
struct Predicate_1_t3666;
// System.Object
struct Object_t;
// DG.Tweening.TweenCallback
struct TweenCallback_t101;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<DG.Tweening.TweenCallback>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m23400(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3666 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<DG.Tweening.TweenCallback>::Invoke(T)
#define Predicate_1_Invoke_m23401(__this, ___obj, method) (( bool (*) (Predicate_1_t3666 *, TweenCallback_t101 *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<DG.Tweening.TweenCallback>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23402(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3666 *, TweenCallback_t101 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<DG.Tweening.TweenCallback>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23403(__this, ___result, method) (( bool (*) (Predicate_1_t3666 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
