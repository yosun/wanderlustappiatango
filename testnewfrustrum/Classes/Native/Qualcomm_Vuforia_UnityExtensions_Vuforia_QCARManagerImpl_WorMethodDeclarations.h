﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/WordResultData
struct WordResultData_t646;
struct WordResultData_t646_marshaled;

void WordResultData_t646_marshal(const WordResultData_t646& unmarshaled, WordResultData_t646_marshaled& marshaled);
void WordResultData_t646_marshal_back(const WordResultData_t646_marshaled& marshaled, WordResultData_t646& unmarshaled);
void WordResultData_t646_marshal_cleanup(WordResultData_t646_marshaled& marshaled);
