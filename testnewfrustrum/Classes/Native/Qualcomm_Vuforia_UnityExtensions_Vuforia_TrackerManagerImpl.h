﻿#pragma once
#include <stdint.h>
// Vuforia.ObjectTracker
struct ObjectTracker_t574;
// Vuforia.MarkerTracker
struct MarkerTracker_t629;
// Vuforia.TextTracker
struct TextTracker_t676;
// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t674;
// Vuforia.StateManager
struct StateManager_t713;
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.TrackerManagerImpl
struct  TrackerManagerImpl_t729  : public TrackerManager_t728
{
	// Vuforia.ObjectTracker Vuforia.TrackerManagerImpl::mObjectTracker
	ObjectTracker_t574 * ___mObjectTracker_1;
	// Vuforia.MarkerTracker Vuforia.TrackerManagerImpl::mMarkerTracker
	MarkerTracker_t629 * ___mMarkerTracker_2;
	// Vuforia.TextTracker Vuforia.TrackerManagerImpl::mTextTracker
	TextTracker_t676 * ___mTextTracker_3;
	// Vuforia.SmartTerrainTracker Vuforia.TrackerManagerImpl::mSmartTerrainTracker
	SmartTerrainTracker_t674 * ___mSmartTerrainTracker_4;
	// Vuforia.StateManager Vuforia.TrackerManagerImpl::mStateManager
	StateManager_t713 * ___mStateManager_5;
};
