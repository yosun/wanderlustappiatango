﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t1032;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"

// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23638_gshared (DOGetter_1_t1032 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23638(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1032 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23638_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C" Color2_t1000  DOGetter_1_Invoke_m23639_gshared (DOGetter_1_t1032 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23639(__this, method) (( Color2_t1000  (*) (DOGetter_1_t1032 *, const MethodInfo*))DOGetter_1_Invoke_m23639_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23640_gshared (DOGetter_1_t1032 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23640(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1032 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23640_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" Color2_t1000  DOGetter_1_EndInvoke_m23641_gshared (DOGetter_1_t1032 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23641(__this, ___result, method) (( Color2_t1000  (*) (DOGetter_1_t1032 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23641_gshared)(__this, ___result, method)
