﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>
struct InternalEnumerator_1_t3599;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22422_gshared (InternalEnumerator_1_t3599 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22422(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3599 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22422_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22423_gshared (InternalEnumerator_1_t3599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22423(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3599 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22423_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22424_gshared (InternalEnumerator_1_t3599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22424(__this, method) (( void (*) (InternalEnumerator_1_t3599 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22424_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22425_gshared (InternalEnumerator_1_t3599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22425(__this, method) (( bool (*) (InternalEnumerator_1_t3599 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22425_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
extern "C" WebCamDevice_t879  InternalEnumerator_1_get_Current_m22426_gshared (InternalEnumerator_1_t3599 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22426(__this, method) (( WebCamDevice_t879  (*) (InternalEnumerator_1_t3599 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22426_gshared)(__this, method)
