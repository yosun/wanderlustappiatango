﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
struct Enumerator_t3781;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3776;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25276_gshared (Enumerator_t3781 * __this, Dictionary_2_t3776 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25276(__this, ___host, method) (( void (*) (Enumerator_t3781 *, Dictionary_2_t3776 *, const MethodInfo*))Enumerator__ctor_m25276_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25277_gshared (Enumerator_t3781 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25277(__this, method) (( Object_t * (*) (Enumerator_t3781 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25277_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m25278_gshared (Enumerator_t3781 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25278(__this, method) (( void (*) (Enumerator_t3781 *, const MethodInfo*))Enumerator_Dispose_m25278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25279_gshared (Enumerator_t3781 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25279(__this, method) (( bool (*) (Enumerator_t3781 *, const MethodInfo*))Enumerator_MoveNext_m25279_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m25280_gshared (Enumerator_t3781 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25280(__this, method) (( Object_t * (*) (Enumerator_t3781 *, const MethodInfo*))Enumerator_get_Current_m25280_gshared)(__this, method)
