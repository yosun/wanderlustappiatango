﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSource
struct TrackableSource_t619;

// System.Void Vuforia.TrackableSource::.ctor()
extern "C" void TrackableSource__ctor_m4055 (TrackableSource_t619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
