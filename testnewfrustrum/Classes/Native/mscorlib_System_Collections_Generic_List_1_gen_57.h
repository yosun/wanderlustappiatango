﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct MatchDirectConnectInfoU5BU5D_t3796;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  List_1_t1261  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::_items
	MatchDirectConnectInfoU5BU5D_t3796* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::_version
	int32_t ____version_3;
};
struct List_1_t1261_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::EmptyArray
	MatchDirectConnectInfoU5BU5D_t3796* ___EmptyArray_4;
};
