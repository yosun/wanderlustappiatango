﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>
struct InternalEnumerator_1_t3881;
// System.Object
struct Object_t;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t501;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m26566(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3881 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26567(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3881 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m26568(__this, method) (( void (*) (InternalEnumerator_1_t3881 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m26569(__this, method) (( bool (*) (InternalEnumerator_1_t3881 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m26570(__this, method) (( DisallowMultipleComponent_t501 * (*) (InternalEnumerator_1_t3881 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
