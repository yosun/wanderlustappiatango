﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
struct Enumerator_t893;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t790;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t754;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23133(__this, ___l, method) (( void (*) (Enumerator_t893 *, List_1_t754 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23134(__this, method) (( Object_t * (*) (Enumerator_t893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::Dispose()
#define Enumerator_Dispose_m23135(__this, method) (( void (*) (Enumerator_t893 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23136(__this, method) (( void (*) (Enumerator_t893 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4654(__this, method) (( bool (*) (Enumerator_t893 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::get_Current()
#define Enumerator_get_Current_m4653(__this, method) (( Object_t * (*) (Enumerator_t893 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
