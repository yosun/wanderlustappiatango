﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManager
struct QCARManager_t155;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_t175;
// UnityEngine.Transform
struct Transform_t11;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"

// Vuforia.QCARManager Vuforia.QCARManager::get_Instance()
extern "C" QCARManager_t155 * QCARManager_get_Instance_m484 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManager::get_WorldCenterMode()
// System.Void Vuforia.QCARManager::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManager::get_WorldCenter()
// System.Void Vuforia.QCARManager::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
// UnityEngine.Transform Vuforia.QCARManager::get_ARCameraTransform()
// System.Void Vuforia.QCARManager::set_ARCameraTransform(UnityEngine.Transform)
// System.Boolean Vuforia.QCARManager::get_Initialized()
// System.Int32 Vuforia.QCARManager::get_QCARFrameIndex()
// System.Boolean Vuforia.QCARManager::Init()
// System.Void Vuforia.QCARManager::Deinit()
// System.Void Vuforia.QCARManager::.ctor()
extern "C" void QCARManager__ctor_m3001 (QCARManager_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManager::.cctor()
extern "C" void QCARManager__cctor_m3002 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
