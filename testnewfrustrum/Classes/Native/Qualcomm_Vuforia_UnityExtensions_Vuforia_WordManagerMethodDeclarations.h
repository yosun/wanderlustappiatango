﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordManager
struct WordManager_t687;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t771;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t772;
// Vuforia.Word
struct Word_t696;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t93;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t773;

// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManager::GetActiveWordResults()
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManager::GetNewWords()
// System.Collections.Generic.IEnumerable`1<Vuforia.Word> Vuforia.WordManager::GetLostWords()
// System.Boolean Vuforia.WordManager::TryGetWordBehaviour(Vuforia.Word,Vuforia.WordAbstractBehaviour&)
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManager::GetTrackableBehaviours()
// System.Void Vuforia.WordManager::DestroyWordBehaviour(Vuforia.WordAbstractBehaviour,System.Boolean)
// System.Void Vuforia.WordManager::.ctor()
extern "C" void WordManager__ctor_m3120 (WordManager_t687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
