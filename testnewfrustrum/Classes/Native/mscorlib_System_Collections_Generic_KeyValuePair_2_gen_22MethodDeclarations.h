﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct KeyValuePair_2_t3523;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t73;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21250(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3523 *, int32_t, SurfaceAbstractBehaviour_t73 *, const MethodInfo*))KeyValuePair_2__ctor_m16229_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m21251(__this, method) (( int32_t (*) (KeyValuePair_2_t3523 *, const MethodInfo*))KeyValuePair_2_get_Key_m16230_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21252(__this, ___value, method) (( void (*) (KeyValuePair_2_t3523 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16231_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m21253(__this, method) (( SurfaceAbstractBehaviour_t73 * (*) (KeyValuePair_2_t3523 *, const MethodInfo*))KeyValuePair_2_get_Value_m16232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21254(__this, ___value, method) (( void (*) (KeyValuePair_2_t3523 *, SurfaceAbstractBehaviour_t73 *, const MethodInfo*))KeyValuePair_2_set_Value_m16233_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m21255(__this, method) (( String_t* (*) (KeyValuePair_2_t3523 *, const MethodInfo*))KeyValuePair_2_ToString_m16234_gshared)(__this, method)
