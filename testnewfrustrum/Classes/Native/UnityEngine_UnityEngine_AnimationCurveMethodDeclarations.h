﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t1237;
struct AnimationCurve_t1237_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1363;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m6404 (AnimationCurve_t1237 * __this, KeyframeU5BU5D_t1363* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m6405 (AnimationCurve_t1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m6406 (AnimationCurve_t1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m6407 (AnimationCurve_t1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m6408 (AnimationCurve_t1237 * __this, KeyframeU5BU5D_t1363* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t1237_marshal(const AnimationCurve_t1237& unmarshaled, AnimationCurve_t1237_marshaled& marshaled);
void AnimationCurve_t1237_marshal_back(const AnimationCurve_t1237_marshaled& marshaled, AnimationCurve_t1237& unmarshaled);
void AnimationCurve_t1237_marshal_cleanup(AnimationCurve_t1237_marshaled& marshaled);
