﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3554;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21876_gshared (Enumerator_t3554 * __this, Dictionary_2_t869 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m21876(__this, ___dictionary, method) (( void (*) (Enumerator_t3554 *, Dictionary_2_t869 *, const MethodInfo*))Enumerator__ctor_m21876_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21877_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21877(__this, method) (( Object_t * (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21877_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21878_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21878(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21878_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21879_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21879(__this, method) (( Object_t * (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21880_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21880(__this, method) (( Object_t * (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21881_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21881(__this, method) (( bool (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_MoveNext_m21881_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" KeyValuePair_2_t3550  Enumerator_get_Current_m21882_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21882(__this, method) (( KeyValuePair_2_t3550  (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_get_Current_m21882_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m21883_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m21883(__this, method) (( int32_t (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_get_CurrentKey_m21883_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C" TrackableResultData_t642  Enumerator_get_CurrentValue_m21884_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m21884(__this, method) (( TrackableResultData_t642  (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_get_CurrentValue_m21884_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::VerifyState()
extern "C" void Enumerator_VerifyState_m21885_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m21885(__this, method) (( void (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_VerifyState_m21885_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m21886_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m21886(__this, method) (( void (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_VerifyCurrent_m21886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m21887_gshared (Enumerator_t3554 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21887(__this, method) (( void (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_Dispose_m21887_gshared)(__this, method)
