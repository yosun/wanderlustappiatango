﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1035;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23665_gshared (DOGetter_1_t1035 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23665(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1035 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23665_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C" Rect_t124  DOGetter_1_Invoke_m23666_gshared (DOGetter_1_t1035 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23666(__this, method) (( Rect_t124  (*) (DOGetter_1_t1035 *, const MethodInfo*))DOGetter_1_Invoke_m23666_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23667_gshared (DOGetter_1_t1035 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23667(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1035 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23667_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C" Rect_t124  DOGetter_1_EndInvoke_m23668_gshared (DOGetter_1_t1035 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23668(__this, ___result, method) (( Rect_t124  (*) (DOGetter_1_t1035 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23668_gshared)(__this, ___result, method)
