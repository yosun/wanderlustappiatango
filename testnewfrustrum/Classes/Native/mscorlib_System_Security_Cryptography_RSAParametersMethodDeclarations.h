﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1774;
struct RSAParameters_t1774_marshaled;

void RSAParameters_t1774_marshal(const RSAParameters_t1774& unmarshaled, RSAParameters_t1774_marshaled& marshaled);
void RSAParameters_t1774_marshal_back(const RSAParameters_t1774_marshaled& marshaled, RSAParameters_t1774& unmarshaled);
void RSAParameters_t1774_marshal_cleanup(RSAParameters_t1774_marshaled& marshaled);
