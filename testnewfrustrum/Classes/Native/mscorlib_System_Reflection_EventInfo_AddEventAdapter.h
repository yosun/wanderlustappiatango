﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t143;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.EventInfo/AddEventAdapter
struct  AddEventAdapter_t2244  : public MulticastDelegate_t307
{
};
