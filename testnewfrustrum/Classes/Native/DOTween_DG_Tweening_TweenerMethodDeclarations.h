﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Tweener
struct Tweener_t99;

// System.Void DG.Tweening.Tweener::.ctor()
extern "C" void Tweener__ctor_m5477 (Tweener_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
