﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t3870;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3861;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26482_gshared (ShimEnumerator_t3870 * __this, Dictionary_2_t3861 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26482(__this, ___host, method) (( void (*) (ShimEnumerator_t3870 *, Dictionary_2_t3861 *, const MethodInfo*))ShimEnumerator__ctor_m26482_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26483_gshared (ShimEnumerator_t3870 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26483(__this, method) (( bool (*) (ShimEnumerator_t3870 *, const MethodInfo*))ShimEnumerator_MoveNext_m26483_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m26484_gshared (ShimEnumerator_t3870 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26484(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3870 *, const MethodInfo*))ShimEnumerator_get_Entry_m26484_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26485_gshared (ShimEnumerator_t3870 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26485(__this, method) (( Object_t * (*) (ShimEnumerator_t3870 *, const MethodInfo*))ShimEnumerator_get_Key_m26485_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26486_gshared (ShimEnumerator_t3870 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26486(__this, method) (( Object_t * (*) (ShimEnumerator_t3870 *, const MethodInfo*))ShimEnumerator_get_Value_m26486_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26487_gshared (ShimEnumerator_t3870 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26487(__this, method) (( Object_t * (*) (ShimEnumerator_t3870 *, const MethodInfo*))ShimEnumerator_get_Current_m26487_gshared)(__this, method)
