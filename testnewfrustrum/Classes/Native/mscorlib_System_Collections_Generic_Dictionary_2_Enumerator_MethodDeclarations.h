﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Enumerator_t428;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t236;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t242;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m16362(__this, ___dictionary, method) (( void (*) (Enumerator_t428 *, Dictionary_2_t242 *, const MethodInfo*))Enumerator__ctor_m16259_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16363(__this, method) (( Object_t * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16364(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16365(__this, method) (( Object_t * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16366(__this, method) (( Object_t * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m2011(__this, method) (( bool (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_MoveNext_m16264_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m2008(__this, method) (( KeyValuePair_2_t427  (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_get_Current_m16265_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16367(__this, method) (( int32_t (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_get_CurrentKey_m16266_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16368(__this, method) (( PointerEventData_t236 * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_get_CurrentValue_m16267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m16369(__this, method) (( void (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_VerifyState_m16268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16370(__this, method) (( void (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_VerifyCurrent_m16269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m16371(__this, method) (( void (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_Dispose_m16270_gshared)(__this, method)
