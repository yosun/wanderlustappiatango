﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRendererImpl
struct QCARRendererImpl_t666;
// UnityEngine.Texture2D
struct Texture2D_t270;
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"

// UnityEngine.Texture2D Vuforia.QCARRendererImpl::get_VideoBackgroundTexture()
extern "C" Texture2D_t270 * QCARRendererImpl_get_VideoBackgroundTexture_m3036 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRendererImpl::GetVideoBackgroundConfig()
extern "C" VideoBGCfgData_t662  QCARRendererImpl_GetVideoBackgroundConfig_m3037 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::ClearVideoBackgroundConfig()
extern "C" void QCARRendererImpl_ClearVideoBackgroundConfig_m3038 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
extern "C" void QCARRendererImpl_SetVideoBackgroundConfig_m3039 (QCARRendererImpl_t666 * __this, VideoBGCfgData_t662  ___config, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
extern "C" bool QCARRendererImpl_SetVideoBackgroundTexture_m3040 (QCARRendererImpl_t666 * __this, Texture2D_t270 * ___texture, int32_t ___nativeTextureID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::IsVideoBackgroundInfoAvailable()
extern "C" bool QCARRendererImpl_IsVideoBackgroundInfoAvailable_m3041 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRendererImpl::GetVideoTextureInfo()
extern "C" VideoTextureInfo_t561  QCARRendererImpl_GetVideoTextureInfo_m3042 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::Pause(System.Boolean)
extern "C" void QCARRendererImpl_Pause_m3043 (QCARRendererImpl_t666 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::UnityRenderEvent(Vuforia.QCARRendererImpl/RenderEvent)
extern "C" void QCARRendererImpl_UnityRenderEvent_m3044 (QCARRendererImpl_t666 * __this, int32_t ___renderEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::.ctor()
extern "C" void QCARRendererImpl__ctor_m3045 (QCARRendererImpl_t666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
