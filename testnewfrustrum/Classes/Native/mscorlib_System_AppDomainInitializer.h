﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t109;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AppDomainInitializer
struct  AppDomainInitializer_t2492  : public MulticastDelegate_t307
{
};
