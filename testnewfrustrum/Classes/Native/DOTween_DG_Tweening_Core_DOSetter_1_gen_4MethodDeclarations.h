﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1033;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"

// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23642_gshared (DOSetter_1_t1033 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23642(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1033 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23642_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23643_gshared (DOSetter_1_t1033 * __this, Color2_t1000  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23643(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1033 *, Color2_t1000 , const MethodInfo*))DOSetter_1_Invoke_m23643_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23644_gshared (DOSetter_1_t1033 * __this, Color2_t1000  ___pNewValue, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23644(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1033 *, Color2_t1000 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23644_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23645_gshared (DOSetter_1_t1033 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23645(__this, ___result, method) (( void (*) (DOSetter_1_t1033 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23645_gshared)(__this, ___result, method)
