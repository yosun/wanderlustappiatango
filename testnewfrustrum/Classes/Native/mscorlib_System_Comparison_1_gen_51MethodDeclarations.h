﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<DG.Tweening.Tween>
struct Comparison_1_t3673;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t934;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<DG.Tweening.Tween>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m23504(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3673 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15149_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<DG.Tweening.Tween>::Invoke(T,T)
#define Comparison_1_Invoke_m23505(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3673 *, Tween_t934 *, Tween_t934 *, const MethodInfo*))Comparison_1_Invoke_m15150_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<DG.Tweening.Tween>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m23506(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3673 *, Tween_t934 *, Tween_t934 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15151_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<DG.Tweening.Tween>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m23507(__this, ___result, method) (( int32_t (*) (Comparison_1_t3673 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15152_gshared)(__this, ___result, method)
