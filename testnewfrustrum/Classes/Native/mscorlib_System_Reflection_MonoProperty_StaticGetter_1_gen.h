﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct  StaticGetter_1_t3993  : public MulticastDelegate_t307
{
};
