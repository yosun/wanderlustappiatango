﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Color2
struct Color2_t1000;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"

// System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern "C" void Color2__ctor_m5488 (Color2_t1000 * __this, Color_t90  ___ca, Color_t90  ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C" Color2_t1000  Color2_op_Addition_m5489 (Object_t * __this /* static, unused */, Color2_t1000  ___c1, Color2_t1000  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C" Color2_t1000  Color2_op_Subtraction_m5490 (Object_t * __this /* static, unused */, Color2_t1000  ___c1, Color2_t1000  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern "C" Color2_t1000  Color2_op_Multiply_m5491 (Object_t * __this /* static, unused */, Color2_t1000  ___c1, float ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
