﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1344;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1346;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1347;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m6872 (PersistentCallGroup_t1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m6873 (PersistentCallGroup_t1344 * __this, InvokableCallList_t1346 * ___invokableList, UnityEventBase_t1347 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
