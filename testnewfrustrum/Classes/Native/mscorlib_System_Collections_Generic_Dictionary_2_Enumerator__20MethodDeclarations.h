﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Enumerator_t3525;
// System.Object
struct Object_t;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t73;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t709;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m21283(__this, ___dictionary, method) (( void (*) (Enumerator_t3525 *, Dictionary_2_t709 *, const MethodInfo*))Enumerator__ctor_m16259_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21284(__this, method) (( Object_t * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16260_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21285(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16261_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21286(__this, method) (( Object_t * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16262_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21287(__this, method) (( Object_t * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16263_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m21288(__this, method) (( bool (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_MoveNext_m16264_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m21289(__this, method) (( KeyValuePair_2_t3523  (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_get_Current_m16265_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21290(__this, method) (( int32_t (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_get_CurrentKey_m16266_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21291(__this, method) (( SurfaceAbstractBehaviour_t73 * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_get_CurrentValue_m16267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m21292(__this, method) (( void (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_VerifyState_m16268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21293(__this, method) (( void (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_VerifyCurrent_m16269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m21294(__this, method) (( void (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_Dispose_m16270_gshared)(__this, method)
