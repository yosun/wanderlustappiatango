﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t781;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t98;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface>
struct IEnumerable_1_t779;
// System.Collections.Generic.IEnumerator`1<Vuforia.Surface>
struct IEnumerator_1_t858;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.Surface>
struct ICollection_1_t4208;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>
struct ReadOnlyCollection_1_t3537;
// Vuforia.Surface[]
struct SurfaceU5BU5D_t3519;
// System.Predicate`1<Vuforia.Surface>
struct Predicate_1_t3538;
// System.Comparison`1<Vuforia.Surface>
struct Comparison_1_t3539;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4542(__this, method) (( void (*) (List_1_t781 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21581(__this, ___collection, method) (( void (*) (List_1_t781 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor(System.Int32)
#define List_1__ctor_m21582(__this, ___capacity, method) (( void (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.cctor()
#define List_1__cctor_m21583(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21584(__this, method) (( Object_t* (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21585(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t781 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21586(__this, method) (( Object_t * (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21587(__this, ___item, method) (( int32_t (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21588(__this, ___item, method) (( bool (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21589(__this, ___item, method) (( int32_t (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21590(__this, ___index, ___item, method) (( void (*) (List_1_t781 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21591(__this, ___item, method) (( void (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21592(__this, method) (( bool (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21593(__this, method) (( bool (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21594(__this, method) (( Object_t * (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21595(__this, method) (( bool (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21596(__this, method) (( bool (*) (List_1_t781 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21597(__this, ___index, method) (( Object_t * (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21598(__this, ___index, ___value, method) (( void (*) (List_1_t781 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Add(T)
#define List_1_Add_m21599(__this, ___item, method) (( void (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21600(__this, ___newCount, method) (( void (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21601(__this, ___collection, method) (( void (*) (List_1_t781 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21602(__this, ___enumerable, method) (( void (*) (List_1_t781 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21603(__this, ___collection, method) (( void (*) (List_1_t781 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Surface>::AsReadOnly()
#define List_1_AsReadOnly_m21604(__this, method) (( ReadOnlyCollection_1_t3537 * (*) (List_1_t781 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Clear()
#define List_1_Clear_m21605(__this, method) (( void (*) (List_1_t781 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::Contains(T)
#define List_1_Contains_m21606(__this, ___item, method) (( bool (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21607(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t781 *, SurfaceU5BU5D_t3519*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Surface>::Find(System.Predicate`1<T>)
#define List_1_Find_m21608(__this, ___match, method) (( Object_t * (*) (List_1_t781 *, Predicate_1_t3538 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21609(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3538 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21610(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t781 *, int32_t, int32_t, Predicate_1_t3538 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Surface>::GetEnumerator()
#define List_1_GetEnumerator_m4539(__this, method) (( Enumerator_t862  (*) (List_1_t781 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::IndexOf(T)
#define List_1_IndexOf_m21611(__this, ___item, method) (( int32_t (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21612(__this, ___start, ___delta, method) (( void (*) (List_1_t781 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21613(__this, ___index, method) (( void (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Insert(System.Int32,T)
#define List_1_Insert_m21614(__this, ___index, ___item, method) (( void (*) (List_1_t781 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21615(__this, ___collection, method) (( void (*) (List_1_t781 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::Remove(T)
#define List_1_Remove_m21616(__this, ___item, method) (( bool (*) (List_1_t781 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21617(__this, ___match, method) (( int32_t (*) (List_1_t781 *, Predicate_1_t3538 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21618(__this, ___index, method) (( void (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Reverse()
#define List_1_Reverse_m21619(__this, method) (( void (*) (List_1_t781 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Sort()
#define List_1_Sort_m21620(__this, method) (( void (*) (List_1_t781 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21621(__this, ___comparison, method) (( void (*) (List_1_t781 *, Comparison_1_t3539 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Surface>::ToArray()
#define List_1_ToArray_m21622(__this, method) (( SurfaceU5BU5D_t3519* (*) (List_1_t781 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::TrimExcess()
#define List_1_TrimExcess_m21623(__this, method) (( void (*) (List_1_t781 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::get_Capacity()
#define List_1_get_Capacity_m21624(__this, method) (( int32_t (*) (List_1_t781 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21625(__this, ___value, method) (( void (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::get_Count()
#define List_1_get_Count_m21626(__this, method) (( int32_t (*) (List_1_t781 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Surface>::get_Item(System.Int32)
#define List_1_get_Item_m21627(__this, ___index, method) (( Object_t * (*) (List_1_t781 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::set_Item(System.Int32,T)
#define List_1_set_Item_m21628(__this, ___index, ___value, method) (( void (*) (List_1_t781 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
