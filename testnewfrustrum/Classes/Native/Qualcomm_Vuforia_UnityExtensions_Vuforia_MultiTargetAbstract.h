﻿#pragma once
#include <stdint.h>
// Vuforia.MultiTarget
struct MultiTarget_t740;
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
// Vuforia.MultiTargetAbstractBehaviour
struct  MultiTargetAbstractBehaviour_t62  : public DataSetTrackableBehaviour_t567
{
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	Object_t * ___mMultiTarget_20;
};
