﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t86;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.VirtualButtonAbstractBehaviour>
struct  Predicate_1_t3581  : public MulticastDelegate_t307
{
};
