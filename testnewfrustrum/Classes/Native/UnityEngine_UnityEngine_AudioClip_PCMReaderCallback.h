﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t585;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t1228  : public MulticastDelegate_t307
{
};
