﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueueChildren
struct SetRenderQueueChildren_t21;

// System.Void SetRenderQueueChildren::.ctor()
extern "C" void SetRenderQueueChildren__ctor_m87 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Start()
extern "C" void SetRenderQueueChildren_Start_m88 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Awake()
extern "C" void SetRenderQueueChildren_Awake_m89 (SetRenderQueueChildren_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
