﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition System.Text.RegularExpressions.IMachine
extern TypeInfo IMachine_t1927_il2cpp_TypeInfo;
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IMachine_t1927_IMachine_Scan_m9448_ParameterInfos[] = 
{
	{"regex", 0, 134218240, 0, &Regex_t822_0_0_0},
	{"text", 1, 134218241, 0, &String_t_0_0_0},
	{"start", 2, 134218242, 0, &Int32_t127_0_0_0},
	{"end", 3, 134218243, 0, &Int32_t127_0_0_0},
};
extern const Il2CppType Match_t1061_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.IMachine::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern const MethodInfo IMachine_Scan_m9448_MethodInfo = 
{
	"Scan"/* name */
	, NULL/* method */
	, &IMachine_t1927_il2cpp_TypeInfo/* declaring_type */
	, &Match_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, IMachine_t1927_IMachine_Scan_m9448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IMachine_t1927_IMachine_Split_m9449_ParameterInfos[] = 
{
	{"regex", 0, 134218244, 0, &Regex_t822_0_0_0},
	{"input", 1, 134218245, 0, &String_t_0_0_0},
	{"count", 2, 134218246, 0, &Int32_t127_0_0_0},
	{"startat", 3, 134218247, 0, &Int32_t127_0_0_0},
};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.IMachine::Split(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern const MethodInfo IMachine_Split_m9449_MethodInfo = 
{
	"Split"/* name */
	, NULL/* method */
	, &IMachine_t1927_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, IMachine_t1927_IMachine_Split_m9449_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IMachine_t1927_IMachine_Replace_m9450_ParameterInfos[] = 
{
	{"regex", 0, 134218248, 0, &Regex_t822_0_0_0},
	{"input", 1, 134218249, 0, &String_t_0_0_0},
	{"replacement", 2, 134218250, 0, &String_t_0_0_0},
	{"count", 3, 134218251, 0, &Int32_t127_0_0_0},
	{"startat", 4, 134218252, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.IMachine::Replace(System.Text.RegularExpressions.Regex,System.String,System.String,System.Int32,System.Int32)
extern const MethodInfo IMachine_Replace_m9450_MethodInfo = 
{
	"Replace"/* name */
	, NULL/* method */
	, &IMachine_t1927_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, IMachine_t1927_IMachine_Replace_m9450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMachine_t1927_MethodInfos[] =
{
	&IMachine_Scan_m9448_MethodInfo,
	&IMachine_Split_m9449_MethodInfo,
	&IMachine_Replace_m9450_MethodInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachine_t1927_0_0_0;
extern const Il2CppType IMachine_t1927_1_0_0;
struct IMachine_t1927;
const Il2CppTypeDefinitionMetadata IMachine_t1927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMachine_t1927_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachine"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IMachine_t1927_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMachine_t1927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachine_t1927_0_0_0/* byval_arg */
	, &IMachine_t1927_1_0_0/* this_arg */
	, &IMachine_t1927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.IMachineFactory
extern TypeInfo IMachineFactory_t1930_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.IMachineFactory::NewInstance()
extern const MethodInfo IMachineFactory_NewInstance_m9451_MethodInfo = 
{
	"NewInstance"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t1927_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1931_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.IMachineFactory::get_Mapping()
extern const MethodInfo IMachineFactory_get_Mapping_m9452_MethodInfo = 
{
	"get_Mapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1931_0_0_0;
static const ParameterInfo IMachineFactory_t1930_IMachineFactory_set_Mapping_m9453_ParameterInfos[] = 
{
	{"value", 0, 134218253, 0, &IDictionary_t1931_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_Mapping(System.Collections.IDictionary)
extern const MethodInfo IMachineFactory_set_Mapping_m9453_MethodInfo = 
{
	"set_Mapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IMachineFactory_t1930_IMachineFactory_set_Mapping_m9453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IMachineFactory::get_GroupCount()
extern const MethodInfo IMachineFactory_get_GroupCount_m9454_MethodInfo = 
{
	"get_GroupCount"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IMachineFactory::get_Gap()
extern const MethodInfo IMachineFactory_get_Gap_m9455_MethodInfo = 
{
	"get_Gap"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IMachineFactory_t1930_IMachineFactory_set_Gap_m9456_ParameterInfos[] = 
{
	{"value", 0, 134218254, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_Gap(System.Int32)
extern const MethodInfo IMachineFactory_set_Gap_m9456_MethodInfo = 
{
	"set_Gap"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, IMachineFactory_t1930_IMachineFactory_set_Gap_m9456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.IMachineFactory::get_NamesMapping()
extern const MethodInfo IMachineFactory_get_NamesMapping_m9457_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
static const ParameterInfo IMachineFactory_t1930_IMachineFactory_set_NamesMapping_m9458_ParameterInfos[] = 
{
	{"value", 0, 134218255, 0, &StringU5BU5D_t109_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_NamesMapping(System.String[])
extern const MethodInfo IMachineFactory_set_NamesMapping_m9458_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IMachineFactory_t1930_IMachineFactory_set_NamesMapping_m9458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMachineFactory_t1930_MethodInfos[] =
{
	&IMachineFactory_NewInstance_m9451_MethodInfo,
	&IMachineFactory_get_Mapping_m9452_MethodInfo,
	&IMachineFactory_set_Mapping_m9453_MethodInfo,
	&IMachineFactory_get_GroupCount_m9454_MethodInfo,
	&IMachineFactory_get_Gap_m9455_MethodInfo,
	&IMachineFactory_set_Gap_m9456_MethodInfo,
	&IMachineFactory_get_NamesMapping_m9457_MethodInfo,
	&IMachineFactory_set_NamesMapping_m9458_MethodInfo,
	NULL
};
extern const MethodInfo IMachineFactory_get_Mapping_m9452_MethodInfo;
extern const MethodInfo IMachineFactory_set_Mapping_m9453_MethodInfo;
static const PropertyInfo IMachineFactory_t1930____Mapping_PropertyInfo = 
{
	&IMachineFactory_t1930_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &IMachineFactory_get_Mapping_m9452_MethodInfo/* get */
	, &IMachineFactory_set_Mapping_m9453_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_GroupCount_m9454_MethodInfo;
static const PropertyInfo IMachineFactory_t1930____GroupCount_PropertyInfo = 
{
	&IMachineFactory_t1930_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &IMachineFactory_get_GroupCount_m9454_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_Gap_m9455_MethodInfo;
extern const MethodInfo IMachineFactory_set_Gap_m9456_MethodInfo;
static const PropertyInfo IMachineFactory_t1930____Gap_PropertyInfo = 
{
	&IMachineFactory_t1930_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &IMachineFactory_get_Gap_m9455_MethodInfo/* get */
	, &IMachineFactory_set_Gap_m9456_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_NamesMapping_m9457_MethodInfo;
extern const MethodInfo IMachineFactory_set_NamesMapping_m9458_MethodInfo;
static const PropertyInfo IMachineFactory_t1930____NamesMapping_PropertyInfo = 
{
	&IMachineFactory_t1930_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &IMachineFactory_get_NamesMapping_m9457_MethodInfo/* get */
	, &IMachineFactory_set_NamesMapping_m9458_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMachineFactory_t1930_PropertyInfos[] =
{
	&IMachineFactory_t1930____Mapping_PropertyInfo,
	&IMachineFactory_t1930____GroupCount_PropertyInfo,
	&IMachineFactory_t1930____Gap_PropertyInfo,
	&IMachineFactory_t1930____NamesMapping_PropertyInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachineFactory_t1930_0_0_0;
extern const Il2CppType IMachineFactory_t1930_1_0_0;
struct IMachineFactory_t1930;
const Il2CppTypeDefinitionMetadata IMachineFactory_t1930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMachineFactory_t1930_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachineFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IMachineFactory_t1930_MethodInfos/* methods */
	, IMachineFactory_t1930_PropertyInfos/* properties */
	, NULL/* events */
	, &IMachineFactory_t1930_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachineFactory_t1930_0_0_0/* byval_arg */
	, &IMachineFactory_t1930_1_0_0/* this_arg */
	, &IMachineFactory_t1930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_Key.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache/Key
extern TypeInfo Key_t1937_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_KeyMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Key_t1937_Key__ctor_m8937_ParameterInfos[] = 
{
	{"pattern", 0, 134218262, 0, &String_t_0_0_0},
	{"options", 1, 134218263, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache/Key::.ctor(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Key__ctor_m8937_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Key__ctor_m8937/* method */
	, &Key_t1937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, Key_t1937_Key__ctor_m8937_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.FactoryCache/Key::GetHashCode()
extern const MethodInfo Key_GetHashCode_m8938_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Key_GetHashCode_m8938/* method */
	, &Key_t1937_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Key_t1937_Key_Equals_m8939_ParameterInfos[] = 
{
	{"o", 0, 134218264, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t169_0_0_0;
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.FactoryCache/Key::Equals(System.Object)
extern const MethodInfo Key_Equals_m8939_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Key_Equals_m8939/* method */
	, &Key_t1937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Key_t1937_Key_Equals_m8939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.FactoryCache/Key::ToString()
extern const MethodInfo Key_ToString_m8940_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Key_ToString_m8940/* method */
	, &Key_t1937_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Key_t1937_MethodInfos[] =
{
	&Key__ctor_m8937_MethodInfo,
	&Key_GetHashCode_m8938_MethodInfo,
	&Key_Equals_m8939_MethodInfo,
	&Key_ToString_m8940_MethodInfo,
	NULL
};
extern const MethodInfo Key_Equals_m8939_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Key_GetHashCode_m8938_MethodInfo;
extern const MethodInfo Key_ToString_m8940_MethodInfo;
static const Il2CppMethodReference Key_t1937_VTable[] =
{
	&Key_Equals_m8939_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Key_GetHashCode_m8938_MethodInfo,
	&Key_ToString_m8940_MethodInfo,
};
static bool Key_t1937_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Key_t1937_0_0_0;
extern const Il2CppType Key_t1937_1_0_0;
extern TypeInfo FactoryCache_t1929_il2cpp_TypeInfo;
extern const Il2CppType FactoryCache_t1929_0_0_0;
struct Key_t1937;
const Il2CppTypeDefinitionMetadata Key_t1937_DefinitionMetadata = 
{
	&FactoryCache_t1929_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Key_t1937_VTable/* vtableMethods */
	, Key_t1937_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 503/* fieldStart */

};
TypeInfo Key_t1937_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Key"/* name */
	, ""/* namespaze */
	, Key_t1937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Key_t1937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Key_t1937_0_0_0/* byval_arg */
	, &Key_t1937_1_0_0/* this_arg */
	, &Key_t1937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Key_t1937)/* instance_size */
	, sizeof (Key_t1937)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCacheMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo FactoryCache_t1929_FactoryCache__ctor_m8941_ParameterInfos[] = 
{
	{"capacity", 0, 134218256, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::.ctor(System.Int32)
extern const MethodInfo FactoryCache__ctor_m8941_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FactoryCache__ctor_m8941/* method */
	, &FactoryCache_t1929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, FactoryCache_t1929_FactoryCache__ctor_m8941_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
extern const Il2CppType IMachineFactory_t1930_0_0_0;
static const ParameterInfo FactoryCache_t1929_FactoryCache_Add_m8942_ParameterInfos[] = 
{
	{"pattern", 0, 134218257, 0, &String_t_0_0_0},
	{"options", 1, 134218258, 0, &RegexOptions_t1933_0_0_0},
	{"factory", 2, 134218259, 0, &IMachineFactory_t1930_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Add(System.String,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.IMachineFactory)
extern const MethodInfo FactoryCache_Add_m8942_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&FactoryCache_Add_m8942/* method */
	, &FactoryCache_t1929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t/* invoker_method */
	, FactoryCache_t1929_FactoryCache_Add_m8942_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Cleanup()
extern const MethodInfo FactoryCache_Cleanup_m8943_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&FactoryCache_Cleanup_m8943/* method */
	, &FactoryCache_t1929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo FactoryCache_t1929_FactoryCache_Lookup_m8944_ParameterInfos[] = 
{
	{"pattern", 0, 134218260, 0, &String_t_0_0_0},
	{"options", 1, 134218261, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.FactoryCache::Lookup(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo FactoryCache_Lookup_m8944_MethodInfo = 
{
	"Lookup"/* name */
	, (methodPointerType)&FactoryCache_Lookup_m8944/* method */
	, &FactoryCache_t1929_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1930_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, FactoryCache_t1929_FactoryCache_Lookup_m8944_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FactoryCache_t1929_MethodInfos[] =
{
	&FactoryCache__ctor_m8941_MethodInfo,
	&FactoryCache_Add_m8942_MethodInfo,
	&FactoryCache_Cleanup_m8943_MethodInfo,
	&FactoryCache_Lookup_m8944_MethodInfo,
	NULL
};
static const Il2CppType* FactoryCache_t1929_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Key_t1937_0_0_0,
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference FactoryCache_t1929_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool FactoryCache_t1929_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FactoryCache_t1929_1_0_0;
struct FactoryCache_t1929;
const Il2CppTypeDefinitionMetadata FactoryCache_t1929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FactoryCache_t1929_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FactoryCache_t1929_VTable/* vtableMethods */
	, FactoryCache_t1929_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 505/* fieldStart */

};
TypeInfo FactoryCache_t1929_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FactoryCache"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, FactoryCache_t1929_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FactoryCache_t1929_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FactoryCache_t1929_0_0_0/* byval_arg */
	, &FactoryCache_t1929_1_0_0/* this_arg */
	, &FactoryCache_t1929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FactoryCache_t1929)/* instance_size */
	, sizeof (FactoryCache_t1929)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// Metadata Definition System.Text.RegularExpressions.MRUList/Node
extern TypeInfo Node_t1939_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_NodeMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Node_t1939_Node__ctor_m8945_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
extern const MethodInfo Node__ctor_m8945_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Node__ctor_m8945/* method */
	, &Node_t1939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Node_t1939_Node__ctor_m8945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Node_t1939_MethodInfos[] =
{
	&Node__ctor_m8945_MethodInfo,
	NULL
};
static const Il2CppMethodReference Node_t1939_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Node_t1939_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Node_t1939_0_0_0;
extern const Il2CppType Node_t1939_1_0_0;
extern TypeInfo MRUList_t1938_il2cpp_TypeInfo;
extern const Il2CppType MRUList_t1938_0_0_0;
struct Node_t1939;
const Il2CppTypeDefinitionMetadata Node_t1939_DefinitionMetadata = 
{
	&MRUList_t1938_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Node_t1939_VTable/* vtableMethods */
	, Node_t1939_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 508/* fieldStart */

};
TypeInfo Node_t1939_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Node"/* name */
	, ""/* namespaze */
	, Node_t1939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Node_t1939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Node_t1939_0_0_0/* byval_arg */
	, &Node_t1939_1_0_0/* this_arg */
	, &Node_t1939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Node_t1939)/* instance_size */
	, sizeof (Node_t1939)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// Metadata Definition System.Text.RegularExpressions.MRUList
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::.ctor()
extern const MethodInfo MRUList__ctor_m8946_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MRUList__ctor_m8946/* method */
	, &MRUList_t1938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MRUList_t1938_MRUList_Use_m8947_ParameterInfos[] = 
{
	{"o", 0, 134218265, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::Use(System.Object)
extern const MethodInfo MRUList_Use_m8947_MethodInfo = 
{
	"Use"/* name */
	, (methodPointerType)&MRUList_Use_m8947/* method */
	, &MRUList_t1938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, MRUList_t1938_MRUList_Use_m8947_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.MRUList::Evict()
extern const MethodInfo MRUList_Evict_m8948_MethodInfo = 
{
	"Evict"/* name */
	, (methodPointerType)&MRUList_Evict_m8948/* method */
	, &MRUList_t1938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MRUList_t1938_MethodInfos[] =
{
	&MRUList__ctor_m8946_MethodInfo,
	&MRUList_Use_m8947_MethodInfo,
	&MRUList_Evict_m8948_MethodInfo,
	NULL
};
static const Il2CppType* MRUList_t1938_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Node_t1939_0_0_0,
};
static const Il2CppMethodReference MRUList_t1938_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MRUList_t1938_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MRUList_t1938_1_0_0;
struct MRUList_t1938;
const Il2CppTypeDefinitionMetadata MRUList_t1938_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MRUList_t1938_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MRUList_t1938_VTable/* vtableMethods */
	, MRUList_t1938_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 511/* fieldStart */

};
TypeInfo MRUList_t1938_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MRUList"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MRUList_t1938_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MRUList_t1938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MRUList_t1938_0_0_0/* byval_arg */
	, &MRUList_t1938_1_0_0/* this_arg */
	, &MRUList_t1938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MRUList_t1938)/* instance_size */
	, sizeof (MRUList_t1938)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// Metadata Definition System.Text.RegularExpressions.Category
extern TypeInfo Category_t1940_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_CategoryMethodDeclarations.h"
static const MethodInfo* Category_t1940_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference Category_t1940_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Category_t1940_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair Category_t1940_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Category_t1940_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t454_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Category_t1940_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Category_t1940_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Category_t1940_VTable/* vtableMethods */
	, Category_t1940_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 513/* fieldStart */

};
TypeInfo Category_t1940_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Category"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Category_t1940_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt16_t454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Category_t1940_0_0_0/* byval_arg */
	, &Category_t1940_1_0_0/* this_arg */
	, &Category_t1940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Category_t1940)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Category_t1940)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 146/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern TypeInfo CategoryUtils_t1941_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtilsMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CategoryUtils_t1941_CategoryUtils_CategoryFromName_m8949_ParameterInfos[] = 
{
	{"name", 0, 134218267, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Category_t1940_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.CategoryUtils::CategoryFromName(System.String)
extern const MethodInfo CategoryUtils_CategoryFromName_m8949_MethodInfo = 
{
	"CategoryFromName"/* name */
	, (methodPointerType)&CategoryUtils_CategoryFromName_m8949/* method */
	, &CategoryUtils_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1940_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1940_Object_t/* invoker_method */
	, CategoryUtils_t1941_CategoryUtils_CategoryFromName_m8949_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo CategoryUtils_t1941_CategoryUtils_IsCategory_m8950_ParameterInfos[] = 
{
	{"cat", 0, 134218268, 0, &Category_t1940_0_0_0},
	{"c", 1, 134218269, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_UInt16_t454_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Text.RegularExpressions.Category,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m8950_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m8950/* method */
	, &CategoryUtils_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_UInt16_t454_Int16_t534/* invoker_method */
	, CategoryUtils_t1941_CategoryUtils_IsCategory_m8950_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnicodeCategory_t2037_0_0_0;
extern const Il2CppType UnicodeCategory_t2037_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo CategoryUtils_t1941_CategoryUtils_IsCategory_m8951_ParameterInfos[] = 
{
	{"uc", 0, 134218270, 0, &UnicodeCategory_t2037_0_0_0},
	{"c", 1, 134218271, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Globalization.UnicodeCategory,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m8951_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m8951/* method */
	, &CategoryUtils_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127_Int16_t534/* invoker_method */
	, CategoryUtils_t1941_CategoryUtils_IsCategory_m8951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CategoryUtils_t1941_MethodInfos[] =
{
	&CategoryUtils_CategoryFromName_m8949_MethodInfo,
	&CategoryUtils_IsCategory_m8950_MethodInfo,
	&CategoryUtils_IsCategory_m8951_MethodInfo,
	NULL
};
static const Il2CppMethodReference CategoryUtils_t1941_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CategoryUtils_t1941_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CategoryUtils_t1941_0_0_0;
extern const Il2CppType CategoryUtils_t1941_1_0_0;
struct CategoryUtils_t1941;
const Il2CppTypeDefinitionMetadata CategoryUtils_t1941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CategoryUtils_t1941_VTable/* vtableMethods */
	, CategoryUtils_t1941_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CategoryUtils_t1941_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CategoryUtils"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, CategoryUtils_t1941_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CategoryUtils_t1941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CategoryUtils_t1941_0_0_0/* byval_arg */
	, &CategoryUtils_t1941_1_0_0/* this_arg */
	, &CategoryUtils_t1941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CategoryUtils_t1941)/* instance_size */
	, sizeof (CategoryUtils_t1941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern TypeInfo LinkRef_t1942_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern const MethodInfo LinkRef__ctor_m8952_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkRef__ctor_m8952/* method */
	, &LinkRef_t1942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkRef_t1942_MethodInfos[] =
{
	&LinkRef__ctor_m8952_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkRef_t1942_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool LinkRef_t1942_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkRef_t1942_0_0_0;
extern const Il2CppType LinkRef_t1942_1_0_0;
struct LinkRef_t1942;
const Il2CppTypeDefinitionMetadata LinkRef_t1942_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LinkRef_t1942_VTable/* vtableMethods */
	, LinkRef_t1942_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LinkRef_t1942_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkRef"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkRef_t1942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkRef_t1942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkRef_t1942_0_0_0/* byval_arg */
	, &LinkRef_t1942_1_0_0/* this_arg */
	, &LinkRef_t1942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkRef_t1942)/* instance_size */
	, sizeof (LinkRef_t1942)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.ICompiler
extern TypeInfo ICompiler_t1997_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.ICompiler::GetMachineFactory()
extern const MethodInfo ICompiler_GetMachineFactory_m9459_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1930_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFalse()
extern const MethodInfo ICompiler_EmitFalse_m9460_MethodInfo = 
{
	"EmitFalse"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTrue()
extern const MethodInfo ICompiler_EmitTrue_m9461_MethodInfo = 
{
	"EmitTrue"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitCharacter_m9462_ParameterInfos[] = 
{
	{"c", 0, 134218272, 0, &Char_t451_0_0_0},
	{"negate", 1, 134218273, 0, &Boolean_t169_0_0_0},
	{"ignore", 2, 134218274, 0, &Boolean_t169_0_0_0},
	{"reverse", 3, 134218275, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCharacter_m9462_MethodInfo = 
{
	"EmitCharacter"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitCharacter_m9462_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitCategory_m9463_ParameterInfos[] = 
{
	{"cat", 0, 134218276, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218277, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218278, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCategory_m9463_MethodInfo = 
{
	"EmitCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitCategory_m9463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitNotCategory_m9464_ParameterInfos[] = 
{
	{"cat", 0, 134218279, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218280, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218281, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitNotCategory_m9464_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitNotCategory_m9464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitRange_m9465_ParameterInfos[] = 
{
	{"lo", 0, 134218282, 0, &Char_t451_0_0_0},
	{"hi", 1, 134218283, 0, &Char_t451_0_0_0},
	{"negate", 2, 134218284, 0, &Boolean_t169_0_0_0},
	{"ignore", 3, 134218285, 0, &Boolean_t169_0_0_0},
	{"reverse", 4, 134218286, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitRange_m9465_MethodInfo = 
{
	"EmitRange"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitRange_m9465_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType BitArray_t1978_0_0_0;
extern const Il2CppType BitArray_t1978_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitSet_m9466_ParameterInfos[] = 
{
	{"lo", 0, 134218287, 0, &Char_t451_0_0_0},
	{"set", 1, 134218288, 0, &BitArray_t1978_0_0_0},
	{"negate", 2, 134218289, 0, &Boolean_t169_0_0_0},
	{"ignore", 3, 134218290, 0, &Boolean_t169_0_0_0},
	{"reverse", 4, 134218291, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Object_t_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitSet_m9466_MethodInfo = 
{
	"EmitSet"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Object_t_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitSet_m9466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitString_m9467_ParameterInfos[] = 
{
	{"str", 0, 134218292, 0, &String_t_0_0_0},
	{"ignore", 1, 134218293, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218294, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitString_m9467_MethodInfo = 
{
	"EmitString"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitString_m9467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1936_0_0_0;
extern const Il2CppType Position_t1936_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitPosition_m9468_ParameterInfos[] = 
{
	{"pos", 0, 134218295, 0, &Position_t1936_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo ICompiler_EmitPosition_m9468_MethodInfo = 
{
	"EmitPosition"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitPosition_m9468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitOpen_m9469_ParameterInfos[] = 
{
	{"gid", 0, 134218296, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitOpen(System.Int32)
extern const MethodInfo ICompiler_EmitOpen_m9469_MethodInfo = 
{
	"EmitOpen"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitOpen_m9469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitClose_m9470_ParameterInfos[] = 
{
	{"gid", 0, 134218297, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitClose(System.Int32)
extern const MethodInfo ICompiler_EmitClose_m9470_MethodInfo = 
{
	"EmitClose"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitClose_m9470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitBalanceStart_m9471_ParameterInfos[] = 
{
	{"gid", 0, 134218298, 0, &Int32_t127_0_0_0},
	{"balance", 1, 134218299, 0, &Int32_t127_0_0_0},
	{"capture", 2, 134218300, 0, &Boolean_t169_0_0_0},
	{"tail", 3, 134218301, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBalanceStart_m9471_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitBalanceStart_m9471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalance()
extern const MethodInfo ICompiler_EmitBalance_m9472_MethodInfo = 
{
	"EmitBalance"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitReference_m9473_ParameterInfos[] = 
{
	{"gid", 0, 134218302, 0, &Int32_t127_0_0_0},
	{"ignore", 1, 134218303, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218304, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitReference_m9473_MethodInfo = 
{
	"EmitReference"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170_SByte_t170/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitReference_m9473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitIfDefined_m9474_ParameterInfos[] = 
{
	{"gid", 0, 134218305, 0, &Int32_t127_0_0_0},
	{"tail", 1, 134218306, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIfDefined_m9474_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitIfDefined_m9474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitSub_m9475_ParameterInfos[] = 
{
	{"tail", 0, 134218307, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitSub_m9475_MethodInfo = 
{
	"EmitSub"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitSub_m9475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitTest_m9476_ParameterInfos[] = 
{
	{"yes", 0, 134218308, 0, &LinkRef_t1942_0_0_0},
	{"tail", 1, 134218309, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitTest_m9476_MethodInfo = 
{
	"EmitTest"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitTest_m9476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitBranch_m9477_ParameterInfos[] = 
{
	{"next", 0, 134218310, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBranch_m9477_MethodInfo = 
{
	"EmitBranch"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitBranch_m9477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitJump_m9478_ParameterInfos[] = 
{
	{"target", 0, 134218311, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitJump_m9478_MethodInfo = 
{
	"EmitJump"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitJump_m9478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitRepeat_m9479_ParameterInfos[] = 
{
	{"min", 0, 134218312, 0, &Int32_t127_0_0_0},
	{"max", 1, 134218313, 0, &Int32_t127_0_0_0},
	{"lazy", 2, 134218314, 0, &Boolean_t169_0_0_0},
	{"until", 3, 134218315, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitRepeat_m9479_MethodInfo = 
{
	"EmitRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitRepeat_m9479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitUntil_m9480_ParameterInfos[] = 
{
	{"repeat", 0, 134218316, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitUntil_m9480_MethodInfo = 
{
	"EmitUntil"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitUntil_m9480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitIn_m9481_ParameterInfos[] = 
{
	{"tail", 0, 134218317, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIn_m9481_MethodInfo = 
{
	"EmitIn"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitIn_m9481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitInfo_m9482_ParameterInfos[] = 
{
	{"count", 0, 134218318, 0, &Int32_t127_0_0_0},
	{"min", 1, 134218319, 0, &Int32_t127_0_0_0},
	{"max", 2, 134218320, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo ICompiler_EmitInfo_m9482_MethodInfo = 
{
	"EmitInfo"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitInfo_m9482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitFastRepeat_m9483_ParameterInfos[] = 
{
	{"min", 0, 134218321, 0, &Int32_t127_0_0_0},
	{"max", 1, 134218322, 0, &Int32_t127_0_0_0},
	{"lazy", 2, 134218323, 0, &Boolean_t169_0_0_0},
	{"tail", 3, 134218324, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitFastRepeat_m9483_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitFastRepeat_m9483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_EmitAnchor_m9484_ParameterInfos[] = 
{
	{"reverse", 0, 134218325, 0, &Boolean_t169_0_0_0},
	{"offset", 1, 134218326, 0, &Int32_t127_0_0_0},
	{"tail", 2, 134218327, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitAnchor_m9484_MethodInfo = 
{
	"EmitAnchor"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170_Int32_t127_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_EmitAnchor_m9484_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranchEnd()
extern const MethodInfo ICompiler_EmitBranchEnd_m9485_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAlternationEnd()
extern const MethodInfo ICompiler_EmitAlternationEnd_m9486_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink()
extern const MethodInfo ICompiler_NewLink_m9487_MethodInfo = 
{
	"NewLink"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1942_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo ICompiler_t1997_ICompiler_ResolveLink_m9488_ParameterInfos[] = 
{
	{"link", 0, 134218328, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_ResolveLink_m9488_MethodInfo = 
{
	"ResolveLink"/* name */
	, NULL/* method */
	, &ICompiler_t1997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ICompiler_t1997_ICompiler_ResolveLink_m9488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICompiler_t1997_MethodInfos[] =
{
	&ICompiler_GetMachineFactory_m9459_MethodInfo,
	&ICompiler_EmitFalse_m9460_MethodInfo,
	&ICompiler_EmitTrue_m9461_MethodInfo,
	&ICompiler_EmitCharacter_m9462_MethodInfo,
	&ICompiler_EmitCategory_m9463_MethodInfo,
	&ICompiler_EmitNotCategory_m9464_MethodInfo,
	&ICompiler_EmitRange_m9465_MethodInfo,
	&ICompiler_EmitSet_m9466_MethodInfo,
	&ICompiler_EmitString_m9467_MethodInfo,
	&ICompiler_EmitPosition_m9468_MethodInfo,
	&ICompiler_EmitOpen_m9469_MethodInfo,
	&ICompiler_EmitClose_m9470_MethodInfo,
	&ICompiler_EmitBalanceStart_m9471_MethodInfo,
	&ICompiler_EmitBalance_m9472_MethodInfo,
	&ICompiler_EmitReference_m9473_MethodInfo,
	&ICompiler_EmitIfDefined_m9474_MethodInfo,
	&ICompiler_EmitSub_m9475_MethodInfo,
	&ICompiler_EmitTest_m9476_MethodInfo,
	&ICompiler_EmitBranch_m9477_MethodInfo,
	&ICompiler_EmitJump_m9478_MethodInfo,
	&ICompiler_EmitRepeat_m9479_MethodInfo,
	&ICompiler_EmitUntil_m9480_MethodInfo,
	&ICompiler_EmitIn_m9481_MethodInfo,
	&ICompiler_EmitInfo_m9482_MethodInfo,
	&ICompiler_EmitFastRepeat_m9483_MethodInfo,
	&ICompiler_EmitAnchor_m9484_MethodInfo,
	&ICompiler_EmitBranchEnd_m9485_MethodInfo,
	&ICompiler_EmitAlternationEnd_m9486_MethodInfo,
	&ICompiler_NewLink_m9487_MethodInfo,
	&ICompiler_ResolveLink_m9488_MethodInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType ICompiler_t1997_1_0_0;
struct ICompiler_t1997;
const Il2CppTypeDefinitionMetadata ICompiler_t1997_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICompiler_t1997_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ICompiler_t1997_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICompiler_t1997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICompiler_t1997_0_0_0/* byval_arg */
	, &ICompiler_t1997_1_0_0/* this_arg */
	, &ICompiler_t1997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern TypeInfo InterpreterFactory_t1943_il2cpp_TypeInfo;
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactoryMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1060_0_0_0;
extern const Il2CppType UInt16U5BU5D_t1060_0_0_0;
static const ParameterInfo InterpreterFactory_t1943_InterpreterFactory__ctor_m8953_ParameterInfos[] = 
{
	{"pattern", 0, 134218329, 0, &UInt16U5BU5D_t1060_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::.ctor(System.UInt16[])
extern const MethodInfo InterpreterFactory__ctor_m8953_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterpreterFactory__ctor_m8953/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InterpreterFactory_t1943_InterpreterFactory__ctor_m8953_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.InterpreterFactory::NewInstance()
extern const MethodInfo InterpreterFactory_NewInstance_m8954_MethodInfo = 
{
	"NewInstance"/* name */
	, (methodPointerType)&InterpreterFactory_NewInstance_m8954/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t1927_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_GroupCount()
extern const MethodInfo InterpreterFactory_get_GroupCount_m8955_MethodInfo = 
{
	"get_GroupCount"/* name */
	, (methodPointerType)&InterpreterFactory_get_GroupCount_m8955/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_Gap()
extern const MethodInfo InterpreterFactory_get_Gap_m8956_MethodInfo = 
{
	"get_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_get_Gap_m8956/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo InterpreterFactory_t1943_InterpreterFactory_set_Gap_m8957_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Gap(System.Int32)
extern const MethodInfo InterpreterFactory_set_Gap_m8957_MethodInfo = 
{
	"set_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_set_Gap_m8957/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, InterpreterFactory_t1943_InterpreterFactory_set_Gap_m8957_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::get_Mapping()
extern const MethodInfo InterpreterFactory_get_Mapping_m8958_MethodInfo = 
{
	"get_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_Mapping_m8958/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1931_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1931_0_0_0;
static const ParameterInfo InterpreterFactory_t1943_InterpreterFactory_set_Mapping_m8959_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &IDictionary_t1931_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Mapping(System.Collections.IDictionary)
extern const MethodInfo InterpreterFactory_set_Mapping_m8959_MethodInfo = 
{
	"set_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_Mapping_m8959/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InterpreterFactory_t1943_InterpreterFactory_set_Mapping_m8959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.InterpreterFactory::get_NamesMapping()
extern const MethodInfo InterpreterFactory_get_NamesMapping_m8960_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_NamesMapping_m8960/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
static const ParameterInfo InterpreterFactory_t1943_InterpreterFactory_set_NamesMapping_m8961_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &StringU5BU5D_t109_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_NamesMapping(System.String[])
extern const MethodInfo InterpreterFactory_set_NamesMapping_m8961_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_NamesMapping_m8961/* method */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InterpreterFactory_t1943_InterpreterFactory_set_NamesMapping_m8961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterpreterFactory_t1943_MethodInfos[] =
{
	&InterpreterFactory__ctor_m8953_MethodInfo,
	&InterpreterFactory_NewInstance_m8954_MethodInfo,
	&InterpreterFactory_get_GroupCount_m8955_MethodInfo,
	&InterpreterFactory_get_Gap_m8956_MethodInfo,
	&InterpreterFactory_set_Gap_m8957_MethodInfo,
	&InterpreterFactory_get_Mapping_m8958_MethodInfo,
	&InterpreterFactory_set_Mapping_m8959_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m8960_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m8961_MethodInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_get_GroupCount_m8955_MethodInfo;
static const PropertyInfo InterpreterFactory_t1943____GroupCount_PropertyInfo = 
{
	&InterpreterFactory_t1943_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &InterpreterFactory_get_GroupCount_m8955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Gap_m8956_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Gap_m8957_MethodInfo;
static const PropertyInfo InterpreterFactory_t1943____Gap_PropertyInfo = 
{
	&InterpreterFactory_t1943_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &InterpreterFactory_get_Gap_m8956_MethodInfo/* get */
	, &InterpreterFactory_set_Gap_m8957_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Mapping_m8958_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Mapping_m8959_MethodInfo;
static const PropertyInfo InterpreterFactory_t1943____Mapping_PropertyInfo = 
{
	&InterpreterFactory_t1943_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &InterpreterFactory_get_Mapping_m8958_MethodInfo/* get */
	, &InterpreterFactory_set_Mapping_m8959_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_NamesMapping_m8960_MethodInfo;
extern const MethodInfo InterpreterFactory_set_NamesMapping_m8961_MethodInfo;
static const PropertyInfo InterpreterFactory_t1943____NamesMapping_PropertyInfo = 
{
	&InterpreterFactory_t1943_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &InterpreterFactory_get_NamesMapping_m8960_MethodInfo/* get */
	, &InterpreterFactory_set_NamesMapping_m8961_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* InterpreterFactory_t1943_PropertyInfos[] =
{
	&InterpreterFactory_t1943____GroupCount_PropertyInfo,
	&InterpreterFactory_t1943____Gap_PropertyInfo,
	&InterpreterFactory_t1943____Mapping_PropertyInfo,
	&InterpreterFactory_t1943____NamesMapping_PropertyInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_NewInstance_m8954_MethodInfo;
static const Il2CppMethodReference InterpreterFactory_t1943_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InterpreterFactory_NewInstance_m8954_MethodInfo,
	&InterpreterFactory_get_Mapping_m8958_MethodInfo,
	&InterpreterFactory_set_Mapping_m8959_MethodInfo,
	&InterpreterFactory_get_GroupCount_m8955_MethodInfo,
	&InterpreterFactory_get_Gap_m8956_MethodInfo,
	&InterpreterFactory_set_Gap_m8957_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m8960_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m8961_MethodInfo,
};
static bool InterpreterFactory_t1943_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* InterpreterFactory_t1943_InterfacesTypeInfos[] = 
{
	&IMachineFactory_t1930_0_0_0,
};
static Il2CppInterfaceOffsetPair InterpreterFactory_t1943_InterfacesOffsets[] = 
{
	{ &IMachineFactory_t1930_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType InterpreterFactory_t1943_0_0_0;
extern const Il2CppType InterpreterFactory_t1943_1_0_0;
struct InterpreterFactory_t1943;
const Il2CppTypeDefinitionMetadata InterpreterFactory_t1943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, InterpreterFactory_t1943_InterfacesTypeInfos/* implementedInterfaces */
	, InterpreterFactory_t1943_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InterpreterFactory_t1943_VTable/* vtableMethods */
	, InterpreterFactory_t1943_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 659/* fieldStart */

};
TypeInfo InterpreterFactory_t1943_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterpreterFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, InterpreterFactory_t1943_MethodInfos/* methods */
	, InterpreterFactory_t1943_PropertyInfos/* properties */
	, NULL/* events */
	, &InterpreterFactory_t1943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InterpreterFactory_t1943_0_0_0/* byval_arg */
	, &InterpreterFactory_t1943_1_0_0/* this_arg */
	, &InterpreterFactory_t1943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterpreterFactory_t1943)/* instance_size */
	, sizeof (InterpreterFactory_t1943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern TypeInfo Link_t1944_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_PatterMethodDeclarations.h"
static const MethodInfo* Link_t1944_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference Link_t1944_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Link_t1944_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Link_t1944_0_0_0;
extern const Il2CppType Link_t1944_1_0_0;
extern const Il2CppType ValueType_t524_0_0_0;
extern TypeInfo PatternLinkStack_t1945_il2cpp_TypeInfo;
extern const Il2CppType PatternLinkStack_t1945_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1944_DefinitionMetadata = 
{
	&PatternLinkStack_t1945_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Link_t1944_VTable/* vtableMethods */
	, Link_t1944_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 663/* fieldStart */

};
TypeInfo Link_t1944_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1944_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Link_t1944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1944_0_0_0/* byval_arg */
	, &Link_t1944_1_0_0/* this_arg */
	, &Link_t1944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t1944)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t1944)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t1944 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::.ctor()
extern const MethodInfo PatternLinkStack__ctor_m8962_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternLinkStack__ctor_m8962/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternLinkStack_t1945_PatternLinkStack_set_BaseAddress_m8963_ParameterInfos[] = 
{
	{"value", 0, 134218403, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_BaseAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_BaseAddress_m8963_MethodInfo = 
{
	"set_BaseAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_BaseAddress_m8963/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, PatternLinkStack_t1945_PatternLinkStack_set_BaseAddress_m8963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::get_OffsetAddress()
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m8964_MethodInfo = 
{
	"get_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_get_OffsetAddress_m8964/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternLinkStack_t1945_PatternLinkStack_set_OffsetAddress_m8965_ParameterInfos[] = 
{
	{"value", 0, 134218404, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_OffsetAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m8965_MethodInfo = 
{
	"set_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_OffsetAddress_m8965/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, PatternLinkStack_t1945_PatternLinkStack_set_OffsetAddress_m8965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternLinkStack_t1945_PatternLinkStack_GetOffset_m8966_ParameterInfos[] = 
{
	{"target_addr", 0, 134218405, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetOffset(System.Int32)
extern const MethodInfo PatternLinkStack_GetOffset_m8966_MethodInfo = 
{
	"GetOffset"/* name */
	, (methodPointerType)&PatternLinkStack_GetOffset_m8966/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, PatternLinkStack_t1945_PatternLinkStack_GetOffset_m8966_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetCurrent()
extern const MethodInfo PatternLinkStack_GetCurrent_m8967_MethodInfo = 
{
	"GetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_GetCurrent_m8967/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PatternLinkStack_t1945_PatternLinkStack_SetCurrent_m8968_ParameterInfos[] = 
{
	{"l", 0, 134218406, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::SetCurrent(System.Object)
extern const MethodInfo PatternLinkStack_SetCurrent_m8968_MethodInfo = 
{
	"SetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_SetCurrent_m8968/* method */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternLinkStack_t1945_PatternLinkStack_SetCurrent_m8968_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternLinkStack_t1945_MethodInfos[] =
{
	&PatternLinkStack__ctor_m8962_MethodInfo,
	&PatternLinkStack_set_BaseAddress_m8963_MethodInfo,
	&PatternLinkStack_get_OffsetAddress_m8964_MethodInfo,
	&PatternLinkStack_set_OffsetAddress_m8965_MethodInfo,
	&PatternLinkStack_GetOffset_m8966_MethodInfo,
	&PatternLinkStack_GetCurrent_m8967_MethodInfo,
	&PatternLinkStack_SetCurrent_m8968_MethodInfo,
	NULL
};
extern const MethodInfo PatternLinkStack_set_BaseAddress_m8963_MethodInfo;
static const PropertyInfo PatternLinkStack_t1945____BaseAddress_PropertyInfo = 
{
	&PatternLinkStack_t1945_il2cpp_TypeInfo/* parent */
	, "BaseAddress"/* name */
	, NULL/* get */
	, &PatternLinkStack_set_BaseAddress_m8963_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m8964_MethodInfo;
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m8965_MethodInfo;
static const PropertyInfo PatternLinkStack_t1945____OffsetAddress_PropertyInfo = 
{
	&PatternLinkStack_t1945_il2cpp_TypeInfo/* parent */
	, "OffsetAddress"/* name */
	, &PatternLinkStack_get_OffsetAddress_m8964_MethodInfo/* get */
	, &PatternLinkStack_set_OffsetAddress_m8965_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternLinkStack_t1945_PropertyInfos[] =
{
	&PatternLinkStack_t1945____BaseAddress_PropertyInfo,
	&PatternLinkStack_t1945____OffsetAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternLinkStack_t1945_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Link_t1944_0_0_0,
};
extern const MethodInfo PatternLinkStack_GetCurrent_m8967_MethodInfo;
extern const MethodInfo PatternLinkStack_SetCurrent_m8968_MethodInfo;
static const Il2CppMethodReference PatternLinkStack_t1945_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&PatternLinkStack_GetCurrent_m8967_MethodInfo,
	&PatternLinkStack_SetCurrent_m8968_MethodInfo,
};
static bool PatternLinkStack_t1945_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternLinkStack_t1945_1_0_0;
extern const Il2CppType LinkStack_t1946_0_0_0;
extern TypeInfo PatternCompiler_t1947_il2cpp_TypeInfo;
extern const Il2CppType PatternCompiler_t1947_0_0_0;
struct PatternLinkStack_t1945;
const Il2CppTypeDefinitionMetadata PatternLinkStack_t1945_DefinitionMetadata = 
{
	&PatternCompiler_t1947_0_0_0/* declaringType */
	, PatternLinkStack_t1945_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkStack_t1946_0_0_0/* parent */
	, PatternLinkStack_t1945_VTable/* vtableMethods */
	, PatternLinkStack_t1945_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 665/* fieldStart */

};
TypeInfo PatternLinkStack_t1945_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternLinkStack"/* name */
	, ""/* namespaze */
	, PatternLinkStack_t1945_MethodInfos/* methods */
	, PatternLinkStack_t1945_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternLinkStack_t1945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternLinkStack_t1945_0_0_0/* byval_arg */
	, &PatternLinkStack_t1945_1_0_0/* this_arg */
	, &PatternLinkStack_t1945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternLinkStack_t1945)/* instance_size */
	, sizeof (PatternLinkStack_t1945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompilerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::.ctor()
extern const MethodInfo PatternCompiler__ctor_m8969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternCompiler__ctor_m8969/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1934_0_0_0;
extern const Il2CppType OpCode_t1934_0_0_0;
extern const Il2CppType OpFlags_t1935_0_0_0;
extern const Il2CppType OpFlags_t1935_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EncodeOp_m8970_ParameterInfos[] = 
{
	{"op", 0, 134218333, 0, &OpCode_t1934_0_0_0},
	{"flags", 1, 134218334, 0, &OpFlags_t1935_0_0_0},
};
extern const Il2CppType UInt16_t454_0_0_0;
extern void* RuntimeInvoker_UInt16_t454_UInt16_t454_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.UInt16 System.Text.RegularExpressions.PatternCompiler::EncodeOp(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_EncodeOp_m8970_MethodInfo = 
{
	"EncodeOp"/* name */
	, (methodPointerType)&PatternCompiler_EncodeOp_m8970/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &UInt16_t454_0_0_0/* return_type */
	, RuntimeInvoker_UInt16_t454_UInt16_t454_UInt16_t454/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EncodeOp_m8970_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.PatternCompiler::GetMachineFactory()
extern const MethodInfo PatternCompiler_GetMachineFactory_m8971_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, (methodPointerType)&PatternCompiler_GetMachineFactory_m8971/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1930_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFalse()
extern const MethodInfo PatternCompiler_EmitFalse_m8972_MethodInfo = 
{
	"EmitFalse"/* name */
	, (methodPointerType)&PatternCompiler_EmitFalse_m8972/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTrue()
extern const MethodInfo PatternCompiler_EmitTrue_m8973_MethodInfo = 
{
	"EmitTrue"/* name */
	, (methodPointerType)&PatternCompiler_EmitTrue_m8973/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitCount_m8974_ParameterInfos[] = 
{
	{"count", 0, 134218335, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCount(System.Int32)
extern const MethodInfo PatternCompiler_EmitCount_m8974_MethodInfo = 
{
	"EmitCount"/* name */
	, (methodPointerType)&PatternCompiler_EmitCount_m8974/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitCount_m8974_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitCharacter_m8975_ParameterInfos[] = 
{
	{"c", 0, 134218336, 0, &Char_t451_0_0_0},
	{"negate", 1, 134218337, 0, &Boolean_t169_0_0_0},
	{"ignore", 2, 134218338, 0, &Boolean_t169_0_0_0},
	{"reverse", 3, 134218339, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCharacter_m8975_MethodInfo = 
{
	"EmitCharacter"/* name */
	, (methodPointerType)&PatternCompiler_EmitCharacter_m8975/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitCharacter_m8975_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitCategory_m8976_ParameterInfos[] = 
{
	{"cat", 0, 134218340, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218341, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218342, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCategory_m8976_MethodInfo = 
{
	"EmitCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitCategory_m8976/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitCategory_m8976_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitNotCategory_m8977_ParameterInfos[] = 
{
	{"cat", 0, 134218343, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218344, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218345, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitNotCategory_m8977_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitNotCategory_m8977/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitNotCategory_m8977_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitRange_m8978_ParameterInfos[] = 
{
	{"lo", 0, 134218346, 0, &Char_t451_0_0_0},
	{"hi", 1, 134218347, 0, &Char_t451_0_0_0},
	{"negate", 2, 134218348, 0, &Boolean_t169_0_0_0},
	{"ignore", 3, 134218349, 0, &Boolean_t169_0_0_0},
	{"reverse", 4, 134218350, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitRange_m8978_MethodInfo = 
{
	"EmitRange"/* name */
	, (methodPointerType)&PatternCompiler_EmitRange_m8978/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Int16_t534_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitRange_m8978_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType BitArray_t1978_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitSet_m8979_ParameterInfos[] = 
{
	{"lo", 0, 134218351, 0, &Char_t451_0_0_0},
	{"set", 1, 134218352, 0, &BitArray_t1978_0_0_0},
	{"negate", 2, 134218353, 0, &Boolean_t169_0_0_0},
	{"ignore", 3, 134218354, 0, &Boolean_t169_0_0_0},
	{"reverse", 4, 134218355, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Object_t_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitSet_m8979_MethodInfo = 
{
	"EmitSet"/* name */
	, (methodPointerType)&PatternCompiler_EmitSet_m8979/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Object_t_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitSet_m8979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitString_m8980_ParameterInfos[] = 
{
	{"str", 0, 134218356, 0, &String_t_0_0_0},
	{"ignore", 1, 134218357, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218358, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitString_m8980_MethodInfo = 
{
	"EmitString"/* name */
	, (methodPointerType)&PatternCompiler_EmitString_m8980/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitString_m8980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1936_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitPosition_m8981_ParameterInfos[] = 
{
	{"pos", 0, 134218359, 0, &Position_t1936_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo PatternCompiler_EmitPosition_m8981_MethodInfo = 
{
	"EmitPosition"/* name */
	, (methodPointerType)&PatternCompiler_EmitPosition_m8981/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitPosition_m8981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitOpen_m8982_ParameterInfos[] = 
{
	{"gid", 0, 134218360, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitOpen(System.Int32)
extern const MethodInfo PatternCompiler_EmitOpen_m8982_MethodInfo = 
{
	"EmitOpen"/* name */
	, (methodPointerType)&PatternCompiler_EmitOpen_m8982/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitOpen_m8982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitClose_m8983_ParameterInfos[] = 
{
	{"gid", 0, 134218361, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitClose(System.Int32)
extern const MethodInfo PatternCompiler_EmitClose_m8983_MethodInfo = 
{
	"EmitClose"/* name */
	, (methodPointerType)&PatternCompiler_EmitClose_m8983/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitClose_m8983_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitBalanceStart_m8984_ParameterInfos[] = 
{
	{"gid", 0, 134218362, 0, &Int32_t127_0_0_0},
	{"balance", 1, 134218363, 0, &Int32_t127_0_0_0},
	{"capture", 2, 134218364, 0, &Boolean_t169_0_0_0},
	{"tail", 3, 134218365, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBalanceStart_m8984_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalanceStart_m8984/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitBalanceStart_m8984_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalance()
extern const MethodInfo PatternCompiler_EmitBalance_m8985_MethodInfo = 
{
	"EmitBalance"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalance_m8985/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitReference_m8986_ParameterInfos[] = 
{
	{"gid", 0, 134218366, 0, &Int32_t127_0_0_0},
	{"ignore", 1, 134218367, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218368, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitReference_m8986_MethodInfo = 
{
	"EmitReference"/* name */
	, (methodPointerType)&PatternCompiler_EmitReference_m8986/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitReference_m8986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitIfDefined_m8987_ParameterInfos[] = 
{
	{"gid", 0, 134218369, 0, &Int32_t127_0_0_0},
	{"tail", 1, 134218370, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIfDefined_m8987_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, (methodPointerType)&PatternCompiler_EmitIfDefined_m8987/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitIfDefined_m8987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitSub_m8988_ParameterInfos[] = 
{
	{"tail", 0, 134218371, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitSub_m8988_MethodInfo = 
{
	"EmitSub"/* name */
	, (methodPointerType)&PatternCompiler_EmitSub_m8988/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitSub_m8988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitTest_m8989_ParameterInfos[] = 
{
	{"yes", 0, 134218372, 0, &LinkRef_t1942_0_0_0},
	{"tail", 1, 134218373, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitTest_m8989_MethodInfo = 
{
	"EmitTest"/* name */
	, (methodPointerType)&PatternCompiler_EmitTest_m8989/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitTest_m8989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitBranch_m8990_ParameterInfos[] = 
{
	{"next", 0, 134218374, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBranch_m8990_MethodInfo = 
{
	"EmitBranch"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranch_m8990/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitBranch_m8990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitJump_m8991_ParameterInfos[] = 
{
	{"target", 0, 134218375, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitJump_m8991_MethodInfo = 
{
	"EmitJump"/* name */
	, (methodPointerType)&PatternCompiler_EmitJump_m8991/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitJump_m8991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitRepeat_m8992_ParameterInfos[] = 
{
	{"min", 0, 134218376, 0, &Int32_t127_0_0_0},
	{"max", 1, 134218377, 0, &Int32_t127_0_0_0},
	{"lazy", 2, 134218378, 0, &Boolean_t169_0_0_0},
	{"until", 3, 134218379, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitRepeat_m8992_MethodInfo = 
{
	"EmitRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitRepeat_m8992/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitRepeat_m8992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitUntil_m8993_ParameterInfos[] = 
{
	{"repeat", 0, 134218380, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitUntil_m8993_MethodInfo = 
{
	"EmitUntil"/* name */
	, (methodPointerType)&PatternCompiler_EmitUntil_m8993/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitUntil_m8993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitFastRepeat_m8994_ParameterInfos[] = 
{
	{"min", 0, 134218381, 0, &Int32_t127_0_0_0},
	{"max", 1, 134218382, 0, &Int32_t127_0_0_0},
	{"lazy", 2, 134218383, 0, &Boolean_t169_0_0_0},
	{"tail", 3, 134218384, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitFastRepeat_m8994_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitFastRepeat_m8994/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitFastRepeat_m8994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitIn_m8995_ParameterInfos[] = 
{
	{"tail", 0, 134218385, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIn_m8995_MethodInfo = 
{
	"EmitIn"/* name */
	, (methodPointerType)&PatternCompiler_EmitIn_m8995/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitIn_m8995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitAnchor_m8996_ParameterInfos[] = 
{
	{"reverse", 0, 134218386, 0, &Boolean_t169_0_0_0},
	{"offset", 1, 134218387, 0, &Int32_t127_0_0_0},
	{"tail", 2, 134218388, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitAnchor_m8996_MethodInfo = 
{
	"EmitAnchor"/* name */
	, (methodPointerType)&PatternCompiler_EmitAnchor_m8996/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170_Int32_t127_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitAnchor_m8996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitInfo_m8997_ParameterInfos[] = 
{
	{"count", 0, 134218389, 0, &Int32_t127_0_0_0},
	{"min", 1, 134218390, 0, &Int32_t127_0_0_0},
	{"max", 2, 134218391, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo PatternCompiler_EmitInfo_m8997_MethodInfo = 
{
	"EmitInfo"/* name */
	, (methodPointerType)&PatternCompiler_EmitInfo_m8997/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitInfo_m8997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.PatternCompiler::NewLink()
extern const MethodInfo PatternCompiler_NewLink_m8998_MethodInfo = 
{
	"NewLink"/* name */
	, (methodPointerType)&PatternCompiler_NewLink_m8998/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1942_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_ResolveLink_m8999_ParameterInfos[] = 
{
	{"lref", 0, 134218392, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_ResolveLink_m8999_MethodInfo = 
{
	"ResolveLink"/* name */
	, (methodPointerType)&PatternCompiler_ResolveLink_m8999/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_ResolveLink_m8999_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranchEnd()
extern const MethodInfo PatternCompiler_EmitBranchEnd_m9000_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranchEnd_m9000/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAlternationEnd()
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m9001_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitAlternationEnd_m9001/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_MakeFlags_m9002_ParameterInfos[] = 
{
	{"negate", 0, 134218393, 0, &Boolean_t169_0_0_0},
	{"ignore", 1, 134218394, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218395, 0, &Boolean_t169_0_0_0},
	{"lazy", 3, 134218396, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_OpFlags_t1935_SByte_t170_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.OpFlags System.Text.RegularExpressions.PatternCompiler::MakeFlags(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_MakeFlags_m9002_MethodInfo = 
{
	"MakeFlags"/* name */
	, (methodPointerType)&PatternCompiler_MakeFlags_m9002/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &OpFlags_t1935_0_0_0/* return_type */
	, RuntimeInvoker_OpFlags_t1935_SByte_t170_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_MakeFlags_m9002_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1934_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_Emit_m9003_ParameterInfos[] = 
{
	{"op", 0, 134218397, 0, &OpCode_t1934_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode)
extern const MethodInfo PatternCompiler_Emit_m9003_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m9003/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_Emit_m9003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1934_0_0_0;
extern const Il2CppType OpFlags_t1935_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_Emit_m9004_ParameterInfos[] = 
{
	{"op", 0, 134218398, 0, &OpCode_t1934_0_0_0},
	{"flags", 1, 134218399, 0, &OpFlags_t1935_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_Emit_m9004_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m9004/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_UInt16_t454/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_Emit_m9004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt16_t454_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_Emit_m9005_ParameterInfos[] = 
{
	{"word", 0, 134218400, 0, &UInt16_t454_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.UInt16)
extern const MethodInfo PatternCompiler_Emit_m9005_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m9005/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_Emit_m9005_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler::get_CurrentAddress()
extern const MethodInfo PatternCompiler_get_CurrentAddress_m9006_MethodInfo = 
{
	"get_CurrentAddress"/* name */
	, (methodPointerType)&PatternCompiler_get_CurrentAddress_m9006/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_BeginLink_m9007_ParameterInfos[] = 
{
	{"lref", 0, 134218401, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::BeginLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_BeginLink_m9007_MethodInfo = 
{
	"BeginLink"/* name */
	, (methodPointerType)&PatternCompiler_BeginLink_m9007/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_BeginLink_m9007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1942_0_0_0;
static const ParameterInfo PatternCompiler_t1947_PatternCompiler_EmitLink_m9008_ParameterInfos[] = 
{
	{"lref", 0, 134218402, 0, &LinkRef_t1942_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitLink_m9008_MethodInfo = 
{
	"EmitLink"/* name */
	, (methodPointerType)&PatternCompiler_EmitLink_m9008/* method */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PatternCompiler_t1947_PatternCompiler_EmitLink_m9008_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternCompiler_t1947_MethodInfos[] =
{
	&PatternCompiler__ctor_m8969_MethodInfo,
	&PatternCompiler_EncodeOp_m8970_MethodInfo,
	&PatternCompiler_GetMachineFactory_m8971_MethodInfo,
	&PatternCompiler_EmitFalse_m8972_MethodInfo,
	&PatternCompiler_EmitTrue_m8973_MethodInfo,
	&PatternCompiler_EmitCount_m8974_MethodInfo,
	&PatternCompiler_EmitCharacter_m8975_MethodInfo,
	&PatternCompiler_EmitCategory_m8976_MethodInfo,
	&PatternCompiler_EmitNotCategory_m8977_MethodInfo,
	&PatternCompiler_EmitRange_m8978_MethodInfo,
	&PatternCompiler_EmitSet_m8979_MethodInfo,
	&PatternCompiler_EmitString_m8980_MethodInfo,
	&PatternCompiler_EmitPosition_m8981_MethodInfo,
	&PatternCompiler_EmitOpen_m8982_MethodInfo,
	&PatternCompiler_EmitClose_m8983_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m8984_MethodInfo,
	&PatternCompiler_EmitBalance_m8985_MethodInfo,
	&PatternCompiler_EmitReference_m8986_MethodInfo,
	&PatternCompiler_EmitIfDefined_m8987_MethodInfo,
	&PatternCompiler_EmitSub_m8988_MethodInfo,
	&PatternCompiler_EmitTest_m8989_MethodInfo,
	&PatternCompiler_EmitBranch_m8990_MethodInfo,
	&PatternCompiler_EmitJump_m8991_MethodInfo,
	&PatternCompiler_EmitRepeat_m8992_MethodInfo,
	&PatternCompiler_EmitUntil_m8993_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m8994_MethodInfo,
	&PatternCompiler_EmitIn_m8995_MethodInfo,
	&PatternCompiler_EmitAnchor_m8996_MethodInfo,
	&PatternCompiler_EmitInfo_m8997_MethodInfo,
	&PatternCompiler_NewLink_m8998_MethodInfo,
	&PatternCompiler_ResolveLink_m8999_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m9000_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m9001_MethodInfo,
	&PatternCompiler_MakeFlags_m9002_MethodInfo,
	&PatternCompiler_Emit_m9003_MethodInfo,
	&PatternCompiler_Emit_m9004_MethodInfo,
	&PatternCompiler_Emit_m9005_MethodInfo,
	&PatternCompiler_get_CurrentAddress_m9006_MethodInfo,
	&PatternCompiler_BeginLink_m9007_MethodInfo,
	&PatternCompiler_EmitLink_m9008_MethodInfo,
	NULL
};
extern const MethodInfo PatternCompiler_get_CurrentAddress_m9006_MethodInfo;
static const PropertyInfo PatternCompiler_t1947____CurrentAddress_PropertyInfo = 
{
	&PatternCompiler_t1947_il2cpp_TypeInfo/* parent */
	, "CurrentAddress"/* name */
	, &PatternCompiler_get_CurrentAddress_m9006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternCompiler_t1947_PropertyInfos[] =
{
	&PatternCompiler_t1947____CurrentAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternCompiler_t1947_il2cpp_TypeInfo__nestedTypes[1] =
{
	&PatternLinkStack_t1945_0_0_0,
};
extern const MethodInfo PatternCompiler_GetMachineFactory_m8971_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFalse_m8972_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTrue_m8973_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCharacter_m8975_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCategory_m8976_MethodInfo;
extern const MethodInfo PatternCompiler_EmitNotCategory_m8977_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRange_m8978_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSet_m8979_MethodInfo;
extern const MethodInfo PatternCompiler_EmitString_m8980_MethodInfo;
extern const MethodInfo PatternCompiler_EmitPosition_m8981_MethodInfo;
extern const MethodInfo PatternCompiler_EmitOpen_m8982_MethodInfo;
extern const MethodInfo PatternCompiler_EmitClose_m8983_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalanceStart_m8984_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalance_m8985_MethodInfo;
extern const MethodInfo PatternCompiler_EmitReference_m8986_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIfDefined_m8987_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSub_m8988_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTest_m8989_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranch_m8990_MethodInfo;
extern const MethodInfo PatternCompiler_EmitJump_m8991_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRepeat_m8992_MethodInfo;
extern const MethodInfo PatternCompiler_EmitUntil_m8993_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIn_m8995_MethodInfo;
extern const MethodInfo PatternCompiler_EmitInfo_m8997_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFastRepeat_m8994_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAnchor_m8996_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranchEnd_m9000_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m9001_MethodInfo;
extern const MethodInfo PatternCompiler_NewLink_m8998_MethodInfo;
extern const MethodInfo PatternCompiler_ResolveLink_m8999_MethodInfo;
static const Il2CppMethodReference PatternCompiler_t1947_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&PatternCompiler_GetMachineFactory_m8971_MethodInfo,
	&PatternCompiler_EmitFalse_m8972_MethodInfo,
	&PatternCompiler_EmitTrue_m8973_MethodInfo,
	&PatternCompiler_EmitCharacter_m8975_MethodInfo,
	&PatternCompiler_EmitCategory_m8976_MethodInfo,
	&PatternCompiler_EmitNotCategory_m8977_MethodInfo,
	&PatternCompiler_EmitRange_m8978_MethodInfo,
	&PatternCompiler_EmitSet_m8979_MethodInfo,
	&PatternCompiler_EmitString_m8980_MethodInfo,
	&PatternCompiler_EmitPosition_m8981_MethodInfo,
	&PatternCompiler_EmitOpen_m8982_MethodInfo,
	&PatternCompiler_EmitClose_m8983_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m8984_MethodInfo,
	&PatternCompiler_EmitBalance_m8985_MethodInfo,
	&PatternCompiler_EmitReference_m8986_MethodInfo,
	&PatternCompiler_EmitIfDefined_m8987_MethodInfo,
	&PatternCompiler_EmitSub_m8988_MethodInfo,
	&PatternCompiler_EmitTest_m8989_MethodInfo,
	&PatternCompiler_EmitBranch_m8990_MethodInfo,
	&PatternCompiler_EmitJump_m8991_MethodInfo,
	&PatternCompiler_EmitRepeat_m8992_MethodInfo,
	&PatternCompiler_EmitUntil_m8993_MethodInfo,
	&PatternCompiler_EmitIn_m8995_MethodInfo,
	&PatternCompiler_EmitInfo_m8997_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m8994_MethodInfo,
	&PatternCompiler_EmitAnchor_m8996_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m9000_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m9001_MethodInfo,
	&PatternCompiler_NewLink_m8998_MethodInfo,
	&PatternCompiler_ResolveLink_m8999_MethodInfo,
};
static bool PatternCompiler_t1947_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PatternCompiler_t1947_InterfacesTypeInfos[] = 
{
	&ICompiler_t1997_0_0_0,
};
static Il2CppInterfaceOffsetPair PatternCompiler_t1947_InterfacesOffsets[] = 
{
	{ &ICompiler_t1997_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternCompiler_t1947_1_0_0;
struct PatternCompiler_t1947;
const Il2CppTypeDefinitionMetadata PatternCompiler_t1947_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PatternCompiler_t1947_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PatternCompiler_t1947_InterfacesTypeInfos/* implementedInterfaces */
	, PatternCompiler_t1947_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PatternCompiler_t1947_VTable/* vtableMethods */
	, PatternCompiler_t1947_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 666/* fieldStart */

};
TypeInfo PatternCompiler_t1947_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternCompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, PatternCompiler_t1947_MethodInfos/* methods */
	, PatternCompiler_t1947_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternCompiler_t1947_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternCompiler_t1947_0_0_0/* byval_arg */
	, &PatternCompiler_t1947_1_0_0/* this_arg */
	, &PatternCompiler_t1947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternCompiler_t1947)/* instance_size */
	, sizeof (PatternCompiler_t1947)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern TypeInfo LinkStack_t1946_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
extern const MethodInfo LinkStack__ctor_m9009_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkStack__ctor_m9009/* method */
	, &LinkStack_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::Push()
extern const MethodInfo LinkStack_Push_m9010_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&LinkStack_Push_m9010/* method */
	, &LinkStack_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
extern const MethodInfo LinkStack_Pop_m9011_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&LinkStack_Pop_m9011/* method */
	, &LinkStack_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
extern const MethodInfo LinkStack_GetCurrent_m9489_MethodInfo = 
{
	"GetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LinkStack_t1946_LinkStack_SetCurrent_m9490_ParameterInfos[] = 
{
	{"l", 0, 134218407, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
extern const MethodInfo LinkStack_SetCurrent_m9490_MethodInfo = 
{
	"SetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LinkStack_t1946_LinkStack_SetCurrent_m9490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkStack_t1946_MethodInfos[] =
{
	&LinkStack__ctor_m9009_MethodInfo,
	&LinkStack_Push_m9010_MethodInfo,
	&LinkStack_Pop_m9011_MethodInfo,
	&LinkStack_GetCurrent_m9489_MethodInfo,
	&LinkStack_SetCurrent_m9490_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkStack_t1946_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
};
static bool LinkStack_t1946_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkStack_t1946_1_0_0;
struct LinkStack_t1946;
const Il2CppTypeDefinitionMetadata LinkStack_t1946_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkRef_t1942_0_0_0/* parent */
	, LinkStack_t1946_VTable/* vtableMethods */
	, LinkStack_t1946_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 667/* fieldStart */

};
TypeInfo LinkStack_t1946_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkStack"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkStack_t1946_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkStack_t1946_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkStack_t1946_0_0_0/* byval_arg */
	, &LinkStack_t1946_1_0_0/* this_arg */
	, &LinkStack_t1946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkStack_t1946)/* instance_size */
	, sizeof (LinkStack_t1946)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// Metadata Definition System.Text.RegularExpressions.Mark
extern TypeInfo Mark_t1948_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_MarkMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
extern const MethodInfo Mark_get_IsDefined_m9012_MethodInfo = 
{
	"get_IsDefined"/* name */
	, (methodPointerType)&Mark_get_IsDefined_m9012/* method */
	, &Mark_t1948_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
extern const MethodInfo Mark_get_Index_m9013_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&Mark_get_Index_m9013/* method */
	, &Mark_t1948_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
extern const MethodInfo Mark_get_Length_m9014_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&Mark_get_Length_m9014/* method */
	, &Mark_t1948_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mark_t1948_MethodInfos[] =
{
	&Mark_get_IsDefined_m9012_MethodInfo,
	&Mark_get_Index_m9013_MethodInfo,
	&Mark_get_Length_m9014_MethodInfo,
	NULL
};
extern const MethodInfo Mark_get_IsDefined_m9012_MethodInfo;
static const PropertyInfo Mark_t1948____IsDefined_PropertyInfo = 
{
	&Mark_t1948_il2cpp_TypeInfo/* parent */
	, "IsDefined"/* name */
	, &Mark_get_IsDefined_m9012_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Index_m9013_MethodInfo;
static const PropertyInfo Mark_t1948____Index_PropertyInfo = 
{
	&Mark_t1948_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &Mark_get_Index_m9013_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Length_m9014_MethodInfo;
static const PropertyInfo Mark_t1948____Length_PropertyInfo = 
{
	&Mark_t1948_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &Mark_get_Length_m9014_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mark_t1948_PropertyInfos[] =
{
	&Mark_t1948____IsDefined_PropertyInfo,
	&Mark_t1948____Index_PropertyInfo,
	&Mark_t1948____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Mark_t1948_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Mark_t1948_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mark_t1948_0_0_0;
extern const Il2CppType Mark_t1948_1_0_0;
const Il2CppTypeDefinitionMetadata Mark_t1948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Mark_t1948_VTable/* vtableMethods */
	, Mark_t1948_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 668/* fieldStart */

};
TypeInfo Mark_t1948_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mark"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Mark_t1948_MethodInfos/* methods */
	, Mark_t1948_PropertyInfos/* properties */
	, NULL/* events */
	, &Mark_t1948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mark_t1948_0_0_0/* byval_arg */
	, &Mark_t1948_1_0_0/* this_arg */
	, &Mark_t1948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mark_t1948)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mark_t1948)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mark_t1948 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern TypeInfo IntStack_t1949_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStackMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::Pop()
extern const MethodInfo IntStack_Pop_m9015_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&IntStack_Pop_m9015/* method */
	, &IntStack_t1949_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntStack_t1949_IntStack_Push_m9016_ParameterInfos[] = 
{
	{"value", 0, 134218445, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::Push(System.Int32)
extern const MethodInfo IntStack_Push_m9016_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&IntStack_Push_m9016/* method */
	, &IntStack_t1949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, IntStack_t1949_IntStack_Push_m9016_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::get_Count()
extern const MethodInfo IntStack_get_Count_m9017_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntStack_get_Count_m9017/* method */
	, &IntStack_t1949_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntStack_t1949_IntStack_set_Count_m9018_ParameterInfos[] = 
{
	{"value", 0, 134218446, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::set_Count(System.Int32)
extern const MethodInfo IntStack_set_Count_m9018_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&IntStack_set_Count_m9018/* method */
	, &IntStack_t1949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, IntStack_t1949_IntStack_set_Count_m9018_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntStack_t1949_MethodInfos[] =
{
	&IntStack_Pop_m9015_MethodInfo,
	&IntStack_Push_m9016_MethodInfo,
	&IntStack_get_Count_m9017_MethodInfo,
	&IntStack_set_Count_m9018_MethodInfo,
	NULL
};
extern const MethodInfo IntStack_get_Count_m9017_MethodInfo;
extern const MethodInfo IntStack_set_Count_m9018_MethodInfo;
static const PropertyInfo IntStack_t1949____Count_PropertyInfo = 
{
	&IntStack_t1949_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntStack_get_Count_m9017_MethodInfo/* get */
	, &IntStack_set_Count_m9018_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntStack_t1949_PropertyInfos[] =
{
	&IntStack_t1949____Count_PropertyInfo,
	NULL
};
static const Il2CppMethodReference IntStack_t1949_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool IntStack_t1949_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntStack_t1949_0_0_0;
extern const Il2CppType IntStack_t1949_1_0_0;
extern TypeInfo Interpreter_t1954_il2cpp_TypeInfo;
extern const Il2CppType Interpreter_t1954_0_0_0;
const Il2CppTypeDefinitionMetadata IntStack_t1949_DefinitionMetadata = 
{
	&Interpreter_t1954_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, IntStack_t1949_VTable/* vtableMethods */
	, IntStack_t1949_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 671/* fieldStart */

};
TypeInfo IntStack_t1949_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntStack"/* name */
	, ""/* namespaze */
	, IntStack_t1949_MethodInfos/* methods */
	, IntStack_t1949_PropertyInfos/* properties */
	, NULL/* events */
	, &IntStack_t1949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntStack_t1949_0_0_0/* byval_arg */
	, &IntStack_t1949_1_0_0/* this_arg */
	, &IntStack_t1949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)IntStack_t1949_marshal/* marshal_to_native_func */
	, (methodPointerType)IntStack_t1949_marshal_back/* marshal_from_native_func */
	, (methodPointerType)IntStack_t1949_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (IntStack_t1949)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntStack_t1949)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntStack_t1949_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern TypeInfo RepeatContext_t1950_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatContMethodDeclarations.h"
extern const Il2CppType RepeatContext_t1950_0_0_0;
extern const Il2CppType RepeatContext_t1950_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo RepeatContext_t1950_RepeatContext__ctor_m9019_ParameterInfos[] = 
{
	{"previous", 0, 134218447, 0, &RepeatContext_t1950_0_0_0},
	{"min", 1, 134218448, 0, &Int32_t127_0_0_0},
	{"max", 2, 134218449, 0, &Int32_t127_0_0_0},
	{"lazy", 3, 134218450, 0, &Boolean_t169_0_0_0},
	{"expr_pc", 4, 134218451, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::.ctor(System.Text.RegularExpressions.Interpreter/RepeatContext,System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo RepeatContext__ctor_m9019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RepeatContext__ctor_m9019/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_SByte_t170_Int32_t127/* invoker_method */
	, RepeatContext_t1950_RepeatContext__ctor_m9019_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Count()
extern const MethodInfo RepeatContext_get_Count_m9020_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&RepeatContext_get_Count_m9020/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo RepeatContext_t1950_RepeatContext_set_Count_m9021_ParameterInfos[] = 
{
	{"value", 0, 134218452, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Count(System.Int32)
extern const MethodInfo RepeatContext_set_Count_m9021_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&RepeatContext_set_Count_m9021/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, RepeatContext_t1950_RepeatContext_set_Count_m9021_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Start()
extern const MethodInfo RepeatContext_get_Start_m9022_MethodInfo = 
{
	"get_Start"/* name */
	, (methodPointerType)&RepeatContext_get_Start_m9022/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo RepeatContext_t1950_RepeatContext_set_Start_m9023_ParameterInfos[] = 
{
	{"value", 0, 134218453, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Start(System.Int32)
extern const MethodInfo RepeatContext_set_Start_m9023_MethodInfo = 
{
	"set_Start"/* name */
	, (methodPointerType)&RepeatContext_set_Start_m9023/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, RepeatContext_t1950_RepeatContext_set_Start_m9023_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMinimum()
extern const MethodInfo RepeatContext_get_IsMinimum_m9024_MethodInfo = 
{
	"get_IsMinimum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMinimum_m9024/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMaximum()
extern const MethodInfo RepeatContext_get_IsMaximum_m9025_MethodInfo = 
{
	"get_IsMaximum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMaximum_m9025/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsLazy()
extern const MethodInfo RepeatContext_get_IsLazy_m9026_MethodInfo = 
{
	"get_IsLazy"/* name */
	, (methodPointerType)&RepeatContext_get_IsLazy_m9026/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Expression()
extern const MethodInfo RepeatContext_get_Expression_m9027_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&RepeatContext_get_Expression_m9027/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter/RepeatContext::get_Previous()
extern const MethodInfo RepeatContext_get_Previous_m9028_MethodInfo = 
{
	"get_Previous"/* name */
	, (methodPointerType)&RepeatContext_get_Previous_m9028/* method */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* declaring_type */
	, &RepeatContext_t1950_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RepeatContext_t1950_MethodInfos[] =
{
	&RepeatContext__ctor_m9019_MethodInfo,
	&RepeatContext_get_Count_m9020_MethodInfo,
	&RepeatContext_set_Count_m9021_MethodInfo,
	&RepeatContext_get_Start_m9022_MethodInfo,
	&RepeatContext_set_Start_m9023_MethodInfo,
	&RepeatContext_get_IsMinimum_m9024_MethodInfo,
	&RepeatContext_get_IsMaximum_m9025_MethodInfo,
	&RepeatContext_get_IsLazy_m9026_MethodInfo,
	&RepeatContext_get_Expression_m9027_MethodInfo,
	&RepeatContext_get_Previous_m9028_MethodInfo,
	NULL
};
extern const MethodInfo RepeatContext_get_Count_m9020_MethodInfo;
extern const MethodInfo RepeatContext_set_Count_m9021_MethodInfo;
static const PropertyInfo RepeatContext_t1950____Count_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &RepeatContext_get_Count_m9020_MethodInfo/* get */
	, &RepeatContext_set_Count_m9021_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Start_m9022_MethodInfo;
extern const MethodInfo RepeatContext_set_Start_m9023_MethodInfo;
static const PropertyInfo RepeatContext_t1950____Start_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "Start"/* name */
	, &RepeatContext_get_Start_m9022_MethodInfo/* get */
	, &RepeatContext_set_Start_m9023_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMinimum_m9024_MethodInfo;
static const PropertyInfo RepeatContext_t1950____IsMinimum_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "IsMinimum"/* name */
	, &RepeatContext_get_IsMinimum_m9024_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMaximum_m9025_MethodInfo;
static const PropertyInfo RepeatContext_t1950____IsMaximum_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "IsMaximum"/* name */
	, &RepeatContext_get_IsMaximum_m9025_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsLazy_m9026_MethodInfo;
static const PropertyInfo RepeatContext_t1950____IsLazy_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "IsLazy"/* name */
	, &RepeatContext_get_IsLazy_m9026_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Expression_m9027_MethodInfo;
static const PropertyInfo RepeatContext_t1950____Expression_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &RepeatContext_get_Expression_m9027_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Previous_m9028_MethodInfo;
static const PropertyInfo RepeatContext_t1950____Previous_PropertyInfo = 
{
	&RepeatContext_t1950_il2cpp_TypeInfo/* parent */
	, "Previous"/* name */
	, &RepeatContext_get_Previous_m9028_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RepeatContext_t1950_PropertyInfos[] =
{
	&RepeatContext_t1950____Count_PropertyInfo,
	&RepeatContext_t1950____Start_PropertyInfo,
	&RepeatContext_t1950____IsMinimum_PropertyInfo,
	&RepeatContext_t1950____IsMaximum_PropertyInfo,
	&RepeatContext_t1950____IsLazy_PropertyInfo,
	&RepeatContext_t1950____Expression_PropertyInfo,
	&RepeatContext_t1950____Previous_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RepeatContext_t1950_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RepeatContext_t1950_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RepeatContext_t1950_1_0_0;
struct RepeatContext_t1950;
const Il2CppTypeDefinitionMetadata RepeatContext_t1950_DefinitionMetadata = 
{
	&Interpreter_t1954_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RepeatContext_t1950_VTable/* vtableMethods */
	, RepeatContext_t1950_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 673/* fieldStart */

};
TypeInfo RepeatContext_t1950_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RepeatContext"/* name */
	, ""/* namespaze */
	, RepeatContext_t1950_MethodInfos/* methods */
	, RepeatContext_t1950_PropertyInfos/* properties */
	, NULL/* events */
	, &RepeatContext_t1950_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RepeatContext_t1950_0_0_0/* byval_arg */
	, &RepeatContext_t1950_1_0_0/* this_arg */
	, &RepeatContext_t1950_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RepeatContext_t1950)/* instance_size */
	, sizeof (RepeatContext_t1950)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern TypeInfo Mode_t1951_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_ModeMethodDeclarations.h"
static const MethodInfo* Mode_t1951_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Mode_t1951_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Mode_t1951_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t1951_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mode_t1951_0_0_0;
extern const Il2CppType Mode_t1951_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Mode_t1951_DefinitionMetadata = 
{
	&Interpreter_t1954_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t1951_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Mode_t1951_VTable/* vtableMethods */
	, Mode_t1951_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 680/* fieldStart */

};
TypeInfo Mode_t1951_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t1951_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t1951_0_0_0/* byval_arg */
	, &Mode_t1951_1_0_0/* this_arg */
	, &Mode_t1951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t1951)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t1951)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_InterpreterMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1060_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter__ctor_m9029_ParameterInfos[] = 
{
	{"program", 0, 134218408, 0, &UInt16U5BU5D_t1060_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::.ctor(System.UInt16[])
extern const MethodInfo Interpreter__ctor_m9029_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interpreter__ctor_m9029/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Interpreter_t1954_Interpreter__ctor_m9029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_ReadProgramCount_m9030_ParameterInfos[] = 
{
	{"ptr", 0, 134218409, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::ReadProgramCount(System.Int32)
extern const MethodInfo Interpreter_ReadProgramCount_m9030_MethodInfo = 
{
	"ReadProgramCount"/* name */
	, (methodPointerType)&Interpreter_ReadProgramCount_m9030/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_ReadProgramCount_m9030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Scan_m9031_ParameterInfos[] = 
{
	{"regex", 0, 134218410, 0, &Regex_t822_0_0_0},
	{"text", 1, 134218411, 0, &String_t_0_0_0},
	{"start", 2, 134218412, 0, &Int32_t127_0_0_0},
	{"end", 3, 134218413, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern const MethodInfo Interpreter_Scan_m9031_MethodInfo = 
{
	"Scan"/* name */
	, (methodPointerType)&Interpreter_Scan_m9031/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Match_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Scan_m9031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Reset()
extern const MethodInfo Interpreter_Reset_m9032_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Interpreter_Reset_m9032/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1951_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Eval_m9033_ParameterInfos[] = 
{
	{"mode", 0, 134218414, 0, &Mode_t1951_0_0_0},
	{"ref_ptr", 1, 134218415, 0, &Int32_t127_1_0_0},
	{"pc", 2, 134218416, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Eval(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32)
extern const MethodInfo Interpreter_Eval_m9033_MethodInfo = 
{
	"Eval"/* name */
	, (methodPointerType)&Interpreter_Eval_m9033/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Eval_m9033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1951_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_EvalChar_m9034_ParameterInfos[] = 
{
	{"mode", 0, 134218417, 0, &Mode_t1951_0_0_0},
	{"ptr", 1, 134218418, 0, &Int32_t127_1_0_0},
	{"pc", 2, 134218419, 0, &Int32_t127_1_0_0},
	{"multi", 3, 134218420, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535_Int32U26_t535_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::EvalChar(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32&,System.Boolean)
extern const MethodInfo Interpreter_EvalChar_m9034_MethodInfo = 
{
	"EvalChar"/* name */
	, (methodPointerType)&Interpreter_EvalChar_m9034/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127_Int32U26_t535_Int32U26_t535_SByte_t170/* invoker_method */
	, Interpreter_t1954_Interpreter_EvalChar_m9034_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_TryMatch_m9035_ParameterInfos[] = 
{
	{"ref_ptr", 0, 134218421, 0, &Int32_t127_1_0_0},
	{"pc", 1, 134218422, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::TryMatch(System.Int32&,System.Int32)
extern const MethodInfo Interpreter_TryMatch_m9035_MethodInfo = 
{
	"TryMatch"/* name */
	, (methodPointerType)&Interpreter_TryMatch_m9035/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_TryMatch_m9035_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1936_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_IsPosition_m9036_ParameterInfos[] = 
{
	{"pos", 0, 134218423, 0, &Position_t1936_0_0_0},
	{"ptr", 1, 134218424, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_UInt16_t454_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsPosition(System.Text.RegularExpressions.Position,System.Int32)
extern const MethodInfo Interpreter_IsPosition_m9036_MethodInfo = 
{
	"IsPosition"/* name */
	, (methodPointerType)&Interpreter_IsPosition_m9036/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_UInt16_t454_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_IsPosition_m9036_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_IsWordChar_m9037_ParameterInfos[] = 
{
	{"c", 0, 134218425, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsWordChar(System.Char)
extern const MethodInfo Interpreter_IsWordChar_m9037_MethodInfo = 
{
	"IsWordChar"/* name */
	, (methodPointerType)&Interpreter_IsWordChar_m9037/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int16_t534/* invoker_method */
	, Interpreter_t1954_Interpreter_IsWordChar_m9037_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_GetString_m9038_ParameterInfos[] = 
{
	{"pc", 0, 134218426, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Interpreter::GetString(System.Int32)
extern const MethodInfo Interpreter_GetString_m9038_MethodInfo = 
{
	"GetString"/* name */
	, (methodPointerType)&Interpreter_GetString_m9038/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_GetString_m9038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Open_m9039_ParameterInfos[] = 
{
	{"gid", 0, 134218427, 0, &Int32_t127_0_0_0},
	{"ptr", 1, 134218428, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Open(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Open_m9039_MethodInfo = 
{
	"Open"/* name */
	, (methodPointerType)&Interpreter_Open_m9039/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Open_m9039_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Close_m9040_ParameterInfos[] = 
{
	{"gid", 0, 134218429, 0, &Int32_t127_0_0_0},
	{"ptr", 1, 134218430, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Close(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Close_m9040_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&Interpreter_Close_m9040/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Close_m9040_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Balance_m9041_ParameterInfos[] = 
{
	{"gid", 0, 134218431, 0, &Int32_t127_0_0_0},
	{"balance_gid", 1, 134218432, 0, &Int32_t127_0_0_0},
	{"capture", 2, 134218433, 0, &Boolean_t169_0_0_0},
	{"ptr", 3, 134218434, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Balance(System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo Interpreter_Balance_m9041_MethodInfo = 
{
	"Balance"/* name */
	, (methodPointerType)&Interpreter_Balance_m9041/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127_Int32_t127_SByte_t170_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Balance_m9041_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::Checkpoint()
extern const MethodInfo Interpreter_Checkpoint_m9042_MethodInfo = 
{
	"Checkpoint"/* name */
	, (methodPointerType)&Interpreter_Checkpoint_m9042/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_Backtrack_m9043_ParameterInfos[] = 
{
	{"cp", 0, 134218435, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Backtrack(System.Int32)
extern const MethodInfo Interpreter_Backtrack_m9043_MethodInfo = 
{
	"Backtrack"/* name */
	, (methodPointerType)&Interpreter_Backtrack_m9043/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_Backtrack_m9043_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::ResetGroups()
extern const MethodInfo Interpreter_ResetGroups_m9044_MethodInfo = 
{
	"ResetGroups"/* name */
	, (methodPointerType)&Interpreter_ResetGroups_m9044/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_GetLastDefined_m9045_ParameterInfos[] = 
{
	{"gid", 0, 134218436, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::GetLastDefined(System.Int32)
extern const MethodInfo Interpreter_GetLastDefined_m9045_MethodInfo = 
{
	"GetLastDefined"/* name */
	, (methodPointerType)&Interpreter_GetLastDefined_m9045/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_GetLastDefined_m9045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_CreateMark_m9046_ParameterInfos[] = 
{
	{"previous", 0, 134218437, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::CreateMark(System.Int32)
extern const MethodInfo Interpreter_CreateMark_m9046_MethodInfo = 
{
	"CreateMark"/* name */
	, (methodPointerType)&Interpreter_CreateMark_m9046/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_CreateMark_m9046_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Interpreter_t1954_Interpreter_GetGroupInfo_m9047_ParameterInfos[] = 
{
	{"gid", 0, 134218438, 0, &Int32_t127_0_0_0},
	{"first_mark_index", 1, 134218439, 0, &Int32_t127_1_0_2},
	{"n_caps", 2, 134218440, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::GetGroupInfo(System.Int32,System.Int32&,System.Int32&)
extern const MethodInfo Interpreter_GetGroupInfo_m9047_MethodInfo = 
{
	"GetGroupInfo"/* name */
	, (methodPointerType)&Interpreter_GetGroupInfo_m9047/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Interpreter_t1954_Interpreter_GetGroupInfo_m9047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1063_0_0_0;
extern const Il2CppType Group_t1063_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_PopulateGroup_m9048_ParameterInfos[] = 
{
	{"g", 0, 134218441, 0, &Group_t1063_0_0_0},
	{"first_mark_index", 1, 134218442, 0, &Int32_t127_0_0_0},
	{"n_caps", 2, 134218443, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::PopulateGroup(System.Text.RegularExpressions.Group,System.Int32,System.Int32)
extern const MethodInfo Interpreter_PopulateGroup_m9048_MethodInfo = 
{
	"PopulateGroup"/* name */
	, (methodPointerType)&Interpreter_PopulateGroup_m9048/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, Interpreter_t1954_Interpreter_PopulateGroup_m9048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t822_0_0_0;
static const ParameterInfo Interpreter_t1954_Interpreter_GenerateMatch_m9049_ParameterInfos[] = 
{
	{"regex", 0, 134218444, 0, &Regex_t822_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::GenerateMatch(System.Text.RegularExpressions.Regex)
extern const MethodInfo Interpreter_GenerateMatch_m9049_MethodInfo = 
{
	"GenerateMatch"/* name */
	, (methodPointerType)&Interpreter_GenerateMatch_m9049/* method */
	, &Interpreter_t1954_il2cpp_TypeInfo/* declaring_type */
	, &Match_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Interpreter_t1954_Interpreter_GenerateMatch_m9049_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interpreter_t1954_MethodInfos[] =
{
	&Interpreter__ctor_m9029_MethodInfo,
	&Interpreter_ReadProgramCount_m9030_MethodInfo,
	&Interpreter_Scan_m9031_MethodInfo,
	&Interpreter_Reset_m9032_MethodInfo,
	&Interpreter_Eval_m9033_MethodInfo,
	&Interpreter_EvalChar_m9034_MethodInfo,
	&Interpreter_TryMatch_m9035_MethodInfo,
	&Interpreter_IsPosition_m9036_MethodInfo,
	&Interpreter_IsWordChar_m9037_MethodInfo,
	&Interpreter_GetString_m9038_MethodInfo,
	&Interpreter_Open_m9039_MethodInfo,
	&Interpreter_Close_m9040_MethodInfo,
	&Interpreter_Balance_m9041_MethodInfo,
	&Interpreter_Checkpoint_m9042_MethodInfo,
	&Interpreter_Backtrack_m9043_MethodInfo,
	&Interpreter_ResetGroups_m9044_MethodInfo,
	&Interpreter_GetLastDefined_m9045_MethodInfo,
	&Interpreter_CreateMark_m9046_MethodInfo,
	&Interpreter_GetGroupInfo_m9047_MethodInfo,
	&Interpreter_PopulateGroup_m9048_MethodInfo,
	&Interpreter_GenerateMatch_m9049_MethodInfo,
	NULL
};
static const Il2CppType* Interpreter_t1954_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IntStack_t1949_0_0_0,
	&RepeatContext_t1950_0_0_0,
	&Mode_t1951_0_0_0,
};
extern const MethodInfo Interpreter_Scan_m9031_MethodInfo;
extern const MethodInfo BaseMachine_Split_m8857_MethodInfo;
extern const MethodInfo BaseMachine_Replace_m8856_MethodInfo;
static const Il2CppMethodReference Interpreter_t1954_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Interpreter_Scan_m9031_MethodInfo,
	&BaseMachine_Split_m8857_MethodInfo,
	&BaseMachine_Replace_m8856_MethodInfo,
	&BaseMachine_Replace_m8856_MethodInfo,
	&BaseMachine_Split_m8857_MethodInfo,
	&Interpreter_Scan_m9031_MethodInfo,
};
static bool Interpreter_t1954_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Interpreter_t1954_InterfacesOffsets[] = 
{
	{ &IMachine_t1927_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interpreter_t1954_1_0_0;
extern const Il2CppType BaseMachine_t1922_0_0_0;
struct Interpreter_t1954;
const Il2CppTypeDefinitionMetadata Interpreter_t1954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Interpreter_t1954_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Interpreter_t1954_InterfacesOffsets/* interfaceOffsets */
	, &BaseMachine_t1922_0_0_0/* parent */
	, Interpreter_t1954_VTable/* vtableMethods */
	, Interpreter_t1954_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 684/* fieldStart */

};
TypeInfo Interpreter_t1954_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interpreter"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interpreter_t1954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Interpreter_t1954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interpreter_t1954_0_0_0/* byval_arg */
	, &Interpreter_t1954_1_0_0/* this_arg */
	, &Interpreter_t1954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interpreter_t1954)/* instance_size */
	, sizeof (Interpreter_t1954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// Metadata Definition System.Text.RegularExpressions.Interval
extern TypeInfo Interval_t1955_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interval_t1955_Interval__ctor_m9050_ParameterInfos[] = 
{
	{"low", 0, 134218454, 0, &Int32_t127_0_0_0},
	{"high", 1, 134218455, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::.ctor(System.Int32,System.Int32)
extern const MethodInfo Interval__ctor_m9050_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interval__ctor_m9050/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, Interval_t1955_Interval__ctor_m9050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
extern void* RuntimeInvoker_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Interval::get_Empty()
extern const MethodInfo Interval_get_Empty_m9051_MethodInfo = 
{
	"get_Empty"/* name */
	, (methodPointerType)&Interval_get_Empty_m9051/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1955_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1955/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsDiscontiguous()
extern const MethodInfo Interval_get_IsDiscontiguous_m9052_MethodInfo = 
{
	"get_IsDiscontiguous"/* name */
	, (methodPointerType)&Interval_get_IsDiscontiguous_m9052/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsSingleton()
extern const MethodInfo Interval_get_IsSingleton_m9053_MethodInfo = 
{
	"get_IsSingleton"/* name */
	, (methodPointerType)&Interval_get_IsSingleton_m9053/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsEmpty()
extern const MethodInfo Interval_get_IsEmpty_m9054_MethodInfo = 
{
	"get_IsEmpty"/* name */
	, (methodPointerType)&Interval_get_IsEmpty_m9054/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::get_Size()
extern const MethodInfo Interval_get_Size_m9055_MethodInfo = 
{
	"get_Size"/* name */
	, (methodPointerType)&Interval_get_Size_m9055/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo Interval_t1955_Interval_IsDisjoint_m9056_ParameterInfos[] = 
{
	{"i", 0, 134218456, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsDisjoint(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsDisjoint_m9056_MethodInfo = 
{
	"IsDisjoint"/* name */
	, (methodPointerType)&Interval_IsDisjoint_m9056/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Interval_t1955/* invoker_method */
	, Interval_t1955_Interval_IsDisjoint_m9056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo Interval_t1955_Interval_IsAdjacent_m9057_ParameterInfos[] = 
{
	{"i", 0, 134218457, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsAdjacent(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsAdjacent_m9057_MethodInfo = 
{
	"IsAdjacent"/* name */
	, (methodPointerType)&Interval_IsAdjacent_m9057/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Interval_t1955/* invoker_method */
	, Interval_t1955_Interval_IsAdjacent_m9057_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo Interval_t1955_Interval_Contains_m9058_ParameterInfos[] = 
{
	{"i", 0, 134218458, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Contains_m9058_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m9058/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Interval_t1955/* invoker_method */
	, Interval_t1955_Interval_Contains_m9058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Interval_t1955_Interval_Contains_m9059_ParameterInfos[] = 
{
	{"i", 0, 134218459, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Int32)
extern const MethodInfo Interval_Contains_m9059_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m9059/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Interval_t1955_Interval_Contains_m9059_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo Interval_t1955_Interval_Intersects_m9060_ParameterInfos[] = 
{
	{"i", 0, 134218460, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Intersects(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Intersects_m9060_MethodInfo = 
{
	"Intersects"/* name */
	, (methodPointerType)&Interval_Intersects_m9060/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Interval_t1955/* invoker_method */
	, Interval_t1955_Interval_Intersects_m9060_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo Interval_t1955_Interval_Merge_m9061_ParameterInfos[] = 
{
	{"i", 0, 134218461, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::Merge(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Merge_m9061_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Interval_Merge_m9061/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Interval_t1955/* invoker_method */
	, Interval_t1955_Interval_Merge_m9061_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Interval_t1955_Interval_CompareTo_m9062_ParameterInfos[] = 
{
	{"o", 0, 134218462, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::CompareTo(System.Object)
extern const MethodInfo Interval_CompareTo_m9062_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Interval_CompareTo_m9062/* method */
	, &Interval_t1955_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Interval_t1955_Interval_CompareTo_m9062_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interval_t1955_MethodInfos[] =
{
	&Interval__ctor_m9050_MethodInfo,
	&Interval_get_Empty_m9051_MethodInfo,
	&Interval_get_IsDiscontiguous_m9052_MethodInfo,
	&Interval_get_IsSingleton_m9053_MethodInfo,
	&Interval_get_IsEmpty_m9054_MethodInfo,
	&Interval_get_Size_m9055_MethodInfo,
	&Interval_IsDisjoint_m9056_MethodInfo,
	&Interval_IsAdjacent_m9057_MethodInfo,
	&Interval_Contains_m9058_MethodInfo,
	&Interval_Contains_m9059_MethodInfo,
	&Interval_Intersects_m9060_MethodInfo,
	&Interval_Merge_m9061_MethodInfo,
	&Interval_CompareTo_m9062_MethodInfo,
	NULL
};
extern const MethodInfo Interval_get_Empty_m9051_MethodInfo;
static const PropertyInfo Interval_t1955____Empty_PropertyInfo = 
{
	&Interval_t1955_il2cpp_TypeInfo/* parent */
	, "Empty"/* name */
	, &Interval_get_Empty_m9051_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsDiscontiguous_m9052_MethodInfo;
static const PropertyInfo Interval_t1955____IsDiscontiguous_PropertyInfo = 
{
	&Interval_t1955_il2cpp_TypeInfo/* parent */
	, "IsDiscontiguous"/* name */
	, &Interval_get_IsDiscontiguous_m9052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsSingleton_m9053_MethodInfo;
static const PropertyInfo Interval_t1955____IsSingleton_PropertyInfo = 
{
	&Interval_t1955_il2cpp_TypeInfo/* parent */
	, "IsSingleton"/* name */
	, &Interval_get_IsSingleton_m9053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsEmpty_m9054_MethodInfo;
static const PropertyInfo Interval_t1955____IsEmpty_PropertyInfo = 
{
	&Interval_t1955_il2cpp_TypeInfo/* parent */
	, "IsEmpty"/* name */
	, &Interval_get_IsEmpty_m9054_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_Size_m9055_MethodInfo;
static const PropertyInfo Interval_t1955____Size_PropertyInfo = 
{
	&Interval_t1955_il2cpp_TypeInfo/* parent */
	, "Size"/* name */
	, &Interval_get_Size_m9055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Interval_t1955_PropertyInfos[] =
{
	&Interval_t1955____Empty_PropertyInfo,
	&Interval_t1955____IsDiscontiguous_PropertyInfo,
	&Interval_t1955____IsSingleton_PropertyInfo,
	&Interval_t1955____IsEmpty_PropertyInfo,
	&Interval_t1955____Size_PropertyInfo,
	NULL
};
extern const MethodInfo Interval_CompareTo_m9062_MethodInfo;
static const Il2CppMethodReference Interval_t1955_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
	&Interval_CompareTo_m9062_MethodInfo,
};
static bool Interval_t1955_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Interval_t1955_InterfacesTypeInfos[] = 
{
	&IComparable_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair Interval_t1955_InterfacesOffsets[] = 
{
	{ &IComparable_t166_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interval_t1955_1_0_0;
const Il2CppTypeDefinitionMetadata Interval_t1955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Interval_t1955_InterfacesTypeInfos/* implementedInterfaces */
	, Interval_t1955_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Interval_t1955_VTable/* vtableMethods */
	, Interval_t1955_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 700/* fieldStart */

};
TypeInfo Interval_t1955_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interval"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interval_t1955_MethodInfos/* methods */
	, Interval_t1955_PropertyInfos/* properties */
	, NULL/* events */
	, &Interval_t1955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interval_t1955_0_0_0/* byval_arg */
	, &Interval_t1955_1_0_0/* this_arg */
	, &Interval_t1955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Interval_t1955_marshal/* marshal_to_native_func */
	, (methodPointerType)Interval_t1955_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Interval_t1955_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Interval_t1955)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Interval_t1955)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Interval_t1955_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern TypeInfo Enumerator_t1956_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_EnuMethodDeclarations.h"
extern const Il2CppType IList_t1514_0_0_0;
extern const Il2CppType IList_t1514_0_0_0;
static const ParameterInfo Enumerator_t1956_Enumerator__ctor_m9063_ParameterInfos[] = 
{
	{"list", 0, 134218472, 0, &IList_t1514_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
extern const MethodInfo Enumerator__ctor_m9063_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m9063/* method */
	, &Enumerator_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Enumerator_t1956_Enumerator__ctor_m9063_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
extern const MethodInfo Enumerator_get_Current_m9064_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m9064/* method */
	, &Enumerator_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
extern const MethodInfo Enumerator_MoveNext_m9065_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m9065/* method */
	, &Enumerator_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
extern const MethodInfo Enumerator_Reset_m9066_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Enumerator_Reset_m9066/* method */
	, &Enumerator_t1956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Enumerator_t1956_MethodInfos[] =
{
	&Enumerator__ctor_m9063_MethodInfo,
	&Enumerator_get_Current_m9064_MethodInfo,
	&Enumerator_MoveNext_m9065_MethodInfo,
	&Enumerator_Reset_m9066_MethodInfo,
	NULL
};
extern const MethodInfo Enumerator_get_Current_m9064_MethodInfo;
static const PropertyInfo Enumerator_t1956____Current_PropertyInfo = 
{
	&Enumerator_t1956_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m9064_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Enumerator_t1956_PropertyInfos[] =
{
	&Enumerator_t1956____Current_PropertyInfo,
	NULL
};
extern const MethodInfo Enumerator_MoveNext_m9065_MethodInfo;
extern const MethodInfo Enumerator_Reset_m9066_MethodInfo;
static const Il2CppMethodReference Enumerator_t1956_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Enumerator_get_Current_m9064_MethodInfo,
	&Enumerator_MoveNext_m9065_MethodInfo,
	&Enumerator_Reset_m9066_MethodInfo,
};
static bool Enumerator_t1956_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t410_0_0_0;
static const Il2CppType* Enumerator_t1956_InterfacesTypeInfos[] = 
{
	&IEnumerator_t410_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1956_InterfacesOffsets[] = 
{
	{ &IEnumerator_t410_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t1956_0_0_0;
extern const Il2CppType Enumerator_t1956_1_0_0;
extern TypeInfo IntervalCollection_t1958_il2cpp_TypeInfo;
extern const Il2CppType IntervalCollection_t1958_0_0_0;
struct Enumerator_t1956;
const Il2CppTypeDefinitionMetadata Enumerator_t1956_DefinitionMetadata = 
{
	&IntervalCollection_t1958_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1956_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1956_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1956_VTable/* vtableMethods */
	, Enumerator_t1956_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 703/* fieldStart */

};
TypeInfo Enumerator_t1956_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1956_MethodInfos/* methods */
	, Enumerator_t1956_PropertyInfos/* properties */
	, NULL/* events */
	, &Enumerator_t1956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1956_0_0_0/* byval_arg */
	, &Enumerator_t1956_1_0_0/* this_arg */
	, &Enumerator_t1956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1956)/* instance_size */
	, sizeof (Enumerator_t1956)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern TypeInfo CostDelegate_t1957_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CostDelegate_t1957_CostDelegate__ctor_m9067_ParameterInfos[] = 
{
	{"object", 0, 134218473, 0, &Object_t_0_0_0},
	{"method", 1, 134218474, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/CostDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CostDelegate__ctor_m9067_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CostDelegate__ctor_m9067/* method */
	, &CostDelegate_t1957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, CostDelegate_t1957_CostDelegate__ctor_m9067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo CostDelegate_t1957_CostDelegate_Invoke_m9068_ParameterInfos[] = 
{
	{"i", 0, 134218475, 0, &Interval_t1955_0_0_0},
};
extern const Il2CppType Double_t1407_0_0_0;
extern void* RuntimeInvoker_Double_t1407_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::Invoke(System.Text.RegularExpressions.Interval)
extern const MethodInfo CostDelegate_Invoke_m9068_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CostDelegate_Invoke_m9068/* method */
	, &CostDelegate_t1957_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Interval_t1955/* invoker_method */
	, CostDelegate_t1957_CostDelegate_Invoke_m9068_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CostDelegate_t1957_CostDelegate_BeginInvoke_m9069_ParameterInfos[] = 
{
	{"i", 0, 134218476, 0, &Interval_t1955_0_0_0},
	{"callback", 1, 134218477, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134218478, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Interval_t1955_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.IntervalCollection/CostDelegate::BeginInvoke(System.Text.RegularExpressions.Interval,System.AsyncCallback,System.Object)
extern const MethodInfo CostDelegate_BeginInvoke_m9069_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CostDelegate_BeginInvoke_m9069/* method */
	, &CostDelegate_t1957_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Interval_t1955_Object_t_Object_t/* invoker_method */
	, CostDelegate_t1957_CostDelegate_BeginInvoke_m9069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo CostDelegate_t1957_CostDelegate_EndInvoke_m9070_ParameterInfos[] = 
{
	{"result", 0, 134218479, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo CostDelegate_EndInvoke_m9070_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CostDelegate_EndInvoke_m9070/* method */
	, &CostDelegate_t1957_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Object_t/* invoker_method */
	, CostDelegate_t1957_CostDelegate_EndInvoke_m9070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CostDelegate_t1957_MethodInfos[] =
{
	&CostDelegate__ctor_m9067_MethodInfo,
	&CostDelegate_Invoke_m9068_MethodInfo,
	&CostDelegate_BeginInvoke_m9069_MethodInfo,
	&CostDelegate_EndInvoke_m9070_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo CostDelegate_Invoke_m9068_MethodInfo;
extern const MethodInfo CostDelegate_BeginInvoke_m9069_MethodInfo;
extern const MethodInfo CostDelegate_EndInvoke_m9070_MethodInfo;
static const Il2CppMethodReference CostDelegate_t1957_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&CostDelegate_Invoke_m9068_MethodInfo,
	&CostDelegate_BeginInvoke_m9069_MethodInfo,
	&CostDelegate_EndInvoke_m9070_MethodInfo,
};
static bool CostDelegate_t1957_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
static Il2CppInterfaceOffsetPair CostDelegate_t1957_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CostDelegate_t1957_0_0_0;
extern const Il2CppType CostDelegate_t1957_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct CostDelegate_t1957;
const Il2CppTypeDefinitionMetadata CostDelegate_t1957_DefinitionMetadata = 
{
	&IntervalCollection_t1958_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CostDelegate_t1957_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, CostDelegate_t1957_VTable/* vtableMethods */
	, CostDelegate_t1957_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CostDelegate_t1957_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CostDelegate"/* name */
	, ""/* namespaze */
	, CostDelegate_t1957_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CostDelegate_t1957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CostDelegate_t1957_0_0_0/* byval_arg */
	, &CostDelegate_t1957_1_0_0/* this_arg */
	, &CostDelegate_t1957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CostDelegate_t1957/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CostDelegate_t1957)/* instance_size */
	, sizeof (CostDelegate_t1957)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::.ctor()
extern const MethodInfo IntervalCollection__ctor_m9071_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntervalCollection__ctor_m9071/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntervalCollection_t1958_IntervalCollection_get_Item_m9072_ParameterInfos[] = 
{
	{"i", 0, 134218463, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1955_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.IntervalCollection::get_Item(System.Int32)
extern const MethodInfo IntervalCollection_get_Item_m9072_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&IntervalCollection_get_Item_m9072/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1955_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1955_Int32_t127/* invoker_method */
	, IntervalCollection_t1958_IntervalCollection_get_Item_m9072_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo IntervalCollection_t1958_IntervalCollection_Add_m9073_ParameterInfos[] = 
{
	{"i", 0, 134218464, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Add(System.Text.RegularExpressions.Interval)
extern const MethodInfo IntervalCollection_Add_m9073_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&IntervalCollection_Add_m9073/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Interval_t1955/* invoker_method */
	, IntervalCollection_t1958_IntervalCollection_Add_m9073_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Normalize()
extern const MethodInfo IntervalCollection_Normalize_m9074_MethodInfo = 
{
	"Normalize"/* name */
	, (methodPointerType)&IntervalCollection_Normalize_m9074/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CostDelegate_t1957_0_0_0;
static const ParameterInfo IntervalCollection_t1958_IntervalCollection_GetMetaCollection_m9075_ParameterInfos[] = 
{
	{"cost_del", 0, 134218465, 0, &CostDelegate_t1957_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IntervalCollection System.Text.RegularExpressions.IntervalCollection::GetMetaCollection(System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_GetMetaCollection_m9075_MethodInfo = 
{
	"GetMetaCollection"/* name */
	, (methodPointerType)&IntervalCollection_GetMetaCollection_m9075/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &IntervalCollection_t1958_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1958_IntervalCollection_GetMetaCollection_m9075_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IntervalCollection_t1958_0_0_0;
extern const Il2CppType CostDelegate_t1957_0_0_0;
static const ParameterInfo IntervalCollection_t1958_IntervalCollection_Optimize_m9076_ParameterInfos[] = 
{
	{"begin", 0, 134218466, 0, &Int32_t127_0_0_0},
	{"end", 1, 134218467, 0, &Int32_t127_0_0_0},
	{"meta", 2, 134218468, 0, &IntervalCollection_t1958_0_0_0},
	{"cost_del", 3, 134218469, 0, &CostDelegate_t1957_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Optimize(System.Int32,System.Int32,System.Text.RegularExpressions.IntervalCollection,System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_Optimize_m9076_MethodInfo = 
{
	"Optimize"/* name */
	, (methodPointerType)&IntervalCollection_Optimize_m9076/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1958_IntervalCollection_Optimize_m9076_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IntervalCollection::get_Count()
extern const MethodInfo IntervalCollection_get_Count_m9077_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntervalCollection_get_Count_m9077/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection::get_IsSynchronized()
extern const MethodInfo IntervalCollection_get_IsSynchronized_m9078_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&IntervalCollection_get_IsSynchronized_m9078/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection::get_SyncRoot()
extern const MethodInfo IntervalCollection_get_SyncRoot_m9079_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&IntervalCollection_get_SyncRoot_m9079/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IntervalCollection_t1958_IntervalCollection_CopyTo_m9080_ParameterInfos[] = 
{
	{"array", 0, 134218470, 0, &Array_t_0_0_0},
	{"index", 1, 134218471, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::CopyTo(System.Array,System.Int32)
extern const MethodInfo IntervalCollection_CopyTo_m9080_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&IntervalCollection_CopyTo_m9080/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, IntervalCollection_t1958_IntervalCollection_CopyTo_m9080_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator()
extern const MethodInfo IntervalCollection_GetEnumerator_m9081_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&IntervalCollection_GetEnumerator_m9081/* method */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntervalCollection_t1958_MethodInfos[] =
{
	&IntervalCollection__ctor_m9071_MethodInfo,
	&IntervalCollection_get_Item_m9072_MethodInfo,
	&IntervalCollection_Add_m9073_MethodInfo,
	&IntervalCollection_Normalize_m9074_MethodInfo,
	&IntervalCollection_GetMetaCollection_m9075_MethodInfo,
	&IntervalCollection_Optimize_m9076_MethodInfo,
	&IntervalCollection_get_Count_m9077_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m9078_MethodInfo,
	&IntervalCollection_get_SyncRoot_m9079_MethodInfo,
	&IntervalCollection_CopyTo_m9080_MethodInfo,
	&IntervalCollection_GetEnumerator_m9081_MethodInfo,
	NULL
};
extern const MethodInfo IntervalCollection_get_Item_m9072_MethodInfo;
static const PropertyInfo IntervalCollection_t1958____Item_PropertyInfo = 
{
	&IntervalCollection_t1958_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IntervalCollection_get_Item_m9072_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_Count_m9077_MethodInfo;
static const PropertyInfo IntervalCollection_t1958____Count_PropertyInfo = 
{
	&IntervalCollection_t1958_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntervalCollection_get_Count_m9077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_IsSynchronized_m9078_MethodInfo;
static const PropertyInfo IntervalCollection_t1958____IsSynchronized_PropertyInfo = 
{
	&IntervalCollection_t1958_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &IntervalCollection_get_IsSynchronized_m9078_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_SyncRoot_m9079_MethodInfo;
static const PropertyInfo IntervalCollection_t1958____SyncRoot_PropertyInfo = 
{
	&IntervalCollection_t1958_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &IntervalCollection_get_SyncRoot_m9079_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntervalCollection_t1958_PropertyInfos[] =
{
	&IntervalCollection_t1958____Item_PropertyInfo,
	&IntervalCollection_t1958____Count_PropertyInfo,
	&IntervalCollection_t1958____IsSynchronized_PropertyInfo,
	&IntervalCollection_t1958____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* IntervalCollection_t1958_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Enumerator_t1956_0_0_0,
	&CostDelegate_t1957_0_0_0,
};
extern const MethodInfo IntervalCollection_CopyTo_m9080_MethodInfo;
extern const MethodInfo IntervalCollection_GetEnumerator_m9081_MethodInfo;
static const Il2CppMethodReference IntervalCollection_t1958_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&IntervalCollection_get_Count_m9077_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m9078_MethodInfo,
	&IntervalCollection_get_SyncRoot_m9079_MethodInfo,
	&IntervalCollection_CopyTo_m9080_MethodInfo,
	&IntervalCollection_GetEnumerator_m9081_MethodInfo,
};
static bool IntervalCollection_t1958_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_t1513_0_0_0;
extern const Il2CppType IEnumerable_t550_0_0_0;
static const Il2CppType* IntervalCollection_t1958_InterfacesTypeInfos[] = 
{
	&ICollection_t1513_0_0_0,
	&IEnumerable_t550_0_0_0,
};
static Il2CppInterfaceOffsetPair IntervalCollection_t1958_InterfacesOffsets[] = 
{
	{ &ICollection_t1513_0_0_0, 4},
	{ &IEnumerable_t550_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntervalCollection_t1958_1_0_0;
struct IntervalCollection_t1958;
const Il2CppTypeDefinitionMetadata IntervalCollection_t1958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, IntervalCollection_t1958_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, IntervalCollection_t1958_InterfacesTypeInfos/* implementedInterfaces */
	, IntervalCollection_t1958_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntervalCollection_t1958_VTable/* vtableMethods */
	, IntervalCollection_t1958_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 705/* fieldStart */

};
TypeInfo IntervalCollection_t1958_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntervalCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IntervalCollection_t1958_MethodInfos/* methods */
	, IntervalCollection_t1958_PropertyInfos/* properties */
	, NULL/* events */
	, &IntervalCollection_t1958_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 64/* custom_attributes_cache */
	, &IntervalCollection_t1958_0_0_0/* byval_arg */
	, &IntervalCollection_t1958_1_0_0/* this_arg */
	, &IntervalCollection_t1958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntervalCollection_t1958)/* instance_size */
	, sizeof (IntervalCollection_t1958)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern TypeInfo Parser_t1959_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::.ctor()
extern const MethodInfo Parser__ctor_m9082_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Parser__ctor_m9082/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseDecimal_m9083_ParameterInfos[] = 
{
	{"str", 0, 134218480, 0, &String_t_0_0_0},
	{"ptr", 1, 134218481, 0, &Int32_t127_1_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDecimal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseDecimal_m9083_MethodInfo = 
{
	"ParseDecimal"/* name */
	, (methodPointerType)&Parser_ParseDecimal_m9083/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535/* invoker_method */
	, Parser_t1959_Parser_ParseDecimal_m9083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseOctal_m9084_ParameterInfos[] = 
{
	{"str", 0, 134218482, 0, &String_t_0_0_0},
	{"ptr", 1, 134218483, 0, &Int32_t127_1_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseOctal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseOctal_m9084_MethodInfo = 
{
	"ParseOctal"/* name */
	, (methodPointerType)&Parser_ParseOctal_m9084/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535/* invoker_method */
	, Parser_t1959_Parser_ParseOctal_m9084_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseHex_m9085_ParameterInfos[] = 
{
	{"str", 0, 134218484, 0, &String_t_0_0_0},
	{"ptr", 1, 134218485, 0, &Int32_t127_1_0_0},
	{"digits", 2, 134218486, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseHex(System.String,System.Int32&,System.Int32)
extern const MethodInfo Parser_ParseHex_m9085_MethodInfo = 
{
	"ParseHex"/* name */
	, (methodPointerType)&Parser_ParseHex_m9085/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseHex_m9085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseNumber_m9086_ParameterInfos[] = 
{
	{"str", 0, 134218487, 0, &String_t_0_0_0},
	{"ptr", 1, 134218488, 0, &Int32_t127_1_0_0},
	{"b", 2, 134218489, 0, &Int32_t127_0_0_0},
	{"min", 3, 134218490, 0, &Int32_t127_0_0_0},
	{"max", 4, 134218491, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.String,System.Int32&,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m9086_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m9086/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32U26_t535_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseNumber_m9086_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseName_m9087_ParameterInfos[] = 
{
	{"str", 0, 134218492, 0, &String_t_0_0_0},
	{"ptr", 1, 134218493, 0, &Int32_t127_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName(System.String,System.Int32&)
extern const MethodInfo Parser_ParseName_m9087_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m9087/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t535/* invoker_method */
	, Parser_t1959_Parser_ParseName_m9087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseRegularExpression_m9088_ParameterInfos[] = 
{
	{"pattern", 0, 134218494, 0, &String_t_0_0_0},
	{"options", 1, 134218495, 0, &RegexOptions_t1933_0_0_0},
};
extern const Il2CppType RegularExpression_t1965_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.RegularExpression System.Text.RegularExpressions.Syntax.Parser::ParseRegularExpression(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRegularExpression_m9088_MethodInfo = 
{
	"ParseRegularExpression"/* name */
	, (methodPointerType)&Parser_ParseRegularExpression_m9088/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &RegularExpression_t1965_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseRegularExpression_m9088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t1736_0_0_0;
extern const Il2CppType Hashtable_t1736_0_0_0;
static const ParameterInfo Parser_t1959_Parser_GetMapping_m9089_ParameterInfos[] = 
{
	{"mapping", 0, 134218496, 0, &Hashtable_t1736_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::GetMapping(System.Collections.Hashtable)
extern const MethodInfo Parser_GetMapping_m9089_MethodInfo = 
{
	"GetMapping"/* name */
	, (methodPointerType)&Parser_GetMapping_m9089/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Parser_t1959_Parser_GetMapping_m9089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1964_0_0_0;
extern const Il2CppType Group_t1964_0_0_0;
extern const Il2CppType RegexOptions_t1933_0_0_0;
extern const Il2CppType Assertion_t1970_0_0_0;
extern const Il2CppType Assertion_t1970_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseGroup_m9090_ParameterInfos[] = 
{
	{"group", 0, 134218497, 0, &Group_t1964_0_0_0},
	{"options", 1, 134218498, 0, &RegexOptions_t1933_0_0_0},
	{"assertion", 2, 134218499, 0, &Assertion_t1970_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseGroup(System.Text.RegularExpressions.Syntax.Group,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.Syntax.Assertion)
extern const MethodInfo Parser_ParseGroup_m9090_MethodInfo = 
{
	"ParseGroup"/* name */
	, (methodPointerType)&Parser_ParseGroup_m9090/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Object_t/* invoker_method */
	, Parser_t1959_Parser_ParseGroup_m9090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_1_0_0;
extern const Il2CppType RegexOptions_t1933_1_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseGroupingConstruct_m9091_ParameterInfos[] = 
{
	{"options", 0, 134218500, 0, &RegexOptions_t1933_1_0_0},
};
extern const Il2CppType Expression_t1962_0_0_0;
extern void* RuntimeInvoker_Object_t_RegexOptionsU26_t2038 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseGroupingConstruct(System.Text.RegularExpressions.RegexOptions&)
extern const MethodInfo Parser_ParseGroupingConstruct_m9091_MethodInfo = 
{
	"ParseGroupingConstruct"/* name */
	, (methodPointerType)&Parser_ParseGroupingConstruct_m9091/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RegexOptionsU26_t2038/* invoker_method */
	, Parser_t1959_Parser_ParseGroupingConstruct_m9091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ExpressionAssertion_t1971_0_0_0;
extern const Il2CppType ExpressionAssertion_t1971_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseAssertionType_m9092_ParameterInfos[] = 
{
	{"assertion", 0, 134218501, 0, &ExpressionAssertion_t1971_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseAssertionType(System.Text.RegularExpressions.Syntax.ExpressionAssertion)
extern const MethodInfo Parser_ParseAssertionType_m9092_MethodInfo = 
{
	"ParseAssertionType"/* name */
	, (methodPointerType)&Parser_ParseAssertionType_m9092/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Parser_t1959_Parser_ParseAssertionType_m9092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_1_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseOptions_m9093_ParameterInfos[] = 
{
	{"options", 0, 134218502, 0, &RegexOptions_t1933_1_0_0},
	{"negate", 1, 134218503, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_RegexOptionsU26_t2038_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseOptions(System.Text.RegularExpressions.RegexOptions&,System.Boolean)
extern const MethodInfo Parser_ParseOptions_m9093_MethodInfo = 
{
	"ParseOptions"/* name */
	, (methodPointerType)&Parser_ParseOptions_m9093/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_RegexOptionsU26_t2038_SByte_t170/* invoker_method */
	, Parser_t1959_Parser_ParseOptions_m9093_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseCharacterClass_m9094_ParameterInfos[] = 
{
	{"options", 0, 134218504, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseCharacterClass(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseCharacterClass_m9094_MethodInfo = 
{
	"ParseCharacterClass"/* name */
	, (methodPointerType)&Parser_ParseCharacterClass_m9094/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseCharacterClass_m9094_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseRepetitionBounds_m9095_ParameterInfos[] = 
{
	{"min", 0, 134218505, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218506, 0, &Int32_t127_1_0_2},
	{"options", 2, 134218507, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseRepetitionBounds(System.Int32&,System.Int32&,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRepetitionBounds_m9095_MethodInfo = 
{
	"ParseRepetitionBounds"/* name */
	, (methodPointerType)&Parser_ParseRepetitionBounds_m9095/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32U26_t535_Int32U26_t535_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseRepetitionBounds_m9095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Category_t1940 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.Syntax.Parser::ParseUnicodeCategory()
extern const MethodInfo Parser_ParseUnicodeCategory_m9096_MethodInfo = 
{
	"ParseUnicodeCategory"/* name */
	, (methodPointerType)&Parser_ParseUnicodeCategory_m9096/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1940_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1940/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseSpecial_m9097_ParameterInfos[] = 
{
	{"options", 0, 134218508, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseSpecial(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseSpecial_m9097_MethodInfo = 
{
	"ParseSpecial"/* name */
	, (methodPointerType)&Parser_ParseSpecial_m9097/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseSpecial_m9097_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseEscape()
extern const MethodInfo Parser_ParseEscape_m9098_MethodInfo = 
{
	"ParseEscape"/* name */
	, (methodPointerType)&Parser_ParseEscape_m9098/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName()
extern const MethodInfo Parser_ParseName_m9099_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m9099/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsNameChar_m9100_ParameterInfos[] = 
{
	{"c", 0, 134218509, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsNameChar(System.Char)
extern const MethodInfo Parser_IsNameChar_m9100_MethodInfo = 
{
	"IsNameChar"/* name */
	, (methodPointerType)&Parser_IsNameChar_m9100/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int16_t534/* invoker_method */
	, Parser_t1959_Parser_IsNameChar_m9100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseNumber_m9101_ParameterInfos[] = 
{
	{"b", 0, 134218510, 0, &Int32_t127_0_0_0},
	{"min", 1, 134218511, 0, &Int32_t127_0_0_0},
	{"max", 2, 134218512, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m9101_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m9101/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32_t127_Int32_t127_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseNumber_m9101_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ParseDigit_m9102_ParameterInfos[] = 
{
	{"c", 0, 134218513, 0, &Char_t451_0_0_0},
	{"b", 1, 134218514, 0, &Int32_t127_0_0_0},
	{"n", 2, 134218515, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int16_t534_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDigit(System.Char,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseDigit_m9102_MethodInfo = 
{
	"ParseDigit"/* name */
	, (methodPointerType)&Parser_ParseDigit_m9102/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int16_t534_Int32_t127_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_ParseDigit_m9102_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Parser_t1959_Parser_ConsumeWhitespace_m9103_ParameterInfos[] = 
{
	{"ignore", 0, 134218516, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ConsumeWhitespace(System.Boolean)
extern const MethodInfo Parser_ConsumeWhitespace_m9103_MethodInfo = 
{
	"ConsumeWhitespace"/* name */
	, (methodPointerType)&Parser_ConsumeWhitespace_m9103/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Parser_t1959_Parser_ConsumeWhitespace_m9103_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ResolveReferences()
extern const MethodInfo Parser_ResolveReferences_m9104_MethodInfo = 
{
	"ResolveReferences"/* name */
	, (methodPointerType)&Parser_ResolveReferences_m9104/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ArrayList_t1668_0_0_0;
extern const Il2CppType ArrayList_t1668_0_0_0;
static const ParameterInfo Parser_t1959_Parser_HandleExplicitNumericGroups_m9105_ParameterInfos[] = 
{
	{"explicit_numeric_groups", 0, 134218517, 0, &ArrayList_t1668_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::HandleExplicitNumericGroups(System.Collections.ArrayList)
extern const MethodInfo Parser_HandleExplicitNumericGroups_m9105_MethodInfo = 
{
	"HandleExplicitNumericGroups"/* name */
	, (methodPointerType)&Parser_HandleExplicitNumericGroups_m9105/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Parser_t1959_Parser_HandleExplicitNumericGroups_m9105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsIgnoreCase_m9106_ParameterInfos[] = 
{
	{"options", 0, 134218518, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnoreCase(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnoreCase_m9106_MethodInfo = 
{
	"IsIgnoreCase"/* name */
	, (methodPointerType)&Parser_IsIgnoreCase_m9106/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsIgnoreCase_m9106_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsMultiline_m9107_ParameterInfos[] = 
{
	{"options", 0, 134218519, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsMultiline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsMultiline_m9107_MethodInfo = 
{
	"IsMultiline"/* name */
	, (methodPointerType)&Parser_IsMultiline_m9107/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsMultiline_m9107_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsExplicitCapture_m9108_ParameterInfos[] = 
{
	{"options", 0, 134218520, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsExplicitCapture(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsExplicitCapture_m9108_MethodInfo = 
{
	"IsExplicitCapture"/* name */
	, (methodPointerType)&Parser_IsExplicitCapture_m9108/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsExplicitCapture_m9108_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsSingleline_m9109_ParameterInfos[] = 
{
	{"options", 0, 134218521, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsSingleline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsSingleline_m9109_MethodInfo = 
{
	"IsSingleline"/* name */
	, (methodPointerType)&Parser_IsSingleline_m9109/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsSingleline_m9109_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsIgnorePatternWhitespace_m9110_ParameterInfos[] = 
{
	{"options", 0, 134218522, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnorePatternWhitespace(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnorePatternWhitespace_m9110_MethodInfo = 
{
	"IsIgnorePatternWhitespace"/* name */
	, (methodPointerType)&Parser_IsIgnorePatternWhitespace_m9110/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsIgnorePatternWhitespace_m9110_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1933_0_0_0;
static const ParameterInfo Parser_t1959_Parser_IsECMAScript_m9111_ParameterInfos[] = 
{
	{"options", 0, 134218523, 0, &RegexOptions_t1933_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsECMAScript(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsECMAScript_m9111_MethodInfo = 
{
	"IsECMAScript"/* name */
	, (methodPointerType)&Parser_IsECMAScript_m9111/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int32_t127/* invoker_method */
	, Parser_t1959_Parser_IsECMAScript_m9111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Parser_t1959_Parser_NewParseException_m9112_ParameterInfos[] = 
{
	{"msg", 0, 134218524, 0, &String_t_0_0_0},
};
extern const Il2CppType ArgumentException_t470_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.ArgumentException System.Text.RegularExpressions.Syntax.Parser::NewParseException(System.String)
extern const MethodInfo Parser_NewParseException_m9112_MethodInfo = 
{
	"NewParseException"/* name */
	, (methodPointerType)&Parser_NewParseException_m9112/* method */
	, &Parser_t1959_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentException_t470_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t1959_Parser_NewParseException_m9112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Parser_t1959_MethodInfos[] =
{
	&Parser__ctor_m9082_MethodInfo,
	&Parser_ParseDecimal_m9083_MethodInfo,
	&Parser_ParseOctal_m9084_MethodInfo,
	&Parser_ParseHex_m9085_MethodInfo,
	&Parser_ParseNumber_m9086_MethodInfo,
	&Parser_ParseName_m9087_MethodInfo,
	&Parser_ParseRegularExpression_m9088_MethodInfo,
	&Parser_GetMapping_m9089_MethodInfo,
	&Parser_ParseGroup_m9090_MethodInfo,
	&Parser_ParseGroupingConstruct_m9091_MethodInfo,
	&Parser_ParseAssertionType_m9092_MethodInfo,
	&Parser_ParseOptions_m9093_MethodInfo,
	&Parser_ParseCharacterClass_m9094_MethodInfo,
	&Parser_ParseRepetitionBounds_m9095_MethodInfo,
	&Parser_ParseUnicodeCategory_m9096_MethodInfo,
	&Parser_ParseSpecial_m9097_MethodInfo,
	&Parser_ParseEscape_m9098_MethodInfo,
	&Parser_ParseName_m9099_MethodInfo,
	&Parser_IsNameChar_m9100_MethodInfo,
	&Parser_ParseNumber_m9101_MethodInfo,
	&Parser_ParseDigit_m9102_MethodInfo,
	&Parser_ConsumeWhitespace_m9103_MethodInfo,
	&Parser_ResolveReferences_m9104_MethodInfo,
	&Parser_HandleExplicitNumericGroups_m9105_MethodInfo,
	&Parser_IsIgnoreCase_m9106_MethodInfo,
	&Parser_IsMultiline_m9107_MethodInfo,
	&Parser_IsExplicitCapture_m9108_MethodInfo,
	&Parser_IsSingleline_m9109_MethodInfo,
	&Parser_IsIgnorePatternWhitespace_m9110_MethodInfo,
	&Parser_IsECMAScript_m9111_MethodInfo,
	&Parser_NewParseException_m9112_MethodInfo,
	NULL
};
static const Il2CppMethodReference Parser_t1959_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Parser_t1959_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Parser_t1959_0_0_0;
extern const Il2CppType Parser_t1959_1_0_0;
struct Parser_t1959;
const Il2CppTypeDefinitionMetadata Parser_t1959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t1959_VTable/* vtableMethods */
	, Parser_t1959_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 706/* fieldStart */

};
TypeInfo Parser_t1959_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Parser_t1959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Parser_t1959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t1959_0_0_0/* byval_arg */
	, &Parser_t1959_1_0_0/* this_arg */
	, &Parser_t1959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t1959)/* instance_size */
	, sizeof (Parser_t1959)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern TypeInfo QuickSearch_t1952_il2cpp_TypeInfo;
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearchMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo QuickSearch_t1952_QuickSearch__ctor_m9113_ParameterInfos[] = 
{
	{"str", 0, 134218525, 0, &String_t_0_0_0},
	{"ignore", 1, 134218526, 0, &Boolean_t169_0_0_0},
	{"reverse", 2, 134218527, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.ctor(System.String,System.Boolean,System.Boolean)
extern const MethodInfo QuickSearch__ctor_m9113_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuickSearch__ctor_m9113/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, QuickSearch_t1952_QuickSearch__ctor_m9113_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.cctor()
extern const MethodInfo QuickSearch__cctor_m9114_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QuickSearch__cctor_m9114/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::get_Length()
extern const MethodInfo QuickSearch_get_Length_m9115_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&QuickSearch_get_Length_m9115/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo QuickSearch_t1952_QuickSearch_Search_m9116_ParameterInfos[] = 
{
	{"text", 0, 134218528, 0, &String_t_0_0_0},
	{"start", 1, 134218529, 0, &Int32_t127_0_0_0},
	{"end", 2, 134218530, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::Search(System.String,System.Int32,System.Int32)
extern const MethodInfo QuickSearch_Search_m9116_MethodInfo = 
{
	"Search"/* name */
	, (methodPointerType)&QuickSearch_Search_m9116/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t_Int32_t127_Int32_t127/* invoker_method */
	, QuickSearch_t1952_QuickSearch_Search_m9116_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::SetupShiftTable()
extern const MethodInfo QuickSearch_SetupShiftTable_m9117_MethodInfo = 
{
	"SetupShiftTable"/* name */
	, (methodPointerType)&QuickSearch_SetupShiftTable_m9117/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo QuickSearch_t1952_QuickSearch_GetShiftDistance_m9118_ParameterInfos[] = 
{
	{"c", 0, 134218531, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::GetShiftDistance(System.Char)
extern const MethodInfo QuickSearch_GetShiftDistance_m9118_MethodInfo = 
{
	"GetShiftDistance"/* name */
	, (methodPointerType)&QuickSearch_GetShiftDistance_m9118/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int16_t534/* invoker_method */
	, QuickSearch_t1952_QuickSearch_GetShiftDistance_m9118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo QuickSearch_t1952_QuickSearch_GetChar_m9119_ParameterInfos[] = 
{
	{"c", 0, 134218532, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Char_t451_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Text.RegularExpressions.QuickSearch::GetChar(System.Char)
extern const MethodInfo QuickSearch_GetChar_m9119_MethodInfo = 
{
	"GetChar"/* name */
	, (methodPointerType)&QuickSearch_GetChar_m9119/* method */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* declaring_type */
	, &Char_t451_0_0_0/* return_type */
	, RuntimeInvoker_Char_t451_Int16_t534/* invoker_method */
	, QuickSearch_t1952_QuickSearch_GetChar_m9119_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QuickSearch_t1952_MethodInfos[] =
{
	&QuickSearch__ctor_m9113_MethodInfo,
	&QuickSearch__cctor_m9114_MethodInfo,
	&QuickSearch_get_Length_m9115_MethodInfo,
	&QuickSearch_Search_m9116_MethodInfo,
	&QuickSearch_SetupShiftTable_m9117_MethodInfo,
	&QuickSearch_GetShiftDistance_m9118_MethodInfo,
	&QuickSearch_GetChar_m9119_MethodInfo,
	NULL
};
extern const MethodInfo QuickSearch_get_Length_m9115_MethodInfo;
static const PropertyInfo QuickSearch_t1952____Length_PropertyInfo = 
{
	&QuickSearch_t1952_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &QuickSearch_get_Length_m9115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* QuickSearch_t1952_PropertyInfos[] =
{
	&QuickSearch_t1952____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference QuickSearch_t1952_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool QuickSearch_t1952_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType QuickSearch_t1952_0_0_0;
extern const Il2CppType QuickSearch_t1952_1_0_0;
struct QuickSearch_t1952;
const Il2CppTypeDefinitionMetadata QuickSearch_t1952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QuickSearch_t1952_VTable/* vtableMethods */
	, QuickSearch_t1952_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 712/* fieldStart */

};
TypeInfo QuickSearch_t1952_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuickSearch"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, QuickSearch_t1952_MethodInfos/* methods */
	, QuickSearch_t1952_PropertyInfos/* properties */
	, NULL/* events */
	, &QuickSearch_t1952_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuickSearch_t1952_0_0_0/* byval_arg */
	, &QuickSearch_t1952_1_0_0/* this_arg */
	, &QuickSearch_t1952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuickSearch_t1952)/* instance_size */
	, sizeof (QuickSearch_t1952)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QuickSearch_t1952_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.ReplacementEvaluator
extern TypeInfo ReplacementEvaluator_t1960_il2cpp_TypeInfo;
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluatorMethodDeclarations.h"
extern const Il2CppType Regex_t822_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator__ctor_m9120_ParameterInfos[] = 
{
	{"regex", 0, 134218533, 0, &Regex_t822_0_0_0},
	{"replacement", 1, 134218534, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::.ctor(System.Text.RegularExpressions.Regex,System.String)
extern const MethodInfo ReplacementEvaluator__ctor_m9120_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReplacementEvaluator__ctor_m9120/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator__ctor_m9120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1061_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_Evaluate_m9121_ParameterInfos[] = 
{
	{"match", 0, 134218535, 0, &Match_t1061_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.ReplacementEvaluator::Evaluate(System.Text.RegularExpressions.Match)
extern const MethodInfo ReplacementEvaluator_Evaluate_m9121_MethodInfo = 
{
	"Evaluate"/* name */
	, (methodPointerType)&ReplacementEvaluator_Evaluate_m9121/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_Evaluate_m9121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1061_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
extern const Il2CppType StringBuilder_t423_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_EvaluateAppend_m9122_ParameterInfos[] = 
{
	{"match", 0, 134218536, 0, &Match_t1061_0_0_0},
	{"sb", 1, 134218537, 0, &StringBuilder_t423_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::EvaluateAppend(System.Text.RegularExpressions.Match,System.Text.StringBuilder)
extern const MethodInfo ReplacementEvaluator_EvaluateAppend_m9122_MethodInfo = 
{
	"EvaluateAppend"/* name */
	, (methodPointerType)&ReplacementEvaluator_EvaluateAppend_m9122/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_EvaluateAppend_m9122_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.ReplacementEvaluator::get_NeedsGroupsOrCaptures()
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m9123_MethodInfo = 
{
	"get_NeedsGroupsOrCaptures"/* name */
	, (methodPointerType)&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m9123/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_Ensure_m9124_ParameterInfos[] = 
{
	{"size", 0, 134218538, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Ensure(System.Int32)
extern const MethodInfo ReplacementEvaluator_Ensure_m9124_MethodInfo = 
{
	"Ensure"/* name */
	, (methodPointerType)&ReplacementEvaluator_Ensure_m9124/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_Ensure_m9124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_AddFromReplacement_m9125_ParameterInfos[] = 
{
	{"start", 0, 134218539, 0, &Int32_t127_0_0_0},
	{"end", 1, 134218540, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddFromReplacement(System.Int32,System.Int32)
extern const MethodInfo ReplacementEvaluator_AddFromReplacement_m9125_MethodInfo = 
{
	"AddFromReplacement"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddFromReplacement_m9125/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_AddFromReplacement_m9125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_AddInt_m9126_ParameterInfos[] = 
{
	{"i", 0, 134218541, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddInt(System.Int32)
extern const MethodInfo ReplacementEvaluator_AddInt_m9126_MethodInfo = 
{
	"AddInt"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddInt_m9126/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_AddInt_m9126_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Compile()
extern const MethodInfo ReplacementEvaluator_Compile_m9127_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ReplacementEvaluator_Compile_m9127/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_0;
static const ParameterInfo ReplacementEvaluator_t1960_ReplacementEvaluator_CompileTerm_m9128_ParameterInfos[] = 
{
	{"ptr", 0, 134218542, 0, &Int32_t127_1_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.ReplacementEvaluator::CompileTerm(System.Int32&)
extern const MethodInfo ReplacementEvaluator_CompileTerm_m9128_MethodInfo = 
{
	"CompileTerm"/* name */
	, (methodPointerType)&ReplacementEvaluator_CompileTerm_m9128/* method */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int32U26_t535/* invoker_method */
	, ReplacementEvaluator_t1960_ReplacementEvaluator_CompileTerm_m9128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReplacementEvaluator_t1960_MethodInfos[] =
{
	&ReplacementEvaluator__ctor_m9120_MethodInfo,
	&ReplacementEvaluator_Evaluate_m9121_MethodInfo,
	&ReplacementEvaluator_EvaluateAppend_m9122_MethodInfo,
	&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m9123_MethodInfo,
	&ReplacementEvaluator_Ensure_m9124_MethodInfo,
	&ReplacementEvaluator_AddFromReplacement_m9125_MethodInfo,
	&ReplacementEvaluator_AddInt_m9126_MethodInfo,
	&ReplacementEvaluator_Compile_m9127_MethodInfo,
	&ReplacementEvaluator_CompileTerm_m9128_MethodInfo,
	NULL
};
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m9123_MethodInfo;
static const PropertyInfo ReplacementEvaluator_t1960____NeedsGroupsOrCaptures_PropertyInfo = 
{
	&ReplacementEvaluator_t1960_il2cpp_TypeInfo/* parent */
	, "NeedsGroupsOrCaptures"/* name */
	, &ReplacementEvaluator_get_NeedsGroupsOrCaptures_m9123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReplacementEvaluator_t1960_PropertyInfos[] =
{
	&ReplacementEvaluator_t1960____NeedsGroupsOrCaptures_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ReplacementEvaluator_t1960_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ReplacementEvaluator_t1960_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ReplacementEvaluator_t1960_0_0_0;
extern const Il2CppType ReplacementEvaluator_t1960_1_0_0;
struct ReplacementEvaluator_t1960;
const Il2CppTypeDefinitionMetadata ReplacementEvaluator_t1960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementEvaluator_t1960_VTable/* vtableMethods */
	, ReplacementEvaluator_t1960_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 719/* fieldStart */

};
TypeInfo ReplacementEvaluator_t1960_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ReplacementEvaluator_t1960_MethodInfos/* methods */
	, ReplacementEvaluator_t1960_PropertyInfos/* properties */
	, NULL/* events */
	, &ReplacementEvaluator_t1960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementEvaluator_t1960_0_0_0/* byval_arg */
	, &ReplacementEvaluator_t1960_1_0_0/* this_arg */
	, &ReplacementEvaluator_t1960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementEvaluator_t1960)/* instance_size */
	, sizeof (ReplacementEvaluator_t1960)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
extern TypeInfo ExpressionCollection_t1961_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
extern const MethodInfo ExpressionCollection__ctor_m9129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionCollection__ctor_m9129/* method */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo ExpressionCollection_t1961_ExpressionCollection_Add_m9130_ParameterInfos[] = 
{
	{"e", 0, 134218543, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_Add_m9130_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&ExpressionCollection_Add_m9130/* method */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ExpressionCollection_t1961_ExpressionCollection_Add_m9130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ExpressionCollection_t1961_ExpressionCollection_get_Item_m9131_ParameterInfos[] = 
{
	{"i", 0, 134218544, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
extern const MethodInfo ExpressionCollection_get_Item_m9131_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&ExpressionCollection_get_Item_m9131/* method */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, ExpressionCollection_t1961_ExpressionCollection_get_Item_m9131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo ExpressionCollection_t1961_ExpressionCollection_set_Item_m9132_ParameterInfos[] = 
{
	{"i", 0, 134218545, 0, &Int32_t127_0_0_0},
	{"value", 1, 134218546, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_set_Item_m9132_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&ExpressionCollection_set_Item_m9132/* method */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, ExpressionCollection_t1961_ExpressionCollection_set_Item_m9132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ExpressionCollection_t1961_ExpressionCollection_OnValidate_m9133_ParameterInfos[] = 
{
	{"o", 0, 134218547, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
extern const MethodInfo ExpressionCollection_OnValidate_m9133_MethodInfo = 
{
	"OnValidate"/* name */
	, (methodPointerType)&ExpressionCollection_OnValidate_m9133/* method */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ExpressionCollection_t1961_ExpressionCollection_OnValidate_m9133_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionCollection_t1961_MethodInfos[] =
{
	&ExpressionCollection__ctor_m9129_MethodInfo,
	&ExpressionCollection_Add_m9130_MethodInfo,
	&ExpressionCollection_get_Item_m9131_MethodInfo,
	&ExpressionCollection_set_Item_m9132_MethodInfo,
	&ExpressionCollection_OnValidate_m9133_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionCollection_get_Item_m9131_MethodInfo;
extern const MethodInfo ExpressionCollection_set_Item_m9132_MethodInfo;
static const PropertyInfo ExpressionCollection_t1961____Item_PropertyInfo = 
{
	&ExpressionCollection_t1961_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ExpressionCollection_get_Item_m9131_MethodInfo/* get */
	, &ExpressionCollection_set_Item_m9132_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionCollection_t1961_PropertyInfos[] =
{
	&ExpressionCollection_t1961____Item_PropertyInfo,
	NULL
};
extern const MethodInfo CollectionBase_GetEnumerator_m9527_MethodInfo;
extern const MethodInfo CollectionBase_get_Count_m8395_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_IsSynchronized_m8396_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_SyncRoot_m8397_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_CopyTo_m8398_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsFixedSize_m8399_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsReadOnly_m8400_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_Item_m8401_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_set_Item_m8402_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Add_m8403_MethodInfo;
extern const MethodInfo CollectionBase_Clear_m8404_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Contains_m8405_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_IndexOf_m8406_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Insert_m8407_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Remove_m8408_MethodInfo;
extern const MethodInfo CollectionBase_RemoveAt_m8409_MethodInfo;
extern const MethodInfo CollectionBase_OnClear_m8410_MethodInfo;
extern const MethodInfo CollectionBase_OnClearComplete_m8411_MethodInfo;
extern const MethodInfo CollectionBase_OnInsert_m8412_MethodInfo;
extern const MethodInfo CollectionBase_OnInsertComplete_m8413_MethodInfo;
extern const MethodInfo CollectionBase_OnRemove_m8414_MethodInfo;
extern const MethodInfo CollectionBase_OnRemoveComplete_m8415_MethodInfo;
extern const MethodInfo CollectionBase_OnSet_m8416_MethodInfo;
extern const MethodInfo CollectionBase_OnSetComplete_m8417_MethodInfo;
extern const MethodInfo ExpressionCollection_OnValidate_m9133_MethodInfo;
static const Il2CppMethodReference ExpressionCollection_t1961_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CollectionBase_GetEnumerator_m9527_MethodInfo,
	&CollectionBase_get_Count_m8395_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_IsSynchronized_m8396_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_SyncRoot_m8397_MethodInfo,
	&CollectionBase_System_Collections_ICollection_CopyTo_m8398_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsFixedSize_m8399_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsReadOnly_m8400_MethodInfo,
	&CollectionBase_System_Collections_IList_get_Item_m8401_MethodInfo,
	&CollectionBase_System_Collections_IList_set_Item_m8402_MethodInfo,
	&CollectionBase_System_Collections_IList_Add_m8403_MethodInfo,
	&CollectionBase_Clear_m8404_MethodInfo,
	&CollectionBase_System_Collections_IList_Contains_m8405_MethodInfo,
	&CollectionBase_System_Collections_IList_IndexOf_m8406_MethodInfo,
	&CollectionBase_System_Collections_IList_Insert_m8407_MethodInfo,
	&CollectionBase_System_Collections_IList_Remove_m8408_MethodInfo,
	&CollectionBase_RemoveAt_m8409_MethodInfo,
	&CollectionBase_OnClear_m8410_MethodInfo,
	&CollectionBase_OnClearComplete_m8411_MethodInfo,
	&CollectionBase_OnInsert_m8412_MethodInfo,
	&CollectionBase_OnInsertComplete_m8413_MethodInfo,
	&CollectionBase_OnRemove_m8414_MethodInfo,
	&CollectionBase_OnRemoveComplete_m8415_MethodInfo,
	&CollectionBase_OnSet_m8416_MethodInfo,
	&CollectionBase_OnSetComplete_m8417_MethodInfo,
	&ExpressionCollection_OnValidate_m9133_MethodInfo,
};
static bool ExpressionCollection_t1961_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExpressionCollection_t1961_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICollection_t1513_0_0_0, 5},
	{ &IList_t1514_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionCollection_t1961_0_0_0;
extern const Il2CppType ExpressionCollection_t1961_1_0_0;
extern const Il2CppType CollectionBase_t1701_0_0_0;
struct ExpressionCollection_t1961;
const Il2CppTypeDefinitionMetadata ExpressionCollection_t1961_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExpressionCollection_t1961_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1701_0_0_0/* parent */
	, ExpressionCollection_t1961_VTable/* vtableMethods */
	, ExpressionCollection_t1961_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExpressionCollection_t1961_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionCollection"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionCollection_t1961_MethodInfos/* methods */
	, ExpressionCollection_t1961_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionCollection_t1961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 65/* custom_attributes_cache */
	, &ExpressionCollection_t1961_0_0_0/* byval_arg */
	, &ExpressionCollection_t1961_1_0_0/* this_arg */
	, &ExpressionCollection_t1961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionCollection_t1961)/* instance_size */
	, sizeof (ExpressionCollection_t1961)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern TypeInfo Expression_t1962_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::.ctor()
extern const MethodInfo Expression__ctor_m9134_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Expression__ctor_m9134/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Expression_t1962_Expression_Compile_m9491_ParameterInfos[] = 
{
	{"cmp", 0, 134218548, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218549, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Expression_Compile_m9491_MethodInfo = 
{
	"Compile"/* name */
	, NULL/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Expression_t1962_Expression_Compile_m9491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Expression_t1962_Expression_GetWidth_m9492_ParameterInfos[] = 
{
	{"min", 0, 134218550, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218551, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Expression_GetWidth_m9492_MethodInfo = 
{
	"GetWidth"/* name */
	, NULL/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Expression_t1962_Expression_GetWidth_m9492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Expression::GetFixedWidth()
extern const MethodInfo Expression_GetFixedWidth_m9135_MethodInfo = 
{
	"GetFixedWidth"/* name */
	, (methodPointerType)&Expression_GetFixedWidth_m9135/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Expression_t1962_Expression_GetAnchorInfo_m9136_ParameterInfos[] = 
{
	{"reverse", 0, 134218552, 0, &Boolean_t169_0_0_0},
};
extern const Il2CppType AnchorInfo_t1980_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Expression::GetAnchorInfo(System.Boolean)
extern const MethodInfo Expression_GetAnchorInfo_m9136_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Expression_GetAnchorInfo_m9136/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, Expression_t1962_Expression_GetAnchorInfo_m9136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex()
extern const MethodInfo Expression_IsComplex_m9493_MethodInfo = 
{
	"IsComplex"/* name */
	, NULL/* method */
	, &Expression_t1962_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Expression_t1962_MethodInfos[] =
{
	&Expression__ctor_m9134_MethodInfo,
	&Expression_Compile_m9491_MethodInfo,
	&Expression_GetWidth_m9492_MethodInfo,
	&Expression_GetFixedWidth_m9135_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&Expression_IsComplex_m9493_MethodInfo,
	NULL
};
extern const MethodInfo Expression_GetAnchorInfo_m9136_MethodInfo;
static const Il2CppMethodReference Expression_t1962_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	NULL,
};
static bool Expression_t1962_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Expression_t1962_1_0_0;
struct Expression_t1962;
const Il2CppTypeDefinitionMetadata Expression_t1962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Expression_t1962_VTable/* vtableMethods */
	, Expression_t1962_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Expression_t1962_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Expression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Expression_t1962_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Expression_t1962_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Expression_t1962_0_0_0/* byval_arg */
	, &Expression_t1962_1_0_0/* this_arg */
	, &Expression_t1962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Expression_t1962)/* instance_size */
	, sizeof (Expression_t1962)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern TypeInfo CompositeExpression_t1963_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::.ctor()
extern const MethodInfo CompositeExpression__ctor_m9137_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompositeExpression__ctor_m9137/* method */
	, &CompositeExpression_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::get_Expressions()
extern const MethodInfo CompositeExpression_get_Expressions_m9138_MethodInfo = 
{
	"get_Expressions"/* name */
	, (methodPointerType)&CompositeExpression_get_Expressions_m9138/* method */
	, &CompositeExpression_t1963_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1961_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo CompositeExpression_t1963_CompositeExpression_GetWidth_m9139_ParameterInfos[] = 
{
	{"min", 0, 134218553, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218554, 0, &Int32_t127_1_0_2},
	{"count", 2, 134218555, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::GetWidth(System.Int32&,System.Int32&,System.Int32)
extern const MethodInfo CompositeExpression_GetWidth_m9139_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CompositeExpression_GetWidth_m9139/* method */
	, &CompositeExpression_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535_Int32_t127/* invoker_method */
	, CompositeExpression_t1963_CompositeExpression_GetWidth_m9139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CompositeExpression::IsComplex()
extern const MethodInfo CompositeExpression_IsComplex_m9140_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CompositeExpression_IsComplex_m9140/* method */
	, &CompositeExpression_t1963_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CompositeExpression_t1963_MethodInfos[] =
{
	&CompositeExpression__ctor_m9137_MethodInfo,
	&CompositeExpression_get_Expressions_m9138_MethodInfo,
	&CompositeExpression_GetWidth_m9139_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
	NULL
};
extern const MethodInfo CompositeExpression_get_Expressions_m9138_MethodInfo;
static const PropertyInfo CompositeExpression_t1963____Expressions_PropertyInfo = 
{
	&CompositeExpression_t1963_il2cpp_TypeInfo/* parent */
	, "Expressions"/* name */
	, &CompositeExpression_get_Expressions_m9138_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CompositeExpression_t1963_PropertyInfos[] =
{
	&CompositeExpression_t1963____Expressions_PropertyInfo,
	NULL
};
extern const MethodInfo CompositeExpression_IsComplex_m9140_MethodInfo;
static const Il2CppMethodReference CompositeExpression_t1963_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool CompositeExpression_t1963_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CompositeExpression_t1963_0_0_0;
extern const Il2CppType CompositeExpression_t1963_1_0_0;
struct CompositeExpression_t1963;
const Il2CppTypeDefinitionMetadata CompositeExpression_t1963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1962_0_0_0/* parent */
	, CompositeExpression_t1963_VTable/* vtableMethods */
	, CompositeExpression_t1963_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 723/* fieldStart */

};
TypeInfo CompositeExpression_t1963_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompositeExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CompositeExpression_t1963_MethodInfos/* methods */
	, CompositeExpression_t1963_PropertyInfos/* properties */
	, NULL/* events */
	, &CompositeExpression_t1963_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompositeExpression_t1963_0_0_0/* byval_arg */
	, &CompositeExpression_t1963_1_0_0/* this_arg */
	, &CompositeExpression_t1963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompositeExpression_t1963)/* instance_size */
	, sizeof (CompositeExpression_t1963)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern TypeInfo Group_t1964_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::.ctor()
extern const MethodInfo Group__ctor_m9141_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Group__ctor_m9141/* method */
	, &Group_t1964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo Group_t1964_Group_AppendExpression_m9142_ParameterInfos[] = 
{
	{"e", 0, 134218556, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::AppendExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Group_AppendExpression_m9142_MethodInfo = 
{
	"AppendExpression"/* name */
	, (methodPointerType)&Group_AppendExpression_m9142/* method */
	, &Group_t1964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Group_t1964_Group_AppendExpression_m9142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Group_t1964_Group_Compile_m9143_ParameterInfos[] = 
{
	{"cmp", 0, 134218557, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218558, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Group_Compile_m9143_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Group_Compile_m9143/* method */
	, &Group_t1964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Group_t1964_Group_Compile_m9143_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Group_t1964_Group_GetWidth_m9144_ParameterInfos[] = 
{
	{"min", 0, 134218559, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218560, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Group_GetWidth_m9144_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Group_GetWidth_m9144/* method */
	, &Group_t1964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Group_t1964_Group_GetWidth_m9144_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Group_t1964_Group_GetAnchorInfo_m9145_ParameterInfos[] = 
{
	{"reverse", 0, 134218561, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Group::GetAnchorInfo(System.Boolean)
extern const MethodInfo Group_GetAnchorInfo_m9145_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Group_GetAnchorInfo_m9145/* method */
	, &Group_t1964_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, Group_t1964_Group_GetAnchorInfo_m9145_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Group_t1964_MethodInfos[] =
{
	&Group__ctor_m9141_MethodInfo,
	&Group_AppendExpression_m9142_MethodInfo,
	&Group_Compile_m9143_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	NULL
};
extern const MethodInfo Group_Compile_m9143_MethodInfo;
extern const MethodInfo Group_GetWidth_m9144_MethodInfo;
extern const MethodInfo Group_GetAnchorInfo_m9145_MethodInfo;
static const Il2CppMethodReference Group_t1964_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Group_Compile_m9143_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool Group_t1964_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t1964_1_0_0;
struct Group_t1964;
const Il2CppTypeDefinitionMetadata Group_t1964_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1963_0_0_0/* parent */
	, Group_t1964_VTable/* vtableMethods */
	, Group_t1964_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Group_t1964_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Group_t1964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Group_t1964_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t1964_0_0_0/* byval_arg */
	, &Group_t1964_1_0_0/* this_arg */
	, &Group_t1964_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t1964)/* instance_size */
	, sizeof (Group_t1964)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern TypeInfo RegularExpression_t1965_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::.ctor()
extern const MethodInfo RegularExpression__ctor_m9146_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RegularExpression__ctor_m9146/* method */
	, &RegularExpression_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo RegularExpression_t1965_RegularExpression_set_GroupCount_m9147_ParameterInfos[] = 
{
	{"value", 0, 134218562, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::set_GroupCount(System.Int32)
extern const MethodInfo RegularExpression_set_GroupCount_m9147_MethodInfo = 
{
	"set_GroupCount"/* name */
	, (methodPointerType)&RegularExpression_set_GroupCount_m9147/* method */
	, &RegularExpression_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, RegularExpression_t1965_RegularExpression_set_GroupCount_m9147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo RegularExpression_t1965_RegularExpression_Compile_m9148_ParameterInfos[] = 
{
	{"cmp", 0, 134218563, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218564, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo RegularExpression_Compile_m9148_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&RegularExpression_Compile_m9148/* method */
	, &RegularExpression_t1965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, RegularExpression_t1965_RegularExpression_Compile_m9148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RegularExpression_t1965_MethodInfos[] =
{
	&RegularExpression__ctor_m9146_MethodInfo,
	&RegularExpression_set_GroupCount_m9147_MethodInfo,
	&RegularExpression_Compile_m9148_MethodInfo,
	NULL
};
extern const MethodInfo RegularExpression_set_GroupCount_m9147_MethodInfo;
static const PropertyInfo RegularExpression_t1965____GroupCount_PropertyInfo = 
{
	&RegularExpression_t1965_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, NULL/* get */
	, &RegularExpression_set_GroupCount_m9147_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RegularExpression_t1965_PropertyInfos[] =
{
	&RegularExpression_t1965____GroupCount_PropertyInfo,
	NULL
};
extern const MethodInfo RegularExpression_Compile_m9148_MethodInfo;
static const Il2CppMethodReference RegularExpression_t1965_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&RegularExpression_Compile_m9148_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool RegularExpression_t1965_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegularExpression_t1965_1_0_0;
struct RegularExpression_t1965;
const Il2CppTypeDefinitionMetadata RegularExpression_t1965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1964_0_0_0/* parent */
	, RegularExpression_t1965_VTable/* vtableMethods */
	, RegularExpression_t1965_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 724/* fieldStart */

};
TypeInfo RegularExpression_t1965_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegularExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, RegularExpression_t1965_MethodInfos/* methods */
	, RegularExpression_t1965_PropertyInfos/* properties */
	, NULL/* events */
	, &RegularExpression_t1965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RegularExpression_t1965_0_0_0/* byval_arg */
	, &RegularExpression_t1965_1_0_0/* this_arg */
	, &RegularExpression_t1965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegularExpression_t1965)/* instance_size */
	, sizeof (RegularExpression_t1965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern TypeInfo CapturingGroup_t1966_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::.ctor()
extern const MethodInfo CapturingGroup__ctor_m9149_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CapturingGroup__ctor_m9149/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::get_Index()
extern const MethodInfo CapturingGroup_get_Index_m9150_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&CapturingGroup_get_Index_m9150/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo CapturingGroup_t1966_CapturingGroup_set_Index_m9151_ParameterInfos[] = 
{
	{"value", 0, 134218565, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Index(System.Int32)
extern const MethodInfo CapturingGroup_set_Index_m9151_MethodInfo = 
{
	"set_Index"/* name */
	, (methodPointerType)&CapturingGroup_set_Index_m9151/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, CapturingGroup_t1966_CapturingGroup_set_Index_m9151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::get_Name()
extern const MethodInfo CapturingGroup_get_Name_m9152_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&CapturingGroup_get_Name_m9152/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CapturingGroup_t1966_CapturingGroup_set_Name_m9153_ParameterInfos[] = 
{
	{"value", 0, 134218566, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Name(System.String)
extern const MethodInfo CapturingGroup_set_Name_m9153_MethodInfo = 
{
	"set_Name"/* name */
	, (methodPointerType)&CapturingGroup_set_Name_m9153/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CapturingGroup_t1966_CapturingGroup_set_Name_m9153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::get_IsNamed()
extern const MethodInfo CapturingGroup_get_IsNamed_m9154_MethodInfo = 
{
	"get_IsNamed"/* name */
	, (methodPointerType)&CapturingGroup_get_IsNamed_m9154/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CapturingGroup_t1966_CapturingGroup_Compile_m9155_ParameterInfos[] = 
{
	{"cmp", 0, 134218567, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218568, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CapturingGroup_Compile_m9155_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CapturingGroup_Compile_m9155/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, CapturingGroup_t1966_CapturingGroup_Compile_m9155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::IsComplex()
extern const MethodInfo CapturingGroup_IsComplex_m9156_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CapturingGroup_IsComplex_m9156/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CapturingGroup_t1966_CapturingGroup_CompareTo_m9157_ParameterInfos[] = 
{
	{"other", 0, 134218569, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::CompareTo(System.Object)
extern const MethodInfo CapturingGroup_CompareTo_m9157_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&CapturingGroup_CompareTo_m9157/* method */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, CapturingGroup_t1966_CapturingGroup_CompareTo_m9157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CapturingGroup_t1966_MethodInfos[] =
{
	&CapturingGroup__ctor_m9149_MethodInfo,
	&CapturingGroup_get_Index_m9150_MethodInfo,
	&CapturingGroup_set_Index_m9151_MethodInfo,
	&CapturingGroup_get_Name_m9152_MethodInfo,
	&CapturingGroup_set_Name_m9153_MethodInfo,
	&CapturingGroup_get_IsNamed_m9154_MethodInfo,
	&CapturingGroup_Compile_m9155_MethodInfo,
	&CapturingGroup_IsComplex_m9156_MethodInfo,
	&CapturingGroup_CompareTo_m9157_MethodInfo,
	NULL
};
extern const MethodInfo CapturingGroup_get_Index_m9150_MethodInfo;
extern const MethodInfo CapturingGroup_set_Index_m9151_MethodInfo;
static const PropertyInfo CapturingGroup_t1966____Index_PropertyInfo = 
{
	&CapturingGroup_t1966_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &CapturingGroup_get_Index_m9150_MethodInfo/* get */
	, &CapturingGroup_set_Index_m9151_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_Name_m9152_MethodInfo;
extern const MethodInfo CapturingGroup_set_Name_m9153_MethodInfo;
static const PropertyInfo CapturingGroup_t1966____Name_PropertyInfo = 
{
	&CapturingGroup_t1966_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &CapturingGroup_get_Name_m9152_MethodInfo/* get */
	, &CapturingGroup_set_Name_m9153_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_IsNamed_m9154_MethodInfo;
static const PropertyInfo CapturingGroup_t1966____IsNamed_PropertyInfo = 
{
	&CapturingGroup_t1966_il2cpp_TypeInfo/* parent */
	, "IsNamed"/* name */
	, &CapturingGroup_get_IsNamed_m9154_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CapturingGroup_t1966_PropertyInfos[] =
{
	&CapturingGroup_t1966____Index_PropertyInfo,
	&CapturingGroup_t1966____Name_PropertyInfo,
	&CapturingGroup_t1966____IsNamed_PropertyInfo,
	NULL
};
extern const MethodInfo CapturingGroup_Compile_m9155_MethodInfo;
extern const MethodInfo CapturingGroup_IsComplex_m9156_MethodInfo;
extern const MethodInfo CapturingGroup_CompareTo_m9157_MethodInfo;
static const Il2CppMethodReference CapturingGroup_t1966_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CapturingGroup_Compile_m9155_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	&CapturingGroup_IsComplex_m9156_MethodInfo,
	&CapturingGroup_CompareTo_m9157_MethodInfo,
};
static bool CapturingGroup_t1966_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CapturingGroup_t1966_InterfacesTypeInfos[] = 
{
	&IComparable_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair CapturingGroup_t1966_InterfacesOffsets[] = 
{
	{ &IComparable_t166_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CapturingGroup_t1966_0_0_0;
extern const Il2CppType CapturingGroup_t1966_1_0_0;
struct CapturingGroup_t1966;
const Il2CppTypeDefinitionMetadata CapturingGroup_t1966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CapturingGroup_t1966_InterfacesTypeInfos/* implementedInterfaces */
	, CapturingGroup_t1966_InterfacesOffsets/* interfaceOffsets */
	, &Group_t1964_0_0_0/* parent */
	, CapturingGroup_t1966_VTable/* vtableMethods */
	, CapturingGroup_t1966_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 725/* fieldStart */

};
TypeInfo CapturingGroup_t1966_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapturingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CapturingGroup_t1966_MethodInfos/* methods */
	, CapturingGroup_t1966_PropertyInfos/* properties */
	, NULL/* events */
	, &CapturingGroup_t1966_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapturingGroup_t1966_0_0_0/* byval_arg */
	, &CapturingGroup_t1966_1_0_0/* this_arg */
	, &CapturingGroup_t1966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapturingGroup_t1966)/* instance_size */
	, sizeof (CapturingGroup_t1966)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern TypeInfo BalancingGroup_t1967_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::.ctor()
extern const MethodInfo BalancingGroup__ctor_m9158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BalancingGroup__ctor_m9158/* method */
	, &BalancingGroup_t1967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1966_0_0_0;
static const ParameterInfo BalancingGroup_t1967_BalancingGroup_set_Balance_m9159_ParameterInfos[] = 
{
	{"value", 0, 134218570, 0, &CapturingGroup_t1966_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::set_Balance(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo BalancingGroup_set_Balance_m9159_MethodInfo = 
{
	"set_Balance"/* name */
	, (methodPointerType)&BalancingGroup_set_Balance_m9159/* method */
	, &BalancingGroup_t1967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, BalancingGroup_t1967_BalancingGroup_set_Balance_m9159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo BalancingGroup_t1967_BalancingGroup_Compile_m9160_ParameterInfos[] = 
{
	{"cmp", 0, 134218571, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218572, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BalancingGroup_Compile_m9160_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BalancingGroup_Compile_m9160/* method */
	, &BalancingGroup_t1967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, BalancingGroup_t1967_BalancingGroup_Compile_m9160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BalancingGroup_t1967_MethodInfos[] =
{
	&BalancingGroup__ctor_m9158_MethodInfo,
	&BalancingGroup_set_Balance_m9159_MethodInfo,
	&BalancingGroup_Compile_m9160_MethodInfo,
	NULL
};
extern const MethodInfo BalancingGroup_set_Balance_m9159_MethodInfo;
static const PropertyInfo BalancingGroup_t1967____Balance_PropertyInfo = 
{
	&BalancingGroup_t1967_il2cpp_TypeInfo/* parent */
	, "Balance"/* name */
	, NULL/* get */
	, &BalancingGroup_set_Balance_m9159_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BalancingGroup_t1967_PropertyInfos[] =
{
	&BalancingGroup_t1967____Balance_PropertyInfo,
	NULL
};
extern const MethodInfo BalancingGroup_Compile_m9160_MethodInfo;
static const Il2CppMethodReference BalancingGroup_t1967_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&BalancingGroup_Compile_m9160_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	&CapturingGroup_IsComplex_m9156_MethodInfo,
	&CapturingGroup_CompareTo_m9157_MethodInfo,
};
static bool BalancingGroup_t1967_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BalancingGroup_t1967_InterfacesOffsets[] = 
{
	{ &IComparable_t166_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BalancingGroup_t1967_0_0_0;
extern const Il2CppType BalancingGroup_t1967_1_0_0;
struct BalancingGroup_t1967;
const Il2CppTypeDefinitionMetadata BalancingGroup_t1967_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BalancingGroup_t1967_InterfacesOffsets/* interfaceOffsets */
	, &CapturingGroup_t1966_0_0_0/* parent */
	, BalancingGroup_t1967_VTable/* vtableMethods */
	, BalancingGroup_t1967_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 727/* fieldStart */

};
TypeInfo BalancingGroup_t1967_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BalancingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BalancingGroup_t1967_MethodInfos/* methods */
	, BalancingGroup_t1967_PropertyInfos/* properties */
	, NULL/* events */
	, &BalancingGroup_t1967_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BalancingGroup_t1967_0_0_0/* byval_arg */
	, &BalancingGroup_t1967_1_0_0/* this_arg */
	, &BalancingGroup_t1967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BalancingGroup_t1967)/* instance_size */
	, sizeof (BalancingGroup_t1967)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern TypeInfo NonBacktrackingGroup_t1968_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
extern const MethodInfo NonBacktrackingGroup__ctor_m9161_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonBacktrackingGroup__ctor_m9161/* method */
	, &NonBacktrackingGroup_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo NonBacktrackingGroup_t1968_NonBacktrackingGroup_Compile_m9162_ParameterInfos[] = 
{
	{"cmp", 0, 134218573, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218574, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo NonBacktrackingGroup_Compile_m9162_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&NonBacktrackingGroup_Compile_m9162/* method */
	, &NonBacktrackingGroup_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, NonBacktrackingGroup_t1968_NonBacktrackingGroup_Compile_m9162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
extern const MethodInfo NonBacktrackingGroup_IsComplex_m9163_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&NonBacktrackingGroup_IsComplex_m9163/* method */
	, &NonBacktrackingGroup_t1968_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonBacktrackingGroup_t1968_MethodInfos[] =
{
	&NonBacktrackingGroup__ctor_m9161_MethodInfo,
	&NonBacktrackingGroup_Compile_m9162_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m9163_MethodInfo,
	NULL
};
extern const MethodInfo NonBacktrackingGroup_Compile_m9162_MethodInfo;
extern const MethodInfo NonBacktrackingGroup_IsComplex_m9163_MethodInfo;
static const Il2CppMethodReference NonBacktrackingGroup_t1968_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&NonBacktrackingGroup_Compile_m9162_MethodInfo,
	&Group_GetWidth_m9144_MethodInfo,
	&Group_GetAnchorInfo_m9145_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m9163_MethodInfo,
};
static bool NonBacktrackingGroup_t1968_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NonBacktrackingGroup_t1968_0_0_0;
extern const Il2CppType NonBacktrackingGroup_t1968_1_0_0;
struct NonBacktrackingGroup_t1968;
const Il2CppTypeDefinitionMetadata NonBacktrackingGroup_t1968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1964_0_0_0/* parent */
	, NonBacktrackingGroup_t1968_VTable/* vtableMethods */
	, NonBacktrackingGroup_t1968_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonBacktrackingGroup_t1968_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonBacktrackingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NonBacktrackingGroup_t1968_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonBacktrackingGroup_t1968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonBacktrackingGroup_t1968_0_0_0/* byval_arg */
	, &NonBacktrackingGroup_t1968_1_0_0/* this_arg */
	, &NonBacktrackingGroup_t1968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonBacktrackingGroup_t1968)/* instance_size */
	, sizeof (NonBacktrackingGroup_t1968)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern TypeInfo Repetition_t1969_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Repetition_t1969_Repetition__ctor_m9164_ParameterInfos[] = 
{
	{"min", 0, 134218575, 0, &Int32_t127_0_0_0},
	{"max", 1, 134218576, 0, &Int32_t127_0_0_0},
	{"lazy", 2, 134218577, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::.ctor(System.Int32,System.Int32,System.Boolean)
extern const MethodInfo Repetition__ctor_m9164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Repetition__ctor_m9164/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127_SByte_t170/* invoker_method */
	, Repetition_t1969_Repetition__ctor_m9164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Repetition::get_Expression()
extern const MethodInfo Repetition_get_Expression_m9165_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&Repetition_get_Expression_m9165/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo Repetition_t1969_Repetition_set_Expression_m9166_ParameterInfos[] = 
{
	{"value", 0, 134218578, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::set_Expression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Repetition_set_Expression_m9166_MethodInfo = 
{
	"set_Expression"/* name */
	, (methodPointerType)&Repetition_set_Expression_m9166/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Repetition_t1969_Repetition_set_Expression_m9166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::get_Minimum()
extern const MethodInfo Repetition_get_Minimum_m9167_MethodInfo = 
{
	"get_Minimum"/* name */
	, (methodPointerType)&Repetition_get_Minimum_m9167/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Repetition_t1969_Repetition_Compile_m9168_ParameterInfos[] = 
{
	{"cmp", 0, 134218579, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218580, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Repetition_Compile_m9168_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Repetition_Compile_m9168/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Repetition_t1969_Repetition_Compile_m9168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Repetition_t1969_Repetition_GetWidth_m9169_ParameterInfos[] = 
{
	{"min", 0, 134218581, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218582, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Repetition_GetWidth_m9169_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Repetition_GetWidth_m9169/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Repetition_t1969_Repetition_GetWidth_m9169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Repetition_t1969_Repetition_GetAnchorInfo_m9170_ParameterInfos[] = 
{
	{"reverse", 0, 134218583, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Repetition::GetAnchorInfo(System.Boolean)
extern const MethodInfo Repetition_GetAnchorInfo_m9170_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Repetition_GetAnchorInfo_m9170/* method */
	, &Repetition_t1969_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, Repetition_t1969_Repetition_GetAnchorInfo_m9170_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Repetition_t1969_MethodInfos[] =
{
	&Repetition__ctor_m9164_MethodInfo,
	&Repetition_get_Expression_m9165_MethodInfo,
	&Repetition_set_Expression_m9166_MethodInfo,
	&Repetition_get_Minimum_m9167_MethodInfo,
	&Repetition_Compile_m9168_MethodInfo,
	&Repetition_GetWidth_m9169_MethodInfo,
	&Repetition_GetAnchorInfo_m9170_MethodInfo,
	NULL
};
extern const MethodInfo Repetition_get_Expression_m9165_MethodInfo;
extern const MethodInfo Repetition_set_Expression_m9166_MethodInfo;
static const PropertyInfo Repetition_t1969____Expression_PropertyInfo = 
{
	&Repetition_t1969_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &Repetition_get_Expression_m9165_MethodInfo/* get */
	, &Repetition_set_Expression_m9166_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Repetition_get_Minimum_m9167_MethodInfo;
static const PropertyInfo Repetition_t1969____Minimum_PropertyInfo = 
{
	&Repetition_t1969_il2cpp_TypeInfo/* parent */
	, "Minimum"/* name */
	, &Repetition_get_Minimum_m9167_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Repetition_t1969_PropertyInfos[] =
{
	&Repetition_t1969____Expression_PropertyInfo,
	&Repetition_t1969____Minimum_PropertyInfo,
	NULL
};
extern const MethodInfo Repetition_Compile_m9168_MethodInfo;
extern const MethodInfo Repetition_GetWidth_m9169_MethodInfo;
extern const MethodInfo Repetition_GetAnchorInfo_m9170_MethodInfo;
static const Il2CppMethodReference Repetition_t1969_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Repetition_Compile_m9168_MethodInfo,
	&Repetition_GetWidth_m9169_MethodInfo,
	&Repetition_GetAnchorInfo_m9170_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool Repetition_t1969_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Repetition_t1969_0_0_0;
extern const Il2CppType Repetition_t1969_1_0_0;
struct Repetition_t1969;
const Il2CppTypeDefinitionMetadata Repetition_t1969_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1963_0_0_0/* parent */
	, Repetition_t1969_VTable/* vtableMethods */
	, Repetition_t1969_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 728/* fieldStart */

};
TypeInfo Repetition_t1969_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Repetition"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Repetition_t1969_MethodInfos/* methods */
	, Repetition_t1969_PropertyInfos/* properties */
	, NULL/* events */
	, &Repetition_t1969_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Repetition_t1969_0_0_0/* byval_arg */
	, &Repetition_t1969_1_0_0/* this_arg */
	, &Repetition_t1969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Repetition_t1969)/* instance_size */
	, sizeof (Repetition_t1969)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
extern TypeInfo Assertion_t1970_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
extern const MethodInfo Assertion__ctor_m9171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Assertion__ctor_m9171/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
extern const MethodInfo Assertion_get_TrueExpression_m9172_MethodInfo = 
{
	"get_TrueExpression"/* name */
	, (methodPointerType)&Assertion_get_TrueExpression_m9172/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo Assertion_t1970_Assertion_set_TrueExpression_m9173_ParameterInfos[] = 
{
	{"value", 0, 134218584, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_TrueExpression_m9173_MethodInfo = 
{
	"set_TrueExpression"/* name */
	, (methodPointerType)&Assertion_set_TrueExpression_m9173/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Assertion_t1970_Assertion_set_TrueExpression_m9173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
extern const MethodInfo Assertion_get_FalseExpression_m9174_MethodInfo = 
{
	"get_FalseExpression"/* name */
	, (methodPointerType)&Assertion_get_FalseExpression_m9174/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo Assertion_t1970_Assertion_set_FalseExpression_m9175_ParameterInfos[] = 
{
	{"value", 0, 134218585, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_FalseExpression_m9175_MethodInfo = 
{
	"set_FalseExpression"/* name */
	, (methodPointerType)&Assertion_set_FalseExpression_m9175/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Assertion_t1970_Assertion_set_FalseExpression_m9175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Assertion_t1970_Assertion_GetWidth_m9176_ParameterInfos[] = 
{
	{"min", 0, 134218586, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218587, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Assertion_GetWidth_m9176_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Assertion_GetWidth_m9176/* method */
	, &Assertion_t1970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Assertion_t1970_Assertion_GetWidth_m9176_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Assertion_t1970_MethodInfos[] =
{
	&Assertion__ctor_m9171_MethodInfo,
	&Assertion_get_TrueExpression_m9172_MethodInfo,
	&Assertion_set_TrueExpression_m9173_MethodInfo,
	&Assertion_get_FalseExpression_m9174_MethodInfo,
	&Assertion_set_FalseExpression_m9175_MethodInfo,
	&Assertion_GetWidth_m9176_MethodInfo,
	NULL
};
extern const MethodInfo Assertion_get_TrueExpression_m9172_MethodInfo;
extern const MethodInfo Assertion_set_TrueExpression_m9173_MethodInfo;
static const PropertyInfo Assertion_t1970____TrueExpression_PropertyInfo = 
{
	&Assertion_t1970_il2cpp_TypeInfo/* parent */
	, "TrueExpression"/* name */
	, &Assertion_get_TrueExpression_m9172_MethodInfo/* get */
	, &Assertion_set_TrueExpression_m9173_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Assertion_get_FalseExpression_m9174_MethodInfo;
extern const MethodInfo Assertion_set_FalseExpression_m9175_MethodInfo;
static const PropertyInfo Assertion_t1970____FalseExpression_PropertyInfo = 
{
	&Assertion_t1970_il2cpp_TypeInfo/* parent */
	, "FalseExpression"/* name */
	, &Assertion_get_FalseExpression_m9174_MethodInfo/* get */
	, &Assertion_set_FalseExpression_m9175_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Assertion_t1970_PropertyInfos[] =
{
	&Assertion_t1970____TrueExpression_PropertyInfo,
	&Assertion_t1970____FalseExpression_PropertyInfo,
	NULL
};
extern const MethodInfo Assertion_GetWidth_m9176_MethodInfo;
static const Il2CppMethodReference Assertion_t1970_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	&Assertion_GetWidth_m9176_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool Assertion_t1970_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Assertion_t1970_1_0_0;
struct Assertion_t1970;
const Il2CppTypeDefinitionMetadata Assertion_t1970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1963_0_0_0/* parent */
	, Assertion_t1970_VTable/* vtableMethods */
	, Assertion_t1970_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Assertion_t1970_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Assertion_t1970_MethodInfos/* methods */
	, Assertion_t1970_PropertyInfos/* properties */
	, NULL/* events */
	, &Assertion_t1970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Assertion_t1970_0_0_0/* byval_arg */
	, &Assertion_t1970_1_0_0/* this_arg */
	, &Assertion_t1970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assertion_t1970)/* instance_size */
	, sizeof (Assertion_t1970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern TypeInfo CaptureAssertion_t1973_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
extern const Il2CppType Literal_t1972_0_0_0;
extern const Il2CppType Literal_t1972_0_0_0;
static const ParameterInfo CaptureAssertion_t1973_CaptureAssertion__ctor_m9177_ParameterInfos[] = 
{
	{"l", 0, 134218588, 0, &Literal_t1972_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::.ctor(System.Text.RegularExpressions.Syntax.Literal)
extern const MethodInfo CaptureAssertion__ctor_m9177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CaptureAssertion__ctor_m9177/* method */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CaptureAssertion_t1973_CaptureAssertion__ctor_m9177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1966_0_0_0;
static const ParameterInfo CaptureAssertion_t1973_CaptureAssertion_set_CapturingGroup_m9178_ParameterInfos[] = 
{
	{"value", 0, 134218589, 0, &CapturingGroup_t1966_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m9178_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&CaptureAssertion_set_CapturingGroup_m9178/* method */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CaptureAssertion_t1973_CaptureAssertion_set_CapturingGroup_m9178_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CaptureAssertion_t1973_CaptureAssertion_Compile_m9179_ParameterInfos[] = 
{
	{"cmp", 0, 134218590, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218591, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CaptureAssertion_Compile_m9179_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CaptureAssertion_Compile_m9179/* method */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, CaptureAssertion_t1973_CaptureAssertion_Compile_m9179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CaptureAssertion::IsComplex()
extern const MethodInfo CaptureAssertion_IsComplex_m9180_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CaptureAssertion_IsComplex_m9180/* method */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::get_Alternate()
extern const MethodInfo CaptureAssertion_get_Alternate_m9181_MethodInfo = 
{
	"get_Alternate"/* name */
	, (methodPointerType)&CaptureAssertion_get_Alternate_m9181/* method */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionAssertion_t1971_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CaptureAssertion_t1973_MethodInfos[] =
{
	&CaptureAssertion__ctor_m9177_MethodInfo,
	&CaptureAssertion_set_CapturingGroup_m9178_MethodInfo,
	&CaptureAssertion_Compile_m9179_MethodInfo,
	&CaptureAssertion_IsComplex_m9180_MethodInfo,
	&CaptureAssertion_get_Alternate_m9181_MethodInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m9178_MethodInfo;
static const PropertyInfo CaptureAssertion_t1973____CapturingGroup_PropertyInfo = 
{
	&CaptureAssertion_t1973_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, NULL/* get */
	, &CaptureAssertion_set_CapturingGroup_m9178_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CaptureAssertion_get_Alternate_m9181_MethodInfo;
static const PropertyInfo CaptureAssertion_t1973____Alternate_PropertyInfo = 
{
	&CaptureAssertion_t1973_il2cpp_TypeInfo/* parent */
	, "Alternate"/* name */
	, &CaptureAssertion_get_Alternate_m9181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CaptureAssertion_t1973_PropertyInfos[] =
{
	&CaptureAssertion_t1973____CapturingGroup_PropertyInfo,
	&CaptureAssertion_t1973____Alternate_PropertyInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_Compile_m9179_MethodInfo;
extern const MethodInfo CaptureAssertion_IsComplex_m9180_MethodInfo;
static const Il2CppMethodReference CaptureAssertion_t1973_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CaptureAssertion_Compile_m9179_MethodInfo,
	&Assertion_GetWidth_m9176_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&CaptureAssertion_IsComplex_m9180_MethodInfo,
};
static bool CaptureAssertion_t1973_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureAssertion_t1973_0_0_0;
extern const Il2CppType CaptureAssertion_t1973_1_0_0;
struct CaptureAssertion_t1973;
const Il2CppTypeDefinitionMetadata CaptureAssertion_t1973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1970_0_0_0/* parent */
	, CaptureAssertion_t1973_VTable/* vtableMethods */
	, CaptureAssertion_t1973_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 731/* fieldStart */

};
TypeInfo CaptureAssertion_t1973_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CaptureAssertion_t1973_MethodInfos/* methods */
	, CaptureAssertion_t1973_PropertyInfos/* properties */
	, NULL/* events */
	, &CaptureAssertion_t1973_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CaptureAssertion_t1973_0_0_0/* byval_arg */
	, &CaptureAssertion_t1973_1_0_0/* this_arg */
	, &CaptureAssertion_t1973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureAssertion_t1973)/* instance_size */
	, sizeof (CaptureAssertion_t1973)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern TypeInfo ExpressionAssertion_t1971_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
extern const MethodInfo ExpressionAssertion__ctor_m9182_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionAssertion__ctor_m9182/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ExpressionAssertion_t1971_ExpressionAssertion_set_Reverse_m9183_ParameterInfos[] = 
{
	{"value", 0, 134218592, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Reverse_m9183_MethodInfo = 
{
	"set_Reverse"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Reverse_m9183/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ExpressionAssertion_t1971_ExpressionAssertion_set_Reverse_m9183_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ExpressionAssertion_t1971_ExpressionAssertion_set_Negate_m9184_ParameterInfos[] = 
{
	{"value", 0, 134218593, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Negate_m9184_MethodInfo = 
{
	"set_Negate"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Negate_m9184/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ExpressionAssertion_t1971_ExpressionAssertion_set_Negate_m9184_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
extern const MethodInfo ExpressionAssertion_get_TestExpression_m9185_MethodInfo = 
{
	"get_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_get_TestExpression_m9185/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo ExpressionAssertion_t1971_ExpressionAssertion_set_TestExpression_m9186_ParameterInfos[] = 
{
	{"value", 0, 134218594, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionAssertion_set_TestExpression_m9186_MethodInfo = 
{
	"set_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_set_TestExpression_m9186/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ExpressionAssertion_t1971_ExpressionAssertion_set_TestExpression_m9186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ExpressionAssertion_t1971_ExpressionAssertion_Compile_m9187_ParameterInfos[] = 
{
	{"cmp", 0, 134218595, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218596, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo ExpressionAssertion_Compile_m9187_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ExpressionAssertion_Compile_m9187/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, ExpressionAssertion_t1971_ExpressionAssertion_Compile_m9187_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
extern const MethodInfo ExpressionAssertion_IsComplex_m9188_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&ExpressionAssertion_IsComplex_m9188/* method */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionAssertion_t1971_MethodInfos[] =
{
	&ExpressionAssertion__ctor_m9182_MethodInfo,
	&ExpressionAssertion_set_Reverse_m9183_MethodInfo,
	&ExpressionAssertion_set_Negate_m9184_MethodInfo,
	&ExpressionAssertion_get_TestExpression_m9185_MethodInfo,
	&ExpressionAssertion_set_TestExpression_m9186_MethodInfo,
	&ExpressionAssertion_Compile_m9187_MethodInfo,
	&ExpressionAssertion_IsComplex_m9188_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_set_Reverse_m9183_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1971____Reverse_PropertyInfo = 
{
	&ExpressionAssertion_t1971_il2cpp_TypeInfo/* parent */
	, "Reverse"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Reverse_m9183_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_set_Negate_m9184_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1971____Negate_PropertyInfo = 
{
	&ExpressionAssertion_t1971_il2cpp_TypeInfo/* parent */
	, "Negate"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Negate_m9184_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_get_TestExpression_m9185_MethodInfo;
extern const MethodInfo ExpressionAssertion_set_TestExpression_m9186_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1971____TestExpression_PropertyInfo = 
{
	&ExpressionAssertion_t1971_il2cpp_TypeInfo/* parent */
	, "TestExpression"/* name */
	, &ExpressionAssertion_get_TestExpression_m9185_MethodInfo/* get */
	, &ExpressionAssertion_set_TestExpression_m9186_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionAssertion_t1971_PropertyInfos[] =
{
	&ExpressionAssertion_t1971____Reverse_PropertyInfo,
	&ExpressionAssertion_t1971____Negate_PropertyInfo,
	&ExpressionAssertion_t1971____TestExpression_PropertyInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_Compile_m9187_MethodInfo;
extern const MethodInfo ExpressionAssertion_IsComplex_m9188_MethodInfo;
static const Il2CppMethodReference ExpressionAssertion_t1971_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ExpressionAssertion_Compile_m9187_MethodInfo,
	&Assertion_GetWidth_m9176_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&ExpressionAssertion_IsComplex_m9188_MethodInfo,
};
static bool ExpressionAssertion_t1971_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionAssertion_t1971_1_0_0;
struct ExpressionAssertion_t1971;
const Il2CppTypeDefinitionMetadata ExpressionAssertion_t1971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1970_0_0_0/* parent */
	, ExpressionAssertion_t1971_VTable/* vtableMethods */
	, ExpressionAssertion_t1971_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 734/* fieldStart */

};
TypeInfo ExpressionAssertion_t1971_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionAssertion_t1971_MethodInfos/* methods */
	, ExpressionAssertion_t1971_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionAssertion_t1971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExpressionAssertion_t1971_0_0_0/* byval_arg */
	, &ExpressionAssertion_t1971_1_0_0/* this_arg */
	, &ExpressionAssertion_t1971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionAssertion_t1971)/* instance_size */
	, sizeof (ExpressionAssertion_t1971)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
extern TypeInfo Alternation_t1974_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::.ctor()
extern const MethodInfo Alternation__ctor_m9189_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Alternation__ctor_m9189/* method */
	, &Alternation_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.Alternation::get_Alternatives()
extern const MethodInfo Alternation_get_Alternatives_m9190_MethodInfo = 
{
	"get_Alternatives"/* name */
	, (methodPointerType)&Alternation_get_Alternatives_m9190/* method */
	, &Alternation_t1974_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1961_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
static const ParameterInfo Alternation_t1974_Alternation_AddAlternative_m9191_ParameterInfos[] = 
{
	{"e", 0, 134218597, 0, &Expression_t1962_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::AddAlternative(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Alternation_AddAlternative_m9191_MethodInfo = 
{
	"AddAlternative"/* name */
	, (methodPointerType)&Alternation_AddAlternative_m9191/* method */
	, &Alternation_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Alternation_t1974_Alternation_AddAlternative_m9191_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Alternation_t1974_Alternation_Compile_m9192_ParameterInfos[] = 
{
	{"cmp", 0, 134218598, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218599, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Alternation_Compile_m9192_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Alternation_Compile_m9192/* method */
	, &Alternation_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Alternation_t1974_Alternation_Compile_m9192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Alternation_t1974_Alternation_GetWidth_m9193_ParameterInfos[] = 
{
	{"min", 0, 134218600, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218601, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Alternation_GetWidth_m9193_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Alternation_GetWidth_m9193/* method */
	, &Alternation_t1974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Alternation_t1974_Alternation_GetWidth_m9193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Alternation_t1974_MethodInfos[] =
{
	&Alternation__ctor_m9189_MethodInfo,
	&Alternation_get_Alternatives_m9190_MethodInfo,
	&Alternation_AddAlternative_m9191_MethodInfo,
	&Alternation_Compile_m9192_MethodInfo,
	&Alternation_GetWidth_m9193_MethodInfo,
	NULL
};
extern const MethodInfo Alternation_get_Alternatives_m9190_MethodInfo;
static const PropertyInfo Alternation_t1974____Alternatives_PropertyInfo = 
{
	&Alternation_t1974_il2cpp_TypeInfo/* parent */
	, "Alternatives"/* name */
	, &Alternation_get_Alternatives_m9190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Alternation_t1974_PropertyInfos[] =
{
	&Alternation_t1974____Alternatives_PropertyInfo,
	NULL
};
extern const MethodInfo Alternation_Compile_m9192_MethodInfo;
extern const MethodInfo Alternation_GetWidth_m9193_MethodInfo;
static const Il2CppMethodReference Alternation_t1974_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Alternation_Compile_m9192_MethodInfo,
	&Alternation_GetWidth_m9193_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&CompositeExpression_IsComplex_m9140_MethodInfo,
};
static bool Alternation_t1974_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Alternation_t1974_0_0_0;
extern const Il2CppType Alternation_t1974_1_0_0;
struct Alternation_t1974;
const Il2CppTypeDefinitionMetadata Alternation_t1974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1963_0_0_0/* parent */
	, Alternation_t1974_VTable/* vtableMethods */
	, Alternation_t1974_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Alternation_t1974_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alternation"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Alternation_t1974_MethodInfos/* methods */
	, Alternation_t1974_PropertyInfos/* properties */
	, NULL/* events */
	, &Alternation_t1974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alternation_t1974_0_0_0/* byval_arg */
	, &Alternation_t1974_1_0_0/* this_arg */
	, &Alternation_t1974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alternation_t1974)/* instance_size */
	, sizeof (Alternation_t1974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern TypeInfo Literal_t1972_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Literal_t1972_Literal__ctor_m9194_ParameterInfos[] = 
{
	{"str", 0, 134218602, 0, &String_t_0_0_0},
	{"ignore", 1, 134218603, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::.ctor(System.String,System.Boolean)
extern const MethodInfo Literal__ctor_m9194_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Literal__ctor_m9194/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Literal_t1972_Literal__ctor_m9194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Literal_t1972_Literal_CompileLiteral_m9195_ParameterInfos[] = 
{
	{"str", 0, 134218604, 0, &String_t_0_0_0},
	{"cmp", 1, 134218605, 0, &ICompiler_t1997_0_0_0},
	{"ignore", 2, 134218606, 0, &Boolean_t169_0_0_0},
	{"reverse", 3, 134218607, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::CompileLiteral(System.String,System.Text.RegularExpressions.ICompiler,System.Boolean,System.Boolean)
extern const MethodInfo Literal_CompileLiteral_m9195_MethodInfo = 
{
	"CompileLiteral"/* name */
	, (methodPointerType)&Literal_CompileLiteral_m9195/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_SByte_t170/* invoker_method */
	, Literal_t1972_Literal_CompileLiteral_m9195_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Literal_t1972_Literal_Compile_m9196_ParameterInfos[] = 
{
	{"cmp", 0, 134218608, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218609, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Literal_Compile_m9196_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Literal_Compile_m9196/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Literal_t1972_Literal_Compile_m9196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Literal_t1972_Literal_GetWidth_m9197_ParameterInfos[] = 
{
	{"min", 0, 134218610, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218611, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Literal_GetWidth_m9197_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Literal_GetWidth_m9197/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Literal_t1972_Literal_GetWidth_m9197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Literal_t1972_Literal_GetAnchorInfo_m9198_ParameterInfos[] = 
{
	{"reverse", 0, 134218612, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Literal::GetAnchorInfo(System.Boolean)
extern const MethodInfo Literal_GetAnchorInfo_m9198_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Literal_GetAnchorInfo_m9198/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, Literal_t1972_Literal_GetAnchorInfo_m9198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Literal::IsComplex()
extern const MethodInfo Literal_IsComplex_m9199_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Literal_IsComplex_m9199/* method */
	, &Literal_t1972_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Literal_t1972_MethodInfos[] =
{
	&Literal__ctor_m9194_MethodInfo,
	&Literal_CompileLiteral_m9195_MethodInfo,
	&Literal_Compile_m9196_MethodInfo,
	&Literal_GetWidth_m9197_MethodInfo,
	&Literal_GetAnchorInfo_m9198_MethodInfo,
	&Literal_IsComplex_m9199_MethodInfo,
	NULL
};
extern const MethodInfo Literal_Compile_m9196_MethodInfo;
extern const MethodInfo Literal_GetWidth_m9197_MethodInfo;
extern const MethodInfo Literal_GetAnchorInfo_m9198_MethodInfo;
extern const MethodInfo Literal_IsComplex_m9199_MethodInfo;
static const Il2CppMethodReference Literal_t1972_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Literal_Compile_m9196_MethodInfo,
	&Literal_GetWidth_m9197_MethodInfo,
	&Literal_GetAnchorInfo_m9198_MethodInfo,
	&Literal_IsComplex_m9199_MethodInfo,
};
static bool Literal_t1972_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Literal_t1972_1_0_0;
struct Literal_t1972;
const Il2CppTypeDefinitionMetadata Literal_t1972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1962_0_0_0/* parent */
	, Literal_t1972_VTable/* vtableMethods */
	, Literal_t1972_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 736/* fieldStart */

};
TypeInfo Literal_t1972_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Literal"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Literal_t1972_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Literal_t1972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Literal_t1972_0_0_0/* byval_arg */
	, &Literal_t1972_1_0_0/* this_arg */
	, &Literal_t1972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Literal_t1972)/* instance_size */
	, sizeof (Literal_t1972)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern TypeInfo PositionAssertion_t1975_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
extern const Il2CppType Position_t1936_0_0_0;
static const ParameterInfo PositionAssertion_t1975_PositionAssertion__ctor_m9200_ParameterInfos[] = 
{
	{"pos", 0, 134218613, 0, &Position_t1936_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::.ctor(System.Text.RegularExpressions.Position)
extern const MethodInfo PositionAssertion__ctor_m9200_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAssertion__ctor_m9200/* method */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454/* invoker_method */
	, PositionAssertion_t1975_PositionAssertion__ctor_m9200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PositionAssertion_t1975_PositionAssertion_Compile_m9201_ParameterInfos[] = 
{
	{"cmp", 0, 134218614, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218615, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo PositionAssertion_Compile_m9201_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&PositionAssertion_Compile_m9201/* method */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, PositionAssertion_t1975_PositionAssertion_Compile_m9201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo PositionAssertion_t1975_PositionAssertion_GetWidth_m9202_ParameterInfos[] = 
{
	{"min", 0, 134218616, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218617, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo PositionAssertion_GetWidth_m9202_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&PositionAssertion_GetWidth_m9202/* method */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, PositionAssertion_t1975_PositionAssertion_GetWidth_m9202_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.PositionAssertion::IsComplex()
extern const MethodInfo PositionAssertion_IsComplex_m9203_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&PositionAssertion_IsComplex_m9203/* method */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo PositionAssertion_t1975_PositionAssertion_GetAnchorInfo_m9204_ParameterInfos[] = 
{
	{"revers", 0, 134218618, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.PositionAssertion::GetAnchorInfo(System.Boolean)
extern const MethodInfo PositionAssertion_GetAnchorInfo_m9204_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&PositionAssertion_GetAnchorInfo_m9204/* method */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t170/* invoker_method */
	, PositionAssertion_t1975_PositionAssertion_GetAnchorInfo_m9204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAssertion_t1975_MethodInfos[] =
{
	&PositionAssertion__ctor_m9200_MethodInfo,
	&PositionAssertion_Compile_m9201_MethodInfo,
	&PositionAssertion_GetWidth_m9202_MethodInfo,
	&PositionAssertion_IsComplex_m9203_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m9204_MethodInfo,
	NULL
};
extern const MethodInfo PositionAssertion_Compile_m9201_MethodInfo;
extern const MethodInfo PositionAssertion_GetWidth_m9202_MethodInfo;
extern const MethodInfo PositionAssertion_GetAnchorInfo_m9204_MethodInfo;
extern const MethodInfo PositionAssertion_IsComplex_m9203_MethodInfo;
static const Il2CppMethodReference PositionAssertion_t1975_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&PositionAssertion_Compile_m9201_MethodInfo,
	&PositionAssertion_GetWidth_m9202_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m9204_MethodInfo,
	&PositionAssertion_IsComplex_m9203_MethodInfo,
};
static bool PositionAssertion_t1975_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PositionAssertion_t1975_0_0_0;
extern const Il2CppType PositionAssertion_t1975_1_0_0;
struct PositionAssertion_t1975;
const Il2CppTypeDefinitionMetadata PositionAssertion_t1975_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1962_0_0_0/* parent */
	, PositionAssertion_t1975_VTable/* vtableMethods */
	, PositionAssertion_t1975_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 738/* fieldStart */

};
TypeInfo PositionAssertion_t1975_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, PositionAssertion_t1975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAssertion_t1975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PositionAssertion_t1975_0_0_0/* byval_arg */
	, &PositionAssertion_t1975_1_0_0/* this_arg */
	, &PositionAssertion_t1975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAssertion_t1975)/* instance_size */
	, sizeof (PositionAssertion_t1975)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern TypeInfo Reference_t1976_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Reference_t1976_Reference__ctor_m9205_ParameterInfos[] = 
{
	{"ignore", 0, 134218619, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::.ctor(System.Boolean)
extern const MethodInfo Reference__ctor_m9205_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Reference__ctor_m9205/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Reference_t1976_Reference__ctor_m9205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::get_CapturingGroup()
extern const MethodInfo Reference_get_CapturingGroup_m9206_MethodInfo = 
{
	"get_CapturingGroup"/* name */
	, (methodPointerType)&Reference_get_CapturingGroup_m9206/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &CapturingGroup_t1966_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1966_0_0_0;
static const ParameterInfo Reference_t1976_Reference_set_CapturingGroup_m9207_ParameterInfos[] = 
{
	{"value", 0, 134218620, 0, &CapturingGroup_t1966_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo Reference_set_CapturingGroup_m9207_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&Reference_set_CapturingGroup_m9207/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Reference_t1976_Reference_set_CapturingGroup_m9207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::get_IgnoreCase()
extern const MethodInfo Reference_get_IgnoreCase_m9208_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&Reference_get_IgnoreCase_m9208/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Reference_t1976_Reference_Compile_m9209_ParameterInfos[] = 
{
	{"cmp", 0, 134218621, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218622, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Reference_Compile_m9209_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Reference_Compile_m9209/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Reference_t1976_Reference_Compile_m9209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo Reference_t1976_Reference_GetWidth_m9210_ParameterInfos[] = 
{
	{"min", 0, 134218623, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218624, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Reference_GetWidth_m9210_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Reference_GetWidth_m9210/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, Reference_t1976_Reference_GetWidth_m9210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::IsComplex()
extern const MethodInfo Reference_IsComplex_m9211_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Reference_IsComplex_m9211/* method */
	, &Reference_t1976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Reference_t1976_MethodInfos[] =
{
	&Reference__ctor_m9205_MethodInfo,
	&Reference_get_CapturingGroup_m9206_MethodInfo,
	&Reference_set_CapturingGroup_m9207_MethodInfo,
	&Reference_get_IgnoreCase_m9208_MethodInfo,
	&Reference_Compile_m9209_MethodInfo,
	&Reference_GetWidth_m9210_MethodInfo,
	&Reference_IsComplex_m9211_MethodInfo,
	NULL
};
extern const MethodInfo Reference_get_CapturingGroup_m9206_MethodInfo;
extern const MethodInfo Reference_set_CapturingGroup_m9207_MethodInfo;
static const PropertyInfo Reference_t1976____CapturingGroup_PropertyInfo = 
{
	&Reference_t1976_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, &Reference_get_CapturingGroup_m9206_MethodInfo/* get */
	, &Reference_set_CapturingGroup_m9207_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Reference_get_IgnoreCase_m9208_MethodInfo;
static const PropertyInfo Reference_t1976____IgnoreCase_PropertyInfo = 
{
	&Reference_t1976_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &Reference_get_IgnoreCase_m9208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Reference_t1976_PropertyInfos[] =
{
	&Reference_t1976____CapturingGroup_PropertyInfo,
	&Reference_t1976____IgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo Reference_Compile_m9209_MethodInfo;
extern const MethodInfo Reference_GetWidth_m9210_MethodInfo;
extern const MethodInfo Reference_IsComplex_m9211_MethodInfo;
static const Il2CppMethodReference Reference_t1976_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Reference_Compile_m9209_MethodInfo,
	&Reference_GetWidth_m9210_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&Reference_IsComplex_m9211_MethodInfo,
};
static bool Reference_t1976_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Reference_t1976_0_0_0;
extern const Il2CppType Reference_t1976_1_0_0;
struct Reference_t1976;
const Il2CppTypeDefinitionMetadata Reference_t1976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1962_0_0_0/* parent */
	, Reference_t1976_VTable/* vtableMethods */
	, Reference_t1976_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 739/* fieldStart */

};
TypeInfo Reference_t1976_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reference"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Reference_t1976_MethodInfos/* methods */
	, Reference_t1976_PropertyInfos/* properties */
	, NULL/* events */
	, &Reference_t1976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reference_t1976_0_0_0/* byval_arg */
	, &Reference_t1976_1_0_0/* this_arg */
	, &Reference_t1976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reference_t1976)/* instance_size */
	, sizeof (Reference_t1976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern TypeInfo BackslashNumber_t1977_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo BackslashNumber_t1977_BackslashNumber__ctor_m9212_ParameterInfos[] = 
{
	{"ignore", 0, 134218625, 0, &Boolean_t169_0_0_0},
	{"ecma", 1, 134218626, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo BackslashNumber__ctor_m9212_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackslashNumber__ctor_m9212/* method */
	, &BackslashNumber_t1977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170_SByte_t170/* invoker_method */
	, BackslashNumber_t1977_BackslashNumber__ctor_m9212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Hashtable_t1736_0_0_0;
static const ParameterInfo BackslashNumber_t1977_BackslashNumber_ResolveReference_m9213_ParameterInfos[] = 
{
	{"num_str", 0, 134218627, 0, &String_t_0_0_0},
	{"groups", 1, 134218628, 0, &Hashtable_t1736_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
extern const MethodInfo BackslashNumber_ResolveReference_m9213_MethodInfo = 
{
	"ResolveReference"/* name */
	, (methodPointerType)&BackslashNumber_ResolveReference_m9213/* method */
	, &BackslashNumber_t1977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, BackslashNumber_t1977_BackslashNumber_ResolveReference_m9213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo BackslashNumber_t1977_BackslashNumber_Compile_m9214_ParameterInfos[] = 
{
	{"cmp", 0, 134218629, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218630, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BackslashNumber_Compile_m9214_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BackslashNumber_Compile_m9214/* method */
	, &BackslashNumber_t1977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, BackslashNumber_t1977_BackslashNumber_Compile_m9214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BackslashNumber_t1977_MethodInfos[] =
{
	&BackslashNumber__ctor_m9212_MethodInfo,
	&BackslashNumber_ResolveReference_m9213_MethodInfo,
	&BackslashNumber_Compile_m9214_MethodInfo,
	NULL
};
extern const MethodInfo BackslashNumber_Compile_m9214_MethodInfo;
static const Il2CppMethodReference BackslashNumber_t1977_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&BackslashNumber_Compile_m9214_MethodInfo,
	&Reference_GetWidth_m9210_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&Reference_IsComplex_m9211_MethodInfo,
};
static bool BackslashNumber_t1977_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BackslashNumber_t1977_0_0_0;
extern const Il2CppType BackslashNumber_t1977_1_0_0;
struct BackslashNumber_t1977;
const Il2CppTypeDefinitionMetadata BackslashNumber_t1977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reference_t1976_0_0_0/* parent */
	, BackslashNumber_t1977_VTable/* vtableMethods */
	, BackslashNumber_t1977_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 741/* fieldStart */

};
TypeInfo BackslashNumber_t1977_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackslashNumber"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BackslashNumber_t1977_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BackslashNumber_t1977_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackslashNumber_t1977_0_0_0/* byval_arg */
	, &BackslashNumber_t1977_1_0_0/* this_arg */
	, &BackslashNumber_t1977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackslashNumber_t1977)/* instance_size */
	, sizeof (BackslashNumber_t1977)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern TypeInfo CharacterClass_t1979_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass__ctor_m9215_ParameterInfos[] = 
{
	{"negate", 0, 134218631, 0, &Boolean_t169_0_0_0},
	{"ignore", 1, 134218632, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m9215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m9215/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170_SByte_t170/* invoker_method */
	, CharacterClass_t1979_CharacterClass__ctor_m9215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass__ctor_m9216_ParameterInfos[] = 
{
	{"cat", 0, 134218633, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218634, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m9216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m9216/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170/* invoker_method */
	, CharacterClass_t1979_CharacterClass__ctor_m9216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.cctor()
extern const MethodInfo CharacterClass__cctor_m9217_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CharacterClass__cctor_m9217/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1940_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass_AddCategory_m9218_ParameterInfos[] = 
{
	{"cat", 0, 134218635, 0, &Category_t1940_0_0_0},
	{"negate", 1, 134218636, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCategory(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass_AddCategory_m9218_MethodInfo = 
{
	"AddCategory"/* name */
	, (methodPointerType)&CharacterClass_AddCategory_m9218/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UInt16_t454_SByte_t170/* invoker_method */
	, CharacterClass_t1979_CharacterClass_AddCategory_m9218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass_AddCharacter_m9219_ParameterInfos[] = 
{
	{"c", 0, 134218637, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCharacter(System.Char)
extern const MethodInfo CharacterClass_AddCharacter_m9219_MethodInfo = 
{
	"AddCharacter"/* name */
	, (methodPointerType)&CharacterClass_AddCharacter_m9219/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534/* invoker_method */
	, CharacterClass_t1979_CharacterClass_AddCharacter_m9219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass_AddRange_m9220_ParameterInfos[] = 
{
	{"lo", 0, 134218638, 0, &Char_t451_0_0_0},
	{"hi", 1, 134218639, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int16_t534_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddRange(System.Char,System.Char)
extern const MethodInfo CharacterClass_AddRange_m9220_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&CharacterClass_AddRange_m9220/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int16_t534_Int16_t534/* invoker_method */
	, CharacterClass_t1979_CharacterClass_AddRange_m9220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1997_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass_Compile_m9221_ParameterInfos[] = 
{
	{"cmp", 0, 134218640, 0, &ICompiler_t1997_0_0_0},
	{"reverse", 1, 134218641, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CharacterClass_Compile_m9221_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CharacterClass_Compile_m9221/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, CharacterClass_t1979_CharacterClass_Compile_m9221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_1_0_2;
extern const Il2CppType Int32_t127_1_0_2;
static const ParameterInfo CharacterClass_t1979_CharacterClass_GetWidth_m9222_ParameterInfos[] = 
{
	{"min", 0, 134218642, 0, &Int32_t127_1_0_2},
	{"max", 1, 134218643, 0, &Int32_t127_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo CharacterClass_GetWidth_m9222_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CharacterClass_GetWidth_m9222/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32U26_t535_Int32U26_t535/* invoker_method */
	, CharacterClass_t1979_CharacterClass_GetWidth_m9222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::IsComplex()
extern const MethodInfo CharacterClass_IsComplex_m9223_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CharacterClass_IsComplex_m9223/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1955_0_0_0;
static const ParameterInfo CharacterClass_t1979_CharacterClass_GetIntervalCost_m9224_ParameterInfos[] = 
{
	{"i", 0, 134218644, 0, &Interval_t1955_0_0_0},
};
extern void* RuntimeInvoker_Double_t1407_Interval_t1955 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.Syntax.CharacterClass::GetIntervalCost(System.Text.RegularExpressions.Interval)
extern const MethodInfo CharacterClass_GetIntervalCost_m9224_MethodInfo = 
{
	"GetIntervalCost"/* name */
	, (methodPointerType)&CharacterClass_GetIntervalCost_m9224/* method */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407_Interval_t1955/* invoker_method */
	, CharacterClass_t1979_CharacterClass_GetIntervalCost_m9224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CharacterClass_t1979_MethodInfos[] =
{
	&CharacterClass__ctor_m9215_MethodInfo,
	&CharacterClass__ctor_m9216_MethodInfo,
	&CharacterClass__cctor_m9217_MethodInfo,
	&CharacterClass_AddCategory_m9218_MethodInfo,
	&CharacterClass_AddCharacter_m9219_MethodInfo,
	&CharacterClass_AddRange_m9220_MethodInfo,
	&CharacterClass_Compile_m9221_MethodInfo,
	&CharacterClass_GetWidth_m9222_MethodInfo,
	&CharacterClass_IsComplex_m9223_MethodInfo,
	&CharacterClass_GetIntervalCost_m9224_MethodInfo,
	NULL
};
extern const MethodInfo CharacterClass_Compile_m9221_MethodInfo;
extern const MethodInfo CharacterClass_GetWidth_m9222_MethodInfo;
extern const MethodInfo CharacterClass_IsComplex_m9223_MethodInfo;
static const Il2CppMethodReference CharacterClass_t1979_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CharacterClass_Compile_m9221_MethodInfo,
	&CharacterClass_GetWidth_m9222_MethodInfo,
	&Expression_GetAnchorInfo_m9136_MethodInfo,
	&CharacterClass_IsComplex_m9223_MethodInfo,
};
static bool CharacterClass_t1979_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CharacterClass_t1979_0_0_0;
extern const Il2CppType CharacterClass_t1979_1_0_0;
struct CharacterClass_t1979;
const Il2CppTypeDefinitionMetadata CharacterClass_t1979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1962_0_0_0/* parent */
	, CharacterClass_t1979_VTable/* vtableMethods */
	, CharacterClass_t1979_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 743/* fieldStart */

};
TypeInfo CharacterClass_t1979_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterClass"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CharacterClass_t1979_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CharacterClass_t1979_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterClass_t1979_0_0_0/* byval_arg */
	, &CharacterClass_t1979_1_0_0/* this_arg */
	, &CharacterClass_t1979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterClass_t1979)/* instance_size */
	, sizeof (CharacterClass_t1979)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CharacterClass_t1979_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern TypeInfo AnchorInfo_t1980_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
extern const Il2CppType Expression_t1962_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AnchorInfo_t1980_AnchorInfo__ctor_m9225_ParameterInfos[] = 
{
	{"expr", 0, 134218645, 0, &Expression_t1962_0_0_0},
	{"width", 1, 134218646, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32)
extern const MethodInfo AnchorInfo__ctor_m9225_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m9225/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, AnchorInfo_t1980_AnchorInfo__ctor_m9225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo AnchorInfo_t1980_AnchorInfo__ctor_m9226_ParameterInfos[] = 
{
	{"expr", 0, 134218647, 0, &Expression_t1962_0_0_0},
	{"offset", 1, 134218648, 0, &Int32_t127_0_0_0},
	{"width", 2, 134218649, 0, &Int32_t127_0_0_0},
	{"str", 3, 134218650, 0, &String_t_0_0_0},
	{"ignore", 4, 134218651, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.String,System.Boolean)
extern const MethodInfo AnchorInfo__ctor_m9226_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m9226/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_Object_t_SByte_t170/* invoker_method */
	, AnchorInfo_t1980_AnchorInfo__ctor_m9226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1962_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Position_t1936_0_0_0;
static const ParameterInfo AnchorInfo_t1980_AnchorInfo__ctor_m9227_ParameterInfos[] = 
{
	{"expr", 0, 134218652, 0, &Expression_t1962_0_0_0},
	{"offset", 1, 134218653, 0, &Int32_t127_0_0_0},
	{"width", 2, 134218654, 0, &Int32_t127_0_0_0},
	{"pos", 3, 134218655, 0, &Position_t1936_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_UInt16_t454 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.Text.RegularExpressions.Position)
extern const MethodInfo AnchorInfo__ctor_m9227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m9227/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Int32_t127_UInt16_t454/* invoker_method */
	, AnchorInfo_t1980_AnchorInfo__ctor_m9227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Offset()
extern const MethodInfo AnchorInfo_get_Offset_m9228_MethodInfo = 
{
	"get_Offset"/* name */
	, (methodPointerType)&AnchorInfo_get_Offset_m9228/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Width()
extern const MethodInfo AnchorInfo_get_Width_m9229_MethodInfo = 
{
	"get_Width"/* name */
	, (methodPointerType)&AnchorInfo_get_Width_m9229/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Length()
extern const MethodInfo AnchorInfo_get_Length_m9230_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&AnchorInfo_get_Length_m9230/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsUnknownWidth()
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m9231_MethodInfo = 
{
	"get_IsUnknownWidth"/* name */
	, (methodPointerType)&AnchorInfo_get_IsUnknownWidth_m9231/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsComplete()
extern const MethodInfo AnchorInfo_get_IsComplete_m9232_MethodInfo = 
{
	"get_IsComplete"/* name */
	, (methodPointerType)&AnchorInfo_get_IsComplete_m9232/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::get_Substring()
extern const MethodInfo AnchorInfo_get_Substring_m9233_MethodInfo = 
{
	"get_Substring"/* name */
	, (methodPointerType)&AnchorInfo_get_Substring_m9233/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IgnoreCase()
extern const MethodInfo AnchorInfo_get_IgnoreCase_m9234_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&AnchorInfo_get_IgnoreCase_m9234/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Position_t1936 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::get_Position()
extern const MethodInfo AnchorInfo_get_Position_m9235_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&AnchorInfo_get_Position_m9235/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Position_t1936_0_0_0/* return_type */
	, RuntimeInvoker_Position_t1936/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsSubstring()
extern const MethodInfo AnchorInfo_get_IsSubstring_m9236_MethodInfo = 
{
	"get_IsSubstring"/* name */
	, (methodPointerType)&AnchorInfo_get_IsSubstring_m9236/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsPosition()
extern const MethodInfo AnchorInfo_get_IsPosition_m9237_MethodInfo = 
{
	"get_IsPosition"/* name */
	, (methodPointerType)&AnchorInfo_get_IsPosition_m9237/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AnchorInfo_t1980_AnchorInfo_GetInterval_m9238_ParameterInfos[] = 
{
	{"start", 0, 134218656, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1955_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.AnchorInfo::GetInterval(System.Int32)
extern const MethodInfo AnchorInfo_GetInterval_m9238_MethodInfo = 
{
	"GetInterval"/* name */
	, (methodPointerType)&AnchorInfo_GetInterval_m9238/* method */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1955_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1955_Int32_t127/* invoker_method */
	, AnchorInfo_t1980_AnchorInfo_GetInterval_m9238_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AnchorInfo_t1980_MethodInfos[] =
{
	&AnchorInfo__ctor_m9225_MethodInfo,
	&AnchorInfo__ctor_m9226_MethodInfo,
	&AnchorInfo__ctor_m9227_MethodInfo,
	&AnchorInfo_get_Offset_m9228_MethodInfo,
	&AnchorInfo_get_Width_m9229_MethodInfo,
	&AnchorInfo_get_Length_m9230_MethodInfo,
	&AnchorInfo_get_IsUnknownWidth_m9231_MethodInfo,
	&AnchorInfo_get_IsComplete_m9232_MethodInfo,
	&AnchorInfo_get_Substring_m9233_MethodInfo,
	&AnchorInfo_get_IgnoreCase_m9234_MethodInfo,
	&AnchorInfo_get_Position_m9235_MethodInfo,
	&AnchorInfo_get_IsSubstring_m9236_MethodInfo,
	&AnchorInfo_get_IsPosition_m9237_MethodInfo,
	&AnchorInfo_GetInterval_m9238_MethodInfo,
	NULL
};
extern const MethodInfo AnchorInfo_get_Offset_m9228_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____Offset_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "Offset"/* name */
	, &AnchorInfo_get_Offset_m9228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Width_m9229_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____Width_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "Width"/* name */
	, &AnchorInfo_get_Width_m9229_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Length_m9230_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____Length_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &AnchorInfo_get_Length_m9230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m9231_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____IsUnknownWidth_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "IsUnknownWidth"/* name */
	, &AnchorInfo_get_IsUnknownWidth_m9231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsComplete_m9232_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____IsComplete_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "IsComplete"/* name */
	, &AnchorInfo_get_IsComplete_m9232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Substring_m9233_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____Substring_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "Substring"/* name */
	, &AnchorInfo_get_Substring_m9233_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IgnoreCase_m9234_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____IgnoreCase_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &AnchorInfo_get_IgnoreCase_m9234_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Position_m9235_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____Position_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &AnchorInfo_get_Position_m9235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsSubstring_m9236_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____IsSubstring_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "IsSubstring"/* name */
	, &AnchorInfo_get_IsSubstring_m9236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsPosition_m9237_MethodInfo;
static const PropertyInfo AnchorInfo_t1980____IsPosition_PropertyInfo = 
{
	&AnchorInfo_t1980_il2cpp_TypeInfo/* parent */
	, "IsPosition"/* name */
	, &AnchorInfo_get_IsPosition_m9237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AnchorInfo_t1980_PropertyInfos[] =
{
	&AnchorInfo_t1980____Offset_PropertyInfo,
	&AnchorInfo_t1980____Width_PropertyInfo,
	&AnchorInfo_t1980____Length_PropertyInfo,
	&AnchorInfo_t1980____IsUnknownWidth_PropertyInfo,
	&AnchorInfo_t1980____IsComplete_PropertyInfo,
	&AnchorInfo_t1980____Substring_PropertyInfo,
	&AnchorInfo_t1980____IgnoreCase_PropertyInfo,
	&AnchorInfo_t1980____Position_PropertyInfo,
	&AnchorInfo_t1980____IsSubstring_PropertyInfo,
	&AnchorInfo_t1980____IsPosition_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AnchorInfo_t1980_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AnchorInfo_t1980_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AnchorInfo_t1980_1_0_0;
struct AnchorInfo_t1980;
const Il2CppTypeDefinitionMetadata AnchorInfo_t1980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnchorInfo_t1980_VTable/* vtableMethods */
	, AnchorInfo_t1980_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 749/* fieldStart */

};
TypeInfo AnchorInfo_t1980_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnchorInfo"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, AnchorInfo_t1980_MethodInfos/* methods */
	, AnchorInfo_t1980_PropertyInfos/* properties */
	, NULL/* events */
	, &AnchorInfo_t1980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnchorInfo_t1980_0_0_0/* byval_arg */
	, &AnchorInfo_t1980_1_0_0/* this_arg */
	, &AnchorInfo_t1980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnchorInfo_t1980)/* instance_size */
	, sizeof (AnchorInfo_t1980)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 10/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// Metadata Definition System.DefaultUriParser
extern TypeInfo DefaultUriParser_t1981_il2cpp_TypeInfo;
// System.DefaultUriParser
#include "System_System_DefaultUriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor()
extern const MethodInfo DefaultUriParser__ctor_m9239_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m9239/* method */
	, &DefaultUriParser_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultUriParser_t1981_DefaultUriParser__ctor_m9240_ParameterInfos[] = 
{
	{"scheme", 0, 134218657, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor(System.String)
extern const MethodInfo DefaultUriParser__ctor_m9240_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m9240/* method */
	, &DefaultUriParser_t1981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, DefaultUriParser_t1981_DefaultUriParser__ctor_m9240_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultUriParser_t1981_MethodInfos[] =
{
	&DefaultUriParser__ctor_m9239_MethodInfo,
	&DefaultUriParser__ctor_m9240_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_InitializeAndValidate_m9295_MethodInfo;
extern const MethodInfo UriParser_OnRegister_m9296_MethodInfo;
static const Il2CppMethodReference DefaultUriParser_t1981_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UriParser_InitializeAndValidate_m9295_MethodInfo,
	&UriParser_OnRegister_m9296_MethodInfo,
};
static bool DefaultUriParser_t1981_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultUriParser_t1981_0_0_0;
extern const Il2CppType DefaultUriParser_t1981_1_0_0;
extern const Il2CppType UriParser_t1982_0_0_0;
struct DefaultUriParser_t1981;
const Il2CppTypeDefinitionMetadata DefaultUriParser_t1981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1982_0_0_0/* parent */
	, DefaultUriParser_t1981_VTable/* vtableMethods */
	, DefaultUriParser_t1981_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DefaultUriParser_t1981_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultUriParser"/* name */
	, "System"/* namespaze */
	, DefaultUriParser_t1981_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultUriParser_t1981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultUriParser_t1981_0_0_0/* byval_arg */
	, &DefaultUriParser_t1981_1_0_0/* this_arg */
	, &DefaultUriParser_t1981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultUriParser_t1981)/* instance_size */
	, sizeof (DefaultUriParser_t1981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// Metadata Definition System.GenericUriParser
extern TypeInfo GenericUriParser_t1983_il2cpp_TypeInfo;
// System.GenericUriParser
#include "System_System_GenericUriParserMethodDeclarations.h"
static const MethodInfo* GenericUriParser_t1983_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference GenericUriParser_t1983_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UriParser_InitializeAndValidate_m9295_MethodInfo,
	&UriParser_OnRegister_m9296_MethodInfo,
};
static bool GenericUriParser_t1983_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GenericUriParser_t1983_0_0_0;
extern const Il2CppType GenericUriParser_t1983_1_0_0;
struct GenericUriParser_t1983;
const Il2CppTypeDefinitionMetadata GenericUriParser_t1983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1982_0_0_0/* parent */
	, GenericUriParser_t1983_VTable/* vtableMethods */
	, GenericUriParser_t1983_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericUriParser_t1983_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericUriParser"/* name */
	, "System"/* namespaze */
	, GenericUriParser_t1983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericUriParser_t1983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericUriParser_t1983_0_0_0/* byval_arg */
	, &GenericUriParser_t1983_1_0_0/* this_arg */
	, &GenericUriParser_t1983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericUriParser_t1983)/* instance_size */
	, sizeof (GenericUriParser_t1983)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// Metadata Definition System.Uri/UriScheme
extern TypeInfo UriScheme_t1984_il2cpp_TypeInfo;
// System.Uri/UriScheme
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo UriScheme_t1984_UriScheme__ctor_m9241_ParameterInfos[] = 
{
	{"s", 0, 134218710, 0, &String_t_0_0_0},
	{"d", 1, 134218711, 0, &String_t_0_0_0},
	{"p", 2, 134218712, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
extern const MethodInfo UriScheme__ctor_m9241_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriScheme__ctor_m9241/* method */
	, &UriScheme_t1984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Int32_t127/* invoker_method */
	, UriScheme_t1984_UriScheme__ctor_m9241_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriScheme_t1984_MethodInfos[] =
{
	&UriScheme__ctor_m9241_MethodInfo,
	NULL
};
static const Il2CppMethodReference UriScheme_t1984_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool UriScheme_t1984_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriScheme_t1984_0_0_0;
extern const Il2CppType UriScheme_t1984_1_0_0;
extern TypeInfo Uri_t1273_il2cpp_TypeInfo;
extern const Il2CppType Uri_t1273_0_0_0;
const Il2CppTypeDefinitionMetadata UriScheme_t1984_DefinitionMetadata = 
{
	&Uri_t1273_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, UriScheme_t1984_VTable/* vtableMethods */
	, UriScheme_t1984_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 755/* fieldStart */

};
TypeInfo UriScheme_t1984_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriScheme"/* name */
	, ""/* namespaze */
	, UriScheme_t1984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriScheme_t1984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriScheme_t1984_0_0_0/* byval_arg */
	, &UriScheme_t1984_1_0_0/* this_arg */
	, &UriScheme_t1984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)UriScheme_t1984_marshal/* marshal_to_native_func */
	, (methodPointerType)UriScheme_t1984_marshal_back/* marshal_from_native_func */
	, (methodPointerType)UriScheme_t1984_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (UriScheme_t1984)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriScheme_t1984)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UriScheme_t1984_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri
#include "System_System_Uri.h"
// Metadata Definition System.Uri
// System.Uri
#include "System_System_UriMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri__ctor_m6969_ParameterInfos[] = 
{
	{"uriString", 0, 134218658, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String)
extern const MethodInfo Uri__ctor_m6969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m6969/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Uri_t1273_Uri__ctor_m6969_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo Uri_t1273_Uri__ctor_m9242_ParameterInfos[] = 
{
	{"serializationInfo", 0, 134218659, 0, &SerializationInfo_t1382_0_0_0},
	{"streamingContext", 1, 134218660, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri__ctor_m9242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m9242/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, Uri_t1273_Uri__ctor_m9242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Uri_t1273_Uri__ctor_m9243_ParameterInfos[] = 
{
	{"uriString", 0, 134218661, 0, &String_t_0_0_0},
	{"dontEscape", 1, 134218662, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String,System.Boolean)
extern const MethodInfo Uri__ctor_m9243_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m9243/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Uri_t1273_Uri__ctor_m9243_ParameterInfos/* parameters */
	, 72/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t1273_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri__ctor_m6971_ParameterInfos[] = 
{
	{"baseUri", 0, 134218663, 0, &Uri_t1273_0_0_0},
	{"relativeUri", 1, 134218664, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Uri,System.String)
extern const MethodInfo Uri__ctor_m6971_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m6971/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri__ctor_m6971_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.cctor()
extern const MethodInfo Uri__cctor_m9244_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Uri__cctor_m9244/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo Uri_t1273_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_ParameterInfos[] = 
{
	{"info", 0, 134218665, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134218666, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, Uri_t1273_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t1273_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Merge_m9246_ParameterInfos[] = 
{
	{"baseUri", 0, 134218667, 0, &Uri_t1273_0_0_0},
	{"relativeUri", 1, 134218668, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Merge(System.Uri,System.String)
extern const MethodInfo Uri_Merge_m9246_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Uri_Merge_m9246/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_Merge_m9246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_AbsoluteUri()
extern const MethodInfo Uri_get_AbsoluteUri_m9247_MethodInfo = 
{
	"get_AbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_AbsoluteUri_m9247/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Authority()
extern const MethodInfo Uri_get_Authority_m9248_MethodInfo = 
{
	"get_Authority"/* name */
	, (methodPointerType)&Uri_get_Authority_m9248/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Host()
extern const MethodInfo Uri_get_Host_m8321_MethodInfo = 
{
	"get_Host"/* name */
	, (methodPointerType)&Uri_get_Host_m8321/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsFile()
extern const MethodInfo Uri_get_IsFile_m9249_MethodInfo = 
{
	"get_IsFile"/* name */
	, (methodPointerType)&Uri_get_IsFile_m9249/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsLoopback()
extern const MethodInfo Uri_get_IsLoopback_m9250_MethodInfo = 
{
	"get_IsLoopback"/* name */
	, (methodPointerType)&Uri_get_IsLoopback_m9250/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsUnc()
extern const MethodInfo Uri_get_IsUnc_m9251_MethodInfo = 
{
	"get_IsUnc"/* name */
	, (methodPointerType)&Uri_get_IsUnc_m9251/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Scheme()
extern const MethodInfo Uri_get_Scheme_m9252_MethodInfo = 
{
	"get_Scheme"/* name */
	, (methodPointerType)&Uri_get_Scheme_m9252/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsAbsoluteUri()
extern const MethodInfo Uri_get_IsAbsoluteUri_m9253_MethodInfo = 
{
	"get_IsAbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_IsAbsoluteUri_m9253/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_CheckHostName_m9254_ParameterInfos[] = 
{
	{"name", 0, 134218669, 0, &String_t_0_0_0},
};
extern const Il2CppType UriHostNameType_t1987_0_0_0;
extern void* RuntimeInvoker_UriHostNameType_t1987_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriHostNameType System.Uri::CheckHostName(System.String)
extern const MethodInfo Uri_CheckHostName_m9254_MethodInfo = 
{
	"CheckHostName"/* name */
	, (methodPointerType)&Uri_CheckHostName_m9254/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &UriHostNameType_t1987_0_0_0/* return_type */
	, RuntimeInvoker_UriHostNameType_t1987_Object_t/* invoker_method */
	, Uri_t1273_Uri_CheckHostName_m9254_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsIPv4Address_m9255_ParameterInfos[] = 
{
	{"name", 0, 134218670, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsIPv4Address(System.String)
extern const MethodInfo Uri_IsIPv4Address_m9255_MethodInfo = 
{
	"IsIPv4Address"/* name */
	, (methodPointerType)&Uri_IsIPv4Address_m9255/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_IsIPv4Address_m9255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsDomainAddress_m9256_ParameterInfos[] = 
{
	{"name", 0, 134218671, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsDomainAddress(System.String)
extern const MethodInfo Uri_IsDomainAddress_m9256_MethodInfo = 
{
	"IsDomainAddress"/* name */
	, (methodPointerType)&Uri_IsDomainAddress_m9256/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_IsDomainAddress_m9256_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_CheckSchemeName_m9257_ParameterInfos[] = 
{
	{"schemeName", 0, 134218672, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CheckSchemeName(System.String)
extern const MethodInfo Uri_CheckSchemeName_m9257_MethodInfo = 
{
	"CheckSchemeName"/* name */
	, (methodPointerType)&Uri_CheckSchemeName_m9257/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_CheckSchemeName_m9257_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsAlpha_m9258_ParameterInfos[] = 
{
	{"c", 0, 134218673, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsAlpha(System.Char)
extern const MethodInfo Uri_IsAlpha_m9258_MethodInfo = 
{
	"IsAlpha"/* name */
	, (methodPointerType)&Uri_IsAlpha_m9258/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int16_t534/* invoker_method */
	, Uri_t1273_Uri_IsAlpha_m9258_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Equals_m9259_ParameterInfos[] = 
{
	{"comparant", 0, 134218674, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::Equals(System.Object)
extern const MethodInfo Uri_Equals_m9259_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Uri_Equals_m9259/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_Equals_m9259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t1273_0_0_0;
static const ParameterInfo Uri_t1273_Uri_InternalEquals_m9260_ParameterInfos[] = 
{
	{"uri", 0, 134218675, 0, &Uri_t1273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::InternalEquals(System.Uri)
extern const MethodInfo Uri_InternalEquals_m9260_MethodInfo = 
{
	"InternalEquals"/* name */
	, (methodPointerType)&Uri_InternalEquals_m9260/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_InternalEquals_m9260_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetHashCode()
extern const MethodInfo Uri_GetHashCode_m9261_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Uri_GetHashCode_m9261/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriPartial_t1989_0_0_0;
extern const Il2CppType UriPartial_t1989_0_0_0;
static const ParameterInfo Uri_t1273_Uri_GetLeftPart_m9262_ParameterInfos[] = 
{
	{"part", 0, 134218676, 0, &UriPartial_t1989_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetLeftPart(System.UriPartial)
extern const MethodInfo Uri_GetLeftPart_m9262_MethodInfo = 
{
	"GetLeftPart"/* name */
	, (methodPointerType)&Uri_GetLeftPart_m9262/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127/* invoker_method */
	, Uri_t1273_Uri_GetLeftPart_m9262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Uri_t1273_Uri_FromHex_m9263_ParameterInfos[] = 
{
	{"digit", 0, 134218677, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::FromHex(System.Char)
extern const MethodInfo Uri_FromHex_m9263_MethodInfo = 
{
	"FromHex"/* name */
	, (methodPointerType)&Uri_FromHex_m9263/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Int16_t534/* invoker_method */
	, Uri_t1273_Uri_FromHex_m9263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Uri_t1273_Uri_HexEscape_m9264_ParameterInfos[] = 
{
	{"character", 0, 134218678, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::HexEscape(System.Char)
extern const MethodInfo Uri_HexEscape_m9264_MethodInfo = 
{
	"HexEscape"/* name */
	, (methodPointerType)&Uri_HexEscape_m9264/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int16_t534/* invoker_method */
	, Uri_t1273_Uri_HexEscape_m9264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t451_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsHexDigit_m9265_ParameterInfos[] = 
{
	{"digit", 0, 134218679, 0, &Char_t451_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Int16_t534 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexDigit(System.Char)
extern const MethodInfo Uri_IsHexDigit_m9265_MethodInfo = 
{
	"IsHexDigit"/* name */
	, (methodPointerType)&Uri_IsHexDigit_m9265/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Int16_t534/* invoker_method */
	, Uri_t1273_Uri_IsHexDigit_m9265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsHexEncoding_m9266_ParameterInfos[] = 
{
	{"pattern", 0, 134218680, 0, &String_t_0_0_0},
	{"index", 1, 134218681, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexEncoding(System.String,System.Int32)
extern const MethodInfo Uri_IsHexEncoding_m9266_MethodInfo = 
{
	"IsHexEncoding"/* name */
	, (methodPointerType)&Uri_IsHexEncoding_m9266/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Int32_t127/* invoker_method */
	, Uri_t1273_Uri_IsHexEncoding_m9266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_0;
static const ParameterInfo Uri_t1273_Uri_AppendQueryAndFragment_m9267_ParameterInfos[] = 
{
	{"result", 0, 134218682, 0, &String_t_1_0_0},
};
extern void* RuntimeInvoker_Void_t168_StringU26_t1131 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::AppendQueryAndFragment(System.String&)
extern const MethodInfo Uri_AppendQueryAndFragment_m9267_MethodInfo = 
{
	"AppendQueryAndFragment"/* name */
	, (methodPointerType)&Uri_AppendQueryAndFragment_m9267/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_StringU26_t1131/* invoker_method */
	, Uri_t1273_Uri_AppendQueryAndFragment_m9267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ToString()
extern const MethodInfo Uri_ToString_m9268_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Uri_ToString_m9268/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_EscapeString_m9269_ParameterInfos[] = 
{
	{"str", 0, 134218683, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String)
extern const MethodInfo Uri_EscapeString_m9269_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m9269/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_EscapeString_m9269_ParameterInfos/* parameters */
	, 73/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Uri_t1273_Uri_EscapeString_m9270_ParameterInfos[] = 
{
	{"str", 0, 134218684, 0, &String_t_0_0_0},
	{"escapeReserved", 1, 134218685, 0, &Boolean_t169_0_0_0},
	{"escapeHex", 2, 134218686, 0, &Boolean_t169_0_0_0},
	{"escapeBrackets", 3, 134218687, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Uri_EscapeString_m9270_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m9270/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170_SByte_t170_SByte_t170/* invoker_method */
	, Uri_t1273_Uri_EscapeString_m9270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1988_0_0_0;
extern const Il2CppType UriKind_t1988_0_0_0;
static const ParameterInfo Uri_t1273_Uri_ParseUri_m9271_ParameterInfos[] = 
{
	{"kind", 0, 134218688, 0, &UriKind_t1988_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseUri(System.UriKind)
extern const MethodInfo Uri_ParseUri_m9271_MethodInfo = 
{
	"ParseUri"/* name */
	, (methodPointerType)&Uri_ParseUri_m9271/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Uri_t1273_Uri_ParseUri_m9271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Unescape_m9272_ParameterInfos[] = 
{
	{"str", 0, 134218689, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String)
extern const MethodInfo Uri_Unescape_m9272_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m9272/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_Unescape_m9272_ParameterInfos/* parameters */
	, 74/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Unescape_m9273_ParameterInfos[] = 
{
	{"str", 0, 134218690, 0, &String_t_0_0_0},
	{"excludeSpecial", 1, 134218691, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String,System.Boolean)
extern const MethodInfo Uri_Unescape_m9273_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m9273/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, Uri_t1273_Uri_Unescape_m9273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_ParseAsWindowsUNC_m9274_ParameterInfos[] = 
{
	{"uriString", 0, 134218692, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsWindowsUNC(System.String)
extern const MethodInfo Uri_ParseAsWindowsUNC_m9274_MethodInfo = 
{
	"ParseAsWindowsUNC"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsUNC_m9274/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Uri_t1273_Uri_ParseAsWindowsUNC_m9274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_ParseAsWindowsAbsoluteFilePath_m9275_ParameterInfos[] = 
{
	{"uriString", 0, 134218693, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsWindowsAbsoluteFilePath_m9275_MethodInfo = 
{
	"ParseAsWindowsAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsAbsoluteFilePath_m9275/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_ParseAsWindowsAbsoluteFilePath_m9275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_ParseAsUnixAbsoluteFilePath_m9276_ParameterInfos[] = 
{
	{"uriString", 0, 134218694, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsUnixAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsUnixAbsoluteFilePath_m9276_MethodInfo = 
{
	"ParseAsUnixAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsUnixAbsoluteFilePath_m9276/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Uri_t1273_Uri_ParseAsUnixAbsoluteFilePath_m9276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1988_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Parse_m9277_ParameterInfos[] = 
{
	{"kind", 0, 134218695, 0, &UriKind_t1988_0_0_0},
	{"uriString", 1, 134218696, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Parse(System.UriKind,System.String)
extern const MethodInfo Uri_Parse_m9277_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Uri_Parse_m9277/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Object_t/* invoker_method */
	, Uri_t1273_Uri_Parse_m9277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1988_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_ParseNoExceptions_m9278_ParameterInfos[] = 
{
	{"kind", 0, 134218697, 0, &UriKind_t1988_0_0_0},
	{"uriString", 1, 134218698, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseNoExceptions(System.UriKind,System.String)
extern const MethodInfo Uri_ParseNoExceptions_m9278_MethodInfo = 
{
	"ParseNoExceptions"/* name */
	, (methodPointerType)&Uri_ParseNoExceptions_m9278/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t127_Object_t/* invoker_method */
	, Uri_t1273_Uri_ParseNoExceptions_m9278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_CompactEscaped_m9279_ParameterInfos[] = 
{
	{"scheme", 0, 134218699, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CompactEscaped(System.String)
extern const MethodInfo Uri_CompactEscaped_m9279_MethodInfo = 
{
	"CompactEscaped"/* name */
	, (methodPointerType)&Uri_CompactEscaped_m9279/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_CompactEscaped_m9279_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Uri_t1273_Uri_Reduce_m9280_ParameterInfos[] = 
{
	{"path", 0, 134218700, 0, &String_t_0_0_0},
	{"compact_escaped", 1, 134218701, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Reduce(System.String,System.Boolean)
extern const MethodInfo Uri_Reduce_m9280_MethodInfo = 
{
	"Reduce"/* name */
	, (methodPointerType)&Uri_Reduce_m9280/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, Uri_t1273_Uri_Reduce_m9280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_1_0_0;
extern const Il2CppType Char_t451_1_0_2;
extern const Il2CppType Char_t451_1_0_0;
static const ParameterInfo Uri_t1273_Uri_HexUnescapeMultiByte_m9281_ParameterInfos[] = 
{
	{"pattern", 0, 134218702, 0, &String_t_0_0_0},
	{"index", 1, 134218703, 0, &Int32_t127_1_0_0},
	{"surrogate", 2, 134218704, 0, &Char_t451_1_0_2},
};
extern void* RuntimeInvoker_Char_t451_Object_t_Int32U26_t535_CharU26_t2039 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Uri::HexUnescapeMultiByte(System.String,System.Int32&,System.Char&)
extern const MethodInfo Uri_HexUnescapeMultiByte_m9281_MethodInfo = 
{
	"HexUnescapeMultiByte"/* name */
	, (methodPointerType)&Uri_HexUnescapeMultiByte_m9281/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Char_t451_0_0_0/* return_type */
	, RuntimeInvoker_Char_t451_Object_t_Int32U26_t535_CharU26_t2039/* invoker_method */
	, Uri_t1273_Uri_HexUnescapeMultiByte_m9281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_GetSchemeDelimiter_m9282_ParameterInfos[] = 
{
	{"scheme", 0, 134218705, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetSchemeDelimiter(System.String)
extern const MethodInfo Uri_GetSchemeDelimiter_m9282_MethodInfo = 
{
	"GetSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetSchemeDelimiter_m9282/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_GetSchemeDelimiter_m9282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_GetDefaultPort_m9283_ParameterInfos[] = 
{
	{"scheme", 0, 134218706, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetDefaultPort(System.String)
extern const MethodInfo Uri_GetDefaultPort_m9283_MethodInfo = 
{
	"GetDefaultPort"/* name */
	, (methodPointerType)&Uri_GetDefaultPort_m9283/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127_Object_t/* invoker_method */
	, Uri_t1273_Uri_GetDefaultPort_m9283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetOpaqueWiseSchemeDelimiter()
extern const MethodInfo Uri_GetOpaqueWiseSchemeDelimiter_m9284_MethodInfo = 
{
	"GetOpaqueWiseSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetOpaqueWiseSchemeDelimiter_m9284/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t1273_Uri_IsPredefinedScheme_m9285_ParameterInfos[] = 
{
	{"scheme", 0, 134218707, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsPredefinedScheme(System.String)
extern const MethodInfo Uri_IsPredefinedScheme_m9285_MethodInfo = 
{
	"IsPredefinedScheme"/* name */
	, (methodPointerType)&Uri_IsPredefinedScheme_m9285/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Uri_t1273_Uri_IsPredefinedScheme_m9285_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.Uri::get_Parser()
extern const MethodInfo Uri_get_Parser_m9286_MethodInfo = 
{
	"get_Parser"/* name */
	, (methodPointerType)&Uri_get_Parser_m9286/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1982_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::EnsureAbsoluteUri()
extern const MethodInfo Uri_EnsureAbsoluteUri_m9287_MethodInfo = 
{
	"EnsureAbsoluteUri"/* name */
	, (methodPointerType)&Uri_EnsureAbsoluteUri_m9287/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t1273_0_0_0;
extern const Il2CppType Uri_t1273_0_0_0;
static const ParameterInfo Uri_t1273_Uri_op_Equality_m9288_ParameterInfos[] = 
{
	{"u1", 0, 134218708, 0, &Uri_t1273_0_0_0},
	{"u2", 1, 134218709, 0, &Uri_t1273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
extern const MethodInfo Uri_op_Equality_m9288_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Uri_op_Equality_m9288/* method */
	, &Uri_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, Uri_t1273_Uri_op_Equality_m9288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Uri_t1273_MethodInfos[] =
{
	&Uri__ctor_m6969_MethodInfo,
	&Uri__ctor_m9242_MethodInfo,
	&Uri__ctor_m9243_MethodInfo,
	&Uri__ctor_m6971_MethodInfo,
	&Uri__cctor_m9244_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_MethodInfo,
	&Uri_Merge_m9246_MethodInfo,
	&Uri_get_AbsoluteUri_m9247_MethodInfo,
	&Uri_get_Authority_m9248_MethodInfo,
	&Uri_get_Host_m8321_MethodInfo,
	&Uri_get_IsFile_m9249_MethodInfo,
	&Uri_get_IsLoopback_m9250_MethodInfo,
	&Uri_get_IsUnc_m9251_MethodInfo,
	&Uri_get_Scheme_m9252_MethodInfo,
	&Uri_get_IsAbsoluteUri_m9253_MethodInfo,
	&Uri_CheckHostName_m9254_MethodInfo,
	&Uri_IsIPv4Address_m9255_MethodInfo,
	&Uri_IsDomainAddress_m9256_MethodInfo,
	&Uri_CheckSchemeName_m9257_MethodInfo,
	&Uri_IsAlpha_m9258_MethodInfo,
	&Uri_Equals_m9259_MethodInfo,
	&Uri_InternalEquals_m9260_MethodInfo,
	&Uri_GetHashCode_m9261_MethodInfo,
	&Uri_GetLeftPart_m9262_MethodInfo,
	&Uri_FromHex_m9263_MethodInfo,
	&Uri_HexEscape_m9264_MethodInfo,
	&Uri_IsHexDigit_m9265_MethodInfo,
	&Uri_IsHexEncoding_m9266_MethodInfo,
	&Uri_AppendQueryAndFragment_m9267_MethodInfo,
	&Uri_ToString_m9268_MethodInfo,
	&Uri_EscapeString_m9269_MethodInfo,
	&Uri_EscapeString_m9270_MethodInfo,
	&Uri_ParseUri_m9271_MethodInfo,
	&Uri_Unescape_m9272_MethodInfo,
	&Uri_Unescape_m9273_MethodInfo,
	&Uri_ParseAsWindowsUNC_m9274_MethodInfo,
	&Uri_ParseAsWindowsAbsoluteFilePath_m9275_MethodInfo,
	&Uri_ParseAsUnixAbsoluteFilePath_m9276_MethodInfo,
	&Uri_Parse_m9277_MethodInfo,
	&Uri_ParseNoExceptions_m9278_MethodInfo,
	&Uri_CompactEscaped_m9279_MethodInfo,
	&Uri_Reduce_m9280_MethodInfo,
	&Uri_HexUnescapeMultiByte_m9281_MethodInfo,
	&Uri_GetSchemeDelimiter_m9282_MethodInfo,
	&Uri_GetDefaultPort_m9283_MethodInfo,
	&Uri_GetOpaqueWiseSchemeDelimiter_m9284_MethodInfo,
	&Uri_IsPredefinedScheme_m9285_MethodInfo,
	&Uri_get_Parser_m9286_MethodInfo,
	&Uri_EnsureAbsoluteUri_m9287_MethodInfo,
	&Uri_op_Equality_m9288_MethodInfo,
	NULL
};
extern const MethodInfo Uri_get_AbsoluteUri_m9247_MethodInfo;
static const PropertyInfo Uri_t1273____AbsoluteUri_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "AbsoluteUri"/* name */
	, &Uri_get_AbsoluteUri_m9247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Authority_m9248_MethodInfo;
static const PropertyInfo Uri_t1273____Authority_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "Authority"/* name */
	, &Uri_get_Authority_m9248_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Host_m8321_MethodInfo;
static const PropertyInfo Uri_t1273____Host_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "Host"/* name */
	, &Uri_get_Host_m8321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsFile_m9249_MethodInfo;
static const PropertyInfo Uri_t1273____IsFile_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "IsFile"/* name */
	, &Uri_get_IsFile_m9249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsLoopback_m9250_MethodInfo;
static const PropertyInfo Uri_t1273____IsLoopback_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "IsLoopback"/* name */
	, &Uri_get_IsLoopback_m9250_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsUnc_m9251_MethodInfo;
static const PropertyInfo Uri_t1273____IsUnc_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "IsUnc"/* name */
	, &Uri_get_IsUnc_m9251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Scheme_m9252_MethodInfo;
static const PropertyInfo Uri_t1273____Scheme_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "Scheme"/* name */
	, &Uri_get_Scheme_m9252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsAbsoluteUri_m9253_MethodInfo;
static const PropertyInfo Uri_t1273____IsAbsoluteUri_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "IsAbsoluteUri"/* name */
	, &Uri_get_IsAbsoluteUri_m9253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Parser_m9286_MethodInfo;
static const PropertyInfo Uri_t1273____Parser_PropertyInfo = 
{
	&Uri_t1273_il2cpp_TypeInfo/* parent */
	, "Parser"/* name */
	, &Uri_get_Parser_m9286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Uri_t1273_PropertyInfos[] =
{
	&Uri_t1273____AbsoluteUri_PropertyInfo,
	&Uri_t1273____Authority_PropertyInfo,
	&Uri_t1273____Host_PropertyInfo,
	&Uri_t1273____IsFile_PropertyInfo,
	&Uri_t1273____IsLoopback_PropertyInfo,
	&Uri_t1273____IsUnc_PropertyInfo,
	&Uri_t1273____Scheme_PropertyInfo,
	&Uri_t1273____IsAbsoluteUri_PropertyInfo,
	&Uri_t1273____Parser_PropertyInfo,
	NULL
};
static const Il2CppType* Uri_t1273_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UriScheme_t1984_0_0_0,
};
extern const MethodInfo Uri_Equals_m9259_MethodInfo;
extern const MethodInfo Uri_GetHashCode_m9261_MethodInfo;
extern const MethodInfo Uri_ToString_m9268_MethodInfo;
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_MethodInfo;
extern const MethodInfo Uri_Unescape_m9272_MethodInfo;
static const Il2CppMethodReference Uri_t1273_VTable[] =
{
	&Uri_Equals_m9259_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Uri_GetHashCode_m9261_MethodInfo,
	&Uri_ToString_m9268_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m9245_MethodInfo,
	&Uri_Unescape_m9272_MethodInfo,
};
static bool Uri_t1273_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Uri_t1273_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
};
static Il2CppInterfaceOffsetPair Uri_t1273_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Uri_t1273_1_0_0;
struct Uri_t1273;
const Il2CppTypeDefinitionMetadata Uri_t1273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Uri_t1273_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Uri_t1273_InterfacesTypeInfos/* implementedInterfaces */
	, Uri_t1273_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Uri_t1273_VTable/* vtableMethods */
	, Uri_t1273_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 758/* fieldStart */

};
TypeInfo Uri_t1273_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Uri"/* name */
	, "System"/* namespaze */
	, Uri_t1273_MethodInfos/* methods */
	, Uri_t1273_PropertyInfos/* properties */
	, NULL/* events */
	, &Uri_t1273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 66/* custom_attributes_cache */
	, &Uri_t1273_0_0_0/* byval_arg */
	, &Uri_t1273_1_0_0/* this_arg */
	, &Uri_t1273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Uri_t1273)/* instance_size */
	, sizeof (Uri_t1273)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Uri_t1273_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 9/* property_count */
	, 38/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UriFormatException
#include "System_System_UriFormatException.h"
// Metadata Definition System.UriFormatException
extern TypeInfo UriFormatException_t1986_il2cpp_TypeInfo;
// System.UriFormatException
#include "System_System_UriFormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor()
extern const MethodInfo UriFormatException__ctor_m9289_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m9289/* method */
	, &UriFormatException_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriFormatException_t1986_UriFormatException__ctor_m9290_ParameterInfos[] = 
{
	{"message", 0, 134218713, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.String)
extern const MethodInfo UriFormatException__ctor_m9290_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m9290/* method */
	, &UriFormatException_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UriFormatException_t1986_UriFormatException__ctor_m9290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UriFormatException_t1986_UriFormatException__ctor_m9291_ParameterInfos[] = 
{
	{"info", 0, 134218714, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134218715, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException__ctor_m9291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m9291/* method */
	, &UriFormatException_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UriFormatException_t1986_UriFormatException__ctor_m9291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UriFormatException_t1986_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_ParameterInfos[] = 
{
	{"info", 0, 134218716, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134218717, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292/* method */
	, &UriFormatException_t1986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UriFormatException_t1986_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriFormatException_t1986_MethodInfos[] =
{
	&UriFormatException__ctor_m9289_MethodInfo,
	&UriFormatException__ctor_m9290_MethodInfo,
	&UriFormatException__ctor_m9291_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m7170_MethodInfo;
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7172_MethodInfo;
extern const MethodInfo Exception_get_Message_m7173_MethodInfo;
extern const MethodInfo Exception_get_Source_m7174_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7175_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7171_MethodInfo;
extern const MethodInfo Exception_GetType_m7176_MethodInfo;
static const Il2CppMethodReference UriFormatException_t1986_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m9292_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool UriFormatException_t1986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UriFormatException_t1986_InterfacesTypeInfos[] = 
{
	&ISerializable_t513_0_0_0,
};
extern const Il2CppType _Exception_t1473_0_0_0;
static Il2CppInterfaceOffsetPair UriFormatException_t1986_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriFormatException_t1986_0_0_0;
extern const Il2CppType UriFormatException_t1986_1_0_0;
extern const Il2CppType FormatException_t1399_0_0_0;
struct UriFormatException_t1986;
const Il2CppTypeDefinitionMetadata UriFormatException_t1986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UriFormatException_t1986_InterfacesTypeInfos/* implementedInterfaces */
	, UriFormatException_t1986_InterfacesOffsets/* interfaceOffsets */
	, &FormatException_t1399_0_0_0/* parent */
	, UriFormatException_t1986_VTable/* vtableMethods */
	, UriFormatException_t1986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriFormatException_t1986_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriFormatException"/* name */
	, "System"/* namespaze */
	, UriFormatException_t1986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriFormatException_t1986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriFormatException_t1986_0_0_0/* byval_arg */
	, &UriFormatException_t1986_1_0_0/* this_arg */
	, &UriFormatException_t1986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriFormatException_t1986)/* instance_size */
	, sizeof (UriFormatException_t1986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// Metadata Definition System.UriHostNameType
extern TypeInfo UriHostNameType_t1987_il2cpp_TypeInfo;
// System.UriHostNameType
#include "System_System_UriHostNameTypeMethodDeclarations.h"
static const MethodInfo* UriHostNameType_t1987_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriHostNameType_t1987_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UriHostNameType_t1987_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriHostNameType_t1987_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriHostNameType_t1987_1_0_0;
const Il2CppTypeDefinitionMetadata UriHostNameType_t1987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriHostNameType_t1987_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UriHostNameType_t1987_VTable/* vtableMethods */
	, UriHostNameType_t1987_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 796/* fieldStart */

};
TypeInfo UriHostNameType_t1987_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriHostNameType"/* name */
	, "System"/* namespaze */
	, UriHostNameType_t1987_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriHostNameType_t1987_0_0_0/* byval_arg */
	, &UriHostNameType_t1987_1_0_0/* this_arg */
	, &UriHostNameType_t1987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriHostNameType_t1987)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriHostNameType_t1987)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriKind
#include "System_System_UriKind.h"
// Metadata Definition System.UriKind
extern TypeInfo UriKind_t1988_il2cpp_TypeInfo;
// System.UriKind
#include "System_System_UriKindMethodDeclarations.h"
static const MethodInfo* UriKind_t1988_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriKind_t1988_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UriKind_t1988_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriKind_t1988_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriKind_t1988_1_0_0;
const Il2CppTypeDefinitionMetadata UriKind_t1988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriKind_t1988_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UriKind_t1988_VTable/* vtableMethods */
	, UriKind_t1988_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 802/* fieldStart */

};
TypeInfo UriKind_t1988_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriKind"/* name */
	, "System"/* namespaze */
	, UriKind_t1988_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriKind_t1988_0_0_0/* byval_arg */
	, &UriKind_t1988_1_0_0/* this_arg */
	, &UriKind_t1988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriKind_t1988)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriKind_t1988)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriParser
#include "System_System_UriParser.h"
// Metadata Definition System.UriParser
extern TypeInfo UriParser_t1982_il2cpp_TypeInfo;
// System.UriParser
#include "System_System_UriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.ctor()
extern const MethodInfo UriParser__ctor_m9293_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriParser__ctor_m9293/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.cctor()
extern const MethodInfo UriParser__cctor_m9294_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UriParser__cctor_m9294/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t1273_0_0_0;
extern const Il2CppType UriFormatException_t1986_1_0_2;
static const ParameterInfo UriParser_t1982_UriParser_InitializeAndValidate_m9295_ParameterInfos[] = 
{
	{"uri", 0, 134218718, 0, &Uri_t1273_0_0_0},
	{"parsingError", 1, 134218719, 0, &UriFormatException_t1986_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_UriFormatExceptionU26_t2040 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern const MethodInfo UriParser_InitializeAndValidate_m9295_MethodInfo = 
{
	"InitializeAndValidate"/* name */
	, (methodPointerType)&UriParser_InitializeAndValidate_m9295/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_UriFormatExceptionU26_t2040/* invoker_method */
	, UriParser_t1982_UriParser_InitializeAndValidate_m9295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 453/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo UriParser_t1982_UriParser_OnRegister_m9296_ParameterInfos[] = 
{
	{"schemeName", 0, 134218720, 0, &String_t_0_0_0},
	{"defaultPort", 1, 134218721, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
extern const MethodInfo UriParser_OnRegister_m9296_MethodInfo = 
{
	"OnRegister"/* name */
	, (methodPointerType)&UriParser_OnRegister_m9296/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, UriParser_t1982_UriParser_OnRegister_m9296_ParameterInfos/* parameters */
	, 75/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1982_UriParser_set_SchemeName_m9297_ParameterInfos[] = 
{
	{"value", 0, 134218722, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_SchemeName(System.String)
extern const MethodInfo UriParser_set_SchemeName_m9297_MethodInfo = 
{
	"set_SchemeName"/* name */
	, (methodPointerType)&UriParser_set_SchemeName_m9297/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UriParser_t1982_UriParser_set_SchemeName_m9297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.UriParser::get_DefaultPort()
extern const MethodInfo UriParser_get_DefaultPort_m9298_MethodInfo = 
{
	"get_DefaultPort"/* name */
	, (methodPointerType)&UriParser_get_DefaultPort_m9298/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo UriParser_t1982_UriParser_set_DefaultPort_m9299_ParameterInfos[] = 
{
	{"value", 0, 134218723, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_DefaultPort(System.Int32)
extern const MethodInfo UriParser_set_DefaultPort_m9299_MethodInfo = 
{
	"set_DefaultPort"/* name */
	, (methodPointerType)&UriParser_set_DefaultPort_m9299/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, UriParser_t1982_UriParser_set_DefaultPort_m9299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::CreateDefaults()
extern const MethodInfo UriParser_CreateDefaults_m9300_MethodInfo = 
{
	"CreateDefaults"/* name */
	, (methodPointerType)&UriParser_CreateDefaults_m9300/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t1736_0_0_0;
extern const Il2CppType UriParser_t1982_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo UriParser_t1982_UriParser_InternalRegister_m9301_ParameterInfos[] = 
{
	{"table", 0, 134218724, 0, &Hashtable_t1736_0_0_0},
	{"uriParser", 1, 134218725, 0, &UriParser_t1982_0_0_0},
	{"schemeName", 2, 134218726, 0, &String_t_0_0_0},
	{"defaultPort", 3, 134218727, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
extern const MethodInfo UriParser_InternalRegister_m9301_MethodInfo = 
{
	"InternalRegister"/* name */
	, (methodPointerType)&UriParser_InternalRegister_m9301/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Int32_t127/* invoker_method */
	, UriParser_t1982_UriParser_InternalRegister_m9301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1982_UriParser_GetParser_m9302_ParameterInfos[] = 
{
	{"schemeName", 0, 134218728, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.UriParser::GetParser(System.String)
extern const MethodInfo UriParser_GetParser_m9302_MethodInfo = 
{
	"GetParser"/* name */
	, (methodPointerType)&UriParser_GetParser_m9302/* method */
	, &UriParser_t1982_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1982_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UriParser_t1982_UriParser_GetParser_m9302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriParser_t1982_MethodInfos[] =
{
	&UriParser__ctor_m9293_MethodInfo,
	&UriParser__cctor_m9294_MethodInfo,
	&UriParser_InitializeAndValidate_m9295_MethodInfo,
	&UriParser_OnRegister_m9296_MethodInfo,
	&UriParser_set_SchemeName_m9297_MethodInfo,
	&UriParser_get_DefaultPort_m9298_MethodInfo,
	&UriParser_set_DefaultPort_m9299_MethodInfo,
	&UriParser_CreateDefaults_m9300_MethodInfo,
	&UriParser_InternalRegister_m9301_MethodInfo,
	&UriParser_GetParser_m9302_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_set_SchemeName_m9297_MethodInfo;
static const PropertyInfo UriParser_t1982____SchemeName_PropertyInfo = 
{
	&UriParser_t1982_il2cpp_TypeInfo/* parent */
	, "SchemeName"/* name */
	, NULL/* get */
	, &UriParser_set_SchemeName_m9297_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UriParser_get_DefaultPort_m9298_MethodInfo;
extern const MethodInfo UriParser_set_DefaultPort_m9299_MethodInfo;
static const PropertyInfo UriParser_t1982____DefaultPort_PropertyInfo = 
{
	&UriParser_t1982_il2cpp_TypeInfo/* parent */
	, "DefaultPort"/* name */
	, &UriParser_get_DefaultPort_m9298_MethodInfo/* get */
	, &UriParser_set_DefaultPort_m9299_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UriParser_t1982_PropertyInfos[] =
{
	&UriParser_t1982____SchemeName_PropertyInfo,
	&UriParser_t1982____DefaultPort_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UriParser_t1982_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&UriParser_InitializeAndValidate_m9295_MethodInfo,
	&UriParser_OnRegister_m9296_MethodInfo,
};
static bool UriParser_t1982_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriParser_t1982_1_0_0;
struct UriParser_t1982;
const Il2CppTypeDefinitionMetadata UriParser_t1982_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UriParser_t1982_VTable/* vtableMethods */
	, UriParser_t1982_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 806/* fieldStart */

};
TypeInfo UriParser_t1982_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriParser"/* name */
	, "System"/* namespaze */
	, UriParser_t1982_MethodInfos/* methods */
	, UriParser_t1982_PropertyInfos/* properties */
	, NULL/* events */
	, &UriParser_t1982_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriParser_t1982_0_0_0/* byval_arg */
	, &UriParser_t1982_1_0_0/* this_arg */
	, &UriParser_t1982_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriParser_t1982)/* instance_size */
	, sizeof (UriParser_t1982)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UriParser_t1982_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriPartial
#include "System_System_UriPartial.h"
// Metadata Definition System.UriPartial
extern TypeInfo UriPartial_t1989_il2cpp_TypeInfo;
// System.UriPartial
#include "System_System_UriPartialMethodDeclarations.h"
static const MethodInfo* UriPartial_t1989_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriPartial_t1989_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UriPartial_t1989_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriPartial_t1989_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriPartial_t1989_1_0_0;
const Il2CppTypeDefinitionMetadata UriPartial_t1989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriPartial_t1989_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UriPartial_t1989_VTable/* vtableMethods */
	, UriPartial_t1989_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 812/* fieldStart */

};
TypeInfo UriPartial_t1989_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriPartial"/* name */
	, "System"/* namespaze */
	, UriPartial_t1989_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriPartial_t1989_0_0_0/* byval_arg */
	, &UriPartial_t1989_1_0_0/* this_arg */
	, &UriPartial_t1989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriPartial_t1989)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriPartial_t1989)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// Metadata Definition System.UriTypeConverter
extern TypeInfo UriTypeConverter_t1990_il2cpp_TypeInfo;
// System.UriTypeConverter
#include "System_System_UriTypeConverterMethodDeclarations.h"
static const MethodInfo* UriTypeConverter_t1990_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriTypeConverter_t1990_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool UriTypeConverter_t1990_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriTypeConverter_t1990_0_0_0;
extern const Il2CppType UriTypeConverter_t1990_1_0_0;
extern const Il2CppType TypeConverter_t1861_0_0_0;
struct UriTypeConverter_t1990;
const Il2CppTypeDefinitionMetadata UriTypeConverter_t1990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeConverter_t1861_0_0_0/* parent */
	, UriTypeConverter_t1990_VTable/* vtableMethods */
	, UriTypeConverter_t1990_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriTypeConverter_t1990_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriTypeConverter"/* name */
	, "System"/* namespaze */
	, UriTypeConverter_t1990_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriTypeConverter_t1990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriTypeConverter_t1990_0_0_0/* byval_arg */
	, &UriTypeConverter_t1990_1_0_0/* this_arg */
	, &UriTypeConverter_t1990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriTypeConverter_t1990)/* instance_size */
	, sizeof (UriTypeConverter_t1990)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern TypeInfo RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo;
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback__ctor_m9303_ParameterInfos[] = 
{
	{"object", 0, 134218729, 0, &Object_t_0_0_0},
	{"method", 1, 134218730, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo RemoteCertificateValidationCallback__ctor_m9303_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback__ctor_m9303/* method */
	, &RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback__ctor_m9303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType X509Chain_t1832_0_0_0;
extern const Il2CppType X509Chain_t1832_0_0_0;
extern const Il2CppType SslPolicyErrors_t1864_0_0_0;
extern const Il2CppType SslPolicyErrors_t1864_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_Invoke_m9304_ParameterInfos[] = 
{
	{"sender", 0, 134218731, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218732, 0, &X509Certificate_t1771_0_0_0},
	{"chain", 2, 134218733, 0, &X509Chain_t1832_0_0_0},
	{"sslPolicyErrors", 3, 134218734, 0, &SslPolicyErrors_t1864_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m9304_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_Invoke_m9304/* method */
	, &RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t_Object_t_Int32_t127/* invoker_method */
	, RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_Invoke_m9304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1771_0_0_0;
extern const Il2CppType X509Chain_t1832_0_0_0;
extern const Il2CppType SslPolicyErrors_t1864_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_BeginInvoke_m9305_ParameterInfos[] = 
{
	{"sender", 0, 134218735, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218736, 0, &X509Certificate_t1771_0_0_0},
	{"chain", 2, 134218737, 0, &X509Chain_t1832_0_0_0},
	{"sslPolicyErrors", 3, 134218738, 0, &SslPolicyErrors_t1864_0_0_0},
	{"callback", 4, 134218739, 0, &AsyncCallback_t305_0_0_0},
	{"object", 5, 134218740, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Net.Security.RemoteCertificateValidationCallback::BeginInvoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors,System.AsyncCallback,System.Object)
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m9305_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_BeginInvoke_m9305/* method */
	, &RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t127_Object_t_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_BeginInvoke_m9305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_EndInvoke_m9306_ParameterInfos[] = 
{
	{"result", 0, 134218741, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m9306_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_EndInvoke_m9306/* method */
	, &RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1830_RemoteCertificateValidationCallback_EndInvoke_m9306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemoteCertificateValidationCallback_t1830_MethodInfos[] =
{
	&RemoteCertificateValidationCallback__ctor_m9303_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m9304_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m9305_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m9306_MethodInfo,
	NULL
};
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m9304_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m9305_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m9306_MethodInfo;
static const Il2CppMethodReference RemoteCertificateValidationCallback_t1830_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m9304_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m9305_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m9306_MethodInfo,
};
static bool RemoteCertificateValidationCallback_t1830_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RemoteCertificateValidationCallback_t1830_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RemoteCertificateValidationCallback_t1830_0_0_0;
extern const Il2CppType RemoteCertificateValidationCallback_t1830_1_0_0;
struct RemoteCertificateValidationCallback_t1830;
const Il2CppTypeDefinitionMetadata RemoteCertificateValidationCallback_t1830_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemoteCertificateValidationCallback_t1830_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, RemoteCertificateValidationCallback_t1830_VTable/* vtableMethods */
	, RemoteCertificateValidationCallback_t1830_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteCertificateValidationCallback"/* name */
	, "System.Net.Security"/* namespaze */
	, RemoteCertificateValidationCallback_t1830_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteCertificateValidationCallback_t1830_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteCertificateValidationCallback_t1830_0_0_0/* byval_arg */
	, &RemoteCertificateValidationCallback_t1830_1_0_0/* this_arg */
	, &RemoteCertificateValidationCallback_t1830_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1830/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteCertificateValidationCallback_t1830)/* instance_size */
	, sizeof (RemoteCertificateValidationCallback_t1830)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.MatchEvaluator
extern TypeInfo MatchEvaluator_t1991_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluatorMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1991_MatchEvaluator__ctor_m9307_ParameterInfos[] = 
{
	{"object", 0, 134218742, 0, &Object_t_0_0_0},
	{"method", 1, 134218743, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MatchEvaluator::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MatchEvaluator__ctor_m9307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchEvaluator__ctor_m9307/* method */
	, &MatchEvaluator_t1991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, MatchEvaluator_t1991_MatchEvaluator__ctor_m9307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1061_0_0_0;
static const ParameterInfo MatchEvaluator_t1991_MatchEvaluator_Invoke_m9308_ParameterInfos[] = 
{
	{"match", 0, 134218744, 0, &Match_t1061_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::Invoke(System.Text.RegularExpressions.Match)
extern const MethodInfo MatchEvaluator_Invoke_m9308_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MatchEvaluator_Invoke_m9308/* method */
	, &MatchEvaluator_t1991_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1991_MatchEvaluator_Invoke_m9308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1061_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1991_MatchEvaluator_BeginInvoke_m9309_ParameterInfos[] = 
{
	{"match", 0, 134218745, 0, &Match_t1061_0_0_0},
	{"callback", 1, 134218746, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134218747, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.MatchEvaluator::BeginInvoke(System.Text.RegularExpressions.Match,System.AsyncCallback,System.Object)
extern const MethodInfo MatchEvaluator_BeginInvoke_m9309_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_BeginInvoke_m9309/* method */
	, &MatchEvaluator_t1991_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1991_MatchEvaluator_BeginInvoke_m9309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo MatchEvaluator_t1991_MatchEvaluator_EndInvoke_m9310_ParameterInfos[] = 
{
	{"result", 0, 134218748, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::EndInvoke(System.IAsyncResult)
extern const MethodInfo MatchEvaluator_EndInvoke_m9310_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_EndInvoke_m9310/* method */
	, &MatchEvaluator_t1991_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1991_MatchEvaluator_EndInvoke_m9310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatchEvaluator_t1991_MethodInfos[] =
{
	&MatchEvaluator__ctor_m9307_MethodInfo,
	&MatchEvaluator_Invoke_m9308_MethodInfo,
	&MatchEvaluator_BeginInvoke_m9309_MethodInfo,
	&MatchEvaluator_EndInvoke_m9310_MethodInfo,
	NULL
};
extern const MethodInfo MatchEvaluator_Invoke_m9308_MethodInfo;
extern const MethodInfo MatchEvaluator_BeginInvoke_m9309_MethodInfo;
extern const MethodInfo MatchEvaluator_EndInvoke_m9310_MethodInfo;
static const Il2CppMethodReference MatchEvaluator_t1991_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&MatchEvaluator_Invoke_m9308_MethodInfo,
	&MatchEvaluator_BeginInvoke_m9309_MethodInfo,
	&MatchEvaluator_EndInvoke_m9310_MethodInfo,
};
static bool MatchEvaluator_t1991_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MatchEvaluator_t1991_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchEvaluator_t1991_0_0_0;
extern const Il2CppType MatchEvaluator_t1991_1_0_0;
struct MatchEvaluator_t1991;
const Il2CppTypeDefinitionMetadata MatchEvaluator_t1991_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchEvaluator_t1991_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, MatchEvaluator_t1991_VTable/* vtableMethods */
	, MatchEvaluator_t1991_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MatchEvaluator_t1991_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MatchEvaluator_t1991_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatchEvaluator_t1991_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchEvaluator_t1991_0_0_0/* byval_arg */
	, &MatchEvaluator_t1991_1_0_0/* this_arg */
	, &MatchEvaluator_t1991_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchEvaluator_t1991/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchEvaluator_t1991)/* instance_size */
	, sizeof (MatchEvaluator_t1991)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t1992_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t1992_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t1992_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24128_t1992_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t1992_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t1992_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1994_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1994_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t1992_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1994_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24128_t1992_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t1992_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t1992_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t1992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t1992_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t1992_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t1992_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t1992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t1992_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1992_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1992_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t1992)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t1992)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t1992_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1993_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1993_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1993_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU2412_t1993_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1993_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1993_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1993_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1994_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU2412_t1993_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1993_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1993_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1993_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1993_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1993_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1993_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1993_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1993)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1993)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1993_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1994_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1994_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U24ArrayTypeU24128_t1992_0_0_0,
	&U24ArrayTypeU2412_t1993_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1994_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1994_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1994_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1994;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1994_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1994_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1994_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1994_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 817/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1994_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1994_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 76/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1994_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1994_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1994)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1994)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1994_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
