﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Vector3ArrayPlugin
struct Vector3ArrayPlugin_t949;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1024;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t154;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t120;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t121;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" void Vector3ArrayPlugin_Reset_m5349 (Vector3ArrayPlugin_t949 * __this, TweenerCore_3_t1024 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] DG.Tweening.Plugins.Vector3ArrayPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,UnityEngine.Vector3)
extern "C" Vector3U5BU5D_t154* Vector3ArrayPlugin_ConvertToStartValue_m5350 (Vector3ArrayPlugin_t949 * __this, TweenerCore_3_t1024 * ___t, Vector3_t15  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" void Vector3ArrayPlugin_SetRelativeEndValue_m5351 (Vector3ArrayPlugin_t949 * __this, TweenerCore_3_t1024 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" void Vector3ArrayPlugin_SetChangeValue_m5352 (Vector3ArrayPlugin_t949 * __this, TweenerCore_3_t1024 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.Vector3ArrayPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.Vector3ArrayOptions,System.Single,UnityEngine.Vector3[])
extern "C" float Vector3ArrayPlugin_GetSpeedBasedDuration_m5353 (Vector3ArrayPlugin_t949 * __this, Vector3ArrayOptions_t951  ___options, float ___unitsXSecond, Vector3U5BU5D_t154* ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.Vector3ArrayOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector3ArrayPlugin_EvaluateAndApply_m5354 (Vector3ArrayPlugin_t949 * __this, Vector3ArrayOptions_t951  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t120 * ___getter, DOSetter_1_t121 * ___setter, float ___elapsed, Vector3U5BU5D_t154* ___startValue, Vector3U5BU5D_t154* ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::.ctor()
extern "C" void Vector3ArrayPlugin__ctor_m5355 (Vector3ArrayPlugin_t949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
