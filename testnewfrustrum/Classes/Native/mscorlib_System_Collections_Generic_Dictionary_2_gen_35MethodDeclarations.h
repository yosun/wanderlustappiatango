﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3461;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1372;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4173;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt16>
struct KeyCollection_t3464;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt16>
struct ValueCollection_t3468;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3226;
// System.Collections.Generic.IDictionary`2<System.Object,System.UInt16>
struct IDictionary_2_t4177;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t4178;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct IEnumerator_1_t4179;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor()
extern "C" void Dictionary_2__ctor_m20021_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m20021(__this, method) (( void (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2__ctor_m20021_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20023_gshared (Dictionary_2_t3461 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20023(__this, ___comparer, method) (( void (*) (Dictionary_2_t3461 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20023_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m20025_gshared (Dictionary_2_t3461 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m20025(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3461 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20025_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m20027_gshared (Dictionary_2_t3461 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m20027(__this, ___capacity, method) (( void (*) (Dictionary_2_t3461 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m20027_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20029_gshared (Dictionary_2_t3461 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20029(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3461 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20029_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m20031_gshared (Dictionary_2_t3461 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m20031(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3461 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m20031_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033(__this, method) (( Object_t* (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20033_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035(__this, method) (( Object_t* (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20035_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m20037_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20037(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20037_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20039_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20039(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20039_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20041_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m20041(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20041_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m20043_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m20043(__this, ___key, method) (( bool (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m20043_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20045_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m20045(__this, ___key, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20045_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047(__this, method) (( bool (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20047_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049(__this, method) (( Object_t * (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20049_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051(__this, method) (( bool (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2_t3462  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3461 *, KeyValuePair_2_t3462 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20053_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2_t3462  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3461 *, KeyValuePair_2_t3462 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20055_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2U5BU5D_t4178* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3461 *, KeyValuePair_2U5BU5D_t4178*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20057_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2_t3462  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3461 *, KeyValuePair_2_t3462 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20059_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20061_gshared (Dictionary_2_t3461 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20061(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3461 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20061_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063(__this, method) (( Object_t * (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20063_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065(__this, method) (( Object_t* (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20065_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067(__this, method) (( Object_t * (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20067_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m20069_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m20069(__this, method) (( int32_t (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_get_Count_m20069_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Item(TKey)
extern "C" uint16_t Dictionary_2_get_Item_m20071_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m20071(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m20071_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m20073_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m20073(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m20073_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m20075_gshared (Dictionary_2_t3461 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m20075(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3461 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m20075_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m20077_gshared (Dictionary_2_t3461 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m20077(__this, ___size, method) (( void (*) (Dictionary_2_t3461 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m20077_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m20079_gshared (Dictionary_2_t3461 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m20079(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3461 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m20079_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3462  Dictionary_2_make_pair_m20081_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m20081(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3462  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m20081_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m20083_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m20083(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m20083_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_value(TKey,TValue)
extern "C" uint16_t Dictionary_2_pick_value_m20085_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m20085(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m20085_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m20087_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2U5BU5D_t4178* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m20087(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3461 *, KeyValuePair_2U5BU5D_t4178*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m20087_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Resize()
extern "C" void Dictionary_2_Resize_m20089_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m20089(__this, method) (( void (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_Resize_m20089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m20091_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m20091(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m20091_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Clear()
extern "C" void Dictionary_2_Clear_m20093_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m20093(__this, method) (( void (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_Clear_m20093_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m20095_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m20095(__this, ___key, method) (( bool (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m20095_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m20097_gshared (Dictionary_2_t3461 * __this, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m20097(__this, ___value, method) (( bool (*) (Dictionary_2_t3461 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m20097_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m20099_gshared (Dictionary_2_t3461 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m20099(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3461 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m20099_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m20101_gshared (Dictionary_2_t3461 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m20101(__this, ___sender, method) (( void (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m20101_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m20103_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m20103(__this, ___key, method) (( bool (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m20103_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m20105_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, uint16_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m20105(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3461 *, Object_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m20105_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Keys()
extern "C" KeyCollection_t3464 * Dictionary_2_get_Keys_m20107_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m20107(__this, method) (( KeyCollection_t3464 * (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_get_Keys_m20107_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Values()
extern "C" ValueCollection_t3468 * Dictionary_2_get_Values_m20109_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m20109(__this, method) (( ValueCollection_t3468 * (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_get_Values_m20109_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m20111_gshared (Dictionary_2_t3461 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m20111(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m20111_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTValue(System.Object)
extern "C" uint16_t Dictionary_2_ToTValue_m20113_gshared (Dictionary_2_t3461 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m20113(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t3461 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m20113_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m20115_gshared (Dictionary_2_t3461 * __this, KeyValuePair_2_t3462  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m20115(__this, ___pair, method) (( bool (*) (Dictionary_2_t3461 *, KeyValuePair_2_t3462 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20115_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3466  Dictionary_2_GetEnumerator_m20117_gshared (Dictionary_2_t3461 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m20117(__this, method) (( Enumerator_t3466  (*) (Dictionary_2_t3461 *, const MethodInfo*))Dictionary_2_GetEnumerator_m20117_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m20119_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m20119(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20119_gshared)(__this /* static, unused */, ___key, ___value, method)
