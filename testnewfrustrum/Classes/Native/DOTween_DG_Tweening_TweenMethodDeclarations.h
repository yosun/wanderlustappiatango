﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.TweenCallback
struct TweenCallback_t101;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Tween::Reset()
extern "C" void Tween_Reset_m5333 (Tween_t934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Tween::Validate()
// System.Single DG.Tweening.Tween::UpdateDelay(System.Single)
extern "C" float Tween_UpdateDelay_m5334 (Tween_t934 * __this, float ___elapsed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Tween::Startup()
// System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
// System.Boolean DG.Tweening.Tween::DoGoto(DG.Tweening.Tween,System.Single,System.Int32,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool Tween_DoGoto_m5335 (Object_t * __this /* static, unused */, Tween_t934 * ___t, float ___toPosition, int32_t ___toCompletedLoops, int32_t ___updateMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback)
extern "C" bool Tween_OnTweenCallback_m5336 (Object_t * __this /* static, unused */, TweenCallback_t101 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Tween::.ctor()
extern "C" void Tween__ctor_m5337 (Tween_t934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
