﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct Enumerator_t3656;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3654;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m23189_gshared (Enumerator_t3656 * __this, HashSet_1_t3654 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m23189(__this, ___hashset, method) (( void (*) (Enumerator_t3656 *, HashSet_1_t3654 *, const MethodInfo*))Enumerator__ctor_m23189_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23190_gshared (Enumerator_t3656 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23190(__this, method) (( Object_t * (*) (Enumerator_t3656 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23190_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23191_gshared (Enumerator_t3656 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23191(__this, method) (( bool (*) (Enumerator_t3656 *, const MethodInfo*))Enumerator_MoveNext_m23191_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m23192_gshared (Enumerator_t3656 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23192(__this, method) (( Object_t * (*) (Enumerator_t3656 *, const MethodInfo*))Enumerator_get_Current_m23192_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m23193_gshared (Enumerator_t3656 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23193(__this, method) (( void (*) (Enumerator_t3656 *, const MethodInfo*))Enumerator_Dispose_m23193_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m23194_gshared (Enumerator_t3656 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m23194(__this, method) (( void (*) (Enumerator_t3656 *, const MethodInfo*))Enumerator_CheckState_m23194_gshared)(__this, method)
