﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t307;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t2585;
// System.Delegate
struct Delegate_t143;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m2556 (MulticastDelegate_t307 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m2554 (MulticastDelegate_t307 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m2555 (MulticastDelegate_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t2585* MulticastDelegate_GetInvocationList_m2558 (MulticastDelegate_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t143 * MulticastDelegate_CombineImpl_m2559 (MulticastDelegate_t307 * __this, Delegate_t143 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m10070 (MulticastDelegate_t307 * __this, MulticastDelegate_t307 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t307 * MulticastDelegate_KPM_m10071 (Object_t * __this /* static, unused */, MulticastDelegate_t307 * ___needle, MulticastDelegate_t307 * ___haystack, MulticastDelegate_t307 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t143 * MulticastDelegate_RemoveImpl_m2560 (MulticastDelegate_t307 * __this, Delegate_t143 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
