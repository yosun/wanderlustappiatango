﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>
struct Enumerator_t3348;
// System.Object
struct Object_t;
// UnityEngine.RectTransform
struct RectTransform_t272;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t372;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18237(__this, ___l, method) (( void (*) (Enumerator_t3348 *, List_1_t372 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18238(__this, method) (( Object_t * (*) (Enumerator_t3348 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m18239(__this, method) (( void (*) (Enumerator_t3348 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::VerifyState()
#define Enumerator_VerifyState_m18240(__this, method) (( void (*) (Enumerator_t3348 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m18241(__this, method) (( bool (*) (Enumerator_t3348 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m18242(__this, method) (( RectTransform_t272 * (*) (Enumerator_t3348 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
