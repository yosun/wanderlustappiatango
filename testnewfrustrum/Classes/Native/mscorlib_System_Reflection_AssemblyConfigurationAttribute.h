﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t481  : public Attribute_t138
{
	// System.String System.Reflection.AssemblyConfigurationAttribute::name
	String_t* ___name_0;
};
