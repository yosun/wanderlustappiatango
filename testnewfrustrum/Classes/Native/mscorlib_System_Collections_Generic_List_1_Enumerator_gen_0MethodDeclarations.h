﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
struct Enumerator_t793;
// System.Object
struct Object_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t178;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t566;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18385(__this, ___l, method) (( void (*) (Enumerator_t793 *, List_1_t566 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18386(__this, method) (( Object_t * (*) (Enumerator_t793 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::Dispose()
#define Enumerator_Dispose_m18387(__this, method) (( void (*) (Enumerator_t793 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::VerifyState()
#define Enumerator_VerifyState_m18388(__this, method) (( void (*) (Enumerator_t793 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4298(__this, method) (( bool (*) (Enumerator_t793 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::get_Current()
#define Enumerator_get_Current_m4297(__this, method) (( Object_t * (*) (Enumerator_t793 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
