﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.Collections.DictionaryEntry>
struct  Transform_1_t3459  : public MulticastDelegate_t307
{
};
