﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t897_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t479_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t483_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t481_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t488_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var;
void g_DOTween_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(482);
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1271);
		DebuggableAttribute_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1270);
		AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(485);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1269);
		GuidAttribute_t479_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(477);
		AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(484);
		AssemblyProductAttribute_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(481);
		AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(478);
		AssemblyConfigurationAttribute_t481_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(479);
		AssemblyTrademarkAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(486);
		AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(480);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 19;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyCopyrightAttribute_t484 * tmp;
		tmp = (AssemblyCopyrightAttribute_t484 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2432(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Daniele Giardini, 2014"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t898 * tmp;
		tmp = (CompilationRelaxationsAttribute_t898 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m4680(tmp, 8, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t897 * tmp;
		tmp = (DebuggableAttribute_t897 *)il2cpp_codegen_object_new (DebuggableAttribute_t897_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4679(tmp, 2, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t487 * tmp;
		tmp = (AssemblyFileVersionAttribute_t487 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2435(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("DOTween46"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t479 * tmp;
		tmp = (GuidAttribute_t479 *)il2cpp_codegen_object_new (GuidAttribute_t479_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2427(tmp, il2cpp_codegen_string_new_wrapper("807e068c-2a0e-4c81-a303-4b4fd3924511"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t486 * tmp;
		tmp = (AssemblyTitleAttribute_t486 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2434(tmp, il2cpp_codegen_string_new_wrapper("DOTween"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t483 * tmp;
		tmp = (AssemblyProductAttribute_t483 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t483_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2431(tmp, il2cpp_codegen_string_new_wrapper("DOTween"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t480 * tmp;
		tmp = (AssemblyDescriptionAttribute_t480 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2428(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t481 * tmp;
		tmp = (AssemblyConfigurationAttribute_t481 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t481_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m2429(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t488 * tmp;
		tmp = (AssemblyTrademarkAttribute_t488 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m2436(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t482 * tmp;
		tmp = (AssemblyCompanyAttribute_t482 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2430(tmp, il2cpp_codegen_string_new_wrapper("Demigiant"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("DOTweenProEditor"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("DOTweenPro"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("DOTween43"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t896 * tmp;
		tmp = (InternalsVisibleToAttribute_t896 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t896_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4678(tmp, il2cpp_codegen_string_new_wrapper("DOTweenEditor"), NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t491_il2cpp_TypeInfo_var;
void DOTweenComponent_t935_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(488);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t491 * tmp;
		tmp = (AddComponentMenu_t491 *)il2cpp_codegen_object_new (AddComponentMenu_t491_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0__ctor_m5281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2__ctor_m5286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4__ctor_m5291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8__ctor_m5301(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a__ctor_m5306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_m5595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m5596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_m5597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m367(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m5598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenExtensions_t957_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void TweenExtensions_t957_CustomAttributesCacheGenerator_TweenExtensions_Duration_m370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_m360(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_m369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass92_t959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass96_t960_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass9a_t961_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClassb2_t962_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClassc6_t963_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void StringPluginExtensions_t992_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void StringPluginExtensions_t992_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m5468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void StringPluginExtensions_t992_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m5469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t489_il2cpp_TypeInfo_var;
void AxisConstraint_t1001_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(487);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t489 * tmp;
		tmp = (FlagsAttribute_t489 *)il2cpp_codegen_object_new (FlagsAttribute_t489_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2437(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_DOTween_Assembly_AttributeGenerators[55] = 
{
	NULL,
	g_DOTween_Assembly_CustomAttributesCacheGenerator,
	DOTweenComponent_t935_CustomAttributesCacheGenerator,
	U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator,
	U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5278,
	U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5280,
	U3CWaitForCompletionU3Ed__0_t936_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__0__ctor_m5281,
	U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator,
	U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5283,
	U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5285,
	U3CWaitForRewindU3Ed__2_t937_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__2__ctor_m5286,
	U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator,
	U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288,
	U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290,
	U3CWaitForKillU3Ed__4_t938_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__4__ctor_m5291,
	U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator,
	U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5293,
	U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5295,
	U3CWaitForElapsedLoopsU3Ed__6_t939_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5296,
	U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator,
	U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5298,
	U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5300,
	U3CWaitForPositionU3Ed__8_t940_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__8__ctor_m5301,
	U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator,
	U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5303,
	U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5305,
	U3CWaitForStartU3Ed__a_t941_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__a__ctor_m5306,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_m5595,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m5596,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_m5597,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m367,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m368,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m372,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m5598,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5357,
	TweenSettingsExtensions_t100_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m5358,
	TweenExtensions_t957_CustomAttributesCacheGenerator,
	TweenExtensions_t957_CustomAttributesCacheGenerator_TweenExtensions_Duration_m370,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_m360,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m371,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m366,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m250,
	ShortcutExtensions_t964_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_m369,
	U3CU3Ec__DisplayClass92_t959_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass96_t960_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9a_t961_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClassb2_t962_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClassc6_t963_CustomAttributesCacheGenerator,
	StringPluginExtensions_t992_CustomAttributesCacheGenerator,
	StringPluginExtensions_t992_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m5468,
	StringPluginExtensions_t992_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m5469,
	AxisConstraint_t1001_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_CustomAttributesCacheGenerator,
};
