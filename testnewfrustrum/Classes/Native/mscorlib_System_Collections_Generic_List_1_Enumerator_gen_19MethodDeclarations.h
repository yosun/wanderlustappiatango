﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
struct Enumerator_t890;
// System.Object
struct Object_t;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t789;
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t752;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23043(__this, ___l, method) (( void (*) (Enumerator_t890 *, List_1_t752 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23044(__this, method) (( Object_t * (*) (Enumerator_t890 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::Dispose()
#define Enumerator_Dispose_m23045(__this, method) (( void (*) (Enumerator_t890 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23046(__this, method) (( void (*) (Enumerator_t890 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4650(__this, method) (( bool (*) (Enumerator_t890 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::get_Current()
#define Enumerator_get_Current_m4649(__this, method) (( Object_t * (*) (Enumerator_t890 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
