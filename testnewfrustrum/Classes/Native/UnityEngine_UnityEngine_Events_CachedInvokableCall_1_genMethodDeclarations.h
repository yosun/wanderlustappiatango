﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1435;
// UnityEngine.Object
struct Object_t123;
struct Object_t123_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C" void CachedInvokableCall_1__ctor_m7032_gshared (CachedInvokableCall_1_t1435 * __this, Object_t123 * ___target, MethodInfo_t * ___theFunction, float ___argument, const MethodInfo* method);
#define CachedInvokableCall_1__ctor_m7032(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1435 *, Object_t123 *, MethodInfo_t *, float, const MethodInfo*))CachedInvokableCall_1__ctor_m7032_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void CachedInvokableCall_1_Invoke_m26727_gshared (CachedInvokableCall_1_t1435 * __this, ObjectU5BU5D_t115* ___args, const MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m26727(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1435 *, ObjectU5BU5D_t115*, const MethodInfo*))CachedInvokableCall_1_Invoke_m26727_gshared)(__this, ___args, method)
