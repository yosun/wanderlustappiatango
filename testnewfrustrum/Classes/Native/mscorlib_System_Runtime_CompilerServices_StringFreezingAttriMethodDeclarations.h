﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t2272;

// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern "C" void StringFreezingAttribute__ctor_m12052 (StringFreezingAttribute_t2272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
