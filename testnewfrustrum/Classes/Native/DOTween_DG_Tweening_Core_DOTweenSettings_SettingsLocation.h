﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocation.h"
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
struct  SettingsLocation_t953 
{
	// System.Int32 DG.Tweening.Core.DOTweenSettings/SettingsLocation::value__
	int32_t ___value___1;
};
