﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t2234;

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
extern "C" void ResolveEventHolder__ctor_m11776 (ResolveEventHolder_t2234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
