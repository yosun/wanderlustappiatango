﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t3391;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Int32[]
struct Int32U5BU5D_t19;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4058;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t3389;

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m18988_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18988(__this, method) (( void (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1__ctor_m18988_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18989_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18989(__this, method) (( bool (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18989_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18990_gshared (Collection_1_t3391 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18990(__this, ___array, ___index, method) (( void (*) (Collection_1_t3391 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18990_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18991_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18991(__this, method) (( Object_t * (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18991_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18992_gshared (Collection_1_t3391 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18992(__this, ___value, method) (( int32_t (*) (Collection_1_t3391 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18992_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18993_gshared (Collection_1_t3391 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18993(__this, ___value, method) (( bool (*) (Collection_1_t3391 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18993_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18994_gshared (Collection_1_t3391 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18994(__this, ___value, method) (( int32_t (*) (Collection_1_t3391 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18994_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18995_gshared (Collection_1_t3391 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18995(__this, ___index, ___value, method) (( void (*) (Collection_1_t3391 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18995_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18996_gshared (Collection_1_t3391 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18996(__this, ___value, method) (( void (*) (Collection_1_t3391 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18996_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18997_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18997(__this, method) (( bool (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18997_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18998_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18998(__this, method) (( Object_t * (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18998_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18999_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18999(__this, method) (( bool (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18999_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m19000_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m19000(__this, method) (( bool (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m19000_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m19001_gshared (Collection_1_t3391 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m19001(__this, ___index, method) (( Object_t * (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m19001_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m19002_gshared (Collection_1_t3391 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m19002(__this, ___index, ___value, method) (( void (*) (Collection_1_t3391 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m19002_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m19003_gshared (Collection_1_t3391 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m19003(__this, ___item, method) (( void (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_Add_m19003_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m19004_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_Clear_m19004(__this, method) (( void (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_Clear_m19004_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m19005_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m19005(__this, method) (( void (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_ClearItems_m19005_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m19006_gshared (Collection_1_t3391 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m19006(__this, ___item, method) (( bool (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_Contains_m19006_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m19007_gshared (Collection_1_t3391 * __this, Int32U5BU5D_t19* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m19007(__this, ___array, ___index, method) (( void (*) (Collection_1_t3391 *, Int32U5BU5D_t19*, int32_t, const MethodInfo*))Collection_1_CopyTo_m19007_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m19008_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m19008(__this, method) (( Object_t* (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_GetEnumerator_m19008_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m19009_gshared (Collection_1_t3391 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m19009(__this, ___item, method) (( int32_t (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m19009_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m19010_gshared (Collection_1_t3391 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m19010(__this, ___index, ___item, method) (( void (*) (Collection_1_t3391 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m19010_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m19011_gshared (Collection_1_t3391 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m19011(__this, ___index, ___item, method) (( void (*) (Collection_1_t3391 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m19011_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m19012_gshared (Collection_1_t3391 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m19012(__this, ___item, method) (( bool (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_Remove_m19012_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m19013_gshared (Collection_1_t3391 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m19013(__this, ___index, method) (( void (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m19013_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m19014_gshared (Collection_1_t3391 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m19014(__this, ___index, method) (( void (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m19014_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m19015_gshared (Collection_1_t3391 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m19015(__this, method) (( int32_t (*) (Collection_1_t3391 *, const MethodInfo*))Collection_1_get_Count_m19015_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m19016_gshared (Collection_1_t3391 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m19016(__this, ___index, method) (( int32_t (*) (Collection_1_t3391 *, int32_t, const MethodInfo*))Collection_1_get_Item_m19016_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m19017_gshared (Collection_1_t3391 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m19017(__this, ___index, ___value, method) (( void (*) (Collection_1_t3391 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m19017_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m19018_gshared (Collection_1_t3391 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m19018(__this, ___index, ___item, method) (( void (*) (Collection_1_t3391 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m19018_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m19019_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m19019(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m19019_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m19020_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m19020(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m19020_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m19021_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m19021(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m19021_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m19022_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m19022(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m19022_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m19023_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m19023(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m19023_gshared)(__this /* static, unused */, ___list, method)
