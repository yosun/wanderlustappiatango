﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t4016;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m27707_gshared (DefaultComparer_t4016 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27707(__this, method) (( void (*) (DefaultComparer_t4016 *, const MethodInfo*))DefaultComparer__ctor_m27707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m27708_gshared (DefaultComparer_t4016 * __this, Guid_t108  ___x, Guid_t108  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m27708(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4016 *, Guid_t108 , Guid_t108 , const MethodInfo*))DefaultComparer_Compare_m27708_gshared)(__this, ___x, ___y, method)
