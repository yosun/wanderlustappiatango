﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t3210;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct  EqualityComparer_1_t3210  : public Object_t
{
};
struct EqualityComparer_1_t3210_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::_default
	EqualityComparer_1_t3210 * ____default_0;
};
