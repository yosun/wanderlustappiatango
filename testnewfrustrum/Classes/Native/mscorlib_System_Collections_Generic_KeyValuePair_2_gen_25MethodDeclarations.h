﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyValuePair_2_t3550;
// System.String
struct String_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m21851_gshared (KeyValuePair_2_t3550 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m21851(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3550 *, int32_t, TrackableResultData_t642 , const MethodInfo*))KeyValuePair_2__ctor_m21851_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m21852_gshared (KeyValuePair_2_t3550 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m21852(__this, method) (( int32_t (*) (KeyValuePair_2_t3550 *, const MethodInfo*))KeyValuePair_2_get_Key_m21852_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m21853_gshared (KeyValuePair_2_t3550 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m21853(__this, ___value, method) (( void (*) (KeyValuePair_2_t3550 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m21853_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
extern "C" TrackableResultData_t642  KeyValuePair_2_get_Value_m21854_gshared (KeyValuePair_2_t3550 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m21854(__this, method) (( TrackableResultData_t642  (*) (KeyValuePair_2_t3550 *, const MethodInfo*))KeyValuePair_2_get_Value_m21854_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m21855_gshared (KeyValuePair_2_t3550 * __this, TrackableResultData_t642  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m21855(__this, ___value, method) (( void (*) (KeyValuePair_2_t3550 *, TrackableResultData_t642 , const MethodInfo*))KeyValuePair_2_set_Value_m21855_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m21856_gshared (KeyValuePair_2_t3550 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m21856(__this, method) (( String_t* (*) (KeyValuePair_2_t3550 *, const MethodInfo*))KeyValuePair_2_ToString_m21856_gshared)(__this, method)
