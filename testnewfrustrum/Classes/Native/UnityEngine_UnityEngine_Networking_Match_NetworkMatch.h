﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Uri
struct Uri_t1273;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t1274  : public MonoBehaviour_t7
{
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t1273 * ___m_BaseUri_3;
};
