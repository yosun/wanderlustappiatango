﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3823;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3814;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25747_gshared (Enumerator_t3823 * __this, Dictionary_2_t3814 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25747(__this, ___host, method) (( void (*) (Enumerator_t3823 *, Dictionary_2_t3814 *, const MethodInfo*))Enumerator__ctor_m25747_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25748_gshared (Enumerator_t3823 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25748(__this, method) (( Object_t * (*) (Enumerator_t3823 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25748_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25749_gshared (Enumerator_t3823 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25749(__this, method) (( void (*) (Enumerator_t3823 *, const MethodInfo*))Enumerator_Dispose_m25749_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25750_gshared (Enumerator_t3823 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25750(__this, method) (( bool (*) (Enumerator_t3823 *, const MethodInfo*))Enumerator_MoveNext_m25750_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m25751_gshared (Enumerator_t3823 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25751(__this, method) (( Object_t * (*) (Enumerator_t3823 *, const MethodInfo*))Enumerator_get_Current_m25751_gshared)(__this, method)
