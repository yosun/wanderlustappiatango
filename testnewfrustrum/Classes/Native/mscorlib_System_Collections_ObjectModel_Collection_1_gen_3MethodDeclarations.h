﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t3588;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3583;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t796;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t3586;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Collection_1__ctor_m22264_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22264(__this, method) (( void (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1__ctor_m22264_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22265_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22265(__this, method) (( bool (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22265_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22266_gshared (Collection_1_t3588 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22266(__this, ___array, ___index, method) (( void (*) (Collection_1_t3588 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22266_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22267_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22267(__this, method) (( Object_t * (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22267_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22268_gshared (Collection_1_t3588 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22268(__this, ___value, method) (( int32_t (*) (Collection_1_t3588 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22268_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22269_gshared (Collection_1_t3588 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22269(__this, ___value, method) (( bool (*) (Collection_1_t3588 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22269_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22270_gshared (Collection_1_t3588 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22270(__this, ___value, method) (( int32_t (*) (Collection_1_t3588 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22270_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22271_gshared (Collection_1_t3588 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22271(__this, ___index, ___value, method) (( void (*) (Collection_1_t3588 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22271_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22272_gshared (Collection_1_t3588 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22272(__this, ___value, method) (( void (*) (Collection_1_t3588 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22272_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22273_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22273(__this, method) (( bool (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22273_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22274_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22274(__this, method) (( Object_t * (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22274_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22275_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22275(__this, method) (( bool (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22275_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22276_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22276(__this, method) (( bool (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22276_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22277_gshared (Collection_1_t3588 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22277(__this, ___index, method) (( Object_t * (*) (Collection_1_t3588 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22277_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22278_gshared (Collection_1_t3588 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22278(__this, ___index, ___value, method) (( void (*) (Collection_1_t3588 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22278_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void Collection_1_Add_m22279_gshared (Collection_1_t3588 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_Add_m22279(__this, ___item, method) (( void (*) (Collection_1_t3588 *, TargetSearchResult_t720 , const MethodInfo*))Collection_1_Add_m22279_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void Collection_1_Clear_m22280_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22280(__this, method) (( void (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_Clear_m22280_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m22281_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22281(__this, method) (( void (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_ClearItems_m22281_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool Collection_1_Contains_m22282_gshared (Collection_1_t3588 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22282(__this, ___item, method) (( bool (*) (Collection_1_t3588 *, TargetSearchResult_t720 , const MethodInfo*))Collection_1_Contains_m22282_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22283_gshared (Collection_1_t3588 * __this, TargetSearchResultU5BU5D_t3583* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22283(__this, ___array, ___index, method) (( void (*) (Collection_1_t3588 *, TargetSearchResultU5BU5D_t3583*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22283_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22284_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22284(__this, method) (( Object_t* (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_GetEnumerator_m22284_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22285_gshared (Collection_1_t3588 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22285(__this, ___item, method) (( int32_t (*) (Collection_1_t3588 *, TargetSearchResult_t720 , const MethodInfo*))Collection_1_IndexOf_m22285_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22286_gshared (Collection_1_t3588 * __this, int32_t ___index, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22286(__this, ___index, ___item, method) (( void (*) (Collection_1_t3588 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))Collection_1_Insert_m22286_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22287_gshared (Collection_1_t3588 * __this, int32_t ___index, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22287(__this, ___index, ___item, method) (( void (*) (Collection_1_t3588 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))Collection_1_InsertItem_m22287_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool Collection_1_Remove_m22288_gshared (Collection_1_t3588 * __this, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22288(__this, ___item, method) (( bool (*) (Collection_1_t3588 *, TargetSearchResult_t720 , const MethodInfo*))Collection_1_Remove_m22288_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22289_gshared (Collection_1_t3588 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22289(__this, ___index, method) (( void (*) (Collection_1_t3588 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22289_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22290_gshared (Collection_1_t3588 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22290(__this, ___index, method) (( void (*) (Collection_1_t3588 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22290_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22291_gshared (Collection_1_t3588 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22291(__this, method) (( int32_t (*) (Collection_1_t3588 *, const MethodInfo*))Collection_1_get_Count_m22291_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t720  Collection_1_get_Item_m22292_gshared (Collection_1_t3588 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22292(__this, ___index, method) (( TargetSearchResult_t720  (*) (Collection_1_t3588 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22292_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22293_gshared (Collection_1_t3588 * __this, int32_t ___index, TargetSearchResult_t720  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22293(__this, ___index, ___value, method) (( void (*) (Collection_1_t3588 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))Collection_1_set_Item_m22293_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22294_gshared (Collection_1_t3588 * __this, int32_t ___index, TargetSearchResult_t720  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22294(__this, ___index, ___item, method) (( void (*) (Collection_1_t3588 *, int32_t, TargetSearchResult_t720 , const MethodInfo*))Collection_1_SetItem_m22294_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22295_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22295(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22295_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
extern "C" TargetSearchResult_t720  Collection_1_ConvertItem_m22296_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22296(__this /* static, unused */, ___item, method) (( TargetSearchResult_t720  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22296_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22297_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22297(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22297_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22298_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22298(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22298_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22299_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22299(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22299_gshared)(__this /* static, unused */, ___list, method)
