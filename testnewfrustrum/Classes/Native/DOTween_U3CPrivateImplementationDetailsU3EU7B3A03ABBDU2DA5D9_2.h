﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
struct  U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016  : public Object_t
{
};
struct U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1016_StaticFields{
	// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}::$$method0x60001fa-1
	__StaticArrayInitTypeSizeU3D120_t1013  ___U24U24method0x60001faU2D1_0;
	// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}::$$method0x60001fa-2
	__StaticArrayInitTypeSizeU3D50_t1014  ___U24U24method0x60001faU2D2_1;
	// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}::$$method0x60001fa-3
	__StaticArrayInitTypeSizeU3D50_t1014  ___U24U24method0x60001faU2D3_2;
	// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}::$$method0x60001fa-4
	__StaticArrayInitTypeSizeU3D20_t1015  ___U24U24method0x60001faU2D4_3;
};
