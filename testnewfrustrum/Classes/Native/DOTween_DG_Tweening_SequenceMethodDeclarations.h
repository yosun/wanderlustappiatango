﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Sequence
struct Sequence_t122;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Sequence::.ctor()
extern "C" void Sequence__ctor_m5338 (Sequence_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern "C" Sequence_t122 * Sequence_DoInsert_m5339 (Object_t * __this /* static, unused */, Sequence_t122 * ___inSequence, Tween_t934 * ___t, float ___atPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Sequence::Reset()
extern "C" void Sequence_Reset_m5340 (Sequence_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::Validate()
extern "C" bool Sequence_Validate_m5341 (Sequence_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::Startup()
extern "C" bool Sequence_Startup_m5342 (Sequence_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool Sequence_ApplyTween_m5343 (Sequence_t122 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern "C" void Sequence_Setup_m5344 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern "C" bool Sequence_DoStartup_m5345 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool Sequence_DoApplyTween_m5346 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern "C" bool Sequence_ApplyInternalCycle_m5347 (Object_t * __this /* static, unused */, Sequence_t122 * ___s, float ___fromPos, float ___toPos, int32_t ___updateMode, bool ___useInverse, bool ___prevPosIsInverse, bool ___multiCycleStep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern "C" int32_t Sequence_SortSequencedObjs_m5348 (Object_t * __this /* static, unused */, ABSSequentiable_t943 * ___a, ABSSequentiable_t943 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
