﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t715;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t827;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4058;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t3390;
// System.Int32[]
struct Int32U5BU5D_t19;
// System.Predicate`1<System.Int32>
struct Predicate_1_t3392;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3396;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m4477_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1__ctor_m4477(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1__ctor_m4477_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m4418_gshared (List_1_t715 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m4418(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1__ctor_m4418_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m18860_gshared (List_1_t715 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m18860(__this, ___capacity, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1__ctor_m18860_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m18862_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m18862(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18862_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864(__this, method) (( Object_t* (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18864_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18866_gshared (List_1_t715 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m18866(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t715 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18866_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m18868_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18868(__this, method) (( Object_t * (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18868_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m18870_gshared (List_1_t715 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m18870(__this, ___item, method) (( int32_t (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18870_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m18872_gshared (List_1_t715 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m18872(__this, ___item, method) (( bool (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m18872_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m18874_gshared (List_1_t715 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m18874(__this, ___item, method) (( int32_t (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m18874_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m18876_gshared (List_1_t715 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m18876(__this, ___index, ___item, method) (( void (*) (List_1_t715 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m18876_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m18878_gshared (List_1_t715 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m18878(__this, ___item, method) (( void (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m18878_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m18882_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18882(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m18882_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m18884_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m18884(__this, method) (( Object_t * (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m18884_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m18886_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m18886(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m18886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m18888_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m18888(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m18888_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m18890_gshared (List_1_t715 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m18890(__this, ___index, method) (( Object_t * (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m18890_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m18892_gshared (List_1_t715 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m18892(__this, ___index, ___value, method) (( void (*) (List_1_t715 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m18892_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m18894_gshared (List_1_t715 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m18894(__this, ___item, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_Add_m18894_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m18896_gshared (List_1_t715 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m18896(__this, ___newCount, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m18896_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m18898_gshared (List_1_t715 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m18898(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddCollection_m18898_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m18900_gshared (List_1_t715 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m18900(__this, ___enumerable, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m18900_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m18902_gshared (List_1_t715 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m18902(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddRange_m18902_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3390 * List_1_AsReadOnly_m18904_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m18904(__this, method) (( ReadOnlyCollection_1_t3390 * (*) (List_1_t715 *, const MethodInfo*))List_1_AsReadOnly_m18904_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m18906_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_Clear_m18906(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Clear_m18906_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m18908_gshared (List_1_t715 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m18908(__this, ___item, method) (( bool (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_Contains_m18908_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m18910_gshared (List_1_t715 * __this, Int32U5BU5D_t19* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m18910(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t715 *, Int32U5BU5D_t19*, int32_t, const MethodInfo*))List_1_CopyTo_m18910_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m18912_gshared (List_1_t715 * __this, Predicate_1_t3392 * ___match, const MethodInfo* method);
#define List_1_Find_m18912(__this, ___match, method) (( int32_t (*) (List_1_t715 *, Predicate_1_t3392 *, const MethodInfo*))List_1_Find_m18912_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m18914_gshared (Object_t * __this /* static, unused */, Predicate_1_t3392 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m18914(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3392 *, const MethodInfo*))List_1_CheckMatch_m18914_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m18916_gshared (List_1_t715 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3392 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m18916(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t715 *, int32_t, int32_t, Predicate_1_t3392 *, const MethodInfo*))List_1_GetIndex_m18916_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t816  List_1_GetEnumerator_m4419_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4419(__this, method) (( Enumerator_t816  (*) (List_1_t715 *, const MethodInfo*))List_1_GetEnumerator_m4419_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m18919_gshared (List_1_t715 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m18919(__this, ___item, method) (( int32_t (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_IndexOf_m18919_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m18921_gshared (List_1_t715 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m18921(__this, ___start, ___delta, method) (( void (*) (List_1_t715 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m18921_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m18923_gshared (List_1_t715 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m18923(__this, ___index, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_CheckIndex_m18923_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m18925_gshared (List_1_t715 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m18925(__this, ___index, ___item, method) (( void (*) (List_1_t715 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m18925_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m18927_gshared (List_1_t715 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m18927(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m18927_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m18929_gshared (List_1_t715 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m18929(__this, ___item, method) (( bool (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_Remove_m18929_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m18931_gshared (List_1_t715 * __this, Predicate_1_t3392 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m18931(__this, ___match, method) (( int32_t (*) (List_1_t715 *, Predicate_1_t3392 *, const MethodInfo*))List_1_RemoveAll_m18931_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m18933_gshared (List_1_t715 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m18933(__this, ___index, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_RemoveAt_m18933_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m18935_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_Reverse_m18935(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Reverse_m18935_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m18937_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_Sort_m18937(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Sort_m18937_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m18939_gshared (List_1_t715 * __this, Comparison_1_t3396 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m18939(__this, ___comparison, method) (( void (*) (List_1_t715 *, Comparison_1_t3396 *, const MethodInfo*))List_1_Sort_m18939_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t19* List_1_ToArray_m18941_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_ToArray_m18941(__this, method) (( Int32U5BU5D_t19* (*) (List_1_t715 *, const MethodInfo*))List_1_ToArray_m18941_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m18943_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m18943(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_TrimExcess_m18943_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m18945_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m18945(__this, method) (( int32_t (*) (List_1_t715 *, const MethodInfo*))List_1_get_Capacity_m18945_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m18947_gshared (List_1_t715 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m18947(__this, ___value, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_set_Capacity_m18947_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m18949_gshared (List_1_t715 * __this, const MethodInfo* method);
#define List_1_get_Count_m18949(__this, method) (( int32_t (*) (List_1_t715 *, const MethodInfo*))List_1_get_Count_m18949_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m18951_gshared (List_1_t715 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m18951(__this, ___index, method) (( int32_t (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_get_Item_m18951_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m18953_gshared (List_1_t715 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m18953(__this, ___index, ___value, method) (( void (*) (List_1_t715 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m18953_gshared)(__this, ___index, ___value, method)
