﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t720;
struct TargetSearchResult_t720_marshaled;

void TargetSearchResult_t720_marshal(const TargetSearchResult_t720& unmarshaled, TargetSearchResult_t720_marshaled& marshaled);
void TargetSearchResult_t720_marshal_back(const TargetSearchResult_t720_marshaled& marshaled, TargetSearchResult_t720& unmarshaled);
void TargetSearchResult_t720_marshal_cleanup(TargetSearchResult_t720_marshaled& marshaled);
