﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>
struct GenericEqualityComparer_1_t3828;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m25775_gshared (GenericEqualityComparer_1_t3828 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m25775(__this, method) (( void (*) (GenericEqualityComparer_1_t3828 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m25775_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m25776_gshared (GenericEqualityComparer_1_t3828 * __this, uint64_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m25776(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3828 *, uint64_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m25776_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt64>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m25777_gshared (GenericEqualityComparer_1_t3828 * __this, uint64_t ___x, uint64_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m25777(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3828 *, uint64_t, uint64_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m25777_gshared)(__this, ___x, ___y, method)
