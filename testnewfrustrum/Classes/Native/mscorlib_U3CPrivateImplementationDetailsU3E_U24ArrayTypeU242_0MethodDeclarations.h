﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct U24ArrayTypeU2420_t2567;
struct U24ArrayTypeU2420_t2567_marshaled;

void U24ArrayTypeU2420_t2567_marshal(const U24ArrayTypeU2420_t2567& unmarshaled, U24ArrayTypeU2420_t2567_marshaled& marshaled);
void U24ArrayTypeU2420_t2567_marshal_back(const U24ArrayTypeU2420_t2567_marshaled& marshaled, U24ArrayTypeU2420_t2567& unmarshaled);
void U24ArrayTypeU2420_t2567_marshal_cleanup(U24ArrayTypeU2420_t2567_marshaled& marshaled);
