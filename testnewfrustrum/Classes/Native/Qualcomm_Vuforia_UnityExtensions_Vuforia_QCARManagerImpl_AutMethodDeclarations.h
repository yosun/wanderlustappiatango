﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/AutoRotationState
struct AutoRotationState_t654;
struct AutoRotationState_t654_marshaled;

void AutoRotationState_t654_marshal(const AutoRotationState_t654& unmarshaled, AutoRotationState_t654_marshaled& marshaled);
void AutoRotationState_t654_marshal_back(const AutoRotationState_t654_marshaled& marshaled, AutoRotationState_t654& unmarshaled);
void AutoRotationState_t654_marshal_cleanup(AutoRotationState_t654_marshaled& marshaled);
