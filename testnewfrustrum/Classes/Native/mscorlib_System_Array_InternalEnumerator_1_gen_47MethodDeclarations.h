﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct InternalEnumerator_1_t3584;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22223_gshared (InternalEnumerator_1_t3584 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22223(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3584 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22223_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22224_gshared (InternalEnumerator_1_t3584 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22224(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3584 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22224_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22225_gshared (InternalEnumerator_1_t3584 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22225(__this, method) (( void (*) (InternalEnumerator_1_t3584 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22225_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22226_gshared (InternalEnumerator_1_t3584 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22226(__this, method) (( bool (*) (InternalEnumerator_1_t3584 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22226_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t720  InternalEnumerator_1_get_Current_m22227_gshared (InternalEnumerator_1_t3584 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22227(__this, method) (( TargetSearchResult_t720  (*) (InternalEnumerator_1_t3584 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22227_gshared)(__this, method)
