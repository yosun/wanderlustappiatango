﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct ShimEnumerator_t3560;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m21923_gshared (ShimEnumerator_t3560 * __this, Dictionary_2_t869 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m21923(__this, ___host, method) (( void (*) (ShimEnumerator_t3560 *, Dictionary_2_t869 *, const MethodInfo*))ShimEnumerator__ctor_m21923_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m21924_gshared (ShimEnumerator_t3560 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m21924(__this, method) (( bool (*) (ShimEnumerator_t3560 *, const MethodInfo*))ShimEnumerator_MoveNext_m21924_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m21925_gshared (ShimEnumerator_t3560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m21925(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3560 *, const MethodInfo*))ShimEnumerator_get_Entry_m21925_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m21926_gshared (ShimEnumerator_t3560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m21926(__this, method) (( Object_t * (*) (ShimEnumerator_t3560 *, const MethodInfo*))ShimEnumerator_get_Key_m21926_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m21927_gshared (ShimEnumerator_t3560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m21927(__this, method) (( Object_t * (*) (ShimEnumerator_t3560 *, const MethodInfo*))ShimEnumerator_get_Value_m21927_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m21928_gshared (ShimEnumerator_t3560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m21928(__this, method) (( Object_t * (*) (ShimEnumerator_t3560 *, const MethodInfo*))ShimEnumerator_get_Current_m21928_gshared)(__this, method)
