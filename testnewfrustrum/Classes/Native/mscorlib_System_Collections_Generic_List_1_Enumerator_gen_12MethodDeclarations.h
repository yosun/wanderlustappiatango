﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>
struct Enumerator_t849;
// System.Object
struct Object_t;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t777;
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t705;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21093(__this, ___l, method) (( void (*) (Enumerator_t849 *, List_1_t705 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21094(__this, method) (( Object_t * (*) (Enumerator_t849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::Dispose()
#define Enumerator_Dispose_m21095(__this, method) (( void (*) (Enumerator_t849 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::VerifyState()
#define Enumerator_VerifyState_m21096(__this, method) (( void (*) (Enumerator_t849 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4517(__this, method) (( bool (*) (Enumerator_t849 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::get_Current()
#define Enumerator_get_Current_m4516(__this, method) (( Object_t * (*) (Enumerator_t849 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
