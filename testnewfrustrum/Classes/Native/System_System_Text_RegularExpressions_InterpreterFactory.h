﻿#pragma once
#include <stdint.h>
// System.Collections.IDictionary
struct IDictionary_t1931;
// System.UInt16[]
struct UInt16U5BU5D_t1060;
// System.String[]
struct StringU5BU5D_t109;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.InterpreterFactory
struct  InterpreterFactory_t1943  : public Object_t
{
	// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::mapping
	Object_t * ___mapping_0;
	// System.UInt16[] System.Text.RegularExpressions.InterpreterFactory::pattern
	UInt16U5BU5D_t1060* ___pattern_1;
	// System.String[] System.Text.RegularExpressions.InterpreterFactory::namesMapping
	StringU5BU5D_t109* ___namesMapping_2;
	// System.Int32 System.Text.RegularExpressions.InterpreterFactory::gap
	int32_t ___gap_3;
};
