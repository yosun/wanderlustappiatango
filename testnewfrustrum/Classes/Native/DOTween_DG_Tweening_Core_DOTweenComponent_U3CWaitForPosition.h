﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t935;
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
struct  U3CWaitForPositionU3Ed__8_t940  : public Object_t
{
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::<>2__current
	Object_t * ___U3CU3E2__current_0;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::<>1__state
	int32_t ___U3CU3E1__state_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::t
	Tween_t934 * ___t_2;
	// System.Single DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::position
	float ___position_3;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::<>4__this
	DOTweenComponent_t935 * ___U3CU3E4__this_4;
};
