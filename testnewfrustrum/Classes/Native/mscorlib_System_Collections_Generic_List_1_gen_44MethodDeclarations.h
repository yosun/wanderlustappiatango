﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t754;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t790;
// System.Collections.Generic.IEnumerable`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerable_1_t4276;
// System.Collections.Generic.IEnumerator`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerator_1_t4277;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ICollection_1_t4278;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ReadOnlyCollection_1_t3645;
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t3643;
// System.Predicate`1<Vuforia.IUserDefinedTargetEventHandler>
struct Predicate_1_t3646;
// System.Comparison`1<Vuforia.IUserDefinedTargetEventHandler>
struct Comparison_1_t3647;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4655(__this, method) (( void (*) (List_1_t754 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23051(__this, ___collection, method) (( void (*) (List_1_t754 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m23052(__this, ___capacity, method) (( void (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.cctor()
#define List_1__cctor_m23053(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23054(__this, method) (( Object_t* (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23055(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t754 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23056(__this, method) (( Object_t * (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23057(__this, ___item, method) (( int32_t (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23058(__this, ___item, method) (( bool (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23059(__this, ___item, method) (( int32_t (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23060(__this, ___index, ___item, method) (( void (*) (List_1_t754 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23061(__this, ___item, method) (( void (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23062(__this, method) (( bool (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23063(__this, method) (( bool (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23064(__this, method) (( Object_t * (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23065(__this, method) (( bool (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23066(__this, method) (( bool (*) (List_1_t754 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23067(__this, ___index, method) (( Object_t * (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23068(__this, ___index, ___value, method) (( void (*) (List_1_t754 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Add(T)
#define List_1_Add_m23069(__this, ___item, method) (( void (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23070(__this, ___newCount, method) (( void (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23071(__this, ___collection, method) (( void (*) (List_1_t754 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23072(__this, ___enumerable, method) (( void (*) (List_1_t754 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23073(__this, ___collection, method) (( void (*) (List_1_t754 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23074(__this, method) (( ReadOnlyCollection_1_t3645 * (*) (List_1_t754 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Clear()
#define List_1_Clear_m23075(__this, method) (( void (*) (List_1_t754 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Contains(T)
#define List_1_Contains_m23076(__this, ___item, method) (( bool (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23077(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t754 *, IUserDefinedTargetEventHandlerU5BU5D_t3643*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23078(__this, ___match, method) (( Object_t * (*) (List_1_t754 *, Predicate_1_t3646 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23079(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3646 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23080(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t754 *, int32_t, int32_t, Predicate_1_t3646 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4652(__this, method) (( Enumerator_t893  (*) (List_1_t754 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23081(__this, ___item, method) (( int32_t (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23082(__this, ___start, ___delta, method) (( void (*) (List_1_t754 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23083(__this, ___index, method) (( void (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23084(__this, ___index, ___item, method) (( void (*) (List_1_t754 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23085(__this, ___collection, method) (( void (*) (List_1_t754 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Remove(T)
#define List_1_Remove_m23086(__this, ___item, method) (( bool (*) (List_1_t754 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23087(__this, ___match, method) (( int32_t (*) (List_1_t754 *, Predicate_1_t3646 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23088(__this, ___index, method) (( void (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Reverse()
#define List_1_Reverse_m23089(__this, method) (( void (*) (List_1_t754 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort()
#define List_1_Sort_m23090(__this, method) (( void (*) (List_1_t754 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23091(__this, ___comparison, method) (( void (*) (List_1_t754 *, Comparison_1_t3647 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::ToArray()
#define List_1_ToArray_m23092(__this, method) (( IUserDefinedTargetEventHandlerU5BU5D_t3643* (*) (List_1_t754 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23093(__this, method) (( void (*) (List_1_t754 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23094(__this, method) (( int32_t (*) (List_1_t754 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23095(__this, ___value, method) (( void (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Count()
#define List_1_get_Count_m23096(__this, method) (( int32_t (*) (List_1_t754 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23097(__this, ___index, method) (( Object_t * (*) (List_1_t754 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23098(__this, ___index, ___value, method) (( void (*) (List_1_t754 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
