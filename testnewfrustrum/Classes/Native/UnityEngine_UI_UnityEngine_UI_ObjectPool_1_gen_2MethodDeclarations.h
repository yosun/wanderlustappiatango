﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t383;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t384;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t418;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2423(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t383 *, UnityAction_1_t384 *, UnityAction_1_t384 *, const MethodInfo*))ObjectPool_1__ctor_m15407_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m18275(__this, method) (( int32_t (*) (ObjectPool_1_t383 *, const MethodInfo*))ObjectPool_1_get_countAll_m15409_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18276(__this, ___value, method) (( void (*) (ObjectPool_1_t383 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15411_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m18277(__this, method) (( int32_t (*) (ObjectPool_1_t383 *, const MethodInfo*))ObjectPool_1_get_countActive_m15413_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18278(__this, method) (( int32_t (*) (ObjectPool_1_t383 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15415_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m2424(__this, method) (( List_1_t418 * (*) (ObjectPool_1_t383 *, const MethodInfo*))ObjectPool_1_Get_m15417_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m2425(__this, ___element, method) (( void (*) (ObjectPool_1_t383 *, List_1_t418 *, const MethodInfo*))ObjectPool_1_Release_m15419_gshared)(__this, ___element, method)
