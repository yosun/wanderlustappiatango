﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
struct __StaticArrayInitTypeSizeU3D20_t1015;
struct __StaticArrayInitTypeSizeU3D20_t1015_marshaled;

void __StaticArrayInitTypeSizeU3D20_t1015_marshal(const __StaticArrayInitTypeSizeU3D20_t1015& unmarshaled, __StaticArrayInitTypeSizeU3D20_t1015_marshaled& marshaled);
void __StaticArrayInitTypeSizeU3D20_t1015_marshal_back(const __StaticArrayInitTypeSizeU3D20_t1015_marshaled& marshaled, __StaticArrayInitTypeSizeU3D20_t1015& unmarshaled);
void __StaticArrayInitTypeSizeU3D20_t1015_marshal_cleanup(__StaticArrayInitTypeSizeU3D20_t1015_marshaled& marshaled);
