﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3557;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21906_gshared (Enumerator_t3557 * __this, Dictionary_2_t869 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21906(__this, ___host, method) (( void (*) (Enumerator_t3557 *, Dictionary_2_t869 *, const MethodInfo*))Enumerator__ctor_m21906_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21907_gshared (Enumerator_t3557 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21907(__this, method) (( Object_t * (*) (Enumerator_t3557 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21907_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m21908_gshared (Enumerator_t3557 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21908(__this, method) (( void (*) (Enumerator_t3557 *, const MethodInfo*))Enumerator_Dispose_m21908_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21909_gshared (Enumerator_t3557 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21909(__this, method) (( bool (*) (Enumerator_t3557 *, const MethodInfo*))Enumerator_MoveNext_m21909_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t642  Enumerator_get_Current_m21910_gshared (Enumerator_t3557 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21910(__this, method) (( TrackableResultData_t642  (*) (Enumerator_t3557 *, const MethodInfo*))Enumerator_get_Current_m21910_gshared)(__this, method)
