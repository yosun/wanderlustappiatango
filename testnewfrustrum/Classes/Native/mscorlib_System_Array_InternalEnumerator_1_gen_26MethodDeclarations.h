﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>
struct InternalEnumerator_1_t3356;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18298_gshared (InternalEnumerator_1_t3356 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18298(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3356 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18298_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18299_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18299(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18299_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18300_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18300(__this, method) (( void (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18300_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18301_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18301(__this, method) (( bool (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18301_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::get_Current()
extern "C" EyewearCalibrationReading_t586  InternalEnumerator_1_get_Current_m18302_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18302(__this, method) (( EyewearCalibrationReading_t586  (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18302_gshared)(__this, method)
