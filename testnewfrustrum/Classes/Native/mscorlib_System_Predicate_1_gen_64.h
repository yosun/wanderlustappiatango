﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2438;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Security.Policy.StrongName>
struct  Predicate_1_t4004  : public MulticastDelegate_t307
{
};
