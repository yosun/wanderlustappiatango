﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t88;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t610;

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern "C" bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4253 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4254 (WebCamAbstractBehaviour_t88 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern "C" String_t* WebCamAbstractBehaviour_get_DeviceName_m4255 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern "C" void WebCamAbstractBehaviour_set_DeviceName_m4256 (WebCamAbstractBehaviour_t88 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern "C" bool WebCamAbstractBehaviour_get_FlipHorizontally_m4257 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_FlipHorizontally_m4258 (WebCamAbstractBehaviour_t88 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern "C" bool WebCamAbstractBehaviour_get_TurnOffWebCam_m4259 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_TurnOffWebCam_m4260 (WebCamAbstractBehaviour_t88 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern "C" bool WebCamAbstractBehaviour_get_IsPlaying_m4261 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern "C" bool WebCamAbstractBehaviour_IsWebCamUsed_m4262 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern "C" WebCamImpl_t610 * WebCamAbstractBehaviour_get_ImplementationClass_m4263 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern "C" void WebCamAbstractBehaviour_InitCamera_m4264 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
extern "C" bool WebCamAbstractBehaviour_CheckNativePluginSupport_m4265 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern "C" void WebCamAbstractBehaviour_OnLevelWasLoaded_m4266 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern "C" void WebCamAbstractBehaviour_OnDestroy_m4267 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern "C" void WebCamAbstractBehaviour_Update_m4268 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
extern "C" int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4269 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C" void WebCamAbstractBehaviour__ctor_m477 (WebCamAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
