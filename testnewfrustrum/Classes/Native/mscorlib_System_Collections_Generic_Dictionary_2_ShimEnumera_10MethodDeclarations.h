﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t3963;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1932;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m27382_gshared (ShimEnumerator_t3963 * __this, Dictionary_2_t1932 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m27382(__this, ___host, method) (( void (*) (ShimEnumerator_t3963 *, Dictionary_2_t1932 *, const MethodInfo*))ShimEnumerator__ctor_m27382_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m27383_gshared (ShimEnumerator_t3963 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m27383(__this, method) (( bool (*) (ShimEnumerator_t3963 *, const MethodInfo*))ShimEnumerator_MoveNext_m27383_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m27384_gshared (ShimEnumerator_t3963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m27384(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3963 *, const MethodInfo*))ShimEnumerator_get_Entry_m27384_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m27385_gshared (ShimEnumerator_t3963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m27385(__this, method) (( Object_t * (*) (ShimEnumerator_t3963 *, const MethodInfo*))ShimEnumerator_get_Key_m27385_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m27386_gshared (ShimEnumerator_t3963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m27386(__this, method) (( Object_t * (*) (ShimEnumerator_t3963 *, const MethodInfo*))ShimEnumerator_get_Value_m27386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m27387_gshared (ShimEnumerator_t3963 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m27387(__this, method) (( Object_t * (*) (ShimEnumerator_t3963 *, const MethodInfo*))ShimEnumerator_get_Current_m27387_gshared)(__this, method)
