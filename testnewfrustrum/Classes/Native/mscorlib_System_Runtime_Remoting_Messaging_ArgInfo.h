﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t19;
// System.Reflection.MethodBase
struct MethodBase_t1434;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.ArgInfo
struct  ArgInfo_t2311  : public Object_t
{
	// System.Int32[] System.Runtime.Remoting.Messaging.ArgInfo::_paramMap
	Int32U5BU5D_t19* ____paramMap_0;
	// System.Int32 System.Runtime.Remoting.Messaging.ArgInfo::_inoutArgCount
	int32_t ____inoutArgCount_1;
	// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ArgInfo::_method
	MethodBase_t1434 * ____method_2;
};
