﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t315;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t4092;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4089;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t468;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t3269;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t312;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3270;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t3272;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_36.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m2201_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1__ctor_m2201(__this, method) (( void (*) (List_1_t315 *, const MethodInfo*))List_1__ctor_m2201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17143_gshared (List_1_t315 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17143(__this, ___collection, method) (( void (*) (List_1_t315 *, Object_t*, const MethodInfo*))List_1__ctor_m17143_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m6948_gshared (List_1_t315 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m6948(__this, ___capacity, method) (( void (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1__ctor_m6948_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m17144_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17144(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17144_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17145_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17145(__this, method) (( Object_t* (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17146_gshared (List_1_t315 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17146(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t315 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17146_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17147_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17147(__this, method) (( Object_t * (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17147_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17148_gshared (List_1_t315 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17148(__this, ___item, method) (( int32_t (*) (List_1_t315 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17148_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17149_gshared (List_1_t315 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17149(__this, ___item, method) (( bool (*) (List_1_t315 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17149_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17150_gshared (List_1_t315 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17150(__this, ___item, method) (( int32_t (*) (List_1_t315 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17150_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17151_gshared (List_1_t315 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17151(__this, ___index, ___item, method) (( void (*) (List_1_t315 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17151_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17152_gshared (List_1_t315 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17152(__this, ___item, method) (( void (*) (List_1_t315 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17152_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17153_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17153(__this, method) (( bool (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17154_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17154(__this, method) (( bool (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17154_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17155_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17155(__this, method) (( Object_t * (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17155_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17156_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17156(__this, method) (( bool (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17157_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17157(__this, method) (( bool (*) (List_1_t315 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17157_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17158_gshared (List_1_t315 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17158(__this, ___index, method) (( Object_t * (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17158_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17159_gshared (List_1_t315 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17159(__this, ___index, ___value, method) (( void (*) (List_1_t315 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17159_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m17160_gshared (List_1_t315 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define List_1_Add_m17160(__this, ___item, method) (( void (*) (List_1_t315 *, UIVertex_t313 , const MethodInfo*))List_1_Add_m17160_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17161_gshared (List_1_t315 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17161(__this, ___newCount, method) (( void (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17161_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17162_gshared (List_1_t315 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17162(__this, ___collection, method) (( void (*) (List_1_t315 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17162_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17163_gshared (List_1_t315 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17163(__this, ___enumerable, method) (( void (*) (List_1_t315 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17163_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17164_gshared (List_1_t315 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17164(__this, ___collection, method) (( void (*) (List_1_t315 *, Object_t*, const MethodInfo*))List_1_AddRange_m17164_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3269 * List_1_AsReadOnly_m17165_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17165(__this, method) (( ReadOnlyCollection_1_t3269 * (*) (List_1_t315 *, const MethodInfo*))List_1_AsReadOnly_m17165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m17166_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_Clear_m17166(__this, method) (( void (*) (List_1_t315 *, const MethodInfo*))List_1_Clear_m17166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m17167_gshared (List_1_t315 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define List_1_Contains_m17167(__this, ___item, method) (( bool (*) (List_1_t315 *, UIVertex_t313 , const MethodInfo*))List_1_Contains_m17167_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17168_gshared (List_1_t315 * __this, UIVertexU5BU5D_t312* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17168(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t315 *, UIVertexU5BU5D_t312*, int32_t, const MethodInfo*))List_1_CopyTo_m17168_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t313  List_1_Find_m17169_gshared (List_1_t315 * __this, Predicate_1_t3270 * ___match, const MethodInfo* method);
#define List_1_Find_m17169(__this, ___match, method) (( UIVertex_t313  (*) (List_1_t315 *, Predicate_1_t3270 *, const MethodInfo*))List_1_Find_m17169_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17170_gshared (Object_t * __this /* static, unused */, Predicate_1_t3270 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17170(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3270 *, const MethodInfo*))List_1_CheckMatch_m17170_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17171_gshared (List_1_t315 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3270 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17171(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t315 *, int32_t, int32_t, Predicate_1_t3270 *, const MethodInfo*))List_1_GetIndex_m17171_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t3271  List_1_GetEnumerator_m17172_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17172(__this, method) (( Enumerator_t3271  (*) (List_1_t315 *, const MethodInfo*))List_1_GetEnumerator_m17172_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17173_gshared (List_1_t315 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17173(__this, ___item, method) (( int32_t (*) (List_1_t315 *, UIVertex_t313 , const MethodInfo*))List_1_IndexOf_m17173_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17174_gshared (List_1_t315 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17174(__this, ___start, ___delta, method) (( void (*) (List_1_t315 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17174_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17175_gshared (List_1_t315 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17175(__this, ___index, method) (( void (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17175_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17176_gshared (List_1_t315 * __this, int32_t ___index, UIVertex_t313  ___item, const MethodInfo* method);
#define List_1_Insert_m17176(__this, ___index, ___item, method) (( void (*) (List_1_t315 *, int32_t, UIVertex_t313 , const MethodInfo*))List_1_Insert_m17176_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17177_gshared (List_1_t315 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17177(__this, ___collection, method) (( void (*) (List_1_t315 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17177_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m17178_gshared (List_1_t315 * __this, UIVertex_t313  ___item, const MethodInfo* method);
#define List_1_Remove_m17178(__this, ___item, method) (( bool (*) (List_1_t315 *, UIVertex_t313 , const MethodInfo*))List_1_Remove_m17178_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17179_gshared (List_1_t315 * __this, Predicate_1_t3270 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17179(__this, ___match, method) (( int32_t (*) (List_1_t315 *, Predicate_1_t3270 *, const MethodInfo*))List_1_RemoveAll_m17179_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17180_gshared (List_1_t315 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17180(__this, ___index, method) (( void (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17180_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m17181_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_Reverse_m17181(__this, method) (( void (*) (List_1_t315 *, const MethodInfo*))List_1_Reverse_m17181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m17182_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_Sort_m17182(__this, method) (( void (*) (List_1_t315 *, const MethodInfo*))List_1_Sort_m17182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17183_gshared (List_1_t315 * __this, Comparison_1_t3272 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17183(__this, ___comparison, method) (( void (*) (List_1_t315 *, Comparison_1_t3272 *, const MethodInfo*))List_1_Sort_m17183_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t312* List_1_ToArray_m2256_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_ToArray_m2256(__this, method) (( UIVertexU5BU5D_t312* (*) (List_1_t315 *, const MethodInfo*))List_1_ToArray_m2256_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m17184_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17184(__this, method) (( void (*) (List_1_t315 *, const MethodInfo*))List_1_TrimExcess_m17184_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m2119_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2119(__this, method) (( int32_t (*) (List_1_t315 *, const MethodInfo*))List_1_get_Capacity_m2119_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m2120_gshared (List_1_t315 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m2120(__this, ___value, method) (( void (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2120_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m17185_gshared (List_1_t315 * __this, const MethodInfo* method);
#define List_1_get_Count_m17185(__this, method) (( int32_t (*) (List_1_t315 *, const MethodInfo*))List_1_get_Count_m17185_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t313  List_1_get_Item_m17186_gshared (List_1_t315 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17186(__this, ___index, method) (( UIVertex_t313  (*) (List_1_t315 *, int32_t, const MethodInfo*))List_1_get_Item_m17186_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17187_gshared (List_1_t315 * __this, int32_t ___index, UIVertex_t313  ___value, const MethodInfo* method);
#define List_1_set_Item_m17187(__this, ___index, ___value, method) (( void (*) (List_1_t315 *, int32_t, UIVertex_t313 , const MethodInfo*))List_1_set_Item_m17187_gshared)(__this, ___index, ___value, method)
