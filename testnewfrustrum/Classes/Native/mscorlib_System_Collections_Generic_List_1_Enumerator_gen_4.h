﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t624;
// Vuforia.DataSetImpl
struct DataSetImpl_t578;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
struct  Enumerator_t809 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::l
	List_1_t624 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::current
	DataSetImpl_t578 * ___current_3;
};
