﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct IList_1_t3797;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  ReadOnlyCollection_1_t3798  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::list
	Object_t* ___list_0;
};
