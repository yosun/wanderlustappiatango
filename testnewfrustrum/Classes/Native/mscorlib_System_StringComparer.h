﻿#pragma once
#include <stdint.h>
// System.StringComparer
struct StringComparer_t1390;
// System.Object
#include "mscorlib_System_Object.h"
// System.StringComparer
struct  StringComparer_t1390  : public Object_t
{
};
struct StringComparer_t1390_StaticFields{
	// System.StringComparer System.StringComparer::invariantCultureIgnoreCase
	StringComparer_t1390 * ___invariantCultureIgnoreCase_0;
	// System.StringComparer System.StringComparer::invariantCulture
	StringComparer_t1390 * ___invariantCulture_1;
	// System.StringComparer System.StringComparer::ordinalIgnoreCase
	StringComparer_t1390 * ___ordinalIgnoreCase_2;
	// System.StringComparer System.StringComparer::ordinal
	StringComparer_t1390 * ___ordinal_3;
};
