﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1796;
struct U24ArrayTypeU2412_t1796_marshaled;

void U24ArrayTypeU2412_t1796_marshal(const U24ArrayTypeU2412_t1796& unmarshaled, U24ArrayTypeU2412_t1796_marshaled& marshaled);
void U24ArrayTypeU2412_t1796_marshal_back(const U24ArrayTypeU2412_t1796_marshaled& marshaled, U24ArrayTypeU2412_t1796& unmarshaled);
void U24ArrayTypeU2412_t1796_marshal_cleanup(U24ArrayTypeU2412_t1796_marshaled& marshaled);
