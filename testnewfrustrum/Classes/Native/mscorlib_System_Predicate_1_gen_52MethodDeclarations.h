﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<DG.Tweening.Core.ABSSequentiable>
struct Predicate_1_t3677;
// System.Object
struct Object_t;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m23586(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3677 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::Invoke(T)
#define Predicate_1_Invoke_m23587(__this, ___obj, method) (( bool (*) (Predicate_1_t3677 *, ABSSequentiable_t943 *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23588(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3677 *, ABSSequentiable_t943 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23589(__this, ___result, method) (( bool (*) (Predicate_1_t3677 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
