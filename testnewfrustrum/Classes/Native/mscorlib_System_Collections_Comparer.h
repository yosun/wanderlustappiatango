﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t2145;
// System.Globalization.CompareInfo
struct CompareInfo_t1827;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct  Comparer_t2145  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t1827 * ___m_compareInfo_2;
};
struct Comparer_t2145_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t2145 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t2145 * ___DefaultInvariant_1;
};
